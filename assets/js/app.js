require('jquery');

const imagesContext = require.context('../images', true, /\.(png|jpg|jpeg|gif|ico|svg|webp)$/);
imagesContext.keys().forEach(imagesContext);


import "@fortawesome/fontawesome-free/js/all";

const $ = require('jquery');
global.$ = global.jQuery = $;
require('jquery-ui');
require('jquery-ui/ui/widgets/sortable');

require('../scss/bootstrap-custom.scss');
window.bootstrap = require('bootstrap');
import {Tooltip, Toast, Popover, Modal} from 'bootstrap';
require('floatthead/dist/jquery.floatThead')

require('select2');

import Chart from 'chart.js/auto';
global.Chart=Chart;

import ChartDataLabels from 'chartjs-plugin-datalabels';
Chart.register(ChartDataLabels)




const palette = require('google-palette')
global.palette=palette;

import Autocomplete from '@trevoreyre/autocomplete-js'

global.Autocomplete = Autocomplete;

import '@pugx/filter-bundle/js/filter';

import {createApp} from 'vue';
import SyncTable from "./components/SyncTable.vue";
global.VueCreateApp = createApp;
global.SyncTable = SyncTable;
globalThis.__VUE_PROD_DEVTOOLS__ = process.env.NODE_ENV === "development" || process.env.NODE_ENV === "dev";

import axios from "axios";
global.axios = axios;

$(document).ready(function () {
    'use strict';

    if (jQuery().pugxFilter) {
        $('#filter').pugxFilter();
    }

    /** select2 no focus workaround **/
    $(document).on('select2:open', () => {
        let allFound = document.querySelectorAll('.select2-container--open .select2-search__field');
        $(this).one('mouseup keyup', () => {
            setTimeout(() => {
                allFound[allFound.length - 1].focus();
            }, 0);
        });
    });

    $(document).ajaxError(function (event, jqXHR) {
        if (jqXHR.status === 403 && jqXHR.responseText === "Authentication timeout") {
            window.location.reload();
        }
    });

    $('.table-responsive').on('shown.bs.dropdown', function (e) {
        let $tableCont = $(this),
            $menuBtn = $(e.target),
            $menu = $menuBtn.parent().find(".dropdown-menu"),
            menuBtnTop = $menuBtn.offset().top - $tableCont.offset().top,
            menuBtnBottom = menuBtnTop + $menuBtn.outerHeight(true),
            lowerMenuBottom = menuBtnBottom + $menu.outerHeight(true);

        if (menuBtnTop < $menu.outerHeight(true) &&  lowerMenuBottom > $tableCont.height())
            $tableCont.css("padding-bottom", $menu.outerHeight(true));
    }).on('hide.bs.dropdown', function () {
        $(this).css("padding-bottom", 0);
    });

});
