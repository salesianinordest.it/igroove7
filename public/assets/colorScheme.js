(self["webpackChunk"] = self["webpackChunk"] || []).push([["colorScheme"],{

/***/ "./assets/js/colorScheme.js":
/*!**********************************!*\
  !*** ./assets/js/colorScheme.js ***!
  \**********************************/
/***/ (() => {

/*!
 * Color mode toggler for Bootstrap's docs (https://getbootstrap.com/)
 * Copyright 2011-2023 The Bootstrap Authors
 * Licensed under the Creative Commons Attribution 3.0 Unported License.
 */
(function () {
  'use strict';

  var getStoredTheme = function getStoredTheme() {
    return localStorage.getItem('theme');
  };

  var setStoredTheme = function setStoredTheme(theme) {
    return localStorage.setItem('theme', theme);
  };

  var getPreferredTheme = function getPreferredTheme() {
    var storedTheme = getStoredTheme();

    if (storedTheme) {
      return storedTheme;
    }

    return 'auto';
  };

  var setTheme = function setTheme(theme) {
    if (theme === 'auto' && window.matchMedia('(prefers-color-scheme: dark)').matches) {
      document.documentElement.setAttribute('data-bs-theme', 'dark');
    } else {
      document.documentElement.setAttribute('data-bs-theme', theme);
    }
  };

  setTheme(getPreferredTheme());

  var showActiveTheme = function showActiveTheme(theme) {
    var focus = arguments.length > 1 && arguments[1] !== undefined ? arguments[1] : false;
    var themeSwitcher = document.querySelector('#bd-theme');

    if (!themeSwitcher) {
      return;
    }

    var btnToActive = document.querySelector("[data-bs-theme-value=\"".concat(theme, "\"]"));
    document.querySelectorAll('[data-bs-theme-value]').forEach(function (element) {
      element.classList.remove('active');
      element.setAttribute('aria-pressed', 'false');
    });
    btnToActive.classList.add('active');
    btnToActive.setAttribute('aria-pressed', 'true');

    if (focus) {
      themeSwitcher.focus();
    }
  };

  window.matchMedia('(prefers-color-scheme: dark)').addEventListener('change', function () {
    var storedTheme = getStoredTheme();

    if (storedTheme !== 'light' && storedTheme !== 'dark') {
      setTheme(getPreferredTheme());
    }
  });
  window.addEventListener('DOMContentLoaded', function () {
    showActiveTheme(getPreferredTheme());
    document.querySelectorAll('[data-bs-theme-value]').forEach(function (toggle) {
      toggle.addEventListener('click', function () {
        var theme = toggle.getAttribute('data-bs-theme-value');
        setStoredTheme(theme);
        setTheme(theme);
        showActiveTheme(theme, true);
      });
    });
  });
})();

/***/ })

},
/******/ __webpack_require__ => { // webpackRuntimeModules
/******/ var __webpack_exec__ = (moduleId) => (__webpack_require__(__webpack_require__.s = moduleId))
/******/ var __webpack_exports__ = (__webpack_exec__("./assets/js/colorScheme.js"));
/******/ }
]);
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiY29sb3JTY2hlbWUuanMiLCJtYXBwaW5ncyI6Ijs7Ozs7Ozs7QUFBQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBRUEsQ0FBQyxZQUFNO0VBQ0g7O0VBRUEsSUFBTUEsY0FBYyxHQUFHLFNBQWpCQSxjQUFpQjtJQUFBLE9BQU1DLFlBQVksQ0FBQ0MsT0FBYixDQUFxQixPQUFyQixDQUFOO0VBQUEsQ0FBdkI7O0VBQ0EsSUFBTUMsY0FBYyxHQUFHLFNBQWpCQSxjQUFpQixDQUFBQyxLQUFLO0lBQUEsT0FBSUgsWUFBWSxDQUFDSSxPQUFiLENBQXFCLE9BQXJCLEVBQThCRCxLQUE5QixDQUFKO0VBQUEsQ0FBNUI7O0VBRUEsSUFBTUUsaUJBQWlCLEdBQUcsU0FBcEJBLGlCQUFvQixHQUFNO0lBQzVCLElBQU1DLFdBQVcsR0FBR1AsY0FBYyxFQUFsQzs7SUFDQSxJQUFJTyxXQUFKLEVBQWlCO01BQ2IsT0FBT0EsV0FBUDtJQUNIOztJQUVELE9BQU8sTUFBUDtFQUNILENBUEQ7O0VBU0EsSUFBTUMsUUFBUSxHQUFHLFNBQVhBLFFBQVcsQ0FBQUosS0FBSyxFQUFJO0lBQ3RCLElBQUlBLEtBQUssS0FBSyxNQUFWLElBQW9CSyxNQUFNLENBQUNDLFVBQVAsQ0FBa0IsOEJBQWxCLEVBQWtEQyxPQUExRSxFQUFtRjtNQUMvRUMsUUFBUSxDQUFDQyxlQUFULENBQXlCQyxZQUF6QixDQUFzQyxlQUF0QyxFQUF1RCxNQUF2RDtJQUNILENBRkQsTUFFTztNQUNIRixRQUFRLENBQUNDLGVBQVQsQ0FBeUJDLFlBQXpCLENBQXNDLGVBQXRDLEVBQXVEVixLQUF2RDtJQUNIO0VBQ0osQ0FORDs7RUFRQUksUUFBUSxDQUFDRixpQkFBaUIsRUFBbEIsQ0FBUjs7RUFFQSxJQUFNUyxlQUFlLEdBQUcsU0FBbEJBLGVBQWtCLENBQUNYLEtBQUQsRUFBMEI7SUFBQSxJQUFsQlksS0FBa0IsdUVBQVYsS0FBVTtJQUM5QyxJQUFNQyxhQUFhLEdBQUdMLFFBQVEsQ0FBQ00sYUFBVCxDQUF1QixXQUF2QixDQUF0Qjs7SUFFQSxJQUFJLENBQUNELGFBQUwsRUFBb0I7TUFDaEI7SUFDSDs7SUFFRCxJQUFNRSxXQUFXLEdBQUdQLFFBQVEsQ0FBQ00sYUFBVCxrQ0FBZ0RkLEtBQWhELFNBQXBCO0lBRUFRLFFBQVEsQ0FBQ1EsZ0JBQVQsQ0FBMEIsdUJBQTFCLEVBQW1EQyxPQUFuRCxDQUEyRCxVQUFBQyxPQUFPLEVBQUk7TUFDbEVBLE9BQU8sQ0FBQ0MsU0FBUixDQUFrQkMsTUFBbEIsQ0FBeUIsUUFBekI7TUFDQUYsT0FBTyxDQUFDUixZQUFSLENBQXFCLGNBQXJCLEVBQXFDLE9BQXJDO0lBQ0gsQ0FIRDtJQUtBSyxXQUFXLENBQUNJLFNBQVosQ0FBc0JFLEdBQXRCLENBQTBCLFFBQTFCO0lBQ0FOLFdBQVcsQ0FBQ0wsWUFBWixDQUF5QixjQUF6QixFQUF5QyxNQUF6Qzs7SUFFQSxJQUFJRSxLQUFKLEVBQVc7TUFDUEMsYUFBYSxDQUFDRCxLQUFkO0lBQ0g7RUFDSixDQXBCRDs7RUFzQkFQLE1BQU0sQ0FBQ0MsVUFBUCxDQUFrQiw4QkFBbEIsRUFBa0RnQixnQkFBbEQsQ0FBbUUsUUFBbkUsRUFBNkUsWUFBTTtJQUMvRSxJQUFNbkIsV0FBVyxHQUFHUCxjQUFjLEVBQWxDOztJQUNBLElBQUlPLFdBQVcsS0FBSyxPQUFoQixJQUEyQkEsV0FBVyxLQUFLLE1BQS9DLEVBQXVEO01BQ25EQyxRQUFRLENBQUNGLGlCQUFpQixFQUFsQixDQUFSO0lBQ0g7RUFDSixDQUxEO0VBT0FHLE1BQU0sQ0FBQ2lCLGdCQUFQLENBQXdCLGtCQUF4QixFQUE0QyxZQUFNO0lBQzlDWCxlQUFlLENBQUNULGlCQUFpQixFQUFsQixDQUFmO0lBRUFNLFFBQVEsQ0FBQ1EsZ0JBQVQsQ0FBMEIsdUJBQTFCLEVBQ0tDLE9BREwsQ0FDYSxVQUFBTSxNQUFNLEVBQUk7TUFDZkEsTUFBTSxDQUFDRCxnQkFBUCxDQUF3QixPQUF4QixFQUFpQyxZQUFNO1FBQ25DLElBQU10QixLQUFLLEdBQUd1QixNQUFNLENBQUNDLFlBQVAsQ0FBb0IscUJBQXBCLENBQWQ7UUFDQXpCLGNBQWMsQ0FBQ0MsS0FBRCxDQUFkO1FBQ0FJLFFBQVEsQ0FBQ0osS0FBRCxDQUFSO1FBQ0FXLGVBQWUsQ0FBQ1gsS0FBRCxFQUFRLElBQVIsQ0FBZjtNQUNILENBTEQ7SUFNSCxDQVJMO0VBU0gsQ0FaRDtBQWFILENBbkVEIiwic291cmNlcyI6WyJ3ZWJwYWNrOi8vLy4vYXNzZXRzL2pzL2NvbG9yU2NoZW1lLmpzIl0sInNvdXJjZXNDb250ZW50IjpbIi8qIVxuICogQ29sb3IgbW9kZSB0b2dnbGVyIGZvciBCb290c3RyYXAncyBkb2NzIChodHRwczovL2dldGJvb3RzdHJhcC5jb20vKVxuICogQ29weXJpZ2h0IDIwMTEtMjAyMyBUaGUgQm9vdHN0cmFwIEF1dGhvcnNcbiAqIExpY2Vuc2VkIHVuZGVyIHRoZSBDcmVhdGl2ZSBDb21tb25zIEF0dHJpYnV0aW9uIDMuMCBVbnBvcnRlZCBMaWNlbnNlLlxuICovXG5cbigoKSA9PiB7XG4gICAgJ3VzZSBzdHJpY3QnXG5cbiAgICBjb25zdCBnZXRTdG9yZWRUaGVtZSA9ICgpID0+IGxvY2FsU3RvcmFnZS5nZXRJdGVtKCd0aGVtZScpXG4gICAgY29uc3Qgc2V0U3RvcmVkVGhlbWUgPSB0aGVtZSA9PiBsb2NhbFN0b3JhZ2Uuc2V0SXRlbSgndGhlbWUnLCB0aGVtZSlcblxuICAgIGNvbnN0IGdldFByZWZlcnJlZFRoZW1lID0gKCkgPT4ge1xuICAgICAgICBjb25zdCBzdG9yZWRUaGVtZSA9IGdldFN0b3JlZFRoZW1lKClcbiAgICAgICAgaWYgKHN0b3JlZFRoZW1lKSB7XG4gICAgICAgICAgICByZXR1cm4gc3RvcmVkVGhlbWVcbiAgICAgICAgfVxuXG4gICAgICAgIHJldHVybiAnYXV0byc7XG4gICAgfVxuXG4gICAgY29uc3Qgc2V0VGhlbWUgPSB0aGVtZSA9PiB7XG4gICAgICAgIGlmICh0aGVtZSA9PT0gJ2F1dG8nICYmIHdpbmRvdy5tYXRjaE1lZGlhKCcocHJlZmVycy1jb2xvci1zY2hlbWU6IGRhcmspJykubWF0Y2hlcykge1xuICAgICAgICAgICAgZG9jdW1lbnQuZG9jdW1lbnRFbGVtZW50LnNldEF0dHJpYnV0ZSgnZGF0YS1icy10aGVtZScsICdkYXJrJylcbiAgICAgICAgfSBlbHNlIHtcbiAgICAgICAgICAgIGRvY3VtZW50LmRvY3VtZW50RWxlbWVudC5zZXRBdHRyaWJ1dGUoJ2RhdGEtYnMtdGhlbWUnLCB0aGVtZSlcbiAgICAgICAgfVxuICAgIH1cblxuICAgIHNldFRoZW1lKGdldFByZWZlcnJlZFRoZW1lKCkpXG5cbiAgICBjb25zdCBzaG93QWN0aXZlVGhlbWUgPSAodGhlbWUsIGZvY3VzID0gZmFsc2UpID0+IHtcbiAgICAgICAgY29uc3QgdGhlbWVTd2l0Y2hlciA9IGRvY3VtZW50LnF1ZXJ5U2VsZWN0b3IoJyNiZC10aGVtZScpXG5cbiAgICAgICAgaWYgKCF0aGVtZVN3aXRjaGVyKSB7XG4gICAgICAgICAgICByZXR1cm5cbiAgICAgICAgfVxuXG4gICAgICAgIGNvbnN0IGJ0blRvQWN0aXZlID0gZG9jdW1lbnQucXVlcnlTZWxlY3RvcihgW2RhdGEtYnMtdGhlbWUtdmFsdWU9XCIke3RoZW1lfVwiXWApXG5cbiAgICAgICAgZG9jdW1lbnQucXVlcnlTZWxlY3RvckFsbCgnW2RhdGEtYnMtdGhlbWUtdmFsdWVdJykuZm9yRWFjaChlbGVtZW50ID0+IHtcbiAgICAgICAgICAgIGVsZW1lbnQuY2xhc3NMaXN0LnJlbW92ZSgnYWN0aXZlJylcbiAgICAgICAgICAgIGVsZW1lbnQuc2V0QXR0cmlidXRlKCdhcmlhLXByZXNzZWQnLCAnZmFsc2UnKVxuICAgICAgICB9KVxuXG4gICAgICAgIGJ0blRvQWN0aXZlLmNsYXNzTGlzdC5hZGQoJ2FjdGl2ZScpXG4gICAgICAgIGJ0blRvQWN0aXZlLnNldEF0dHJpYnV0ZSgnYXJpYS1wcmVzc2VkJywgJ3RydWUnKVxuXG4gICAgICAgIGlmIChmb2N1cykge1xuICAgICAgICAgICAgdGhlbWVTd2l0Y2hlci5mb2N1cygpXG4gICAgICAgIH1cbiAgICB9XG5cbiAgICB3aW5kb3cubWF0Y2hNZWRpYSgnKHByZWZlcnMtY29sb3Itc2NoZW1lOiBkYXJrKScpLmFkZEV2ZW50TGlzdGVuZXIoJ2NoYW5nZScsICgpID0+IHtcbiAgICAgICAgY29uc3Qgc3RvcmVkVGhlbWUgPSBnZXRTdG9yZWRUaGVtZSgpXG4gICAgICAgIGlmIChzdG9yZWRUaGVtZSAhPT0gJ2xpZ2h0JyAmJiBzdG9yZWRUaGVtZSAhPT0gJ2RhcmsnKSB7XG4gICAgICAgICAgICBzZXRUaGVtZShnZXRQcmVmZXJyZWRUaGVtZSgpKVxuICAgICAgICB9XG4gICAgfSlcblxuICAgIHdpbmRvdy5hZGRFdmVudExpc3RlbmVyKCdET01Db250ZW50TG9hZGVkJywgKCkgPT4ge1xuICAgICAgICBzaG93QWN0aXZlVGhlbWUoZ2V0UHJlZmVycmVkVGhlbWUoKSlcblxuICAgICAgICBkb2N1bWVudC5xdWVyeVNlbGVjdG9yQWxsKCdbZGF0YS1icy10aGVtZS12YWx1ZV0nKVxuICAgICAgICAgICAgLmZvckVhY2godG9nZ2xlID0+IHtcbiAgICAgICAgICAgICAgICB0b2dnbGUuYWRkRXZlbnRMaXN0ZW5lcignY2xpY2snLCAoKSA9PiB7XG4gICAgICAgICAgICAgICAgICAgIGNvbnN0IHRoZW1lID0gdG9nZ2xlLmdldEF0dHJpYnV0ZSgnZGF0YS1icy10aGVtZS12YWx1ZScpXG4gICAgICAgICAgICAgICAgICAgIHNldFN0b3JlZFRoZW1lKHRoZW1lKVxuICAgICAgICAgICAgICAgICAgICBzZXRUaGVtZSh0aGVtZSlcbiAgICAgICAgICAgICAgICAgICAgc2hvd0FjdGl2ZVRoZW1lKHRoZW1lLCB0cnVlKVxuICAgICAgICAgICAgICAgIH0pXG4gICAgICAgICAgICB9KVxuICAgIH0pXG59KSgpIl0sIm5hbWVzIjpbImdldFN0b3JlZFRoZW1lIiwibG9jYWxTdG9yYWdlIiwiZ2V0SXRlbSIsInNldFN0b3JlZFRoZW1lIiwidGhlbWUiLCJzZXRJdGVtIiwiZ2V0UHJlZmVycmVkVGhlbWUiLCJzdG9yZWRUaGVtZSIsInNldFRoZW1lIiwid2luZG93IiwibWF0Y2hNZWRpYSIsIm1hdGNoZXMiLCJkb2N1bWVudCIsImRvY3VtZW50RWxlbWVudCIsInNldEF0dHJpYnV0ZSIsInNob3dBY3RpdmVUaGVtZSIsImZvY3VzIiwidGhlbWVTd2l0Y2hlciIsInF1ZXJ5U2VsZWN0b3IiLCJidG5Ub0FjdGl2ZSIsInF1ZXJ5U2VsZWN0b3JBbGwiLCJmb3JFYWNoIiwiZWxlbWVudCIsImNsYXNzTGlzdCIsInJlbW92ZSIsImFkZCIsImFkZEV2ZW50TGlzdGVuZXIiLCJ0b2dnbGUiLCJnZXRBdHRyaWJ1dGUiXSwic291cmNlUm9vdCI6IiJ9