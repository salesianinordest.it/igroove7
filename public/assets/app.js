(self["webpackChunk"] = self["webpackChunk"] || []).push([["app"],{

/***/ "./assets/images sync recursive \\.(png%7Cjpg%7Cjpeg%7Cgif%7Cico%7Csvg%7Cwebp)$":
/*!****************************************************************************!*\
  !*** ./assets/images/ sync \.(png%7Cjpg%7Cjpeg%7Cgif%7Cico%7Csvg%7Cwebp)$ ***!
  \****************************************************************************/
/***/ ((module, __unused_webpack_exports, __webpack_require__) => {

var map = {
	"./Fortinet_logo.svg": "./assets/images/Fortinet_logo.svg",
	"./Mikrotik_logo.svg": "./assets/images/Mikrotik_logo.svg",
	"./Watchguard_logo.svg": "./assets/images/Watchguard_logo.svg",
	"./favicon/android-chrome-192x192.png": "./assets/images/favicon/android-chrome-192x192.png",
	"./favicon/android-chrome-512x512.png": "./assets/images/favicon/android-chrome-512x512.png",
	"./favicon/apple-touch-icon.png": "./assets/images/favicon/apple-touch-icon.png",
	"./favicon/favicon-16x16.png": "./assets/images/favicon/favicon-16x16.png",
	"./favicon/favicon-32x32.png": "./assets/images/favicon/favicon-32x32.png",
	"./favicon/favicon.ico": "./assets/images/favicon/favicon.ico"
};


function webpackContext(req) {
	var id = webpackContextResolve(req);
	return __webpack_require__(id);
}
function webpackContextResolve(req) {
	if(!__webpack_require__.o(map, req)) {
		var e = new Error("Cannot find module '" + req + "'");
		e.code = 'MODULE_NOT_FOUND';
		throw e;
	}
	return map[req];
}
webpackContext.keys = function webpackContextKeys() {
	return Object.keys(map);
};
webpackContext.resolve = webpackContextResolve;
module.exports = webpackContext;
webpackContext.id = "./assets/images sync recursive \\.(png%7Cjpg%7Cjpeg%7Cgif%7Cico%7Csvg%7Cwebp)$";

/***/ }),

/***/ "./assets/js/app.js":
/*!**************************!*\
  !*** ./assets/js/app.js ***!
  \**************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _fortawesome_fontawesome_free_js_all__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @fortawesome/fontawesome-free/js/all */ "./node_modules/@fortawesome/fontawesome-free/js/all.js");
/* harmony import */ var _fortawesome_fontawesome_free_js_all__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(_fortawesome_fontawesome_free_js_all__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var bootstrap__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! bootstrap */ "./node_modules/bootstrap/dist/js/bootstrap.esm.js");
/* harmony import */ var chart_js_auto__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! chart.js/auto */ "./node_modules/chart.js/auto/auto.esm.js");
/* harmony import */ var chartjs_plugin_datalabels__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! chartjs-plugin-datalabels */ "./node_modules/chartjs-plugin-datalabels/dist/chartjs-plugin-datalabels.esm.js");
/* harmony import */ var _trevoreyre_autocomplete_js__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @trevoreyre/autocomplete-js */ "./node_modules/@trevoreyre/autocomplete-js/dist/autocomplete.esm.js");
/* harmony import */ var _pugx_filter_bundle_js_filter__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! @pugx/filter-bundle/js/filter */ "./node_modules/@pugx/filter-bundle/js/filter.js");
/* harmony import */ var _pugx_filter_bundle_js_filter__WEBPACK_IMPORTED_MODULE_5___default = /*#__PURE__*/__webpack_require__.n(_pugx_filter_bundle_js_filter__WEBPACK_IMPORTED_MODULE_5__);
/* harmony import */ var vue__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! vue */ "./node_modules/vue/dist/vue.esm-bundler.js");
/* harmony import */ var _components_SyncTable_vue__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! ./components/SyncTable.vue */ "./assets/js/components/SyncTable.vue");
/* harmony import */ var axios__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! axios */ "./node_modules/axios/lib/axios.js");
/* provided dependency */ var jQuery = __webpack_require__(/*! jquery */ "./node_modules/jquery/dist/jquery.js");
__webpack_require__(/*! jquery */ "./node_modules/jquery/dist/jquery.js");

var imagesContext = __webpack_require__("./assets/images sync recursive \\.(png%7Cjpg%7Cjpeg%7Cgif%7Cico%7Csvg%7Cwebp)$");

imagesContext.keys().forEach(imagesContext);


var $ = __webpack_require__(/*! jquery */ "./node_modules/jquery/dist/jquery.js");

__webpack_require__.g.$ = __webpack_require__.g.jQuery = $;

__webpack_require__(/*! jquery-ui */ "./node_modules/jquery-ui/ui/widget.js");

__webpack_require__(/*! jquery-ui/ui/widgets/sortable */ "./node_modules/jquery-ui/ui/widgets/sortable.js");

__webpack_require__(/*! ../scss/bootstrap-custom.scss */ "./assets/scss/bootstrap-custom.scss");

window.bootstrap = __webpack_require__(/*! bootstrap */ "./node_modules/bootstrap/dist/js/bootstrap.esm.js");


__webpack_require__(/*! floatthead/dist/jquery.floatThead */ "./node_modules/floatthead/dist/jquery.floatThead.js");

__webpack_require__(/*! select2 */ "./node_modules/select2/dist/js/select2.js");


__webpack_require__.g.Chart = chart_js_auto__WEBPACK_IMPORTED_MODULE_2__["default"];

chart_js_auto__WEBPACK_IMPORTED_MODULE_2__["default"].register(chartjs_plugin_datalabels__WEBPACK_IMPORTED_MODULE_3__["default"]);

var palette = __webpack_require__(/*! google-palette */ "./node_modules/google-palette/palette.js");

__webpack_require__.g.palette = palette;

__webpack_require__.g.Autocomplete = _trevoreyre_autocomplete_js__WEBPACK_IMPORTED_MODULE_4__["default"];



__webpack_require__.g.VueCreateApp = vue__WEBPACK_IMPORTED_MODULE_6__.createApp;
__webpack_require__.g.SyncTable = _components_SyncTable_vue__WEBPACK_IMPORTED_MODULE_7__["default"];
globalThis.__VUE_PROD_DEVTOOLS__ =  true || 0;

__webpack_require__.g.axios = axios__WEBPACK_IMPORTED_MODULE_8__["default"];
$(document).ready(function () {
  'use strict';

  var _this = this;

  if (jQuery().pugxFilter) {
    $('#filter').pugxFilter();
  }
  /** select2 no focus workaround **/


  $(document).on('select2:open', function () {
    var allFound = document.querySelectorAll('.select2-container--open .select2-search__field');
    $(_this).one('mouseup keyup', function () {
      setTimeout(function () {
        allFound[allFound.length - 1].focus();
      }, 0);
    });
  });
  $(document).ajaxError(function (event, jqXHR) {
    if (jqXHR.status === 403 && jqXHR.responseText === "Authentication timeout") {
      window.location.reload();
    }
  });
  $('.table-responsive').on('shown.bs.dropdown', function (e) {
    var $tableCont = $(this),
        $menuBtn = $(e.target),
        $menu = $menuBtn.parent().find(".dropdown-menu"),
        menuBtnTop = $menuBtn.offset().top - $tableCont.offset().top,
        menuBtnBottom = menuBtnTop + $menuBtn.outerHeight(true),
        lowerMenuBottom = menuBtnBottom + $menu.outerHeight(true);
    if (menuBtnTop < $menu.outerHeight(true) && lowerMenuBottom > $tableCont.height()) $tableCont.css("padding-bottom", $menu.outerHeight(true));
  }).on('hide.bs.dropdown', function () {
    $(this).css("padding-bottom", 0);
  });
});

/***/ }),

/***/ "./node_modules/babel-loader/lib/index.js??clonedRuleSet-1.use[0]!./node_modules/vue-loader/dist/index.js??ruleSet[0].use[0]!./assets/js/components/SyncTable.vue?vue&type=script&lang=js":
/*!************************************************************************************************************************************************************************************************!*\
  !*** ./node_modules/babel-loader/lib/index.js??clonedRuleSet-1.use[0]!./node_modules/vue-loader/dist/index.js??ruleSet[0].use[0]!./assets/js/components/SyncTable.vue?vue&type=script&lang=js ***!
  \************************************************************************************************************************************************************************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "default": () => (__WEBPACK_DEFAULT_EXPORT__)
/* harmony export */ });
/* harmony import */ var _SyncTableTr_vue__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./SyncTableTr.vue */ "./assets/js/components/SyncTableTr.vue");
/* harmony import */ var axios__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! axios */ "./node_modules/axios/lib/axios.js");
function _typeof(obj) { "@babel/helpers - typeof"; return _typeof = "function" == typeof Symbol && "symbol" == typeof Symbol.iterator ? function (obj) { return typeof obj; } : function (obj) { return obj && "function" == typeof Symbol && obj.constructor === Symbol && obj !== Symbol.prototype ? "symbol" : typeof obj; }, _typeof(obj); }

function _createForOfIteratorHelper(o, allowArrayLike) { var it = typeof Symbol !== "undefined" && o[Symbol.iterator] || o["@@iterator"]; if (!it) { if (Array.isArray(o) || (it = _unsupportedIterableToArray(o)) || allowArrayLike && o && typeof o.length === "number") { if (it) o = it; var i = 0; var F = function F() {}; return { s: F, n: function n() { if (i >= o.length) return { done: true }; return { done: false, value: o[i++] }; }, e: function e(_e) { throw _e; }, f: F }; } throw new TypeError("Invalid attempt to iterate non-iterable instance.\nIn order to be iterable, non-array objects must have a [Symbol.iterator]() method."); } var normalCompletion = true, didErr = false, err; return { s: function s() { it = it.call(o); }, n: function n() { var step = it.next(); normalCompletion = step.done; return step; }, e: function e(_e2) { didErr = true; err = _e2; }, f: function f() { try { if (!normalCompletion && it["return"] != null) it["return"](); } finally { if (didErr) throw err; } } }; }

function _unsupportedIterableToArray(o, minLen) { if (!o) return; if (typeof o === "string") return _arrayLikeToArray(o, minLen); var n = Object.prototype.toString.call(o).slice(8, -1); if (n === "Object" && o.constructor) n = o.constructor.name; if (n === "Map" || n === "Set") return Array.from(o); if (n === "Arguments" || /^(?:Ui|I)nt(?:8|16|32)(?:Clamped)?Array$/.test(n)) return _arrayLikeToArray(o, minLen); }

function _arrayLikeToArray(arr, len) { if (len == null || len > arr.length) len = arr.length; for (var i = 0, arr2 = new Array(len); i < len; i++) { arr2[i] = arr[i]; } return arr2; }

function ownKeys(object, enumerableOnly) { var keys = Object.keys(object); if (Object.getOwnPropertySymbols) { var symbols = Object.getOwnPropertySymbols(object); enumerableOnly && (symbols = symbols.filter(function (sym) { return Object.getOwnPropertyDescriptor(object, sym).enumerable; })), keys.push.apply(keys, symbols); } return keys; }

function _objectSpread(target) { for (var i = 1; i < arguments.length; i++) { var source = null != arguments[i] ? arguments[i] : {}; i % 2 ? ownKeys(Object(source), !0).forEach(function (key) { _defineProperty(target, key, source[key]); }) : Object.getOwnPropertyDescriptors ? Object.defineProperties(target, Object.getOwnPropertyDescriptors(source)) : ownKeys(Object(source)).forEach(function (key) { Object.defineProperty(target, key, Object.getOwnPropertyDescriptor(source, key)); }); } return target; }

function _defineProperty(obj, key, value) { if (key in obj) { Object.defineProperty(obj, key, { value: value, enumerable: true, configurable: true, writable: true }); } else { obj[key] = value; } return obj; }



/* harmony default export */ const __WEBPACK_DEFAULT_EXPORT__ = ({
  name: "SyncTable",
  props: {
    entities: {
      type: Object
    },
    entityType: {
      type: String
    },
    defaultAction: {
      type: String
    },
    defaultDeleteOptions: {
      type: Object
    },
    url: {
      type: String
    },
    deleteUrl: {
      type: String,
      "default": null
    },
    mainActionLabel: {
      type: String,
      "default": 'Esegui'
    },
    showMainAction: {
      type: Boolean,
      "default": true
    },
    showDeleteAction: {
      type: Boolean,
      "default": false
    },
    extraColumns: {
      type: Array,
      "default": []
    },
    initialStateLabel: {
      type: String,
      "default": '---'
    }
  },
  data: function data() {
    return {
      checked: [],
      allChecked: false,
      deleteOptions: _objectSpread({}, this.defaultDeleteOptions),
      executionQueue: [],
      queueWorking: false,
      entitiesToDelete: []
    };
  },
  methods: {
    checkboxClicked: function checkboxClicked(id) {
      var pos = this.checked.indexOf(id);

      if (pos > -1) {
        this.checked.splice(pos, 1);
      } else {
        this.checked.push(id);
      }
    },
    toggleAllCheckbox: function toggleAllCheckbox() {
      this.checked = [];

      if (!this.allChecked) {
        var _iterator = _createForOfIteratorHelper(this.$refs.children),
            _step;

        try {
          for (_iterator.s(); !(_step = _iterator.n()).done;) {
            var child = _step.value;

            if (!child.disabled) {
              this.checked.push(child.id);
            }
          }
        } catch (err) {
          _iterator.e(err);
        } finally {
          _iterator.f();
        }
      }

      this.allChecked = !this.allChecked;
    },
    callAction: function callAction(payload) {
      if (_typeof(payload) !== 'object' || payload.entityId === undefined || payload.entityType === undefined || payload.entity === undefined || payload.action === undefined) {
        console.warn("Invalid call action payload", payload);
        return;
      }

      payload.entity.state = 2;
      var url = this.url;

      if (payload.action === "delete") {
        url = this.deleteUrl;
      }

      if (url === "") {
        return;
      }

      if (typeof payload.entity.data.providerId !== "undefined") {
        url = url.replace("*providerId*", payload.entity.data.providerId);
      }

      var $this = this;
      var data = {
        entityData: payload.entity.data,
        entityId: payload.entityId,
        entityType: payload.entityType
      };

      if (payload.action === "delete") {
        if (payload.deleteOptions === undefined) {
          console.warn("Invalid call action payload options", payload);
          return;
        }

        data['options'] = payload.deleteOptions;
      }

      axios__WEBPACK_IMPORTED_MODULE_1__["default"].post(url, data, {
        headers: {
          'Content-Type': 'multipart/form-data'
        }
      }).then(function (response) {
        if (_typeof(response.data) !== "object" || response.data.success !== true) {
          throw _typeof(response.data) === 'object' && response.data.error !== undefined ? response.data.error : null;
        }

        payload.entity.state = 3;

        if (response.data.newEntityId !== undefined) {
          this.$emit('newEntityInserted', _objectSpread(_objectSpread({}, payload), {}, {
            newEntityId: response.data.newEntityId
          }));
        }

        $this.queueWorking = false;
        $this.tickQueue();
      })["catch"](function (err) {
        payload.entity.error = typeof err === "string" ? err : "Impossibile elaborare la richiesta";
        payload.entity.state = 0;
        $this.queueWorking = false;
        $this.tickQueue();
      });
    },
    queueSelectedForDefaultAction: function queueSelectedForDefaultAction() {
      for (var entityIdIdx in this.checked) {
        this.queueAction(this.checked[entityIdIdx]);
      }

      this.checked = [];
    },
    deleteEntity: function deleteEntity(entityId) {
      if (this.entityType === 'students' || this.entityType === 'teachers' || this.entityType === 'groups') {
        this.entitiesToDelete = [entityId];
        return;
      }

      this.queueAction(entityId, 'delete');
    },
    deleteSelected: function deleteSelected() {
      if (this.entityType === 'students' || this.entityType === 'teachers' || this.entityType === 'groups') {
        this.entitiesToDelete = this.checked;
        return;
      }

      var _iterator2 = _createForOfIteratorHelper(this.checked),
          _step2;

      try {
        for (_iterator2.s(); !(_step2 = _iterator2.n()).done;) {
          var entityId = _step2.value;
          this.queueAction(entityId, 'delete');
        }
      } catch (err) {
        _iterator2.e(err);
      } finally {
        _iterator2.f();
      }

      this.checked = [];
    },
    queueDeleteActions: function queueDeleteActions() {
      var _iterator3 = _createForOfIteratorHelper(this.entitiesToDelete),
          _step3;

      try {
        for (_iterator3.s(); !(_step3 = _iterator3.n()).done;) {
          var entityId = _step3.value;
          this.queueAction(entityId, 'delete');
        }
      } catch (err) {
        _iterator3.e(err);
      } finally {
        _iterator3.f();
      }

      this.entitiesToDelete = [];
      this.checked = [];
    },
    queueAction: function queueAction(entityId) {
      var action = arguments.length > 1 && arguments[1] !== undefined ? arguments[1] : null;

      if (this.entities[entityId].state > 0) {
        return;
      }

      this.entities[entityId].state = 1;
      this.entities[entityId].error = null;
      this.executionQueue.push({
        action: action !== null ? action : this.defaultAction,
        entityId: entityId,
        entityType: this.entityType,
        entity: this.entities[entityId],
        deleteOptions: this.deleteOptions
      });
      this.tickQueue();
    },
    tickQueue: function tickQueue() {
      if (this.queueWorking || this.executionQueue.length < 1) {
        return;
      }

      var $this = this;
      $this.queueWorking = true;
      setTimeout(function () {
        $this.callAction($this.executionQueue.shift());
      }, 0);
    },
    setDeleteOption: function setDeleteOption(key, value) {
      this.deleteOptions[key] = value;
    }
  },
  components: {
    'synctable-tr': _SyncTableTr_vue__WEBPACK_IMPORTED_MODULE_0__["default"]
  }
});

/***/ }),

/***/ "./node_modules/babel-loader/lib/index.js??clonedRuleSet-1.use[0]!./node_modules/vue-loader/dist/index.js??ruleSet[0].use[0]!./assets/js/components/SyncTableTr.vue?vue&type=script&lang=js":
/*!**************************************************************************************************************************************************************************************************!*\
  !*** ./node_modules/babel-loader/lib/index.js??clonedRuleSet-1.use[0]!./node_modules/vue-loader/dist/index.js??ruleSet[0].use[0]!./assets/js/components/SyncTableTr.vue?vue&type=script&lang=js ***!
  \**************************************************************************************************************************************************************************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "default": () => (__WEBPACK_DEFAULT_EXPORT__)
/* harmony export */ });
function _typeof(obj) { "@babel/helpers - typeof"; return _typeof = "function" == typeof Symbol && "symbol" == typeof Symbol.iterator ? function (obj) { return typeof obj; } : function (obj) { return obj && "function" == typeof Symbol && obj.constructor === Symbol && obj !== Symbol.prototype ? "symbol" : typeof obj; }, _typeof(obj); }

/* harmony default export */ const __WEBPACK_DEFAULT_EXPORT__ = ({
  name: "SyncTableTr",
  props: ['entity', 'entityType', 'checked', 'id', 'extraColumns', 'showDeleteBtn', 'showMainActionBtn', 'mainActionBtnLabel', 'initialStateLabel'],
  computed: {
    stateMsg: function stateMsg() {
      if (this.entity.error !== null) {
        return "Errore: " + this.entity.error;
      }

      if (this.entityType === "students" && this.entity.data.internalGroupId === null) {
        return "Disabilitato per classe non importata";
      }

      if (this.entityType === "groups" && this.entity.data.internalSectorId === null) {
        return "Disabilitato per settore non importato";
      }

      switch (this.entity.state) {
        case 1:
          return "In Coda";

        case 2:
          return "Elaborazione";

        case 3:
          return "Completato";

        default:
          return this.initialStateLabel;
      }
    },
    disabled: function disabled() {
      if (this.entity.state > 0) {
        return true;
      }

      if (this.entityType === "students" && _typeof(this.entity.data) === "object" && this.entity.data.internalGroupId === null) {
        return true;
      }

      if (this.entityType === "groups" && _typeof(this.entity.data) === "object" && this.entity.data.internalSectorId === null) {
        return true;
      }

      return false;
    }
  }
});

/***/ }),

/***/ "./node_modules/babel-loader/lib/index.js??clonedRuleSet-1.use[0]!./node_modules/vue-loader/dist/templateLoader.js??ruleSet[1].rules[2]!./node_modules/vue-loader/dist/index.js??ruleSet[0].use[0]!./assets/js/components/SyncTable.vue?vue&type=template&id=7a5b30bc":
/*!****************************************************************************************************************************************************************************************************************************************************************************!*\
  !*** ./node_modules/babel-loader/lib/index.js??clonedRuleSet-1.use[0]!./node_modules/vue-loader/dist/templateLoader.js??ruleSet[1].rules[2]!./node_modules/vue-loader/dist/index.js??ruleSet[0].use[0]!./assets/js/components/SyncTable.vue?vue&type=template&id=7a5b30bc ***!
  \****************************************************************************************************************************************************************************************************************************************************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   render: () => (/* binding */ render)
/* harmony export */ });
/* harmony import */ var vue__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! vue */ "./node_modules/vue/dist/vue.esm-bundler.js");

var _hoisted_1 = {
  key: 0
};
var _hoisted_2 = {
  "class": "table-responsive"
};
var _hoisted_3 = {
  "class": "table table-striped"
};
var _hoisted_4 = ["checked"];

var _hoisted_5 = /*#__PURE__*/(0,vue__WEBPACK_IMPORTED_MODULE_0__.createElementVNode)("th", null, "Nome", -1
/* HOISTED */
);

var _hoisted_6 = /*#__PURE__*/(0,vue__WEBPACK_IMPORTED_MODULE_0__.createElementVNode)("th", null, "Stato", -1
/* HOISTED */
);

var _hoisted_7 = /*#__PURE__*/(0,vue__WEBPACK_IMPORTED_MODULE_0__.createElementVNode)("th", null, "Azione", -1
/* HOISTED */
);

var _hoisted_8 = ["disabled"];
var _hoisted_9 = ["disabled"];
var _hoisted_10 = {
  "class": "modal-dialog"
};
var _hoisted_11 = {
  "class": "modal-content"
};
var _hoisted_12 = {
  "class": "modal-header"
};

var _hoisted_13 = /*#__PURE__*/(0,vue__WEBPACK_IMPORTED_MODULE_0__.createElementVNode)("h5", {
  "class": "modal-title"
}, "Opzioni per l'eliminazione", -1
/* HOISTED */
);

var _hoisted_14 = {
  "class": "modal-body"
};
var _hoisted_15 = {
  "class": "row row-cols-lg-auto g-3 align-items-center"
};
var _hoisted_16 = {
  "class": "form"
};
var _hoisted_17 = {
  key: 0,
  "class": "form-check mb-0"
};
var _hoisted_18 = ["id", "checked"];
var _hoisted_19 = ["for"];
var _hoisted_20 = {
  key: 1,
  "class": "form-check mb-0 pl-0"
};
var _hoisted_21 = ["id", "checked"];
var _hoisted_22 = ["for"];
var _hoisted_23 = {
  key: 2,
  "class": "d-flex gap-2 align-items-center"
};
var _hoisted_24 = ["for"];
var _hoisted_25 = ["id", "value"];

var _hoisted_26 = /*#__PURE__*/(0,vue__WEBPACK_IMPORTED_MODULE_0__.createElementVNode)("option", {
  value: "0"
}, "No", -1
/* HOISTED */
);

var _hoisted_27 = /*#__PURE__*/(0,vue__WEBPACK_IMPORTED_MODULE_0__.createElementVNode)("option", {
  value: "1"
}, "Si", -1
/* HOISTED */
);

var _hoisted_28 = /*#__PURE__*/(0,vue__WEBPACK_IMPORTED_MODULE_0__.createElementVNode)("option", {
  value: "2"
}, "Sposta in OU", -1
/* HOISTED */
);

var _hoisted_29 = [_hoisted_26, _hoisted_27, _hoisted_28];
var _hoisted_30 = {
  key: 3,
  "class": "d-flex gap-2 align-items-center"
};
var _hoisted_31 = ["for"];
var _hoisted_32 = ["id", "value"];
var _hoisted_33 = {
  "class": "modal-footer"
};
function render(_ctx, _cache, $props, $setup, $data, $options) {
  var _component_synctable_tr = (0,vue__WEBPACK_IMPORTED_MODULE_0__.resolveComponent)("synctable-tr");

  return $props.entities !== undefined && Object.keys($props.entities).length > 0 ? ((0,vue__WEBPACK_IMPORTED_MODULE_0__.openBlock)(), (0,vue__WEBPACK_IMPORTED_MODULE_0__.createElementBlock)("div", _hoisted_1, [(0,vue__WEBPACK_IMPORTED_MODULE_0__.createElementVNode)("div", _hoisted_2, [(0,vue__WEBPACK_IMPORTED_MODULE_0__.createElementVNode)("table", _hoisted_3, [(0,vue__WEBPACK_IMPORTED_MODULE_0__.createElementVNode)("tr", null, [(0,vue__WEBPACK_IMPORTED_MODULE_0__.createElementVNode)("th", null, [(0,vue__WEBPACK_IMPORTED_MODULE_0__.createElementVNode)("input", {
    type: "checkbox",
    checked: _ctx.allChecked,
    onInput: _cache[0] || (_cache[0] = function () {
      return $options.toggleAllCheckbox && $options.toggleAllCheckbox.apply($options, arguments);
    }),
    "class": "form-check-input"
  }, null, 40
  /* PROPS, HYDRATE_EVENTS */
  , _hoisted_4)]), _hoisted_5, ((0,vue__WEBPACK_IMPORTED_MODULE_0__.openBlock)(true), (0,vue__WEBPACK_IMPORTED_MODULE_0__.createElementBlock)(vue__WEBPACK_IMPORTED_MODULE_0__.Fragment, null, (0,vue__WEBPACK_IMPORTED_MODULE_0__.renderList)($props.extraColumns, function (column) {
    return (0,vue__WEBPACK_IMPORTED_MODULE_0__.openBlock)(), (0,vue__WEBPACK_IMPORTED_MODULE_0__.createElementBlock)("th", null, (0,vue__WEBPACK_IMPORTED_MODULE_0__.toDisplayString)(column.name), 1
    /* TEXT */
    );
  }), 256
  /* UNKEYED_FRAGMENT */
  )), _hoisted_6, _hoisted_7]), ((0,vue__WEBPACK_IMPORTED_MODULE_0__.openBlock)(true), (0,vue__WEBPACK_IMPORTED_MODULE_0__.createElementBlock)(vue__WEBPACK_IMPORTED_MODULE_0__.Fragment, null, (0,vue__WEBPACK_IMPORTED_MODULE_0__.renderList)($props.entities, function (entity, id) {
    return (0,vue__WEBPACK_IMPORTED_MODULE_0__.openBlock)(), (0,vue__WEBPACK_IMPORTED_MODULE_0__.createBlock)(_component_synctable_tr, {
      entity: entity,
      "entity-type": $props.entityType,
      key: id,
      id: String(id),
      checked: _ctx.checked.indexOf(String(id)) > -1,
      "extra-columns": $props.extraColumns,
      "show-main-action-btn": $props.showMainAction,
      "show-delete-btn": $props.showDeleteAction,
      "main-action-btn-label": $props.mainActionLabel,
      "initial-state-label": $props.initialStateLabel,
      onMainBtnClick: _cache[1] || (_cache[1] = function ($event) {
        return $options.queueAction($event);
      }),
      onDeleteBtnClick: _cache[2] || (_cache[2] = function ($event) {
        return $options.deleteEntity($event);
      }),
      onCheckboxclicked: $options.checkboxClicked,
      ref_for: true,
      ref: "children"
    }, null, 8
    /* PROPS */
    , ["entity", "entity-type", "id", "checked", "extra-columns", "show-main-action-btn", "show-delete-btn", "main-action-btn-label", "initial-state-label", "onCheckboxclicked"]);
  }), 128
  /* KEYED_FRAGMENT */
  ))])]), $props.showMainAction ? ((0,vue__WEBPACK_IMPORTED_MODULE_0__.openBlock)(), (0,vue__WEBPACK_IMPORTED_MODULE_0__.createElementBlock)("button", {
    key: 0,
    "class": "btn btn-primary",
    disabled: _ctx.checked.length < 1,
    onClick: _cache[3] || (_cache[3] = function () {
      return $options.queueSelectedForDefaultAction && $options.queueSelectedForDefaultAction.apply($options, arguments);
    })
  }, (0,vue__WEBPACK_IMPORTED_MODULE_0__.toDisplayString)($props.mainActionLabel) + " selezionati", 9
  /* TEXT, PROPS */
  , _hoisted_8)) : (0,vue__WEBPACK_IMPORTED_MODULE_0__.createCommentVNode)("v-if", true), $props.showDeleteAction ? ((0,vue__WEBPACK_IMPORTED_MODULE_0__.openBlock)(), (0,vue__WEBPACK_IMPORTED_MODULE_0__.createElementBlock)("button", {
    key: 1,
    "class": "btn btn-danger ms-2",
    disabled: _ctx.checked.length < 1,
    onClick: _cache[4] || (_cache[4] = function () {
      return $options.deleteSelected && $options.deleteSelected.apply($options, arguments);
    })
  }, "Elimina selezionati", 8
  /* PROPS */
  , _hoisted_9)) : (0,vue__WEBPACK_IMPORTED_MODULE_0__.createCommentVNode)("v-if", true), ((0,vue__WEBPACK_IMPORTED_MODULE_0__.openBlock)(), (0,vue__WEBPACK_IMPORTED_MODULE_0__.createBlock)(vue__WEBPACK_IMPORTED_MODULE_0__.Teleport, {
    to: "body"
  }, [(0,vue__WEBPACK_IMPORTED_MODULE_0__.createElementVNode)("div", {
    "class": (0,vue__WEBPACK_IMPORTED_MODULE_0__.normalizeClass)(["modal", {
      show: _ctx.entitiesToDelete.length > 0,
      'd-block': _ctx.entitiesToDelete.length > 0
    }]),
    tabindex: "-1"
  }, [(0,vue__WEBPACK_IMPORTED_MODULE_0__.createElementVNode)("div", _hoisted_10, [(0,vue__WEBPACK_IMPORTED_MODULE_0__.createElementVNode)("div", _hoisted_11, [(0,vue__WEBPACK_IMPORTED_MODULE_0__.createElementVNode)("div", _hoisted_12, [_hoisted_13, (0,vue__WEBPACK_IMPORTED_MODULE_0__.createElementVNode)("button", {
    type: "button",
    "class": "btn-close",
    "aria-label": "Close",
    onClick: _cache[5] || (_cache[5] = function ($event) {
      return _ctx.entitiesToDelete = [];
    })
  })]), (0,vue__WEBPACK_IMPORTED_MODULE_0__.createElementVNode)("div", _hoisted_14, [(0,vue__WEBPACK_IMPORTED_MODULE_0__.createElementVNode)("form", _hoisted_15, [(0,vue__WEBPACK_IMPORTED_MODULE_0__.createElementVNode)("form", _hoisted_16, [$props.entityType === 'students' || $props.entityType === 'teachers' || $props.entityType === 'groups' ? ((0,vue__WEBPACK_IMPORTED_MODULE_0__.openBlock)(), (0,vue__WEBPACK_IMPORTED_MODULE_0__.createElementBlock)("div", _hoisted_17, [(0,vue__WEBPACK_IMPORTED_MODULE_0__.createElementVNode)("input", {
    type: "checkbox",
    "class": "form-check-input",
    id: 'syncComp-deleteLdap' + $props.entityType,
    checked: _ctx.deleteOptions.deleteOnLdap,
    onInput: _cache[6] || (_cache[6] = function (event) {
      return $options.setDeleteOption('deleteOnLdap', event.target.checked);
    })
  }, null, 40
  /* PROPS, HYDRATE_EVENTS */
  , _hoisted_18), (0,vue__WEBPACK_IMPORTED_MODULE_0__.createElementVNode)("label", {
    "class": "form-check-label",
    "for": 'syncComp-deleteLdap' + $props.entityType
  }, "Elimina da Ldap", 8
  /* PROPS */
  , _hoisted_19)])) : (0,vue__WEBPACK_IMPORTED_MODULE_0__.createCommentVNode)("v-if", true), $props.entityType === 'groups' ? ((0,vue__WEBPACK_IMPORTED_MODULE_0__.openBlock)(), (0,vue__WEBPACK_IMPORTED_MODULE_0__.createElementBlock)("div", _hoisted_20, [(0,vue__WEBPACK_IMPORTED_MODULE_0__.createElementVNode)("input", {
    type: "checkbox",
    "class": "form-check-input",
    id: 'syncComp-deleteGapps' + $props.entityType,
    checked: _ctx.deleteOptions.deleteOnGApps === '1',
    onInput: _cache[7] || (_cache[7] = function (event) {
      return $options.setDeleteOption('deleteOnGApps', event.target.checked ? '1' : '0');
    })
  }, null, 40
  /* PROPS, HYDRATE_EVENTS */
  , _hoisted_21), (0,vue__WEBPACK_IMPORTED_MODULE_0__.createElementVNode)("label", {
    "class": "form-check-label",
    "for": 'syncComp-deleteGapps' + $props.entityType
  }, "Elimina da Google Apps", 8
  /* PROPS */
  , _hoisted_22)])) : (0,vue__WEBPACK_IMPORTED_MODULE_0__.createCommentVNode)("v-if", true), $props.entityType === 'students' || $props.entityType === 'teachers' ? ((0,vue__WEBPACK_IMPORTED_MODULE_0__.openBlock)(), (0,vue__WEBPACK_IMPORTED_MODULE_0__.createElementBlock)("div", _hoisted_23, [(0,vue__WEBPACK_IMPORTED_MODULE_0__.createElementVNode)("label", {
    "class": "text-nowrap",
    "for": 'deleteOnGApps' + $props.entityType
  }, "Elimina su Google Apps", 8
  /* PROPS */
  , _hoisted_24), (0,vue__WEBPACK_IMPORTED_MODULE_0__.createElementVNode)("select", {
    "class": "form-select",
    id: 'deleteOnGApps' + $props.entityType,
    value: _ctx.deleteOptions.deleteOnGApps,
    onInput: _cache[8] || (_cache[8] = function (event) {
      return $options.setDeleteOption('deleteOnGApps', event.target.value);
    })
  }, _hoisted_29, 40
  /* PROPS, HYDRATE_EVENTS */
  , _hoisted_25)])) : (0,vue__WEBPACK_IMPORTED_MODULE_0__.createCommentVNode)("v-if", true), ($props.entityType === 'students' || $props.entityType === 'teachers') && _ctx.deleteOptions.deleteOnGApps === '2' ? ((0,vue__WEBPACK_IMPORTED_MODULE_0__.openBlock)(), (0,vue__WEBPACK_IMPORTED_MODULE_0__.createElementBlock)("div", _hoisted_30, [(0,vue__WEBPACK_IMPORTED_MODULE_0__.createElementVNode)("label", {
    "for": 'gAppsOu' + $props.entityType
  }, "OU", 8
  /* PROPS */
  , _hoisted_31), (0,vue__WEBPACK_IMPORTED_MODULE_0__.createElementVNode)("input", {
    type: "text",
    "class": "form-control",
    id: 'gAppsOu' + $props.entityType,
    value: _ctx.deleteOptions.gAppsOu,
    onInput: _cache[9] || (_cache[9] = function (event) {
      return $options.setDeleteOption('gAppsOu', event.target.value);
    })
  }, null, 40
  /* PROPS, HYDRATE_EVENTS */
  , _hoisted_32)])) : (0,vue__WEBPACK_IMPORTED_MODULE_0__.createCommentVNode)("v-if", true)])])]), (0,vue__WEBPACK_IMPORTED_MODULE_0__.createElementVNode)("div", _hoisted_33, [(0,vue__WEBPACK_IMPORTED_MODULE_0__.createElementVNode)("button", {
    type: "button",
    "class": "btn btn-secondary",
    onClick: _cache[10] || (_cache[10] = function ($event) {
      return _ctx.entitiesToDelete = [];
    })
  }, "Annulla"), (0,vue__WEBPACK_IMPORTED_MODULE_0__.createElementVNode)("button", {
    type: "button",
    "class": "btn btn-danger",
    onClick: _cache[11] || (_cache[11] = function () {
      return $options.queueDeleteActions && $options.queueDeleteActions.apply($options, arguments);
    })
  }, "Elimina")])])])], 2
  /* CLASS */
  )]))])) : (0,vue__WEBPACK_IMPORTED_MODULE_0__.createCommentVNode)("v-if", true);
}

/***/ }),

/***/ "./node_modules/babel-loader/lib/index.js??clonedRuleSet-1.use[0]!./node_modules/vue-loader/dist/templateLoader.js??ruleSet[1].rules[2]!./node_modules/vue-loader/dist/index.js??ruleSet[0].use[0]!./assets/js/components/SyncTableTr.vue?vue&type=template&id=cf524e80":
/*!******************************************************************************************************************************************************************************************************************************************************************************!*\
  !*** ./node_modules/babel-loader/lib/index.js??clonedRuleSet-1.use[0]!./node_modules/vue-loader/dist/templateLoader.js??ruleSet[1].rules[2]!./node_modules/vue-loader/dist/index.js??ruleSet[0].use[0]!./assets/js/components/SyncTableTr.vue?vue&type=template&id=cf524e80 ***!
  \******************************************************************************************************************************************************************************************************************************************************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   render: () => (/* binding */ render)
/* harmony export */ });
/* harmony import */ var vue__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! vue */ "./node_modules/vue/dist/vue.esm-bundler.js");

var _hoisted_1 = ["checked", "disabled"];
var _hoisted_2 = ["disabled"];
var _hoisted_3 = ["disabled"];
function render(_ctx, _cache, $props, $setup, $data, $options) {
  return (0,vue__WEBPACK_IMPORTED_MODULE_0__.openBlock)(), (0,vue__WEBPACK_IMPORTED_MODULE_0__.createElementBlock)("tr", null, [(0,vue__WEBPACK_IMPORTED_MODULE_0__.createElementVNode)("td", null, [(0,vue__WEBPACK_IMPORTED_MODULE_0__.createElementVNode)("input", {
    type: "checkbox",
    checked: $props.checked,
    onInput: _cache[0] || (_cache[0] = function ($event) {
      return _ctx.$emit('checkboxclicked', $props.id);
    }),
    disabled: $options.disabled,
    "class": "form-check-input"
  }, null, 40
  /* PROPS, HYDRATE_EVENTS */
  , _hoisted_1)]), (0,vue__WEBPACK_IMPORTED_MODULE_0__.createElementVNode)("td", null, (0,vue__WEBPACK_IMPORTED_MODULE_0__.toDisplayString)($props.entity.data._name), 1
  /* TEXT */
  ), ((0,vue__WEBPACK_IMPORTED_MODULE_0__.openBlock)(true), (0,vue__WEBPACK_IMPORTED_MODULE_0__.createElementBlock)(vue__WEBPACK_IMPORTED_MODULE_0__.Fragment, null, (0,vue__WEBPACK_IMPORTED_MODULE_0__.renderList)($props.extraColumns, function (column) {
    var _$props$entity$data$c;

    return (0,vue__WEBPACK_IMPORTED_MODULE_0__.openBlock)(), (0,vue__WEBPACK_IMPORTED_MODULE_0__.createElementBlock)("td", null, (0,vue__WEBPACK_IMPORTED_MODULE_0__.toDisplayString)((_$props$entity$data$c = $props.entity.data[column.key]) !== null && _$props$entity$data$c !== void 0 ? _$props$entity$data$c : ''), 1
    /* TEXT */
    );
  }), 256
  /* UNKEYED_FRAGMENT */
  )), (0,vue__WEBPACK_IMPORTED_MODULE_0__.createElementVNode)("td", null, (0,vue__WEBPACK_IMPORTED_MODULE_0__.toDisplayString)($options.stateMsg), 1
  /* TEXT */
  ), (0,vue__WEBPACK_IMPORTED_MODULE_0__.createElementVNode)("td", null, [$props.showMainActionBtn ? ((0,vue__WEBPACK_IMPORTED_MODULE_0__.openBlock)(), (0,vue__WEBPACK_IMPORTED_MODULE_0__.createElementBlock)("button", {
    key: 0,
    onClick: _cache[1] || (_cache[1] = function ($event) {
      return _ctx.$emit('main-btn-click', $props.id);
    }),
    "class": "btn btn-primary",
    disabled: $options.disabled
  }, (0,vue__WEBPACK_IMPORTED_MODULE_0__.toDisplayString)($props.mainActionBtnLabel), 9
  /* TEXT, PROPS */
  , _hoisted_2)) : (0,vue__WEBPACK_IMPORTED_MODULE_0__.createCommentVNode)("v-if", true), $props.showDeleteBtn ? ((0,vue__WEBPACK_IMPORTED_MODULE_0__.openBlock)(), (0,vue__WEBPACK_IMPORTED_MODULE_0__.createElementBlock)("button", {
    key: 1,
    onClick: _cache[2] || (_cache[2] = function ($event) {
      return _ctx.$emit('delete-btn-click', $props.id);
    }),
    "class": "btn btn-danger ms-2",
    disabled: $options.disabled
  }, "Elimina", 8
  /* PROPS */
  , _hoisted_3)) : (0,vue__WEBPACK_IMPORTED_MODULE_0__.createCommentVNode)("v-if", true)])]);
}

/***/ }),

/***/ "./assets/scss/bootstrap-custom.scss":
/*!*******************************************!*\
  !*** ./assets/scss/bootstrap-custom.scss ***!
  \*******************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

"use strict";
__webpack_require__.r(__webpack_exports__);
// extracted by mini-css-extract-plugin


/***/ }),

/***/ "./assets/js/components/SyncTable.vue":
/*!********************************************!*\
  !*** ./assets/js/components/SyncTable.vue ***!
  \********************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "default": () => (__WEBPACK_DEFAULT_EXPORT__)
/* harmony export */ });
/* harmony import */ var _SyncTable_vue_vue_type_template_id_7a5b30bc__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./SyncTable.vue?vue&type=template&id=7a5b30bc */ "./assets/js/components/SyncTable.vue?vue&type=template&id=7a5b30bc");
/* harmony import */ var _SyncTable_vue_vue_type_script_lang_js__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./SyncTable.vue?vue&type=script&lang=js */ "./assets/js/components/SyncTable.vue?vue&type=script&lang=js");
/* harmony import */ var _node_modules_vue_loader_dist_exportHelper_js__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../../../node_modules/vue-loader/dist/exportHelper.js */ "./node_modules/vue-loader/dist/exportHelper.js");




;
const __exports__ = /*#__PURE__*/(0,_node_modules_vue_loader_dist_exportHelper_js__WEBPACK_IMPORTED_MODULE_2__["default"])(_SyncTable_vue_vue_type_script_lang_js__WEBPACK_IMPORTED_MODULE_1__["default"], [['render',_SyncTable_vue_vue_type_template_id_7a5b30bc__WEBPACK_IMPORTED_MODULE_0__.render],['__file',"assets/js/components/SyncTable.vue"]])
/* hot reload */
if (false) {}


/* harmony default export */ const __WEBPACK_DEFAULT_EXPORT__ = (__exports__);

/***/ }),

/***/ "./assets/js/components/SyncTableTr.vue":
/*!**********************************************!*\
  !*** ./assets/js/components/SyncTableTr.vue ***!
  \**********************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "default": () => (__WEBPACK_DEFAULT_EXPORT__)
/* harmony export */ });
/* harmony import */ var _SyncTableTr_vue_vue_type_template_id_cf524e80__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./SyncTableTr.vue?vue&type=template&id=cf524e80 */ "./assets/js/components/SyncTableTr.vue?vue&type=template&id=cf524e80");
/* harmony import */ var _SyncTableTr_vue_vue_type_script_lang_js__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./SyncTableTr.vue?vue&type=script&lang=js */ "./assets/js/components/SyncTableTr.vue?vue&type=script&lang=js");
/* harmony import */ var _node_modules_vue_loader_dist_exportHelper_js__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../../../node_modules/vue-loader/dist/exportHelper.js */ "./node_modules/vue-loader/dist/exportHelper.js");




;
const __exports__ = /*#__PURE__*/(0,_node_modules_vue_loader_dist_exportHelper_js__WEBPACK_IMPORTED_MODULE_2__["default"])(_SyncTableTr_vue_vue_type_script_lang_js__WEBPACK_IMPORTED_MODULE_1__["default"], [['render',_SyncTableTr_vue_vue_type_template_id_cf524e80__WEBPACK_IMPORTED_MODULE_0__.render],['__file',"assets/js/components/SyncTableTr.vue"]])
/* hot reload */
if (false) {}


/* harmony default export */ const __WEBPACK_DEFAULT_EXPORT__ = (__exports__);

/***/ }),

/***/ "./assets/js/components/SyncTable.vue?vue&type=script&lang=js":
/*!********************************************************************!*\
  !*** ./assets/js/components/SyncTable.vue?vue&type=script&lang=js ***!
  \********************************************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "default": () => (/* reexport safe */ _node_modules_babel_loader_lib_index_js_clonedRuleSet_1_use_0_node_modules_vue_loader_dist_index_js_ruleSet_0_use_0_SyncTable_vue_vue_type_script_lang_js__WEBPACK_IMPORTED_MODULE_0__["default"])
/* harmony export */ });
/* harmony import */ var _node_modules_babel_loader_lib_index_js_clonedRuleSet_1_use_0_node_modules_vue_loader_dist_index_js_ruleSet_0_use_0_SyncTable_vue_vue_type_script_lang_js__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! -!../../../node_modules/babel-loader/lib/index.js??clonedRuleSet-1.use[0]!../../../node_modules/vue-loader/dist/index.js??ruleSet[0].use[0]!./SyncTable.vue?vue&type=script&lang=js */ "./node_modules/babel-loader/lib/index.js??clonedRuleSet-1.use[0]!./node_modules/vue-loader/dist/index.js??ruleSet[0].use[0]!./assets/js/components/SyncTable.vue?vue&type=script&lang=js");
 

/***/ }),

/***/ "./assets/js/components/SyncTableTr.vue?vue&type=script&lang=js":
/*!**********************************************************************!*\
  !*** ./assets/js/components/SyncTableTr.vue?vue&type=script&lang=js ***!
  \**********************************************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "default": () => (/* reexport safe */ _node_modules_babel_loader_lib_index_js_clonedRuleSet_1_use_0_node_modules_vue_loader_dist_index_js_ruleSet_0_use_0_SyncTableTr_vue_vue_type_script_lang_js__WEBPACK_IMPORTED_MODULE_0__["default"])
/* harmony export */ });
/* harmony import */ var _node_modules_babel_loader_lib_index_js_clonedRuleSet_1_use_0_node_modules_vue_loader_dist_index_js_ruleSet_0_use_0_SyncTableTr_vue_vue_type_script_lang_js__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! -!../../../node_modules/babel-loader/lib/index.js??clonedRuleSet-1.use[0]!../../../node_modules/vue-loader/dist/index.js??ruleSet[0].use[0]!./SyncTableTr.vue?vue&type=script&lang=js */ "./node_modules/babel-loader/lib/index.js??clonedRuleSet-1.use[0]!./node_modules/vue-loader/dist/index.js??ruleSet[0].use[0]!./assets/js/components/SyncTableTr.vue?vue&type=script&lang=js");
 

/***/ }),

/***/ "./assets/js/components/SyncTable.vue?vue&type=template&id=7a5b30bc":
/*!**************************************************************************!*\
  !*** ./assets/js/components/SyncTable.vue?vue&type=template&id=7a5b30bc ***!
  \**************************************************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   render: () => (/* reexport safe */ _node_modules_babel_loader_lib_index_js_clonedRuleSet_1_use_0_node_modules_vue_loader_dist_templateLoader_js_ruleSet_1_rules_2_node_modules_vue_loader_dist_index_js_ruleSet_0_use_0_SyncTable_vue_vue_type_template_id_7a5b30bc__WEBPACK_IMPORTED_MODULE_0__.render)
/* harmony export */ });
/* harmony import */ var _node_modules_babel_loader_lib_index_js_clonedRuleSet_1_use_0_node_modules_vue_loader_dist_templateLoader_js_ruleSet_1_rules_2_node_modules_vue_loader_dist_index_js_ruleSet_0_use_0_SyncTable_vue_vue_type_template_id_7a5b30bc__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! -!../../../node_modules/babel-loader/lib/index.js??clonedRuleSet-1.use[0]!../../../node_modules/vue-loader/dist/templateLoader.js??ruleSet[1].rules[2]!../../../node_modules/vue-loader/dist/index.js??ruleSet[0].use[0]!./SyncTable.vue?vue&type=template&id=7a5b30bc */ "./node_modules/babel-loader/lib/index.js??clonedRuleSet-1.use[0]!./node_modules/vue-loader/dist/templateLoader.js??ruleSet[1].rules[2]!./node_modules/vue-loader/dist/index.js??ruleSet[0].use[0]!./assets/js/components/SyncTable.vue?vue&type=template&id=7a5b30bc");


/***/ }),

/***/ "./assets/js/components/SyncTableTr.vue?vue&type=template&id=cf524e80":
/*!****************************************************************************!*\
  !*** ./assets/js/components/SyncTableTr.vue?vue&type=template&id=cf524e80 ***!
  \****************************************************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   render: () => (/* reexport safe */ _node_modules_babel_loader_lib_index_js_clonedRuleSet_1_use_0_node_modules_vue_loader_dist_templateLoader_js_ruleSet_1_rules_2_node_modules_vue_loader_dist_index_js_ruleSet_0_use_0_SyncTableTr_vue_vue_type_template_id_cf524e80__WEBPACK_IMPORTED_MODULE_0__.render)
/* harmony export */ });
/* harmony import */ var _node_modules_babel_loader_lib_index_js_clonedRuleSet_1_use_0_node_modules_vue_loader_dist_templateLoader_js_ruleSet_1_rules_2_node_modules_vue_loader_dist_index_js_ruleSet_0_use_0_SyncTableTr_vue_vue_type_template_id_cf524e80__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! -!../../../node_modules/babel-loader/lib/index.js??clonedRuleSet-1.use[0]!../../../node_modules/vue-loader/dist/templateLoader.js??ruleSet[1].rules[2]!../../../node_modules/vue-loader/dist/index.js??ruleSet[0].use[0]!./SyncTableTr.vue?vue&type=template&id=cf524e80 */ "./node_modules/babel-loader/lib/index.js??clonedRuleSet-1.use[0]!./node_modules/vue-loader/dist/templateLoader.js??ruleSet[1].rules[2]!./node_modules/vue-loader/dist/index.js??ruleSet[0].use[0]!./assets/js/components/SyncTableTr.vue?vue&type=template&id=cf524e80");


/***/ }),

/***/ "./assets/images/Fortinet_logo.svg":
/*!*****************************************!*\
  !*** ./assets/images/Fortinet_logo.svg ***!
  \*****************************************/
/***/ ((module, __unused_webpack_exports, __webpack_require__) => {

"use strict";
module.exports = __webpack_require__.p + "images/Fortinet_logo.dcdc6960.svg";

/***/ }),

/***/ "./assets/images/Mikrotik_logo.svg":
/*!*****************************************!*\
  !*** ./assets/images/Mikrotik_logo.svg ***!
  \*****************************************/
/***/ ((module, __unused_webpack_exports, __webpack_require__) => {

"use strict";
module.exports = __webpack_require__.p + "images/Mikrotik_logo.2568ef88.svg";

/***/ }),

/***/ "./assets/images/Watchguard_logo.svg":
/*!*******************************************!*\
  !*** ./assets/images/Watchguard_logo.svg ***!
  \*******************************************/
/***/ ((module, __unused_webpack_exports, __webpack_require__) => {

"use strict";
module.exports = __webpack_require__.p + "images/Watchguard_logo.bb43db5f.svg";

/***/ }),

/***/ "./assets/images/favicon/android-chrome-192x192.png":
/*!**********************************************************!*\
  !*** ./assets/images/favicon/android-chrome-192x192.png ***!
  \**********************************************************/
/***/ ((module, __unused_webpack_exports, __webpack_require__) => {

"use strict";
module.exports = __webpack_require__.p + "images/android-chrome-192x192.f5b11640.png";

/***/ }),

/***/ "./assets/images/favicon/android-chrome-512x512.png":
/*!**********************************************************!*\
  !*** ./assets/images/favicon/android-chrome-512x512.png ***!
  \**********************************************************/
/***/ ((module, __unused_webpack_exports, __webpack_require__) => {

"use strict";
module.exports = __webpack_require__.p + "images/android-chrome-512x512.255054bd.png";

/***/ }),

/***/ "./assets/images/favicon/apple-touch-icon.png":
/*!****************************************************!*\
  !*** ./assets/images/favicon/apple-touch-icon.png ***!
  \****************************************************/
/***/ ((module, __unused_webpack_exports, __webpack_require__) => {

"use strict";
module.exports = __webpack_require__.p + "images/apple-touch-icon.e755ebc3.png";

/***/ }),

/***/ "./assets/images/favicon/favicon-16x16.png":
/*!*************************************************!*\
  !*** ./assets/images/favicon/favicon-16x16.png ***!
  \*************************************************/
/***/ ((module, __unused_webpack_exports, __webpack_require__) => {

"use strict";
module.exports = __webpack_require__.p + "images/favicon-16x16.9f1b883a.png";

/***/ }),

/***/ "./assets/images/favicon/favicon-32x32.png":
/*!*************************************************!*\
  !*** ./assets/images/favicon/favicon-32x32.png ***!
  \*************************************************/
/***/ ((module, __unused_webpack_exports, __webpack_require__) => {

"use strict";
module.exports = __webpack_require__.p + "images/favicon-32x32.17d0016a.png";

/***/ }),

/***/ "./assets/images/favicon/favicon.ico":
/*!*******************************************!*\
  !*** ./assets/images/favicon/favicon.ico ***!
  \*******************************************/
/***/ ((module, __unused_webpack_exports, __webpack_require__) => {

"use strict";
module.exports = __webpack_require__.p + "images/favicon.bc2028a0.ico";

/***/ })

},
/******/ __webpack_require__ => { // webpackRuntimeModules
/******/ var __webpack_exec__ = (moduleId) => (__webpack_require__(__webpack_require__.s = moduleId))
/******/ __webpack_require__.O(0, ["vendors-node_modules_fortawesome_fontawesome-free_js_all_js-node_modules_pugx_filter-bundle_j-9a1ab2"], () => (__webpack_exec__("./assets/js/app.js")));
/******/ var __webpack_exports__ = __webpack_require__.O();
/******/ }
]);
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiYXBwLmpzIiwibWFwcGluZ3MiOiI7Ozs7Ozs7O0FBQUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTs7O0FBR0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBOzs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7QUM5QkFBLG1CQUFPLENBQUMsb0RBQUQsQ0FBUDs7QUFFQSxJQUFNQyxhQUFhLEdBQUdELHFHQUF0Qjs7QUFDQUMsYUFBYSxDQUFDRSxJQUFkLEdBQXFCQyxPQUFyQixDQUE2QkgsYUFBN0I7QUFHQTs7QUFFQSxJQUFNSSxDQUFDLEdBQUdMLG1CQUFPLENBQUMsb0RBQUQsQ0FBakI7O0FBQ0FNLHFCQUFNLENBQUNELENBQVAsR0FBV0MscUJBQU0sQ0FBQ0MsTUFBUCxHQUFnQkYsQ0FBM0I7O0FBQ0FMLG1CQUFPLENBQUMsd0RBQUQsQ0FBUDs7QUFDQUEsbUJBQU8sQ0FBQyxzRkFBRCxDQUFQOztBQUVBQSxtQkFBTyxDQUFDLDBFQUFELENBQVA7O0FBQ0FRLE1BQU0sQ0FBQ0MsU0FBUCxHQUFtQlQsbUJBQU8sQ0FBQyxvRUFBRCxDQUExQjtBQUNBOztBQUNBQSxtQkFBTyxDQUFDLDhGQUFELENBQVA7O0FBRUFBLG1CQUFPLENBQUMsMERBQUQsQ0FBUDs7QUFFQTtBQUNBTSxxQkFBTSxDQUFDUSxLQUFQLEdBQWFBLHFEQUFiO0FBRUE7QUFDQUEscURBQUssQ0FBQ0UsUUFBTixDQUFlRCxpRUFBZjs7QUFLQSxJQUFNRSxPQUFPLEdBQUdqQixtQkFBTyxDQUFDLGdFQUFELENBQXZCOztBQUNBTSxxQkFBTSxDQUFDVyxPQUFQLEdBQWVBLE9BQWY7QUFFQTtBQUVBWCxxQkFBTSxDQUFDWSxZQUFQLEdBQXNCQSxtRUFBdEI7QUFFQTtBQUVBO0FBQ0E7QUFDQVoscUJBQU0sQ0FBQ2UsWUFBUCxHQUFzQkYsMENBQXRCO0FBQ0FiLHFCQUFNLENBQUNjLFNBQVAsR0FBbUJBLGlFQUFuQjtBQUNBRSxVQUFVLENBQUNDLHFCQUFYLEdBQW1DQyxLQUFBLElBQTBDQSxDQUE3RTtBQUVBO0FBQ0FsQixxQkFBTSxDQUFDcUIsS0FBUCxHQUFlQSw2Q0FBZjtBQUVBdEIsQ0FBQyxDQUFDdUIsUUFBRCxDQUFELENBQVlDLEtBQVosQ0FBa0IsWUFBWTtFQUMxQjs7RUFEMEI7O0VBRzFCLElBQUl0QixNQUFNLEdBQUd1QixVQUFiLEVBQXlCO0lBQ3JCekIsQ0FBQyxDQUFDLFNBQUQsQ0FBRCxDQUFheUIsVUFBYjtFQUNIO0VBRUQ7OztFQUNBekIsQ0FBQyxDQUFDdUIsUUFBRCxDQUFELENBQVlHLEVBQVosQ0FBZSxjQUFmLEVBQStCLFlBQU07SUFDakMsSUFBSUMsUUFBUSxHQUFHSixRQUFRLENBQUNLLGdCQUFULENBQTBCLGlEQUExQixDQUFmO0lBQ0E1QixDQUFDLENBQUMsS0FBRCxDQUFELENBQVE2QixHQUFSLENBQVksZUFBWixFQUE2QixZQUFNO01BQy9CQyxVQUFVLENBQUMsWUFBTTtRQUNiSCxRQUFRLENBQUNBLFFBQVEsQ0FBQ0ksTUFBVCxHQUFrQixDQUFuQixDQUFSLENBQThCQyxLQUE5QjtNQUNILENBRlMsRUFFUCxDQUZPLENBQVY7SUFHSCxDQUpEO0VBS0gsQ0FQRDtFQVNBaEMsQ0FBQyxDQUFDdUIsUUFBRCxDQUFELENBQVlVLFNBQVosQ0FBc0IsVUFBVUMsS0FBVixFQUFpQkMsS0FBakIsRUFBd0I7SUFDMUMsSUFBSUEsS0FBSyxDQUFDQyxNQUFOLEtBQWlCLEdBQWpCLElBQXdCRCxLQUFLLENBQUNFLFlBQU4sS0FBdUIsd0JBQW5ELEVBQTZFO01BQ3pFbEMsTUFBTSxDQUFDbUMsUUFBUCxDQUFnQkMsTUFBaEI7SUFDSDtFQUNKLENBSkQ7RUFNQXZDLENBQUMsQ0FBQyxtQkFBRCxDQUFELENBQXVCMEIsRUFBdkIsQ0FBMEIsbUJBQTFCLEVBQStDLFVBQVVjLENBQVYsRUFBYTtJQUN4RCxJQUFJQyxVQUFVLEdBQUd6QyxDQUFDLENBQUMsSUFBRCxDQUFsQjtJQUFBLElBQ0kwQyxRQUFRLEdBQUcxQyxDQUFDLENBQUN3QyxDQUFDLENBQUNHLE1BQUgsQ0FEaEI7SUFBQSxJQUVJQyxLQUFLLEdBQUdGLFFBQVEsQ0FBQ0csTUFBVCxHQUFrQkMsSUFBbEIsQ0FBdUIsZ0JBQXZCLENBRlo7SUFBQSxJQUdJQyxVQUFVLEdBQUdMLFFBQVEsQ0FBQ00sTUFBVCxHQUFrQkMsR0FBbEIsR0FBd0JSLFVBQVUsQ0FBQ08sTUFBWCxHQUFvQkMsR0FIN0Q7SUFBQSxJQUlJQyxhQUFhLEdBQUdILFVBQVUsR0FBR0wsUUFBUSxDQUFDUyxXQUFULENBQXFCLElBQXJCLENBSmpDO0lBQUEsSUFLSUMsZUFBZSxHQUFHRixhQUFhLEdBQUdOLEtBQUssQ0FBQ08sV0FBTixDQUFrQixJQUFsQixDQUx0QztJQU9BLElBQUlKLFVBQVUsR0FBR0gsS0FBSyxDQUFDTyxXQUFOLENBQWtCLElBQWxCLENBQWIsSUFBeUNDLGVBQWUsR0FBR1gsVUFBVSxDQUFDWSxNQUFYLEVBQS9ELEVBQ0laLFVBQVUsQ0FBQ2EsR0FBWCxDQUFlLGdCQUFmLEVBQWlDVixLQUFLLENBQUNPLFdBQU4sQ0FBa0IsSUFBbEIsQ0FBakM7RUFDUCxDQVZELEVBVUd6QixFQVZILENBVU0sa0JBVk4sRUFVMEIsWUFBWTtJQUNsQzFCLENBQUMsQ0FBQyxJQUFELENBQUQsQ0FBUXNELEdBQVIsQ0FBWSxnQkFBWixFQUE4QixDQUE5QjtFQUNILENBWkQ7QUFjSCxDQXJDRDs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7OztBQ2dDQTtBQUNBO0FBRUEsaUVBQWU7RUFDWEUsSUFBSSxFQUFFLFdBREs7RUFFWEMsS0FBSyxFQUFFO0lBQ0hDLFFBQVEsRUFBRTtNQUFDQyxJQUFJLEVBQUVDO0lBQVAsQ0FEUDtJQUVIQyxVQUFVLEVBQUU7TUFBQ0YsSUFBSSxFQUFFRztJQUFQLENBRlQ7SUFHSEMsYUFBYSxFQUFFO01BQUNKLElBQUksRUFBRUc7SUFBUCxDQUhaO0lBSUhFLG9CQUFvQixFQUFFO01BQUNMLElBQUksRUFBRUM7SUFBUCxDQUpuQjtJQUtISyxHQUFHLEVBQUU7TUFBQ04sSUFBSSxFQUFFRztJQUFQLENBTEY7SUFNSEksU0FBUyxFQUFFO01BQUNQLElBQUksRUFBRUcsTUFBUDtNQUFlLFdBQVM7SUFBeEIsQ0FOUjtJQU9ISyxlQUFlLEVBQUU7TUFBQ1IsSUFBSSxFQUFFRyxNQUFQO01BQWUsV0FBUztJQUF4QixDQVBkO0lBUUhNLGNBQWMsRUFBRTtNQUFDVCxJQUFJLEVBQUVVLE9BQVA7TUFBZ0IsV0FBUztJQUF6QixDQVJiO0lBU0hDLGdCQUFnQixFQUFFO01BQUNYLElBQUksRUFBRVUsT0FBUDtNQUFnQixXQUFTO0lBQXpCLENBVGY7SUFVSEUsWUFBWSxFQUFFO01BQUNaLElBQUksRUFBRWEsS0FBUDtNQUFjLFdBQVM7SUFBdkIsQ0FWWDtJQVdIQyxpQkFBaUIsRUFBRTtNQUFDZCxJQUFJLEVBQUVHLE1BQVA7TUFBZSxXQUFTO0lBQXhCO0VBWGhCLENBRkk7RUFlWFksSUFBSSxFQUFFLGdCQUFZO0lBQ2QsT0FBTztNQUNIQyxPQUFPLEVBQUUsRUFETjtNQUVIQyxVQUFVLEVBQUUsS0FGVDtNQUdIQyxhQUFhLG9CQUFNLEtBQUtiLG9CQUFYLENBSFY7TUFJSGMsY0FBYyxFQUFFLEVBSmI7TUFLSEMsWUFBWSxFQUFFLEtBTFg7TUFNSEMsZ0JBQWdCLEVBQUU7SUFOZixDQUFQO0VBUUgsQ0F4QlU7RUF5QlhDLE9BQU8sRUFBRTtJQUNMQyxlQUFlLEVBQUUseUJBQVVDLEVBQVYsRUFBYztNQUMzQixJQUFJQyxHQUFFLEdBQUksS0FBS1QsT0FBTCxDQUFhVSxPQUFiLENBQXFCRixFQUFyQixDQUFWOztNQUNBLElBQUlDLEdBQUUsR0FBSSxDQUFDLENBQVgsRUFBYztRQUNWLEtBQUtULE9BQUwsQ0FBYVcsTUFBYixDQUFvQkYsR0FBcEIsRUFBeUIsQ0FBekI7TUFDSixDQUZBLE1BRU87UUFDSCxLQUFLVCxPQUFMLENBQWFZLElBQWIsQ0FBa0JKLEVBQWxCO01BQ0o7SUFDSCxDQVJJO0lBU0xLLGlCQUFpQixFQUFFLDZCQUFZO01BQzNCLEtBQUtiLE9BQUwsR0FBZSxFQUFmOztNQUNBLElBQUksQ0FBQyxLQUFLQyxVQUFWLEVBQXNCO1FBQUEsMkNBQ0EsS0FBS2EsS0FBTCxDQUFXQyxRQURYO1FBQUE7O1FBQUE7VUFDbEIsb0RBQXVDO1lBQUEsSUFBOUJDLEtBQThCOztZQUNuQyxJQUFJLENBQUNBLEtBQUssQ0FBQ0MsUUFBWCxFQUFxQjtjQUNqQixLQUFLakIsT0FBTCxDQUFhWSxJQUFiLENBQWtCSSxLQUFLLENBQUNSLEVBQXhCO1lBQ0o7VUFDSjtRQUxrQjtVQUFBO1FBQUE7VUFBQTtRQUFBO01BTXRCOztNQUNBLEtBQUtQLFVBQUwsR0FBa0IsQ0FBQyxLQUFLQSxVQUF4QjtJQUNILENBbkJJO0lBb0JMaUIsVUFBVSxFQUFFLG9CQUFVQyxPQUFWLEVBQW1CO01BQzNCLElBQUksUUFBT0EsT0FBUCxNQUFtQixRQUFuQixJQUNBQSxPQUFPLENBQUNDLFFBQVIsS0FBcUJDLFNBRHJCLElBRUFGLE9BQU8sQ0FBQ2pDLFVBQVIsS0FBdUJtQyxTQUZ2QixJQUdBRixPQUFPLENBQUNHLE1BQVIsS0FBbUJELFNBSG5CLElBSUFGLE9BQU8sQ0FBQ0ksTUFBUixLQUFtQkYsU0FKdkIsRUFLRTtRQUNFRyxPQUFPLENBQUNDLElBQVIsQ0FBYSw2QkFBYixFQUE0Q04sT0FBNUM7UUFDQTtNQUNKOztNQUVBQSxPQUFPLENBQUNHLE1BQVIsQ0FBZUksS0FBZixHQUF1QixDQUF2QjtNQUVBLElBQUlwQyxHQUFFLEdBQUksS0FBS0EsR0FBZjs7TUFDQSxJQUFHNkIsT0FBTyxDQUFDSSxNQUFSLEtBQW1CLFFBQXRCLEVBQWdDO1FBQzVCakMsR0FBRSxHQUFJLEtBQUtDLFNBQVg7TUFDSjs7TUFFQSxJQUFJRCxHQUFFLEtBQU0sRUFBWixFQUFnQjtRQUNaO01BQ0o7O01BRUEsSUFBRyxPQUFPNkIsT0FBTyxDQUFDRyxNQUFSLENBQWV2QixJQUFmLENBQW9CNEIsVUFBM0IsS0FBMEMsV0FBN0MsRUFBMEQ7UUFDdERyQyxHQUFFLEdBQUlBLEdBQUcsQ0FBQ3NDLE9BQUosQ0FBWSxjQUFaLEVBQTJCVCxPQUFPLENBQUNHLE1BQVIsQ0FBZXZCLElBQWYsQ0FBb0I0QixVQUEvQyxDQUFOO01BQ0o7O01BRUEsSUFBTUUsS0FBSSxHQUFJLElBQWQ7TUFDQSxJQUFJOUIsSUFBRyxHQUFJO1FBQ1ArQixVQUFVLEVBQUVYLE9BQU8sQ0FBQ0csTUFBUixDQUFldkIsSUFEcEI7UUFFUHFCLFFBQVEsRUFBRUQsT0FBTyxDQUFDQyxRQUZYO1FBR1BsQyxVQUFVLEVBQUVpQyxPQUFPLENBQUNqQztNQUhiLENBQVg7O01BTUEsSUFBR2lDLE9BQU8sQ0FBQ0ksTUFBUixLQUFtQixRQUF0QixFQUFnQztRQUM1QixJQUFHSixPQUFPLENBQUNqQixhQUFSLEtBQTBCbUIsU0FBN0IsRUFBd0M7VUFDcENHLE9BQU8sQ0FBQ0MsSUFBUixDQUFhLHFDQUFiLEVBQW9ETixPQUFwRDtVQUNBO1FBQ0o7O1FBQ0FwQixJQUFJLENBQUMsU0FBRCxDQUFKLEdBQWtCb0IsT0FBTyxDQUFDakIsYUFBMUI7TUFDSjs7TUFFQXZELDZDQUFLLENBQUNvRixJQUFOLENBQVd6QyxHQUFYLEVBQWdCUyxJQUFoQixFQUFxQjtRQUNqQmlDLE9BQU8sRUFBRTtVQUNMLGdCQUFnQjtRQURYO01BRFEsQ0FBckIsRUFJR0MsSUFKSCxDQUlRLFVBQVVDLFFBQVYsRUFBb0I7UUFDeEIsSUFBSSxRQUFPQSxRQUFRLENBQUNuQyxJQUFoQixNQUF5QixRQUF6QixJQUFxQ21DLFFBQVEsQ0FBQ25DLElBQVQsQ0FBY29DLE9BQWQsS0FBMEIsSUFBbkUsRUFBeUU7VUFDckUsTUFBTSxRQUFPRCxRQUFRLENBQUNuQyxJQUFoQixNQUF5QixRQUF6QixJQUFxQ21DLFFBQVEsQ0FBQ25DLElBQVQsQ0FBY3FDLEtBQWQsS0FBd0JmLFNBQTdELEdBQXlFYSxRQUFRLENBQUNuQyxJQUFULENBQWNxQyxLQUF2RixHQUErRixJQUFyRztRQUNKOztRQUVBakIsT0FBTyxDQUFDRyxNQUFSLENBQWVJLEtBQWYsR0FBdUIsQ0FBdkI7O1FBQ0EsSUFBSVEsUUFBUSxDQUFDbkMsSUFBVCxDQUFjc0MsV0FBZCxLQUE4QmhCLFNBQWxDLEVBQTZDO1VBQ3pDLEtBQUtpQixLQUFMLENBQVcsbUJBQVgsa0NBQW9DbkIsT0FBcEM7WUFBNkNrQixXQUFXLEVBQUVILFFBQVEsQ0FBQ25DLElBQVQsQ0FBY3NDO1VBQXhFO1FBQ0o7O1FBRUFSLEtBQUssQ0FBQ3pCLFlBQU4sR0FBcUIsS0FBckI7UUFDQXlCLEtBQUssQ0FBQ1UsU0FBTjtNQUNILENBaEJELFdBZ0JTLFVBQVVDLEdBQVYsRUFBZTtRQUNwQnJCLE9BQU8sQ0FBQ0csTUFBUixDQUFlYyxLQUFmLEdBQXVCLE9BQU9JLEdBQVAsS0FBZSxRQUFmLEdBQTBCQSxHQUExQixHQUFnQyxvQ0FBdkQ7UUFDQXJCLE9BQU8sQ0FBQ0csTUFBUixDQUFlSSxLQUFmLEdBQXVCLENBQXZCO1FBQ0FHLEtBQUssQ0FBQ3pCLFlBQU4sR0FBcUIsS0FBckI7UUFDQXlCLEtBQUssQ0FBQ1UsU0FBTjtNQUNILENBckJEO0lBc0JILENBbkZJO0lBb0ZMRSw2QkFBNkIsRUFBRSx5Q0FBWTtNQUN2QyxLQUFLLElBQUlDLFdBQVQsSUFBd0IsS0FBSzFDLE9BQTdCLEVBQXNDO1FBQ2xDLEtBQUsyQyxXQUFMLENBQWlCLEtBQUszQyxPQUFMLENBQWEwQyxXQUFiLENBQWpCO01BQ0o7O01BRUEsS0FBSzFDLE9BQUwsR0FBZSxFQUFmO0lBQ0gsQ0ExRkk7SUEyRkw0QyxZQTNGSyx3QkEyRlF4QixRQTNGUixFQTJGa0I7TUFDbkIsSUFBRyxLQUFLbEMsVUFBTCxLQUFvQixVQUFwQixJQUFrQyxLQUFLQSxVQUFMLEtBQW9CLFVBQXRELElBQW9FLEtBQUtBLFVBQUwsS0FBb0IsUUFBM0YsRUFBcUc7UUFDakcsS0FBS21CLGdCQUFMLEdBQXdCLENBQUNlLFFBQUQsQ0FBeEI7UUFDQTtNQUNKOztNQUVBLEtBQUt1QixXQUFMLENBQWlCdkIsUUFBakIsRUFBMkIsUUFBM0I7SUFDSCxDQWxHSTtJQW1HTHlCLGNBbkdLLDRCQW1HWTtNQUNiLElBQUcsS0FBSzNELFVBQUwsS0FBb0IsVUFBcEIsSUFBa0MsS0FBS0EsVUFBTCxLQUFvQixVQUF0RCxJQUFvRSxLQUFLQSxVQUFMLEtBQW9CLFFBQTNGLEVBQXFHO1FBQ2pHLEtBQUttQixnQkFBTCxHQUF3QixLQUFLTCxPQUE3QjtRQUNBO01BQ0o7O01BSmEsNENBTVEsS0FBS0EsT0FOYjtNQUFBOztNQUFBO1FBTWIsdURBQW1DO1VBQUEsSUFBMUJvQixRQUEwQjtVQUMvQixLQUFLdUIsV0FBTCxDQUFpQnZCLFFBQWpCLEVBQTJCLFFBQTNCO1FBQ0o7TUFSYTtRQUFBO01BQUE7UUFBQTtNQUFBOztNQVNiLEtBQUtwQixPQUFMLEdBQWUsRUFBZjtJQUNILENBN0dJO0lBOEdMOEMsa0JBOUdLLGdDQThHZ0I7TUFBQSw0Q0FDSSxLQUFLekMsZ0JBRFQ7TUFBQTs7TUFBQTtRQUNqQix1REFBNEM7VUFBQSxJQUFuQ2UsUUFBbUM7VUFDeEMsS0FBS3VCLFdBQUwsQ0FBaUJ2QixRQUFqQixFQUEyQixRQUEzQjtRQUNKO01BSGlCO1FBQUE7TUFBQTtRQUFBO01BQUE7O01BS2pCLEtBQUtmLGdCQUFMLEdBQXdCLEVBQXhCO01BQ0EsS0FBS0wsT0FBTCxHQUFlLEVBQWY7SUFDSCxDQXJISTtJQXNITDJDLFdBQVcsRUFBRSxxQkFBVXZCLFFBQVYsRUFBaUM7TUFBQSxJQUFiRyxNQUFhLHVFQUFOLElBQU07O01BQzFDLElBQUksS0FBS3hDLFFBQUwsQ0FBY3FDLFFBQWQsRUFBd0JNLEtBQXhCLEdBQWdDLENBQXBDLEVBQXVDO1FBQ25DO01BQ0o7O01BRUEsS0FBSzNDLFFBQUwsQ0FBY3FDLFFBQWQsRUFBd0JNLEtBQXhCLEdBQWdDLENBQWhDO01BQ0EsS0FBSzNDLFFBQUwsQ0FBY3FDLFFBQWQsRUFBd0JnQixLQUF4QixHQUFnQyxJQUFoQztNQUVBLEtBQUtqQyxjQUFMLENBQW9CUyxJQUFwQixDQUF5QjtRQUNyQlcsTUFBTSxFQUFFQSxNQUFLLEtBQU0sSUFBWCxHQUFrQkEsTUFBbEIsR0FBMkIsS0FBS25DLGFBRG5CO1FBRXJCZ0MsUUFBUSxFQUFFQSxRQUZXO1FBR3JCbEMsVUFBVSxFQUFFLEtBQUtBLFVBSEk7UUFJckJvQyxNQUFNLEVBQUUsS0FBS3ZDLFFBQUwsQ0FBY3FDLFFBQWQsQ0FKYTtRQUtyQmxCLGFBQWEsRUFBRSxLQUFLQTtNQUxDLENBQXpCO01BUUEsS0FBS3FDLFNBQUw7SUFDSCxDQXZJSTtJQXdJTEEsU0FBUyxFQUFFLHFCQUFZO01BQ25CLElBQUksS0FBS25DLFlBQUwsSUFBcUIsS0FBS0QsY0FBTCxDQUFvQi9DLE1BQXBCLEdBQTZCLENBQXRELEVBQXlEO1FBQ3JEO01BQ0o7O01BRUEsSUFBTXlFLEtBQUksR0FBSSxJQUFkO01BQ0FBLEtBQUssQ0FBQ3pCLFlBQU4sR0FBcUIsSUFBckI7TUFFQWpELFVBQVUsQ0FBQyxZQUFZO1FBQ25CMEUsS0FBSyxDQUFDWCxVQUFOLENBQWlCVyxLQUFLLENBQUMxQixjQUFOLENBQXFCNEMsS0FBckIsRUFBakI7TUFDSCxDQUZTLEVBRVAsQ0FGTyxDQUFWO0lBR0gsQ0FuSkk7SUFvSkxDLGVBcEpLLDJCQW9KV0MsR0FwSlgsRUFvSmdCQyxLQXBKaEIsRUFvSnVCO01BQ3hCLEtBQUtoRCxhQUFMLENBQW1CK0MsR0FBbkIsSUFBMEJDLEtBQTFCO0lBQ0o7RUF0SkssQ0F6QkU7RUFpTFhDLFVBQVUsRUFBRTtJQUNSLGdCQUFnQnZFLHdEQUFXQTtFQURuQjtBQWpMRCxDQUFmOzs7Ozs7Ozs7Ozs7Ozs7OztBQ3BFQSxpRUFBZTtFQUNYQyxJQUFJLEVBQUUsYUFESztFQUVYQyxLQUFLLEVBQUUsQ0FBQyxRQUFELEVBQVcsWUFBWCxFQUF5QixTQUF6QixFQUFvQyxJQUFwQyxFQUEwQyxjQUExQyxFQUEwRCxlQUExRCxFQUEyRSxtQkFBM0UsRUFBZ0csb0JBQWhHLEVBQXNILG1CQUF0SCxDQUZJO0VBR1hzRSxRQUFRLEVBQUU7SUFDTkMsUUFBUSxFQUFFLG9CQUFZO01BQ2xCLElBQUksS0FBSy9CLE1BQUwsQ0FBWWMsS0FBWixLQUFzQixJQUExQixFQUFnQztRQUM1QixPQUFPLGFBQWEsS0FBS2QsTUFBTCxDQUFZYyxLQUFoQztNQUNKOztNQUVBLElBQUksS0FBS2xELFVBQUwsS0FBb0IsVUFBcEIsSUFBa0MsS0FBS29DLE1BQUwsQ0FBWXZCLElBQVosQ0FBaUJ1RCxlQUFqQixLQUFxQyxJQUEzRSxFQUFpRjtRQUM3RSxPQUFPLHVDQUFQO01BQ0o7O01BRUEsSUFBSSxLQUFLcEUsVUFBTCxLQUFvQixRQUFwQixJQUFnQyxLQUFLb0MsTUFBTCxDQUFZdkIsSUFBWixDQUFpQndELGdCQUFqQixLQUFzQyxJQUExRSxFQUFnRjtRQUM1RSxPQUFPLHdDQUFQO01BQ0o7O01BRUEsUUFBUSxLQUFLakMsTUFBTCxDQUFZSSxLQUFwQjtRQUNJLEtBQUssQ0FBTDtVQUNJLE9BQU8sU0FBUDs7UUFFSixLQUFLLENBQUw7VUFDSSxPQUFPLGNBQVA7O1FBRUosS0FBSyxDQUFMO1VBQ0ksT0FBTyxZQUFQOztRQUVKO1VBQ0ksT0FBTyxLQUFLNUIsaUJBQVo7TUFYUjtJQWFILENBM0JLO0lBNEJObUIsUUFBUSxFQUFFLG9CQUFZO01BQ2xCLElBQUksS0FBS0ssTUFBTCxDQUFZSSxLQUFaLEdBQW9CLENBQXhCLEVBQTJCO1FBQ3ZCLE9BQU8sSUFBUDtNQUNKOztNQUVBLElBQUksS0FBS3hDLFVBQUwsS0FBb0IsVUFBcEIsSUFBa0MsUUFBTyxLQUFLb0MsTUFBTCxDQUFZdkIsSUFBbkIsTUFBNEIsUUFBOUQsSUFBMEUsS0FBS3VCLE1BQUwsQ0FBWXZCLElBQVosQ0FBaUJ1RCxlQUFqQixLQUFxQyxJQUFuSCxFQUF5SDtRQUNySCxPQUFPLElBQVA7TUFDSjs7TUFFQSxJQUFJLEtBQUtwRSxVQUFMLEtBQW9CLFFBQXBCLElBQWdDLFFBQU8sS0FBS29DLE1BQUwsQ0FBWXZCLElBQW5CLE1BQTRCLFFBQTVELElBQXdFLEtBQUt1QixNQUFMLENBQVl2QixJQUFaLENBQWlCd0QsZ0JBQWpCLEtBQXNDLElBQWxILEVBQXdIO1FBQ3BILE9BQU8sSUFBUDtNQUNKOztNQUVBLE9BQU8sS0FBUDtJQUNKO0VBMUNNO0FBSEMsQ0FBZjs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7O0VEWmEsU0FBTTs7O0VBQ0EsU0FBTTs7Ozs4QkFHTEMsdURBQUFBLENBQWEsSUFBYixFQUFhLElBQWIsRUFBSSxNQUFKLEVBQVE7QUFBQTtBQUFSOzs4QkFFQUEsdURBQUFBLENBQWMsSUFBZCxFQUFjLElBQWQsRUFBSSxPQUFKLEVBQVM7QUFBQTtBQUFUOzs4QkFDQUEsdURBQUFBLENBQWUsSUFBZixFQUFlLElBQWYsRUFBSSxRQUFKLEVBQVU7QUFBQTtBQUFWOzs7OztFQTBCSCxTQUFNOzs7RUFDRixTQUFNOzs7RUFDRixTQUFNOzs7K0JBQ1BBLHVEQUFBQSxDQUF1RCxJQUF2RCxFQUF1RDtFQUFuRCxTQUFNO0FBQTZDLENBQXZELEVBQXdCLDRCQUF4QixFQUFrRDtBQUFBO0FBQWxEOzs7RUFHQyxTQUFNOzs7RUFDRCxTQUFNOzs7RUFDRixTQUFNOzs7O0VBQ0gsU0FBTTs7Ozs7O0VBSU4sU0FBTTs7Ozs7O0VBSU4sU0FBTTs7Ozs7K0JBR0hBLHVEQUFBQSxDQUE2QixRQUE3QixFQUE2QjtFQUFyQk4sS0FBSyxFQUFDO0FBQWUsQ0FBN0IsRUFBa0IsSUFBbEIsRUFBb0I7QUFBQTtBQUFwQjs7K0JBQ0FNLHVEQUFBQSxDQUE2QixRQUE3QixFQUE2QjtFQUFyQk4sS0FBSyxFQUFDO0FBQWUsQ0FBN0IsRUFBa0IsSUFBbEIsRUFBb0I7QUFBQTtBQUFwQjs7K0JBQ0FNLHVEQUFBQSxDQUF1QyxRQUF2QyxFQUF1QztFQUEvQk4sS0FBSyxFQUFDO0FBQXlCLENBQXZDLEVBQWtCLGNBQWxCLEVBQThCO0FBQUE7QUFBOUI7O21CQUZBTyxhQUNBQyxhQUNBQzs7O0VBR0gsU0FBTTs7Ozs7RUFPbEIsU0FBTTs7Ozs7U0FsRWhCQyxvQkFBYXZDLFNBQWIsSUFBMEJwQyxNQUFNLENBQUM5RCxJQUFQLENBQVl5SSxlQUFaLEVBQXNCeEcsTUFBdEIsR0FBNEIsdURBQWpFeUcsdURBQUFBLENBMEVNLEtBMUVOLEVBMEVNQyxVQTFFTixFQTBFTSxDQXpFRk4sdURBQUFBLENBMkJNLEtBM0JOLGNBMkJNLENBMUJGQSx1REFBQUEsQ0F5QlEsT0F6QlIsY0F5QlEsQ0F4QkpBLHVEQUFBQSxDQU1LLElBTkwsRUFNSyxJQU5MLEVBTUssQ0FMREEsdURBQUFBLENBQTBHLElBQTFHLEVBQTBHLElBQTFHLEVBQTBHLENBQXRHQSx1REFBQUEsQ0FBaUcsT0FBakcsRUFBaUc7SUFBMUZ4RSxJQUFJLEVBQUMsVUFBcUY7SUFBekVnQixPQUFPLEVBQUUrRCxlQUFnRTtJQUFuREMsT0FBSztNQUFBLE9BQUVDLG1GQUFGO0lBQUEsRUFBOEM7SUFBekIsU0FBTTtFQUFtQixDQUFqRzs7RUFBQSxhQUFzRyxDQUExRyxDQUtDLEVBSkRDLFVBSUMseURBSERMLHVEQUFBQSxDQUF1RE0seUNBQXZELEVBQXVELElBQXZELEVBQXVEQywrQ0FBQUEsQ0FBbENSLG1CQUFrQyxFQUF0QixVQUF0QlMsTUFBc0IsRUFBaEI7NkRBQWpCUix1REFBQUEsQ0FBdUQsSUFBdkQsRUFBdUQsSUFBdkQsRUFBdURTLG9EQUFBQSxDQUFsQkQsTUFBTSxDQUFDeEYsSUFBVyxDQUF2RCxFQUFnRDtJQUFBO0lBQWhEO0dBQXVELENBQXZEOztFQUFBLENBR0MsR0FGRDBGLFVBRUMsRUFEREMsVUFDQyxDQU5MLENBd0JJLHlEQWpCSlgsdURBQUFBLENBZ0JnQk0seUNBaEJoQixFQWdCZ0IsSUFoQmhCLEVBZ0JnQkMsK0NBQUFBLENBZmVSLGVBZWYsRUFmdUIsVUFBdkJ0QyxNQUF1QixFQUFmZCxFQUFlLEVBQWI7NkRBRDFCaUUsZ0RBQUFBLENBZ0JnQkMsdUJBaEJoQixFQWdCZ0I7TUFkUHBELE1BQU0sRUFBRUEsTUFjRDtNQWJQLGVBQWFzQyxpQkFhTjtNQVpQWCxHQUFHLEVBQUV6QyxFQVlFO01BWFBBLEVBQUUsRUFBRXJCLE1BQU0sQ0FBQ3FCLEVBQUQsQ0FXSDtNQVZQUixPQUFPLEVBQUUrRCxhQUFRckQsT0FBUixDQUFnQnZCLE1BQU0sQ0FBQ3FCLEVBQUQsQ0FBdEIsSUFBeUIsRUFVM0I7TUFUUCxpQkFBZW9ELG1CQVNSO01BUlAsd0JBQXNCQSxxQkFRZjtNQVBQLG1CQUFpQkEsdUJBT1Y7TUFOUCx5QkFBdUJBLHNCQU1oQjtNQUxQLHVCQUFxQkEsd0JBS2Q7TUFKUGUsY0FBYztRQUFBLE9BQUVWLHFCQUFZVyxNQUFaLENBQUY7TUFBQSxFQUlQO01BSFBDLGdCQUFnQjtRQUFBLE9BQUVaLHNCQUFhVyxNQUFiLENBQUY7TUFBQSxFQUdUO01BRlBFLGlCQUFlLEVBQUViLHdCQUVWO21CQUFBO01BRFJjLEdBQUcsRUFBQztJQUNJLENBaEJoQjs7SUFBQTtHQWdCZ0IsQ0FoQmhCOztFQUFBLENBaUJJLEVBekJSLENBMEJFLENBM0JOLENBeUVFLEVBN0NvQ25CLHlCQUFBQSw4Q0FBQUEsSUFBdENDLHVEQUFBQSxDQUFzSyxRQUF0SyxFQUFzSztVQUFBO0lBQTlKLFNBQU0saUJBQXdKO0lBQS9HNUMsUUFBUSxFQUFFOEMsYUFBUTNHLE1BQVIsR0FBYyxDQUF1RjtJQUFoRjRILE9BQUs7TUFBQSxPQUFFZiwyR0FBRjtJQUFBO0VBQTJFLENBQXRLLHVEQUErSEwsMEJBQWtCLGNBQWpKLEVBQTZKO0VBQUE7RUFBN0osRUFBNkpxQixVQUE3SiwwRUE2Q0UsRUE1Q3dDckIsMkJBQUFBLDhDQUFBQSxJQUExQ0MsdURBQUFBLENBQStJLFFBQS9JLEVBQStJO1VBQUE7SUFBdkksU0FBTSxxQkFBaUk7SUFBbEY1QyxRQUFRLEVBQUU4QyxhQUFRM0csTUFBUixHQUFjLENBQTBEO0lBQW5ENEgsT0FBSztNQUFBLE9BQUVmLDZFQUFGO0lBQUE7RUFBOEMsQ0FBL0ksRUFBbUgscUJBQW5ILEVBQXNJO0VBQUE7RUFBdEksRUFBc0lpQixVQUF0SSwwRUE0Q0UscURBMUNGVCxnREFBQUEsQ0F5Q1dVLHlDQXpDWCxFQXlDVztJQXpDREMsRUFBRSxFQUFDO0VBeUNGLENBekNYLEVBQW1CLENBQ25CNUIsdURBQUFBLENBdUNNLEtBdkNOLEVBdUNNO0lBdkNELFNBQUs2QixtREFBQUEsRUFBQyxPQUFELEVBQVE7TUFBQUMsTUFBOEJ2QixzQkFBaUIzRyxNQUFqQixHQUF1QixDQUFyRDtNQUFxRCxXQUFpQjJHLHNCQUFpQjNHLE1BQWpCLEdBQXVCO0lBQTdGLENBQVIsRUF1Q0o7SUF2Q2FtSSxRQUFRLEVBQUM7RUF1Q3RCLENBdkNOLEdBQ0kvQix1REFBQUEsQ0FxQ00sS0FyQ04sZUFxQ00sQ0FwQ0ZBLHVEQUFBQSxDQW1DTSxLQW5DTixlQW1DTSxDQWxDRkEsdURBQUFBLENBR00sS0FITixlQUdNLENBRkZnQyxXQUVFLEVBREZoQyx1REFBQUEsQ0FBaUcsUUFBakcsRUFBaUc7SUFBekZ4RSxJQUFJLEVBQUMsUUFBb0Y7SUFBM0UsU0FBTSxXQUFxRTtJQUF6RCxjQUFXLE9BQThDO0lBQXJDZ0csT0FBSztNQUFBLE9BQUVqQix3QkFBZ0IsRUFBbEI7SUFBQTtFQUFnQyxDQUFqRyxDQUNFLENBSE4sQ0FrQ0UsRUE5QkZQLHVEQUFBQSxDQXlCTSxLQXpCTixlQXlCTSxDQXhCRkEsdURBQUFBLENBdUJPLE1BdkJQLGVBdUJPLENBdEJIQSx1REFBQUEsQ0FxQk8sTUFyQlAsZUFxQk8sQ0FwQmdDSSxzQkFBVSxVQUFWLElBQTZCQSxzQkFBVSxVQUF2QyxJQUEwREEsc0JBQVUsOERBQXZHQyx1REFBQUEsQ0FHTSxLQUhOLGVBR00sQ0FGRkwsdURBQUFBLENBQXNNLE9BQXRNLEVBQXNNO0lBQS9MeEUsSUFBSSxFQUFDLFVBQTBMO0lBQS9LLFNBQU0sa0JBQXlLO0lBQXJKd0IsRUFBRSwwQkFBd0JvRCxpQkFBMkg7SUFBOUc1RCxPQUFPLEVBQUUrRCxtQkFBYzBCLFlBQXVGO0lBQXhFekIsT0FBSyxzQ0FBR3pHLEtBQUg7TUFBQSxPQUFhMEcseUJBQWUsY0FBZixFQUErQjFHLEtBQUssQ0FBQ1MsTUFBTixDQUFhZ0MsT0FBNUMsQ0FBYjtJQUFBO0VBQW1FLENBQXRNOztFQUFBLGNBRUUsRUFERndELHVEQUFBQSxDQUErRixPQUEvRixFQUErRjtJQUF4RixTQUFNLGtCQUFrRjtJQUE5RCxPQUFHLHdCQUF3Qkk7RUFBbUMsQ0FBL0YsRUFBd0UsaUJBQXhFLEVBQXVGO0VBQUE7RUFBdkYsRUFBdUY4QixXQUF2RixDQUNFLENBSE4sMEVBb0JHLEVBaEJxQzlCLHNCQUFVLDhEQUFsREMsdURBQUFBLENBR00sS0FITixlQUdNLENBRkZMLHVEQUFBQSxDQUF1TixPQUF2TixFQUF1TjtJQUFoTnhFLElBQUksRUFBQyxVQUEyTTtJQUFoTSxTQUFNLGtCQUEwTDtJQUF0S3dCLEVBQUUsMkJBQXlCb0QsaUJBQTJJO0lBQTlINUQsT0FBTyxFQUFFK0QsbUJBQWM0QixhQUFkLEtBQTJCLEdBQTBGO0lBQWpGM0IsT0FBSyxzQ0FBR3pHLEtBQUg7TUFBQSxPQUFhMEcseUJBQWUsZUFBZixFQUFnQzFHLEtBQUssQ0FBQ1MsTUFBTixDQUFhZ0MsT0FBYixHQUFvQixHQUFwQixHQUFvQixHQUFwRCxDQUFiO0lBQUE7RUFBNEUsQ0FBdk47O0VBQUEsY0FFRSxFQURGd0QsdURBQUFBLENBQXVHLE9BQXZHLEVBQXVHO0lBQWhHLFNBQU0sa0JBQTBGO0lBQXRFLE9BQUcseUJBQXlCSTtFQUEwQyxDQUF2RyxFQUF5RSx3QkFBekUsRUFBK0Y7RUFBQTtFQUEvRixFQUErRmdDLFdBQS9GLENBQ0UsQ0FITiwwRUFnQkcsRUFaZ0RoQyxzQkFBVSxVQUFWLElBQTZCQSxzQkFBVSxnRUFBMUZDLHVEQUFBQSxDQU9NLEtBUE4sZUFPTSxDQU5GTCx1REFBQUEsQ0FBMkYsT0FBM0YsRUFBMkY7SUFBcEYsU0FBTSxhQUE4RTtJQUEvRCxPQUFHLGtCQUFrQkk7RUFBMEMsQ0FBM0YsRUFBNkQsd0JBQTdELEVBQW1GO0VBQUE7RUFBbkYsRUFBbUZpQyxXQUFuRixDQU1FLEVBTEZyQyx1REFBQUEsQ0FJUyxRQUpULEVBSVM7SUFKRCxTQUFNLGFBSUw7SUFKb0JoRCxFQUFFLG9CQUFrQm9ELGlCQUl4QztJQUpxRFYsS0FBSyxFQUFFYSxtQkFBYzRCLGFBSTFFO0lBSjBGM0IsT0FBSyxzQ0FBR3pHLEtBQUg7TUFBQSxPQUFhMEcseUJBQWUsZUFBZixFQUFnQzFHLEtBQUssQ0FBQ1MsTUFBTixDQUFha0YsS0FBN0MsQ0FBYjtJQUFBO0VBSS9GLENBSlQ7O0VBQUEsY0FLRSxDQVBOLDBFQVlHLEdBSmlEVSxzQkFBVSxVQUFWLElBQTZCQSxzQkFBVSxlQUFvQkcsbUJBQWM0QixhQUFkLEtBQTJCLHlEQUExSTlCLHVEQUFBQSxDQUdNLEtBSE4sZUFHTSxDQUZGTCx1REFBQUEsQ0FBNkMsT0FBN0MsRUFBNkM7SUFBckMsT0FBRyxZQUFZSTtFQUFzQixDQUE3QyxFQUFtQyxJQUFuQyxFQUFxQztFQUFBO0VBQXJDLEVBQXFDa0MsV0FBckMsQ0FFRSxFQURGdEMsdURBQUFBLENBQW9LLE9BQXBLLEVBQW9LO0lBQTdKeEUsSUFBSSxFQUFDLE1BQXdKO0lBQWpKLFNBQU0sY0FBMkk7SUFBM0h3QixFQUFFLGNBQVlvRCxpQkFBNkc7SUFBaEdWLEtBQUssRUFBRWEsbUJBQWNnQyxPQUEyRTtJQUFqRS9CLE9BQUssc0NBQUd6RyxLQUFIO01BQUEsT0FBYTBHLHlCQUFlLFNBQWYsRUFBMEIxRyxLQUFLLENBQUNTLE1BQU4sQ0FBYWtGLEtBQXZDLENBQWI7SUFBQTtFQUE0RCxDQUFwSzs7RUFBQSxjQUNFLENBSE4sMEVBSUcsQ0FyQlAsQ0FzQkcsQ0F2QlAsQ0F3QkUsQ0F6Qk4sQ0E4QkUsRUFKRk0sdURBQUFBLENBR00sS0FITixlQUdNLENBRkZBLHVEQUFBQSxDQUE2RixRQUE3RixFQUE2RjtJQUFyRnhFLElBQUksRUFBQyxRQUFnRjtJQUF2RSxTQUFNLG1CQUFpRTtJQUE1Q2dHLE9BQUs7TUFBQSxPQUFFakIsd0JBQWdCLEVBQWxCO0lBQUE7RUFBdUMsQ0FBN0YsRUFBNkUsU0FBN0UsQ0FFRSxFQURGUCx1REFBQUEsQ0FBeUYsUUFBekYsRUFBeUY7SUFBakZ4RSxJQUFJLEVBQUMsUUFBNEU7SUFBbkUsU0FBTSxnQkFBNkQ7SUFBM0NnRyxPQUFLO01BQUEsT0FBRWYscUZBQUY7SUFBQTtFQUFzQyxDQUF6RixFQUF5RSxTQUF6RSxDQUNFLENBSE4sQ0FJRSxDQW5DTixDQW9DRSxDQXJDTixFQURKOztFQUFBLENBRG1CLENBQW5CLENBMENFLEVBMUVOOzs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7OzJEQ0FBSix1REFBQUEsQ0FTSyxJQVRMLEVBU0ssSUFUTCxFQVNLLENBUkRMLHVEQUFBQSxDQUF5SSxJQUF6SSxFQUF5SSxJQUF6SSxFQUF5SSxDQUFySUEsdURBQUFBLENBQWdJLE9BQWhJLEVBQWdJO0lBQXpIeEUsSUFBSSxFQUFDLFVBQW9IO0lBQXhHZ0IsT0FBTyxFQUFFNEQsY0FBK0Y7SUFBckZJLE9BQUs7TUFBQSxPQUFFRCxXQUFLLGlCQUFMLEVBQXlCSCxTQUF6QixDQUFGO0lBQUEsRUFBZ0Y7SUFBL0MzQyxRQUFRLEVBQUVnRCxpQkFBcUM7SUFBM0IsU0FBTTtFQUFxQixDQUFoSTs7RUFBQSxhQUFxSSxDQUF6SSxDQVFDLEVBUERULHVEQUFBQSxDQUFnQyxJQUFoQyxFQUFnQyxJQUFoQyxFQUFnQ2Msb0RBQUFBLENBQXpCVixjQUFPN0QsSUFBUCxDQUFZaUcsS0FBYSxDQUFoQyxFQUF3QjtFQUFBO0VBQXhCLENBT0MseURBTkRuQyx1REFBQUEsQ0FBMkVNLHlDQUEzRSxFQUEyRSxJQUEzRSxFQUEyRUMsK0NBQUFBLENBQXREUixtQkFBc0QsRUFBMUMsVUFBdEJTLE1BQXNCLEVBQWhCO0lBQUE7OzZEQUFqQlIsdURBQUFBLENBQTJFLElBQTNFLEVBQTJFLElBQTNFLEVBQTJFUyxvREFBQUEsMEJBQXJDVixjQUFPN0QsSUFBUCxDQUFZc0UsTUFBTSxDQUFDcEIsR0FBbkIsQ0FBcUMseUVBQWYsRUFBZSxDQUEzRSxFQUE0RDtJQUFBO0lBQTVEO0dBQTJFLENBQTNFOztFQUFBLENBTUMsR0FMRE8sdURBQUFBLENBQXVCLElBQXZCLEVBQXVCLElBQXZCLEVBQXVCYyxvREFBQUEsQ0FBaEJMLGlCQUFnQixDQUF2QixFQUFlO0VBQUE7RUFBZixDQUtDLEVBSkRULHVEQUFBQSxDQUdLLElBSEwsRUFHSyxJQUhMLEVBR0ssQ0FGK0ZJLDRCQUFBQSw4Q0FBQUEsSUFBaEdDLHVEQUFBQSxDQUFrSixRQUFsSixFQUFrSjtVQUFBO0lBQXpJbUIsT0FBSztNQUFBLE9BQUVqQixXQUFLLGdCQUFMLEVBQXdCSCxTQUF4QixDQUFGO0lBQUEsRUFBb0k7SUFBckcsU0FBTSxpQkFBK0Y7SUFBNUUzQyxRQUFRLEVBQUVnRDtFQUFrRSxDQUFsSix1REFBcUhMLDBCQUFySCxFQUF1STtFQUFBO0VBQXZJLEVBQXVJcUMsVUFBdkksMEVBRUMsRUFEcUdyQyx3QkFBQUEsOENBQUFBLElBQXRHQyx1REFBQUEsQ0FBcUksUUFBckksRUFBcUk7VUFBQTtJQUE1SG1CLE9BQUs7TUFBQSxPQUFFakIsV0FBSyxrQkFBTCxFQUEwQkgsU0FBMUIsQ0FBRjtJQUFBLEVBQXVIO0lBQXRGLFNBQU0scUJBQWdGO0lBQXpEM0MsUUFBUSxFQUFFZ0Q7RUFBK0MsQ0FBckksRUFBcUgsU0FBckgsRUFBNEg7RUFBQTtFQUE1SCxFQUE0SGlDLFVBQTVILDBFQUNDLENBSEwsQ0FJQyxDQVRMOzs7Ozs7Ozs7Ozs7O0FDREo7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7QUNBc0U7QUFDVjtBQUNMOztBQUV2RCxDQUFtRjtBQUNuRixpQ0FBaUMseUZBQWUsQ0FBQyw4RUFBTSxhQUFhLGdGQUFNO0FBQzFFO0FBQ0EsSUFBSSxLQUFVLEVBQUUsRUFZZjs7O0FBR0QsaUVBQWU7Ozs7Ozs7Ozs7Ozs7Ozs7OztBQ3RCeUQ7QUFDVjtBQUNMOztBQUV6RCxDQUFtRjtBQUNuRixpQ0FBaUMseUZBQWUsQ0FBQyxnRkFBTSxhQUFhLGtGQUFNO0FBQzFFO0FBQ0EsSUFBSSxLQUFVLEVBQUUsRUFZZjs7O0FBR0QsaUVBQWU7Ozs7Ozs7Ozs7Ozs7Ozs7QUN0QitMOzs7Ozs7Ozs7Ozs7Ozs7O0FDQUUiLCJzb3VyY2VzIjpbIndlYnBhY2s6Ly8vLi9hc3NldHMvaW1hZ2VzLyBzeW5jIFxcLihwbmclN0NqcGclN0NqcGVnJTdDZ2lmJTdDaWNvJTdDc3ZnJTdDd2VicCkkIiwid2VicGFjazovLy8uL2Fzc2V0cy9qcy9hcHAuanMiLCJ3ZWJwYWNrOi8vLy4vYXNzZXRzL2pzL2NvbXBvbmVudHMvU3luY1RhYmxlLnZ1ZSIsIndlYnBhY2s6Ly8vLi9hc3NldHMvanMvY29tcG9uZW50cy9TeW5jVGFibGVUci52dWUiLCJ3ZWJwYWNrOi8vLy4vYXNzZXRzL3Njc3MvYm9vdHN0cmFwLWN1c3RvbS5zY3NzIiwid2VicGFjazovLy8uL2Fzc2V0cy9qcy9jb21wb25lbnRzL1N5bmNUYWJsZS52dWU/ZjFhYiIsIndlYnBhY2s6Ly8vLi9hc3NldHMvanMvY29tcG9uZW50cy9TeW5jVGFibGVUci52dWU/YWE5ZiIsIndlYnBhY2s6Ly8vLi9hc3NldHMvanMvY29tcG9uZW50cy9TeW5jVGFibGUudnVlPzgyYjQiLCJ3ZWJwYWNrOi8vLy4vYXNzZXRzL2pzL2NvbXBvbmVudHMvU3luY1RhYmxlVHIudnVlP2Y5ZTQiLCJ3ZWJwYWNrOi8vLy4vYXNzZXRzL2pzL2NvbXBvbmVudHMvU3luY1RhYmxlLnZ1ZT9kOGNjIiwid2VicGFjazovLy8uL2Fzc2V0cy9qcy9jb21wb25lbnRzL1N5bmNUYWJsZVRyLnZ1ZT8yZjRlIl0sInNvdXJjZXNDb250ZW50IjpbInZhciBtYXAgPSB7XG5cdFwiLi9Gb3J0aW5ldF9sb2dvLnN2Z1wiOiBcIi4vYXNzZXRzL2ltYWdlcy9Gb3J0aW5ldF9sb2dvLnN2Z1wiLFxuXHRcIi4vTWlrcm90aWtfbG9nby5zdmdcIjogXCIuL2Fzc2V0cy9pbWFnZXMvTWlrcm90aWtfbG9nby5zdmdcIixcblx0XCIuL1dhdGNoZ3VhcmRfbG9nby5zdmdcIjogXCIuL2Fzc2V0cy9pbWFnZXMvV2F0Y2hndWFyZF9sb2dvLnN2Z1wiLFxuXHRcIi4vZmF2aWNvbi9hbmRyb2lkLWNocm9tZS0xOTJ4MTkyLnBuZ1wiOiBcIi4vYXNzZXRzL2ltYWdlcy9mYXZpY29uL2FuZHJvaWQtY2hyb21lLTE5MngxOTIucG5nXCIsXG5cdFwiLi9mYXZpY29uL2FuZHJvaWQtY2hyb21lLTUxMng1MTIucG5nXCI6IFwiLi9hc3NldHMvaW1hZ2VzL2Zhdmljb24vYW5kcm9pZC1jaHJvbWUtNTEyeDUxMi5wbmdcIixcblx0XCIuL2Zhdmljb24vYXBwbGUtdG91Y2gtaWNvbi5wbmdcIjogXCIuL2Fzc2V0cy9pbWFnZXMvZmF2aWNvbi9hcHBsZS10b3VjaC1pY29uLnBuZ1wiLFxuXHRcIi4vZmF2aWNvbi9mYXZpY29uLTE2eDE2LnBuZ1wiOiBcIi4vYXNzZXRzL2ltYWdlcy9mYXZpY29uL2Zhdmljb24tMTZ4MTYucG5nXCIsXG5cdFwiLi9mYXZpY29uL2Zhdmljb24tMzJ4MzIucG5nXCI6IFwiLi9hc3NldHMvaW1hZ2VzL2Zhdmljb24vZmF2aWNvbi0zMngzMi5wbmdcIixcblx0XCIuL2Zhdmljb24vZmF2aWNvbi5pY29cIjogXCIuL2Fzc2V0cy9pbWFnZXMvZmF2aWNvbi9mYXZpY29uLmljb1wiXG59O1xuXG5cbmZ1bmN0aW9uIHdlYnBhY2tDb250ZXh0KHJlcSkge1xuXHR2YXIgaWQgPSB3ZWJwYWNrQ29udGV4dFJlc29sdmUocmVxKTtcblx0cmV0dXJuIF9fd2VicGFja19yZXF1aXJlX18oaWQpO1xufVxuZnVuY3Rpb24gd2VicGFja0NvbnRleHRSZXNvbHZlKHJlcSkge1xuXHRpZighX193ZWJwYWNrX3JlcXVpcmVfXy5vKG1hcCwgcmVxKSkge1xuXHRcdHZhciBlID0gbmV3IEVycm9yKFwiQ2Fubm90IGZpbmQgbW9kdWxlICdcIiArIHJlcSArIFwiJ1wiKTtcblx0XHRlLmNvZGUgPSAnTU9EVUxFX05PVF9GT1VORCc7XG5cdFx0dGhyb3cgZTtcblx0fVxuXHRyZXR1cm4gbWFwW3JlcV07XG59XG53ZWJwYWNrQ29udGV4dC5rZXlzID0gZnVuY3Rpb24gd2VicGFja0NvbnRleHRLZXlzKCkge1xuXHRyZXR1cm4gT2JqZWN0LmtleXMobWFwKTtcbn07XG53ZWJwYWNrQ29udGV4dC5yZXNvbHZlID0gd2VicGFja0NvbnRleHRSZXNvbHZlO1xubW9kdWxlLmV4cG9ydHMgPSB3ZWJwYWNrQ29udGV4dDtcbndlYnBhY2tDb250ZXh0LmlkID0gXCIuL2Fzc2V0cy9pbWFnZXMgc3luYyByZWN1cnNpdmUgXFxcXC4ocG5nJTdDanBnJTdDanBlZyU3Q2dpZiU3Q2ljbyU3Q3N2ZyU3Q3dlYnApJFwiOyIsInJlcXVpcmUoJ2pxdWVyeScpO1xuXG5jb25zdCBpbWFnZXNDb250ZXh0ID0gcmVxdWlyZS5jb250ZXh0KCcuLi9pbWFnZXMnLCB0cnVlLCAvXFwuKHBuZ3xqcGd8anBlZ3xnaWZ8aWNvfHN2Z3x3ZWJwKSQvKTtcbmltYWdlc0NvbnRleHQua2V5cygpLmZvckVhY2goaW1hZ2VzQ29udGV4dCk7XG5cblxuaW1wb3J0IFwiQGZvcnRhd2Vzb21lL2ZvbnRhd2Vzb21lLWZyZWUvanMvYWxsXCI7XG5cbmNvbnN0ICQgPSByZXF1aXJlKCdqcXVlcnknKTtcbmdsb2JhbC4kID0gZ2xvYmFsLmpRdWVyeSA9ICQ7XG5yZXF1aXJlKCdqcXVlcnktdWknKTtcbnJlcXVpcmUoJ2pxdWVyeS11aS91aS93aWRnZXRzL3NvcnRhYmxlJyk7XG5cbnJlcXVpcmUoJy4uL3Njc3MvYm9vdHN0cmFwLWN1c3RvbS5zY3NzJyk7XG53aW5kb3cuYm9vdHN0cmFwID0gcmVxdWlyZSgnYm9vdHN0cmFwJyk7XG5pbXBvcnQge1Rvb2x0aXAsIFRvYXN0LCBQb3BvdmVyLCBNb2RhbH0gZnJvbSAnYm9vdHN0cmFwJztcbnJlcXVpcmUoJ2Zsb2F0dGhlYWQvZGlzdC9qcXVlcnkuZmxvYXRUaGVhZCcpXG5cbnJlcXVpcmUoJ3NlbGVjdDInKTtcblxuaW1wb3J0IENoYXJ0IGZyb20gJ2NoYXJ0LmpzL2F1dG8nO1xuZ2xvYmFsLkNoYXJ0PUNoYXJ0O1xuXG5pbXBvcnQgQ2hhcnREYXRhTGFiZWxzIGZyb20gJ2NoYXJ0anMtcGx1Z2luLWRhdGFsYWJlbHMnO1xuQ2hhcnQucmVnaXN0ZXIoQ2hhcnREYXRhTGFiZWxzKVxuXG5cblxuXG5jb25zdCBwYWxldHRlID0gcmVxdWlyZSgnZ29vZ2xlLXBhbGV0dGUnKVxuZ2xvYmFsLnBhbGV0dGU9cGFsZXR0ZTtcblxuaW1wb3J0IEF1dG9jb21wbGV0ZSBmcm9tICdAdHJldm9yZXlyZS9hdXRvY29tcGxldGUtanMnXG5cbmdsb2JhbC5BdXRvY29tcGxldGUgPSBBdXRvY29tcGxldGU7XG5cbmltcG9ydCAnQHB1Z3gvZmlsdGVyLWJ1bmRsZS9qcy9maWx0ZXInO1xuXG5pbXBvcnQge2NyZWF0ZUFwcH0gZnJvbSAndnVlJztcbmltcG9ydCBTeW5jVGFibGUgZnJvbSBcIi4vY29tcG9uZW50cy9TeW5jVGFibGUudnVlXCI7XG5nbG9iYWwuVnVlQ3JlYXRlQXBwID0gY3JlYXRlQXBwO1xuZ2xvYmFsLlN5bmNUYWJsZSA9IFN5bmNUYWJsZTtcbmdsb2JhbFRoaXMuX19WVUVfUFJPRF9ERVZUT09MU19fID0gcHJvY2Vzcy5lbnYuTk9ERV9FTlYgPT09IFwiZGV2ZWxvcG1lbnRcIiB8fCBwcm9jZXNzLmVudi5OT0RFX0VOViA9PT0gXCJkZXZcIjtcblxuaW1wb3J0IGF4aW9zIGZyb20gXCJheGlvc1wiO1xuZ2xvYmFsLmF4aW9zID0gYXhpb3M7XG5cbiQoZG9jdW1lbnQpLnJlYWR5KGZ1bmN0aW9uICgpIHtcbiAgICAndXNlIHN0cmljdCc7XG5cbiAgICBpZiAoalF1ZXJ5KCkucHVneEZpbHRlcikge1xuICAgICAgICAkKCcjZmlsdGVyJykucHVneEZpbHRlcigpO1xuICAgIH1cblxuICAgIC8qKiBzZWxlY3QyIG5vIGZvY3VzIHdvcmthcm91bmQgKiovXG4gICAgJChkb2N1bWVudCkub24oJ3NlbGVjdDI6b3BlbicsICgpID0+IHtcbiAgICAgICAgbGV0IGFsbEZvdW5kID0gZG9jdW1lbnQucXVlcnlTZWxlY3RvckFsbCgnLnNlbGVjdDItY29udGFpbmVyLS1vcGVuIC5zZWxlY3QyLXNlYXJjaF9fZmllbGQnKTtcbiAgICAgICAgJCh0aGlzKS5vbmUoJ21vdXNldXAga2V5dXAnLCAoKSA9PiB7XG4gICAgICAgICAgICBzZXRUaW1lb3V0KCgpID0+IHtcbiAgICAgICAgICAgICAgICBhbGxGb3VuZFthbGxGb3VuZC5sZW5ndGggLSAxXS5mb2N1cygpO1xuICAgICAgICAgICAgfSwgMCk7XG4gICAgICAgIH0pO1xuICAgIH0pO1xuXG4gICAgJChkb2N1bWVudCkuYWpheEVycm9yKGZ1bmN0aW9uIChldmVudCwganFYSFIpIHtcbiAgICAgICAgaWYgKGpxWEhSLnN0YXR1cyA9PT0gNDAzICYmIGpxWEhSLnJlc3BvbnNlVGV4dCA9PT0gXCJBdXRoZW50aWNhdGlvbiB0aW1lb3V0XCIpIHtcbiAgICAgICAgICAgIHdpbmRvdy5sb2NhdGlvbi5yZWxvYWQoKTtcbiAgICAgICAgfVxuICAgIH0pO1xuXG4gICAgJCgnLnRhYmxlLXJlc3BvbnNpdmUnKS5vbignc2hvd24uYnMuZHJvcGRvd24nLCBmdW5jdGlvbiAoZSkge1xuICAgICAgICBsZXQgJHRhYmxlQ29udCA9ICQodGhpcyksXG4gICAgICAgICAgICAkbWVudUJ0biA9ICQoZS50YXJnZXQpLFxuICAgICAgICAgICAgJG1lbnUgPSAkbWVudUJ0bi5wYXJlbnQoKS5maW5kKFwiLmRyb3Bkb3duLW1lbnVcIiksXG4gICAgICAgICAgICBtZW51QnRuVG9wID0gJG1lbnVCdG4ub2Zmc2V0KCkudG9wIC0gJHRhYmxlQ29udC5vZmZzZXQoKS50b3AsXG4gICAgICAgICAgICBtZW51QnRuQm90dG9tID0gbWVudUJ0blRvcCArICRtZW51QnRuLm91dGVySGVpZ2h0KHRydWUpLFxuICAgICAgICAgICAgbG93ZXJNZW51Qm90dG9tID0gbWVudUJ0bkJvdHRvbSArICRtZW51Lm91dGVySGVpZ2h0KHRydWUpO1xuXG4gICAgICAgIGlmIChtZW51QnRuVG9wIDwgJG1lbnUub3V0ZXJIZWlnaHQodHJ1ZSkgJiYgIGxvd2VyTWVudUJvdHRvbSA+ICR0YWJsZUNvbnQuaGVpZ2h0KCkpXG4gICAgICAgICAgICAkdGFibGVDb250LmNzcyhcInBhZGRpbmctYm90dG9tXCIsICRtZW51Lm91dGVySGVpZ2h0KHRydWUpKTtcbiAgICB9KS5vbignaGlkZS5icy5kcm9wZG93bicsIGZ1bmN0aW9uICgpIHtcbiAgICAgICAgJCh0aGlzKS5jc3MoXCJwYWRkaW5nLWJvdHRvbVwiLCAwKTtcbiAgICB9KTtcblxufSk7XG4iLCI8dGVtcGxhdGU+XG4gICAgPGRpdiB2LWlmPVwiZW50aXRpZXMgIT09IHVuZGVmaW5lZCAmJiBPYmplY3Qua2V5cyhlbnRpdGllcykubGVuZ3RoID4gMFwiPlxuICAgICAgICA8ZGl2IGNsYXNzPVwidGFibGUtcmVzcG9uc2l2ZVwiPlxuICAgICAgICAgICAgPHRhYmxlIGNsYXNzPVwidGFibGUgdGFibGUtc3RyaXBlZFwiPlxuICAgICAgICAgICAgICAgIDx0cj5cbiAgICAgICAgICAgICAgICAgICAgPHRoPjxpbnB1dCB0eXBlPVwiY2hlY2tib3hcIiA6Y2hlY2tlZD1cImFsbENoZWNrZWRcIiBAaW5wdXQ9XCJ0b2dnbGVBbGxDaGVja2JveFwiIGNsYXNzPVwiZm9ybS1jaGVjay1pbnB1dFwiPjwvdGg+XG4gICAgICAgICAgICAgICAgICAgIDx0aD5Ob21lPC90aD5cbiAgICAgICAgICAgICAgICAgICAgPHRoIHYtZm9yPVwiY29sdW1uIGluIGV4dHJhQ29sdW1uc1wiPnt7Y29sdW1uLm5hbWV9fTwvdGg+XG4gICAgICAgICAgICAgICAgICAgIDx0aD5TdGF0bzwvdGg+XG4gICAgICAgICAgICAgICAgICAgIDx0aD5BemlvbmU8L3RoPlxuICAgICAgICAgICAgICAgIDwvdHI+XG4gICAgICAgICAgICAgICAgPHN5bmN0YWJsZS10clxuICAgICAgICAgICAgICAgICAgICAgICAgdi1mb3I9XCIoZW50aXR5LCBpZCkgaW4gZW50aXRpZXNcIlxuICAgICAgICAgICAgICAgICAgICAgICAgOmVudGl0eT1cImVudGl0eVwiXG4gICAgICAgICAgICAgICAgICAgICAgICA6ZW50aXR5LXR5cGU9XCJlbnRpdHlUeXBlXCJcbiAgICAgICAgICAgICAgICAgICAgICAgIDprZXk9XCJpZFwiXG4gICAgICAgICAgICAgICAgICAgICAgICA6aWQ9XCJTdHJpbmcoaWQpXCJcbiAgICAgICAgICAgICAgICAgICAgICAgIDpjaGVja2VkPVwiY2hlY2tlZC5pbmRleE9mKFN0cmluZyhpZCkpID4gLTFcIlxuICAgICAgICAgICAgICAgICAgICAgICAgOmV4dHJhLWNvbHVtbnM9XCJleHRyYUNvbHVtbnNcIlxuICAgICAgICAgICAgICAgICAgICAgICAgOnNob3ctbWFpbi1hY3Rpb24tYnRuPVwic2hvd01haW5BY3Rpb25cIlxuICAgICAgICAgICAgICAgICAgICAgICAgOnNob3ctZGVsZXRlLWJ0bj1cInNob3dEZWxldGVBY3Rpb25cIlxuICAgICAgICAgICAgICAgICAgICAgICAgOm1haW4tYWN0aW9uLWJ0bi1sYWJlbD1cIm1haW5BY3Rpb25MYWJlbFwiXG4gICAgICAgICAgICAgICAgICAgICAgICA6aW5pdGlhbC1zdGF0ZS1sYWJlbD1cImluaXRpYWxTdGF0ZUxhYmVsXCJcbiAgICAgICAgICAgICAgICAgICAgICAgIEBtYWluLWJ0bi1jbGljaz1cInF1ZXVlQWN0aW9uKCRldmVudClcIlxuICAgICAgICAgICAgICAgICAgICAgICAgQGRlbGV0ZS1idG4tY2xpY2s9XCJkZWxldGVFbnRpdHkoJGV2ZW50KVwiXG4gICAgICAgICAgICAgICAgICAgICAgICBAY2hlY2tib3hjbGlja2VkPVwiY2hlY2tib3hDbGlja2VkXCJcbiAgICAgICAgICAgICAgICAgICAgICAgIHJlZj1cImNoaWxkcmVuXCJcbiAgICAgICAgICAgICAgICA+PC9zeW5jdGFibGUtdHI+XG4gICAgICAgICAgICA8L3RhYmxlPlxuICAgICAgICA8L2Rpdj5cbiAgICAgICAgPGJ1dHRvbiBjbGFzcz1cImJ0biBidG4tcHJpbWFyeVwiIHYtaWY9XCJzaG93TWFpbkFjdGlvblwiIDpkaXNhYmxlZD1cImNoZWNrZWQubGVuZ3RoIDwgMVwiIEBjbGljaz1cInF1ZXVlU2VsZWN0ZWRGb3JEZWZhdWx0QWN0aW9uXCI+e3sgbWFpbkFjdGlvbkxhYmVsIH19IHNlbGV6aW9uYXRpPC9idXR0b24+XG4gICAgICAgIDxidXR0b24gY2xhc3M9XCJidG4gYnRuLWRhbmdlciBtcy0yXCIgdi1pZj1cInNob3dEZWxldGVBY3Rpb25cIiA6ZGlzYWJsZWQ9XCJjaGVja2VkLmxlbmd0aCA8IDFcIiBAY2xpY2s9XCJkZWxldGVTZWxlY3RlZFwiPkVsaW1pbmEgc2VsZXppb25hdGk8L2J1dHRvbj5cblxuICAgICAgICA8dGVsZXBvcnQgdG89XCJib2R5XCI+XG4gICAgICAgIDxkaXYgY2xhc3M9XCJtb2RhbFwiIHRhYmluZGV4PVwiLTFcIiA6Y2xhc3M9XCJ7c2hvdzogZW50aXRpZXNUb0RlbGV0ZS5sZW5ndGggPiAwLCAnZC1ibG9jayc6IGVudGl0aWVzVG9EZWxldGUubGVuZ3RoID4gMH1cIj5cbiAgICAgICAgICAgIDxkaXYgY2xhc3M9XCJtb2RhbC1kaWFsb2dcIj5cbiAgICAgICAgICAgICAgICA8ZGl2IGNsYXNzPVwibW9kYWwtY29udGVudFwiPlxuICAgICAgICAgICAgICAgICAgICA8ZGl2IGNsYXNzPVwibW9kYWwtaGVhZGVyXCI+XG4gICAgICAgICAgICAgICAgICAgICAgICA8aDUgY2xhc3M9XCJtb2RhbC10aXRsZVwiPk9wemlvbmkgcGVyIGwnZWxpbWluYXppb25lPC9oNT5cbiAgICAgICAgICAgICAgICAgICAgICAgIDxidXR0b24gdHlwZT1cImJ1dHRvblwiIGNsYXNzPVwiYnRuLWNsb3NlXCIgYXJpYS1sYWJlbD1cIkNsb3NlXCIgQGNsaWNrPVwiZW50aXRpZXNUb0RlbGV0ZT1bXVwiPjwvYnV0dG9uPlxuICAgICAgICAgICAgICAgICAgICA8L2Rpdj5cbiAgICAgICAgICAgICAgICAgICAgPGRpdiBjbGFzcz1cIm1vZGFsLWJvZHlcIj5cbiAgICAgICAgICAgICAgICAgICAgICAgIDxmb3JtIGNsYXNzPVwicm93IHJvdy1jb2xzLWxnLWF1dG8gZy0zIGFsaWduLWl0ZW1zLWNlbnRlclwiPlxuICAgICAgICAgICAgICAgICAgICAgICAgICAgIDxmb3JtIGNsYXNzPVwiZm9ybVwiPlxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICA8ZGl2IGNsYXNzPVwiZm9ybS1jaGVjayBtYi0wXCIgdi1pZj1cImVudGl0eVR5cGUgPT09ICdzdHVkZW50cycgfHwgZW50aXR5VHlwZSA9PT0gJ3RlYWNoZXJzJyB8fCBlbnRpdHlUeXBlID09PSAnZ3JvdXBzJ1wiPlxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgPGlucHV0IHR5cGU9XCJjaGVja2JveFwiIGNsYXNzPVwiZm9ybS1jaGVjay1pbnB1dFwiIDppZD1cIidzeW5jQ29tcC1kZWxldGVMZGFwJytlbnRpdHlUeXBlXCIgOmNoZWNrZWQ9XCJkZWxldGVPcHRpb25zLmRlbGV0ZU9uTGRhcFwiIEBpbnB1dD1cIihldmVudCkgPT4gc2V0RGVsZXRlT3B0aW9uKCdkZWxldGVPbkxkYXAnLGV2ZW50LnRhcmdldC5jaGVja2VkKVwiPlxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgPGxhYmVsIGNsYXNzPVwiZm9ybS1jaGVjay1sYWJlbFwiIDpmb3I9XCInc3luY0NvbXAtZGVsZXRlTGRhcCcrZW50aXR5VHlwZVwiPkVsaW1pbmEgZGEgTGRhcDwvbGFiZWw+XG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIDwvZGl2PlxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICA8ZGl2IGNsYXNzPVwiZm9ybS1jaGVjayBtYi0wIHBsLTBcIiB2LWlmPVwiZW50aXR5VHlwZSA9PT0gJ2dyb3VwcydcIj5cbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIDxpbnB1dCB0eXBlPVwiY2hlY2tib3hcIiBjbGFzcz1cImZvcm0tY2hlY2staW5wdXRcIiA6aWQ9XCInc3luY0NvbXAtZGVsZXRlR2FwcHMnK2VudGl0eVR5cGVcIiA6Y2hlY2tlZD1cImRlbGV0ZU9wdGlvbnMuZGVsZXRlT25HQXBwcz09PScxJ1wiIEBpbnB1dD1cIihldmVudCkgPT4gc2V0RGVsZXRlT3B0aW9uKCdkZWxldGVPbkdBcHBzJyxldmVudC50YXJnZXQuY2hlY2tlZD8nMSc6JzAnKVwiPlxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgPGxhYmVsIGNsYXNzPVwiZm9ybS1jaGVjay1sYWJlbFwiIDpmb3I9XCInc3luY0NvbXAtZGVsZXRlR2FwcHMnK2VudGl0eVR5cGVcIj5FbGltaW5hIGRhIEdvb2dsZSBBcHBzPC9sYWJlbD5cbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgPC9kaXY+XG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIDxkaXYgY2xhc3M9XCJkLWZsZXggZ2FwLTIgYWxpZ24taXRlbXMtY2VudGVyXCIgdi1pZj1cImVudGl0eVR5cGUgPT09ICdzdHVkZW50cycgfHwgZW50aXR5VHlwZSA9PT0gJ3RlYWNoZXJzJ1wiPlxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgPGxhYmVsIGNsYXNzPVwidGV4dC1ub3dyYXBcIiA6Zm9yPVwiJ2RlbGV0ZU9uR0FwcHMnK2VudGl0eVR5cGVcIj5FbGltaW5hIHN1IEdvb2dsZSBBcHBzPC9sYWJlbD5cbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIDxzZWxlY3QgY2xhc3M9XCJmb3JtLXNlbGVjdFwiIDppZD1cIidkZWxldGVPbkdBcHBzJytlbnRpdHlUeXBlXCIgOnZhbHVlPVwiZGVsZXRlT3B0aW9ucy5kZWxldGVPbkdBcHBzXCIgQGlucHV0PVwiKGV2ZW50KSA9PiBzZXREZWxldGVPcHRpb24oJ2RlbGV0ZU9uR0FwcHMnLGV2ZW50LnRhcmdldC52YWx1ZSlcIj5cbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICA8b3B0aW9uIHZhbHVlPVwiMFwiPk5vPC9vcHRpb24+XG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgPG9wdGlvbiB2YWx1ZT1cIjFcIj5TaTwvb3B0aW9uPlxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIDxvcHRpb24gdmFsdWU9XCIyXCI+U3Bvc3RhIGluIE9VPC9vcHRpb24+XG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICA8L3NlbGVjdD5cbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgPC9kaXY+XG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIDxkaXYgY2xhc3M9XCJkLWZsZXggZ2FwLTIgYWxpZ24taXRlbXMtY2VudGVyXCIgdi1pZj1cIihlbnRpdHlUeXBlID09PSAnc3R1ZGVudHMnIHx8IGVudGl0eVR5cGUgPT09ICd0ZWFjaGVycycpICYmIGRlbGV0ZU9wdGlvbnMuZGVsZXRlT25HQXBwcyA9PT0gJzInXCI+XG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICA8bGFiZWwgOmZvcj1cIidnQXBwc091JytlbnRpdHlUeXBlXCI+T1U8L2xhYmVsPlxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgPGlucHV0IHR5cGU9XCJ0ZXh0XCIgY2xhc3M9XCJmb3JtLWNvbnRyb2xcIiA6aWQ9XCInZ0FwcHNPdScrZW50aXR5VHlwZVwiIDp2YWx1ZT1cImRlbGV0ZU9wdGlvbnMuZ0FwcHNPdVwiIEBpbnB1dD1cIihldmVudCkgPT4gc2V0RGVsZXRlT3B0aW9uKCdnQXBwc091JyxldmVudC50YXJnZXQudmFsdWUpXCI+XG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIDwvZGl2PlxuICAgICAgICAgICAgICAgICAgICAgICAgICAgIDwvZm9ybT5cbiAgICAgICAgICAgICAgICAgICAgICAgIDwvZm9ybT5cbiAgICAgICAgICAgICAgICAgICAgPC9kaXY+XG4gICAgICAgICAgICAgICAgICAgIDxkaXYgY2xhc3M9XCJtb2RhbC1mb290ZXJcIj5cbiAgICAgICAgICAgICAgICAgICAgICAgIDxidXR0b24gdHlwZT1cImJ1dHRvblwiIGNsYXNzPVwiYnRuIGJ0bi1zZWNvbmRhcnlcIiBAY2xpY2s9XCJlbnRpdGllc1RvRGVsZXRlPVtdXCI+QW5udWxsYTwvYnV0dG9uPlxuICAgICAgICAgICAgICAgICAgICAgICAgPGJ1dHRvbiB0eXBlPVwiYnV0dG9uXCIgY2xhc3M9XCJidG4gYnRuLWRhbmdlclwiIEBjbGljaz1cInF1ZXVlRGVsZXRlQWN0aW9uc1wiPkVsaW1pbmE8L2J1dHRvbj5cbiAgICAgICAgICAgICAgICAgICAgPC9kaXY+XG4gICAgICAgICAgICAgICAgPC9kaXY+XG4gICAgICAgICAgICA8L2Rpdj5cbiAgICAgICAgPC9kaXY+XG4gICAgICAgIDwvdGVsZXBvcnQ+XG4gICAgPC9kaXY+XG48L3RlbXBsYXRlPlxuXG48c2NyaXB0PlxuaW1wb3J0IFN5bmNUYWJsZVRyIGZyb20gXCIuL1N5bmNUYWJsZVRyLnZ1ZVwiO1xuaW1wb3J0IGF4aW9zIGZyb20gXCJheGlvc1wiO1xuXG5leHBvcnQgZGVmYXVsdCB7XG4gICAgbmFtZTogXCJTeW5jVGFibGVcIixcbiAgICBwcm9wczoge1xuICAgICAgICBlbnRpdGllczoge3R5cGU6IE9iamVjdH0sXG4gICAgICAgIGVudGl0eVR5cGU6IHt0eXBlOiBTdHJpbmd9LFxuICAgICAgICBkZWZhdWx0QWN0aW9uOiB7dHlwZTogU3RyaW5nfSxcbiAgICAgICAgZGVmYXVsdERlbGV0ZU9wdGlvbnM6IHt0eXBlOiBPYmplY3R9LFxuICAgICAgICB1cmw6IHt0eXBlOiBTdHJpbmd9LFxuICAgICAgICBkZWxldGVVcmw6IHt0eXBlOiBTdHJpbmcsIGRlZmF1bHQ6IG51bGx9LFxuICAgICAgICBtYWluQWN0aW9uTGFiZWw6IHt0eXBlOiBTdHJpbmcsIGRlZmF1bHQ6ICdFc2VndWknfSxcbiAgICAgICAgc2hvd01haW5BY3Rpb246IHt0eXBlOiBCb29sZWFuLCBkZWZhdWx0OiB0cnVlfSxcbiAgICAgICAgc2hvd0RlbGV0ZUFjdGlvbjoge3R5cGU6IEJvb2xlYW4sIGRlZmF1bHQ6IGZhbHNlfSxcbiAgICAgICAgZXh0cmFDb2x1bW5zOiB7dHlwZTogQXJyYXksIGRlZmF1bHQ6IFtdfSxcbiAgICAgICAgaW5pdGlhbFN0YXRlTGFiZWw6IHt0eXBlOiBTdHJpbmcsIGRlZmF1bHQ6ICctLS0nfVxuICAgIH0sXG4gICAgZGF0YTogZnVuY3Rpb24gKCkge1xuICAgICAgICByZXR1cm4ge1xuICAgICAgICAgICAgY2hlY2tlZDogW10sXG4gICAgICAgICAgICBhbGxDaGVja2VkOiBmYWxzZSxcbiAgICAgICAgICAgIGRlbGV0ZU9wdGlvbnM6IHsuLi50aGlzLmRlZmF1bHREZWxldGVPcHRpb25zfSxcbiAgICAgICAgICAgIGV4ZWN1dGlvblF1ZXVlOiBbXSxcbiAgICAgICAgICAgIHF1ZXVlV29ya2luZzogZmFsc2UsXG4gICAgICAgICAgICBlbnRpdGllc1RvRGVsZXRlOiBbXVxuICAgICAgICB9XG4gICAgfSxcbiAgICBtZXRob2RzOiB7XG4gICAgICAgIGNoZWNrYm94Q2xpY2tlZDogZnVuY3Rpb24gKGlkKSB7XG4gICAgICAgICAgICB2YXIgcG9zID0gdGhpcy5jaGVja2VkLmluZGV4T2YoaWQpO1xuICAgICAgICAgICAgaWYgKHBvcyA+IC0xKSB7XG4gICAgICAgICAgICAgICAgdGhpcy5jaGVja2VkLnNwbGljZShwb3MsIDEpO1xuICAgICAgICAgICAgfSBlbHNlIHtcbiAgICAgICAgICAgICAgICB0aGlzLmNoZWNrZWQucHVzaChpZCk7XG4gICAgICAgICAgICB9XG4gICAgICAgIH0sXG4gICAgICAgIHRvZ2dsZUFsbENoZWNrYm94OiBmdW5jdGlvbiAoKSB7XG4gICAgICAgICAgICB0aGlzLmNoZWNrZWQgPSBbXTtcbiAgICAgICAgICAgIGlmICghdGhpcy5hbGxDaGVja2VkKSB7XG4gICAgICAgICAgICAgICAgZm9yIChsZXQgY2hpbGQgb2YgdGhpcy4kcmVmcy5jaGlsZHJlbikge1xuICAgICAgICAgICAgICAgICAgICBpZiAoIWNoaWxkLmRpc2FibGVkKSB7XG4gICAgICAgICAgICAgICAgICAgICAgICB0aGlzLmNoZWNrZWQucHVzaChjaGlsZC5pZCk7XG4gICAgICAgICAgICAgICAgICAgIH1cbiAgICAgICAgICAgICAgICB9XG4gICAgICAgICAgICB9XG4gICAgICAgICAgICB0aGlzLmFsbENoZWNrZWQgPSAhdGhpcy5hbGxDaGVja2VkO1xuICAgICAgICB9LFxuICAgICAgICBjYWxsQWN0aW9uOiBmdW5jdGlvbiAocGF5bG9hZCkge1xuICAgICAgICAgICAgaWYgKHR5cGVvZiBwYXlsb2FkICE9PSAnb2JqZWN0JyB8fFxuICAgICAgICAgICAgICAgIHBheWxvYWQuZW50aXR5SWQgPT09IHVuZGVmaW5lZCB8fFxuICAgICAgICAgICAgICAgIHBheWxvYWQuZW50aXR5VHlwZSA9PT0gdW5kZWZpbmVkIHx8XG4gICAgICAgICAgICAgICAgcGF5bG9hZC5lbnRpdHkgPT09IHVuZGVmaW5lZCB8fFxuICAgICAgICAgICAgICAgIHBheWxvYWQuYWN0aW9uID09PSB1bmRlZmluZWRcbiAgICAgICAgICAgICkge1xuICAgICAgICAgICAgICAgIGNvbnNvbGUud2FybihcIkludmFsaWQgY2FsbCBhY3Rpb24gcGF5bG9hZFwiLCBwYXlsb2FkKTtcbiAgICAgICAgICAgICAgICByZXR1cm47XG4gICAgICAgICAgICB9XG5cbiAgICAgICAgICAgIHBheWxvYWQuZW50aXR5LnN0YXRlID0gMjtcblxuICAgICAgICAgICAgbGV0IHVybCA9IHRoaXMudXJsO1xuICAgICAgICAgICAgaWYocGF5bG9hZC5hY3Rpb24gPT09IFwiZGVsZXRlXCIpIHtcbiAgICAgICAgICAgICAgICB1cmwgPSB0aGlzLmRlbGV0ZVVybDtcbiAgICAgICAgICAgIH1cblxuICAgICAgICAgICAgaWYgKHVybCA9PT0gXCJcIikge1xuICAgICAgICAgICAgICAgIHJldHVybjtcbiAgICAgICAgICAgIH1cblxuICAgICAgICAgICAgaWYodHlwZW9mIHBheWxvYWQuZW50aXR5LmRhdGEucHJvdmlkZXJJZCAhPT0gXCJ1bmRlZmluZWRcIikge1xuICAgICAgICAgICAgICAgIHVybCA9IHVybC5yZXBsYWNlKFwiKnByb3ZpZGVySWQqXCIscGF5bG9hZC5lbnRpdHkuZGF0YS5wcm92aWRlcklkKTtcbiAgICAgICAgICAgIH1cblxuICAgICAgICAgICAgY29uc3QgJHRoaXMgPSB0aGlzO1xuICAgICAgICAgICAgbGV0IGRhdGEgPSB7XG4gICAgICAgICAgICAgICAgZW50aXR5RGF0YTogcGF5bG9hZC5lbnRpdHkuZGF0YSxcbiAgICAgICAgICAgICAgICBlbnRpdHlJZDogcGF5bG9hZC5lbnRpdHlJZCxcbiAgICAgICAgICAgICAgICBlbnRpdHlUeXBlOiBwYXlsb2FkLmVudGl0eVR5cGUsXG4gICAgICAgICAgICB9O1xuXG4gICAgICAgICAgICBpZihwYXlsb2FkLmFjdGlvbiA9PT0gXCJkZWxldGVcIikge1xuICAgICAgICAgICAgICAgIGlmKHBheWxvYWQuZGVsZXRlT3B0aW9ucyA9PT0gdW5kZWZpbmVkKSB7XG4gICAgICAgICAgICAgICAgICAgIGNvbnNvbGUud2FybihcIkludmFsaWQgY2FsbCBhY3Rpb24gcGF5bG9hZCBvcHRpb25zXCIsIHBheWxvYWQpO1xuICAgICAgICAgICAgICAgICAgICByZXR1cm47XG4gICAgICAgICAgICAgICAgfVxuICAgICAgICAgICAgICAgIGRhdGFbJ29wdGlvbnMnXSA9IHBheWxvYWQuZGVsZXRlT3B0aW9ucztcbiAgICAgICAgICAgIH1cblxuICAgICAgICAgICAgYXhpb3MucG9zdCh1cmwsIGRhdGEse1xuICAgICAgICAgICAgICAgIGhlYWRlcnM6IHtcbiAgICAgICAgICAgICAgICAgICAgJ0NvbnRlbnQtVHlwZSc6ICdtdWx0aXBhcnQvZm9ybS1kYXRhJ1xuICAgICAgICAgICAgICAgIH1cbiAgICAgICAgICAgIH0pLnRoZW4oZnVuY3Rpb24gKHJlc3BvbnNlKSB7XG4gICAgICAgICAgICAgICAgaWYgKHR5cGVvZiByZXNwb25zZS5kYXRhICE9PSBcIm9iamVjdFwiIHx8IHJlc3BvbnNlLmRhdGEuc3VjY2VzcyAhPT0gdHJ1ZSkge1xuICAgICAgICAgICAgICAgICAgICB0aHJvdyB0eXBlb2YgcmVzcG9uc2UuZGF0YSA9PT0gJ29iamVjdCcgJiYgcmVzcG9uc2UuZGF0YS5lcnJvciAhPT0gdW5kZWZpbmVkID8gcmVzcG9uc2UuZGF0YS5lcnJvciA6IG51bGw7XG4gICAgICAgICAgICAgICAgfVxuXG4gICAgICAgICAgICAgICAgcGF5bG9hZC5lbnRpdHkuc3RhdGUgPSAzO1xuICAgICAgICAgICAgICAgIGlmIChyZXNwb25zZS5kYXRhLm5ld0VudGl0eUlkICE9PSB1bmRlZmluZWQpIHtcbiAgICAgICAgICAgICAgICAgICAgdGhpcy4kZW1pdCgnbmV3RW50aXR5SW5zZXJ0ZWQnLCB7Li4ucGF5bG9hZCwgbmV3RW50aXR5SWQ6IHJlc3BvbnNlLmRhdGEubmV3RW50aXR5SWR9KTtcbiAgICAgICAgICAgICAgICB9XG5cbiAgICAgICAgICAgICAgICAkdGhpcy5xdWV1ZVdvcmtpbmcgPSBmYWxzZTtcbiAgICAgICAgICAgICAgICAkdGhpcy50aWNrUXVldWUoKTtcbiAgICAgICAgICAgIH0pLmNhdGNoKGZ1bmN0aW9uIChlcnIpIHtcbiAgICAgICAgICAgICAgICBwYXlsb2FkLmVudGl0eS5lcnJvciA9IHR5cGVvZiBlcnIgPT09IFwic3RyaW5nXCIgPyBlcnIgOiBcIkltcG9zc2liaWxlIGVsYWJvcmFyZSBsYSByaWNoaWVzdGFcIjtcbiAgICAgICAgICAgICAgICBwYXlsb2FkLmVudGl0eS5zdGF0ZSA9IDA7XG4gICAgICAgICAgICAgICAgJHRoaXMucXVldWVXb3JraW5nID0gZmFsc2U7XG4gICAgICAgICAgICAgICAgJHRoaXMudGlja1F1ZXVlKCk7XG4gICAgICAgICAgICB9KTtcbiAgICAgICAgfSxcbiAgICAgICAgcXVldWVTZWxlY3RlZEZvckRlZmF1bHRBY3Rpb246IGZ1bmN0aW9uICgpIHtcbiAgICAgICAgICAgIGZvciAobGV0IGVudGl0eUlkSWR4IGluIHRoaXMuY2hlY2tlZCkge1xuICAgICAgICAgICAgICAgIHRoaXMucXVldWVBY3Rpb24odGhpcy5jaGVja2VkW2VudGl0eUlkSWR4XSk7XG4gICAgICAgICAgICB9XG5cbiAgICAgICAgICAgIHRoaXMuY2hlY2tlZCA9IFtdO1xuICAgICAgICB9LFxuICAgICAgICBkZWxldGVFbnRpdHkoZW50aXR5SWQpIHtcbiAgICAgICAgICAgIGlmKHRoaXMuZW50aXR5VHlwZSA9PT0gJ3N0dWRlbnRzJyB8fCB0aGlzLmVudGl0eVR5cGUgPT09ICd0ZWFjaGVycycgfHwgdGhpcy5lbnRpdHlUeXBlID09PSAnZ3JvdXBzJykge1xuICAgICAgICAgICAgICAgIHRoaXMuZW50aXRpZXNUb0RlbGV0ZSA9IFtlbnRpdHlJZF07XG4gICAgICAgICAgICAgICAgcmV0dXJuO1xuICAgICAgICAgICAgfVxuXG4gICAgICAgICAgICB0aGlzLnF1ZXVlQWN0aW9uKGVudGl0eUlkLCAnZGVsZXRlJyk7XG4gICAgICAgIH0sXG4gICAgICAgIGRlbGV0ZVNlbGVjdGVkKCkge1xuICAgICAgICAgICAgaWYodGhpcy5lbnRpdHlUeXBlID09PSAnc3R1ZGVudHMnIHx8IHRoaXMuZW50aXR5VHlwZSA9PT0gJ3RlYWNoZXJzJyB8fCB0aGlzLmVudGl0eVR5cGUgPT09ICdncm91cHMnKSB7XG4gICAgICAgICAgICAgICAgdGhpcy5lbnRpdGllc1RvRGVsZXRlID0gdGhpcy5jaGVja2VkO1xuICAgICAgICAgICAgICAgIHJldHVybjtcbiAgICAgICAgICAgIH1cblxuICAgICAgICAgICAgZm9yIChsZXQgZW50aXR5SWQgb2YgdGhpcy5jaGVja2VkKSB7XG4gICAgICAgICAgICAgICAgdGhpcy5xdWV1ZUFjdGlvbihlbnRpdHlJZCwgJ2RlbGV0ZScpO1xuICAgICAgICAgICAgfVxuICAgICAgICAgICAgdGhpcy5jaGVja2VkID0gW107XG4gICAgICAgIH0sXG4gICAgICAgIHF1ZXVlRGVsZXRlQWN0aW9ucygpIHtcbiAgICAgICAgICAgIGZvciAobGV0IGVudGl0eUlkIG9mIHRoaXMuZW50aXRpZXNUb0RlbGV0ZSkge1xuICAgICAgICAgICAgICAgIHRoaXMucXVldWVBY3Rpb24oZW50aXR5SWQsICdkZWxldGUnKTtcbiAgICAgICAgICAgIH1cblxuICAgICAgICAgICAgdGhpcy5lbnRpdGllc1RvRGVsZXRlID0gW107XG4gICAgICAgICAgICB0aGlzLmNoZWNrZWQgPSBbXTtcbiAgICAgICAgfSxcbiAgICAgICAgcXVldWVBY3Rpb246IGZ1bmN0aW9uIChlbnRpdHlJZCwgYWN0aW9uPW51bGwpIHtcbiAgICAgICAgICAgIGlmICh0aGlzLmVudGl0aWVzW2VudGl0eUlkXS5zdGF0ZSA+IDApIHtcbiAgICAgICAgICAgICAgICByZXR1cm47XG4gICAgICAgICAgICB9XG5cbiAgICAgICAgICAgIHRoaXMuZW50aXRpZXNbZW50aXR5SWRdLnN0YXRlID0gMTtcbiAgICAgICAgICAgIHRoaXMuZW50aXRpZXNbZW50aXR5SWRdLmVycm9yID0gbnVsbDtcblxuICAgICAgICAgICAgdGhpcy5leGVjdXRpb25RdWV1ZS5wdXNoKHtcbiAgICAgICAgICAgICAgICBhY3Rpb246IGFjdGlvbiAhPT0gbnVsbCA/IGFjdGlvbiA6IHRoaXMuZGVmYXVsdEFjdGlvbixcbiAgICAgICAgICAgICAgICBlbnRpdHlJZDogZW50aXR5SWQsXG4gICAgICAgICAgICAgICAgZW50aXR5VHlwZTogdGhpcy5lbnRpdHlUeXBlLFxuICAgICAgICAgICAgICAgIGVudGl0eTogdGhpcy5lbnRpdGllc1tlbnRpdHlJZF0sXG4gICAgICAgICAgICAgICAgZGVsZXRlT3B0aW9uczogdGhpcy5kZWxldGVPcHRpb25zXG4gICAgICAgICAgICB9KTtcblxuICAgICAgICAgICAgdGhpcy50aWNrUXVldWUoKTtcbiAgICAgICAgfSxcbiAgICAgICAgdGlja1F1ZXVlOiBmdW5jdGlvbiAoKSB7XG4gICAgICAgICAgICBpZiAodGhpcy5xdWV1ZVdvcmtpbmcgfHwgdGhpcy5leGVjdXRpb25RdWV1ZS5sZW5ndGggPCAxKSB7XG4gICAgICAgICAgICAgICAgcmV0dXJuO1xuICAgICAgICAgICAgfVxuXG4gICAgICAgICAgICBjb25zdCAkdGhpcyA9IHRoaXM7XG4gICAgICAgICAgICAkdGhpcy5xdWV1ZVdvcmtpbmcgPSB0cnVlO1xuXG4gICAgICAgICAgICBzZXRUaW1lb3V0KGZ1bmN0aW9uICgpIHtcbiAgICAgICAgICAgICAgICAkdGhpcy5jYWxsQWN0aW9uKCR0aGlzLmV4ZWN1dGlvblF1ZXVlLnNoaWZ0KCkpO1xuICAgICAgICAgICAgfSwgMCk7XG4gICAgICAgIH0sXG4gICAgICAgIHNldERlbGV0ZU9wdGlvbihrZXksIHZhbHVlKSB7XG4gICAgICAgICAgICB0aGlzLmRlbGV0ZU9wdGlvbnNba2V5XSA9IHZhbHVlO1xuICAgICAgICB9XG4gICAgfSxcbiAgICBjb21wb25lbnRzOiB7XG4gICAgICAgICdzeW5jdGFibGUtdHInOiBTeW5jVGFibGVUclxuICAgIH1cbn1cbjwvc2NyaXB0PlxuXG48c3R5bGUgbGFuZz1cInNjc3NcIiBzY29wZWQ+XG5cbjwvc3R5bGU+IiwiPHRlbXBsYXRlPlxuICAgIDx0cj5cbiAgICAgICAgPHRkPjxpbnB1dCB0eXBlPVwiY2hlY2tib3hcIiA6Y2hlY2tlZD1cImNoZWNrZWRcIiBAaW5wdXQ9XCIkZW1pdCgnY2hlY2tib3hjbGlja2VkJywgaWQpXCIgOmRpc2FibGVkPVwiZGlzYWJsZWRcIiBjbGFzcz1cImZvcm0tY2hlY2staW5wdXRcIiAvPjwvdGQ+XG4gICAgICAgIDx0ZD57eyBlbnRpdHkuZGF0YS5fbmFtZSB9fTwvdGQ+XG4gICAgICAgIDx0ZCB2LWZvcj1cImNvbHVtbiBpbiBleHRyYUNvbHVtbnNcIj57eyBlbnRpdHkuZGF0YVtjb2x1bW4ua2V5XSA/PyAnJyB9fTwvdGQ+XG4gICAgICAgIDx0ZD57eyBzdGF0ZU1zZyB9fTwvdGQ+XG4gICAgICAgIDx0ZD5cbiAgICAgICAgICAgIDxidXR0b24gQGNsaWNrPVwiJGVtaXQoJ21haW4tYnRuLWNsaWNrJywgaWQpXCIgY2xhc3M9XCJidG4gYnRuLXByaW1hcnlcIiA6ZGlzYWJsZWQ9XCJkaXNhYmxlZFwiIHYtaWY9XCJzaG93TWFpbkFjdGlvbkJ0blwiPnt7bWFpbkFjdGlvbkJ0bkxhYmVsfX08L2J1dHRvbj5cbiAgICAgICAgICAgIDxidXR0b24gQGNsaWNrPVwiJGVtaXQoJ2RlbGV0ZS1idG4tY2xpY2snLCBpZClcIiBjbGFzcz1cImJ0biBidG4tZGFuZ2VyIG1zLTJcIiA6ZGlzYWJsZWQ9XCJkaXNhYmxlZFwiIHYtaWY9XCJzaG93RGVsZXRlQnRuXCI+RWxpbWluYTwvYnV0dG9uPlxuICAgICAgICA8L3RkPlxuICAgIDwvdHI+XG48L3RlbXBsYXRlPlxuXG48c2NyaXB0PlxuZXhwb3J0IGRlZmF1bHQge1xuICAgIG5hbWU6IFwiU3luY1RhYmxlVHJcIixcbiAgICBwcm9wczogWydlbnRpdHknLCAnZW50aXR5VHlwZScsICdjaGVja2VkJywgJ2lkJywgJ2V4dHJhQ29sdW1ucycsICdzaG93RGVsZXRlQnRuJywgJ3Nob3dNYWluQWN0aW9uQnRuJywgJ21haW5BY3Rpb25CdG5MYWJlbCcsICdpbml0aWFsU3RhdGVMYWJlbCddLFxuICAgIGNvbXB1dGVkOiB7XG4gICAgICAgIHN0YXRlTXNnOiBmdW5jdGlvbiAoKSB7XG4gICAgICAgICAgICBpZiAodGhpcy5lbnRpdHkuZXJyb3IgIT09IG51bGwpIHtcbiAgICAgICAgICAgICAgICByZXR1cm4gXCJFcnJvcmU6IFwiICsgdGhpcy5lbnRpdHkuZXJyb3I7XG4gICAgICAgICAgICB9XG5cbiAgICAgICAgICAgIGlmICh0aGlzLmVudGl0eVR5cGUgPT09IFwic3R1ZGVudHNcIiAmJiB0aGlzLmVudGl0eS5kYXRhLmludGVybmFsR3JvdXBJZCA9PT0gbnVsbCkge1xuICAgICAgICAgICAgICAgIHJldHVybiBcIkRpc2FiaWxpdGF0byBwZXIgY2xhc3NlIG5vbiBpbXBvcnRhdGFcIjtcbiAgICAgICAgICAgIH1cblxuICAgICAgICAgICAgaWYgKHRoaXMuZW50aXR5VHlwZSA9PT0gXCJncm91cHNcIiAmJiB0aGlzLmVudGl0eS5kYXRhLmludGVybmFsU2VjdG9ySWQgPT09IG51bGwpIHtcbiAgICAgICAgICAgICAgICByZXR1cm4gXCJEaXNhYmlsaXRhdG8gcGVyIHNldHRvcmUgbm9uIGltcG9ydGF0b1wiO1xuICAgICAgICAgICAgfVxuXG4gICAgICAgICAgICBzd2l0Y2ggKHRoaXMuZW50aXR5LnN0YXRlKSB7XG4gICAgICAgICAgICAgICAgY2FzZSAxOlxuICAgICAgICAgICAgICAgICAgICByZXR1cm4gXCJJbiBDb2RhXCI7XG5cbiAgICAgICAgICAgICAgICBjYXNlIDI6XG4gICAgICAgICAgICAgICAgICAgIHJldHVybiBcIkVsYWJvcmF6aW9uZVwiO1xuXG4gICAgICAgICAgICAgICAgY2FzZSAzOlxuICAgICAgICAgICAgICAgICAgICByZXR1cm4gXCJDb21wbGV0YXRvXCI7XG5cbiAgICAgICAgICAgICAgICBkZWZhdWx0OlxuICAgICAgICAgICAgICAgICAgICByZXR1cm4gdGhpcy5pbml0aWFsU3RhdGVMYWJlbDtcbiAgICAgICAgICAgIH1cbiAgICAgICAgfSxcbiAgICAgICAgZGlzYWJsZWQ6IGZ1bmN0aW9uICgpIHtcbiAgICAgICAgICAgIGlmICh0aGlzLmVudGl0eS5zdGF0ZSA+IDApIHtcbiAgICAgICAgICAgICAgICByZXR1cm4gdHJ1ZTtcbiAgICAgICAgICAgIH1cblxuICAgICAgICAgICAgaWYgKHRoaXMuZW50aXR5VHlwZSA9PT0gXCJzdHVkZW50c1wiICYmIHR5cGVvZiB0aGlzLmVudGl0eS5kYXRhID09PSBcIm9iamVjdFwiICYmIHRoaXMuZW50aXR5LmRhdGEuaW50ZXJuYWxHcm91cElkID09PSBudWxsKSB7XG4gICAgICAgICAgICAgICAgcmV0dXJuIHRydWU7XG4gICAgICAgICAgICB9XG5cbiAgICAgICAgICAgIGlmICh0aGlzLmVudGl0eVR5cGUgPT09IFwiZ3JvdXBzXCIgJiYgdHlwZW9mIHRoaXMuZW50aXR5LmRhdGEgPT09IFwib2JqZWN0XCIgJiYgdGhpcy5lbnRpdHkuZGF0YS5pbnRlcm5hbFNlY3RvcklkID09PSBudWxsKSB7XG4gICAgICAgICAgICAgICAgcmV0dXJuIHRydWU7XG4gICAgICAgICAgICB9XG5cbiAgICAgICAgICAgIHJldHVybiBmYWxzZTtcbiAgICAgICAgfVxuICAgIH1cbn1cbjwvc2NyaXB0PlxuXG48c3R5bGUgbGFuZz1cInNjc3NcIiBzY29wZWQ+XG5cbjwvc3R5bGU+IiwiLy8gZXh0cmFjdGVkIGJ5IG1pbmktY3NzLWV4dHJhY3QtcGx1Z2luXG5leHBvcnQge307IiwiaW1wb3J0IHsgcmVuZGVyIH0gZnJvbSBcIi4vU3luY1RhYmxlLnZ1ZT92dWUmdHlwZT10ZW1wbGF0ZSZpZD03YTViMzBiY1wiXG5pbXBvcnQgc2NyaXB0IGZyb20gXCIuL1N5bmNUYWJsZS52dWU/dnVlJnR5cGU9c2NyaXB0Jmxhbmc9anNcIlxuZXhwb3J0ICogZnJvbSBcIi4vU3luY1RhYmxlLnZ1ZT92dWUmdHlwZT1zY3JpcHQmbGFuZz1qc1wiXG5cbmltcG9ydCBleHBvcnRDb21wb25lbnQgZnJvbSBcIi4uLy4uLy4uL25vZGVfbW9kdWxlcy92dWUtbG9hZGVyL2Rpc3QvZXhwb3J0SGVscGVyLmpzXCJcbmNvbnN0IF9fZXhwb3J0c19fID0gLyojX19QVVJFX18qL2V4cG9ydENvbXBvbmVudChzY3JpcHQsIFtbJ3JlbmRlcicscmVuZGVyXSxbJ19fZmlsZScsXCJhc3NldHMvanMvY29tcG9uZW50cy9TeW5jVGFibGUudnVlXCJdXSlcbi8qIGhvdCByZWxvYWQgKi9cbmlmIChtb2R1bGUuaG90KSB7XG4gIF9fZXhwb3J0c19fLl9faG1ySWQgPSBcIjdhNWIzMGJjXCJcbiAgY29uc3QgYXBpID0gX19WVUVfSE1SX1JVTlRJTUVfX1xuICBtb2R1bGUuaG90LmFjY2VwdCgpXG4gIGlmICghYXBpLmNyZWF0ZVJlY29yZCgnN2E1YjMwYmMnLCBfX2V4cG9ydHNfXykpIHtcbiAgICBhcGkucmVsb2FkKCc3YTViMzBiYycsIF9fZXhwb3J0c19fKVxuICB9XG4gIFxuICBtb2R1bGUuaG90LmFjY2VwdChcIi4vU3luY1RhYmxlLnZ1ZT92dWUmdHlwZT10ZW1wbGF0ZSZpZD03YTViMzBiY1wiLCAoKSA9PiB7XG4gICAgYXBpLnJlcmVuZGVyKCc3YTViMzBiYycsIHJlbmRlcilcbiAgfSlcblxufVxuXG5cbmV4cG9ydCBkZWZhdWx0IF9fZXhwb3J0c19fIiwiaW1wb3J0IHsgcmVuZGVyIH0gZnJvbSBcIi4vU3luY1RhYmxlVHIudnVlP3Z1ZSZ0eXBlPXRlbXBsYXRlJmlkPWNmNTI0ZTgwXCJcbmltcG9ydCBzY3JpcHQgZnJvbSBcIi4vU3luY1RhYmxlVHIudnVlP3Z1ZSZ0eXBlPXNjcmlwdCZsYW5nPWpzXCJcbmV4cG9ydCAqIGZyb20gXCIuL1N5bmNUYWJsZVRyLnZ1ZT92dWUmdHlwZT1zY3JpcHQmbGFuZz1qc1wiXG5cbmltcG9ydCBleHBvcnRDb21wb25lbnQgZnJvbSBcIi4uLy4uLy4uL25vZGVfbW9kdWxlcy92dWUtbG9hZGVyL2Rpc3QvZXhwb3J0SGVscGVyLmpzXCJcbmNvbnN0IF9fZXhwb3J0c19fID0gLyojX19QVVJFX18qL2V4cG9ydENvbXBvbmVudChzY3JpcHQsIFtbJ3JlbmRlcicscmVuZGVyXSxbJ19fZmlsZScsXCJhc3NldHMvanMvY29tcG9uZW50cy9TeW5jVGFibGVUci52dWVcIl1dKVxuLyogaG90IHJlbG9hZCAqL1xuaWYgKG1vZHVsZS5ob3QpIHtcbiAgX19leHBvcnRzX18uX19obXJJZCA9IFwiY2Y1MjRlODBcIlxuICBjb25zdCBhcGkgPSBfX1ZVRV9ITVJfUlVOVElNRV9fXG4gIG1vZHVsZS5ob3QuYWNjZXB0KClcbiAgaWYgKCFhcGkuY3JlYXRlUmVjb3JkKCdjZjUyNGU4MCcsIF9fZXhwb3J0c19fKSkge1xuICAgIGFwaS5yZWxvYWQoJ2NmNTI0ZTgwJywgX19leHBvcnRzX18pXG4gIH1cbiAgXG4gIG1vZHVsZS5ob3QuYWNjZXB0KFwiLi9TeW5jVGFibGVUci52dWU/dnVlJnR5cGU9dGVtcGxhdGUmaWQ9Y2Y1MjRlODBcIiwgKCkgPT4ge1xuICAgIGFwaS5yZXJlbmRlcignY2Y1MjRlODAnLCByZW5kZXIpXG4gIH0pXG5cbn1cblxuXG5leHBvcnQgZGVmYXVsdCBfX2V4cG9ydHNfXyIsImV4cG9ydCB7IGRlZmF1bHQgfSBmcm9tIFwiLSEuLi8uLi8uLi9ub2RlX21vZHVsZXMvYmFiZWwtbG9hZGVyL2xpYi9pbmRleC5qcz8/Y2xvbmVkUnVsZVNldC0xLnVzZVswXSEuLi8uLi8uLi9ub2RlX21vZHVsZXMvdnVlLWxvYWRlci9kaXN0L2luZGV4LmpzPz9ydWxlU2V0WzBdLnVzZVswXSEuL1N5bmNUYWJsZS52dWU/dnVlJnR5cGU9c2NyaXB0Jmxhbmc9anNcIjsgZXhwb3J0ICogZnJvbSBcIi0hLi4vLi4vLi4vbm9kZV9tb2R1bGVzL2JhYmVsLWxvYWRlci9saWIvaW5kZXguanM/P2Nsb25lZFJ1bGVTZXQtMS51c2VbMF0hLi4vLi4vLi4vbm9kZV9tb2R1bGVzL3Z1ZS1sb2FkZXIvZGlzdC9pbmRleC5qcz8/cnVsZVNldFswXS51c2VbMF0hLi9TeW5jVGFibGUudnVlP3Z1ZSZ0eXBlPXNjcmlwdCZsYW5nPWpzXCIiLCJleHBvcnQgeyBkZWZhdWx0IH0gZnJvbSBcIi0hLi4vLi4vLi4vbm9kZV9tb2R1bGVzL2JhYmVsLWxvYWRlci9saWIvaW5kZXguanM/P2Nsb25lZFJ1bGVTZXQtMS51c2VbMF0hLi4vLi4vLi4vbm9kZV9tb2R1bGVzL3Z1ZS1sb2FkZXIvZGlzdC9pbmRleC5qcz8/cnVsZVNldFswXS51c2VbMF0hLi9TeW5jVGFibGVUci52dWU/dnVlJnR5cGU9c2NyaXB0Jmxhbmc9anNcIjsgZXhwb3J0ICogZnJvbSBcIi0hLi4vLi4vLi4vbm9kZV9tb2R1bGVzL2JhYmVsLWxvYWRlci9saWIvaW5kZXguanM/P2Nsb25lZFJ1bGVTZXQtMS51c2VbMF0hLi4vLi4vLi4vbm9kZV9tb2R1bGVzL3Z1ZS1sb2FkZXIvZGlzdC9pbmRleC5qcz8/cnVsZVNldFswXS51c2VbMF0hLi9TeW5jVGFibGVUci52dWU/dnVlJnR5cGU9c2NyaXB0Jmxhbmc9anNcIiIsImV4cG9ydCAqIGZyb20gXCItIS4uLy4uLy4uL25vZGVfbW9kdWxlcy9iYWJlbC1sb2FkZXIvbGliL2luZGV4LmpzPz9jbG9uZWRSdWxlU2V0LTEudXNlWzBdIS4uLy4uLy4uL25vZGVfbW9kdWxlcy92dWUtbG9hZGVyL2Rpc3QvdGVtcGxhdGVMb2FkZXIuanM/P3J1bGVTZXRbMV0ucnVsZXNbMl0hLi4vLi4vLi4vbm9kZV9tb2R1bGVzL3Z1ZS1sb2FkZXIvZGlzdC9pbmRleC5qcz8/cnVsZVNldFswXS51c2VbMF0hLi9TeW5jVGFibGUudnVlP3Z1ZSZ0eXBlPXRlbXBsYXRlJmlkPTdhNWIzMGJjXCIiLCJleHBvcnQgKiBmcm9tIFwiLSEuLi8uLi8uLi9ub2RlX21vZHVsZXMvYmFiZWwtbG9hZGVyL2xpYi9pbmRleC5qcz8/Y2xvbmVkUnVsZVNldC0xLnVzZVswXSEuLi8uLi8uLi9ub2RlX21vZHVsZXMvdnVlLWxvYWRlci9kaXN0L3RlbXBsYXRlTG9hZGVyLmpzPz9ydWxlU2V0WzFdLnJ1bGVzWzJdIS4uLy4uLy4uL25vZGVfbW9kdWxlcy92dWUtbG9hZGVyL2Rpc3QvaW5kZXguanM/P3J1bGVTZXRbMF0udXNlWzBdIS4vU3luY1RhYmxlVHIudnVlP3Z1ZSZ0eXBlPXRlbXBsYXRlJmlkPWNmNTI0ZTgwXCIiXSwibmFtZXMiOlsicmVxdWlyZSIsImltYWdlc0NvbnRleHQiLCJjb250ZXh0Iiwia2V5cyIsImZvckVhY2giLCIkIiwiZ2xvYmFsIiwialF1ZXJ5Iiwid2luZG93IiwiYm9vdHN0cmFwIiwiVG9vbHRpcCIsIlRvYXN0IiwiUG9wb3ZlciIsIk1vZGFsIiwiQ2hhcnQiLCJDaGFydERhdGFMYWJlbHMiLCJyZWdpc3RlciIsInBhbGV0dGUiLCJBdXRvY29tcGxldGUiLCJjcmVhdGVBcHAiLCJTeW5jVGFibGUiLCJWdWVDcmVhdGVBcHAiLCJnbG9iYWxUaGlzIiwiX19WVUVfUFJPRF9ERVZUT09MU19fIiwicHJvY2VzcyIsImVudiIsIk5PREVfRU5WIiwiYXhpb3MiLCJkb2N1bWVudCIsInJlYWR5IiwicHVneEZpbHRlciIsIm9uIiwiYWxsRm91bmQiLCJxdWVyeVNlbGVjdG9yQWxsIiwib25lIiwic2V0VGltZW91dCIsImxlbmd0aCIsImZvY3VzIiwiYWpheEVycm9yIiwiZXZlbnQiLCJqcVhIUiIsInN0YXR1cyIsInJlc3BvbnNlVGV4dCIsImxvY2F0aW9uIiwicmVsb2FkIiwiZSIsIiR0YWJsZUNvbnQiLCIkbWVudUJ0biIsInRhcmdldCIsIiRtZW51IiwicGFyZW50IiwiZmluZCIsIm1lbnVCdG5Ub3AiLCJvZmZzZXQiLCJ0b3AiLCJtZW51QnRuQm90dG9tIiwib3V0ZXJIZWlnaHQiLCJsb3dlck1lbnVCb3R0b20iLCJoZWlnaHQiLCJjc3MiLCJTeW5jVGFibGVUciIsIm5hbWUiLCJwcm9wcyIsImVudGl0aWVzIiwidHlwZSIsIk9iamVjdCIsImVudGl0eVR5cGUiLCJTdHJpbmciLCJkZWZhdWx0QWN0aW9uIiwiZGVmYXVsdERlbGV0ZU9wdGlvbnMiLCJ1cmwiLCJkZWxldGVVcmwiLCJtYWluQWN0aW9uTGFiZWwiLCJzaG93TWFpbkFjdGlvbiIsIkJvb2xlYW4iLCJzaG93RGVsZXRlQWN0aW9uIiwiZXh0cmFDb2x1bW5zIiwiQXJyYXkiLCJpbml0aWFsU3RhdGVMYWJlbCIsImRhdGEiLCJjaGVja2VkIiwiYWxsQ2hlY2tlZCIsImRlbGV0ZU9wdGlvbnMiLCJleGVjdXRpb25RdWV1ZSIsInF1ZXVlV29ya2luZyIsImVudGl0aWVzVG9EZWxldGUiLCJtZXRob2RzIiwiY2hlY2tib3hDbGlja2VkIiwiaWQiLCJwb3MiLCJpbmRleE9mIiwic3BsaWNlIiwicHVzaCIsInRvZ2dsZUFsbENoZWNrYm94IiwiJHJlZnMiLCJjaGlsZHJlbiIsImNoaWxkIiwiZGlzYWJsZWQiLCJjYWxsQWN0aW9uIiwicGF5bG9hZCIsImVudGl0eUlkIiwidW5kZWZpbmVkIiwiZW50aXR5IiwiYWN0aW9uIiwiY29uc29sZSIsIndhcm4iLCJzdGF0ZSIsInByb3ZpZGVySWQiLCJyZXBsYWNlIiwiJHRoaXMiLCJlbnRpdHlEYXRhIiwicG9zdCIsImhlYWRlcnMiLCJ0aGVuIiwicmVzcG9uc2UiLCJzdWNjZXNzIiwiZXJyb3IiLCJuZXdFbnRpdHlJZCIsIiRlbWl0IiwidGlja1F1ZXVlIiwiZXJyIiwicXVldWVTZWxlY3RlZEZvckRlZmF1bHRBY3Rpb24iLCJlbnRpdHlJZElkeCIsInF1ZXVlQWN0aW9uIiwiZGVsZXRlRW50aXR5IiwiZGVsZXRlU2VsZWN0ZWQiLCJxdWV1ZURlbGV0ZUFjdGlvbnMiLCJzaGlmdCIsInNldERlbGV0ZU9wdGlvbiIsImtleSIsInZhbHVlIiwiY29tcG9uZW50cyIsImNvbXB1dGVkIiwic3RhdGVNc2ciLCJpbnRlcm5hbEdyb3VwSWQiLCJpbnRlcm5hbFNlY3RvcklkIiwiX2NyZWF0ZUVsZW1lbnRWTm9kZSIsIl9ob2lzdGVkXzI2IiwiX2hvaXN0ZWRfMjciLCJfaG9pc3RlZF8yOCIsIiRwcm9wcyIsIl9jcmVhdGVFbGVtZW50QmxvY2siLCJfaG9pc3RlZF8xIiwiX2N0eCIsIm9uSW5wdXQiLCIkb3B0aW9ucyIsIl9ob2lzdGVkXzUiLCJfRnJhZ21lbnQiLCJfcmVuZGVyTGlzdCIsImNvbHVtbiIsIl90b0Rpc3BsYXlTdHJpbmciLCJfaG9pc3RlZF82IiwiX2hvaXN0ZWRfNyIsIl9jcmVhdGVCbG9jayIsIl9jb21wb25lbnRfc3luY3RhYmxlX3RyIiwib25NYWluQnRuQ2xpY2siLCIkZXZlbnQiLCJvbkRlbGV0ZUJ0bkNsaWNrIiwib25DaGVja2JveGNsaWNrZWQiLCJyZWYiLCJvbkNsaWNrIiwiX2hvaXN0ZWRfOCIsIl9ob2lzdGVkXzkiLCJfVGVsZXBvcnQiLCJ0byIsIl9ub3JtYWxpemVDbGFzcyIsInNob3ciLCJ0YWJpbmRleCIsIl9ob2lzdGVkXzEzIiwiZGVsZXRlT25MZGFwIiwiX2hvaXN0ZWRfMTkiLCJkZWxldGVPbkdBcHBzIiwiX2hvaXN0ZWRfMjIiLCJfaG9pc3RlZF8yNCIsIl9ob2lzdGVkXzMxIiwiZ0FwcHNPdSIsIl9uYW1lIiwiX2hvaXN0ZWRfMiIsIl9ob2lzdGVkXzMiXSwic291cmNlUm9vdCI6IiJ9