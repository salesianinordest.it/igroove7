mysqldump -u root -p igroove --complete-insert  --no-create-db --no-create-info --no-set-names --no-tablespaces --compact Device > /tmp/t.sql

sed -i 's/`bypassHotspot`/bypass_hotspot/g'  /tmp/t.sql
sed -i 's/`mikrotikList_id`/mikrotik_list_id/g'  /tmp/t.sql
sed -i "s/\\\'//g"  /tmp/t.sql
sed -i 's/,0,0,/,False,False,/g'  /tmp/t.sql
sed -i 's/,1,1,/,True,True,/g'  /tmp/t.sql
sed -i 's/,0,1,/,False,True,/g'  /tmp/t.sql
sed -i 's/,1,0,/,True,False,/g'  /tmp/t.sql
sed -i 's/`Device`/device/g'  /tmp/t.sql
sed -i 's/`//g'  /tmp/t.sql
echo "Attendere la sostituzione degli indici numerici"
echo
for i in {0..9}; do
  echo -n .
 sed -ie "s/'$i'/'faea458a-7188-11e9-9a0$i-000c29f44e74'/g" /tmp/t.sql
 done
 for i in {10..99}; do
   echo -n .
  sed -ie "s/'$i'/'faea458a-7187-11e9-7b$i-000c29f44e74'/g" /tmp/t.sql
  done
  for i in {100..999}; do
    echo -n .
   sed -ie "s/'$i'/'faea458a-7186-11e9-6$i-000c29f44e74'/g" /tmp/t.sql
   done
   for i in {1000..9999}; do
     echo -n .
    sed -ie "s/'$i'/'faea458a-7185-11e9-$i-000c29f44e74'/g" /tmp/t.sql
    done   

sudo -u postgres psql -d igroove < /tmp/t.sql