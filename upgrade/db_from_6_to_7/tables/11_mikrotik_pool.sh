mysqldump -u root -p igroove --complete-insert  --no-create-db --no-create-info --no-set-names --no-tablespaces --compact MikrotikPool > /tmp/t.sql

sed -i 's/`ipStart`/ip_start/g'  /tmp/t.sql
sed -i 's/`ipEnd`/ip_end/g'  /tmp/t.sql
sed -i 's/`creareSuMikrotik`/creare_su_mikrotik/g'  /tmp/t.sql
sed -i 's/`dhcpServerName`/dhcp_server_name/g'  /tmp/t.sql
sed -i 's/,0,/,False,/g'  /tmp/t.sql
sed -i 's/,1,/,True,/g'  /tmp/t.sql
sed -i 's/`MikrotikPool`/mikrotik_pool/g'  /tmp/t.sql
sed -i 's/`//g'  /tmp/t.sql

sudo -u postgres psql -d igroove < /tmp/t.sql