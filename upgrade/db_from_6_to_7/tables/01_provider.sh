


mysqldump -u root -p igroove --complete-insert --skip-extended-insert  --no-create-db --no-create-info --no-set-names --no-tablespaces --compact Provider > /tmp/t.sql

sed -i 's/studentAutoCreateUsername/student_auto_create_username/g' /tmp/t.sql
sed -i 's/studentGoogleAppDomain/student_google_app_domain/g' /tmp/t.sql
sed -i 's/studentGoogleAppClientId/student_google_app_client_id/g' /tmp/t.sql
sed -i 's/studentGoogleAppClientSecret/student_google_app_client_secret/g' /tmp/t.sql
sed -i 's/studentGoogleAppAutoEmailStyle/student_google_app_auto_email_style/g' /tmp/t.sql
sed -i 's/studentGoogleAppAutoEmail/student_google_app_auto_email/g' /tmp/t.sql
sed -i 's/studentGoogleAppCreateGroup/student_google_app_create_group/g' /tmp/t.sql
sed -i 's/studentGoogleAppGroupPrefix/student_google_app_group_prefix/g' /tmp/t.sql
sed -i 's/studentGoogleAppCreateOU/student_google_app_create_ou/g' /tmp/t.sql
sed -i 's/studentGoogleAppOUPrefix/student_google_app_ouprefix/g' /tmp/t.sql
sed -i 's/studentGoogleAppOUPath/student_google_app_oupath/g' /tmp/t.sql
sed -i 's/internetTeachersControl/internet_teachers_control/g' /tmp/t.sql
sed -i 's/internetOpenAccessRange/internet_open_access_range/g' /tmp/t.sql
sed -i 's/filterData/filter_data/g' /tmp/t.sql
sed -i 's/studentLdapCreateOU/student_ldap_create_ou/g' /tmp/t.sql
sed -i 's/studentLdapOUPrefix/student_ldap_ouprefix/g' /tmp/t.sql
sed -i 's/studentLdapOUPath/student_ldap_oupath/g' /tmp/t.sql
sed -i 's/teacherAutoCreateUsername/teacher_auto_create_username/g' /tmp/t.sql
sed -i 's/teacherLdapCreateOU/teacher_ldap_create_ou/g' /tmp/t.sql
sed -i 's/teacherLdapOUPrefix/teacher_ldap_ouprefix/g' /tmp/t.sql
sed -i 's/teacherLdapOUPath/teacher_ldap_oupath/g' /tmp/t.sql
sed -i 's/teacherGoogleAppDomain/teacher_google_app_domain/g' /tmp/t.sql
sed -i 's/teacherGoogleAppClientId/teacher_google_app_client_id/g' /tmp/t.sql
sed -i 's/teacherGoogleAppClientSecret/teacher_google_app_client_secret/g' /tmp/t.sql
sed -i 's/teacherGoogleAppAutoEmailStyle/teacher_google_app_auto_email_style/g' /tmp/t.sql
sed -i 's/teacherGoogleAppAutoEmail/teacher_google_app_auto_email/g' /tmp/t.sql
sed -i 's/teacherGoogleAppCreateGroup/teacher_google_app_create_group/g' /tmp/t.sql
sed -i 's/teacherGoogleAppGroupPrefix/teacher_google_app_group_prefix/g' /tmp/t.sql
sed -i 's/teacherGoogleAppCreateOU/teacher_google_app_create_ou/g' /tmp/t.sql
sed -i 's/teacherGoogleAppOUPrefix/teacher_google_app_ouprefix/g' /tmp/t.sql
sed -i 's/teacherGoogleAppOUPath/teacher_google_app_oupath/g' /tmp/t.sql
sed -i 's/pdfHeaderHeight/pdf_header_height/g' /tmp/t.sql
sed -i 's/pdfFooterHeight/pdf_footer_height/g' /tmp/t.sql
sed -i 's/pdfHeader/pdf_header/g' /tmp/t.sql
sed -i 's/pdfFooter/pdf_footer/g' /tmp/t.sql
sed -i 's/pdfStudentBadgePageSize/pdf_student_badge_page_size/g' /tmp/t.sql
sed -i 's/pdfStudentBadgePageLandscape/pdf_student_badge_page_landscape/g' /tmp/t.sql
sed -i 's/pdfStudentBadge/pdf_student_badge/g' /tmp/t.sql
sed -i 's/studentGoogleAppCreateProviderGroup/student_google_app_create_provider_group/g' /tmp/t.sql
sed -i 's/studentGoogleAppGroupExtraEmail/student_google_app_group_extra_email/g' /tmp/t.sql
sed -i 's/studentGoogleAppProviderGroupExtraEmail/student_google_app_provider_group_extra_email/g' /tmp/t.sql
sed -i 's/teacherGoogleAppCreateProviderGroup/teacher_google_app_create_provider_group/g' /tmp/t.sql
sed -i 's/teacherGoogleAppGroupExtraEmail/teacher_google_app_group_extra_email/g' /tmp/t.sql
sed -i 's/teacherGoogleAppProviderGroupExtraEmail/teacher_google_app_provider_group_extra_email/g' /tmp/t.sql
sed -i 's/studentGoogleAppUseUserInProviderGroup/student_google_app_use_user_in_provider_group/g' /tmp/t.sql
sed -i 's/teacherGoogleAppUseUserInProviderGroup/teacher_google_app_use_user_in_provider_group/g' /tmp/t.sql
sed -i 's/studentForceImportedPasswordInitial/student_force_imported_password_initial/g' /tmp/t.sql
sed -i 's/teacherForceImportedPasswordInitial/teacher_force_imported_password_initial/g' /tmp/t.sql
sed -i 's/teacherForceImportedPassword/teacher_force_imported_password/g' /tmp/t.sql
sed -i 's/studentForceImportedPassword/student_force_imported_password/g' /tmp/t.sql
sed -i 's/studentsOrderByInTeacherList/students_order_by_in_teacher_list/g' /tmp/t.sql
sed -i 's/studentLdapExtendedUsernameSuffix/student_ldap_extended_username_suffix/g' /tmp/t.sql
sed -i 's/studentLdapUseEmailForExtendedUsername/student_ldap_use_email_for_extended_username/g' /tmp/t.sql
sed -i 's/studentLdapAutoUsernameStyle/student_ldap_auto_username_style/g' /tmp/t.sql
sed -i 's/teacherLdapAutoUsernameStyle/teacher_ldap_auto_username_style/g' /tmp/t.sql
sed -i 's/teacherLdapExtendedUsernameSuffix/teacher_ldap_extended_username_suffix/g' /tmp/t.sql
sed -i 's/teacherLdapUseEmailForExtendedUsername/teacher_ldap_use_email_for_extended_username/g' /tmp/t.sql
sed -i 's/appleSchoolLocation_id/apple_school_location_id/g' /tmp/t.sql

sed -i 's/,0,/,False,/g'  /tmp/t.sql
sed -i 's/,1,/,True,/g'  /tmp/t.sql


sed -i 's/\\"/"/g' /tmp/t.sql


sed -i 's/Provider/provider/g'  /tmp/t.sql
sed -i 's/`//g'  /tmp/t.sql

sudo -u postgres psql -d igroove < /tmp/t.sql

