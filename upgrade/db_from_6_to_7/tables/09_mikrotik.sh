mysqldump -u root -p igroove --complete-insert  --no-create-db --no-create-info --no-set-names --no-tablespaces --compact Mikrotik > /tmp/t.sql

sed -i 's/`apiUsername`/api_username/g'  /tmp/t.sql
sed -i 's/`apiPassword`/api_password/g'  /tmp/t.sql
sed -i 's/`useBypass`/use_bypass/g'  /tmp/t.sql
sed -i 's/`syncSuffix`/sync_suffix/g'  /tmp/t.sql
sed -i 's/,0,0,/,False,False,/g'  /tmp/t.sql
sed -i 's/,1,1,/,True,True,/g'  /tmp/t.sql
sed -i 's/,0,1,/,False,True,/g'  /tmp/t.sql
sed -i 's/,1,0,/,True,False,/g'  /tmp/t.sql
sed -i 's/`Mikrotik`/mikrotik/g'  /tmp/t.sql
sed -i 's/`//g'  /tmp/t.sql
echo "Attendere la sostituzione degli indici numerici"
echo
for i in {0..9}; do
  echo -n .
 sed -ie "s/'$i'/'faea458a-7188-11e9-9a0$i-000c29f44e74'/g" /tmp/t.sql
 done

sudo -u postgres psql -d igroove < /tmp/t.sql