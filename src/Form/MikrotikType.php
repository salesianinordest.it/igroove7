<?php

namespace App\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\PasswordType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;

class MikrotikType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
	        ->add('ip', null, ['required' => true])
            ->add('apiUsername')
            ->add('apiPassword', PasswordType::class, ['label' => 'Api Password (lasciare bianca per non modificare)', 'required' => false, 'always_empty' => false, 'attr' => ['autocomplete' => 'off']])
//            ->add('syncSuffix', 'text', ['label' => 'Stringa di sincronizzazione (aggiungere per utilizzare più iGroove sullo stesso mikrotik'])
            ->add('useBypass')
        ;
    }

    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver->setDefaults([
            'data_class' => 'App\Entity\Mikrotik',
        ]);
    }

    /**
     * @return string
     */
    public function getName()
    {
        return 'zen_igroovebundle_mikrotik';
    }
}
