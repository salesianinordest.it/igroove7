<?php

namespace App\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;

class GroupCompositionType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
                ->add('imported')
                ->add('rootGroup')
                ->add('subGroup')
                ->add('updated_at')
                ->add('created_at')
                ->add('student')
        ;
    }

    public function getName()
    {
        return 'zen_igroovebundle_groupcompositiontype';
    }
}
