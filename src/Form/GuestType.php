<?php

namespace App\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;

class GuestType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
	        ->add('lastname', null, array('label' => 'Cognome', 'required' => true))
                ->add('firstname', null, ['label' => 'Nome'])
                ->add('identified_by', null, ['label' => 'Identificato attraverso'])
                ->add('starting_from', null, ['widget' => 'single_text', 'label' => 'Accesso attivo dal'])
                ->add('ending_to', null, ['widget' => 'single_text', 'label' => 'Fino al'])
        ;
    }

    public function getName()
    {
        return 'zen_igroovebundle_guesttype';
    }
}
