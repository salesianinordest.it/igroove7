<?php

namespace App\Form;

use App\Entity\Group;
use App\Entity\Provider;
use App\Entity\Student;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;

class StudentType extends AbstractType
{
    protected $modelManager;

    /*
     ** @todo: per Daniele :-)
        function __construct(ModelManager $modelManager)
        {
            $this->modelManager = $modelManager;
        }
    */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $entity = $builder->getData();
        $newEntity = $imported = $autoUsername = $autoEmail = false;
        if ($entity instanceof Student) {
            if (null == $entity->getId()) {
                $newEntity = true;
            }

            $imported = ('' != $entity->getIdOnProvider());

            $provider = $entity->getProvider();
            if ($provider instanceof Provider) {
                $autoUsername = $provider->getStudentAutoCreateUsername();
                $autoEmail = $provider->getStudentGoogleAppAutoEmail();
            }
        }

        $builder
            ->add('fiscalCode', TextType::class, ['label' => 'Codice Fiscale', 'required' => true])
            ->add('lastname', null, ['label' => 'Cognome', 'required' => true, 'disabled' => $imported]) // disabilito se importati da provider (id_on_provider non nullo)
            ->add('firstname', null, ['label' => 'Nome', 'required' => true, 'disabled' => $imported]) // disabilito se importati da provider
            ->add('username', null, ['label' => 'Username'.($autoUsername ? ' (Lasciare vuoto per generare automaticamente)' : '')])  // , 'disabled' => $autoUsername
            ->add('email', null, ['label' => 'E-Mail'.($autoEmail ? ' (Lasciare vuoto per generare automaticamente)' : '')]) // , 'disabled' => $autoEmail
            ->add('provider', EntityType::class, ['label' => 'Provider', 'class' => "App\Entity\Provider",
                'choice_label' => 'name', 'required' => true, ]) // , 'disabled' => $imported
            ->add('additionalProviders', EntityType::class, [
                'label' => 'Provider Aggiuntivi',
                'required' => false,
                'multiple' => true,
                'expanded' => true,
                'class' => Provider::class,
            ])
            ->add(
                'memberOf',
                EntityType::class,
                [
                    'label' => 'Gruppi/Classi',
                    'required' => false,
                    'multiple' => true,
                    'expanded' => true,
                    'class' => Group::class,
					'choice_label' => fn(Group $group) => $group->getName().($entity->getProvider() !== null && $group->getProvider()->getId() !== $entity->getProvider()->getId() ? ' ('.$group->getProvider()->getName().')' : '')
                ]
            );
    }

    public function getName()
    {
        return 'app_studenttype';
    }
}
