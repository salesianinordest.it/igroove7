<?php

namespace App\Form;

use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\IntegerType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;

class GroupType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
	        ->add('name', TextType::class, ['label' => "Nome", 'disabled' => !$builder->getData()->getManageManually(), 'required' => true])
            ->add('radiusVlan', IntegerType::class, ['label' => 'vLan Dinamica (Opzionale)', 'required' => false, 'disabled' => $builder->getData()->getManageManually()])
            ->add('sector', EntityType::class, ['label' => 'Settore (Opzionale)', 'class' => "App\Entity\Sector",
                                                    'choice_label' => 'name', 'required' => false, 'disabled' => !$builder->getData()->getManageManually(), ]) // @todo filtrare per provider
        ;
    }

    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver->setDefaults([
            'data_class' => 'App\Entity\Group',
        ]);
    }

    /**
     * @return string
     */
    public function getName()
    {
        return 'zen_igroovebundle_group';
    }
}
