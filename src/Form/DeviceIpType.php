<?php

namespace App\Form;

use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\CallbackTransformer;
use Symfony\Component\Form\Exception\TransformationFailedException;
use Symfony\Component\Form\Extension\Core\Type\HiddenType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class DeviceIpType extends AbstractType
{
    private $entityManager;

    public function __construct(EntityManagerInterface $entityManager) // Create the constructor if not exist and add the entity manager as first parameter (we will add it later)
    {
        $this->entityManager = $entityManager;
    }

    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('ip', TextType::class)
            ->add('mikrotikPool', HiddenType::class);

        $builder->get('mikrotikPool')->addModelTransformer(new CallbackTransformer(function ($data) {
            if (null === $data) {
                return '';
            }

            return $data->getId();
        }, function ($data) {
            if (null === $data) {
                throw new TransformationFailedException(sprintf('Pool cannot be null!', $data));
            }

            $pool = $this->entityManager
                ->getRepository('App\Entity\MikrotikPool')
                ->find($data);

            if (null === $pool) {
                throw new TransformationFailedException(sprintf('An issue with number "%s" does not exist!', $pool));
            }

            return $pool;
        }));
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => 'App\Entity\DeviceIp',
        ]);
    }

    /**
     * @return string
     */
    public function getName()
    {
        return 'zen_igroovebundle_deviceip';
    }
}
