<?php

namespace App\Form;

use App\Entity\AppleSchool;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\CheckboxType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\CollectionType;
use Symfony\Component\Form\Extension\Core\Type\PasswordType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class AppleSchoolType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
	    $builder->add('organizationName', TextType::class, ['label' => "Nome Organizzazione", 'required' => true])
	            ->add('organizationId', TextType::class, ['label' => "ID Organizzazione", 'required' => true])
	            ->add('sftpUsername', TextType::class, ['label' => "Username SFTP", 'required' => true])
                ->add('sftpPassword', PasswordType::class, ['label' => 'Password SFTP (lasciare vuoto per non cambiare)', 'required' => false, 'attr' => ['autocomplete' => 'off']])
                ->add('courseType', ChoiceType::class, ['label' => 'Tipi di corsi da creare', 'choices' => [
                    'Solo classe generica' => 0,
                    'Combinazioni Classe-Materia' => 1,
                    'Classe generica e Combinazioni Classe-Materia' => 2,
                ]])
                ->add('locations', CollectionType::class, [
                    'label' => 'Sedi',
                    'by_reference' => false,
                    'allow_add' => true,
                    'allow_delete' => true,
                    'entry_type' => AppleSchoolLocationType::class,
                ])
                ->add('active', CheckboxType::class, ['label' => 'Attivo', 'required' => false])
        ;
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(
            [
                'data_class' => AppleSchool::class,
            ]
        );
    }
}
