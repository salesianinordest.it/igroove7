<?php

namespace App\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\PasswordType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;

class WatchguardType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
	        ->add('host', null, ['required' => true])
            ->add('port', null, ['required' => true])
            ->add('username', null, ['required' => true])
            ->add('password', PasswordType::class, ['required' => true])
        ;
    }

    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver->setDefaults([
            'data_class' => 'App\Entity\Watchguard',
        ]);
    }

    /**
     * @return string
     */
    public function getName()
    {
        return 'zen_igroovebundle_watchguard';
    }
}
