<?php

namespace App\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\CheckboxType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\PasswordType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class MdmServerType extends AbstractType {

    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
	    $connectorsList = [];
	    if(isset($options['mdmConnectors'])) {
		    $connectorsList = $options['mdmConnectors'];
	    }

		$builder
            ->add('name', null, ['label' => "Nome", 'required' => true])
            ->add('type', ChoiceType::class, [
                'choices' => $this->generateConnectorsSelect($connectorsList),
                'label' => 'Tipo di server',
                'required' => true
            ]);

        $this->generateConnectionDataForm($builder, $connectorsList);

        $builder->add('active', CheckboxType::class, ['label' => 'Attivo', 'required' => false]);
    }

    /**
     * @return string
     */
    public function getName()
    {
        return 'app_mdmserver';
    }

	public function configureOptions(OptionsResolver $resolver) {
		$resolver->setDefaults([
			'mdmConnectors' => [],
		]);
	}

    protected function generateConnectorsSelect(array $connectorsList) {
        $options = [];
        foreach($connectorsList as $id => $data) {
            $options[$data['name']] = $id;
        }

        return $options;
    }

    protected function generateConnectionDataForm(FormBuilderInterface $builder, array $connectorsList) {
        $container = $builder->create("connectorDataCont", null, ['compound' => true, 'by_reference' => false, 'mapped' => false, 'label' => " "]);
        foreach($connectorsList as $id => $data) {
            $form = $builder->create($id,null,['compound' => true, 'by_reference' => false, 'mapped' => false, 'label' => " "]);
            foreach($data['ui'] as $uiId => $uiData) {
                if($uiData['type'] == "password")
                    $form->add($uiId,PasswordType::class,['label' => $uiData['title']." (lasciare vuoto per non cambiare)", 'always_empty' => false]);
                else
                    $form->add($uiId,null,['label' => $uiData['title']]);
            }
            $container->add($form);
        }
        $builder->add($container);
    }

}
