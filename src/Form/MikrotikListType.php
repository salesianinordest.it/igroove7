<?php

namespace App\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\NumberType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;

class MikrotikListType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('mikrotik')
	        ->add('nome', null, ['label' => "Nome", 'required' => true])
	        ->add('radiusVlan', NumberType::class, ['label' => "vLan Dinamica (Opzionale)", 'required' => false])
	        ->add('showAsLaboratory', null, ['label' => "Mostra agli insegnanti come laboratorio abilitabile?"])
        ;
    }

    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver->setDefaults([
            'data_class' => 'App\Entity\MikrotikList',
        ]);
    }

    /**
     * @return string
     */
    public function getName()
    {
        return 'zen_igroovebundle_mikrotiklist';
    }
}
