<?php

namespace App\Form;

use App\Entity\AppleSchoolLocation;
use App\Manager\ConfigurationManager;
use Dompdf\Adapter\CPDF;
use FOS\CKEditorBundle\Form\Type\CKEditorType;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\CheckboxType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\IntegerType;
use Symfony\Component\Form\Extension\Core\Type\PasswordType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class ProviderType extends AbstractType
{
    protected $filterProviderData;

    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $this->setFilterProviderData($options['filterProviderData']);
        $builder
	        ->add('name', TextType::class, ['label' => 'Nome del provider:', 'required' => true])
            ->add(
                'filter',
                ChoiceType::class,
                [
                    'choices' => $this->generateFilterSelect(),
                    'label' => 'Processare i dati secondo lo standard:',
                    'required' => false,
                ]
            )
            ->add('appleSchoolLocation', EntityType::class, ['class' => AppleSchoolLocation::class, 'choice_label' => 'full_name', 'label' => 'Sede AppleSchool (Lasciare vuota per non usare)', 'required' => false])
            ->add('active', CheckboxType::class, ['label' => 'E\' attivo questo provider', 'required' => false]);

        $this->generateFilterForm($builder);

        $builder
            ->add(
                'studentAutoCreateUsername',
                CheckboxType::class,
                [
                    'label' => 'Generare automaticamente gli username? (disabilitare in caso siano forniti dal provider).',
                    'required' => false,
                ]
            )
            ->add(
                'studentLdapAutoUsernameStyle',
                ChoiceType::class,
                ['label' => 'Sintassi dell\'username: (se diversa da quella generale)', 'required' => false,
                      'choices' => ConfigurationManager::$usernameStyles,
                ]
            )
            ->add(
                'studentLdapPasswordOverrideOptions',
                CheckboxType::class,
                ['label' => "Utilizza delle regole diverse per la generazione delle password?", 'required' => false]
            )
            ->add('studentLdapPasswordMinChar', IntegerType::class, ['label' => 'Numero minimo dei caratteri:', 'required' => false] )
            ->add('studentLdapPasswordComplexity', CheckboxType::class, ['label' => 'Necessaria password complessa.', 'required' => false])
	        ->add('studentLdapPasswordUseDictionary', CheckboxType::class, ['label' => 'Utilizza i nomi delle province per generare una password semplice da ricordare', 'required' => false])
	        ->add('studentLdapPasswordPrefix', TextType::class, ['label' => 'Prefisso da anteporre alle password generate', 'required' => false])
            ->add(
                'studentForceImportedPasswordInitial',
                CheckboxType::class,
                ['label' => "Imposta la password proveniente dal provider alla creazione dell'utente?", 'required' => false]
            )
            ->add(
                'studentForceImportedPassword',
                CheckboxType::class,
                ['label' => 'ReImpostare la password proveniente dal provider ad ogni importazione?', 'required' => false]
            )
	        ->add('studentHaveManagedDevice', CheckboxType::class,
		        ['label' => 'Gli studenti usano un dispositivo gestito?', 'required' => false]
	        )
            ->add(
                'studentLdapExtendedUsernameSuffix',
                TextType::class,
                ['label' => "Suffisso terminale/dominio per l'username esteso Active Directory (lasciare vuoto per usare il nome dominio generale)", 'required' => false]
            )
            ->add(
                'studentLdapUseEmailForExtendedUsername',
                CheckboxType::class,
                ['label' => "Usa la prima parte dell'email come username esteso di Active Directory?", 'required' => false]
            )
            ->add(
                'studentsOrderByInTeacherList',
                ChoiceType::class,
                ['label' => 'Ordina Studenti nella lista per i docenti per', 'choices' => ['Username' => 'username', 'Cognome' => 'lastname', 'Nome' => 'firstname']]
            )
            ->add(
                'studentLdapCreateOU',
                CheckboxType::class,
                ['label' => 'Crea le unità organizzative', 'required' => false]
            )
            ->add(
                'studentLdapOUPrefix',
                TextType::class,
                ['label' => 'Prefisso al nome delle unità organizzative', 'required' => false, 'trim' => false]
            )
            ->add(
                'studentLdapOUPath',
                TextType::class,
                ['label' => 'Percorso delle unità organizzative', 'required' => false]
            )
            ->add('studentGoogleAppClientId', TextType::class, ['label' => 'ClientId Google Apps (lasciare vuoto per disattivare)', 'required' => false])
            ->add('studentGoogleAppDomain', TextType::class, ['label' => 'Dominio Google Apps', 'required' => false])
            ->add('studentGoogleAppClientSecret', TextType::class, ['label' => 'Client Secret Google Apps', 'required' => false])
            ->add(
                'studentGoogleAppAutoEmail',
                ChoiceType::class,
                ['label' => 'Comportamento per la creazione degli indirizzi email', 'required' => false, 'choices' => [
                    'Crea solo gli indirizzi email presenti nel provider' => 0,
                    'Crea gli indirizzi presenti nel provider e usa lo username se mancano' => 1,
                    'Crea gli indirizzi email usando lo username e non considerando quanto presente nel provider' => 2,
                    'Crea gli indirizzi usando una sintassi specificata' => 4,
                    'Crea gli indirizzi presenti nel provider oppure crea gli indirizzi usando una sintassi specificata' => 5,
                    'Non creare nulla, sospendi la creazione degli indirizzi email' => 3,
                ]]
            )
            ->add(
                'studentGoogleAppAutoEmailStyle',
                ChoiceType::class,
                ['label' => 'Sintassi dell\'indirizzo email:', 'required' => false,
                      'choices' => ConfigurationManager::$usernameStyles,
                ]
            )
            ->add(
                'studentGoogleAppCreateGroup',
                CheckboxType::class,
                ['label' => 'Crea i gruppi di distribuzione', 'required' => false]
            )
            ->add(
                'studentGoogleAppCreateProviderGroup',
                CheckboxType::class,
                ['label' => 'Crea il gruppo del provider', 'required' => false]
            )
            ->add(
                'studentGoogleAppUseUserInProviderGroup',
                CheckboxType::class,
                ['label' => 'Inserisci gli utenti al posto dei gruppi del gruppo provider', 'required' => false]
            )
            ->add(
                'studentGoogleAppGroupPrefix',
                TextType::class,
                ['label' => 'Prefisso al nome dei gruppi di distribuzione:', 'required' => false, 'trim' => false]
            )
            ->add(
                'studentGoogleAppGroupExtraEmail',
                TextareaType::class,
                ['label' => 'Lista di email da aggiungere in ogni gruppo di distribuzione: (separate da virgola)', 'required' => false]
            )
            ->add(
                'studentGoogleAppProviderGroupExtraEmail',
                TextareaType::class,
                ['label' => 'Lista di email da aggiungere nel gruppo del provider: (separate da virgola)', 'required' => false]
            )
            ->add(
                'studentGoogleAppCreateOU',
                CheckboxType::class,
                ['label' => 'Crea le unità organizzative', 'required' => false]
            )
            ->add(
                'studentGoogleAppOUPrefix',
                TextType::class,
                ['label' => 'Prefisso al nome delle unità organizzative', 'required' => false]
            )
            ->add(
                'studentGoogleAppOUPath',
                TextType::class,
                ['label' => 'Percorso delle unità organizzative', 'required' => true, 'empty_data' => '/']
            )
	        ->add(
		        'studentTrashedLdapBehaviour',
		        ChoiceType::class,
		        ['label' => 'Comportamento su LDAP/Active Directory per gli studenti cestinati', 'required' => false, 'choices' => [
			        'Non fare nulla' => 0,
			        'Disabilita' => 1,
			        'Cancella' => 2,
		        ]]
	        )
	        ->add(
		        'studentTrashedLdapOUPath',
		        TextType::class,
		        ['label' => 'Percorso unità organizzativa LDAP in cui spostare gli utenti cestinati (lasciar vuoto per non spostare)', 'required' => false, 'trim' => false]
	        )
	        ->add(
		        'studentTrashedLdapGroupName',
		        TextType::class,
		        ['label' => 'Nome del gruppo LDAP in cui aggiungere gli studenti (lasciar vuoto per non aggiungere)', 'required' => false, 'trim' => false]
	        )
	        ->add(
		        'studentTrashedLdapLeaveInClassesGroup',
		        CheckboxType::class,
		        ['label' => 'Lascia gli studenti cestinati nei gruppi classe su LDAP', 'required' => false]
	        )
	        ->add(
		        'studentTrashedLdapLeaveInProviderGroup', //fare sparire se non selezionato il metti i singoli utenti nel provider?
		        CheckboxType::class,
		        ['label' => 'Lascia gli studenti cestinati nei gruppi provider su LDAP', 'required' => false]
	        )

	        ->add(
		        'studentTrashedGoogleAppBehaviour',
		        ChoiceType::class,
		        ['label' => 'Comportamento su Google Workspace per gli studenti cestinati', 'required' => false, 'choices' => [
			        'Non fare nulla' => 0,
			        'Disabilita' => 1,
			        'Cancella' => 2,
		        ]]
	        )
	        ->add(
		        'studentTrashedGoogleAppOUPath',
		        TextType::class,
		        ['label' => 'Percorso unità organizzativa Google Workspace in cui spostare gli utenti cestinati (lasciar vuoto per non spostare)', 'required' => false, 'trim' => false]
	        )
	        ->add(
		        'studentTrashedGoogleAppGroupName',
		        TextType::class,
		        ['label' => 'Nome del gruppo Google Workspace in cui aggiungere gli studenti (lasciar vuoto per non aggiungere)', 'required' => false, 'trim' => false]
	        )
	        ->add(
		        'studentTrashedGoogleAppLeaveInClassesGroup', //->omettere per i docenti?
		        CheckboxType::class,
		        ['label' => 'Lascia gli studenti cestinati nei gruppi classe su Google Workspace', 'required' => false]
	        )
	        ->add(
		        'studentTrashedGoogleAppLeaveInProviderGroup', //fare sparire se non selezionato il metti i singoli utenti nel provider?
		        CheckboxType::class,
		        ['label' => 'Lascia gli studenti cestinati nei gruppi provider su Google Workspace', 'required' => false]
	        )
	        ->add(
		        'studentTrashedAsmBehaviour',
		        ChoiceType::class,
		        ['label' => 'Comportamento su AppleSchool per gli studenti cestinati', 'required' => false, 'choices' => [
			        'Non fare nulla' => 0,
			        'Cancella' => 1,
		        ]]
	        )
	        ->add(
		        'studentTrashedAsmLeaveInClassesGroup',
		        CheckboxType::class,
		        ['label' => 'Lascia gli studenti cestinati nei gruppi classe su Asm', 'required' => false]
	        )
	        ->add(
		        'studentTrashedDisableRestoreIfPresentOnProvider',
		        CheckboxType::class,
		        ['label' => 'Disabilita il ripristino dal cestino per gli studenti che riappaiono nel provider d\'origine', 'required' => false]
	        )
        ;

        $builder
            ->add(
                'teacherAutoCreateUsername',
                CheckboxType::class,
                [
                    'label' => 'Generare automaticamente gli username? (disabilitare in caso siano forniti dal provider).',
                    'required' => false,
                ]
            )
            ->add(
                'teacherLdapAutoUsernameStyle',
                ChoiceType::class,
                ['label' => 'Sintassi dell\'username: (se diversa da quella generale)', 'required' => false,
                      'choices' => ConfigurationManager::$usernameStyles,
                ]
            )
	        ->add(
		        'teacherLdapPasswordOverrideOptions',
		        CheckboxType::class,
		        ['label' => "Utilizza delle regole diverse per la generazione delle password?", 'required' => false]
	        )
	        ->add('teacherLdapPasswordMinChar', IntegerType::class, ['label' => 'Numero minimo dei caratteri:', 'required' => false] )
	        ->add('teacherLdapPasswordComplexity', CheckboxType::class, ['label' => 'Necessaria password complessa.', 'required' => false])
	        ->add('teacherLdapPasswordUseDictionary', CheckboxType::class, ['label' => 'Utilizza i nomi delle province per generare una password semplice da ricordare', 'required' => false])
	        ->add('teacherLdapPasswordPrefix', TextType::class, ['label' => 'Prefisso da anteporre alle password generate', 'required' => false])
            ->add(
                'teacherForceImportedPasswordInitial',
                CheckboxType::class,
                ['label' => "Imposta la password proveniente dal provider alla creazione dell'utente?", 'required' => false]
            )
            ->add(
                'teacherForceImportedPassword',
                CheckboxType::class,
                ['label' => 'ReImpostare la password proveniente dal provider ad ogni importazione?', 'required' => false]
            )
	        ->add(
				'teacherHaveManagedDevice', CheckboxType::class,
		        ['label' => 'I docenti usano un dispositivo gestito?', 'required' => false]
	        )
	        ->add(
				'teacherCanEnablePersonalDevicesForClassrooms', CheckboxType::class,
		        ['label' => 'I docenti possono abilitare i dispositivi personali alle intere classi?', 'required' => false]
	        )
            ->add(
                'teacherLdapExtendedUsernameSuffix',
                TextType::class,
                ['label' => "Suffisso terminale/dominio per l'username esteso Active Directory (lasciare vuoto per usare il nome dominio generale)", 'required' => false]
            )
            ->add(
                'teacherLdapUseEmailForExtendedUsername',
                CheckboxType::class,
                ['label' => "Usa la prima parte dell'email come username esteso di Active Directory?", 'required' => false]
            )
            ->add(
                'teacherLdapCreateOU',
                CheckboxType::class,
                ['label' => 'Crea l\'unità organizzativa', 'required' => false]
            )
            ->add(
                'teacherLdapOUPrefix',
                TextType::class,
                ['label' => 'Nome dell\'unità organizzativa', 'required' => false, 'trim' => false, 'empty_data' => 'Teachers']
            )
            ->add(
                'teacherLdapOUPath',
                TextType::class,
                ['label' => 'Percorso delle unità organizzativa', 'required' => false]
            )
            ->add('teacherGoogleAppClientId', TextType::class, ['label' => 'ClientId Google Apps (lasciare vuoto per disattivare)', 'required' => false])
            ->add('teacherGoogleAppDomain', TextType::class, ['label' => 'Dominio Google Apps', 'required' => false])
            ->add('teacherGoogleAppClientSecret', TextType::class, ['label' => 'Client Secret Google Apps', 'required' => false])
            ->add(
                'teacherGoogleAppAutoEmail',
                ChoiceType::class,
                ['label' => 'Comportamento per la creazione degli indirizzi email', 'required' => false, 'choices' => [
                    'Crea solo gli indirizzi email presenti nel provider' => 0,
                    'Crea gli indirizzi presenti nel provider e usa lo username se mancano' => 1,
                    'Crea gli indirizzi email usando lo username e non considerando quanto presente nel provider' => 2,
                    'Crea gli indirizzi usando una sintassi specificata' => 4,
                    'Crea gli indirizzi presenti nel provider oppure crea gli indirizzi usando una sintassi specificata' => 5,
                    'Non creare nulla, sospendi la creazione degli indirizzi email' => 3,
                ]]
            )
            ->add(
                'teacherGoogleAppAutoEmailStyle',
                ChoiceType::class,
                ['label' => 'Sintassi dell\'indirizzo email:', 'required' => false,
                      'choices' => ConfigurationManager::$usernameStyles,
                ]
            )
            ->add(
                'teacherGoogleAppCreateGroup',
                CheckboxType::class,
                ['label' => 'Crea i gruppi di distribuzione', 'required' => false]
            )
            ->add(
                'teacherGoogleAppCreateProviderGroup',
                CheckboxType::class,
                ['label' => 'Crea il gruppo del provider', 'required' => false]
            )
            ->add(
                'teacherGoogleAppUseUserInProviderGroup',
                CheckboxType::class,
                ['label' => 'Inserisci gli utenti al posto dei gruppi del gruppo provider', 'required' => false]
            )
            ->add(
                'teacherGoogleAppGroupPrefix',
                TextType::class,
                ['label' => 'Prefisso al nome dei gruppi di distribuzione:', 'required' => false, 'trim' => false]
            )
            ->add(
                'teacherGoogleAppGroupExtraEmail',
                TextareaType::class,
                ['label' => 'Lista di email da aggiungere in ogni gruppo di distribuzione: (separate da virgola)', 'required' => false]
            )
            ->add(
                'teacherGoogleAppProviderGroupExtraEmail',
                TextareaType::class,
                ['label' => 'Lista di email da aggiungere nel gruppo del provider: (separate da virgola)', 'required' => false]
            )
            ->add(
                'teacherGoogleAppCreateOU',
                CheckboxType::class,
                ['label' => 'Crea l\'unità organizzativa', 'required' => false]
            )
            ->add(
                'teacherGoogleAppOUPrefix',
                TextType::class,
                ['label' => 'Nome dell\'unità organizzativa', 'required' => false, 'empty_data' => 'Teachers']
            )
            ->add(
                'teacherGoogleAppOUPath',
                TextType::class,
                ['label' => 'Percorso delle unità organizzativa', 'required' => true, 'empty_data' => '/']
            )

	        ->add(
		        'teacherTrashedLdapBehaviour',
		        ChoiceType::class,
		        ['label' => 'Comportamento su LDAP/Active Directory per i docenti cestinati', 'required' => false, 'choices' => [
			        'Non fare nulla' => 0,
			        'Disabilita' => 1,
			        'Cancella' => 2,
		        ]]
	        )
	        ->add(
		        'teacherTrashedLdapOUPath',
		        TextType::class,
		        ['label' => 'Percorso unità organizzativa LDAP in cui spostare gli utenti cestinati (lasciar vuoto per non spostare)', 'required' => false, 'trim' => false]
	        )
	        ->add(
		        'teacherTrashedLdapGroupName',
		        TextType::class,
		        ['label' => 'Nome del gruppo LDAP in cui aggiungere i docenti (lasciar vuoto per non aggiungere)', 'required' => false, 'trim' => false]
	        )
	        ->add(
		        'teacherTrashedLdapLeaveInClassesGroup',
		        CheckboxType::class,
		        ['label' => 'Lascia i docenti cestinati nei gruppi classe su LDAP (finché non viene rimosso l\'abbinamento sul provider)', 'required' => false]
	        )
	        ->add(
		        'teacherTrashedLdapLeaveInProviderGroup', //fare sparire se non selezionato il metti i singoli utenti nel provider?
		        CheckboxType::class,
		        ['label' => 'Lascia i docenti cestinati nei gruppi provider su LDAP', 'required' => false]
	        )

	        ->add(
		        'teacherTrashedGoogleAppBehaviour',
		        ChoiceType::class,
		        ['label' => 'Comportamento su Google Workspace per i docenti cestinati', 'required' => false, 'choices' => [
			        'Non fare nulla' => 0,
			        'Disabilita' => 1,
			        'Cancella' => 2,
		        ]]
	        )
	        ->add(
		        'teacherTrashedGoogleAppOUPath',
		        TextType::class,
		        ['label' => 'Percorso unità organizzativa Google Workspace in cui spostare gli utenti cestinati (lasciar vuoto per non spostare)', 'required' => false, 'trim' => false]
	        )
	        ->add(
		        'teacherTrashedGoogleAppGroupName',
		        TextType::class,
		        ['label' => 'Nome del gruppo Google Workspace in cui aggiungere i docenti (lasciar vuoto per non aggiungere)', 'required' => false, 'trim' => false]
	        )
	        ->add(
		        'teacherTrashedGoogleAppLeaveInClassesGroup',
		        CheckboxType::class,
		        ['label' => 'Lascia i docenti cestinati nei gruppi classe su Google Workspace (finché non viene rimosso l\'abbinamento sul provider)', 'required' => false]
	        )
	        ->add(
		        'teacherTrashedGoogleAppLeaveInProviderGroup', //fare sparire se non selezionato il metti i singoli utenti nel provider?
		        CheckboxType::class,
		        ['label' => 'Lascia i docenti cestinati nei gruppi provider su Google Workspace', 'required' => false]
	        )
	        ->add(
		        'teacherTrashedAsmBehaviour',
		        ChoiceType::class,
		        ['label' => 'Comportamento su AppleSchool per i docenti cestinati', 'required' => false, 'choices' => [
			        'Non fare nulla' => 0,
			        'Cancella' => 1,
		        ]]
	        )
	        ->add(
		        'teacherTrashedAsmLeaveInClassesGroup',
		        CheckboxType::class,
		        ['label' => 'Lascia i docenti cestinati nei gruppi classe su Asm (finché non viene rimosso l\'abbinamento sul provider)', 'required' => false]
	        )
	        ->add(
		        'teacherTrashedDisableRestoreIfPresentOnProvider',
		        CheckboxType::class,
		        ['label' => 'Disabilita il ripristino dal cestino per i docenti che riappaiono nel provider d\'origine', 'required' => false]
	        )
        ;

        $builder
            ->add(
                'internetTeachersControl',
                CheckboxType::class,
                ['label' => 'Gli insegnanti possono controllare l\'accesso ad internet degli studenti?', 'required' => false]
            )
            ->add(
                'internetOpenAccessRange',
                TextareaType::class,
                ['label' => 'Orari in cui internet è aperto per tutti gli studenti:', 'required' => false]
            );

        $pageSizes = [];
        foreach (CPDF::$PAPER_SIZES as $name => $size) {
            $pageSizes[$name] = $name;
        }

        $builder
            ->add('pdfHeader', CKEditorType::class, ['label' => 'Testata nelle comunicazioni stampate', 'required' => false])
            ->add('pdfHeaderHeight', IntegerType::class, ['label' => 'Altezza in px della testata', 'required' => false])
            ->add('pdfFooter', CKEditorType::class, ['label' => 'Fondo pagina nelle comunicazioni stampate', 'required' => false])
            ->add('pdfFooterHeight', IntegerType::class, ['label' => 'Altezza in px del fondo pagina', 'required' => false])
            ->add('pdfStudentBadge', CKEditorType::class, ['label' => 'Testo badge studenti', 'required' => false])
            ->add('pdfStudentBadgePageSize', ChoiceType::class, ['label' => 'Tipologia di carta per badge studenti', 'choices' => $pageSizes, 'preferred_choices' => ['a4']])
            ->add('pdfStudentBadgePageLandscape', CheckboxType::class, ['label' => 'Orientamento Orizzontale della carta per badge studenti', 'required' => false])
        ;
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
        'filterProviderData' => [],
    ]);
    }

    public function setDefaultOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(
            [
                'data_class' => 'App\Entity\Provider',
            ]
        );
    }

    /**
     * @return string
     */
    public function getName()
    {
        return 'zen_igroovebundle_provider';
    }

    /**
     * @param mixed $filterProviderData
     */
    public function setFilterProviderData($filterProviderData)
    {
        $this->filterProviderData = $filterProviderData;
    }

    protected function generateFilterSelect()
    {
        $options = [];
        foreach ($this->filterProviderData as $id => $data) {
            $options[$data['name']] = $id;
        }

        return $options;
    }

    protected function generateFilterForm(FormBuilderInterface $builder)
    {
        $container = $builder->create('filterDataCont', null, ['compound' => true, 'by_reference' => false, 'mapped' => false, 'label' => false, 'row_attr' => ['class' => " "]]);
        foreach ($this->filterProviderData as $id => $data) {
            $form = $builder->create($id, null, ['compound' => true, 'by_reference' => false, 'mapped' => false, 'label' => false, 'row_attr' => ['class' => " "]]);
            foreach ($data['ui'] as $uiId => $uiData) {
                if (PasswordType::class == $uiData['type']) {
                    $form->add($uiId, $uiData['type'], ['label' => $uiData['title'], 'attr' => ['class' => 'form-control mb-3'], 'always_empty' => false]);
                } elseif (ChoiceType::class == $uiData['type']) {
                    $form->add($uiId, $uiData['type'], ['label' => $uiData['title'], 'attr' => ['class' => 'form-select mb-3'], 'choices' => $uiData['choices']]);
                } else {
                    $form->add($uiId, $uiData['type'], ['label' => $uiData['title'], 'attr' => ['class' => 'form-control mb-3']]);
                }
            }
            $container->add($form);
        }
        $builder->add($container);
    }
}
