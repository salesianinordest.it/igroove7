<?php

namespace App\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\ButtonType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;

class MikrotikPoolType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
	        ->add('nome', null, ['required' => true])
            ->add('mikrotik')
            ->add(
                'dhcpServerName',
                TextType::class /*
                , ['attr' => ['input_group' => [
                    'button_append' => [
                        'name' => 'searchDhcpServerNameBt',
                        'type' => ButtonType::class,
                        'options' => [
                            'label' => 'Cerca',//<span class="glyphicon glyphicon-search"></span>
//                    'attr' => ['data-toggle' => 'modal', 'data-target' => '#searchDhcpServerNameModal']
                        ]
                    ]
                ]
                ]
                ] */
            )
            ->add('dottedIpStart')
            ->add('dottedIpEnd')
            ->add('creareSuMikrotik', null, ['label' => 'Creare automaticamente su mikrotik']);
    }

    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver->setDefaults([
            'data_class' => 'App\Entity\MikrotikPool',
        ]);
    }

    /**
     * @return string
     */
    public function getName()
    {
        return 'zen_igroovebundle_mikrotikpool';
    }
}
