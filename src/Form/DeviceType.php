<?php

namespace App\Form;

use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\CollectionType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Form\FormEvent;
use Symfony\Component\Form\FormEvents;
use Symfony\Component\OptionsResolver\OptionsResolver;

class DeviceType extends AbstractType
{
    private $entityManager;

    public function __construct(EntityManagerInterface $entityManager) // Create the constructor if not exist and add the entity manager as first parameter (we will add it later)
    {
        $this->entityManager = $entityManager;
    }

    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
	        ->add('device', null, array('label' => 'Dispositivo', 'required' => true))
	        ->add('mac', null, array('label' => 'Indirizzo MAC', 'required' => true))
            ->add('mikrotikList', null, [
                'label' => 'Lista IP su mikrotik (opzionale)',
                'required' => false,
                'query_builder' => function ($er) {
                    return $er->createQueryBuilder('p')->orderBy('p.nome', 'ASC');
                }, ])
            ->add('ips', CollectionType::class, [
                'label' => 'Indirizzi IP riservati (opzionale)',
                'required' => false,
                'by_reference' => false,
                'allow_add' => true,
                'allow_delete' => true,
                'entry_type' => "App\Form\DeviceIpType",
            ])
            ->add('bypassHotspot', null, ['label' => 'Attiva bypass su hotspot'])
            ->add('active', null, ['label' => 'Attivo']);

        $builder->addEventListener(FormEvents::PRE_SUBMIT, function (FormEvent $event) {
            $data = $event->getData();
            if (isset($data['mac'])) {
                $data['mac'] = strtoupper(preg_replace("/^(?:([a-fA-F0-9]{2})[:|\-]?)(?:([a-fA-F0-9]{2})[:|\-]?)(?:([a-fA-F0-9]{2})[:|\-]?)(?:([a-fA-F0-9]{2})[:|\-]?)(?:([a-fA-F0-9]{2})[:|\-]?)(?:([a-fA-F0-9]{2})[:|\-]?)/", '$1:$2:$3:$4:$5:$6', $data['mac']));
                $event->setData($data);
            }
        });
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(
            [
                'data_class' => 'App\Entity\Device',
            ]
        );
    }

    public function getName()
    {
        return 'zen_igroovebundle_mactype';
    }
}
