<?php

namespace App\Command;

use App\Entity\Radacct;
use App\Manager\AppleSchoolManager;
use App\Manager\ConfigurationManager;
use Doctrine\ORM\EntityManagerInterface;
use Psr\Log\LoggerInterface;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Command\Command;

class FreeradiusCleanCommand extends Command {

	protected EntityManagerInterface $em;
	protected ConfigurationManager $configurationManager;

	public function __construct(EntityManagerInterface $em, ConfigurationManager $configurationManager, $name = NULL) {
		$this->em = $em;
		$this->configurationManager = $configurationManager;
		parent::__construct($name);
	}

	/**
	 * {@inheritdoc}
	 */
	protected function configure() {
		$this->setName('freeradius-clear')
			->setDescription('Clean old freeradius logs')
		;
	}

	/**
	 * {@inheritdoc}
	 */
	protected function execute(InputInterface $input, OutputInterface $output) : int
	{
		if($this->configurationManager->getFreeradiusKeepDays() < 1) {
			$output->writeln("Cleaning disabled");
			return Command::SUCCESS;
		} else {
			$output->writeln("Cleaning started: cleaning logs older than {$this->configurationManager->getFreeradiusKeepDays()} days");
		}

		$day = (new \DateTime("-{$this->configurationManager->getFreeradiusKeepDays()} days"))->setTime(0,0,0)->format("Y-m-d");

		$queryBuilder = $this->em->createQueryBuilder();
		$queryBuilder->delete(Radacct::class, 'r');
		$queryBuilder->where('r.acctstarttime < :day');
		$queryBuilder->setParameter('day', $day);
		$rowCount = $queryBuilder->getQuery()->getSingleScalarResult();

		$output->writeln("Cleaning completed. Removed {$rowCount} rows");
		return Command::SUCCESS;
	}

}