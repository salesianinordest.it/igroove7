<?php

namespace App\Command;

use App\Manager\MikrotikManager;
use App\Manager\VersionManager;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

class TestVersionCommand extends Command
{
    /**
     * @var VersionManager
     */
    protected $versionManager;

    /**
     * @param ContainerInterface $container
     */
    public function __construct(VersionManager $versionManager)
    {
        $this->versionManager = $versionManager;
        parent::__construct();
    }

    protected function configure()
    {
        $this->setName('test:version')
            ->setDescription('test');
    }

    protected function execute(InputInterface $input, OutputInterface $output) : int
    {

       $version=$this->versionManager->getVersions(true);
        $output->writeln([
            '<info>Versione installata               Versione disponibile</>',
            '<info>==============================    ==============================</>',
            substr(str_pad('Versione: '.$version['versionInstalled'],30),0,30).'    '.
            substr(str_pad('Versione: '.$version['versionAvailable'],30),0,30),

            substr(str_pad('Data: '.$version['versionDateInstalled'],30),0,30).'    '.
            substr(str_pad('Data: '.$version['versionDateAvailable'],30),0,30),

            '',

        ]);
        return Command::SUCCESS;
    }
}
