<?php

namespace App\Command;

use App\Manager\PasswordGenerator;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

class TestPasswordGeneratorCommand extends Command
{
    protected $passwordGenerator;

    /**
     * @param ContainerInterface $container
     */
    public function __construct(PasswordGenerator $passwordGenerator)
    {
        $this->passwordGenerator = $passwordGenerator;
        parent::__construct();
    }

    protected function configure()
    {
        $this->setName('test:password')
            ->setDescription('test');
    }

    protected function execute(InputInterface $input, OutputInterface $output) : int
    {
        $output->writeln('<info>Non complessa e senza dizionario</info>');
        $output->writeln($this->passwordGenerator->getRandomPassword(false, false, 8));
        $output->writeln($this->passwordGenerator->getRandomPassword(false, false, 8));
        $output->writeln($this->passwordGenerator->getRandomPassword(false, false, 8));
        $output->writeln('<info>Non complessa con dizionario</info>');
        $output->writeln($this->passwordGenerator->getRandomPassword(false, true, 8));
        $output->writeln($this->passwordGenerator->getRandomPassword(false, true, 8));
        $output->writeln($this->passwordGenerator->getRandomPassword(false, true, 8));
        $output->writeln('<info>Complessa e senza dizionario</info>');
        $output->writeln($this->passwordGenerator->getRandomPassword(true, false, 8));
        $output->writeln($this->passwordGenerator->getRandomPassword(true, false, 8));
        $output->writeln($this->passwordGenerator->getRandomPassword(true, false, 8));
        $output->writeln('<info>Complessa e con dizionario</info>');
        $output->writeln($this->passwordGenerator->getRandomPassword(true, true, 8));
        $output->writeln($this->passwordGenerator->getRandomPassword(true, true, 8));
        $output->writeln($this->passwordGenerator->getRandomPassword(true, true, 8));

	    $output->writeln('<info>Non complessa e senza dizionario con prefisso Pr</info>');
	    $output->writeln($this->passwordGenerator->getRandomPassword(false, false, 8, 'Pr'));
	    $output->writeln('<info>Non complessa con dizionario con prefisso Pr</info>');
	    $output->writeln($this->passwordGenerator->getRandomPassword(false, true, 8, 'Pr'));
	    $output->writeln('<info>Complessa e senza dizionario con prefisso Pr</info>');
	    $output->writeln($this->passwordGenerator->getRandomPassword(true, false, 8, 'Pr'));
	    $output->writeln('<info>Complessa e con dizionario con prefisso Pr</info>');
	    $output->writeln($this->passwordGenerator->getRandomPassword(true, true, 8, 'Pr'));

        return Command::SUCCESS;
    }
}
