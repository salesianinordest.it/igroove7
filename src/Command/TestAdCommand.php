<?php

namespace App\Command;

use App\Entity\LdapUser;
use App\Manager\ConfigurationManager;
use App\Manager\MicrosoftLdapService;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

class TestAdCommand extends Command
{
    /**
     * @var ConfigurationManager
     */
    private $configurationManager;

    /**
     * @var MicrosoftLdapService
     */
    private $microsoftLdapService;

    public function __construct(ConfigurationManager $configurationManager, MicrosoftLdapService $microsoftLdapService)
    {
        parent::__construct();
        $this->configurationManager = $configurationManager;
        $this->microsoftLdapService = $microsoftLdapService;
    }

    protected function configure()
    {
        $this->setName('test:ad')
            ->setDescription('test all MS Active directory operations');
    }

    protected function execute(InputInterface $input, OutputInterface $output) : int
    {
        $commandFail = false;

        $this->microsoftLdapService->setParameters(
            $this->configurationManager->getActiveDirectoryConfiguration()
        );
        $output->write('Connection to AD... ');
        try {
            $this->microsoftLdapService->connect();
            $output->writeln('<info>Ok</>');
        } catch (\Exception $e) {
            $output->writeln('<error>Fail</>');
            $commandFail = true;
        }

        $id1 = substr(md5(time()), 0, 4);
        $id2 = substr(md5(time()), 0, 4);
        $username = 'test-user-'.$id1;
        $output->write('Create user... ');
        $ldapUser = new LdapUser();
        $ldapUser->setUsername($username);
        $ldapUser->setFirstname('Firstname '.$id1);
        $ldapUser->setLastname('Test');
        $userAttributes = [
            'description' => 'Creato da igroove'.$this->configurationManager->getActiveDirectorySyncSuffix(),
            'homedrive' => (1 == strlen($this->configurationManager->getActiveDirectoryHomeDrive())) ? strtoupper(
                $this->configurationManager->getActiveDirectoryHomeDrive().':'
            ) : null,
            'homedirectory' => (strlen(
                $this->configurationManager->getActiveDirectoryHomeFolder()
            ) > 2) ? $this->configurationManager->getActiveDirectoryHomeFolder().'\\'.$ldapUser->getUsername() : null,
            'email' => 'no@email',
            'radius-vlan' => null,
            'extendedUsername' => $username,
        ];
        $ldapUser->setAttributes(json_encode($userAttributes));
        $ldapUser->setStartingPassword('Az-18932klhhcksaoi76iu34jkclksjghfdjkashgr7etrgk,jshgfkjsdhgf');
        $ldapUser->setDistinguishedId($id1);

        try {
            $this->microsoftLdapService->createUser($ldapUser);
            $output->writeln('<info>Ok</>');
        } catch (\Exception $e) {
            $output->writeln('<error>Fail</>');
            $commandFail = true;
        }

        $output->write('getAllUsers... ');
        try {
            $users = $this->microsoftLdapService->getAllUsers();
            $count = count($users);
            $output->writeln('<info>Ok</>');
        } catch (\Exception $e) {
            $output->writeln('<error>Fail</>');
            $commandFail = true;
        }

        $output->write('getUser... ');
        try {
            $user = $this->microsoftLdapService->getUser($username);
            if (is_array($user)) {
                $output->writeln('<info>Ok</>');
            } else {
                $output->writeln('<error>Fail</>');
                $commandFail = true;
            }
        } catch (\Exception $e) {
            $output->writeln($e.'<error>Fail</>');
            $commandFail = true;
        }

        $output->write('getObjectGUIDFromUsername...');
        try {
            $userGUID = $this->microsoftLdapService->getObjectGUIDFromUsername($username);
            if (is_string($userGUID)) {
                $output->writeln(' <info>Ok</>');
            } else {
                $output->writeln('<error>Fail</>');
                $commandFail = true;
            }
        } catch (\Exception $e) {
            $output->writeln('<error>Fail</>');
            $commandFail = true;
        }

        $output->write('getUserMembership... ');
        try {
            $user = $this->microsoftLdapService->getUserMembership($username);
            if (is_array($user)) {
                $output->writeln('<info>Ok</>');
            } else {
                $output->writeln('<error>Fail</>');
                $commandFail = true;
            }
        } catch (\Exception $e) {
            $output->writeln('<error>Fail</>');
            $commandFail = true;
        }

        $output->write('modifyUser... ');
        try {
            $ldapUser->setFirstname($ldapUser->getFirstname().' modified 1');
            $this->microsoftLdapService->modifyUser($ldapUser);
            $userAttributes['objectGUID'] = $userGUID;
            $ldapUser->setAttributes(json_encode($userAttributes));
            $ldapUser->setFirstname($ldapUser->getFirstname().' and 2');
            $this->microsoftLdapService->modifyUser($ldapUser);

            $output->writeln('<info>Ok</>');
        } catch (\Exception $e) {
            $output->writeln('<error>Fail</>');
            $commandFail = true;
        }

        $output->write('changeUserPassword... ');
        try {
            $newPassword = 'Az-p938n32iujhf';
            $this->microsoftLdapService->changeUserPassword($username, $newPassword);

            $output->writeln('<info>Ok</>');
        } catch (\Exception $e) {
            $output->writeln('<error>Fail</>');
            $commandFail = true;
        }
        $output->write('checkUserPassword... ');
        try {
            $this->microsoftLdapService->checkUserPassword($username, $newPassword);

            $output->writeln('<info>Ok</>');
        } catch (\Exception $e) {
            $output->writeln('<error>Fail</>');
            $commandFail = true;
        }

        $output->write('createGroup... ');
        $groupName = 'test-group-'.$id1;
        try {
            $this->microsoftLdapService->createGroup($groupName);
            $output->writeln('<info>Ok</>');
        } catch (\Exception $e) {
            $output->writeln('<error>Fail</>');
            $commandFail = true;
        }

        $output->write('getAllGroups... ');
        try {
            $groups = $this->microsoftLdapService->getAllGroups();
            $countGroups = count($groups);
            $output->writeln('<info>Ok</>');
        } catch (\Exception $e) {
            $output->writeln('<error>Fail</>');
            $commandFail = true;
        }

        $output->write('renameGroup... ');
        try {
            $newGroupName = 'test-group-'.$id2;
            $this->microsoftLdapService->renameGroup($groupName, $newGroupName);
            $output->writeln('<info>Ok</>');
        } catch (\Exception $e) {
            $output->writeln('<error>Fail</>');
            $commandFail = true;
        }

        $output->write('updateUsersIntoGroup... ');
        try {
            $this->microsoftLdapService->updateUsersIntoGroup($newGroupName, [$username]);
            $output->writeln('<info>Ok</>');
        } catch (\Exception $e) {
            $output->writeln('<error>Fail</>');
            $commandFail = true;
        }

        $output->write('updateGroupsIntoGroup... ');
        try {
            $groupName2 = 'test-group-MAIN';
            try {
                $this->microsoftLdapService->createGroup($groupName2);
            } catch (\Exception $e) {
            }
            $this->microsoftLdapService->updateGroupsIntoGroup($groupName2, [$newGroupName]);
            try {
                $this->microsoftLdapService->removeGroup($groupName2);
            } catch (\Exception $e) {
            }
            $output->writeln('<info>Ok</>');
        } catch (\Exception $e) {
            $output->writeln('<error>Fail</>');
            $commandFail = true;
        }

        $output->write('getGroupMembership... ');
        try {
            $this->microsoftLdapService->getGroupMembership($newGroupName);
            $output->writeln('<info>Ok</>');
        } catch (\Exception $e) {
            $output->writeln('<error>Fail</>');
            $commandFail = true;
        }

        $output->write('createOu... ');
        try {
            $ouName = 'test-ou-'.$id1;
            $this->microsoftLdapService->createOu($ouName);
            $output->writeln('<info>Ok</>');
        } catch (\Exception $e) {
            $output->writeln($e.'<error>Fail</>');
            $commandFail = true;
        }

        $output->write('ouExists... ');
        try {
            $found = $this->microsoftLdapService->ouExists($ouName);
            if (!$found) {
                $output->writeln('<error>Fail</>');
                $commandFail = true;
            }
            $output->writeln('<info>Ok</>');
        } catch (\Exception $e) {
            $output->writeln($e.'<error>Fail</>');
            $commandFail = true;
        }

        $output->write('moveUserIntoOu... ');
        try {
            $this->microsoftLdapService->moveUserIntoOu($ouName, $username);
            $output->writeln('<info>Ok</>');
        } catch (\Exception $e) {
            $output->writeln($e.'<error>Fail</>');
            $commandFail = true;
        }

        $output->write('moveGroupIntoOu... ');
        try {
            $this->microsoftLdapService->moveGroupIntoOu($ouName, $groupName);
            $output->writeln('<info>Ok</>');
        } catch (\Exception $e) {
            $output->writeln($e.'<error>Fail</>');
            $commandFail = true;
        }

        /*
                      $output->write('dnPathToOUList');
                      $output->write('refreshConnection');
                      $output->write('getConnectedServerHostname');
              */

        $output->write('removeGroup... ');
        try {
            $groups = $this->microsoftLdapService->getAllGroups();
            foreach ($groups as $group) {
                if (false !== strpos($group, 'test-group-')) {
                    $this->microsoftLdapService->removeGroup($group);
                }
            }
            $output->writeln('<info>Ok</>');
        } catch (\Exception $e) {
            $output->writeln('<error>Fail</>');
            $commandFail = true;
        }

        $output->write('removeUser... ');
        try {
            $users = $this->microsoftLdapService->getAllUsers();
            foreach ($users as $user) {
                if (strpos($user['parameters']['description'], 'igroove') &&
                    false !== strpos($user['username'], 'test-')) {
                    $this->microsoftLdapService->removeUser($user['username']);
                }
            }
            $output->writeln('<info>Ok</>');
        } catch (\Exception $e) {
            $output->writeln('<error>Fail</>');
            $commandFail = true;
        }

        $output->write('removeOU... ');
        try {
            $ous = $this->microsoftLdapService->getAllOUs();
            foreach ($ous as $ou) {
                if (false !== strpos($ou, 'test-ou-')) {
                    $this->microsoftLdapService->removeOU($ou);
                }
            }
            $output->writeln('<info>Ok</>');
        } catch (\Exception $e) {
            $output->writeln('<error>Fail</>');
            $commandFail = true;
        }

        return ($commandFail) ? Command::FAILURE : Command::SUCCESS;
    }
}
