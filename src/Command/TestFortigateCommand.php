<?php

namespace App\Command;

use App\Manager\FortigateManager;
use App\Manager\MikrotikManager;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

class TestFortigateCommand extends Command
{
    protected FortigateManager $fortigateManager;

    /**
     * @param ContainerInterface $container
     */
    public function __construct(FortigateManager $fortigateManager)
    {
        $this->fortigateManager = $fortigateManager;
        parent::__construct();
    }

    protected function configure()
    {
        $this->setName('test:fortigate')
            ->setDescription('test');
    }

    protected function execute(InputInterface $input, OutputInterface $output) : int
    {
        $this->fortigateManager->KickOffUsers();

        return Command::SUCCESS;
    }
}
