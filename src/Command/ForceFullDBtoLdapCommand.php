<?php

namespace App\Command;

use App\Entity\Provider;
use App\Manager\AppleSchoolManager;
use App\Manager\ConfigurationManager;
use App\Manager\LdapProxy;
use App\Manager\PersonsAndGroups;
use App\Manager\VersionManager;
use App\Message\LdapMessage;
use Doctrine\ORM\EntityManagerInterface;
use Psr\Log\LoggerInterface;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\Messenger\Envelope;
use Symfony\Component\Messenger\MessageBusInterface;

class ForceFullDBtoLdapCommand extends Command
{
    private $em;
    protected $logger;
    protected $output;
    protected $container;
    protected $versionManager;

    public function __construct(LoggerInterface $logger, EntityManagerInterface $em,MessageBusInterface $messageBus)
    {
        $this->logger = $logger;
        $this->em = $em;
        $this->messageBus = $messageBus;
        parent::__construct();
    }

    protected function configure()
    {
        $this->setName('force-full-db-to-ldap')
            ->setDescription('Force full sync (add MODIFY) from DB to LDAP');
    }

    protected function execute(InputInterface $input, OutputInterface $output) : int
    {

        $this->output = $output;
        $ldapUsers = $this->em->getRepository('App\Entity\LdapUser')->findAll();
        foreach ($ldapUsers as $ldapUser) {
            $ldapUser->setOperation('MODIFY');
        }
        $this->em->flush();

        $this->printAndLogInfo('syncLDAPfromDB</info> via RabbitMQ');

        $msg = new LdapMessage();
        $msg->setAction('syncLDAPfromDB');

        $this->messageBus->dispatch(new Envelope($msg));


       


        $this->printAndLogInfo('All done!');


        return Command::SUCCESS;
    }

    protected function printAndLogInfo($message)
    {
        $this->output->writeln('<info>' . $message . '</info>');
        $this->logger->info('FORCE FULL DB -> LDAP:' . $message);
    }
}
