<?php

namespace App\Command;

use App\Manager\GoogleApps;
use Doctrine\ORM\EntityManagerInterface;
use Psr\Log\LoggerInterface;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

class TestGoogleAppsCommand extends Command
{

	protected EntityManagerInterface $em;
	protected LoggerInterface $logger;

	public function __construct(LoggerInterface $logger, EntityManagerInterface $em, $name = NULL) {
		$this->logger = $logger;
		$this->em = $em;
		parent::__construct($name);
	}
    protected function configure()
    {
        $this->setName('test:google-apps')
            ->setDescription('test');
    }

    protected function execute(InputInterface $input, OutputInterface $output) : int
    {
        $providers = $this->em->getRepository('App\Entity\Provider')->findBy(['active' => true]);
        foreach ($providers as $provider) {
            if ($provider->getStudentGoogleAppDomain()) {
                $output->writeln('<info>'.$provider->getStudentGoogleAppDomain().'</info>');
                $googleApps = new GoogleApps(
                    $provider->getStudentGoogleAppDomain(),
                    $provider->getStudentGoogleAppClientId(),
                    $provider->getStudentGoogleAppclientSecret(),
                    $provider->getStudentGoogleAppOUPath(),
                    '/var/www/igroove',
                    $this->logger
                );
                $n = rand(1, 10);
				$this->doTests($googleApps, 'studente', $n, $output);
            }
            if ($provider->getTeacherGoogleAppDomain()) {
                $output->writeln('<info>'.$provider->getTeacherGoogleAppDomain().'</info>');
                $googleApps = new GoogleApps(
                    $provider->getTeacherGoogleAppDomain(),
                    $provider->getTeacherGoogleAppClientId(),
                    $provider->getTeacherGoogleAppclientSecret(),
                    $provider->getTeacherGoogleAppOUPath(),
                    '/var/www/igroove/src',
                    $this->logger
                );
                $n = rand(1, 10);
	            $this->doTests($googleApps, 'docente', $n, $output);
            }
        }
	    return Command::SUCCESS;
    }

	protected function doTests(GoogleApps $googleApps, string $userType, int $n, OutputInterface $output) {
		$userName = 'test-'.$userType.'-'.$n;
		$groupName = 'test-gruppo-'.$userType.'-'.$n;
		$ouName = 'test-ou-'.$n;
		$waitTime = 3;

		$output->writeln('Crea utente: '.$userName);
		$googleApps->createUser($userName, 'nome'.$n, 'cognome'.$n);
		sleep($waitTime);
		$output->writeln("Verifica esistenza utente {$userName}:");
		$output->writeln($googleApps->userExists($userName) ? 'true' : 'false');

		$output->writeln('Crea gruppo: '.$groupName);
		$googleApps->createGroup($groupName);
		sleep($waitTime);
		$output->writeln("Verifica esistenza gruppo {$groupName}:");
		$output->writeln($googleApps->groupExists($groupName) ? 'true' : 'false');
		$output->writeln('Aggiungi utente al gruppo');
		$googleApps->addUserToGroup($userName, $groupName);

		$output->writeln('Crea OU: '.$ouName);
		$googleApps->createOU($ouName);
		sleep($waitTime);
		$output->writeln("Verifica esistenza ou {$ouName}:");
		$output->writeln($googleApps->ouExists($ouName) ? 'true' : 'false');
		$output->writeln('Sposta utente nella OU');
		$googleApps->addUserToOU($userName, $ouName);
		sleep($waitTime*2);
		$output->writeln("Lista utenti nella OU {$ouName}:");
		$output->writeln(join(",", $googleApps->getUsersInOu($ouName)));

		$output->writeln("Sospendi utente {$userName}");
		$googleApps->setSuspendedUser($userName);
		sleep($waitTime*2);
		$output->writeln("Verifica utente sospeso {$userName}:");
		$output->writeln($googleApps->isUserSuspended($userName) ? 'true' : 'false');
		$output->writeln("Ripristina utente {$userName}");
		$googleApps->setUnsuspendedUser($userName);
		sleep($waitTime*2);
		$output->writeln("Verifica utente sospeso {$userName}:");
		$output->writeln($googleApps->isUserSuspended($userName) ? 'true' : 'false');

		$output->writeln("Reset password utente {$userName}");
		$googleApps->resetUserPassword($userName);

		$output->writeln("Rinomina utente {$userName} a {$userName}-3");
		$googleApps->renameUser($userName."-3", $userName);
		sleep($waitTime);
		$output->writeln("Verifica esistenza utente {$userName}-3:");
		$output->writeln($googleApps->userExists($userName."-3") ? 'true' : 'false');

		$output->writeln('Crea utente: '.$userName."-2");
		$googleApps->createUser($userName."-2", 'nome'.$n, 'cognome'.$n."-2");

		$output->writeln("Sposta utenti nell gruppo");
		$googleApps->updateGroupMembers([$userName."-3", $userName."-2"], $groupName);
		sleep($waitTime*2);
		$output->writeln("Lista utenti nel gruppo {$groupName}:");
		$output->writeln(join(",", $googleApps->getUsersInGroup($groupName)));

		$output->writeln("Sposta utenti nella OU");
		$googleApps->updateOUMembers([$userName."-3", $userName."-2"], $ouName);
		sleep($waitTime);
		$output->writeln("Lista utenti nella OU {$ouName}:");
		$output->writeln(join(",", $googleApps->getUsersInOu($ouName)));

		$output->writeln("Rinomina gruppo {$groupName} a {$groupName}-2");
		$googleApps->renameGroup($groupName."-2", $groupName);
		sleep($waitTime);
		$output->writeln("Verifica esistenza gruppo {$groupName}-2:");
		$output->writeln($googleApps->groupExists($groupName."-2") ? 'true' : 'false');

		$output->writeln("Rimuovo utente {$userName}-2");
		$googleApps->removeUser($userName."-2");
		$output->writeln("Rimuovo utente {$userName}-3");
		$googleApps->removeUser($userName."-3");

		$output->writeln("Rimuovo gruppo {$groupName}-2");
		$googleApps->removeGroup($groupName."-2");
	}
}
