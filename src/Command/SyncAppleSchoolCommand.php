<?php

namespace App\Command;

use App\Entity\AppleSchool;
use App\Manager\AppleSchoolManager;
use Doctrine\ORM\EntityManagerInterface;
use Psr\Log\LoggerInterface;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Command\Command;

class SyncAppleSchoolCommand extends Command {

	protected AppleSchoolManager $appleSchoolManager;
	protected EntityManagerInterface $em;
	protected LoggerInterface $logger;

	public function __construct(AppleSchoolManager $appleSchoolManager, LoggerInterface $logger, EntityManagerInterface $em, $name = NULL) {
		$this->appleSchoolManager = $appleSchoolManager;
		$this->logger = $logger;
		$this->em = $em;
		parent::__construct($name);
	}

	/**
	 * {@inheritdoc}
	 */
	protected function configure() {
		$this->setName('sync-apple-school')
			->setDescription('Sync the apple school data')
		;
	}

	/**
	 * {@inheritdoc}
	 */
	protected function execute(InputInterface $input, OutputInterface $output) : int
	{
		$output->writeln("Inizio sincronizzazione server mdm");

		$appleSchools = $this->em->getRepository(AppleSchool::class)->findBy(['active' => true]);
		foreach ($appleSchools as $appleSchool) {
			$output->writeln("Sincronizzo {$appleSchool->getOrganizationName()}");

			try {
				$this->appleSchoolManager->generateAndUploadCsvFilesFor($appleSchool);
				$output->writeln("- Sincronizzazione completata");
			} catch (\Exception $e) {
				$output->writeln("- Si son verificati errori durante la sincronizzazione: ".$e->getMessage());
				$this->logger->error("Error during apple school syncronization: ({$e->getFile()}:{$e->getLine()}): ".$e->getMessage(), ['exception' => $e]);
			}
		}

		$output->writeln("Sincronizzazione completa");
		return Command::SUCCESS;
	}

}