<?php

namespace App\Command;

use App\Manager\MikrotikManager;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

class TestMikrotikCommand extends Command
{
    protected $mikrotikManager;

    /**
     * @param ContainerInterface $container
     */
    public function __construct(MikrotikManager $mikrotikManager)
    {
        $this->mikrotikManager = $mikrotikManager;
        parent::__construct();
    }

    protected function configure()
    {
        $this->setName('test:mikrotik')
            ->setDescription('test');
    }

    protected function execute(InputInterface $input, OutputInterface $output) : int
    {
        // var_dump($this->mikrotikManager->addPools());
        var_dump($this->mikrotikManager->manageMacToHotspot());
        // var_dump($this->mikrotikManager->addIpToIpList());
        // var_dump($this->mikrotikManager->addIpReservation());
        return Command::SUCCESS;
    }
}
