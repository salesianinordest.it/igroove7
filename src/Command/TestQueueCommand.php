<?php

namespace App\Command;

use App\Message\GoogleMessage;
use App\Message\LdapMessage;
use App\Message\MikrotikMessage;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Messenger\Envelope;
use Symfony\Component\Messenger\MessageBusInterface;

class TestQueueCommand extends Command
{
    public function __construct(MessageBusInterface $messageBus)
    {
        $this->messageBus = $messageBus;
        parent::__construct();
    }

    protected function configure()
    {
        $this->setName('test:queues')
            ->setDescription('test all queues');
    }

    protected function execute(InputInterface $input, OutputInterface $output) : int
    {
        $output->writeln('<info>Send MikrotikMessage</info>');
        $message = new MikrotikMessage();
        $this->messageBus->dispatch(new Envelope($message));
        $output->writeln('<info>Send LdapMessage</info>');
        $message = new LdapMessage();
        $this->messageBus->dispatch(new Envelope($message));
        $output->writeln('<info>Send GoogleMessage</info>');
        $message = new GoogleMessage();
        $this->messageBus->dispatch(new Envelope($message));

        return Command::SUCCESS;
    }
}
