<?php

namespace App\Command;

use App\Manager\FortigateManager;
use App\Manager\MikrotikManager;
use App\Manager\WatchguardManager;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

class TestWatchguardCommand extends Command
{
    protected WatchguardManager $watchguardManager;

    /**
     * @param ContainerInterface $container
     */
    public function __construct(WatchguardManager $watchguardManager)
    {
        $this->watchguardManager = $watchguardManager;
        parent::__construct();
    }

    protected function configure()
    {
        $this->setName('test:watchguard');
    }

    protected function execute(InputInterface $input, OutputInterface $output) : int
    {
        $this->watchguardManager->KickOffUsers();

        return Command::SUCCESS;
    }
}
