<?php

namespace App\Command;

use App\Entity\MdmServer;
use App\Manager\MdmServerManager;
use Doctrine\ORM\EntityManagerInterface;
use Psr\Log\LoggerInterface;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

class SyncMdmServerCommand extends Command {

	protected MdmServerManager $mdmServerManager;
	protected EntityManagerInterface $em;
	protected LoggerInterface $logger;

	public function __construct(MdmServerManager $appleSchoolManager, LoggerInterface $logger, EntityManagerInterface $em, $name = NULL) {
		$this->mdmServerManager = $appleSchoolManager;
		$this->logger = $logger;
		$this->em = $em;
		parent::__construct($name);
	}

	/**
     * {@inheritdoc}
     */
    protected function configure() {
        $this->setName('sync-mdm-servers')
            ->setDescription('Sync the mdm servers data')
            ->addOption("fullSync", "f")
        ;
    }

    /**
     * {@inheritdoc}
     */
    protected function execute(InputInterface $input, OutputInterface $output) : int
    {
        $output->writeln("Inizio sincronizzazione server mdm");

        $mdmServers = $this->em->getRepository(MdmServer::class)->findBy(['active' => true]);
        foreach ($mdmServers as $mdmServer) {
            $output->writeln("Sincronizzo {$mdmServer->getName()}");

            try {
                $this->mdmServerManager->importFromServer($mdmServer, $input->getOption('fullSync'));
                $output->writeln("- Sincronizzazione server completata");
            } catch (\Exception $e) {
                $output->writeln("- Si sono verificati errori durante l'importazione: ".$e->getMessage());
                $this->logger->error("Error during mdm syncronization: ({$e->getFile()}:{$e->getLine()}): ".$e->getMessage(), ['exception' => $e]);
            }
        }

        $output->writeln("Sincronizzazione completa");
	    return Command::SUCCESS;
    }
}
