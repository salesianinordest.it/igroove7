<?php

namespace App\Command;

use App\Entity\LdapGroup;
use App\Manager\CoaManager;
use App\Manager\ConfigurationManager;
use App\Manager\LdapProxy;
use App\Manager\MikrotikManager;
use App\Manager\FortigateManager;
use App\Manager\PersonsAndGroups;
use App\Manager\WatchguardManager;
use App\Message\LdapMessage;
use App\Message\MikrotikMessage;
use Doctrine\ORM\EntityManagerInterface;
use Psr\Log\LoggerInterface;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\Messenger\Envelope;
use Symfony\Component\Messenger\MessageBusInterface;

class InternetCommand extends Command
{
    /**
     * @var \App\Manager\PersonsAndGroups
     */
    private $personsAndGroups;
    /**
     * @var \App\Manager\LdapProxy
     */
    private $ldapProxy;
    private $configurationManager;
    private $em;
    protected $logger;
    /**
     * @var OutputInterface
     */
    protected $output;
    /**
     * @var \App\Repository\InternetOpenRepository
     */
    protected $internetOpenRepository;
    /**
     * @var \App\Repository\LdapGroupRepository
     */
    protected $ldapGroupRepository;
    protected $adGeneratedGroupPrefix;
    protected $ldapNeedSync = false;
    protected $mikrotikManager;
    protected $fortigateManager;
    protected $coaManager;
    /**
     * @var MessageBusInterface
     */
    protected $messageBus;

    private WatchguardManager $watchguardManager;

    public function __construct(
        LoggerInterface        $logger,
        EntityManagerInterface $em,
        ConfigurationManager   $configurationManager,
        PersonsAndGroups       $personsAndGroups,
        LdapProxy              $ldapProxy,
        MikrotikManager        $mikrotikManager,
        FortigateManager       $fortigateManager,
        WatchguardManager      $watchguardManager,
        CoaManager             $coaManager,
        MessageBusInterface    $messageBus
    )
    {
        $this->logger = $logger;
        $this->em = $em;
        $this->configurationManager = $configurationManager;
        $this->personsAndGroups = $personsAndGroups;
        $this->ldapProxy = $ldapProxy;
        $this->mikrotikManager = $mikrotikManager;
        $this->fortigateManager = $fortigateManager;
        $this->watchguardManager = $watchguardManager;
        $this->coaManager = $coaManager;
        $this->messageBus = $messageBus;
        parent::__construct();

    }

    protected function configure()
    {
        $this
            ->setName('internet')
            ->setDescription('Check default internet behavior');
    }

    protected function execute(InputInterface $input, OutputInterface $output): int
    {
        $this->ldapProxy->connect();

        $this->output = $output;
        $this->printAndLogInfo('Starting internet command');
        $this->adGeneratedGroupPrefix = $this->configurationManager->getActiveDirectoryGeneratedGroupPrefix();
        $this->internetOpenRepository = $this->em->getRepository('App\Entity\InternetOpen');
        $this->ldapGroupRepository = $this->em->getRepository('App\Entity\LdapGroup');

        $this->printAndLogInfo('-Current active users, groups and ip lists:');
        $currentActiveEntities = $this->internetOpenRepository->findAll();
        $currentActiveEntitiesByType = ['group' => [], 'user' => [], 'ipList' => []];
        foreach ($currentActiveEntities as $entity) {
            $currentActiveEntitiesByType[$entity->getType()][] = $entity->getAccount();
        }

        foreach ($currentActiveEntitiesByType as $type => $accounts) {
            if (empty($accounts)) {
                continue;
            }

            $this->printAndLogInfo("--[{$type}] " . implode(' ', $accounts));
        }

        $this->checkProvidersActionsAndStatus();
        $this->checkInternetOpensTimeout();

        if ($this->ldapNeedSync) {
            $this->em->flush();
            $this->printAndLogInfo('-LdapGroup Updated');

            $this->printAndLogInfo('-syncInternetAccessLdapGroup via RabbitMQ');
            $msg = new LdapMessage();
            $msg->setAction('syncInternetAccessLdapGroup');
            $this->messageBus->dispatch(new Envelope($msg));
        }

        $this->printAndLogInfo('-Mikrotik KickOffUsers');
        $this->mikrotikManager->KickOffUsers();
        $this->printAndLogInfo('-Fortigate KickOffUsers');
        $this->fortigateManager->KickOffUsers();
        $this->printAndLogInfo('-WatchGuard KickOffUsers');
        $this->watchguardManager->KickOffUsers();
        $this->printAndLogInfo('All done!');
        echo PHP_EOL;

        $this->em->getRepository('App\Entity\Cron')->setLatestRun('internet');

        return Command::SUCCESS;
    }

    protected function checkProvidersActionsAndStatus()
    {
        // check available command to execute in providers
        $providers = $this->em->getRepository('App\Entity\Provider')->findBy(['active' => true]);
        $nowOneMinuteDown = new \DateTime('now -1 minutes');
        $nowOneMinuteUp = new \DateTime('now +1 minutes');
        $commandsToSet = [];

        foreach ($providers as $provider) {
            if ($provider->getInternetOpenAccessRange() == "") {
                continue;
            }

            $rows = explode("\r\n", trim($provider->getInternetOpenAccessRange()));
            foreach ($rows as $row) {
                if (0 == strlen(trim($row))) {
                    continue;
                }
                list($time, $check) = explode(': ', trim($row));
                $time = trim($time);
                $check = trim($check);
                $timeToCheck = new \DateTime($time);
                if (($timeToCheck >= $nowOneMinuteDown) and ($timeToCheck <= $nowOneMinuteUp)) {
                    $commandsToSet[$provider->getName()] = $check;
                }
            }
        }

        // get current internet access group member
        $internetAccessGroup = $this->getInternetAccessGroup();

        // cycle command to set from provider
        $this->printAndLogInfo('-Check providers command');
        foreach ($commandsToSet as $name => $toSet) {
            $providerGroupMembers = $this->getLdapGroupMembers($name);
            if (empty($providerGroupMembers->group) || !is_array($providerGroupMembers->group)) {
                continue;
            }

            if ('on' == $toSet) {
                // add all classroom groups in the provider into the internet access group
                foreach ($providerGroupMembers->group as $groupName) {
                    $internetAccessGroup->addMember('group', $groupName);
                    $providerClassroomGroupMembers = $this->em->getRepository(LdapGroup::class)->getAllChildrenRecursiveUsers($groupName);
                    try {
                        $this->coaManager->notifyUsersActivation($providerClassroomGroupMembers);
                    } catch (\Exception $e) {
                        $this->logger->warning("Error sending activation notification to the coa system: {$e->getMessage()}");
                    }
                }

                $this->ldapNeedSync = true;
                $this->printAndLogInfo('--Groups added to internetaccess group: ' . implode(',', $providerGroupMembers->group));
            } elseif ('off' == $toSet) {
                foreach ($providerGroupMembers->group as $providerClassroomGroup) {
                    $providerClassroomGroupMembers = $this->getLdapGroupMembers($providerClassroomGroup);

                    // remove user in current classroom group from internetaccess group and internetopen table
                    if (is_array($providerClassroomGroupMembers->user)) {
                        foreach ($providerClassroomGroupMembers->user as $providerClassroomGroupUser) {
                            $iouEntities = $this->internetOpenRepository->findBy(['type' => 'user', 'account' => strtolower($providerClassroomGroupUser)]);
                            foreach ($iouEntities as $entity) {
                                $this->em->remove($entity);
                            }

                            $internetAccessGroup->removeMember('user', $providerClassroomGroupUser);
                            try {
                                $this->coaManager->KickOffUser($providerClassroomGroupUser);
                            } catch (\Exception $e) {
                                $this->logger->warning("Error sending kick off to the coa system: {$e->getMessage()}");
                            }
                        }
                    }

                    // remove the classroom group from internetaccess group and internetopen table
                    $internetAccessGroup->removeMember('group', $providerClassroomGroup);
                    $this->removeCoaFromGroup($providerClassroomGroup);

                    $iogEntities = $this->internetOpenRepository->findBy(['type' => 'group', 'account' => strtolower($providerClassroomGroup)]);
                    foreach ($iogEntities as $entity) {
                        $this->em->remove($entity);
                    }
                    $this->em->flush();

                    $this->ldapNeedSync = true;
                    $this->printAndLogInfo("--Group {$providerClassroomGroup} and his users removed from internetaccess group");
                }
            }
        }
    }

    protected function checkInternetOpensTimeout()
    {
        $internetAccessGroup = $this->getInternetAccessGroup();
        $personalDeviceAccessGroup = $this->getPersonalDeviceAccessGroup();

        // query the expired activation
        $this->printAndLogInfo('-Check single user, group and ip list timeout from internetaccess table');
        $date = new \DateTime();
        $query = $this->internetOpenRepository
            ->createQueryBuilder('i')
            ->select('i.id, i.type, i.account, i.permitPersonalDevices')
            ->where(' i.close_at <= :date')
            ->setParameter('date', $date)
            ->getQuery();
        $internetOpens = $query->getResult();

        $updateIpList = false;
        foreach ($internetOpens as $internetOpen) {
            if ('group' == $internetOpen['type']) {
                $this->printAndLogInfo('--remove group: ' . $internetOpen['account']);

                if ($internetAccessGroup->memberExists('group', $internetOpen['account'])) {
                    $internetAccessGroup->removeMember('group', $internetOpen['account']);
                    $this->ldapNeedSync = true;
                }

                if ($internetOpen['permitPersonalDevices'] && $personalDeviceAccessGroup->memberExists('group', $internetOpen['account'])) {
                    $personalDeviceAccessGroup->removeMember('group', $internetOpen['account']);

                    $this->removeCoaFromGroup($internetOpen['account']);
                    $this->ldapNeedSync = true;
                }
            } elseif ('user' == $internetOpen['type']) {
                $this->printAndLogInfo('--remove user ' . $internetOpen['account']);

                if ($internetAccessGroup->memberExists('user', $internetOpen['account'])) {
                    $internetAccessGroup->removeMember('user', $internetOpen['account']);

                    try {
                        $this->coaManager->KickOffUser($internetOpen['account']);
                    } catch (\Exception $e) {
                        $this->logger->warning("Error sending kick off to the coa system: {$e->getMessage()}");
                    }

                    $this->ldapNeedSync = true;
                }

                if ($internetOpen['permitPersonalDevices'] && $personalDeviceAccessGroup->memberExists('user', $internetOpen['account'])) {
                    $personalDeviceAccessGroup->removeMember('user', $internetOpen['account']);
                    $this->ldapNeedSync = true;
                }
            } elseif ('ipList' == $internetOpen['type']) {
                $this->printAndLogInfo('--remove iplist ' . $internetOpen['account']);
                $updateIpList = true;
            }

            // remove the internetOpen entity
            $internetOpen = $this->em->getReference('App\Entity\InternetOpen', $internetOpen['id']);
            $this->em->remove($internetOpen);
            $this->em->flush();
        }

        if ($updateIpList) {
            $this->printAndLogInfo('-syncInternetAccessLdapGroup via RabbitMQ');
            $msg = new MikrotikMessage();
            $msg->setCommand('addIpToBypassedList');
            $this->messageBus->dispatch(new Envelope($msg));
        }
    }

    protected function getInternetAccessGroup()
    {
        $internetAccessGroup = $this->ldapGroupRepository->findOneBy(['name' => $this->adGeneratedGroupPrefix . 'InternetAccess']);
        if (!$internetAccessGroup instanceof LdapGroup) {
            $internetAccessGroup = $this->personsAndGroups->checkInternetAccessLdapGroups();
        }

        return $internetAccessGroup;
    }

    protected function getPersonalDeviceAccessGroup()
    {
        $personalDeviceAccessGroup = $this->ldapGroupRepository->findOneBy(['name' => $this->adGeneratedGroupPrefix . 'PersonalDeviceAccess']);
        if (!$personalDeviceAccessGroup instanceof LdapGroup) {
            $personalDeviceAccessGroup = $this->personsAndGroups->checkPersonalDeviceAccessLdapGroups();
        }

        return $personalDeviceAccessGroup;
    }

    protected function getLdapGroupMembers($nameOrGroup)
    {
        $groupMembers = $group = null;
        if (is_string($nameOrGroup)) {
            $group = $this->ldapGroupRepository->findOneBy(['name' => $this->adGeneratedGroupPrefix . $this->ldapGroupRepository::ldapEscape($nameOrGroup)]);
        } elseif ($nameOrGroup instanceof LdapGroup) {
            $group = $nameOrGroup;
        }

        if ($group instanceof LdapGroup && '' != $group->getMembers()) {
            $groupMembers = json_decode($group->getMembers());
        }

        if (null === $groupMembers) {
            $groupMembers = new \stdClass();
        }

        if (!isset($groupMembers->group) || null == $groupMembers->group) {
            $groupMembers->group = [];
        } elseif (is_object($groupMembers->group)) {
            $groupMembers->group = (array)$groupMembers->group;
        }

        if (!isset($groupMembers->user)) {
            $groupMembers->user = [];
        } elseif (is_object($groupMembers->user)) {
            $groupMembers->user = (array)$groupMembers->user;
        }

        return $groupMembers;
    }

    protected function printAndLogInfo($message)
    {
        $this->output->writeln('<info>' . $message . '</info>');
        $this->logger->info('CRON:' . $message);
    }

    private function removeCoaFromGroup($groupName)
    {
        $usernames = $this->em->getRepository('App\Entity\LdapGroup')
            ->getAllChildrenRecursiveUsers($groupName);
        foreach ($usernames as $username) {
            $this->coaManager->KickOffUser($username);
        }
    }
}
