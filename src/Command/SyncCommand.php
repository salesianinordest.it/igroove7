<?php

namespace App\Command;

use App\Manager\LdapProxy;
use App\Message\LdapMessage;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\Messenger\Envelope;
use Symfony\Component\Messenger\MessageBusInterface;

class SyncCommand extends Command
{
    protected $ldapProxy;

    public function __construct(LdapProxy $ldapProxy, MessageBusInterface $messageBus)
    {
        $this->ldapProxy = $ldapProxy;
        $this->messageBus = $messageBus;
        parent::__construct();
    }

    protected function configure()
    {
        $this->setName('sync')
            ->setDescription('Sync DB and Ldap and viceversa')
            ->addOption(
                'from',
                null,
                InputOption::VALUE_REQUIRED,
                'Sorgente della sincronizzazione?',
                'DB'
            )
            ->addOption(
                'to',
                null,
                InputOption::VALUE_REQUIRED,
                'Destinazione della sincronizzazione?',
                'LDAP'
            )
            ->addOption(
                'useMQ',
                null,
                InputOption::VALUE_NONE,
                'Usa RabbitMQ'
            );
    }

    protected function execute(InputInterface $input, OutputInterface $output) : int
    {
        $this->ldapProxy->connect();

        $from = $input->getOption('from');
        $to = $input->getOption('to');

        $useMQ = $input->getOption('useMQ');

        $output->writeln('Sync from: <info>'.$from.'</info>');
        $output->writeln('       to: <info>'.$to.'</info>');

        if ($useMQ) {
            $output->writeln('Using RabbitMQ');
        } else {
            $output->writeln('Not using RabbitMQ');
        }

        $syncDirection = $from.$to;

        if ('dbldap' == strtolower($syncDirection)) {
            if ($useMQ) {
                $msg = new LdapMessage();
                $msg->setAction('syncLDAPfromDB');

                $this->messageBus->dispatch(new Envelope($msg));
            } else {
                $this->ldapProxy->syncLDAPfromDB();
            }
        } elseif ('ldapdb' == strtolower($syncDirection)) {
            if ($useMQ) {
                $msg = new LdapMessage();
                $msg->setAction('purgeAndPopulate');

                $this->messageBus->dispatch(new Envelope($msg));
            } else {
                $this->ldapProxy->purgeAndPopulateAll();
            }
        } else {
            return Command::FAILURE;
        }

        return Command::SUCCESS;
    }
}
