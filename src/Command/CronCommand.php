<?php

namespace App\Command;

use App\Entity\Provider;
use App\Manager\AppleSchoolManager;
use App\Manager\LdapProxy;
use App\Manager\PersonsAndGroups;
use App\Manager\VersionManager;
use App\Message\LdapMessage;
use Doctrine\ORM\EntityManagerInterface;
use Psr\Log\LoggerInterface;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\Messenger\Envelope;
use Symfony\Component\Messenger\MessageBusInterface;

class CronCommand extends Command
{
	/**
	 * @var \App\Manager\PersonsAndGroups
	 */
	private $personsAndGroups;
	/**
	 * @var \App\Manager\LdapProxy
	 */
	private $ldapProxy;
	/**
	 * @var AppleSchoolManager
	 */
	private $appleSchool;

	private $em;
	protected $logger;
	protected $output;
	protected $container;
	protected $versionManager;

	public function __construct(ContainerInterface $container, LoggerInterface $logger, EntityManagerInterface $em, PersonsAndGroups $personsAndGroups, LdapProxy $ldapProxy, AppleSchoolManager $appleSchoolManager, MessageBusInterface $messageBus,VersionManager $versionManager)
	{
		$this->logger = $logger;
		$this->em = $em;
		$this->personsAndGroups = $personsAndGroups;
		$this->ldapProxy = $ldapProxy;
		$this->appleSchool = $appleSchoolManager;
		$this->container = $container;
		$this->messageBus = $messageBus;
		$this->versionManager=$versionManager;
		parent::__construct();
	}

	protected function configure()
	{
		$this->setName('cron')
			->setDescription('Importing and parsing');
	}

	protected function execute(InputInterface $input, OutputInterface $output) : int
	{
		$this->ldapProxy->connect();
		$this->output = $output;

		//@todo option to partial sync

		$this->importData();
		$this->syncLdap();
		$this->syncGoogle();
		$this->syncAsm();
		$this->updateVersion();

		$this->printAndLogInfo('All done!');

		return Command::SUCCESS;
	}

	public function importData()
	{
		$providers = $this->em->getRepository('App\Entity\Provider')->findBy(['active' => true]);
		foreach ($providers as $provider) {
			if (!$provider instanceof Provider || !$provider->getFilter()) {
				continue;
			}

			$this->output->writeln("<info>Start Importing data from {$provider->getName()}</info>");
			$this->logger->info("Start Importing data from {$provider->getName()}");
			$filter = clone $this->container->get($provider->getFilter());

			$filter->setParameters($provider->getFilterData());
			try {
				$filter->parseRemoteData();
			} catch (\ErrorException $e) {
				$this->output->writeln("Errore nel parsing del provider {$provider->getName()}: ".$e->getMessage());
				$this->logger->error("Errore nel parsing del provider {$provider->getName()}: ".$e->getMessage());
				continue;
			}
			$this->personsAndGroups->importElements($filter, $provider);
			$this->output->writeln("<info>Ended Importing data from {$provider->getName()}</info>");
			$this->logger->info("Ended Importing data from {$provider->getName()}");
		}
	}

	public function syncLdap()
	{
		$this->printAndLogInfo('purgeAndPopulateAll');
		$this->ldapProxy->purgeAndPopulateAll();

		$this->printAndLogInfo('checkLdapGroups');
		$this->personsAndGroups->checkLdapGroups();

		$this->printAndLogInfo('checkLdapOUs');
		$this->personsAndGroups->checkLdapOUs();
		$this->printAndLogInfo('checkLdapUsers');
		$this->personsAndGroups->checkLdapUsers();

		$this->printAndLogInfo('checkLdapUsersMembership');
		$this->personsAndGroups->checkLdapUsersMembership();

		$this->printAndLogInfo('syncLDAPfromDB via RabbitMQ');
		$msg = new LdapMessage();
		$msg->setAction('syncLDAPfromDB');
		$this->messageBus->dispatch(new Envelope($msg));

		$this->printAndLogInfo('checkLdapUsersOUMembership');
		$this->personsAndGroups->checkLdapUsersOUMembership();
	}

	public function syncGoogle()
	{
		$this->printAndLogInfo('checkGoogleAppsGroups via RabbitMQ');
		$this->personsAndGroups->checkGoogleAppsGroups();
		$this->printAndLogInfo('checkGoogleAppsOUs via RabbitMQ');
		$this->personsAndGroups->checkGoogleAppsOUs();
		$this->printAndLogInfo('checkGoogleAppsUsers via RabbitMQ');
		$this->personsAndGroups->checkGoogleAppsUsers();
		$this->printAndLogInfo('checkGoogleAppsUsersMembershipToGroup via RabbitMQ');
		$this->personsAndGroups->checkGoogleAppsUsersMembershipToGroup();
		$this->printAndLogInfo('checkGoogleAppsUsersMembershipToOU via RabbitMQ');
		$this->personsAndGroups->checkGoogleAppsUsersMembershipToOU();
	}

	public function syncAsm()
	{
		$this->printAndLogInfo('appleSchool generateAndUploadAllCsvFiles');
		$this->appleSchool->generateAndUploadAllCsvFiles();
	}

	public function updateVersion()
	{
		$this->printAndLogInfo('Retrieved available version');
		$this->versionManager->getVersions(true);
	}

	protected function printAndLogInfo($message)
	{
		$this->output->writeln('<info>'.$message.'</info>');
		$this->logger->info('CRON:'.$message);
	}
}
