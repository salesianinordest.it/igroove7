<?php

namespace App\Repository;

use App\Entity\Provider;
use App\Entity\Sector;

class SectorRepository extends \Doctrine\ORM\EntityRepository
{
    public function createWithImportData($importedData, Provider $provider, $idOnProvider)
    {
        if (!$importedData instanceof \App\ImporterFilter\ImportedEntity\Sector) {
            return null;
        }

        $sector = new Sector();
        $sector->setName($importedData->getName());
        $sector->setProvider($provider);
        $sector->setIdOnProvider($idOnProvider);
        $this->getEntityManager()->persist($sector);
        $this->getEntityManager()->flush();

        return $sector;
    }

    public function updateWithImportData(Sector $sector, $newData, Provider $provider)
    {
        if (!$newData instanceof \App\ImporterFilter\ImportedEntity\Sector) {
            return $sector;
        }

        $sector->setName($newData->getName());

        $this->getEntityManager()->flush();

        return $sector;
    }
}
