<?php

namespace App\Repository;

use App\Entity\MikrotikPool;
use Doctrine\ORM\EntityRepository;

class DeviceIpRepository extends EntityRepository
{
    public function getAllIpsInRange($ipStart, $ipEnd)
    {
        $criteria = new \Doctrine\Common\Collections\Criteria();
        $expr = $criteria->expr();
        $criteria->where($expr->andX(
            $expr->gte('ip', $ipStart),
            $expr->lte('ip', $ipEnd)
        ));

        return $this->matching($criteria);
    }

    public function getAllNotAssociatedIpsInRange($ipStart, $ipEnd)
    {
        $criteria = new \Doctrine\Common\Collections\Criteria();
        $expr = $criteria->expr();
        $criteria->where($expr->andX(
            $expr->gte('ip', $ipStart),
            $expr->lte('ip', $ipEnd),
            $expr->isNull('mikrotikPool')
        ));

        return $this->matching($criteria);
    }

    public function getAllAssociatedIpsOutOfRange(MikrotikPool $mikrotikPool, $ipStart, $ipEnd)
    {
        $criteria = new \Doctrine\Common\Collections\Criteria();
        $expr = $criteria->expr();
        $criteria->where($expr->andX(
            $expr->orX(
                $expr->lt('ip', $ipStart),
                $expr->gt('ip', $ipEnd)
            ),
            $expr->eq('mikrotikPool', $mikrotikPool)
        ));

        return $this->matching($criteria);
    }
}
