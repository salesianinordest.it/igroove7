<?php

namespace App\Repository;

use Doctrine\ORM\EntityRepository;

class MikrotikPoolRepository extends EntityRepository
{
    public function searchPoolForIp($ip)
    {
        $em = $this->getEntityManager();
        $criteria = new \Doctrine\Common\Collections\Criteria();
        $expr = $criteria->expr();
        $criteria->where($expr->andX(
            $expr->lte('ipStart', $ip),
            $expr->gte('ipEnd', $ip)
        ));

        $result = $em->getRepository('App\Entity\MikrotikPool')->matching($criteria);

        return $result->first();
    }
}
