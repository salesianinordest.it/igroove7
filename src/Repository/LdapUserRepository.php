<?php

namespace App\Repository;

use App\Entity\LdapUser;
use App\Entity\Student;
use App\Entity\Teacher;
use Doctrine\ORM\EntityRepository;
use Doctrine\ORM\UnexpectedResultException;

class LdapUserRepository extends EntityRepository
{
    protected $usernameCache = null;

    /**
     * Return all the LdapUser that have an operation to do.
     *
     * @return array|LdapUser[]
     */
    public function getAllWithOperation()
    {
        $query = $this->createQueryBuilder('lu')
            ->where('lu.operation IS NOT NULL')
            ->andWhere("lu.operation <>''")
            ->getQuery();

        return $query->getResult();
    }

    public function getInternetStatus($username, $active_directory_generated_group_prefix)
    {
        $em = $this->getEntityManager();
        $ldapGroup = $em->getRepository('App\Entity\LdapGroup')->find(
            $active_directory_generated_group_prefix.'InternetAccess'
        );
		if($ldapGroup) {
			$members = $ldapGroup->getMembersList();
			if ( array_key_exists( 'user', $members ) and in_array( $username, $members['user'] ) ) {
				return 'user-active';
			}
		}

        return 'not-active';
    }

    /**
     * @param $distinguishedId
     *
     * @return LdapUser|null
     */
    public function findOneByDistinguishedId($distinguishedId)
    {
        $query = $this->createQueryBuilder('lu')
            ->where('LOWER(lu.distinguishedId) =  :did ')
            ->setParameter('did', strtolower($distinguishedId))
            ->setMaxResults(1)
            ->getQuery();

        try {
            $ldapUser = $query->getSingleResult();
        } catch (UnexpectedResultException $e) {
            return null;
        }

        return $ldapUser;
    }

    public function usernameAlreadyExists($username)
    {
        $query = $this->getEntityManager()->createQuery('SELECT COUNT(lu.username) FROM App\Entity\LdapUser lu WHERE lu.username = :username');
        $query->setParameter('username', $username);

        return $query->getSingleScalarResult() > 0;
    }

    /**
     * @return Student|Teacher|null
     */
    public function getPersonFromLdapUser(LdapUser $ldapUser)
    {
        $em = $this->getEntityManager();

        $student = $em->getRepository('App\Entity\Student')->findOneBy(['username' => $ldapUser->getUsername()]);
        if ($student instanceof Student) {
            return $student;
        }

        $teacher = $em->getRepository('App\Entity\Teacher')->findOneBy(['username' => $ldapUser->getUsername()]);
        if ($teacher instanceof Teacher) {
            return $teacher;
        }

        return null;
    }
}
