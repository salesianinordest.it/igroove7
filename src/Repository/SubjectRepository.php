<?php

namespace App\Repository;

use App\Entity\Provider;
use App\Entity\Subject;

class SubjectRepository extends \Doctrine\ORM\EntityRepository
{
    public function createWithImportData($importedData, Provider $provider, $idOnProvider)
    {
        if (!$importedData instanceof \App\ImporterFilter\ImportedEntity\Subject) {
            return null;
        }

        $subject = new Subject();
        $subject->setName($importedData->getName());
        $subject->setProvider($provider);
        $subject->setIdOnProvider($idOnProvider);
        $this->getEntityManager()->persist($subject);
        $this->getEntityManager()->flush();

        return $subject;
    }

    public function updateWithImportData(Subject $subject, $newData, Provider $provider)
    {
        if (!$newData instanceof \App\ImporterFilter\ImportedEntity\Subject) {
            return $subject;
        }

        $subject->setName($newData->getName());

        $this->getEntityManager()->flush();

        return $subject;
    }
}
