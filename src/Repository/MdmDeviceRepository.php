<?php

namespace App\Repository;

use Doctrine\DBAL\Driver\AbstractMySQLDriver;
use Doctrine\ORM\EntityRepository;
use Doctrine\ORM\Query;
use Symfony\Component\Uid\Uuid;

class MdmDeviceRepository extends EntityRepository {

    /**
     * @param array $filters
     * @return Query
     */
    public function getDevicesWithOwnersQueryFromFilters(array $filters = []): Query {
        $queryBuilder = $this->createQueryBuilder('md');

		$studentOwner = !isset($filters['ownerType']) || $filters['ownerType'] == "" || $filters['ownerType'] == "student";
		$teacherOwner = !isset($filters['ownerType']) || $filters['ownerType'] == "" || $filters['ownerType'] == "teacher";

        if($studentOwner) {
            $queryBuilder->leftJoin('md.studentOwner', 'student');
            $queryBuilder->leftJoin('student.memberOf', 'grp');
        }

        if($teacherOwner) {
	        $queryBuilder->leftJoin('md.teacherOwner', 'teacher');
        }

        if(isset($filters['q']) && $filters['q'] != "") {
            $orX = $queryBuilder->expr()->orX(
                $queryBuilder->expr()->like("md.name",":qs"),
                $queryBuilder->expr()->like("md.serial",":qs"),
                $queryBuilder->expr()->like("md.mac",":qs")
            );

            if($studentOwner) {
                $orX->add($queryBuilder->expr()->like("student.username",":qs"));
                $orX->add($queryBuilder->expr()->like("student.firstname",":qs"));
                $orX->add($queryBuilder->expr()->like("student.lastname",":qs"));
                $orX->add($queryBuilder->expr()->like("grp.name",":qs"));
            }

            if($teacherOwner) {
                $orX->add($queryBuilder->expr()->like("teacher.username",":qs"));
                $orX->add($queryBuilder->expr()->like("teacher.firstname",":qs"));
                $orX->add($queryBuilder->expr()->like("teacher.lastname",":qs"));
            }

            $queryBuilder->where($orX)
                        ->setParameter("qs", "%{$filters['q']}%");
        }

        if(isset($filters['ownerType'])) {
            switch ($filters['ownerType']) {
		        case 'student':
		            $queryBuilder->andWhere("md.studentOwner IS NOT NULL");
		            break;

                case 'teacher':
                    $queryBuilder->andWhere("md.teacherOwner IS NOT NULL");
                    break;

                case 'none':
	                $queryBuilder->andWhere("md.studentOwner IS NULL");
	                $queryBuilder->andWhere("md.teacherOwner IS NULL");
                    break;
            }
        }

        if(isset($filters['mdmServer']) && $filters['mdmServer'] !== null && $filters['mdmServer'] != "") {
	        if($this->getEntityManager()->getConnection()->getDriver() instanceof AbstractMySQLDriver) {
		        $queryBuilder->andWhere("md.mdmServer = :mdmServerId")->setParameter("mdmServerId", (new Uuid($filters['mdmServer']))->toBinary() );
	        } else {
		        $queryBuilder->andWhere("md.mdmServer = :mdmServerId")->setParameter("mdmServerId", $filters['mdmServer']);
	        }
        }

        if(isset($filters['supervised']) && $filters['supervised'] != "") {
            $queryBuilder->andWhere("md.supervised = :supervised")->setParameter("supervised", $filters['supervised'] == "1" ? 1 : 0);
        }

        if(isset($filters['fromDep']) && $filters['fromDep'] != "") {
            $queryBuilder->andWhere("md.fromDep = :fromDep")->setParameter("fromDep", $filters['fromDep'] == "1" ? 1 : 0);
        }

        if(isset($filters['moreDevices']) && $filters['moreDevices']) {
            $subQueryBuilder = $this->createQueryBuilder("mdc")
                ->select('mdc.ownerUsername')
                ->groupBy('mdc.ownerUsername')
                ->having("COUNT(mdc.id) > 1")
                ->where("NOT mdc.ownerUsername IS NULL")
                ->getDQL();
            $queryBuilder->andWhere($queryBuilder->expr()->in('md.ownerUsername', $subQueryBuilder));
        }
        
        if(isset($filters['orderByServer']) && $filters['orderByServer']) {
            $queryBuilder->addOrderBy('md.mdmServer', 'ASC');
        }

        if (isset($filters['order'])) {
            switch ($filters['order']) {
                case "username":
					if($studentOwner) {
						$queryBuilder->addOrderBy("student.username", strtoupper($filters['orderDir']) == "DESC" ? "DESC" : "ASC");
					}
	                if($teacherOwner) {
		                $queryBuilder->addOrderBy("teacher.username", strtoupper($filters['orderDir']) == "DESC" ? "DESC" : "ASC");
	                }
                    break;

                case "lastname":
	                if($studentOwner) {
		                $queryBuilder->addOrderBy("student.lastname", strtoupper($filters['orderDir']) == "DESC" ? "DESC" : "ASC");
	                }
	                if($teacherOwner) {
		                $queryBuilder->addOrderBy("teacher.lastname", strtoupper($filters['orderDir']) == "DESC" ? "DESC" : "ASC");
	                }
                    break;

                default:
	                $queryBuilder->addOrderBy( "md." . $filters['order'], strtoupper($filters['orderDir']) == "DESC" ? "DESC" : "ASC");
            }

        }

        return $queryBuilder->getQuery();
    }
}