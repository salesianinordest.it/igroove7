<?php

namespace App\Repository;

use Doctrine\DBAL\Driver\AbstractMySQLDriver;
use Doctrine\ORM\EntityRepository;
use Symfony\Component\Uid\Uuid;

class DeviceRepository extends EntityRepository
{
    public function getQueryFromFilter($filter)
    {
        $em = $this->getEntityManager();
        $query = $em->getRepository('App\Entity\Device')
            ->createQueryBuilder('m')
            ->select('m');
        if (array_key_exists('queryString', $filter)) {
            if (filter_var($filter['queryString'], FILTER_VALIDATE_IP, FILTER_FLAG_IPV4)) {
                $query->join('m.ips', 'i', 'm.id = i.mac');
                $query->andWhere('i.ip = :ip')->setParameter('ip', ip2long($filter['queryString']));
            } else {
                $q = '%'.$filter['queryString'].'%';
                $query->andWhere('m.device LIKE :q  or m.mac LIKE :q')->setParameter('q', strtoupper($q));
            }
        }
        if (array_key_exists('onlyActive', $filter) and $filter['onlyActive']) {
            $query->andWhere('m.active = :true  ')->setParameter('true', true);
        }
	    if ((array_key_exists('onlyToApprove', $filter)) and ($filter['onlyToApprove'])) {
		    $query->andWhere('m.toApprove = :true  ')->setParameter('true', true);
	    }
        if (array_key_exists('mikrotikListId', $filter)) {
            $query->join('m.mikrotikList', 'l')
                ->andWhere('l.id = :mikrotiklistid');

	        if($this->getEntityManager()->getConnection()->getDriver() instanceof AbstractMySQLDriver) {
		        $query->setParameter('mikrotiklistid', (new Uuid($filter['mikrotikListId']))->toBinary());
	        } else {
		        $query->setParameter('mikrotiklistid', $filter['mikrotikListId']);
	        }
        }
        $query->orderBy('m.device', 'ASC');

        return $query->getQuery();
    }

    public function getAllBypassedMac()
    {
        $devices = $this->findBy(['active' => true, 'bypassHotspot' => true]);
        $macAddresses = [];
        foreach ($devices as $device) {
            $macAddresses[] = $device->getMac();
        }

        return $macAddresses;
    }
}
