<?php

namespace App\Controller;

use App\Entity\Group;
use App\Entity\InternetOpen;
use App\Entity\LdapGroup;
use App\Entity\MikrotikList;
use App\Entity\Student;
use App\Manager\CoaManager;
use App\Manager\ConfigurationManager;
use App\Message\LdapMessage;
use App\Message\MikrotikMessage;
use Doctrine\ORM\EntityManagerInterface;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\IsGranted;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Messenger\Envelope;
use Symfony\Component\Messenger\MessageBusInterface;

class InternetController extends BaseAbstractController
{
    /**
     * Enable internet for speciefied group.
     *
     * @Route("/internet/{providerId}/{groupId}/on", name="internetOn")
     * @IsGranted("ROLE_TEACHER")
     */
    public function internetOnAction(ConfigurationManager $configurationManager, EntityManagerInterface $em, Request $request, MessageBusInterface $messageBus, CoaManager $coaManager, $providerId, $groupId)
    {
        $active_directory_generated_group_prefix = $configurationManager->getActiveDirectoryGeneratedGroupPrefix();
        $ldapGroupRepository = $em->getRepository('App\Entity\LdapGroup');

        $group = $em->getRepository('App\Entity\Group')->find($groupId);
        if (!$group instanceof Group) {
            throw $this->createNotFoundException('Non è stato possibile trovare il gruppo indicato.');
        }

        $ldapGroupName = $active_directory_generated_group_prefix.$ldapGroupRepository::ldapEscape($group->getName());
        $currentUserName = $this->getUser()->getUserIdentifier();
        $closeAt = new \DateTime($request->get('timeToSet', false));
        $permitPersonalDevice = (bool) $request->get('personalDevice', false);

        $currentEntity = $em->getRepository('App\Entity\InternetOpen')->findOneBy(['account' => $ldapGroupName, 'type' => 'group']);
        if ($currentEntity instanceof InternetOpen) {
            $currentEntity->setCloseAt($closeAt);
            $currentEntity->setActivedBy($currentUserName);
            $currentEntity->setPermitPersonalDevices($permitPersonalDevice);

            $internetLdapGroup = $ldapGroupRepository->find($active_directory_generated_group_prefix.'InternetAccess');
            $personalDeviceLdapGroup = $ldapGroupRepository->find($active_directory_generated_group_prefix.'PersonalDeviceAccess');
            $modifiedLdapGroup = false;

            if (!$internetLdapGroup->memberExists('group', $ldapGroupName)) {
                $internetLdapGroup->addMember('group', $ldapGroupName);
                $modifiedLdapGroup = true;
            }

            if ($permitPersonalDevice && !$personalDeviceLdapGroup->memberExists('group', $ldapGroupName)) {
                $personalDeviceLdapGroup->addMember('group', $ldapGroupName);
                $modifiedLdapGroup = true;
            } elseif (!$permitPersonalDevice && $personalDeviceLdapGroup->memberExists('group', $ldapGroupName)) {
                $personalDeviceLdapGroup->removeMember('group', $ldapGroupName);
                $modifiedLdapGroup = true;
            }

            $em->flush();

            if ($modifiedLdapGroup) {
                $msg = new LdapMessage();
                $msg->setAction('syncInternetAccessLdapGroup');
                $messageBus->dispatch(new Envelope($msg));
            }

	        $this->logAction("internetOn-group","ha esteso {$group->getName()}".($permitPersonalDevice?" con i dispositivi personali":""), ['groupName' => $group->getName(), 'groupId' => $group->getId(), 'personalDevice' => $permitPersonalDevice]);
        } else {
            $internetOpen = new InternetOpen();
            $internetOpen->setType('group');
            $internetOpen->setAccount($ldapGroupName);
            $internetOpen->setCloseAt($closeAt);
            $internetOpen->setActivedBy($currentUserName);
            $internetOpen->setActivationCompleated(false);
            $internetOpen->setPermitPersonalDevices($permitPersonalDevice);
            $em->persist($internetOpen);

            $internetLdapGroup = $em->getRepository('App\Entity\LdapGroup')->find($active_directory_generated_group_prefix.'InternetAccess');
            $personalDeviceLdapGroup = $ldapGroupRepository->find($active_directory_generated_group_prefix.'PersonalDeviceAccess');

			if($internetLdapGroup) {
				$internetLdapGroup->addMember('group', $ldapGroupName);
			}

            if ($permitPersonalDevice && $personalDeviceLdapGroup) {
                $personalDeviceLdapGroup->addMember('group', $ldapGroupName);
            }

            $em->flush();

            $msg = new LdapMessage();
            $msg->setAction('syncInternetAccessLdapGroup');
            $messageBus->dispatch(new Envelope($msg));

	        $this->logAction("internetOn-group","ha abilitato {$group->getName()}".($permitPersonalDevice?" con i dispositivi personali":""), ['groupName' => $group->getName(), 'groupId' => $group->getId(), 'personalDevice' => $permitPersonalDevice]);
        }

	    $groupUsers = $em->getRepository(LdapGroup::class)->getAllChildrenRecursiveUsers($ldapGroupName);
	    foreach ($groupUsers as $username) {
		    try {
			    $coaManager->notifyUserActivation($username, $permitPersonalDevice);
		    } catch (\Exception $e) {
			    $this->logger->warning("Error sending activation notification to the coa system: {$e->getMessage()}");
		    }
	    }

        return $this->redirect(
            $this->generateUrl(
                'list_groups_in_provider',
                [
                    'providerId' => $providerId,
                    'sectorId' => $request->get('sectorId', false),
                    'activatedGroup' => $groupId,
                ]
            )
        );
    }

    /**
     * Disable internet for specified group.
     *
     * @Route("/internet/{providerId}/{groupId}/off", name="internetOff")
     * @IsGranted("ROLE_TEACHER")
     */
    public function internetOffAction(ConfigurationManager $configurationManager, EntityManagerInterface $em, Request $request, CoaManager $coaManager, MessageBusInterface $messageBus, $providerId, $groupId)
    {
        $active_directory_generated_group_prefix = $configurationManager->getActiveDirectoryGeneratedGroupPrefix();
        $ldapGroupRepository = $em->getRepository('App\Entity\LdapGroup');

        $group = $em->getRepository('App\Entity\Group')->find($groupId);
        if (!$group instanceof Group) {
            throw $this->createNotFoundException('Non è stato possibile trovare il gruppo indicato.');
        }

        $internetLdapGroup = $ldapGroupRepository->find($active_directory_generated_group_prefix.'InternetAccess');
        $personalDeviceLdapGroup = $ldapGroupRepository->find($active_directory_generated_group_prefix.'PersonalDeviceAccess');

	    $internetLdapGroup->removeMember('group',  $active_directory_generated_group_prefix .$ldapGroupRepository::ldapEscape($group->getName()));
	    $personalDeviceLdapGroup->removeMember('group',  $active_directory_generated_group_prefix .$ldapGroupRepository::ldapEscape($group->getName()));

	    $em->getRepository('App\Entity\InternetOpen')->removeGroup($active_directory_generated_group_prefix . $ldapGroupRepository::ldapEscape($group->getName()));
        $em->flush();

        // @todo disattivare anche gli studenti singoli?

        $msg = new LdapMessage();
        $msg->setAction('syncInternetAccessLdapGroup');
        $messageBus->dispatch(new Envelope($msg));

        $this->removeCoaFromGroup($em, $coaManager, $active_directory_generated_group_prefix.$group->getName());
	    $this->logAction("internetOff-group","ha disabilitato {$group}", ['groupName' => $group->getName(), 'groupId' => $group->getId()]);

        return $this->redirect(
            $this->generateUrl(
                'list_groups_in_provider',
                [
                    'providerId' => $providerId,
                    'sectorId' => $request->get('sectorId', false),
                ]
            )
        );
    }

    /**
     * Enable internet for speciefied student.
     *
     * @Route("/internet/{providerId}/{groupId}/{studentId}/on", name="internetOnUser")
     * @IsGranted("ROLE_TEACHER")
     */
    public function internetOnUserAction(ConfigurationManager $configurationManager, EntityManagerInterface $em, Request $request, MessageBusInterface $messageBus, CoaManager $coaManager, $providerId, $groupId, $studentId)
    {
        $timeToSet = $request->get('timeToSet', false);
        $closeAt = new \DateTime($timeToSet);
        $currentUserName = $this->getUser()->getUserIdentifier();
        $ldapGroupRepository = $em->getRepository('App\Entity\LdapGroup');
        $permitPersonalDevice = '1' == $request->get('personalDevice', false);

        $student = $em->getRepository('App\Entity\Student')->find($studentId);
        if (!$student instanceof Student) {
            throw $this->createNotFoundException('Non è stato possibile trovare lo studente indicato.');
        }

        $active_directory_generated_group_prefix = $configurationManager->getActiveDirectoryGeneratedGroupPrefix();

        $currentEntity = $em->getRepository('App\Entity\InternetOpen')->findOneBy(['account' => $student->getUsername(), 'type' => 'user']);
        if ($currentEntity instanceof InternetOpen) {
            $currentEntity->setCloseAt($closeAt);
            $currentEntity->setActivedBy($currentUserName);
            $currentEntity->setPermitPersonalDevices($permitPersonalDevice);

            $modifiedLdapGroup = false;
            $internetLdapGroup = $ldapGroupRepository->find($active_directory_generated_group_prefix.'InternetAccess');
            $personalDeviceLdapGroup = $ldapGroupRepository->find($active_directory_generated_group_prefix.'PersonalDeviceAccess');

            if (!$internetLdapGroup->memberExists('user', $student->getUsername())) {
                $internetLdapGroup->addMember('user', $student->getUsername());
                $modifiedLdapGroup = true;
            }

            if ($permitPersonalDevice && !$personalDeviceLdapGroup->memberExists('user', $student->getUsername())) {
                $personalDeviceLdapGroup->addMember('user', $student->getUsername());
                $modifiedLdapGroup = true;
            } elseif (!$permitPersonalDevice && $personalDeviceLdapGroup->memberExists('user', $student->getUsername())) {
                $personalDeviceLdapGroup->removeMember('user', $student->getUsername());
                $modifiedLdapGroup = true;
            }

            $em->flush();

            if ($modifiedLdapGroup) {
                $msg = new LdapMessage();
                $msg->setAction('syncInternetAccessLdapGroup');
                $messageBus->dispatch(new Envelope($msg));
            }

	        $this->logAction("internetOn-user","ha esteso {$student->getUsername()}", ['user' => $student->getUsername(), 'personalDevice' => $permitPersonalDevice]);
        } else {
            $internetOpen = new InternetOpen();
            $internetOpen->setType('user');
            $internetOpen->setAccount($student->getUsername());
            $internetOpen->setCloseAt($closeAt);
            $internetOpen->setActivedBy($currentUserName);
            $internetOpen->setActivationCompleated(false);
            $internetOpen->setPermitPersonalDevices($permitPersonalDevice);
            $em->persist($internetOpen);

            $internetLdapGroup = $ldapGroupRepository->find($active_directory_generated_group_prefix.'InternetAccess');
            $personalDeviceLdapGroup = $ldapGroupRepository->find($active_directory_generated_group_prefix.'PersonalDeviceAccess');

            $internetLdapGroup->addMember('user', $student->getUsername());
            if ($permitPersonalDevice) {
                $personalDeviceLdapGroup->addMember('user', $student->getUsername());
            }

            $em->flush();

            $msg = new LdapMessage();
            $msg->setAction('syncInternetAccessLdapGroup');
            $messageBus->dispatch(new Envelope($msg));

	        $this->logAction("internetOn-user","ha abilitato {$student->getUsername()}", ['user' => $student->getUsername(), 'personalDevice' => $permitPersonalDevice]);
        }

	    try {
		    $coaManager->notifyUserActivation($student->getUsername(), $permitPersonalDevice);
	    } catch (\Exception $e) {
		    $this->logger->warning("Error sending activation notification to the coa system: {$e->getMessage()}");
	    }

	    return $this->redirect($providerId == '0' || $groupId == '0' ?
		    $this->generateUrl( 'student_list', ['activatedUser' => $studentId]) :
            $this->generateUrl(
                'list_students_in_group',
                [
                    'providerId' => $providerId,
                    'groupId' => $groupId,
                    'activatedUser' => $studentId,
                ]
            )
        );
    }

    /**
     * Disable internet for specified student.
     *
     * @Route("/internet/{providerId}/{groupId}/{studentId}/off", name="internetOffUser")
     * @IsGranted("ROLE_TEACHER")
     */
    public function internetOffUserAction(ConfigurationManager $configurationManager, EntityManagerInterface $em, CoaManager $coaManager, MessageBusInterface $messageBus, $providerId, $groupId, $studentId)
    {
        $student = $em->getRepository('App\Entity\Student')->find($studentId);
        if (!$student instanceof Student) {
            throw $this->createNotFoundException('Non è stato possibile trovare lo studente indicato.');
        }

        $em->getRepository('App\Entity\InternetOpen')->removeUser($student->getUsername());

        $active_directory_generated_group_prefix = $configurationManager->getActiveDirectoryGeneratedGroupPrefix();

        $ldapGroupRepository = $em->getRepository('App\Entity\LdapGroup');
        $internetLdapGroup = $ldapGroupRepository->find($active_directory_generated_group_prefix.'InternetAccess');
        $personalDeviceLdapGroup = $ldapGroupRepository->find($active_directory_generated_group_prefix.'PersonalDeviceAccess');

        $internetLdapGroup->removeMember('user', $student->getUsername());
        $personalDeviceLdapGroup->removeMember('user', $student->getUsername());
        $em->flush();

        $msg = new LdapMessage();
        $msg->setAction('syncInternetAccessLdapGroup');
        $messageBus->dispatch(new Envelope($msg));
	    try {
		    $coaManager->KickOffUser( $student->getUsername());
	    } catch (\Exception $e) {
		    $this->logger->warning("Error sending kick off to the coa system: {$e->getMessage()}");
	    }

	    $this->logAction("internetOff-user","ha disabilitato {$student->getUsername()}", ['user' => $student->getUsername()]);

        return $this->redirect(
	        $providerId == '0' || $groupId == '0' ?
            $this->generateUrl( 'student_list') :
	        $this->generateUrl(
		        'list_students_in_group',
		        [
			        'providerId' => $providerId,
			        'groupId' => $groupId,
		        ]
	        )
        );
    }

    /**
     * Enable internet for specified mikrotik ip list.
     *
     * @Route("/mikrotik/list/internet/{mikrotikList}/on", name="internetOnIpList")
     * @IsGranted("ROLE_NETWORK_OPERATOR")
     */
    public function internetOnIpList(EntityManagerInterface $em, Request $request, MikrotikList $mikrotikList, MessageBusInterface $messageBus)
    {
        if (!$mikrotikList instanceof MikrotikList) {
            throw $this->createNotFoundException('Non è stato possibile trovare la lista indicata.');
        }

        $timeToSet = $request->get('timeToSet', false);
        $closeAt = new \DateTime($timeToSet);
        $currentUserName = $this->getUser()->getUserIdentifier();

        $currentEntity = $em->getRepository('App\Entity\InternetOpen')->findOneBy(['account' => $mikrotikList->getId(), 'type' => 'ipList']);
        if ($currentEntity instanceof InternetOpen) {
            $currentEntity->setCloseAt($closeAt);
            $currentEntity->setActivedBy($currentUserName);
            $em->flush();

	        $this->logAction("internetOn-ipList","ha esteso {$mikrotikList->getNome()}", ['id' => $mikrotikList->getId()]);
        } else {
            $internetOpen = new InternetOpen();
            $internetOpen->setType('ipList');
            $internetOpen->setAccount($mikrotikList->getId());
            $internetOpen->setCloseAt($closeAt);
            $internetOpen->setActivedBy($currentUserName);
            $internetOpen->setActivationCompleated(false);
            $em->persist($internetOpen);
            $em->flush();

	        $this->logAction("internetOn-ipList","ha abilitato {$mikrotikList->getNome()}", ['id' => $mikrotikList->getId()]);
        }

        $msg = new MikrotikMessage();
        $msg->setCommand('addIpToBypassedList');
        $messageBus->dispatch(new Envelope($msg));

        return $this->redirect($this->generateUrl('mikrotik_list', []));
    }

    /**
     * Disable internet for specified mikrotik ip list.
     *
     * @Route("/mikrotik/list/internet/{mikrotikListId}/off", name="internetOffIpList")
     * @IsGranted("ROLE_NETWORK_OPERATOR")
     */
    public function internetOffIpList(EntityManagerInterface $em, MessageBusInterface $messageBus, $mikrotikListId)
    {
        $mikrotikList = $em->getRepository("App\Entity\MikrotikList")->find($mikrotikListId);
        if (!$mikrotikList instanceof MikrotikList) {
            throw $this->createNotFoundException('Non è stato possibile trovare la lista indicata.');
        }

        $internetOpen = $em->getRepository('App\Entity\InternetOpen')->findOneBy(['account' => $mikrotikListId, 'type' => 'ipList']);
        if (!$internetOpen instanceof InternetOpen) {
            return $this->redirect($this->generateUrl('mikrotik_list', []));
        }

        $em->remove($internetOpen);
        $em->flush();

	    $this->logAction("internetOff-ipList","ha disabilitato {$mikrotikList->getNome()}", ['id' => $mikrotikList->getId()]);

        $msg = new MikrotikMessage();
        $msg->setCommand('addIpToBypassedList');
        $messageBus->dispatch(new Envelope($msg));

        return $this->redirect($this->generateUrl('mikrotik_list', ['activatedList' => $mikrotikList->getId()]));
    }

    /**
     * @Route("/internet/{providerId}/{groupId}/isOn", name="internetIsOn")
     */
    public function internetIsOnAction(ConfigurationManager $configurationManager, EntityManagerInterface $em, $providerId, $groupId)
    {
        $active_directory_generated_group_prefix = $configurationManager->getActiveDirectoryGeneratedGroupPrefix();
        $response = new Response();
        $response->headers->set('Content-Type', 'application/json');

        $group = $em->getRepository('App\Entity\Group')->find($groupId);
        if (!$group instanceof Group) {
            $response->setContent(json_encode(['on' => null, 'activationCompleted' => null, 'error' => 'Non è stato possibile trovare il gruppo indicato.']));

            return $response;
        }

        $ldapGroupName = $active_directory_generated_group_prefix.$em->getRepository('App\Entity\LdapGroup')::ldapEscape($group->getName());
        $internetOpen = $em->getRepository('App\Entity\InternetOpen')->findOneBy(['account' => $ldapGroupName, 'type' => 'group']);
        if (!$internetOpen instanceof InternetOpen) {
            $response->setContent(json_encode(['on' => false, 'activationCompleted' => null]));
        } else {
            $response->setContent(json_encode(['on' => true, 'activationCompleted' => $internetOpen->getActivationCompleated()]));
        }

        return $response;
    }

    /**
     * @Route("/internet/{providerId}/{groupId}/{studentId}/isOn", name="internetIsOnForUser")
     * @IsGranted("ROLE_TEACHER")
     */
    public function internetIsOnForUserAction(EntityManagerInterface $em, $providerId, $groupId, $studentId)
    {
        $response = new Response();
        $response->headers->set('Content-Type', 'application/json');

        $student = $em->getRepository('App\Entity\Student')->find($studentId);
        if (!$student instanceof Student) {
            $response->setContent(json_encode(['on' => null, 'activationCompleted' => null, 'error' => 'Non è stato possibile trovare lo studente indicato.']));

            return $response;
        }

        $internetOpen = $em->getRepository('App\Entity\InternetOpen')->findOneBy(['account' => $student->getUsername(), 'type' => 'user']);
        if (!$internetOpen instanceof InternetOpen) {
            $response->setContent(json_encode(['on' => false, 'activationCompleted' => null]));
        } else {
            $response->setContent(json_encode(['on' => true, 'activationCompleted' => $internetOpen->getActivationCompleated()]));
        }

        return $response;
    }

	/**
	 * @Route(path="/mikrotik/list/internet/{mikrotikListId}/isOn", name="internetIpListIsOn", methods={"GET"})
	 */
	public function internetIsOnForIpListAction(EntityManagerInterface $em, $mikrotikListId)
	{
		$response = new Response();
		$response->headers->set('Content-Type', 'application/json');

		$mikrotikList = $em->getRepository(MikrotikList::class)->find($mikrotikListId);
		if(!$mikrotikList instanceof MikrotikList) {
			$response->setContent(json_encode(["on" => null, 'activationCompleted' => null, 'error' => 'Non è stato possibile trovare la lista indicata.']));
			return $response;
		}

		$internetOpen = $em->getRepository(InternetOpen::class)->findOneBy(['account' => $mikrotikListId, 'type' => "ipList"]);
		if(!$internetOpen instanceof InternetOpen) {
			$response->setContent(json_encode(["on" => false, 'activationCompleted' => null]));
		} else {
			$response->setContent(json_encode(["on" => true, 'activationCompleted' => $internetOpen->getActivationCompleated()]));
		}

		return $response;
	}

    private function removeCoaFromGroup($em, $coaManager, $groupName)
    {
        $usernames = $em->getRepository('App\Entity\LdapGroup')
            ->getAllChildrenRecursiveUsers($groupName);
        foreach ($usernames as $username) {
	        try {
		        $coaManager->KickOffUser($username);
	        } catch (\Exception $e) {
		        $this->logger->warning("Error sending kick off to the coa system: {$e->getMessage()}");
	        }
        }
    }
}
