<?php

namespace App\Controller;

use App\Entity\Student;
use App\Entity\Teacher;
use App\Manager\ConfigurationManager;
use App\Manager\MicrosoftLdapService;
use App\Manager\PasswordGenerator;
use App\Manager\PersonsAndGroups;
use App\Message\GoogleMessage;
use App\Message\LdapMessage;
use Doctrine\ORM\EntityManagerInterface;
use IMAG\LdapBundle\User\LdapUser;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\IsGranted;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\Exception\AccessDeniedHttpException;
use Symfony\Component\Messenger\Envelope;
use Symfony\Component\Messenger\MessageBusInterface;
use Symfony\Component\Security\Core\Authentication\Token\TokenInterface;
use Symfony\Component\Security\Core\Exception\AccessDeniedException;

class PasswordController extends AbstractController
{
    /**
     * @Route("/passwordChange/forceChangeMyPassword", name="forceChangeMyPassword")
     */
    public function forceChangeMyPasswordAction()
    {
        try {
            return $this->render(
                'password/force_change_my_password.html.twig',
                [$this->changeMyPasswordAction()]
            );
        } catch (AccessDeniedException $e) {
            throw new AccessDeniedHttpException($e->getMessage(), $e);
        }
    }

    /**
     * @Route("/passwordChange/checkPassword", name="forceCheckPassword")
     */
    public function forceCheckPasswordAction()
    {
        try {
            return $this->render(
                'password/force_check_password.html.twig',
                [$this->checkPasswordAction()]);
        } catch (AccessDeniedException $e) {
            throw new AccessDeniedHttpException($e->getMessage(), $e);
        }
    }

    /**
     * @Route("/passwordChange/setNewPassword", name="forceSetNewPassword")
     */
    public function forceSetNewPasswordAction()
    {
        try {
            return $this->render('password/force_set_net_password.html.twig',
                [$this->setNewPasswordAction()]);
        } catch (AccessDeniedException $e) {
            throw new AccessDeniedHttpException($e->getMessage(), $e);
        }
    }

    /**
     * @Route("/changeMyPassword", name="changeMyPassword")
     */
    public function changeMyPasswordAction(ConfigurationManager $configurationManager, EntityManagerInterface $em)
    {
        $configurationAD = $configurationManager->getActiveDirectoryConfiguration();

        if ($this->get('session')->get('forcePasswordChangeUser', null) instanceof LdapUser) { //@todo fix
            $username = $this->get('session')->get('forcePasswordChangeUser')->getUsername();
            $force = true;
        } elseif ($this->getUser() !== null && '' !== $this->getUser()->getUserIdentifier()) {
            $username = $this->getUser()->getUserIdentifier();
            $force = false;
        } else {
            throw $this->createAccessDeniedException();
        }

        $person = null;

        if (($student = $em->getRepository('App\Entity\Student')->findOneBy(['username' => $username])) instanceof Student) {
            $person = $student;
        } elseif (($teacher = $em->getRepository('App\Entity\Teacher')->findOneBy(['username' => $username])) instanceof Teacher) {
            $person = $teacher;
        }

        $google = false;
        if (null !== $person && '' != $person->getEmail() && '' !== $person->getProviderSettings()['googleAppClientId']) {
            $google = true;
        }

        return $this->render('password/change_my_password.html.twig',
            [
            'password' => $configurationAD['password'],
            'google' => $google,
            'force' => $force,
        ]);
    }

    /**
     * @Route("/checkPassword", name="checkPassword")
     */
    public function checkPasswordAction(Request $request, ConfigurationManager $configurationManager, MicrosoftLdapService $microsoftLdapService)
    {
        $password = $request->get('password', false);

        $response = new Response();
        $response->headers->set('Content-Type', 'application/json');

        if ($this->get('session')->get('forcePasswordChangeUser', null) instanceof LdapUser) { //@todo fix
            $username = $this->get('session')->get('forcePasswordChangeUser')->getUsername();
        } elseif ($this->getUser() !== null && '' !== $this->getUser()->getUserIdentifier()) {
	        $username = $this->getUser()->getUserIdentifier();
        } else {
            return new JsonResponse(['success' => false, 'error' => 'Access Denied']);
        }

        $microsoftLdapService->setParameters($configurationManager->getActiveDirectoryConfiguration());
	    $microsoftLdapService->connect();

        $result = $microsoftLdapService->checkUserPassword($username, $password, true);

        return new JsonResponse(['success' => $result]);
    }

    /**
     * @Route("/setNewPassword", name="setNewPassword")
     */
    public function setNewPasswordAction(Request $request, EntityManagerInterface $em, PersonsAndGroups $personsAndGroups)
    {
        $response = new Response();
        $response->headers->set('Content-Type', 'application/json');

        if ($this->get('session')->get('forcePasswordChangeUser', null) instanceof LdapUser) { //@todo fix
            $username = $this->get('session')->get('forcePasswordChangeUser')->getUsername();
        } elseif ($this->getUser() !== null && '' !== $this->getUser()->getUserIdentifier()) {
	        $username = $this->getUser()->getUserIdentifier();
        } else {
            $response->setContent(json_encode(['success' => false, 'error' => 'Access Denied']));

            return $response;
        }

        $password = $request->get('password', false);
        $person = null;

        if (($student = $em->getRepository('App\Entity\Student')->findOneBy(['username' => $username])) instanceof Student) {
            $person = $student;
        } elseif (($teacher = $em->getRepository('App\Entity\Teacher')->findOneBy(['username' => $username])) instanceof Teacher) {
            $person = $teacher;
        }

        if (null === $person) {
            return new JsonResponse(['success' => false, 'error' => 'Invalid user']);
        }

        $newPerson = clone $person;
        $newPerson->setStartingPassword($password);

        $targetService = $request->get('targetService', 'All');

        if ('Google' == $targetService || 'All' == $targetService) {
            try {
                $personsAndGroups->resetPersonGoogleAppsUserPassword($newPerson);
            } catch (\Exception $e) {
                return new JsonResponse(['success' => false, 'error' => $e->getMessage()]);
            }
        }

        if ('Ad' == $targetService || 'All' == $targetService) {
            try {
                $personsAndGroups->resetPersonLdapUserPassword($newPerson);
                $this->get('session')->set('forcePasswordChangeUser', null); //@todo fix
                $this->get('security.token_storage')->setToken(null); //@todo fix
	            //$this->get('request')->getSession()->invalidate(); //@todo fix
            } catch (\Exception $e) {
                return new JsonResponse(['success' => false, 'error' => $e->getMessage()]);
            }
        }

        return new JsonResponse(['success' => true, 'error' => '']);
    }

    /**
     * @Route("/reset-student-password", name="resetStudentPassword")
     * @IsGranted("ROLE_TEACHER")
     */
    public function resetStudentPasswordAction()
    {
        return [];
    }

    /**
     * @Route("/check-student-exists", name="checkStudentExists")
     * @IsGranted("ROLE_TEACHER")
     */
    public function checkStudentExistsAction(Request $request, EntityManagerInterface $em)
    {
        $studentUsername = $request->get('student', false);
        $hasGoogleApp = false;

        $ldapUser = $em->getRepository('App\Entity\LdapUser')->findOneByUsername($studentUsername);
        $student = $em->getRepository('App\Entity\Student')->findOneByUsername($studentUsername);

        $found = ($student and $ldapUser);

        if ($student) {
            $hasGoogleApp = (strlen($student->getProvider()->getGoogleAppDomain() > 0)) ? true : false;
        }

        return new JsonResponse(['stato' => $found, 'hasGoogleApp' => $hasGoogleApp]);
    }

    /**
     * @Route("/do-reset-student-password", name="doResetStudentPassword")
     * @IsGranted("ROLE_TEACHER")
     */
    public function doResetStudentPasswordAction(PersonsAndGroups $personsAndGroups, Request $request, EntityManagerInterface $em, MessageBusInterface $messageBus)
    {
        $emailReset = $request->get('emailReset', false);
        $studentUsername = $request->get('studentUsername', false);
        $hasGoogleApp = $request->get('hasGoogleApp', false);
        $ldapUser = $em->getRepository('App\Entity\LdapUser')->findOneByUsername($studentUsername);
        $student = $em->getRepository('App\Entity\Student')->findOneByUsername($studentUsername);
        $found = ($student and $ldapUser);
        if ($found) {
	        $newPassword = $personsAndGroups->generateStudentPasswordForProvider($student->getProvider());
            $student->setStartingPassword($newPassword);
            $em->flush();

	        $msg = new LdapMessage();
	        $msg->setAction('syncLDAPfromDB');
	        $messageBus->dispatch(new Envelope($msg));

            if ($hasGoogleApp and $emailReset) {
                $msg = new GoogleMessage();
				$msg->setCommand('resetPassword');
				$msg->setParameters(['username' => $ldapUser->getEmail(), 'password' => $newPassword]);
	            $messageBus->dispatch(new Envelope($msg));
            }

            return $this->render(
                'password/do_reset_student_password.html.twig',
                [
                'studentUsername' => $studentUsername,
                'password' => $newPassword,
                'emailReset' => $emailReset,
                'email' => $ldapUser->getEmail(),
            ]);
        }

        return $this->redirect(
            $this->generateUrl(
                'resetStudentPassword'
            )
        );
    }
}
