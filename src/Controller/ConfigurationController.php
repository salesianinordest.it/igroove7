<?php

namespace App\Controller;

use App\Entity\AppleSchool;
use App\Entity\MdmServer;
use App\Entity\Provider;
use App\Manager\ConfigurationManager;
use Doctrine\ORM\EntityManagerInterface;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\IsGranted;
use Symfony\Component\Routing\Annotation\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;

/**
 * Configuration controller.
 */
class ConfigurationController extends AbstractController
{
    /**
     * List Configuration.
     *
     * @Route("/config/edit", name="config_edit")
     * @IsGranted("ROLE_ADMIN")
     */
    public function editAction(Request $request, ConfigurationManager $configurationManager, EntityManagerInterface $entityManager)
    {
        $appleSchools = $entityManager->getRepository(AppleSchool::class)->findAll();
        $providers = $entityManager->getRepository(Provider::class)->findAll();
	    $mdmServers = $entityManager->getRepository(MdmServer::class)->findAll();

        $form = $this->createForm('App\Form\ConfigurationType', $configurationManager, [
            'action' => $this->generateUrl('config_update'),
            'method' => 'POST',
        ]);

        return $this->render(
            'configuration/edit.html.twig',
            [
                'form' => $form->createView(),
                'saved' => $request->get('saved', false),
                'appleSchools' => $appleSchools,
                'providers' => $providers,
                'mdmServers' => $mdmServers
            ]
        );
    }

    /**
     * Edit a Configuration.
     *
     * @Route("/config/update", name="config_update", methods={"POST"})
     * @Template("configuration/edit.html.twig")
     * @IsGranted("ROLE_ADMIN")
     */
    public function updateAction(Request $request, ConfigurationManager $configurationManager, EntityManagerInterface $entityManager)
    {
        $form = $this->createForm('App\Form\ConfigurationType', $configurationManager);

        $form->handleRequest($request);

        if ($form->isValid()) {
            $configurationManager->save();

            return $this->redirect($this->generateUrl('config_edit', ['saved' => true]));
        }

	    $appleSchools = $entityManager->getRepository(AppleSchool::class)->findAll();
	    $providers = $entityManager->getRepository(Provider::class)->findAll();
	    $mdmServers = $entityManager->getRepository(MdmServer::class)->findAll();

	    return array(
		    'form' => $form->createView(),
		    'saved' => false,
		    'appleSchools' => $appleSchools,
		    'providers' => $providers,
		    'mdmServers' => $mdmServers
	    );
    }
}
