<?php

namespace App\Controller;

use App\Entity\Fortigate;
use App\Manager\FortigateManager;
use App\Message\FortigateMessage;
use Doctrine\ORM\EntityManagerInterface;
use Knp\Component\Pager\PaginatorInterface;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\IsGranted;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Messenger\Envelope;
use Symfony\Component\Messenger\MessageBusInterface;

/**
 * Fortigate controller.
 *
 * @Route("/config/fortigate")
 */
class FortigateController extends AbstractController
{


    /**
     * Lists all FortigateList entities.
     *
     * @Route("/", name="fortigate")
     * @IsGranted("ROLE_ADMIN")
     */
    public function indexAction(PaginatorInterface $paginator, Request $request, EntityManagerInterface $em)
    {
        $queryString = $request->get('queryString', false);
        $q = '%'.$queryString.'%';
        if ($queryString) {
            $query = $em->createQuery(
                "SELECT l FROM App\Entity\Fortigate l WHERE l.host  LIKE :q ORDER BY l.host"
            )
                ->setParameter('q', $q);
        } else {
            $query = $em->createQuery("SELECT l FROM App\Entity\Fortigate l  ORDER BY l.host");
        }

        $pagination = $paginator->paginate(
            $query,
            $request->query->get('page', 1), 25
        );

        return $this->render(
            'fortigate/index.html.twig',
            [
                'pagination' => $pagination,
            ]
        );
    }



    /**
     * Creates a new Fortigate entity.
     *
     * @Route("/new", name="config_fortigate_new", methods={"POST","GET"})
     * @IsGranted("ROLE_ADMIN")
     */
    public function createAction(Request $request, EntityManagerInterface $em)
    {
        $entity = new Fortigate();
        $form = $this->createForm('App\Form\FortigateType', $entity);
        $form->handleRequest($request);




        if ($form->isSubmitted() && $form->isValid()) {
            $em->persist($entity);
            $em->flush();

            return $this->redirect($this->generateUrl('fortigate', []));
        }

        return $this->render(
            'fortigate/edit.html.twig',
            [
                'entity' => $entity,
                'main_form' => $form->createView(),
                'isEditing' => false
            ]
        );
    }

    /**
     * Displays a form to edit an existing Fortigate entity.
     *
     * @Route("/{id}/edit", name="config_fortigate_edit")
     * @IsGranted("ROLE_ADMIN")
     */
    public function editAction(request $request, Fortigate $fortigate, EntityManagerInterface $em)
    {
        if (!$fortigate) {
            throw $this->createNotFoundException('Unable to find Fortigate entity.');
        }

        $editForm = $this->createForm('App\Form\FortigateType', $fortigate);

        $editForm->handleRequest($request);

        if ($editForm->isSubmitted() && $editForm->isValid()) {
	        $em->flush();
            return $this->redirect($this->generateUrl('fortigate', []));
        }

        return $this->render(
            'fortigate/edit.html.twig',
            [
                'entity' => $fortigate,
                'main_form' => $editForm->createView(),
                'isEditing' => true
            ]
        );
    }

    /**
     * Deletes a Fortigate entity.
     *
     * @Route("/{id}/delete", name="config_fortigate_delete")
     * @IsGranted("ROLE_ADMIN")
     */
    public function deleteAction(Request $request, Fortigate $fortigate, EntityManagerInterface $em)
    {
        if (!$fortigate) {
            throw $this->createNotFoundException('Unable to find Fortigate entity.');
        }

        $em->remove($fortigate);
        $em->flush();

        return $this->redirect($this->generateUrl('fortigate'));
    }




}
