<?php

namespace App\Controller;

use App\Entity\MikrotikPool;
use App\Message\MikrotikMessage;
use Doctrine\ORM\EntityManagerInterface;
use Knp\Component\Pager\PaginatorInterface;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\IsGranted;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Messenger\Envelope;
use Symfony\Component\Messenger\MessageBusInterface;

/**
 * MikrotikPool controller.
 *
 * @Route("/mikrotik/pool")
 */
class MikrotikPoolController extends AbstractController
{
    /**
     * Lists all MikrotikPool entities.
     *
     * @Route("/", name="mikrotik_pool")
     * @IsGranted("ROLE_ADMIN")
     */
    public function indexAction(MessageBusInterface $messageBus, PaginatorInterface $paginator, Request $request, EntityManagerInterface $em)
    {
        $msg = new MikrotikMessage();
        $msg->setCommand('addPools');
        $messageBus->dispatch(new Envelope($msg));

        $queryString = $request->get('queryString', false);
        $q = '%'.$queryString.'%';
        if ($queryString) {
            $query = $em->createQuery(
                'SELECT l FROM App\Entity\MikrotikPool l WHERE l.nome  LIKE :q ORDER BY l.nome'
            )
                ->setParameter('q', $q);
        } else {
            $query = $em->createQuery('SELECT l FROM App\Entity\MikrotikPool l  ORDER BY l.nome');
        }
        $pagination = $paginator->paginate(
            $query,
            $request->query->get('page', 1), 25
        );

        return $this->render(
            'mikrotik_pool/index.html.twig',
            [
                'pagination' => $pagination,
                'queryString' => $queryString
            ]
        );
    }

    /**
     * Creates a new MikrotikPool entity.
     *
     * @Route("/create", name="mikrotik_pool_create", methods={"POST"})
     * @IsGranted("ROLE_ADMIN")
     */
    public function createAction(Request $request, EntityManagerInterface $em)
    {
        $entity = new MikrotikPool();

        $form = $this->createForm('App\Form\MikrotikPoolType', $entity);
        $form->handleRequest($request);

        if ($form->isValid()) {
            $em->persist($entity);

            $notAssociatedDeviceIps = $em->getRepository('App\Entity\DeviceIp')->getAllNotAssociatedIpsInRange($entity->getIpStart(), $entity->getIpEnd())->toArray();
            $outOfRangeDeviceIps = $em->getRepository('App\Entity\DeviceIp')->getAllAssociatedIpsOutOfRange($entity, $entity->getIpStart(), $entity->getIpEnd())->toArray();

            foreach ($notAssociatedDeviceIps as $notAssociatedDeviceIp) {
                $notAssociatedDeviceIp->setMikrotikPool($entity);
                $em->persist($notAssociatedDeviceIp);
            }

            foreach ($outOfRangeDeviceIps as $outOfRangeDeviceIp) {
                $outOfRangeDeviceIp->setMikrotikPool(null);
                $em->persist($outOfRangeDeviceIp);
            }

            $em->flush();

            return $this->redirect($this->generateUrl('mikrotik_pool'));
        }

        return $this->render(
	        'mikrotik_pool/edit.html.twig',[
            'entity' => $entity,
            'main_form' => $form->createView(),
            'isEditing' => false
        ]);
    }

    /**
     * Displays a form to create a new MikrotikPool entity.
     *
     * @Route("/new", name="mikrotik_pool_new")
     * @IsGranted("ROLE_ADMIN")
     */
    public function newAction()
    {
        $entity = new MikrotikPool();
        $form = $this->createForm('App\Form\MikrotikPoolType', $entity);

        return $this->render(
            'mikrotik_pool/edit.html.twig',
            [
                'entity' => $entity,
                'main_form' => $form->createView(),
                'isEditing' => false
            ]
        );
    }

    /**
     * Displays a form to edit an existing MikrotikPool entity.
     *
     * @Route("/{id}/edit", name="mikrotik_pool_edit")
     * @IsGranted("ROLE_ADMIN")
     */
    public function editAction(Request $request, MikrotikPool $mikrotikPool, EntityManagerInterface $em)
    {
        if (!$mikrotikPool) {
            throw $this->createNotFoundException('Unable to find Mikrotik entity.');
        }

        $editForm = $this->createForm('App\Form\MikrotikPoolType', $mikrotikPool);

        $editForm->handleRequest($request);

        if ($editForm->isSubmitted() && $editForm->isValid()) {
            $notAssociatedDeviceIps = $em->getRepository('App\Entity\DeviceIp')->getAllNotAssociatedIpsInRange($mikrotikPool->getIpStart(), $mikrotikPool->getIpEnd())->toArray();
            $outOfRangeDeviceIps = $em->getRepository('App\Entity\DeviceIp')->getAllAssociatedIpsOutOfRange($mikrotikPool, $mikrotikPool->getIpStart(), $mikrotikPool->getIpEnd())->toArray();

            foreach ($notAssociatedDeviceIps as $notAssociatedDeviceIp) {
                $notAssociatedDeviceIp->setMikrotikPool($mikrotikPool);
                $em->persist($notAssociatedDeviceIp);
            }

            foreach ($outOfRangeDeviceIps as $outOfRangeDeviceIp) {
                $outOfRangeDeviceIp->setMikrotikPool(null);
                $em->persist($outOfRangeDeviceIp);
            }

            $em->flush();

            return $this->redirect($this->generateUrl('mikrotik_pool', []));
        }

        return $this->render(
            'mikrotik_pool/edit.html.twig',
            [
                'entity' => $mikrotikPool,
                'main_form' => $editForm->createView(),
                'isEditing' => true
            ]
        );
    }

    /**
     * Deletes a MikrotikPool entity.
     *
     * @Route("/{id}", name="mikrotik_pool_delete")
     * @IsGranted("ROLE_ADMIN")
     */
    public function deleteAction(Request $request, MikrotikPool $mikrotikPool, EntityManagerInterface $em)
    {
        if (!$mikrotikPool) {
            throw $this->createNotFoundException('Unable to find Mikrotik entity.');
        }
        foreach ($mikrotikPool->getDeviceIps() as $deviceIp) {
            $deviceIp->setMikrotikPool(null);
            $em->persist($deviceIp);
        }

        $em->remove($mikrotikPool);
        $em->flush();

        return $this->redirect($this->generateUrl('mikrotik_pool'));
    }

    /**
     * Check first free ip for in a pool.
     *
     * @Route("/{id}/checkFreeIp", name="mikrotik_pool_check_free_ip")
     * @IsGranted("ROLE_NETWORK_OPERATOR")
     */
    public function checkFreeIpAction($id, EntityManagerInterface $em)
    {
        $entity = $em->getRepository('App\Entity\MikrotikPool')->find($id);

        if (!$entity) {
            return new Response(json_encode(['error' => 'Unable to find the pool you specified']), 404);
        }

        $selectedIp = null;
        $lastIp = $entity->getIpStart();
        foreach ($entity->getDeviceIps() as $ip) {
            if ($lastIp < $ip->getRawIp()) {
                $selectedIp = $lastIp;
                break;
            }

            $lastIp = $ip->getRawIp() + 1;
        }

        if (null === $selectedIp && $lastIp <= $entity->getIpEnd()) {
            $selectedIp = $lastIp;
        }

        if (null === $selectedIp) {
            return new Response(json_encode(['error' => "No more ip in the range {$entity->getDottedIpStart()} - {$entity->getDottedIpEnd()} of the pool {$entity->getNome()}"]));
        }

        return new Response(json_encode(['ip' => long2ip($selectedIp)]));
    }
}
