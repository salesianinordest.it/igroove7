<?php

namespace App\Controller;

use App\Entity\Group;
use App\Entity\PersonAbstract;
use App\Entity\Provider;
use App\Entity\ProviderToRemoveEntity;
use App\Entity\Sector;
use App\Entity\Student;
use App\Entity\Subject;
use App\Entity\Teacher;
use App\RepositoryAction\StudentRepositoryAction;
use App\RepositoryAction\TeacherRepositoryAction;
use Doctrine\DBAL\Driver\AbstractMySQLDriver;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\IsGranted;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Symfony\Component\Uid\Uuid;

/**
 * Trash controller.
 *
 * @Route("/trash")
 */
class TrashController extends AbstractController
{

    /**
     * @Route("/", name="trash_index")
     * @IsGranted("ROLE_ACCOUNTS_MANAGER")
     * @Template()
     */
    public function indexAction(EntityManagerInterface $em)
    {

        $data = ['teachers' => [], 'students' => []];
        $trashedTeachers = $em->getRepository(Teacher::class)
                                    ->createQueryBuilder('t')
                                    ->join('t.provider', 'p')
                                    ->andWhere('NOT t.trashedDate IS NULL')
                                    ->addOrderBy('t.trashedDate', 'ASC')
                                    ->addOrderBy('p.name', 'ASC')
                                    ->addOrderBy('t.lastname', 'ASC')
                                    ->getQuery()
                                    ->execute();

        foreach ($trashedTeachers as $trashedTeacher) {
            $data['teachers'][(string)$trashedTeacher->getId()] = [
                'data' => [
                    'id' => (string)$trashedTeacher->getId(),
                    '_name' => $trashedTeacher->getLastname().' '.$trashedTeacher->getFirstname().' ('.$trashedTeacher->getFiscalCode().')',
                    'providerName' => $trashedTeacher->getProvider()->getName(),
                    'username' => $trashedTeacher->getUsername(),
                    'email' => $trashedTeacher->getEmail(),
                    'ipadSerials' => join(", ", array_map(fn($entity) => $entity->getSerial(), $trashedTeacher->getMdmDevices()->toArray())),
                    'trashedDate' => $trashedTeacher->getTrashedDate()->format("d/m/Y")
                ],
                'state' => 0,
                'error' => null
            ];
        }

        $trashedStudents = $em->getRepository(Student::class)
                                    ->createQueryBuilder('s')
                                    ->join('s.provider', 'p')
                                    ->andWhere('NOT s.trashedDate IS NULL')
	                                ->join('s.memberOf', 'g')
	                                ->addOrderBy('p.name', "ASC")
                                    ->addOrderBy('g.name', 'ASC')
                                    ->addOrderBy('s.lastname', 'ASC')
                                    ->getQuery()
                                    ->execute();

        foreach ($trashedStudents as $trashedStudent) {
            $data['students'][(string)$trashedStudent->getId()] = [
                'data' => [
                    'id' => (string)$trashedStudent->getId(),
                    '_name' => $trashedStudent->getLastname().' '.$trashedStudent->getFirstname().' ('.$trashedStudent->getFiscalCode().')',
                    'internalGroupId' => 0,
                    'providerName' => $trashedStudent->getProvider()->getName(),
                    'username' => $trashedStudent->getUsername(),
                    'email' => $trashedStudent->getEmail(),
                    'groups' => join(", ", array_map(fn($entity) => $entity->getName(), $trashedStudent->getMemberOf()->toArray())),
                    'ipadSerials' => join(", ", array_map(fn($entity) => $entity->getSerial(), $trashedStudent->getMdmDevices()->toArray())),
                    'trashedDate' => $trashedStudent->getTrashedDate()->format("d/m/Y")
                ],
                'state' => 0,
                'error' => null
            ];
        }

        $canDelete = $this->isGranted('ROLE_ADMIN');

        return ['data' => $data, 'canDelete' => $canDelete];
    }

	/**
	 * @Route("/trash-user", name="trash_do_trash")
	 * @IsGranted("ROLE_ACCOUNTS_MANAGER")
	 */
	public function trashUserAction(Request $request, EntityManagerInterface $entityManager, StudentRepositoryAction $studentRepositoryAction, TeacherRepositoryAction $teacherRepositoryAction)
	{
		return $this->setUserTrashState(new \Datetime('now'), $request, $entityManager, $studentRepositoryAction, $teacherRepositoryAction);
	}

	/**
	 * @Route("/untrash-user", name="trash_do_untrash")
	 * @IsGranted("ROLE_ACCOUNTS_MANAGER")
	 */
	public function untrashUserAction(Request $request, EntityManagerInterface $entityManager, StudentRepositoryAction $studentRepositoryAction, TeacherRepositoryAction $teacherRepositoryAction)
	{
		return $this->setUserTrashState(null, $request, $entityManager, $studentRepositoryAction, $teacherRepositoryAction);
	}

    /**
     * @Route("/sync", name="trash_sync")
     * @IsGranted("ROLE_ACCOUNTS_MANAGER")
     * @Template()
     */
    public function showSyncAction(EntityManagerInterface $em)
    {
        $providers = $em->getRepository(Provider::class)->getProviderWithSyncableFilter();
        return ['providers' => $providers];
    }

    /**
     * @Route("/sync/to-be-removed", name="trash_sync_to_be_removed")
     * @IsGranted("ROLE_ACCOUNTS_MANAGER")
     * @Template()
     */
    public function showToBeRemovedEntitiesAction(EntityManagerInterface $em)
    {
        $toBeRemoved = $em->getRepository(ProviderToRemoveEntity::class)->findAll();
        $toBeRemoveIdsByType = [];
        foreach ($toBeRemoved as $row) {
            if(!isset($toBeRemoveIdsByType[$row->getEntityType()])) {
                $toBeRemoveIdsByType[$row->getEntityType()] = [];
            }

	        if($em->getConnection()->getDriver() instanceof AbstractMySQLDriver) {
		        $toBeRemoveIdsByType[ $row->getEntityType() ][] = (new Uuid($row->getEntityId()))->toBinary();
	        } else {
		        $toBeRemoveIdsByType[ $row->getEntityType() ][] = $row->getEntityId();
	        }
        }

        $toBeRemoveEntitiesByType = [];
        $types = ['students' => Student::class, 'teachers' => Teacher::class, 'groups' => Group::class, 'sectors' => Sector::class, 'subjects' => Subject::class];
        foreach ($types as $typeName => $typeClass) {
            $toBeRemoveEntitiesByType[$typeName] = [];

            $explodedClassname = explode('\\', $typeClass);
            $className = end($explodedClassname);
            if(empty($toBeRemoveIdsByType[$className])) {
                continue;
            }

            $entitiesQuery = $em->getRepository($typeClass)
                ->createQueryBuilder('s')
                ->join('s.provider', 'p')
	            ->where('s.id IN (:ids)')
	            ->setParameter('ids', $toBeRemoveIdsByType[$className])
	            ->addOrderBy('p.name', 'ASC');

			if($typeName === "teachers" || $typeName === "students") {
				$entitiesQuery->andWhere('s.trashedDate IS NULL');
			}

			if($typeName === "students") {
				$entitiesQuery->join('s.memberOf', 'g')
					->addOrderBy('g.name', "ASC");
			}

			$entitiesQuery->addOrderBy($typeName === "teachers" || $typeName === "students" ? "s.lastname" : "s.name", 'ASC');

            $entities = $entitiesQuery->getQuery()->execute();
            foreach ($entities as $entity) {
                $toBeRemoveEntitiesByType[$typeName][(string)$entity->getId()] = [
                    'data' => [
                        'id' => (string)$entity->getId(),
                        '_name' => $typeName == "students" || $typeName == "teachers" ? $entity->getLastname().' '.$entity->getFirstname().' ('.$entity->getFiscalCode().')' : $entity->getName(),
                        'internalGroupId' => 0,
                        'providerName' => $entity->getProvider()->getName(),
                        'providerId' => $entity->getProvider()->getId(),
                        'username' => $entity instanceof PersonAbstract ? $entity->getUsername() : '',
                        'email' => $entity instanceof PersonAbstract ? $entity->getEmail() : '',
                        'groups' => $entity instanceof Student ? join(", ", array_map(fn($entity) => $entity->getName(), $entity->getMemberOf()->toArray())) : '',
                        'ipadSerials' => $entity instanceof PersonAbstract ? join(", ", array_map(fn($entity) => $entity->getSerial(), $entity->getMdmDevices()->toArray())) : ''
                    ],
                    'state' => 0,
                    'error' => null
                ];
            }
        }

        $canDelete = $this->isGranted('ROLE_ADMIN');

        return ['data' => $toBeRemoveEntitiesByType, 'canDelete' => $canDelete];
    }

	protected function setUserTrashState(?\DateTime $trashState, Request $request, EntityManagerInterface $entityManager, StudentRepositoryAction $studentRepositoryAction, TeacherRepositoryAction $teacherRepositoryAction)
	{
		try {
			$person = $this->getPersonEntityForRequest($request, $entityManager);
		} catch (\Exception $e) {
			return new JsonResponse(['success' => false, 'error' => $e->getMessage()]);
		}

		$previousEntity = clone $person;
		$person->setTrashedDate($trashState);
		$entityManager->flush();

		try {
			if($person instanceof Student) {
				$studentRepositoryAction->executeAfterUpdate($person, $previousEntity);
			} elseif ($person instanceof Teacher) {
				$teacherRepositoryAction->executeAfterUpdate($person, $previousEntity);
			}
		} catch (\Exception $e) {
			return new JsonResponse(['success' => false, 'error' => 'Error during post operation: '.$e->getMessage()]);
		}

		return new JsonResponse(['success' => true, 'error' => null]);
	}

	protected function getPersonEntityForRequest(Request $request, EntityManagerInterface $entityManager) : PersonAbstract {
		$entityType = $request->get('entityType');
		switch ($entityType) {
			case "students":
				$entityClassType = Student::class;
				break;

			case "teachers":
				$entityClassType = Teacher::class;
				break;

			default:
				throw new \InvalidArgumentException('Invalid entity type');
		}

		$repository = $entityManager->getRepository($entityClassType);
		$entityId = $request->get('entityId');
		$entity = $repository->find($entityId);
		if (!$entity instanceof $entityClassType) {
			throw new \InvalidArgumentException('Invalid entity');
		}

		return $entity;
	}
}