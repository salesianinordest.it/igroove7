<?php

namespace App\Controller;

use App\Entity\InternetOpen;
use App\Entity\MikrotikList;
use App\Message\MikrotikMessage;
use Doctrine\ORM\EntityManagerInterface;
use Knp\Component\Pager\PaginatorInterface;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\IsGranted;
use Symfony\Component\Routing\Annotation\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Messenger\Envelope;
use Symfony\Component\Messenger\MessageBusInterface;

/**
 * MikrotikList controller.
 *
 * @Route("/mikrotik/list")
 */
class MikrotikListController extends AbstractController
{
    /**
     * Lists all MikrotikList entities.
     *
     * @Route("/", name="mikrotik_list")
     * @IsGranted("ROLE_NETWORK_OPERATOR")
     */
    public function indexAction(PaginatorInterface $paginator, Request $request, EntityManagerInterface $em)
    {
        $queryString = $request->get('queryString', false);
        $q = '%'.$queryString.'%';
        if ($queryString) {
            $query = $em->createQuery(
                'SELECT l FROM App\Entity\MikrotikList l WHERE l.nome  LIKE :q ORDER BY l.nome'
            )
                ->setParameter('q', $q);
        } else {
            $query = $em->createQuery('SELECT l FROM App\Entity\MikrotikList l  ORDER BY l.nome');
        }

        $currentUserIsAdmin = $this->isGranted('ROLE_ADMIN');
        $currentUserName = $this->getUser()->getUserIdentifier();

	    $activatingList = false;
        $mikrotikLists = $query->getResult();
        foreach ($mikrotikLists as $k => $mikrotikList) {
            $mikrotikLists[$k]->state = 'not-active';
            $mikrotikLists[$k]->endingTime = '';
            $mikrotikLists[$k]->activedByOtherUser = false;
            $mikrotikLists[$k]->activedBy = '';

            $internetOpen = $em->getRepository('App\Entity\InternetOpen')->findOneBy(['account' => $mikrotikList->getId(), 'type' => 'ipList']);
            if ($internetOpen instanceof InternetOpen) {
                if (!$internetOpen->getActivationCompleated()) {
                    $mikrotikLists[$k]->state = 'activating';
	                $activatingList = true;
                } else {
                    $mikrotikLists[$k]->state = 'active';
                    $mikrotikLists[$k]->endingTime = $internetOpen->getCloseAt()->format('H:i');

                    $mikrotikLists[$k]->activedBy = $currentUserIsAdmin && '' != $internetOpen->getActivedBy() ? $internetOpen->getActivedBy() : '';
                    if ('' != $currentUserName && $internetOpen->getActivedBy() != $currentUserName) {
                        $mikrotikLists[$k]->activedByOtherUser = true;
                    }
                }
            }
        }

        $pagination = $paginator->paginate(
            $mikrotikLists,
            $request->query->get('page', 1),
            25
        );

	    $activatedList = "";
	    if ($activatingList && $request->get('activatedList', "") != "") {
		    $activatedList = $request->get('activatedList', "");
	    }

        return $this->render(
            'mikrotik_list/index.html.twig',
            [
                'pagination' => $pagination,
                'timeToSet' => new \DateTime('+1 hour'),
                'activatedList' => $activatedList,
                'queryString' => $queryString
            ]
        );
    }

    /**
     * Creates a new MikrotikList entity.
     *
     * @Route("/create", name="mikrotik_list_create", methods={"POST"})
     * @IsGranted("ROLE_ADMIN")
     */
    public function createAction(Request $request, EntityManagerInterface $em, MessageBusInterface $messageBus)
    {
        $entity = new MikrotikList();
        $form = $this->createCreateForm($entity);
        $form->handleRequest($request);

        if ($form->isValid()) {
            $em->persist($entity);
            $em->flush();

	        $msg = new MikrotikMessage();
	        $msg->setCommand('addIpList');
	        $messageBus->dispatch(new Envelope($msg));

	        $msg = new MikrotikMessage();
	        $msg->setCommand('addMacToHospot');
	        $messageBus->dispatch(new Envelope($msg));

            return $this->redirect($this->generateUrl('mikrotik_list'));
        }

        return $this->render(
            'mikrotik_list/edit.html.twig',
            [
                'entity' => $entity,
                'main_form' => $form->createView(),
                'isEditing' => false
            ]
        );
    }

    /**
     * Displays a form to create a new MikrotikList entity.
     *
     * @Route("/new", name="mikrotik_list_new")
     * @IsGranted("ROLE_ADMIN")
     * @Template("mikrotik_list/edit.html.twig")
     */
    public function newAction()
    {
        $entity = new MikrotikList();
        $form = $this->createCreateForm($entity);

        return $this->render(
            'mikrotik_list/edit.html.twig',
            [
                'entity' => $entity,
                'main_form' => $form->createView(),
                'isEditing' => false
            ]
        );
    }

    protected function createCreateForm($entity)
    {
        return $this->createForm('App\Form\MikrotikListType', $entity, [
            'action' => $this->generateUrl('mikrotik_list_create'),
            'method' => 'POST',
        ]);
    }

    /**
     * Displays a form to edit an existing MikrotikList entity.
     *
     * @Route("/{id}/edit", name="mikrotik_list_edit")
     * @IsGranted("ROLE_ADMIN")
     * @Template("mikrotik_list/edit.html.twig")
     */
    public function editAction(Request $request, MikrotikList $mikrotikList, EntityManagerInterface $em)
    {
        if (!$mikrotikList) {
            throw $this->createNotFoundException('Unable to find Mikrotik  list entity.');
        }

        $editForm = $this->createEditForm($mikrotikList);
        $editForm->handleRequest($request);

        if ($editForm->isSubmitted() && $editForm->isValid()) {
            $em->flush();

            return $this->redirect($this->generateUrl('mikrotik_list', []));
        }

        return $this->render(
            'mikrotik_list/edit.html.twig',
            [
                'entity' => $mikrotikList,
                'main_form' => $editForm->createView(),
                'isEditing' => true
            ]
        );
    }

    /**
     * Edits an existing MikrotikList entity.
     *
     * @Route("/{id}/update", name="mikrotik_list_update", methods={"PUT"})
     * @IsGranted("ROLE_ADMIN")
     */
    public function updateAction(Request $request, MessageBusInterface $messageBus, EntityManagerInterface $em, $id)
    {
        $entity = $em->getRepository('App\Entity\MikrotikList')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find MikrotikList entity.');
        }

        $editForm = $this->createEditForm($entity);
        $editForm->handleRequest($request);

        if ($editForm->isValid()) {
            $em->flush();

	        $msg = new MikrotikMessage();
	        $msg->setCommand('addIpList');
	        $messageBus->dispatch(new Envelope($msg));

	        $msg = new MikrotikMessage();
	        $msg->setCommand('addMacToHospot');
	        $messageBus->dispatch(new Envelope($msg));

            return $this->redirect($this->generateUrl('mikrotik_list'));
        }

        return $this->render(
            'mikrotik_list/edit.html.twig',
            [
                'entity' => $entity,
                'main_form' => $editForm->createView(),
                'isEditing' => true
            ]
        );
    }

    protected function createEditForm($entity)
    {
        return $this->createForm('App\Form\MikrotikListType', $entity, [
            'action' => $this->generateUrl('mikrotik_list_update', ['id' => $entity->getId()]),
            'method' => 'POST',
        ]);
    }

    /**
     * Deletes a MikrotikList entity.
     *
     * @Route("/{id}/delete", name="mikrotik_list_delete")
     * @IsGranted("ROLE_ADMIN")
     */
    public function deleteAction(Request $request, $id, EntityManagerInterface $em)
    {
        $entity = $em->getRepository('App\Entity\MikrotikList')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find MikrotikList entity.');
        }

        $em->remove($entity);
        $em->flush();

        return $this->redirect($this->generateUrl('mikrotik_list'));
    }
}
