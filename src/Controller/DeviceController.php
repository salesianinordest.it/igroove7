<?php

namespace App\Controller;

use App\Entity\Device;
use App\Entity\MikrotikList;
use App\Message\MikrotikMessage;
use Doctrine\ORM\EntityManagerInterface;
use Knp\Component\Pager\PaginatorInterface;
use Psr\Log\LoggerInterface;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\IsGranted;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Messenger\Envelope;
use Symfony\Component\Messenger\MessageBusInterface;

/**
 * Device controller.
 */
class DeviceController extends BaseAbstractController
{

    public function __construct(LoggerInterface $logger, LoggerInterface $actionLogger)
    {
		BaseAbstractController::__construct($logger, $actionLogger);
    }

    /**
     * Lists all Device entities.
     *
     * @Route("/device", name="device")
     * @IsGranted("ROLE_NETWORK_OPERATOR")
     */
    public function indexAction(PaginatorInterface $paginator, Request $request, EntityManagerInterface $em)
    {
        $filter = [];
        $queryString = $request->get('queryString', false);
        if ($queryString) {
            $filter['queryString'] = $queryString;
        }

	    $filter['onlyActive'] = $request->get('onlyActive', 'off') == 'on';
	    $filter['onlyToApprove'] = $request->get('onlyToApprove', 'off') == 'on';
        $mikrotikListId = $request->get('mikrotikListId', false);
        if ((false !== $mikrotikListId) and ('' != $mikrotikListId)) {
            $filter['mikrotikListId'] = $mikrotikListId;
        }

        $query = $em->getRepository('App\Entity\Device')->getQueryFromFilter($filter);

        $pagination = $paginator->paginate(
            $query,
            $request->query->get('page', 1), 25
        );

        $mikrotikLists = $em->getRepository('App\Entity\MikrotikList')->findBy([], ['nome' => 'ASC']);

        $request->getSession()->set('macControllerLastQuery', $request->getQueryString());

        return $this->render(
            'device/index.html.twig',
            [
            'pagination' => $pagination,
            'filter' => $filter,
            'mikrotikLists' => $mikrotikLists,
        ]
        );
    }

    /**
     * Displays a form to create a new Device entity.
     *
     * @Route("/device/new", name="device_new")
     * @IsGranted("ROLE_NETWORK_OPERATOR")
     */
    public function newAction(Request $request, EntityManagerInterface $em)
    {
        $entity = new Device();
        $entity->setActive(true);

        $qs = $this->getLastQueryForRedirect($request);

        if (!empty($qs) && isset($qs['mikrotikListId']) && '' != $qs['mikrotikListId']) {
            $mikrotikList = $em->getRepository('App\Entity\MikrotikList')->find($qs['mikrotikListId']);
            if ($mikrotikList instanceof MikrotikList) {
                $entity->setMikrotikList($mikrotikList);
            }
        }

        $form = $this->createCreateForm($entity);

        $mikrotikPoolList = $em->getRepository('App\Entity\MikrotikPool')->findBy([], ['nome' => 'ASC']);

        return $this->render(
            'device/edit.html.twig',
            [
            'entity' => $entity,
            'main_form' => $form->createView(),
            'isEditing' => false,
            'mikrotikPoolList' => $mikrotikPoolList,
            'redirect_qs' => $qs,
        ]
        );
    }

    /**
     * Creates a new Device entity.
     *
     * @Route("/device/create", name="device_create", methods={"POST"})
     * @IsGranted("ROLE_NETWORK_OPERATOR")
     */
    public function createAction(MessageBusInterface $messageBus, Request $request, EntityManagerInterface $em)
    {
        $entity = new Device();
        $form = $this->createCreateForm($entity);
        $form->handleRequest($request);

	    $currentUserIsAdmin = $this->isGranted('ROLE_ADMIN');
	    if(!$currentUserIsAdmin) {
		    $entity->setActive(false);
		    $entity->setToApprove(true);
	    }

        if ($form->isValid()) {
            $em->persist($entity);
            $em->flush();

            $this->sendMQmessages($messageBus);

            return $this->redirect($this->generateUrl('device', $this->getLastQueryForRedirect($request)));
        }
        $mikrotikPoolList = $em->getRepository('App\Entity\MikrotikPool')->findBy([], ['nome' => 'ASC']);

	    $this->logAction("device-create","ha inserito il dispositivo {$entity->getMac()}", ['id' => $entity->getId()]);

        return $this->render(
            'device/edit.html.twig',
            [
            'entity' => $entity,
            'main_form' => $form->createView(),
            'isEditing' => false,
            'mikrotikPoolList' => $mikrotikPoolList,
            'redirect_qs' => $this->getLastQueryForRedirect($request),
        ]
        );
    }

	protected function createCreateForm($entity) {
		return $this->createForm('App\Form\DeviceType', $entity, [
			'action' => $this->generateUrl('device_create'),
			'method' => 'POST',
		]);
	}

    /**
     * Displays a form to edit an existing Device entity.
     *
     * @Route("/device/{id}/edit", name="device_edit")
     * @IsGranted("ROLE_NETWORK_OPERATOR")
     */
    public function editAction(Request $request, Device $device, EntityManagerInterface $em)
    {
        if (!$device) {
            throw $this->createNotFoundException('Unable to find Device entity.');
        }
        $mikrotikPoolList = $em->getRepository('App\Entity\MikrotikPool')->findBy([], ['nome' => 'ASC']);

        $editForm = $this->createEditForm($device);

        return $this->render(
            'device/edit.html.twig',
            [
            'entity' => $device,
            'main_form' => $editForm->createView(),
            'isEditing' => true,
            'mikrotikPoolList' => $mikrotikPoolList,
            'redirect_qs' => $this->getLastQueryForRedirect($request),
        ]
        );
    }

    /**
     * Edits an existing Device entity.
     *
     * @Route("/device/{id}/update", name="device_update", methods={"POST"})
     * @IsGranted("ROLE_NETWORK_OPERATOR")
     */
    public function updateAction(MessageBusInterface $messageBus, Request $request, Device $device, EntityManagerInterface $em)
    {
        if (!$device) {
            throw $this->createNotFoundException('Unable to find Device entity.');
        }

        $originalIps = clone $device->getIps();
	    $originallyActive = $device->getActive();
	    $originallyBypassed = $device->getBypassHotspot();
	    $originalMacAddress = $device->getMac();
	    $originalToApprove = (bool)$device->isToApprove();
	    $editForm = $this->createEditForm($device);

        $editForm->handleRequest($request);

	    $currentUserIsAdmin = $this->isGranted('ROLE_ADMIN');
	    if(!$currentUserIsAdmin) {
		    if(strtolower($originalMacAddress) !== strtolower($device->getMac())) {
			    $device->setActive(false);
			    $device->setToApprove(true);
			    //@todo notifica approvazione?
		    } else {
			    $device->setActive($originallyActive);
			    $device->setToApprove($originalToApprove);
		    }
	    } else {
		    $device->setToApprove(false);
	    }

	    if(!$currentUserIsAdmin) {
		    $device->setBypassHotspot($originallyBypassed);
	    }

        if ($editForm->isValid()) {
            foreach ($originalIps as $deviceIp) {
                if (false === $device->getIps()->contains($deviceIp)) {
                    $em->remove($deviceIp);
                }
            }

            $em->persist($device);
            $em->flush();

            $this->sendMQmessages($messageBus);

	        $this->logAction("device-modify","ha modificato il dispositivo {$device->getMac()}", ['id' => $device->getId()]);

            return $this->redirect($this->generateUrl('device', $this->getLastQueryForRedirect($request)));
        }

        $mikrotikPoolList = $em->getRepository('App\Entity\MikrotikPool')->findBy([], ['nome' => 'ASC']);

        return $this->render(
            'device/edit.html.twig',
            [
            'entity' => $device,
            'main_form' => $editForm->createView(),
            'isEditing' => true,
            'mikrotikPoolList' => $mikrotikPoolList,
            'redirect_qs' => $this->getLastQueryForRedirect($request),
        ]
        );
    }

	protected function createEditForm($entity) {
		return $this->createForm('App\Form\DeviceType', $entity, [
			'action' => $this->generateUrl('device_update', ['id' => $entity->getId()]),
			'method' => 'POST',
		]);
	}

    /**
     * Deletes a Device entity.
     *
     * @Route("/device/{id}/delete", name="device_delete")
     * @IsGranted("ROLE_ADMIN")
     */
    public function deleteAction(MessageBusInterface $messageBus, Request $request, Device $device, EntityManagerInterface $em)
    {
        if (!$device) {
            throw $this->createNotFoundException('Unable to find device entity.');
        }

        foreach ($device->getIps() as $deviceIp) {
            $em->remove($deviceIp);
        }

        $em->remove($device);
        $em->flush();

        $this->sendMQmessages($messageBus);

        return $this->redirect($this->generateUrl('device', $this->getLastQueryForRedirect($request)));
    }

    /**
     * Last query searched in list (to go back to the last search).
     *
     * @return array
     */
    protected function getLastQueryForRedirect(Request $request)
    {
        $qs = [];
        if ('' != $request->getSession()->get('macControllerLastQuery', '')) {
            parse_str($request->getSession()->get('macControllerLastQuery'), $qs);
        }

        return $qs;
    }

    private function sendMQmessages(MessageBusInterface $messageBus)
    {
        $msg = new MikrotikMessage();
        $msg->setCommand('addMacToHospot');
        $messageBus->dispatch(new Envelope($msg));
        $msg = new MikrotikMessage();
        $msg->setCommand('addIpReservation');
        $messageBus->dispatch(new Envelope($msg));
        $msg = new MikrotikMessage();
        $msg->setCommand('addIpList');
        $messageBus->dispatch(new Envelope($msg));
    }
}
