<?php

namespace App\Controller;

use App\Entity\Group;
use App\Entity\InternetOpen;
use App\Entity\Provider;
use App\Entity\Sector;
use App\Manager\ConfigurationManager;
use App\RepositoryAction\GroupRepositoryAction;
use Doctrine\ORM\EntityManagerInterface;
use Dompdf\Dompdf;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\IsGranted;
use Symfony\Component\Form\Extension\Core\Type\CheckboxType;
use Symfony\Component\Routing\Annotation\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\FormError;
use Symfony\Component\Form\FormInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpKernel\Exception\BadRequestHttpException;

/**
 * Group controller.
 */
class GroupController extends AbstractController
{
    /**
     * List all providers.
     *
     * @Route("/student/provider", name="group_providers_list")
     * @IsGranted("ROLE_TEACHER")
     * @Template()
     */
    public function listProvidersAction(EntityManagerInterface $em)
    {
        $filterCriteria = ['active' => true];
        if (!$this->isGranted('ROLE_ADMIN')) {
            $filterCriteria['internetTeachersControl'] = true;
        }

        $providers = $em->getRepository('App\Entity\Provider')->findBy($filterCriteria);

        return $this->render(
            'group/list_providers.html.twig',
            ['providers' => $providers]
        );
    }

    /**
     * Lists all Group entities in a provider.
     *
     * @Route("/student/provider/{providerId}", name="list_groups_in_provider")
     * @IsGranted("ROLE_TEACHER")
     */
    public function listGroupsInProviderAction(ConfigurationManager $configurationManager, EntityManagerInterface $em, Request $request, $providerId)
    {
        $ldapGroupRepository = $em->getRepository('App\Entity\LdapGroup');
        $active_directory_generated_group_prefix = $configurationManager->getActiveDirectoryGeneratedGroupPrefix();
        $provider = $em->getRepository('App\Entity\Provider')->find($providerId);
        if (!$provider instanceof Provider) {
	        return new BadRequestHttpException("Invalid provider");
        }

        $currentUserIsAdmin = $this->isGranted('ROLE_ADMIN');
        $currentUserName = $this->getUser()->getUserIdentifier();

        $currentSectorName = '';
        $sectorsList = new \Doctrine\Common\Collections\ArrayCollection();
        if ($currentSectorId = $request->get('sectorId', false)) {
            $currentSector = $em->getRepository('App\Entity\Sector')->find($currentSectorId);
            if (!$currentSector instanceof Sector) {
	            return new BadRequestHttpException("Invalid sector");
            }

            $groups = $currentSector->getGroups();
            $currentSectorName = $currentSector->getName();
        } else {
            $groups = $provider->getGroups();
            $sectorsList = $provider->getSectors();
        }

        $activatingGroup = false;
        $groupsList = [];
        foreach ($groups as $groupId => $group) {
            $groupName = (string) $group->getName();
            $ldapGroupName = $active_directory_generated_group_prefix.$ldapGroupRepository::ldapEscape($groupName);
            $state = $ldapGroupRepository->getInternetStatus($ldapGroupName, $active_directory_generated_group_prefix);
            $endingTime = '';
            $activedByOtherUser = $permitPersonalDevices = false;
            $activedBy = '';
            if ('group-active' == $state) {
                $internetOpen = $em->getRepository('App\Entity\InternetOpen')->findOneBy(['account' => $ldapGroupName, 'type' => 'group']);
                if ($internetOpen instanceof InternetOpen) {
                    $permitPersonalDevices = $internetOpen->getPermitPersonalDevices();
                    if (!$internetOpen->getActivationCompleated()) {
                        $state = 'activating';
                        $activatingGroup = true;
                    } else {
                        $endingTime = $internetOpen->getCloseAt()->format('H:i');

                        $activedBy = $currentUserIsAdmin && '' != $internetOpen->getActivedBy() ? $internetOpen->getActivedBy() : '';
                        if ('' != $currentUserName && $internetOpen->getActivedBy() != $currentUserName) {
                            $activedByOtherUser = true;
                        }
                    }
                }
            }

            $groupsList[$groupName]['id'] = $group->getId();
            $groupsList[$groupName]['name'] = $group->getName();
            $groupsList[$groupName]['state'] = $state;
            $groupsList[$groupName]['permitPersonalDevices'] = $permitPersonalDevices;
            $groupsList[$groupName]['endingTime'] = $endingTime;
            $groupsList[$groupName]['activedByOtherUser'] = $activedByOtherUser;
            $groupsList[$groupName]['activedBy'] = $activedBy;
        }
        ksort($groupsList);
        $timeToSet = new \DateTime('+1 hour');

        $activatedGroup = '';
        if ($activatingGroup && '' != $request->get('activatedGroup', '')) {
            $activatedGroup = $request->get('activatedGroup', '');
        }

        return $this->render(
            'group/list_groups_in_provider.html.twig',
            ['provider' => $provider, 'groups' => $groupsList, 'timeToSet' => $timeToSet, 'currentSectorId' => $currentSectorId,
                'currentSectorName' => $currentSectorName, 'sectorsList' => $sectorsList, 'currentUserIsAdmin' => $currentUserIsAdmin,
                'userCanEnablePersonalDevice' => ($currentUserIsAdmin || ($configurationManager->getTeacherCanEnablePersonalDevice() && $provider->getTeacherCanEnablePersonalDevicesForClassrooms())),
                'wifiSsid' => $configurationManager->getWirelessSsid(), 'activatedGroup' => $activatedGroup, ]
        );
    }

    /**
     * Redirect to group show.
     *
     * @Route("/student/group/{id}", name="group_show")
     * @IsGranted("ROLE_TEACHER")
     */
    public function showAction(EntityManagerInterface $em, $id)
    {
        $group = $em->getRepository('App\Entity\Group')->find($id);

        if (!$group instanceof Group || !$group->getProvider() instanceof Provider) {
            throw $this->createNotFoundException('Specified group not found');
        }

        return $this->redirectToRoute('list_students_in_group', ['providerId' => $group->getProvider()->getId(), 'groupId' => $group->getId()]);
    }

    /**
     * Displays a form to create a new Group entity.
     *
     * @Route("/student/provider/{providerId}/group/new", name="group_new")
     * @IsGranted("ROLE_ADMIN_TEACHER")
     */
    public function newAction(EntityManagerInterface $em, $providerId)
    {
        $entity = new Group();
        $entity->setManageManually(true);

        if ('' != $providerId) {
            $provider = $em->getRepository('App\Entity\Provider')->find($providerId);
            if ($provider instanceof Provider) {
                $entity->setProvider($provider);
            }
        }

        $form = $this->createCreateForm($providerId, $entity);

        return $this->render(
            'group/edit.html.twig',
            [
                'entity' => $entity,
                'main_form' => $form->createView(),
                'isEditing' => false,
                'providerId' => $providerId,
            ]
        );
    }

    /**
     * Creates a new Group entity.
     *
     * @Route("/student/provider/{providerId}/group/create", name="group_create", methods={"POST"})
     * @IsGranted("ROLE_ADMIN_TEACHER")
     */
    public function createAction(EntityManagerInterface $em, Request $request, GroupRepositoryAction $repositoryAction, $providerId)
    {
        $group = new Group();
        $group->setManageManually(true);

        if ('' != $providerId) {
            $provider = $em->getRepository('App\Entity\Provider')->find($providerId);
            if ($provider instanceof Provider) {
                $group->setProvider($provider);
            }
        }

        $form = $this->createCreateForm($providerId, $group);
        $form->handleRequest($request);

        if ($form->isValid()) {
            $em->persist($group);
            $em->flush();

            try {
                $repositoryAction->executeAfterCreate($group);
            } catch (\Exception $e) {
                foreach (explode(PHP_EOL, $e->getMessage()) as $message) {
                    $form->addError(new FormError($message));
                }
            }

            return $this->redirect($this->generateUrl('list_students_in_group', ['groupId' => $group->getId(), 'providerId' => $providerId]));
        }

        return $this->render(
            'group/edit.html.twig',
            [
                'entity' => $group,
                'main_form' => $form->createView(),
                'isEditing' => false,
                'providerId' => $providerId,
            ]
        );
    }

    /**
     * Creates a form to create a Group entity.
     *
     * @param Group $entity The entity
     *
     * @return FormInterface The form
     */
    private function createCreateForm($providerId, Group $entity)
    {
        $form = $this->createForm("App\Form\GroupType", $entity, [
            'action' => $this->generateUrl('group_create', ['providerId' => $providerId]),
            'method' => 'POST',
        ]);

	    $form->add('submit', SubmitType::class, ['label' => 'Salva Modifiche']);

        return $form;
    }

    /**
     * Displays a form to edit an existing Group entity.
     *
     * @Route("/student/provider/{providerId}/group/{groupId}/edit", name="group_edit")
     * @IsGranted("ROLE_ADMIN_TEACHER")
     */
    public function editAction(EntityManagerInterface $em, $providerId, $groupId)
    {
        $entity = $em->getRepository('App\Entity\Group')->find($groupId);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find Group entity.');
        }

        $editForm = $this->createEditForm($providerId, $entity);
//        $deleteForm = $this->createDeleteForm($providerId, $groupId);

        return $this->render(
            'group/edit.html.twig',
            [
                'entity' => $entity,
                'main_form' => $editForm->createView(),
                'isEditing' => true,
//            'delete_form' => $deleteForm->createView(),
                'providerId' => $providerId,
            ]
        );
    }

    /**
     * Edits an existing Group entity.
     *
     * @Route("/student/provider/{providerId}/group/{groupId}/update", name="group_update", methods={"POST"})
     * @IsGranted("ROLE_ADMIN_TEACHER")
     */
    public function updateAction(EntityManagerInterface $em, Request $request, GroupRepositoryAction $repositoryAction, $providerId, $groupId)
    {
        $group = $em->getRepository('App\Entity\Group')->find($groupId);

        if (!$group instanceof Group) {
            throw $this->createNotFoundException('Unable to find Group entity.');
        }

        $oldName = $group->getName();

//        $deleteForm = $this->createDeleteForm($providerId, $groupId);
        $editForm = $this->createEditForm($providerId, $group);
        $editForm->handleRequest($request);

        if ($editForm->isValid()) {
            $em->persist($group);
            $em->flush();

            try {
                $repositoryAction->executeAfterUpdate($group, $oldName);
            } catch (\Exception $e) {
                foreach (explode(PHP_EOL, $e->getMessage()) as $message) {
                    $editForm->addError(new FormError($message));
                }
            }

            return $this->redirect($this->generateUrl('list_students_in_group', ['groupId' => $groupId, 'providerId' => $providerId]));
        }

        return $this->render(
            'group/edit.html.twig', [
                'entity' => $group,
                'main_form' => $editForm->createView(),
				'isEditing' => true,
//            'delete_form' => $deleteForm->createView(),
                'providerId' => $providerId,
            ]
        );
    }

    /**
     * Creates a form to edit a Group entity.
     *
     * @param Group $entity The entity
     *
     * @return FormInterface The form
     */
    private function createEditForm($providerId, Group $entity)
    {
        $form = $this->createForm("App\Form\GroupType", $entity, [
            'action' => $this->generateUrl('group_update', ['groupId' => $entity->getId(), 'providerId' => $providerId]),
            'method' => 'POST',
        ]);

        $form->add('submit', SubmitType::class, ['label' => 'Salva Modifiche']);

        return $form;
    }

    /**
     * Present the form to delete a Student entity.
     *
     * @Route("/student/provider/{providerId}/group/{groupId}/remove", name="group_delete_form")
     * @IsGranted("ROLE_ADMIN_TEACHER")
     */
    public function deleteFormAction(EntityManagerInterface $em, Request $request, $providerId, $groupId)
    {
        $group = $em->getRepository('App\Entity\Group')->find($groupId);

        if (!$group) {
            throw $this->createNotFoundException('Unable to find Group entity.');
        }

        if (!$group->getManageManually()) {
            throw $this->createAccessDeniedException('Imported group cannot be deleted manually');
        }

        $form = $this->createDeleteForm($providerId, $groupId);

        return $this->render(
            'group/delete.html.twig',
            ['form' => $form->createView(), 'entity' => $group, 'providerId' => $providerId]
        );
    }

    /**
     * Deletes a Group entity.
     *
     * @Route("/student/provider/{providerId}/group/{groupId}", name="group_delete", methods={"DELETE"})
     * @IsGranted("ROLE_ADMIN_TEACHER")
     */
    public function deleteAction(Request $request, EntityManagerInterface $em, GroupRepositoryAction $repositoryAction, $providerId, $groupId)
    {
        $form = $this->createDeleteForm($providerId, $groupId);
        $form->handleRequest($request);

        if ($form->isValid()) {
            $group = $em->getRepository('App\Entity\Group')->find($groupId);

            if (!$group) {
                throw $this->createNotFoundException('Unable to find Group group.');
            }

            try {
                $repositoryAction->executeBeforeRemove($group, $form->get('deleteOnLdap')->getData(), $form->get('deleteOnGApps')->getData());
            } catch (\Exception $e) {
                foreach (explode(PHP_EOL, $e->getMessage()) as $message) {
                    $form->addError(new FormError($message));
                }
            }

            $em->remove($group);
            $em->flush();
        }

        return $this->redirect($this->generateUrl('list_groups_in_provider', ['providerId' => $providerId]));
    }

    /**
     * Creates a form to delete a Group entity by id.
     *
     * @param mixed $groupId The entity id
     *
     * @return FormInterface The form
     */
    private function createDeleteForm($providerId, $groupId)
    {
        return $this->createFormBuilder()
            ->setAction($this->generateUrl('group_delete', ['groupId' => $groupId, 'providerId' => $providerId]))
            ->setMethod('DELETE')
            ->add('deleteOnLdap', CheckboxType::class, ['label' => 'Cancellare da Active Directory', 'required' => false])
            ->add('deleteOnGApps', CheckboxType::class, ['label' => 'Cancellare da Google Apps', 'required' => false])
            ->add('submit', SubmitType::class, ['label' => 'Elimina', 'attr' => ['class' => 'btn-danger']])
            ->getForm();
    }

    /**
     * Show all the students in a group.
     *
     * @Route("/student/provider/{providerId}/list-group/{groupId}", name="list_students_in_group")
     * @IsGranted("ROLE_TEACHER")
     */
    public function listStudentsInGroupAction(EntityManagerInterface $em, ConfigurationManager $configurationManager, Request $request, $providerId, $groupId)
    {
        $active_directory_generated_group_prefix = $configurationManager->getActiveDirectoryGeneratedGroupPrefix();
//        $users = $em->getRepository('App\Entity\LdapGroup')->getAllChildrenUsers($active_directory_generated_group_prefix.$group);

        $provider = $em->getRepository('App\Entity\Provider')->find($providerId);
        $group = $em->getRepository('App\Entity\Group')->find($groupId);
        if (!$group instanceof Group) {
            throw $this->createNotFoundException('Non è stato possibile trovare il gruppo indicato.');
        }
        $students = $group->getStudents();
        $orderBy = $provider->getStudentsOrderByInTeacherList();

        $currentUserIsAdmin = $this->isGranted('ROLE_ADMIN') || $this->isGranted('ROLE_ADMIN_TEACHER');
        $currentUserName = $this->getUser()->getUserIdentifier();

        $ldapUsers = $em->getRepository('App\Entity\LdapUser')->findAll();
        foreach ($ldapUsers as $ldapUser) {
            $ldapUsersList[$ldapUser->getDistinguishedId()] = $ldapUser;
        }

        $activatingUser = false;
        $usersList = [];
        foreach ($students as $student) {
			if($student->isTrashed()) {
				continue;
			}

            $username = $student->getUsername();
            $state = $em->getRepository('App\Entity\LdapUser')->getInternetStatus($username, $active_directory_generated_group_prefix);
            $endingTime = '';
            $activedByOtherUser = $permitPersonalDevices = false;
            $activedBy = '';
            if ('user-active' == $state) { // @todo stato utente ldap non creato?
                $internetOpen = $em->getRepository('App\Entity\InternetOpen')->findOneBy(['account' => $username, 'type' => 'user']);
                if ($internetOpen instanceof InternetOpen) {
                    $permitPersonalDevices = $internetOpen->getPermitPersonalDevices();
                    if (!$internetOpen->getActivationCompleated()) {
                        $state = 'activating';
                        $activatingUser = true;
                    } else {
                        $endingTime = $internetOpen->getCloseAt()->format('H:i');

                        $activedBy = $currentUserIsAdmin && '' != $internetOpen->getActivedBy() ? $internetOpen->getActivedBy() : '';
                        if ('' != $currentUserName && $internetOpen->getActivedBy() != $currentUserName) {
                            $activedByOtherUser = true;
                        }
                    }
                }
            }

            switch ($orderBy) {
                case 'username':
                    $k = $username;
                    break;

                case 'firstname':
                    $k = $student->getFirstname().$student->getLastname().$student->getId();
                    break;

                default:
                case 'lastname':
                    $k = $student->getLastname().$student->getFirstname().$student->getId();
                    break;
            }

            $usersList[$k] = [
                'student' => $student,
                'state' => $state,
                'ending_time' => $endingTime,
                'permitPersonalDevices' => $permitPersonalDevices,
                'activedByOtherUser' => $activedByOtherUser,
                'activedBy' => $activedBy,
                'ldapUser' => @$ldapUsersList[(string) $student->getId()],
            ];
        }
        ksort($usersList);

        $timeToSet = new \DateTime('+1 hour');
        $activatedUser = '';
        if ($activatingUser && '' != $request->get('activatedUser', '')) {
            $activatedUser = $request->get('activatedUser', '');
        }

        return $this->render(
            'group/list_students_in_group.html.twig', [
                'group' => $group, 'students' => $usersList, 'timeToSet' => $timeToSet, 'provider' => $provider,
                'currentUserIsAdmin' => $currentUserIsAdmin, 'teacherCanSeeList' => $configurationManager->getTeacherCanSeeList(),
                'teacherCanResetPassword' => $configurationManager->getTeacherCanResetPassword(),
                'userCanEnablePersonalDevice' => ($currentUserIsAdmin || $configurationManager->getTeacherCanEnablePersonalDevice()),
                'wifiSsid' => $configurationManager->getWirelessSsid(), 'activatedUser' => $activatedUser,
            ]
        );
    }

    /**
     * Print the list of the students data, in pdf format.
     *
     * @Route("/student/provider/{providerId}/group/{groupId}/print", name="print_students_in_group")
     * @IsGranted("ROLE_TEACHER")
     */
    public function printStudentsInGroupAction(EntityManagerInterface $em, ConfigurationManager $configurationManager, Request $request, $providerId, $groupId)
    {
        $format = $request->get('_format', 'html');
        if (!$configurationManager->getTeacherCanSeeList() && !$this->isGranted('ROLE_ADMIN')) {
            throw $this->createAccessDeniedException();
        }

        $provider = $em->getRepository('App\Entity\Provider')->find($providerId);
        $group = $em->getRepository('App\Entity\Group')->find($groupId);
        $students = $group->getStudents();
        $showPassword = ($provider->getStudentForceImportedPassword()) ? false : true;
        $list = [];
        foreach ($students as $student) {
            $list[] = [
                'student' => $student,
                'ldapUser' => $em->getRepository('App\Entity\LdapUser')->findOneByDistinguishedId($student->getId()),
            ];
        }

        $pdfHeader = $pdfFooter = '';
        if ('' != $provider->getPdfHeader()) {
            $pdfHeader = $provider->getPdfHeader();
        }

        if ('' != $provider->getPdfFooter()) {
            $pdfFooter = $provider->getPdfFooter();
        }

        $out = $this->render(
            sprintf('group/show_students_group.%s.twig', $format), [
                'provider' => $provider,
                'group' => $group,
                'list' => $list,
                'showPassword' => $showPassword,
                'pdfHeader' => $pdfHeader,
                'pdfFooter' => $pdfFooter,
                'headerHeight' => $provider->getPdfHeaderHeight() > 0 ? $provider->getPdfHeaderHeight() : ('' != $pdfHeader ? 20 : 0),
                'footerHeight' => $provider->getPdfFooterHeight() > 0 ? $provider->getPdfFooterHeight() : ('' != $pdfFooter ? 20 : 0),
            ]
        );

        if ('pdf' == $format) {
            $dompdf = new Dompdf();
            $dompdf->loadHtml($out->getContent());
            $dompdf->getOptions()->set('isRemoteEnabled', true);

            $dompdf->render();
            $out->setContent($dompdf->output());
            $out->headers->set('Content-Type', 'application/pdf');
        }

        return $out;
    }
}
