<?php

namespace App\Controller;

use App\Entity\AppleSchool;
use App\Entity\AppleSchoolLocation;
use App\Form\AppleSchoolType;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\EntityManagerInterface;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\IsGranted;
use Symfony\Component\Routing\Annotation\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;

/**
 * Apple School controller.
 *
 * @Route(path="/config/appleSchool")
 */
class AppleSchoolController extends AbstractController
{
    /**
     * Creates a new Provider entity.
     *
     * @Route("/", name="config_apple_school_create")
     * @IsGranted("ROLE_ADMIN")
     * @Template("apple_school/edit.html.twig")
     */
    public function createAction(Request $request, EntityManagerInterface $entityManager)
    {
        $appleSchool = new AppleSchool();
        $form = $this->createForm(AppleSchoolType::class, $appleSchool, [
            'action' => $this->generateUrl('config_apple_school_create'),
            'method' => 'PUT',
        ]);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $entityManager->persist($appleSchool);
            $entityManager->flush();

            return $this->redirect($this->generateUrl('config_edit').'#appleSchool');
        }

        return [
            'entity' => $appleSchool,
            'main_form' => $form->createView(),
            'isEditing' => false
        ];
    }

    /**
     * Displays a form to edit an existing Provider entity.
     *
     * @Route("/{id}/edit", name="config_apple_school_edit")
     * @IsGranted("ROLE_ADMIN")
     * @Template()
     */
    public function editAction(Request $request, AppleSchool $appleSchool, EntityManagerInterface $entityManager)
    {
        if (!$appleSchool) {
            throw $this->createNotFoundException('Unable to find Provider entity.');
        }

        /** @var AppleSchoolLocation[]|ArrayCollection $originalLocations */
        $originalLocations = new ArrayCollection();
        foreach ($appleSchool->getLocations() as $location) {
            $originalLocations->add($location);
        }

        $editForm = $this->createForm(AppleSchoolType::class, $appleSchool, [
            'action' => $this->generateUrl('config_apple_school_edit', ['id' => (string)$appleSchool->getId()]),
            'method' => 'PUT',
        ]);
        $editForm->handleRequest($request);

        if ($editForm->isSubmitted() && $editForm->isValid()) {
            foreach ($originalLocations as $location) {
                if (false === $appleSchool->getLocations()->contains($location)) {
                    foreach ($location->getProviders() as $provider) {
                        $provider->setAppleSchoolLocation(null);
                    }
                    //					$location->getLocations()->removeElement($appleSchool);
                    $entityManager->remove($location);
                }
            }
            $entityManager->flush();

            return $this->redirect($this->generateUrl('config_edit', []).'#appleSchool');
        }

        return [
            'entity' => $appleSchool,
            'main_form' => $editForm->createView(),
            'isEditing' => true
        ];
    }

    /**
     * Deletes a Provider entity.
     *
     * @Route("/{id}", name="config_apple_school_delete", methods={"GET","DELETE"})
     * @IsGranted("ROLE_ADMIN")
     */
    public function deleteAction(Request $request, EntityManagerInterface $entityManager, $id)
    {
        $entity = $entityManager->getRepository(AppleSchool::class)->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find Apple School entity.');
        }

	    foreach ($entity->getLocations() as $location) {
		    $entityManager->remove($location);
	    }

        $entityManager->remove($entity);
        $entityManager->flush();

        return $this->redirect($this->generateUrl('config_edit').'#appleSchool');
    }
}
