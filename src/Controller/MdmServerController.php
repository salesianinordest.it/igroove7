<?php

namespace App\Controller;

use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\FormInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\IsGranted;
use Symfony\Component\Finder\Finder;
use App\Entity\MdmServer;
use App\Form\MdmServerType;

/**
 * Mdm Server controller.
 *
 * @Route(path="/config/mdmServer")
 */
class MdmServerController extends BaseAbstractController {


    /**
     * Creates a new MdmServer entity.
     *
     * @Route(path="/", name="config_mdmServer_create", methods={"POST"})
     * @IsGranted("ROLE_ADMIN")
     * @Template("mdm_server/edit.html.twig")
     */
    public function createAction(Request $request, EntityManagerInterface $em)
    {
        $entity = new MdmServer();
        $form = $this->createCreateForm($entity);
        $form->handleRequest($request);

        $this->prepareFilterMdmServerData($entity,$form);

        if ($form->isValid()) {
            $em->persist($entity);
            $em->flush();

            return $this->redirect($this->generateUrl('config_edit').'#mdmServer');
        }

        return array(
            'entity' => $entity,
            'main_form'   => $form->createView(),
            'isEditing' => false
        );
    }

    /**
     * Creates a form to create a MdmServer entity.
     *
     * @param MdmServer $entity The entity
     * @IsGranted("ROLE_ADMIN")
     * @return FormInterface The form
     */
    private function createCreateForm(MdmServer $entity): FormInterface
    {
       $form = $this->createForm(MdmServerType::class, $entity, array(
            'action' => $this->generateUrl('config_mdmServer_create'),
            'method' => 'POST',
            'mdmConnectors' => $this->getMdmConnectorsList()
        ));

        $form->add('submit', SubmitType::class, array('label' => 'Crea'));

        return $form;
    }

    /**
     * Displays a form to create a new MdmServer entity.
     *
     * @Route(path="/new", name="config_mdmServer_new", methods={"GET"})
     * @IsGranted("ROLE_ADMIN")
     * @Template("mdm_server/edit.html.twig")
     */
    public function newAction(): array
    {
        $entity = new MdmServer();
        $form   = $this->createCreateForm($entity);

        return array(
            'entity' => $entity,
            'main_form'   => $form->createView(),
            'isEditing' => false
        );
    }

    /**
     * Displays a form to edit an existing MdmServer entity.
     *
     * @Route(path="/{id}", name="config_mdmServer_edit", methods={"GET"})
     * @IsGranted("ROLE_ADMIN")
     * @Template()
     */
    public function editAction(EntityManagerInterface $em, $id): array
    {
        $entity = $em->getRepository(MdmServer::class)->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find MdmServer entity.');
        }

        $editForm = $this->createEditForm($entity);
        $deleteForm = $this->createDeleteForm($id);

        return array(
            'entity'      => $entity,
            'main_form'   => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
            'isEditing' => true
        );
    }

    /**
     * Creates a form to edit a MdmServer entity.
     *
     * @param MdmServer $entity The entity
     *
     * @return FormInterface The form
     */
    private function createEditForm(MdmServer $entity): FormInterface
    {
        $form = $this->createForm(MdmServerType::class, $entity, array(
            'action' => $this->generateUrl('config_mdmServer_update', array('id' => $entity->getId())),
            'method' => 'PUT',
	        'mdmConnectors' => $this->getMdmConnectorsList()
        ));

	    $connectors = $this->getMdmConnectorsList();
        if(!empty($entity->getConnectionData()) && $entity->getType() != "" && isset($connectors[$entity->getType()])) {
            $filterData = $entity->getConnectionData();
            foreach($form->get("connectorDataCont")->get($entity->getType())->all() as $dataField) {
                if(isset($filterData[$dataField->getName()])) {
                    $dataField->setData($filterData[$dataField->getName()]);
                }
            }
        }

        $form->add('submit', SubmitType::class, array('label' => 'Salva'));

        return $form;
    }
    /**
     * Edits an existing MdmServer entity.
     *
     * @Route(path="/{id}", name="config_mdmServer_update", methods={"PUT"})
     * @IsGranted("ROLE_ADMIN")
     * @Template("mdm_server/edit.html.twig")
     */
    public function updateAction(Request $request, EntityManagerInterface $em, $id)
    {
        $entity = $em->getRepository(MdmServer::class)->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find MdmServer entity.');
        }

        $deleteForm = $this->createDeleteForm($id);
        $editForm = $this->createEditForm($entity);
        $editForm->handleRequest($request);

        $this->prepareFilterMdmServerData($entity,$editForm);

        if ($editForm->isValid()) {
            $em->flush();

            return $this->redirect($this->generateUrl('config_edit').'#mdmServer');
        }

        return array(
            'entity'      => $entity,
            'main_form'   => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
            'isEditing'   => true
        );
    }

    /**
     * Deletes a MdmServer entity.
     *
     * @Route(path="/{id}", name="config_mdmServer_delete", methods={"DELETE"})
     * @IsGranted("ROLE_ADMIN")
     */
    public function deleteAction(Request $request, EntityManagerInterface $em, $id): \Symfony\Component\HttpFoundation\RedirectResponse
    {
        $form = $this->createDeleteForm($id);
        $form->handleRequest($request);

        if ($form->isValid()) {
            $entity = $em->getRepository(MdmServer::class)->find($id);

            if (!$entity) {
                throw $this->createNotFoundException('Unable to find MdmServer entity.');
            }

            $em->remove($entity);
            $em->flush();
        }

        return $this->redirect($this->generateUrl('config_edit').'#mdmServer');
    }

    /**
     * Creates a form to delete a MdmServer entity by id.
     *
     * @param mixed $id The entity id
     *
     * @return FormInterface The form
     */
    private function createDeleteForm($id): FormInterface {
        return $this->createFormBuilder()
            ->setAction($this->generateUrl('config_mdmServer_delete', array('id' => $id)))
            ->setMethod('DELETE')
            ->add('submit', SubmitType::class, ['label' => 'Elimina', 'attr' => ['class' => "btn-danger", 'onclick' => "return confirm('Sei sicuro di voler eliminare questo elemento?');"]])
            ->getForm();
    }

    protected function getMdmConnectorsList() {
        static $connectors;
        if(!isset($connectors)) {
            $finder = new Finder();
            $finder->files()->in(__DIR__."/../MdmConnector");
            foreach($finder as $file) {
                if($file->getRelativePathname() == "AbstractMdmConnector.php" || $file->getRelativePathname() == "")
                    continue;

                $class = "App\\MdmConnector\\".substr($file->getRelativePathname(),0,-4);
                if(!class_exists($class))
                    continue;

                $connectors[$class::$internalName] = ['name' => $class::$name, 'ui' => $class::$parametersUi];
            }
        }

        return $connectors;
    }

    protected function prepareFilterMdmServerData(MdmServer $mdmServer, FormInterface $form) {
        $connectorType = $mdmServer->getType();

        if(!$form->get("connectorDataCont")->has($connectorType) || !is_array($form->get("connectorDataCont")->get($connectorType)->getData()))
            return;

        $connectors = $this->getMdmConnectorsList();
        $currentConnectionData = $mdmServer->getConnectionData();
        foreach($form->get("connectorDataCont")->get($connectorType)->getData() as $fieldName => $fieldValue) {
            if(!isset($connectors[$connectorType]['ui'][$fieldName]))
                continue;

            if($connectors[$connectorType]['ui'][$fieldName]['type'] == "password" && $fieldValue == "")
                continue;

            $currentConnectionData[$fieldName] = $fieldValue;
        }

        $mdmServer->setConnectionData($currentConnectionData);
    }
    
}
