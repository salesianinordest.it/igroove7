<?php

namespace App\Controller;

use App\Entity\Guest;
use App\Manager\ConfigurationManager;
use Doctrine\ORM\EntityManagerInterface;
use Fazland\SkebbyRestClient\Client\Client;
use Fazland\SkebbyRestClient\Constant\SendMethods;
use Fazland\SkebbyRestClient\DataStructure\Sms;
use Knp\Component\Pager\PaginatorInterface;
use Knp\Snappy\Pdf;
use PhpOffice\PhpSpreadsheet\IOFactory;
use PhpOffice\PhpSpreadsheet\Spreadsheet;
use PhpOffice\PhpSpreadsheet\Writer\Xlsx;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\IsGranted;
use Symfony\Component\Routing\Annotation\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Form\Extension\Core\Type\DateType;
use Symfony\Component\Form\Extension\Core\Type\FileType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\ResponseHeaderBag;
use Symfony\Component\HttpFoundation\Session\Session;
use Symfony\Component\HttpFoundation\StreamedResponse;
use Symfony\Component\Mailer\Exception\TransportExceptionInterface;
use Symfony\Component\Mailer\MailerInterface;
use Symfony\Component\Mime\Email;

/**
 * Guest controller.
 */
class GuestController extends AbstractController
{

    /**
     * Lists all Guest entities.
     *
     * @Route("/guest", name="guest")
     * @IsGranted("ROLE_RECEPTION")
     * @Template()
     */
    public function indexAction(Request $request, PaginatorInterface $paginator, EntityManagerInterface $entityManager)
    {
        $queryString = $request->get('queryString', false);

        $showInactive = 'on' == $request->get('showInactive', 'off') ? true : false;

        $todayStart = date('Y-m-d') . ' 00:00:00';
        $todayEnd = date('Y-m-d') . ' 23:59:59';
        $timeWhere = '(g.starting_from<= :todayEnd AND g.ending_to >= :todayStart)';

        if ($queryString) {
            $q = '%' . $queryString . '%';

            if ($showInactive) {
                $query = $entityManager->createQuery(
                    'SELECT g
            FROM App\Entity\Guest g
            WHERE g.firstname LIKE :q
            OR g.lastname  LIKE :q
            ORDER BY g.id DESC, g.lastname ASC , g.firstname ASC '
                )
                    ->setParameter('q', $q);
            } else {
                $query = $entityManager->createQuery(
                    "SELECT g
            FROM App\Entity\Guest g
            WHERE g.firstname LIKE :q
            OR g.lastname  LIKE :q AND $timeWhere
            ORDER BY g.id DESC, g.lastname ASC , g.firstname ASC "
                )
                    ->setParameter('q', $q)
                    ->setParameter('todayStart', $todayStart)->setParameter('todayEnd', $todayEnd);
            }
        } else {
            if ($showInactive) {
                $query = $entityManager->createQuery(
                    'SELECT g
            FROM App\Entity\Guest g
            ORDER BY g.id DESC, g.lastname ASC , g.firstname ASC '
                );
            } else {
                $query = $entityManager->createQuery(
                    "SELECT g
            FROM App\Entity\Guest g
            WHERE $timeWhere
            ORDER BY g.id DESC, g.lastname ASC , g.firstname ASC "
                )->setParameter('todayStart', $todayStart)->setParameter('todayEnd', $todayEnd);
            }
        }
        $pagination = $paginator->paginate(
            $query,
            $request->query->get('page', 1),
            25
        );

        return [
            'pagination' => $pagination,
            'showInactive' => $showInactive,
        ];
    }

    /**
     * Finds and displays a Guest entity.
     *
     * @Route("/guest/{id}/show", name="guest_show")
     * @IsGranted("ROLE_RECEPTION")
     * @Template()
     */
    public function showAction($id, EntityManagerInterface $entityManager)
    {
        $entity = $entityManager->getRepository(Guest::class)->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find Guest entity.');
        }

        $deleteForm = $this->createDeleteForm($id);

        return [
            'entity' => $entity,
            'delete_form' => $deleteForm->createView(),
        ];
    }

    /**
     * Displays a form to create a new Guest entity.
     *
     * @Route("/guest/new", name="guest_new")
     * @IsGranted("ROLE_RECEPTION")
     * @Template("guest/edit.html.twig")
     */
    public function newAction()
    {
        $now = new \DateTime();

        $entity = new Guest();
        $entity->setIdentifiedBy((string)$this->getUser()->getUserIdentifier());
        $entity->setStartingFrom($now);
        $entity->setEndingTo($now);
        $form = $this->createForm('App\Form\GuestType', $entity, [
	        'action' => $this->generateUrl('guest_create'),
	        'method' => 'POST',
        ]);
	    $form->add('submit', SubmitType::class, ['label' => 'Salva Modifiche']);

        return [
            'entity' => $entity,
            'main_form' => $form->createView(),
            'isEditing' => false
        ];
    }

    /**
     * Creates a new Guest entity.
     *
     * @Route("/guest/create", name="guest_create", methods={"POST"})
     * @IsGranted("ROLE_RECEPTION")
     * @Template("guest/edit.html.twig")
     */
    public function createAction(Request $request, EntityManagerInterface $entityManager)
    {
        $entity = new Guest();
        $form = $this->createForm('App\Form\GuestType', $entity, [
	        'action' => $this->generateUrl('guest_create'),
	        'method' => 'POST',
        ]);
	    $form->add('submit', SubmitType::class, ['label' => 'Salva Modifiche']);
        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid()) {
            $entity->setPassword();
            $entityManager->persist($entity);
            $entityManager->flush();

            return $this->redirect($this->generateUrl('guest_edit', ['id' => $entity->getId()]));
        }

        return [
            'entity' => $entity,
            'main_form' => $form->createView(),
            'isEditing' => false
        ];
    }

    /**
     * Displays a form to edit an existing Guest entity.
     *
     * @Route("/guest/{id}/edit", name="guest_edit")
     * @IsGranted("ROLE_RECEPTION")
     * @Template()
     */
    public function editAction(request $request, Guest $guest, EntityManagerInterface $entityManager)
    {
        if (!$guest) {
            throw $this->createNotFoundException('Unable to find Guest entity.');
        }

        $editForm = $this->createForm('App\Form\GuestType', $guest);
	    $editForm->add('submit', SubmitType::class, ['label' => 'Salva Modifiche']);

        $editForm->handleRequest($request);

        if ($editForm->isSubmitted() && $editForm->isValid()) {
            if (!$guest->getPassword()) {
                $guest->setPassword();
            }
            $entityManager->flush();

            return $this->redirect($this->generateUrl('guest', []));
        }

        return [
            'entity' => $guest,
            'main_form' => $editForm->createView(),
            'isEditing' => true
        ];
    }

    /**
     * Deletes a Guest entity.
     *
     * @Route("/guest/{id}/delete", name="guest_delete")
     * @IsGranted("ROLE_RECEPTION")
     */
    public function deleteAction($id, EntityManagerInterface $entityManager)
    {
        $entity = $entityManager->getRepository(Guest::class)->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find Guest entity.');
        }

        $entityManager->remove($entity);
        $entityManager->flush();

        return $this->redirect($this->generateUrl('guest'));
    }

    /**
     * Check a Guest entity.
     *
     * @Route("/checkGuest/guest{id}/{password}", name="checkGuest")
     */
    public function checkGuestAction($id, $password, EntityManagerInterface $entityManager)
    {
        $query = $entityManager->getRepository(Guest::class)
            ->createQueryBuilder('g')
            ->select('g.id')
            ->where(
                'g.starting_from <= :date and g.ending_to >= :date and
                    g.id = :id and g.password = :password'
            )
            ->setParameter('date', new \DateTime('today'))
            ->setParameter('password', $password)
            ->setParameter('id', $id)
            ->getQuery();
        $results = $query->getResult();

        if (!$results) {
            return new Response('fail');
        }

        return new Response('ok');
    }

    /**
     * Print a badge of an existing Guest entity.
     *
     * @Route("/guest/{id}/print", name="guest_print")
     * @IsGranted("ROLE_RECEPTION")
     * @Template()
     */
    public function printAction(ConfigurationManager $configurationManager, $id, EntityManagerInterface $entityManager)
    {
        $entity = $entityManager->getRepository(Guest::class)->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find Guest entity.');
        }
        $ssid = $configurationManager->getWirelessSsid();
        $network_type = $configurationManager->getWirelessGuestNetworkType();
        $rules = $configurationManager->getWirelessGuestNetworkRules();
        $realm = $configurationManager->getGuestRealm();

        return [
            'entity' => $entity,
            'ssid' => $ssid,
            'rules' => $rules,
            'network_type' => $network_type,
            'realm' => '' != $realm ? '@' . $realm : '',
        ];
    }


    /**
     * Print advanced badge of an existing Guest entity.
     *
     * @Route("/guest/{id}/print-advanced/badge", name="guest_print_advanced_badge")
     * @IsGranted("ROLE_RECEPTION")
     * ì     */
    public function printAdvancedBadgeAction(ConfigurationManager $configurationManager, $id, EntityManagerInterface $entityManager, $cacheDir)
    {
        $guest = $entityManager->getRepository(Guest::class)->find($id);

        if (!$guest) {
            throw $this->createNotFoundException('Unable to find Guest entity.');
        }
        $ssid = $configurationManager->getWirelessSsid();
        $network_type = $configurationManager->getWirelessGuestNetworkType();
        $rules = $configurationManager->getWirelessGuestNetworkRules();
        $realm = $configurationManager->getGuestRealm();
        $html = $configurationManager->getWirelessGuestHtmlBadge();

        $username = (strlen($realm) > 0) ? 'guest' . $guest->getId() . '@' . $realm : 'guest' . $guest->getId();

        $html = str_replace([
            '{username}',
            '{id}',
            '{password}',
            '{firstname}',
            '{lastname}',
            '{startingFrom}',
            '{endingTo}',
            '{ssid}',
            '{realm}'
        ], [
            $username,
            $guest->getId(),
            $guest->getPassword(),
            $guest->getFirstname(),
            $guest->getLastname(),
            $guest->getStartingFrom()->format('d/m/Y'),
            $guest->getEndingTo()->format('d/m/Y'),
            $ssid,
            $realm
        ], $html);


//        echo $html;die;
        $snappy = new Pdf('/usr/bin/wkhtmltopdf');
        $tmpFile = $cacheDir . DIRECTORY_SEPARATOR . uniqid() . '.pdf';
        $snappy->generateFromHtml($html, $tmpFile);
        $response = new Response(file_get_contents($tmpFile));

        $response->headers->set('Content-Type', 'application/pdf');
        return $response;
    }


    /**
     * Print advanced badge of an existing Guest entity.
     *
     * @Route("/guest/{id}/print-advanced/instructions", name="guest_print_advanced_instructions")
     * @IsGranted("ROLE_RECEPTION")
     * ì     */
    public function printAdvancedInstructionsAction(ConfigurationManager $configurationManager, $id, EntityManagerInterface $entityManager, $cacheDir)
    {
        $guest = $entityManager->getRepository(Guest::class)->find($id);

        if (!$guest) {
            throw $this->createNotFoundException('Unable to find Guest entity.');
        }
        $ssid = $configurationManager->getWirelessSsid();
        $network_type = $configurationManager->getWirelessGuestNetworkType();
        $rules = $configurationManager->getWirelessGuestNetworkRules();
        $realm = $configurationManager->getGuestRealm();
        $html = $configurationManager->getWirelessGuestHtmlinstructions();

        $username = (strlen($realm) > 0) ? 'guest' . $guest->getId() . '@' . $realm : 'guest' . $guest->getId();

        $html = str_replace([
            '{username}',
            '{id}',
            '{password}',
            '{firstname}',
            '{lastname}',
            '{startingFrom}',
            '{endingTo}',
            '{ssid}',
            '{realm}'
        ], [
            $username,
            $guest->getId(),
            $guest->getPassword(),
            $guest->getFirstname(),
            $guest->getLastname(),
            $guest->getStartingFrom()->format('d/m/Y'),
            $guest->getEndingTo()->format('d/m/Y'),
            $ssid,
            $realm
        ], $html);


//        echo $html;
//        die;
        $snappy = new Pdf('/usr/bin/wkhtmltopdf');
        $tmpFile = $cacheDir . DIRECTORY_SEPARATOR . uniqid() . '.pdf';
        $snappy->generateFromHtml($html, $tmpFile);
        $response = new Response(file_get_contents($tmpFile));

        $response->headers->set('Content-Type', 'application/pdf');
        return $response;
    }


    /**
     * Send the student credential to the specified email.
     *
     * @IsGranted("ROLE_RECEPTION")
     * @Route("/guest/{id}/send_credential_to_email", name="guest_send_credential_to_email")
     */
    public function sendCredentialToEmailAction(ConfigurationManager $configurationManager, Request $request, $id, EntityManagerInterface $entityManager, MailerInterface $mailer)
    {
        $response = new Response();
        $response->headers->set('Content-Type', 'application/json');

        $guest = $entityManager->getRepository(Guest::class)->find($id);
        if (!$guest instanceof Guest) {
            $response->setContent(json_encode(['success' => false, 'error' => 'Guest non valido']));

            return $response;
        }

        $entityManagerail = trim($request->get('emailTo', ''));
        if (!filter_var($entityManagerail, FILTER_VALIDATE_EMAIL)) {
            $response->setContent(json_encode(['success' => false, 'error' => 'Email specificata non valida']));

            return $response;
        }

        $ssid = $configurationManager->getWirelessSsid();
        $network_type = $configurationManager->getWirelessGuestNetworkType();
        $realm = '' != $configurationManager->getGuestRealm() ? '@' . $configurationManager->getGuestRealm() : '';

        $body = <<<MAIL
Riepilogo credenziali per l'utenza guest di {$guest->getLastname()} {$guest->getFirstname()}:

Username: guest{$guest->getId()}{$realm}
Password: {$guest->getPassword()}
Rete wifi (SSID): {$ssid}
Cifratura: {$network_type}
Valido dal: {$guest->getStartingFrom()->format('d/m/Y')}
Fino al: {$guest->getEndingTo()->format('d/m/Y')}
MAIL;


        $email = (new Email())
            ->from('no-reply@igroove.network')
            ->to($entityManagerail)
            ->subject("Credenziali per l'utenza guest di {$guest->getLastname()} {$guest->getFirstname()}")
            ->text($body);

        try {
            $mailer->send($email);
            $response->setContent(json_encode(['success' => true]));
        } catch (TransportExceptionInterface $e) {
            $response->setContent(json_encode(['success' => false, 'error' => "Errore durante l'invio dell'email"]));
        }

        return $response;
    }

    /**
     * Check a Guest entity.
     *
     * @Route("/guest/register/sms", name="guest_register_via_sms")
     * @Template()
     */
    public function registerViaSmsAction(ConfigurationManager $configurationManager, Request $request, EntityManagerInterface $entityManager)
    {
        $ssid = $configurationManager->getWirelessSsid();

        if (!$configurationManager->getGuestCanRegisterViaSms()) {
            throw new \Exception('Registration disabled.');
        }

        $entity = new Guest();
        $entity->sms = '';
        $form = $this->createForm('App\Form\GuestSmsType', $entity);
        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid()) {
            $string = 'SMS: ' . $entity->sms;

            $exist = $entityManager->getRepository(Guest::class)->findOneBy(['identified_by' => $string]);
            if ($exist) {
                $exist->setPassword();
                $exist->setEndingTo(new \DateTime('+1 year'));
                $guest = $exist;
            } else {
                $entity->setPassword();
                $entity->setEndingTo(new \DateTime('+1 year'));
                $entity->setStartingFrom(new \DateTime());
                $entity->setIdentifiedBy($string);
                $entityManager->persist($entity);
                $guest = $entity;
            }
            $entityManager->flush();
            $result = $this->sendPasswordViaSms($configurationManager, $guest);

            return $result;
        }

        return [
            'entity' => $entity,
            'form' => $form->createView(),
            'ssid' => $ssid,
        ];
    }

    /**
     * Check a Guest entity.
     *
     * @Route("/guest/recover-password/sms", name="guest_password_recover_via_sms")
     */
    public function passwordRecoverViaSmsAction(ConfigurationManager $configurationManager, Request $request, EntityManagerInterface $entityManager)
    {
        if (!$configurationManager->getGuestCanRegisterViaSms()) {
            throw new \Exception('Registration disabled.');
        }
        $string = 'SMS: ' . $request->get('sms');

        $exists = $entityManager->getRepository(Guest::class)->findOneBy(['identified_by' => $string]);

        if ($exists) {
            $exists->setEndingTo(new \DateTime('+1 year'));
            $exists->setPassword();
            $entityManager->flush();
            $result = $this->sendPasswordViaSms($configurationManager, $exists);

            return $result;
        }

        return $this->render('guest/guest_not_found.html.twig', []);
    }

    private function sendPasswordViaSms(ConfigurationManager $configurationManager, Guest $guest)
    {
        if (!$configurationManager->getGuestCanRegisterViaSms()) {
            throw new \Exception('Registration disabled.');
        }
        $ssid = $configurationManager->getWirelessSsid();

        $skebbyRestClient = new Client([
            'username' => $configurationManager->getSkebbyUsername(),
            'password' => $configurationManager->getSkebbyPassword(),
            'sender' => 'igroove',
            'method' => SendMethods::CLASSIC,
            'endpoint_uri' => 'https://gateway.skebby.it/api/send/smseasy/advanced/rest.php', // (default)
        ]);

        $message = 'Gentile ' . $guest->getFirstname() . ' ' . $guest->getLastname()
            . ' puoi accedere alla rete wireless con SSID: ' . $ssid . ' utilizzando come username guest' . $guest->getId()
            . ' e come password ' . $guest->getPassword() . '. L\'account e\' valido fino al ' . $guest->getEndingTo()->format('d/m/Y') . '. Buona connessione.';

        $phoneSms = '+39' . substr($guest->getIdentifiedBy(), 5);

        $sms = Sms::create()
            ->setRecipients([$phoneSms])
            ->setText($message);

        $skebbyRestClient->send($sms);
        /*
                if ($skebbyRestClient->isResultError()) {
                    return $this->render('guest/sms_send_fail.html.twig', array());
                }
        */
        return $this->render('guest/sms_send_ok.html.twig', []);
    }

    /**
     * massive add.
     *
     * @Route("/guest/massive-add", name="guest_add_massive")
     */
    public function addMassive(ConfigurationManager $configurationManager, Request $request)
    {
        $form = $this->createMassiveAddForm();
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $data = $form->getData();
            $guests = $this->prepareMassiveAdd($data);
            $session = new Session();
            $session->getFlashBag()->add('guests', $guests);

            return $this->render('guest/massive_add_confirm.html.twig', ['guests' => $guests]);
        }

        return $this->render('guest/massive_add.html.twig', ['form' => $form->createView()]);
    }

    private function createMassiveAddForm()
    {
        $defaultData = ['starting_from' => new \DateTime(), 'ending_to' => new \DateTime()];

        $form = $this->createFormBuilder($defaultData)
            ->add('file', FileType::class, ['label' => 'File MS Excel con i nominativi', 'required' => true, 'attr' => ['class' => 'btn btn-info']])
            ->add('identified_by', TextType::class, ['label' => 'Identificato attraverso', 'required' => true, 'attr' => [
                'placeholder' => 'Indicare l\'evento e il nome del referente che ha chiesto la creazione massiva',
            ]])
            ->add('starting_from', DateType::class, ['widget' => 'single_text', 'label' => 'Accesso attivo dal'])
            ->add('ending_to', DateType::class, ['widget' => 'single_text', 'label' => 'Accesso attivo fino al'])
            ->getForm();

        return $form;
    }

    private function prepareMassiveAdd($data)
    {
        $list = [];
        $identified_by = $data['identified_by'];
        $starting_from = $data['starting_from'];
        $ending_to = $data['ending_to'];
        $file = $data['file']->getPathname();
        $spreadsheet = IOFactory::load($file);
        $sheetData = $spreadsheet->getActiveSheet()->toArray(null, true, true, true);
        for ($i = 2; $i <= sizeof($sheetData); ++$i) {
            $list[] = [
                'lastname' => $sheetData[$i]['A'],
                'firstname' => $sheetData[$i]['B'],
                'starting_from' => $starting_from,
                'ending_to' => $ending_to,
                'identified_by' => $identified_by,
            ];
        }

        return $list;
    }

    /**
     * massive add.
     *
     * @Route("/guest/massive-add/confirmed", name="guest_add_massive_confirmed")
     */
    public function addMassiveConfirmed(Request $request, EntityManagerInterface $entityManager)
    {
        $session = new Session();
        $guests = $session->getFlashBag()->get('guests', []);
        $guestEntity = [];
        $i = 0;
        if (isset($guests[0])) {
            foreach ($guests[0] as $guestData) {
                $guestEntity[$i] = new Guest();
                $guestEntity[$i]->setFirstname($guestData['firstname']);
                $guestEntity[$i]->setLastname($guestData['lastname']);
                $guestEntity[$i]->setIdentifiedBy($guestData['identified_by']);
                $guestEntity[$i]->setStartingFrom($guestData['starting_from']);
                $guestEntity[$i]->setEndingTo($guestData['ending_to']);
                $guestEntity[$i]->setPassword();
                $entityManager->persist($guestEntity[$i]);
                $entityManager->flush();
                ++$i;
            }
        }
        $spreadsheet = new Spreadsheet();
        $spreadsheet->setActiveSheetIndex(0);
        $spreadsheet->getActiveSheet()
            ->setCellValue('A1', 'Cognome')
            ->setCellValue('B1', 'Nome')
            ->setCellValue('C1', 'Username')
            ->setCellValue('D1', 'Password');

        $i = 2;
        foreach ($guestEntity as $item) {
            $spreadsheet->getActiveSheet()
                ->setCellValue('A' . $i, $item->getLastname())
                ->setCellValue('B' . $i, $item->getFirstname())
                ->setCellValue('C' . $i, 'guest' . $item->getId())
                ->setCellValue('D' . $i, $item->getPassword());
            ++$i;
        }

        return $this->getResponseForXLS($spreadsheet);
    }

    public function getResponseForXLS($spreadsheet)
    {
        $writer = new Xlsx($spreadsheet);
        $response = new StreamedResponse(
            function () use ($writer) {
                $writer->save('php://output');
            }
        );
        $dispositionHeader = $response->headers->makeDisposition(
            ResponseHeaderBag::DISPOSITION_ATTACHMENT,
            'guests.xlsx'
        );
        $response->headers->set('Content-Type', 'text/vnd.ms-excel; charset=utf-8');
        $response->headers->set('Pragma', 'public');
        $response->headers->set('Cache-Control', 'maxage=1');
        $response->headers->set('Content-Disposition', $dispositionHeader);

        return $response;
    }
}
