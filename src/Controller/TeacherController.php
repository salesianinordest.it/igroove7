<?php

namespace App\Controller;

use App\Entity\Provider;
use App\Entity\Teacher;
use App\Manager\PersonsAndGroups;
use App\RepositoryAction\TeacherRepositoryAction;
use Doctrine\DBAL\Driver\AbstractMySQLDriver;
use Doctrine\ORM\EntityManagerInterface;
use Knp\Component\Pager\PaginatorInterface;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\IsGranted;
use Symfony\Component\Routing\Annotation\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Form\Extension\Core\Type\CheckboxType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormError;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Mailer\Exception\TransportExceptionInterface;
use Symfony\Component\Mailer\MailerInterface;
use Symfony\Component\Mime\Email;
use Symfony\Component\Uid\Uuid;


class TeacherController extends AbstractController
{
    /**
     * Lists all Teacher entities.
     *
     * @Route("/teacher", name="teacher_list")
     * @IsGranted("ROLE_ADMIN")
     * @Template()
     */
    public function indexAction(PaginatorInterface $paginator, Request $request, EntityManagerInterface $em)
    {
        $providers = $em->getRepository('App\Entity\Provider')->findBy([], ['name' => 'ASC']);

        $query = $em->getRepository('App\Entity\Teacher')->createQueryBuilder('t');
        $query->orderBy('t.lastname', 'ASC');

        if ($request->query->has('queryString') && '' != trim($request->query->get('queryString', ''))) {
            $query->where('t.firstname LIKE :qs OR t.lastname LIKE :qs OR t.username LIKE :qs OR t.email LIKE :qs OR t.fiscalCode LIKE :qs')
                ->setParameter('qs', $request->query->get('queryString').'%');
        }

        if ($request->query->has('providerId') && '' != $request->query->get('providerId', '') && '-1' != $request->query->get('providerId', '') ) {
	        $query->andWhere('t.provider = :providerId');
			if($em->getConnection()->getDriver() instanceof AbstractMySQLDriver) {
				$query->setParameter('providerId', (new Uuid($request->query->get('providerId')))->toBinary());
	        } else {
		        $query->setParameter('providerId', $request->query->get('providerId'));
	        }
        }

	    if($request->query->get('type') == "trashed") {
		    $query->andWhere('NOT t.trashedDate IS NULL');
	    } else if($request->query->get('type', "") == "") {
		    $query->andWhere('t.trashedDate IS NULL');
	    }

        $pagination = $paginator->paginate(
            $query,
            $request->query->get('page', 1), 25
        );

        return ['pagination' => $pagination,
                     'queryString' => $request->query->get('queryString', ''),
                     'providerId' => $request->query->get('providerId', ''),
                     'type' => $request->query->get('type', ''),
                     'providers' => $providers, ];
    }

    /**
     * Finds and displays a Teacher entity.
     *
     * @Route("/teacher/{id}/show", name="teacher_show")
     * @Template()
     * @IsGranted("ROLE_TEACHER")
     *
     * @param $id
     *
     * @return array
     */
    public function showAction(EntityManagerInterface $em, $id)
    {
        $entity = $em->getRepository('App\Entity\Teacher')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find Teacher entity.');
        }
        $ldapUser = $em->getRepository('App\Entity\LdapUser')->findOneByDistinguishedId($entity->getId());
        $entity->ldapUser = $ldapUser;

        return [
            'entity' => $entity,
        ];
    }

    /**
     * Displays a form to create a new Teacher entity.
     *
     * @Route("/teacher/new", name="teacher_new")
     * @IsGranted("ROLE_ADMIN")
     * @Template("teacher/edit.html.twig")
     *
     * @return array
     */
    public function newAction(Request $request, EntityManagerInterface $em)
    {
        $teacher = new Teacher(null);

        $providerId = $request->get('providerId', '');
        if ('' != $providerId) {
            $provider = $em->getRepository('App\Entity\Provider')->find($providerId);
            if ($provider instanceof Provider) {
                $teacher->setProvider($provider);
            }
        }

        $form = $this->createForm('App\Form\TeacherType', $teacher, [
	        'action' => $this->generateUrl('teacher_create'),
	        'method' => 'POST',
        ]);
        $form->add('submit', SubmitType::class, ['label' => 'Salva Modifiche']);

        return [
            'teacher' => $teacher,
            'main_form' => $form->createView(),
            'isEditing' => false,
            'providerId' => $providerId,
        ];
    }

    /**
     * Creates a new Teacher entity.
     *
     * @Route("/teacher/create", name="teacher_create", methods={"POST"})
     * @IsGranted("ROLE_ADMIN")
     * @Template("teacher/edit.html.twig")
     *
     * @return array|\Symfony\Component\HttpFoundation\RedirectResponse
     *
     * @throws \Exception
     */
    public function createAction(PersonsAndGroups $personsAndGroups, Request $request, EntityManagerInterface $em, TeacherRepositoryAction $repositoryAction)
    {
        $teacher = new Teacher(null);

        $providerId = $request->get('providerId', '');
        if ('' != $providerId) {
            $provider = $em->getRepository('App\Entity\Provider')->find($providerId);
            if ($provider instanceof Provider) {
                $teacher->setProvider($provider);
            }
        }

        $form = $this->createForm('App\Form\TeacherType', $teacher, [
	        'action' => $this->generateUrl('teacher_create'),
	        'method' => 'POST',
        ]);
        $form->add('submit', SubmitType::class, ['label' => 'Salva Modifiche']);
        $form->handleRequest($request);

        if ($form->isValid()) {
            if ($teacher->getProvider() instanceof Provider && $teacher->haveAdditionalProvider($teacher->getProvider())) {
                $teacher->removeAdditionalProvider($teacher->getProvider());
            }

            $em->persist($teacher);
            $em->flush();

            try {
                $repositoryAction->executeAfterCreate($teacher);
            } catch (\Exception $e) {
                $personsAndGroups->addErrorsToForm($e, $form);
            }

            return $this->redirect($this->generateUrl('teacher_list'));
        }

        return [
            'entity' => $teacher,
            'main_form' => $form->createView(),
            'isEditing' => false,
            'providerId' => $providerId,
        ];
    }

    /**
     * Displays a form to edit an existing Teacher entity.
     *
     * @Route("/teacher/{id}/edit", name="teacher_edit")
     * @IsGranted("ROLE_ADMIN")
     * @Template()
     */
    public function editAction($id, EntityManagerInterface $em, Request $request)
    {
        $teacher = $em->getRepository('App\Entity\Teacher')->find($id);

        if (!$teacher instanceof Teacher) {
            throw $this->createNotFoundException('Unable to find Teacher entity.');
        }

        $editForm = $this->createForm('App\Form\TeacherType', $teacher, [
            'action' => $this->generateUrl('teacher_update', ['id' => $id]),
            'method' => 'POST',
        ]);
        $editForm->add('submit', SubmitType::class, ['label' => 'Salva Modifiche']);

        $providerId = '';
        if ($teacher->getProvider() instanceof Provider) {
            $providerId = $teacher->getProvider()->getId();
        }

        return [
            'entity' => $teacher,
            'main_form' => $editForm->createView(),
            'isEditing' => true,
            'providerId' => $providerId,
        ];
    }

    /**
     * Edits an existing Teacher entity.
     *
     * @Route("/teacher/{id}/update", name="teacher_update", methods={"POST"})
     * @IsGranted("ROLE_ADMIN")
     * @Template("teacher/edit.html.twig")
     */
    public function updateAction($id, Request $request, EntityManagerInterface $em, PersonsAndGroups $personAndGroups, TeacherRepositoryAction $repositoryAction)
    {
        $teacher = $em->getRepository('App\Entity\Teacher')->find($id);

        if (!$teacher instanceof Teacher) {
            throw $this->createNotFoundException('Unable to find Teacher entity.');
        }

        $previousTeacherEntity = clone $teacher;

        $editForm = $this->createForm('App\Form\TeacherType', $teacher, [
            'action' => $this->generateUrl('teacher_update', ['id' => $id]),
            'method' => 'POST',
        ]);
        $editForm->add('submit', SubmitType::class, ['label' => 'Salva Modifiche']);

        $editForm->handleRequest($request);

        if ($editForm->isValid()) {
            if ($teacher->getProvider() instanceof Provider && $teacher->haveAdditionalProvider($teacher->getProvider())) {
                $teacher->removeAdditionalProvider($teacher->getProvider());
            }

            $em->persist($teacher);
            $em->flush();

            try {
                $repositoryAction->executeAfterUpdate($teacher, $previousTeacherEntity);
            } catch (\Exception $e) {
                $personAndGroups->addErrorsToForm($e, $editForm);
            }

            if ($editForm->getErrors()->count() < 1) {
                return $this->redirect($this->generateUrl('teacher_list'));
            }
        }

        $providerId = '';
        if ($teacher->getProvider() instanceof Provider) {
            $providerId = $teacher->getProvider()->getId();
        }

        return [
            'entity' => $teacher,
            'main_form' => $editForm->createView(),
            'isEditing' => true,
            'providerId' => $providerId,
        ];
    }

    /**
     * Present the form to delete a Teacher entity.
     *
     * @Route("/teacher/{id}/remove", name="teacher_delete_form", methods={"GET"})
     * @IsGranted("ROLE_ADMIN")
     * @Template("teacher/delete.html.twig")
     */
    public function deleteFormAction($id, Request $request, EntityManagerInterface $em)
    {
        $teacher = $em->getRepository('App\Entity\Teacher')->find($id);

        if (!$teacher) {
            throw $this->createNotFoundException('Unable to find Teacher entity.');
        }

        $form = $this->createDeleteForm($id);

        return ['form' => $form->createView(), 'entity' => $teacher];
    }

    /**
     * Compose delete form.
     *
     * @param $id
     *
     * @return \Symfony\Component\Form\Form|\Symfony\Component\Form\FormInterface
     */
    private function createDeleteForm($id)
    {
        return $this->createFormBuilder()
            ->setAction($this->generateUrl('teacher_delete', ['id' => $id]))
            ->setMethod('DELETE')
            ->add('deleteOnLdap', CheckboxType::class, ['label' => 'Cancellare da Active Directory', 'required' => false])
            ->add('deleteOnGApps', ChoiceType::class, ['expanded' => true, 'label' => 'Cancellare da Google Apps', 'choices' => ['Si' => 1, 'Spostare in Ou Indicata' => 2], 'required' => false, 'placeholder' => 'No'])
            ->add('gAppsOu', TextType::class, ['label' => 'OU Google Apps di destinazione (se specificato lo spostamento)', 'required' => false])
            ->add('submit', SubmitType::class, ['label' => 'Rimuovi', 'attr' => ['class' => 'btn btn-danger']])
            ->getForm();
    }

    /**
     * Deletes a Teacher entity.
     *
     * @Route("/teacher/{id}/delete", name="teacher_delete", methods={"DELETE"})
     * @IsGranted("ROLE_ADMIN")
     */
    public function deleteAction($id, Request $request, EntityManagerInterface $em, TeacherRepositoryAction $repositoryAction)
    {
        $form = $this->createDeleteForm($id);
        $form->handleRequest($request);

        if ($form->isValid()) {
            $teacher = $em->getRepository('App\Entity\Teacher')->find($id);

            if (!$teacher) {
                throw $this->createNotFoundException('Unable to find Teacher entity.');
            }

            try {
                $repositoryAction->executeBeforeRemove($teacher, $form->get('deleteOnLdap')->getData(), $form->get('deleteOnGApps')->getData(), $form->get('gAppsOu')->getData());
            } catch (\Exception $e) {
                foreach (explode(PHP_EOL, $e->getMessage()) as $message) {
                    $form->addError(new FormError($message));
                }
            }

            $em->remove($teacher);
            $em->flush();
        }

        return $this->redirect($this->generateUrl('teacher_list'));
    }

	/**
	 * Trash a Teacher entity.
	 *
	 * @Route("/teacher/{id}/trash", name="teacher_trash")
	 * @IsGranted("ROLE_ADMIN")
	 */
	public function trashAction($id, Request $request, EntityManagerInterface $em, TeacherRepositoryAction $repositoryAction)
	{
		$teacher = $em->getRepository('App\Entity\Teacher')->find($id);

		if (!$teacher) {
			throw $this->createNotFoundException('Unable to find Teacher entity.');
		}

		$previousTeacherEntity = clone $teacher;

		$teacher->setTrashedDate(new \Datetime());

		$em->persist($teacher);
		$em->flush();

		$repositoryAction->executeAfterUpdate($teacher, $previousTeacherEntity);

		return $this->redirect($this->generateUrl('teacher_list'));
	}

	/**
	 * Restore a Teacher entity from trash.
	 *
	 * @Route("/teacher/{id}/restore", name="teacher_restore")
	 * @IsGranted("ROLE_ADMIN")
	 */
	public function restoreAction($id, Request $request, EntityManagerInterface $em, TeacherRepositoryAction $repositoryAction)
	{
		$teacher = $em->getRepository('App\Entity\Teacher')->find($id);

		if (!$teacher) {
			throw $this->createNotFoundException('Unable to find Teacher entity.');
		}

		$previousTeacherEntity = clone $teacher;

		$teacher->setTrashedDate(null);

		$em->persist($teacher);
		$em->flush();

		$repositoryAction->executeAfterUpdate($teacher, $previousTeacherEntity);

		return $this->redirect($this->generateUrl('teacher_list'));
	}

    /**
     * Show the badge of a teacher.
     *
     * @Route("/teacher/{id}/badge", name="teacher_badge")
     * @IsGranted("ROLE_TEACHER")
     */
    public function printTeacherBadgeAction($id, EntityManagerInterface $em)
    {
        $teacher = $em->getRepository('App\Entity\Teacher')->find($id);

        throw $this->createNotFoundException('Unavailable.'); //@todo fix
//        if (!$teacher) {
//            throw $this->createNotFoundException('Unable to find Teacher entity.');
//        }
//
//        return $this->printStudentBadge($teacher);
    }

    /**
     * Ajax call to reset a teacher password.
     *
     * @Route("/teacher/reset_password_ajax", name="teacher_reset_password_ajax")
     * @IsGranted("ROLE_TEACHER")
     */
    public function resetPasswordAjaxAction(PersonsAndGroups $personsAndGroups, EntityManagerInterface $em, Request $request)
    {
        $teacherId = $request->get('id', '');
        $targetService = $request->get('targetService', 'all');

        return $this->resetPassword($personsAndGroups, $em, $teacherId, $targetService, false);
    }

    /**
     * Ajax call to renew a teacher password.
     *
     * @Route("/teacher/renew_password_ajax", name="teacher_renew_password_ajax")
     * @IsGranted("ROLE_ADMIN")
     */
    public function renewPasswordAjaxAction(PersonsAndGroups $personsAndGroups, EntityManagerInterface $em, Request $request)
    {
        $teacherId = $request->get('id', '');
        $targetService = $request->get('targetService', 'all');

        return $this->resetPassword($personsAndGroups, $em, $teacherId, $targetService, true);
    }

    /**
     * Reset or renew a teacher password, anserwing in json.
     *
     * @param $teacherId
     * @param $targetService
     * @param bool $renew
     *
     * @return Response
     */
    protected function resetPassword(PersonsAndGroups $personsAndGroups, EntityManagerInterface $em, $teacherId, $targetService, $renew = false)
    {
        $response = new Response();
        $response->headers->set('Content-Type', 'application/json');

        $teacher = $em->getRepository('App\Entity\Teacher')->find($teacherId);
        if (!$teacher instanceof Teacher) {
            $response->setContent(json_encode(['success' => false, 'error' => 'Utente specificato non valido']));

            return $response;
        }

        if (0 == strlen(trim($teacher->getStartingPassword())) && !$renew) {
            $response->setContent(json_encode(['success' => false, 'error' => 'Password di default non generata']));

            return $response;
        }

        $newPassword = '';
        if ($renew) {
	        $newPassword = $personsAndGroups->generateTeacherPasswordForProvider($teacher->getProvider());
	        if (0 == strlen(trim($newPassword))) {
		        $response->setContent(json_encode(['success' => false, 'error' => 'Errore durante la generazione della password']));
		        return $response;
	        }

            $teacher->setStartingPassword($newPassword);
        }

        if ('ad' == $targetService || 'all' == $targetService) {
            try {
                $personsAndGroups->resetPersonLdapUserPassword($teacher, true);
            } catch (\Exception $e) {
                $response->setContent(json_encode(['success' => false, 'error' => 'Errore durante il tentativo di cambio password di rete']));

                return $response;
            }
        }

        if ('email' == $targetService || 'all' == $targetService) {
            try {
                $personsAndGroups->resetPersonGoogleAppsUserPassword($teacher, true);
            } catch (\Exception $e) {
                $response->setContent(json_encode(['success' => false, 'error' => "Errore durante il tentativo di cambio password dell'account Google"]));

                return $response;
            }
        }

        $response->setContent(json_encode(['success' => true, 'newPassword' => $newPassword]));

        return $response;
    }

    /**
     * Send the teacher credential to the specified email.
     *
     * @Route("/teacher/{id}/send_credential_to_email", name="teacher_send_credential_to_email")
     * @IsGranted("ROLE_TEACHER")
     */
    public function sendCredentialToEmailAction(Request $request, MailerInterface $mailer, EntityManagerInterface $em, $id)
    {
        $response = new Response();
        $response->headers->set('Content-Type', 'application/json');

        $teacher = $em->getRepository('App\Entity\Teacher')->find($id);
        if (!$teacher instanceof Teacher) {
            $response->setContent(json_encode(['success' => false, 'error' => 'Docente non valido']));

            return $response;
        }

        $email = trim($request->get('emailTo', ''));
        if (!filter_var($email, FILTER_VALIDATE_EMAIL)) {
            $response->setContent(json_encode(['success' => false, 'error' => 'Email specificata non valida']));

            return $response;
        }

        $body = <<<MAIL
Riepilogo credenziali per il docente {$teacher->getLastname()} {$teacher->getFirstname()}:

Username: {$teacher->getUsername()}
E-Mail: {$teacher->getEmail()}
Password: {$teacher->getStartingPassword()}
MAIL;


        $email = (new Email())
            ->from('no-reply@igroove.network')
            ->to($email)
            ->subject("Credenziali per il docente {$teacher->getLastname()} {$teacher->getFirstname()}")
            ->text($body);


        try {
            $mailer->send($email);
            $response->setContent(json_encode(['success' => true]));
        } catch (TransportExceptionInterface $e) {
            $response->setContent(json_encode(['success' => false, 'error' => "Errore durante l'invio dell'email"]));
        }

        return $response;
    }
}
