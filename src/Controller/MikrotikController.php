<?php

namespace App\Controller;

use App\Entity\Mikrotik;
use App\Manager\MikrotikManager;
use App\Message\MikrotikMessage;
use Doctrine\ORM\EntityManagerInterface;
use Knp\Component\Pager\PaginatorInterface;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\IsGranted;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Messenger\Envelope;
use Symfony\Component\Messenger\MessageBusInterface;

/**
 * Mikrotik controller.
 *
 * @Route("/config/mikrotik")
 */
class MikrotikController extends AbstractController
{
    /**
     * Refresh all Mikrotik setups.
     *
     * @Route("/refresh", name="mikrotik_refresh")
     * @IsGranted("ROLE_NETWORK_OPERATOR")
     */
    public function refreshAction(MessageBusInterface $messageBus)
    {
        $msg = new MikrotikMessage();
        $msg->setCommand('addMacToHospot');
        $messageBus->dispatch(new Envelope($msg));
        $msg = new MikrotikMessage();
        $msg->setCommand('addIpReservation');
        $messageBus->dispatch(new Envelope($msg));
        $msg = new MikrotikMessage();
        $msg->setCommand('addIpList');
        $messageBus->dispatch(new Envelope($msg));

        return new JsonResponse([]);
    }

    /**
     * Lists all MikrotikList entities.
     *
     * @Route("/", name="mikrotik")
     * @IsGranted("ROLE_ADMIN")
     */
    public function indexAction(PaginatorInterface $paginator, Request $request, EntityManagerInterface $em)
    {
        $queryString = $request->get('queryString', false);
        $q = '%'.$queryString.'%';
        if ($queryString) {
            $query = $em->createQuery(
                "SELECT l FROM App\Entity\Mikrotik l WHERE l.ip  LIKE :q ORDER BY l.ip"
            )
                ->setParameter('q', $q);
        } else {
            $query = $em->createQuery("SELECT l FROM App\Entity\Mikrotik l  ORDER BY l.ip");
        }

        $pagination = $paginator->paginate(
            $query,
            $request->query->get('page', 1), 25
        );

        return $this->render(
            'mikrotik/index.html.twig',
            [
                'pagination' => $pagination,
            ]
        );
    }

    /**
     * Displays a form to create a new Guest entity.
     *
     * @Route("/new", name="config_mikrotik_new")
     * @IsGranted("ROLE_ADMIN")
     */
    public function newAction()
    {
        $entity = new Mikrotik();
        $form = $this->createForm('App\Form\MikrotikType', $entity);

        return $this->render(
            'mikrotik/edit.html.twig',
            [
                'entity' => $entity,
                'main_form' => $form->createView(),
                'isEditing' => false
            ]
        );
    }

    /**
     * Creates a new Guest entity.
     *
     * @Route("/create", name="config_mikrotik_create", methods={"POST"})
     * @IsGranted("ROLE_ADMIN")
     */
    public function createAction(Request $request, EntityManagerInterface $em)
    {
        $entity = new Mikrotik();
        $form = $this->createForm('App\Form\MikrotikType', $entity);
        $form->handleRequest($request);
        if ($form->isValid()) {
            $em->persist($entity);
            $em->flush();

            return $this->redirect($this->generateUrl('mikrotik', []));
        }

        return $this->render(
            'mikrotik/edit.html.twig',
            [
                'entity' => $entity,
                'main_form' => $form->createView(),
                'isEditing' => false
            ]
        );
    }

    /**
     * Displays a form to edit an existing Mikrotik entity.
     *
     * @Route("/{id}/edit", name="config_mikrotik_edit")
     * @IsGranted("ROLE_ADMIN")
     */
    public function editAction(request $request, Mikrotik $mikrotik, EntityManagerInterface $em)
    {
        if (!$mikrotik) {
            throw $this->createNotFoundException('Unable to find Mikrotik entity.');
        }

        $editForm = $this->createForm('App\Form\MikrotikType', $mikrotik);

        $editForm->handleRequest($request);

        if ($editForm->isSubmitted() && $editForm->isValid()) {
	        $em->flush();
            return $this->redirect($this->generateUrl('mikrotik', []));
        }

        return $this->render(
            'mikrotik/edit.html.twig',
            [
                'entity' => $mikrotik,
                'main_form' => $editForm->createView(),
                'isEditing' => true
            ]
        );
    }

    /**
     * Deletes a Mikrotik entity.
     *
     * @Route("/{id}/delete", name="config_mikrotik_delete")
     * @IsGranted("ROLE_ADMIN")
     */
    public function deleteAction(Request $request, Mikrotik $mikrotik, EntityManagerInterface $em)
    {
        if (!$mikrotik) {
            throw $this->createNotFoundException('Unable to find Mikrotik entity.');
        }

        $em->remove($mikrotik);
        $em->flush();

        return $this->redirect($this->generateUrl('mikrotik'));
    }

    /**
     * Get all dhcp servers name from the Mikrotik.
     *
     * @Route("/{id}/getDhcpServersName", name="config_mikrotik_get_dhcp_servers_name")
     * @IsGranted("ROLE_ADMIN")
     */
    public function getDhcpServerName(MikrotikManager $mikrotikManager, Mikrotik $mikrotik)
    {
        if (!$mikrotik || '' == $mikrotik->getIp()) {
            return new Response(json_encode(['error' => 'Unable to find the mikrotik you specified']), 404);
        }

        return new Response(json_encode(['names' => $mikrotikManager->getDhcpServersName($mikrotik->getIp())]));
    }
}
