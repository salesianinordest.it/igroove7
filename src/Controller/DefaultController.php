<?php

namespace App\Controller;

use App\Entity\Student;
use App\Manager\ConfigurationManager;
use App\Manager\VersionManager;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;

class DefaultController extends AbstractController
{
    /**
     * @Route("/", name="homepage")
     */
    public function homepage(VersionManager $versionManager, ConfigurationManager $configurationManager, EntityManagerInterface $entityManager)
    {
        return $this->render(
            'default/homepage.html.twig',
            [
                'version' => $versionManager->getVersions(),
                'menu_render' => $configurationManager->getMenuConfiguration($this->currentUserIsStudent($entityManager)),
            ]
        );
    }

    protected function currentUserIsStudent(EntityManagerInterface $entityManager)
    {
        static $isStudent;

        if (!isset($isStudent)) {
            $isStudent = false;
            $isStudent = $entityManager->getRepository(Student::class)->userIsStudent($this->getUser()->getUserIdentifier());
        }

        return $isStudent;
    }

    /**
     * @Route("/sidebar", name="sidebar")
     */
    public function sidebar(VersionManager $versionManager, ConfigurationManager $configurationManager, EntityManagerInterface $entityManager, Request $request)
    {
        return $this->render(
            'default/sidebar.html.twig',
            [
                'version' => $versionManager->getVersions(),
                'menu_render' => $configurationManager->getMenuConfiguration($this->currentUserIsStudent($entityManager)),
                'fromUrl' => $request->get('fromUrl', null),
            ]
        );
    }


    /**
     * @Route("/info", name="info")
     */
    public function info(VersionManager $versionManager,)
    {
        $versionManager->checkNewVersion();
        return $this->render(
            'default/info.html.twig',
            [
                'version' => $versionManager->getVersions(),
            ]
        );
    }
}
