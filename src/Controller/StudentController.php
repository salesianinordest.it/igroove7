<?php

namespace App\Controller;

use App\Entity\Group;
use App\Entity\Provider;
use App\Entity\Student;
use App\Manager\ConfigurationManager;
use App\Manager\PersonsAndGroups;
use App\RepositoryAction\StudentRepositoryAction;
use Doctrine\DBAL\Driver\AbstractMySQLDriver;
use Doctrine\ORM\EntityManagerInterface;
use Dompdf\Dompdf;
use Knp\Component\Pager\PaginatorInterface;
use PhpOffice\PhpSpreadsheet\Spreadsheet;
use PhpOffice\PhpSpreadsheet\Writer\Xlsx;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\IsGranted;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use Symfony\Component\HttpFoundation\ResponseHeaderBag;
use Symfony\Component\Routing\Annotation\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Form\Extension\Core\Type\CheckboxType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormError;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Mailer\Exception\TransportExceptionInterface;
use Symfony\Component\Mailer\MailerInterface;
use Symfony\Component\Mime\Email;
use Symfony\Component\Uid\Uuid;


/**
 * Student controller.
 */
class StudentController extends AbstractController
{
    /**
     * Lists all Student entities.
     *
     * @Route("/student", name="student_list")
     * @IsGranted("ROLE_ACCOUNTS_MANAGER")
     * @Template()
     */
    public function indexAction(PaginatorInterface $paginator, Request $request, EntityManagerInterface $em, ConfigurationManager $configurationManager)
    {
        $query = $this->prepareListQuery($request, $em);

	    $active_directory_generated_group_prefix = $configurationManager->getActiveDirectoryGeneratedGroupPrefix();
		$students = $query->getQuery()->execute();
		$ldapUserRepository = $em->getRepository('App\Entity\LdapUser');
		foreach ($students as $student) {
			$student->state = $ldapUserRepository->getInternetStatus($student->getUsername(), $active_directory_generated_group_prefix);
		}

        $pagination = $paginator->paginate(
            $query,
            $request->query->get('page', 1), 25
        );


	    $providers = $em->getRepository('App\Entity\Provider')->findBy([], ['name' => 'ASC']);
	    $groups = $em->getRepository('App\Entity\Group')->findBy([], ['name' => 'ASC']);


        return ['pagination' => $pagination,
            'queryString' => $request->query->get('queryString', ''),
            'providerId' => $request->query->get('providerId', ''),
            'type' => $request->query->get('type', ''),
            'providers' => $providers,
            'groups' => $groups,
            'selectedGroupIds' => is_array($request->query->get('groupIds')) ? $request->query->get('groupIds') : [],
            'timeToSet' => new \DateTime('+1 hour'),
	        'userCanEnablePersonalDevice' => true,
            'wifiSsid' => $configurationManager->getWirelessSsid(),
            'activatedUser' => ''
	    ];
    }

	/**
	 * Export all Student entities.
	 *
	 * @Route("/student/export", name="student_export")
	 * @IsGranted("ROLE_ACCOUNTS_MANAGER")
	 */
	public function exportAction(Request $request, EntityManagerInterface $em)
	{
		$query = $this->prepareListQuery($request, $em);
		$students = $query->getQuery()->execute();
		$data = [['Codice Fiscale', 'Cognome', 'Nome', 'Provider', 'Classi', 'Gruppi', 'Email', 'Username', 'Password iniziale']];
		foreach ($students as $student) {
			/* @var Student $student */
			$data[] = [
				$student->getFiscalCode(),
				$student->getLastname(),
				$student->getFirstname(),
				$student->getProvider() ? $student->getProvider()->getName() : '',
				join(", ", array_map(fn(Group $group) => $group->getName(), $student->getNotManuallyManagedGroups()->toArray())),
				join(", ", array_map(fn(Group $group) => $group->getName(), $student->getManuallyManagedGroups()->toArray())),
				$student->getEmail(),
				$student->getUsername(),
				$student->getStartingPassword(),
			];
		}

		$spreadsheet = new Spreadsheet();
		$spreadsheet->getActiveSheet()->fromArray($data);
		$writer = new Xlsx($spreadsheet);

		$fileName = 'elenco_studenti_igroove_'.date('Y_m_d_H_i').'.xlsx';
		$temp_file = tempnam(sys_get_temp_dir(), $fileName);
		$writer->save($temp_file);
		return $this->file($temp_file, $fileName, ResponseHeaderBag::DISPOSITION_INLINE);
	}

	protected function prepareListQuery(Request $request, EntityManagerInterface $em)
	{
		$query = $em->getRepository('App\Entity\Student')->createQueryBuilder('s');
		$query->orderBy('s.lastname', 'ASC');

		if ($request->query->has('queryString') && '' != trim($request->query->get('queryString', ''))) {
			$query->where('s.firstname LIKE :qs OR s.lastname LIKE :qs OR s.username LIKE :qs OR s.email LIKE :qs')
			      ->setParameter('qs', $request->query->get('queryString').'%');
		}

		if ($request->query->has('providerId') && '' != $request->query->get('providerId') && '' != $request->query->get('providerId', '')) {
			$query->andWhere('s.provider = :providerId');
			if($em->getConnection()->getDriver() instanceof AbstractMySQLDriver) {
				$query->setParameter('providerId', (new Uuid($request->query->get('providerId')))->toBinary());
			} else {
				$query->setParameter('providerId', $request->query->get('providerId'));
			}
		}

		if($request->query->has('groupIds') && is_array($request->query->get('groupIds'))) {
			$query->join('s.memberOf', 'g');
			$query->andWhere('g.id IN (:groupIds)');
			if($em->getConnection()->getDriver() instanceof AbstractMySQLDriver && is_array($request->query->get('groupIds'))) {
				$query->setParameter('groupIds', array_map(fn($group) => (new Uuid($group))->toBinary(), $request->query->get('groupIds')));
			} else {
				$query->setParameter('groupIds', $request->query->get('groupIds'));
			}
		}

		if($request->query->get('type') == "trashed") {
			$query->andWhere('NOT s.trashedDate IS NULL');
		} else if($request->query->get('type', "") == "") {
			$query->andWhere('s.trashedDate IS NULL');
		}

		return $query;
	}

    /**
     * Finds and displays a Student entity.
     *
     * @Route("/student/{id}/show", name="student_show")
     * @Template()
     * @IsGranted("ROLE_TEACHER")
     *
     * @param $id
     *
     * @return array
     */
    public function showAction(ConfigurationManager $configurationManager, EntityManagerInterface $em, $id)
    {
        if (!$configurationManager->getTeacherCanSeeList() && !$this->isGranted('ROLE_ADMIN')) {
            throw $this->createAccessDeniedException();
        }

        $entity = $em->getRepository('App\Entity\Student')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find Student entity.');
        }

        $ldapUser = $em->getRepository('App\Entity\LdapUser')->findOneByDistinguishedId($entity->getId());
        $entity->ldapUser = $ldapUser;
//        $deleteForm = $this->createDeleteForm($id);

        return [
            'entity' => $entity,
//            'delete_form' => $deleteForm->createView()
        ];
    }

    /**
     * Displays a form to create a new Student entity.
     *
     * @Route("/student/new", name="student_new")
     * @IsGranted("ROLE_ACCOUNTS_MANAGER")
     * @Template("student/edit.html.twig")
     *
     * @return array
     */
    public function newAction(Request $request, EntityManagerInterface $em)
    {
        $student = new Student(null);

        $providerId = $request->get('providerId', '');
        if ('' != $providerId) {
            $provider = $em->getRepository('App\Entity\Provider')->find($providerId);
            if ($provider instanceof Provider) {
                $student->setProvider($provider);
            }
        }

        $groupId = $request->get('groupId', '');
        if ('' != $groupId) {
            $group = $em->getRepository('App\Entity\Group')->find($groupId);
            if ($group instanceof Group) {
                $student->addMemberOf($group);
            }
        }

	    $manuallyManagedGroups = $em->getRepository('App\Entity\Group')->findBy(['manageManually' => true], ['name' => 'ASC']);
	    $providerIds = array_map(fn(Provider $provider) => $provider->getId(), $student->getAdditionalProviders()->toArray());
	    if($providerId != "") {
		    $providerIds[] = $providerId;
	    }

	    if(!empty($providerIds)) {
		    $providerManuallyManagedGroups = $em->getRepository('App\Entity\Group')->findBy(['provider' => $providerIds, 'manageManually' => true], ['name' => 'ASC']);
	    }

        $form = $this->createForm('App\Form\StudentType', $student, [
	        'action' => $this->generateUrl('student_create'),
	        'method' => 'POST',
        ]);
        $form->add('submit', SubmitType::class, ['label' => 'Salva Modifiche']);

        return [
            'student' => $student,
            'main_form' => $form->createView(),
            'isEditing' => false,
            'groupId' => $groupId,
            'providerId' => $providerId,
            'manuallyManagedGroupIds' => array_map(fn(Group $group) => (string)$group->getId(), $manuallyManagedGroups),
            'providerManuallyManagedGroupIds' => array_map(fn(Group $group) => (string)$group->getId(), $providerManuallyManagedGroups ?? $manuallyManagedGroups),
        ];
    }

    /**
     * Creates a new Student entity.
     *
     * @Route("/student/create", name="student_create", methods={"POST"})
     * @IsGranted("ROLE_ACCOUNTS_MANAGER")
     * @Template("student/edit.html.twig")
     *
     * @return array|\Symfony\Component\HttpFoundation\RedirectResponse
     */
    public function createAction(Request $request, EntityManagerInterface $em, StudentRepositoryAction $repositoryAction)
    {
        $student = new Student(null);

        $providerId = $request->get('providerId', '');
        if ('' != $providerId) {
            $provider = $em->getRepository('App\Entity\Provider')->find($providerId);
            if ($provider instanceof Provider) {
                $student->setProvider($provider);
            }
        }

        $groupId = $request->get('groupId', '');
        if ('' != $groupId) {
            $group = $em->getRepository('App\Entity\Group')->find($groupId);
            if ($group instanceof Group) {
                $student->addMemberOf($group);
            }
        }

        $form = $this->createForm('App\Form\StudentType', $student, [
		    'action' => $this->generateUrl('student_create'),
		    'method' => 'POST',
	    ]);
        $form->add('submit', SubmitType::class, ['label' => 'Salva Modifiche']);
        $form->handleRequest($request);

        if ($form->isValid()) {
            if ($student->getProvider() instanceof Provider && $student->haveAdditionalProvider($student->getProvider())) {
                $student->removeAdditionalProvider($student->getProvider());
            }

            $em->persist($student);
            $em->flush();

            try {
                $repositoryAction->executeAfterCreate($student);
            } catch (\Exception $e) {
                foreach (explode(PHP_EOL, $e->getMessage()) as $message) {
                    $form->addError(new FormError($message));
                }
            }

	        if ($form->getErrors()->count() < 1) {
		        if ('' != $groupId) {
			        return $this->redirect($this->generateUrl('group_show', ['id' => $groupId]));
		        } else {
			        return $this->redirect($this->generateUrl('student_list'));
		        }
	        }
        }

	    $manuallyManagedGroups = $em->getRepository('App\Entity\Group')->findBy(['manageManually' => true, 'provider' => $student->getProvider()->getId()], ['name' => 'ASC']);
	    $providerIds = array_map(fn(Provider $provider) => $provider->getId(), $student->getAdditionalProviders()->toArray());
		if($providerId != "") {
			$providerIds[] = $providerId;
		}

	    if(!empty($providerIds)) {
		    $providerManuallyManagedGroups = $em->getRepository('App\Entity\Group')->findBy(['provider' => $providerIds, 'manageManually' => true], ['name' => 'ASC']);
	    }

        return [
            'entity' => $student,
            'main_form' => $form->createView(),
            'isEditing' => false,
            'groupId' => $groupId,
            'providerId' => $providerId,
            'manuallyManagedGroupIds' => array_map(fn(Group $group) => (string)$group->getId(), $manuallyManagedGroups),
            'providerManuallyManagedGroupIds' => array_map(fn(Group $group) => (string)$group->getId(), $providerManuallyManagedGroups ?? $manuallyManagedGroups),
        ];
    }

    /**
     * Displays a form to edit an existing Student entity.
     *
     * @Route("/student/{id}/edit", name="student_edit")
     * @IsGranted("ROLE_ACCOUNTS_MANAGER")
     * @Template()
     */
    public function editAction($id, Request $request, EntityManagerInterface $em)
    {
        $student = $em->getRepository('App\Entity\Student')->find($id);

        if (!$student instanceof Student) {
            throw $this->createNotFoundException('Unable to find Student entity.');
        }

        $groupId = $request->get('groupId', '');
        $editForm = $this->createForm('App\Form\StudentType', $student, [
            'action' => $this->generateUrl('student_update', ['id' => $id, 'groupId' => $groupId]),
            'method' => 'POST',
        ]);
        $editForm->add('submit', SubmitType::class, ['label' => 'Salva Modifiche']);
        $deleteForm = $this->createDeleteForm($id, $groupId);

	    $manuallyManagedGroups = $em->getRepository('App\Entity\Group')->findBy(['manageManually' => true], ['name' => 'ASC']);
	    $providerIds = array_map(fn(Provider $provider) => $provider->getId(), $student->getAdditionalProviders()->toArray());
	    $providerId = '';
	    if ($student->getProvider() instanceof Provider) {
		    $providerIds[] = $providerId = $student->getProvider()->getId();
	    }

	    if(!empty($providerIds)) {
		    $providerManuallyManagedGroups = $em->getRepository('App\Entity\Group')->findBy(['provider' => $providerIds, 'manageManually' => true], ['name' => 'ASC']);
	    }

        return [
            'entity' => $student,
            'main_form' => $editForm->createView(),
            'isEditing' => true,
            'delete_form' => $deleteForm->createView(),
            'groupId' => $groupId,
            'manuallyManagedGroupIds' => array_map(fn(Group $group) => (string)$group->getId(), $manuallyManagedGroups),
            'providerManuallyManagedGroupIds' => array_map(fn(Group $group) => (string)$group->getId(), $providerManuallyManagedGroups ?? $manuallyManagedGroups),
            'providerId' => $providerId,
        ];
    }

    /**
     * Edits an existing Student entity.
     *
     * @Route("/student/{id}/update", name="student_update", methods={"POST"})
     * @IsGranted("ROLE_ACCOUNTS_MANAGER")
     * @Template("/student/edit.html.twig")
     */
    public function updateAction($id, Request $request, EntityManagerInterface $em, StudentRepositoryAction $repositoryAction)
    {
        $student = $em->getRepository('App\Entity\Student')->find($id);

        if (!$student instanceof Student) {
            throw $this->createNotFoundException('Unable to find Student entity.');
        }

        $groupId = $request->get('groupId', '');
        $previousStudentEntity = clone $student;

        $editForm = $this->createForm('App\Form\StudentType', $student, [
            'action' => $this->generateUrl('student_update', ['id' => $id, 'groupId' => $groupId]),
            'method' => 'POST',
        ]);
        $editForm->add('submit', SubmitType::class, ['label' => 'Salva Modifiche']);
        $deleteForm = $this->createDeleteForm($id, $groupId);

        $editForm->handleRequest($request);

        if ($editForm->isValid()) {
            if ($student->getProvider() instanceof Provider && $student->haveAdditionalProvider($student->getProvider())) {
                $student->removeAdditionalProvider($student->getProvider());
            }

            $em->persist($student);
            $em->flush();

            try {
                $repositoryAction->executeAfterUpdate($student, $previousStudentEntity);
            } catch (\Exception $e) {
                foreach (explode(PHP_EOL, $e->getMessage()) as $message) {
                    $editForm->addError(new FormError($message));
                }
            }

            if ($editForm->getErrors()->count() < 1) {
                if ('' != $groupId) {
                    return $this->redirect($this->generateUrl('group_show', ['id' => $request->get('groupId')]));
                } else {
                    return $this->redirect($this->generateUrl('student_list'));
                }
            }
        }

	    $manuallyManagedGroups = $em->getRepository('App\Entity\Group')->findBy(['manageManually' => true], ['name' => 'ASC']);
	    $providerIds = array_map(fn(Provider $provider) => $provider->getId(), $student->getAdditionalProviders()->toArray());
        $providerId = '';
        if ($student->getProvider() instanceof Provider) {
	        $providerIds[] = $providerId = $student->getProvider()->getId();
        }

		if(!empty($providerIds)) {
			$providerManuallyManagedGroups = $em->getRepository('App\Entity\Group')->findBy(['provider' => $providerIds, 'manageManually' => true], ['name' => 'ASC']);
		}

        return [
            'entity' => $student,
            'main_form' => $editForm->createView(),
            'isEditing' => true,
            'delete_form' => $deleteForm->createView(),
            'groupId' => $groupId,
            'manuallyManagedGroupIds' => array_map(fn(Group $group) => (string)$group->getId(), $manuallyManagedGroups),
            'providerManuallyManagedGroupIds' => array_map(fn(Group $group) => (string)$group->getId(), $providerManuallyManagedGroups ?? $manuallyManagedGroups),
            'providerId' => $providerId,
        ];
    }

    /**
     * Present the form to delete a Student entity.
     *
     * @Route("/student/{id}/remove", name="student_delete_form", methods={"GET"})
     * @IsGranted("ROLE_ADMIN")
     * @Template("/student/delete.html.twig")
     */
    public function deleteFormAction($id, Request $request, EntityManagerInterface $em)
    {
        $student = $em->getRepository('App\Entity\Student')->find($id);

        if (!$student) {
            throw $this->createNotFoundException('Unable to find Student entity.');
        }

        $groupId = $request->get('groupId', '');
        $form = $this->createDeleteForm($id, $groupId);

        return ['form' => $form->createView(), 'entity' => $student, 'groupId' => $groupId];
    }

    /**
     * Compose delete form.
     *
     * @param $id
     *
     * @return \Symfony\Component\Form\Form|\Symfony\Component\Form\FormInterface
     */
    private function createDeleteForm($id, $groupId = '')
    {
        return $this->createFormBuilder()
            ->setAction($this->generateUrl('student_delete', ['id' => $id, 'groupId' => $groupId]))
            ->setMethod('DELETE')
            ->add('deleteOnLdap', CheckboxType::class, ['label' => 'Cancellare da Active Directory', 'required' => false])
            ->add('deleteOnGApps', ChoiceType::class, ['expanded' => true, 'label' => 'Cancellare da Google Apps', 'choices' => ['Si' => 1, 'Spostare in Ou Indicata' => 2], 'required' => false, 'placeholder' => 'No'])
            ->add('gAppsOu', TextType::class, ['label' => 'OU Google Apps di destinazione (se specificato lo spostamento)', 'required' => false])
            ->add('submit', SubmitType::class, ['label' => 'Rimuovi', 'attr' => ['class' => 'btn btn-danger']])
            ->getForm();
    }

    /**
     * Deletes a Student entity.
     *
     * @Route("/student/{id}/delete", name="student_delete", methods={"DELETE"})
     * @IsGranted("ROLE_ADMIN")
     */
    public function deleteAction($id, Request $request, EntityManagerInterface $em, StudentRepositoryAction $repositoryAction)
    {
        $groupId = $request->get('groupId', '');
        $form = $this->createDeleteForm($id, $groupId);
        $form->handleRequest($request);

        if ($form->isValid()) {
            $student = $em->getRepository('App\Entity\Student')->find($id);

            if (!$student) {
                throw $this->createNotFoundException('Unable to find Student entity.');
            }

            try {
                $repositoryAction->executeBeforeRemove($student, $form->get('deleteOnLdap')->getData(), $form->get('deleteOnGApps')->getData(), $form->get('gAppsOu')->getData());
            } catch (\Exception $e) {
                foreach (explode(PHP_EOL, $e->getMessage()) as $message) {
                    $form->addError(new FormError($message));
                }
            }

            $em->remove($student);
            $em->flush();
        }

        if ('' != $groupId) {
            return $this->redirect($this->generateUrl('group_show', ['id' => $request->get('groupId')]));
        } else {
            return $this->redirect($this->generateUrl('student_list'));
        }
    }

	/**
	 * Trash a Student entity.
	 *
	 * @Route("/student/{id}/trash", name="student_trash")
	 * @IsGranted("ROLE_ACCOUNTS_MANAGER")
	 */
	public function trashAction($id, Request $request, EntityManagerInterface $em, StudentRepositoryAction $repositoryAction)
	{
		$student = $em->getRepository('App\Entity\Student')->find($id);

		if (!$student) {
			throw $this->createNotFoundException('Unable to find Student entity.');
		}

		$previousStudentEntity = clone $student;

		$student->setTrashedDate(new \Datetime());

		$em->persist($student);
		$em->flush();

		$repositoryAction->executeAfterUpdate($student, $previousStudentEntity);

		$groupId = $request->get('groupId', '');
		if ('' != $groupId) {
			return $this->redirect($this->generateUrl('group_show', ['id' => $request->get('groupId')]));
		} else {
			return $this->redirect($this->generateUrl('student_list'));
		}
	}

	/**
	 * Restore a Student entity from trash.
	 *
	 * @Route("/student/{id}/restore", name="student_restore")
	 * @IsGranted("ROLE_ACCOUNTS_MANAGER")
	 */
	public function restoreAction($id, Request $request, EntityManagerInterface $em, StudentRepositoryAction $repositoryAction)
	{
		$student = $em->getRepository('App\Entity\Student')->find($id);

		if (!$student) {
			throw $this->createNotFoundException('Unable to find Student entity.');
		}

		$previousStudentEntity = clone $student;

		$student->setTrashedDate(null);

		$em->persist($student);
		$em->flush();

		$repositoryAction->executeAfterUpdate($student, $previousStudentEntity);

		$groupId = $request->get('groupId', '');
		if ('' != $groupId) {
			return $this->redirect($this->generateUrl('group_show', ['id' => $request->get('groupId')]));
		} else {
			return $this->redirect($this->generateUrl('student_list'));
		}
	}

    /**
     * Show the badge of a student.
     *
     * @Route("/student/{id}/badge", name="student_badge")
     * @IsGranted("ROLE_TEACHER")
     */
    public function printStudentBadgeAction(ConfigurationManager $configurationManager, Request $request, EntityManagerInterface $em, $id)
    {
        if (!$configurationManager->getTeacherCanSeeList() && !$this->isGranted('ROLE_ADMIN')) {
            throw $this->createAccessDeniedException();
        }

        $student = $em->getRepository('App\Entity\Student')->find($id);

        if (!$student) {
            throw $this->createNotFoundException('Unable to find Student entity.');
        }
        $ldapUser = $em->getRepository('App\Entity\LdapUser')->findOneByUsername($student->getUsername());
        $student->ldapUser = $ldapUser;

        return $this->printStudentBadge($student, $configurationManager, $request);
    }

    /**
     * Show current user badge.
     *
     * @Route("/student/printMyBadge", name="printMyBadge")
     */
    public function printMyBadgeAction(Request $request, ConfigurationManager $configurationManager, EntityManagerInterface $em)
    {
        $username = $this->getUser()->getUserIdentifier();
        $student = $em->getRepository('App\Entity\Student')->findOneByUsername($username);

        if (!$student instanceof Student) {
            throw $this->createNotFoundException("L'utenza corrente non sembra essere di uno studente");
        }
        $ldapUser = $em->getRepository('App\Entity\LdapUser')->findOneByDistinguishedId($student->getId());
        $student->ldapUser = $ldapUser;

        return $this->printStudentBadge($student, $configurationManager, $request);
    }

    /**
     * Print the student badge in html or pdf format.
     *
     * @return Response
     *
     * @throws \Exception
     */
    protected function printStudentBadge(Student $student, ConfigurationManager $configurationManager, Request $request)
    {
        $format = $request->get('_format', 'pdf');
        $configurationAD = $configurationManager->getActiveDirectoryConfiguration();

        $pdfHeader = $pdfFooter = $pdfBody = '';
        $provider = $student->getProvider();
        if ($provider instanceof Provider) {
            if ('' != $provider->getPdfHeader()) {
                $pdfHeader = $provider->getPdfHeader();
            }

            if ('' != $provider->getPdfFooter()) {
                $pdfFooter = $provider->getPdfFooter();
            }

            $studentGroups = [];
            foreach ($student->getMemberOf() as $group) {
                $studentGroups[] = $group->getName();
            }

            if ('' != $provider->getPdfStudentBadge()) {
                $pdfBody = $provider->getPdfStudentBadge();
                $pdfBody = str_replace([
                    '{studentFirstName}',
                    '{studentLastName}',
                    '{studentUsername}',
                    '{studentEmail}',
                    '{studentStartingPassword}',
                    '{studentFiscalCode}',
                    '{studentGroups}',
                    '{studentHomeFolder}',
                ], [
                    $student->getFirstname(),
                    $student->getLastname(),
                    $student->getUsername(),
                    $student->ldapUser->getEmail(),
                    $student->getStartingPassword(),
                    $student->getFiscalCode(),
                    implode(' ', $studentGroups),
                    '', // $configurationAD
                ], $pdfBody);
            }
        }

        $out = $this->render(sprintf('student/badge.%s.twig', $format), [
            'student' => $student,
            'pdfHeader' => $pdfHeader,
            'pdfFooter' => $pdfFooter,
            'pdfBody' => $pdfBody,
            'headerHeight' => $provider->getPdfHeaderHeight() > 0 ? $provider->getPdfHeaderHeight() : ('' != $pdfHeader ? 20 : 0),
            'footerHeight' => $provider->getPdfFooterHeight() > 0 ? $provider->getPdfFooterHeight() : ('' != $pdfFooter ? 20 : 0),
            'configurationAD' => $configurationAD,
        ]);

        if ('pdf' == $format) {
            $dompdf = new Dompdf();
            $dompdf->loadHtml($out->getContent());
            $dompdf->getOptions()->set('isRemoteEnabled', true);
            if ('' != $provider->getPdfStudentBadgePageSize()) {
                $dompdf->setPaper($provider->getPdfStudentBadgePageSize(), $provider->getPdfStudentBadgePageLandscape() ? 'landscape' : 'portrait');
            }

            $dompdf->render();
            $out->setContent($dompdf->output());
            $out->headers->set('Content-Type', 'application/pdf');
        }

        return $out;
    }

    /**
     * Ajax call to reset a student password.
     *
     * @Route("/student/reset_password_ajax", name="student_reset_password_ajax")
     * @IsGranted("ROLE_TEACHER")
     */
    public function resetPasswordAjaxAction(ConfigurationManager $configurationManager, PersonsAndGroups $personsAndGroups, Request $request, EntityManagerInterface $em)
    {
        $studentId = $request->get('id', '');
        $targetService = $request->get('targetService', 'all');

        return $this->resetPassword($configurationManager, $personsAndGroups, $em, $studentId, $targetService, false);
    }

    /**
     * Ajax call to renew a student password.
     *
     * @Route("/student/renew_password_ajax", name="student_renew_password_ajax")
     * @Security("is_granted('ROLE_ADMIN_TEACHER') or is_granted('ROLE_ACCOUNTS_MANAGER')")
     */
    public function renewPasswordAjaxAction(ConfigurationManager $configurationManager, PersonsAndGroups $personsAndGroups, Request $request, EntityManagerInterface $em)
    {
        $studentId = $request->get('id', '');
        $targetService = $request->get('targetService', 'all');

        return $this->resetPassword($configurationManager, $personsAndGroups, $em, $studentId, $targetService, true);
    }

    /**
     * Reset or renew a student password, anserwing in json.
     *
     * @param $studentId
     * @param $targetService
     * @param bool $renew
     *
     * @return Response
     */
    protected function resetPassword(ConfigurationManager $configurationManager, PersonsAndGroups $personsAndGroups, EntityManagerInterface $em, $studentId, $targetService, $renew = false)
    {
        $response = new JsonResponse();

        if (!$configurationManager->getTeacherCanResetPassword() && !$this->isGranted('ROLE_ADMIN')) {
            $response->setContent(json_encode(['success' => false, 'error' => 'Accesso negato']));

            return $response;
        }

        $student = $em->getRepository('App\Entity\Student')->find($studentId);
        if (!$student instanceof Student) {
            $response->setContent(json_encode(['success' => false, 'error' => 'Utente specificato non valido']));

            return $response;
        }

        if (0 == strlen(trim($student->getStartingPassword())) && !$renew) {
            $response->setContent(json_encode(['success' => false, 'error' => 'Password di default non generata']));

            return $response;
        }

        $newPassword = '';
        if ($renew) {
	        $newPassword = $personsAndGroups->generateStudentPasswordForProvider($student->getProvider());
            if (0 == strlen(trim($newPassword))) {
                $response->setContent(json_encode(['success' => false, 'error' => 'Errore durante la generazione della password']));

                return $response;
            }

            $student->setStartingPassword($newPassword);
        }
        if ('ad' == $targetService || 'all' == $targetService) {
            try {
                $personsAndGroups->resetPersonLdapUserPassword($student, true);
            } catch (\Exception $e) {
                $response->setContent(json_encode(['success' => false, 'error' => 'Errore durante il tentativo di cambio password di rete']));

                return $response;
            }
        }

        if ('email' == $targetService || 'all' == $targetService) {
            try {
                $personsAndGroups->resetPersonGoogleAppsUserPassword($student, true);
            } catch (\Exception $e) {
                $response->setContent(json_encode(['success' => false, 'error' => "Errore durante il tentativo di cambio password dell'account Google"]));

                return $response;
            }
        }

        $response->setContent(json_encode(['success' => true, 'newPassword' => $newPassword]));

        return $response;
    }

    /**
     * Send the student credential to the specified email.
     *
     * @Route("/student/{id}/send_credential_to_email", name="student_send_credential_to_email")
     * @IsGranted("ROLE_TEACHER")
     */
    public function sendCredentialToEmailAction(ConfigurationManager $configurationManager, Request $request, MailerInterface $mailer, EntityManagerInterface $em, $id)
    {
        $response = new Response();
        $response->headers->set('Content-Type', 'application/json');

        if (!$configurationManager->getTeacherCanSeeList() && !$this->isGranted('ROLE_ADMIN')) {
            $response->setContent(json_encode(['success' => false, 'error' => 'Accesso negato']));

            return $response;
        }

        $student = $em->getRepository('App\Entity\Student')->find($id);
        if (!$student instanceof Student) {
            $response->setContent(json_encode(['success' => false, 'error' => 'Studente non valido']));

            return $response;
        }

        $email = trim($request->get('emailTo', ''));
        if (!filter_var($email, FILTER_VALIDATE_EMAIL)) {
            $response->setContent(json_encode(['success' => false, 'error' => 'Email specificata non valida']));

            return $response;
        }

        $body = <<<MAIL
Riepilogo credenziali per lo studente {$student->getLastname()} {$student->getFirstname()}:

Username: {$student->getUsername()}
E-Mail: {$student->getEmail()}
Password: {$student->getStartingPassword()}
MAIL;

        $email = (new Email())
            ->from('no-reply@igroove.network')
            ->to($email)
            ->subject("Credenziali per lo studente {$student->getLastname()} {$student->getFirstname()}")
            ->text($body);


        try {
            $mailer->send($email);
            $response->setContent(json_encode(['success' => true]));
        } catch (TransportExceptionInterface $e) {
            $response->setContent(json_encode(['success' => false, 'error' => "Errore durante l'invio dell'email"]));
        }

        return $response;
    }
}
