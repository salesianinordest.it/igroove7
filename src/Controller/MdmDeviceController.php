<?php

namespace App\Controller;

use App\Entity\Group;
use App\Entity\MdmDevice;
use App\Entity\MdmServer;
use App\Entity\Provider;
use App\Entity\Student;
use App\Entity\Teacher;
use Doctrine\ORM\EntityManagerInterface;
use Knp\Component\Pager\PaginatorInterface;
use Symfony\Component\Routing\Annotation\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\IsGranted;
use Symfony\Component\HttpFoundation\Request;

/**
 * Class MdmDeviceController
 *
 * @Route(path="/mdm/device")
 * @package App\Controller
 */
class MdmDeviceController extends BaseAbstractController {

    /**
     * @Route(path="/", name="mdm_device_list")
     * @IsGranted("ROLE_MDM_MANAGER")
     * @Template
     */
    public function indexAction(Request $request, EntityManagerInterface $em, PaginatorInterface $paginator)
    {
        $filters = [
            'q' => $request->get('q', ""),
            'ownerType' => $request->get('ownerType', null),
            'mdmServer' => $request->get('mdmServer', null),
            'fromDep' => $request->get('fromDep', null),
            'supervised' => $request->get('supervised', null),
            'moreDevices' => $request->get('moreDevices', false) == "1",
            'order' => $request->get('order', 'serial'),
            'orderDir' => $request->get('orderDir', 'ASC'),
            'orderByServer' => $request->get('orderByServer', false) == "1"
        ];
        $mdmDeviceRepository = $em->getRepository(MdmDevice::class);
        $query = $mdmDeviceRepository->getDevicesWithOwnersQueryFromFilters($filters);

        $paginatedDevices = $paginator->paginate(
            $query,
            $request->query->get('page', 1),50
        );

        $mdmServers = $em->getRepository(MdmServer::class)->findBy(['active' => true]);

        $orderUrls = [];
        foreach (['serial', 'name', 'osVersion', 'lastInfoUpdate', 'lastname'] as $k) {
            $orderUrls[$k] = $this->generateUrl('mdm_device_list', [
                'q' => $filters['q'],
                'ownerType' => $filters['ownerType'],
                'mdmServer' => $filters['mdmServer'],
                'fromDep' => $filters['fromDep'],
                'supervised' => $filters['supervised'],
                'moreDevices' => $filters['moreDevices'],
                'orderByServer' => $filters['orderByServer'],
                'order' => $k,
                'orderDir' => $filters['order'] == $k && $filters['orderDir'] == "ASC" ? 'DESC' : 'ASC'
            ]);
        }

        $orderUrls['orderByServer'] = $this->generateUrl('mdm_device_list', [
            'q' => $filters['q'],
            'ownerType' => $filters['ownerType'],
            'mdmServer' => $filters['mdmServer'],
            'fromDep' => $filters['fromDep'],
            'supervised' => $filters['supervised'],
            'moreDevices' => $filters['moreDevices'],
            'order' => $filters['order'],
            'orderDir' => $filters['orderDir'],
            'orderByServer' => $filters['orderByServer'] == "1" ? '0' : '1'
        ]);

        return ['paginatedDevices' => $paginatedDevices, 'mdmServers' => $mdmServers, 'filters' => $filters, 'orderUrls' => $orderUrls];
    }

    /**
     * @Route(path="/studentsWithoutDevice", name="mdm_device_show_student_without_device")
     * @IsGranted("ROLE_MDM_MANAGER")
     * @Template
     */
    public function showStudentsWithoutDeviceAction(Request $request, EntityManagerInterface $em, PaginatorInterface $paginator)
    {
        $filters = [
            'groupId' => $request->get('groupId', null),
            'providerId' => $request->get('providerId', null)
        ];
        $providers = $em->getRepository(Provider::class)->getProvidersWithManagedStudentsDeviceEnabled();
        $students = $em->getRepository(Student::class)->getStudentsWithoutManagedDeviceQuery($filters, $providers);

        $paginatedStudents = $paginator->paginate(
            $students,
            $request->query->get('page', 1),
            50,
            ['wrap-queries' => true, 'distinct' => false]
        );

        $groups = $em->getRepository(Group::class)->findBy(['manageManually' => false], ['name' => "ASC"]);

        return ["paginatedStudents" => $paginatedStudents, 'filters' => $filters, "groups" => $groups, "providers" => $providers];
    }

    /**
     * @Route(path="/teachersWithoutDevice", name="mdm_device_show_teacher_without_device")
     * @IsGranted("ROLE_MDM_MANAGER")
     * @Template
     */
    public function showTeachersWithoutDeviceAction(Request $request, EntityManagerInterface $em, PaginatorInterface $paginator)
    {
        $filters = [
            'groupId' => $request->get('groupId', null),
            'providerId' => $request->get('providerId', null)
        ];
        $providers = $em->getRepository(Provider::class)->getProvidersWithManagedTeachersDeviceEnabled();
        $teachers = $em->getRepository(Teacher::class)->getTeachersWithoutManagedDeviceQuery($filters, $providers);

        $paginatedTeachers = $paginator->paginate(
            $teachers,
            $request->query->get('page', 1),
            50,
            ['wrap-queries' => true, 'distinct' => false]
        );

        $groups = $em->getRepository(Group::class)->findBy(['manageManually' => false], ['name' => "ASC"]);

        return ["paginatedTeachers" => $paginatedTeachers, 'filters' => $filters, "groups" => $groups, "providers" => $providers];
    }
}
