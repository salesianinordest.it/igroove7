<?php

namespace App\Controller\Admin;

use App\Entity\LdapGroup;
use EasyCorp\Bundle\EasyAdminBundle\Controller\AbstractCrudController;

class LdapGroupCrudController extends AbstractCrudController
{
    #[IsGranted('ROLE_ADMIN')]
    public static function getEntityFqcn(): string
    {
        return LdapGroup::class;
    }
}
