<?php

namespace App\Controller\Admin;

use App\Entity\MdmServer;
use EasyCorp\Bundle\EasyAdminBundle\Controller\AbstractCrudController;

class MdmServerCrudController extends AbstractCrudController
{
    #[IsGranted('ROLE_ADMIN')]
    public static function getEntityFqcn(): string
    {
        return MdmServer::class;
    }
}
