<?php

namespace App\Controller\Admin;

use App\Entity\TeacherSubjectGroup;
use EasyCorp\Bundle\EasyAdminBundle\Controller\AbstractCrudController;

class TeacherSubjectGroupCrudController extends AbstractCrudController
{
    #[IsGranted('ROLE_ADMIN')]
    public static function getEntityFqcn(): string
    {
        return TeacherSubjectGroup::class;
    }
}
