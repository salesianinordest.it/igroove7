<?php

namespace App\Controller\Admin;

use App\Entity\MikrotikList;
use EasyCorp\Bundle\EasyAdminBundle\Controller\AbstractCrudController;

class MikrotikListCrudController extends AbstractCrudController
{
    #[IsGranted('ROLE_ADMIN')]
    public static function getEntityFqcn(): string
    {
        return MikrotikList::class;
    }
}
