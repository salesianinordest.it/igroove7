<?php

namespace App\Controller\Admin;

use App\Entity\MikrotikPool;
use EasyCorp\Bundle\EasyAdminBundle\Controller\AbstractCrudController;

class MikrotikPoolCrudController extends AbstractCrudController
{
    #[IsGranted('ROLE_ADMIN')]
    public static function getEntityFqcn(): string
    {
        return MikrotikPool::class;
    }
}
