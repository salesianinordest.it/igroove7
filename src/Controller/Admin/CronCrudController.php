<?php

namespace App\Controller\Admin;

use App\Entity\Cron;
use EasyCorp\Bundle\EasyAdminBundle\Controller\AbstractCrudController;

class CronCrudController extends AbstractCrudController
{
    #[IsGranted('ROLE_ADMIN')]
    public static function getEntityFqcn(): string
    {
        return Cron::class;
    }
}
