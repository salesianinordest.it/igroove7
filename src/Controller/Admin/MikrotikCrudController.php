<?php

namespace App\Controller\Admin;

use App\Entity\Mikrotik;
use EasyCorp\Bundle\EasyAdminBundle\Controller\AbstractCrudController;

class MikrotikCrudController extends AbstractCrudController
{
    #[IsGranted('ROLE_ADMIN')]
    public static function getEntityFqcn(): string
    {
        return Mikrotik::class;
    }
}
