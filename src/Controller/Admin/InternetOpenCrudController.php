<?php

namespace App\Controller\Admin;

use App\Entity\InternetOpen;
use EasyCorp\Bundle\EasyAdminBundle\Controller\AbstractCrudController;

class InternetOpenCrudController extends AbstractCrudController
{
    #[IsGranted('ROLE_ADMIN')]
    public static function getEntityFqcn(): string
    {
        return InternetOpen::class;
    }
}
