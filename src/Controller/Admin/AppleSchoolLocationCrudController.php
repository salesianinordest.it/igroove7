<?php

namespace App\Controller\Admin;

use App\Entity\AppleSchoolLocation;
use EasyCorp\Bundle\EasyAdminBundle\Controller\AbstractCrudController;

class AppleSchoolLocationCrudController extends AbstractCrudController
{
    #[IsGranted('ROLE_ADMIN')]
    public static function getEntityFqcn(): string
    {
        return AppleSchoolLocation::class;
    }
}
