<?php

namespace App\Controller\Admin;

use App\Entity\ConfigurationStorage;
use EasyCorp\Bundle\EasyAdminBundle\Controller\AbstractCrudController;

class ConfigurationStorageCrudController extends AbstractCrudController
{
    #[IsGranted('ROLE_ADMIN')]
    public static function getEntityFqcn(): string
    {
        return ConfigurationStorage::class;
    }
}
