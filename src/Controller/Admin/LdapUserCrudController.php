<?php

namespace App\Controller\Admin;

use App\Entity\LdapUser;
use EasyCorp\Bundle\EasyAdminBundle\Controller\AbstractCrudController;

class LdapUserCrudController extends AbstractCrudController
{
    #[IsGranted('ROLE_ADMIN')]
    public static function getEntityFqcn(): string
    {
        return LdapUser::class;
    }
}
