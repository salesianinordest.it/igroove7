<?php

namespace App\Controller\Admin;

use App\Entity\Teacher;
use EasyCorp\Bundle\EasyAdminBundle\Config\Filters;
use EasyCorp\Bundle\EasyAdminBundle\Controller\AbstractCrudController;

class TeacherCrudController extends AbstractCrudController
{
    #[IsGranted('ROLE_ADMIN')]
    public static function getEntityFqcn(): string
    {
        return Teacher::class;
    }

	public function configureFilters( Filters $filters ): Filters {
		return $filters->add('lastname')
		               ->add('firstname')
		               ->add('fiscalCode')
		               ->add('username')
		               ->add('email')
			;
	}
}
