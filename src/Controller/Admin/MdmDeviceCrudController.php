<?php

namespace App\Controller\Admin;

use App\Entity\MdmDevice;
use EasyCorp\Bundle\EasyAdminBundle\Controller\AbstractCrudController;

class MdmDeviceCrudController extends AbstractCrudController
{
    #[IsGranted('ROLE_ADMIN')]
    public static function getEntityFqcn(): string
    {
        return MdmDevice::class;
    }
}
