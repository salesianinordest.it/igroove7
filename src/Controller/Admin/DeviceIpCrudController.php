<?php

namespace App\Controller\Admin;

use App\Entity\DeviceIp;
use EasyCorp\Bundle\EasyAdminBundle\Controller\AbstractCrudController;

class DeviceIpCrudController extends AbstractCrudController
{
    #[IsGranted('ROLE_ADMIN')]
    public static function getEntityFqcn(): string
    {
        return DeviceIp::class;
    }
}
