<?php

namespace App\Controller\Admin;

use App\Entity\AppleSchool;
use EasyCorp\Bundle\EasyAdminBundle\Controller\AbstractCrudController;

class AppleSchoolCrudController extends AbstractCrudController
{
    #[IsGranted('ROLE_ADMIN')]
    public static function getEntityFqcn(): string
    {
        return AppleSchool::class;
    }
}
