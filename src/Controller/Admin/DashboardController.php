<?php

namespace App\Controller\Admin;

use App\Entity\ActionLog;
use App\Entity\AppleSchool;
use App\Entity\ConfigurationStorage;
use App\Entity\Cron;
use App\Entity\Device;
use App\Entity\DeviceIp;
use App\Entity\Group;
use App\Entity\Guest;
use App\Entity\InternetOpen;
use App\Entity\LdapGroup;
use App\Entity\LdapUser;
use App\Entity\MdmDevice;
use App\Entity\MdmServer;
use App\Entity\Mikrotik;
use App\Entity\MikrotikList;
use App\Entity\MikrotikPool;
use App\Entity\Provider;
use App\Entity\Sector;
use App\Entity\Student;
use App\Entity\Subject;
use App\Entity\Teacher;
use App\Entity\TeacherSubjectGroup;
use EasyCorp\Bundle\EasyAdminBundle\Config\Dashboard;
use EasyCorp\Bundle\EasyAdminBundle\Config\MenuItem;
use EasyCorp\Bundle\EasyAdminBundle\Controller\AbstractDashboardController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class DashboardController extends AbstractDashboardController
{
    #[Route('/admin', name: 'admin')]
    #[IsGranted('ROLE_ADMIN')]
    public function index(): Response
    {
        return $this->render('admin/index.html.twig');
    }

    public function configureDashboard(): Dashboard
    {
        return Dashboard::new()
            ->setTitle('igroove7');
    }

    public function configureMenuItems(): iterable
    {
        return [
            MenuItem::linkToRoute('Ritorna ad igroove', 'fa fa-backward', 'homepage'),
            MenuItem::section('Persone e gruppi'),
            MenuItem::linkToCrud('Studenti', 'fa fa-user', Student::class),
            MenuItem::linkToCrud('Classi', 'fa fa-users', Group::class),
            MenuItem::linkToCrud('Docenti', 'fa fa-user', Teacher::class),
            MenuItem::linkToCrud('Guest', 'fa fa-user', Guest::class),
            MenuItem::linkToCrud('Provider', 'fa fa-shuffle', Provider::class),
            MenuItem::section('Active directory'),
            MenuItem::linkToCrud('Utenti', 'fa fa-user', LdapUser::class),
            MenuItem::linkToCrud('Gruppi', 'fa fa-users', LdapGroup::class),
            MenuItem::section('Configurazione rete'),
            MenuItem::linkToCrud('Router', 'fa fa-shuffle', Mikrotik::class),
            MenuItem::linkToCrud('Device', 'fa fa-shuffle', Device::class),
            MenuItem::linkToCrud('Device Ip', 'fa fa-shuffle', DeviceIp::class),
            MenuItem::linkToCrud('Mikrotik List', 'fa fa-shuffle', MikrotikList::class),
            MenuItem::linkToCrud('Mikrotik Pool', 'fa fa-shuffle', MikrotikPool::class),
            MenuItem::section('Scuola'),
            MenuItem::linkToCrud('Sector', 'fa fa-shuffle', Sector::class),
            MenuItem::linkToCrud('Subject', 'fa fa-shuffle', Subject::class),
            MenuItem::linkToCrud('Teacher Subject', 'fa fa-shuffle', TeacherSubjectGroup::class),
            MenuItem::section('Mdm'),
            MenuItem::linkToCrud('Mdm Device', 'fa fa-shuffle', MdmDevice::class),
            MenuItem::linkToCrud('Mdm Server', 'fa fa-shuffle', MdmServer::class),
            MenuItem::section('Varie'),
            MenuItem::linkToCrud('Action Log', 'fa fa-shuffle', ActionLog::class),
            MenuItem::linkToCrud('Apple School', 'fa fa-shuffle', AppleSchool::class),
            MenuItem::linkToCrud('Configuration Storage', 'fa fa-shuffle', ConfigurationStorage::class),
            MenuItem::linkToCrud('Cron', 'fa fa-shuffle', Cron::class),
            MenuItem::linkToCrud('Internet Open', 'fa fa-shuffle', InternetOpen::class),
           ];
    }
}
