<?php

namespace App\Controller\Admin;

use App\Entity\ActionLog;
use EasyCorp\Bundle\EasyAdminBundle\Controller\AbstractCrudController;

class ActionLogCrudController extends AbstractCrudController
{
    #[IsGranted('ROLE_ADMIN')]
    public static function getEntityFqcn(): string
    {
        return ActionLog::class;
    }
}
