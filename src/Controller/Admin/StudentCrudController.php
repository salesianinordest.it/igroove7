<?php

namespace App\Controller\Admin;

use App\Entity\Student;
use EasyCorp\Bundle\EasyAdminBundle\Config\Filters;
use EasyCorp\Bundle\EasyAdminBundle\Controller\AbstractCrudController;

class StudentCrudController extends AbstractCrudController
{
    #[IsGranted('ROLE_ADMIN')]
    public static function getEntityFqcn(): string
    {
        return Student::class;
    }

	public function configureFilters( Filters $filters ): Filters {
		return $filters->add('lastname')
						->add('firstname')
						->add('fiscalCode')
						->add('username')
						->add('email')
			;
	}
}
