<?php

namespace App\Controller;

use App\Entity\Fortigate;
use App\Entity\Watchguard;
use App\Manager\FortigateManager;
use App\Message\FortigateMessage;
use Doctrine\ORM\EntityManagerInterface;
use Knp\Component\Pager\PaginatorInterface;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\IsGranted;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Messenger\Envelope;
use Symfony\Component\Messenger\MessageBusInterface;

/**
 * Watchguard controller.
 *
 * @Route("/config/watchguard")
 */
class WatchguardController extends AbstractController
{


    /**
     * Lists all WatchguardList entities.
     *
     * @Route("/", name="watchguard")
     * @IsGranted("ROLE_ADMIN")
     */
    public function indexAction(PaginatorInterface $paginator, Request $request, EntityManagerInterface $em)
    {
        $queryString = $request->get('queryString', false);
        $q = '%'.$queryString.'%';
        if ($queryString) {
            $query = $em->createQuery(
                "SELECT l FROM App\Entity\Watchguard l WHERE l.host  LIKE :q ORDER BY l.host"
            )
                ->setParameter('q', $q);
        } else {
            $query = $em->createQuery("SELECT l FROM App\Entity\Watchguard l  ORDER BY l.host");
        }

        $pagination = $paginator->paginate(
            $query,
            $request->query->get('page', 1), 25
        );

        return $this->render(
            'watchguard/index.html.twig',
            [
                'pagination' => $pagination,
            ]
        );
    }



    /**
     * Creates a new Watchguard entity.
     *
     * @Route("/new", name="config_watchguard_new", methods={"POST","GET"})
     * @IsGranted("ROLE_ADMIN")
     */
    public function createAction(Request $request, EntityManagerInterface $em)
    {
        $entity = new Watchguard();
        $form = $this->createForm('App\Form\WatchguardType', $entity);
        $form->handleRequest($request);




        if ($form->isSubmitted() && $form->isValid()) {
            $em->persist($entity);
            $em->flush();

            return $this->redirect($this->generateUrl('watchguard', []));
        }

        return $this->render(
            'watchguard/edit.html.twig',
            [
                'entity' => $entity,
                'main_form' => $form->createView(),
                'isEditing' => false
            ]
        );
    }

    /**
     * Displays a form to edit an existing Watchguard entity.
     *
     * @Route("/{id}/edit", name="config_watchguard_edit")
     * @IsGranted("ROLE_ADMIN")
     */
    public function editAction(request $request, Watchguard $watchguard, EntityManagerInterface $em)
    {
        if (!$watchguard) {
            throw $this->createNotFoundException('Unable to find Watchguard entity.');
        }

        $editForm = $this->createForm('App\Form\WatchguardType', $watchguard);

        $editForm->handleRequest($request);

        if ($editForm->isSubmitted() && $editForm->isValid()) {
	        $em->flush();
            return $this->redirect($this->generateUrl('watchguard', []));
        }

        return $this->render(
            'watchguard/edit.html.twig',
            [
                'entity' => $watchguard,
                'main_form' => $editForm->createView(),
                'isEditing' => true
            ]
        );
    }

    /**
     * Deletes a Watchguard entity.
     *
     * @Route("/{id}/delete", name="config_watchguard_delete")
     * @IsGranted("ROLE_ADMIN")
     */
    public function deleteAction(Request $request, Watchguard $watchguard, EntityManagerInterface $em)
    {
        if (!$watchguard) {
            throw $this->createNotFoundException('Unable to find Watchguard entity.');
        }

        $em->remove($watchguard);
        $em->flush();

        return $this->redirect($this->generateUrl('watchguard'));
    }




}
