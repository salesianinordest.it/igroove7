<?php

namespace App\Controller;

use App\Entity\Group;
use App\Entity\Provider;
use App\Entity\ProviderToRemoveEntity;
use App\Entity\Sector;
use App\Entity\Student;
use App\Entity\Subject;
use App\Entity\Teacher;
use App\ImporterFilter\AbstractFilter;
use App\ImporterFilter\ImportedEntity\AbstractEntity;
use App\Manager\PersonsAndGroups;
use App\RepositoryAction\GroupRepositoryAction;
use App\RepositoryAction\StudentRepositoryAction;
use App\RepositoryAction\TeacherRepositoryAction;
use Doctrine\ORM\EntityManagerInterface;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\IsGranted;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\Routing\Annotation\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Finder\Finder;
use Symfony\Component\Form\Extension\Core\Type\PasswordType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\FormInterface;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;

/**
 * Provider controller.
 *
 * @Route("/config/provider")
 */
class ProviderController extends AbstractController
{
    protected $filterProviderData;
    protected $importedEntities;

	protected TeacherRepositoryAction $teacherRepositoryAction;
	protected StudentRepositoryAction $studentRepositoryAction;
	protected GroupRepositoryAction $groupRepositoryAction;

	public function __construct(TeacherRepositoryAction $teacherRepositoryAction, StudentRepositoryAction $studentRepositoryAction, GroupRepositoryAction $groupRepositoryAction) {
		$this->teacherRepositoryAction = $teacherRepositoryAction;
		$this->studentRepositoryAction = $studentRepositoryAction;
		$this->groupRepositoryAction = $groupRepositoryAction;
	}

	/**
     * Creates a new Provider entity.
     *
     * @Route("/", name="config_provider_create", methods={"POST"})
     * @IsGranted("ROLE_ADMIN")
     * @Template("provider/edit.html.twig")
     */
    public function createAction(Request $request, EntityManagerInterface $entityManager)
    {
        $entity = new Provider();
        $form = $this->createCreateForm($entity);
        $form->handleRequest($request);

        $this->prepareFilterProviderData($entity, $form);

        if ($form->isValid()) {
            $entityManager->persist($entity);
            $entityManager->flush();

            return $this->redirect($this->generateUrl('config_edit').'#provider');
        }

        return [
            'entity' => $entity,
            'form' => $form->createView(),
        ];
    }

    /**
     * Creates a form to create a Provider entity.
     *
     * @param Provider $entity The entity
     * @IsGranted("ROLE_ADMIN")
     *
     * @return \Symfony\Component\Form\FormInterface The form
     */
    private function createCreateForm(Provider $entity)
    {
        $form = $this->createForm('App\Form\ProviderType', $entity, [
            'action' => $this->generateUrl('config_provider_create'),
            'method' => 'POST',
            'filterProviderData' => $this->getFilterProviderData(),
        ]);

        $form->add('submit', SubmitType::class, ['label' => 'Crea']);

        return $form;
    }

    /**
     * Displays a form to create a new Provider entity.
     *
     * @Route("/new", name="config_provider_new")
     * @IsGranted("ROLE_ADMIN")
     * @Template("provider/edit.html.twig")
     */
    public function newAction()
    {
        $entity = new Provider();
        $form = $this->createCreateForm($entity);

        return [
            'entity' => $entity,
            'main_form' => $form->createView(),
        ];
    }

    /**
     * Displays a form to edit an existing Provider entity.
     *
     * @Route("/{id}/edit", name="config_provider_edit")
     * @IsGranted("ROLE_ADMIN")
     * @Template()
     */
    public function editAction(Request $request, Provider $provider, EntityManagerInterface $entityManager)
    {
        if (!$provider) {
            throw $this->createNotFoundException('Unable to find Provider entity.');
        }

        $editForm = $this->createEditForm($provider);

        $editForm->handleRequest($request);
        $this->prepareFilterProviderData($provider, $editForm);

        if ($editForm->isSubmitted() && $editForm->isValid()) {
            $entityManager->flush();

            return $this->redirect($this->generateUrl('config_edit', []).'#provider');
        }

        return [
            'entity' => $provider,
            'main_form' => $editForm->createView(),
            'delete_form' => $this->createDeleteForm($provider->getId())->createView()
        ];
    }

    /**
     * Creates a form to edit a Provider entity.
     *
     * @param Provider $entity The entity
     *
     * @return \Symfony\Component\Form\FormInterface The form
     */
    private function createEditForm(Provider $entity)
    {
        $form = $this->createForm('App\Form\ProviderType', $entity, [
            'action' => $this->generateUrl('config_provider_edit', ['id' => $entity->getId()]),
            'method' => 'PUT',
            'filterProviderData' => $this->getFilterProviderData(),
        ]);
        if (!empty($entity->getFilterData()) && '' != $entity->getFilter() && isset($this->filterProviderData[$entity->getFilter()])) {
            $filterData = $entity->getFilterData();
            foreach ($form->get('filterDataCont')->get($entity->getFilter())->all() as $dataField) {
                if (isset($filterData[$dataField->getName()])) {
                    $dataField->setData($filterData[$dataField->getName()]);
                }
            }
        }

	    $form->add('submit', SubmitType::class, array('label' => 'Salva'));

        return $form;
    }

    /**
     * Deletes a Provider entity.
     *
     * @Route("/{id}", name="config_provider_delete", methods={"DELETE"})
     * @IsGranted("ROLE_ADMIN")
     */
    public function deleteAction(Request $request, EntityManagerInterface $entityManager, $id)
    {
        $form = $this->createDeleteForm($id);
        $form->handleRequest($request);

        if ($form->isValid()) {
            $entity = $entityManager->getRepository(Provider::class)->find($id);

            if (!$entity) {
                throw $this->createNotFoundException('Unable to find Provider entity.');
            }

            $entityManager->remove($entity);
            $entityManager->flush();
        }

        return $this->redirect($this->generateUrl('config_edit').'#provider');
    }

    /**
     * Creates a form to delete a Provider entity by id.
     *
     * @param mixed $id The entity id
     *
     * @return \Symfony\Component\Form\FormInterface The form
     */
    private function createDeleteForm($id)
    {
        return $this->createFormBuilder()
            ->setAction($this->generateUrl('config_provider_delete', ['id' => $id]))
            ->setMethod('DELETE')
	        ->add('submit', SubmitType::class, ['label' => 'Elimina', 'attr' => ['class' => "btn-danger", 'onclick' => "return confirm('Sei sicuro di voler eliminare questo elemento?');"]])
            ->getForm();
    }

    protected function getFilterProviderData()
    {
        if (empty($this->filterProviderData)) {
            $finder = new Finder();
            $finder->files()->in(__DIR__.'/../ImporterFilter');
            foreach ($finder as $file) {
                if ('AbstractFilter.php' == $file->getRelativePathname() || '' == $file->getRelativePathname()) {
                    continue;
                }

                $class = 'App\\ImporterFilter\\'.substr($file->getRelativePathname(), 0, -4);
                if (!class_exists($class)) {
                    continue;
                }

                $this->filterProviderData[$class::$internalName] = ['name' => $class::$name, 'ui' => $class::$parametersUi];
            }
        }

        return $this->filterProviderData;
    }

    protected function prepareFilterProviderData(Provider $provider, FormInterface $form)
    {
        $filter = $provider->getFilter();

        if (!$form->get('filterDataCont')->has($filter) || !is_array($form->get('filterDataCont')->get($filter)->getData())) {
            return;
        }

        $currentFilterData = $provider->getFilterData();
        foreach ($form->get('filterDataCont')->get($filter)->getData() as $fieldName => $fieldValue) {
            if (!isset($this->filterProviderData[$filter]['ui'][$fieldName])) {
                continue;
            }

            if (PasswordType::class == $this->filterProviderData[$filter]['ui'][$fieldName]['type'] && '' == $fieldValue) {
                continue;
            }

            $currentFilterData[$fieldName] = $fieldValue;
        }

        $provider->setFilterData($currentFilterData);
    }

    /**
     * @Route("/{id}/sync", name="provider_show_manual_sync")
     * @IsGranted("ROLE_ACCOUNTS_MANAGER")
     * @Template("provider/showManualSync.html.twig")
     */
    public function showManualSyncAction(EntityManagerInterface $entityManager, $id)
    {
        $provider = $entityManager->getRepository(Provider::class)->find($id);

        if (!$provider) {
            throw $this->createNotFoundException('Unable to find Provider entity.');
        }

        $canDelete = $this->isGranted('ROLE_ADMIN');

        return ['provider' => $provider, 'canDelete' => $canDelete];
    }


    /**
     * @Route("/{id}/load-data", name="config_provider_load_data", methods={"POST"})
     * @IsGranted("ROLE_ACCOUNTS_MANAGER")
     */
    public function loadDataToSyncAction(EntityManagerInterface $entityManager, ContainerInterface $container, $id)
    {
        $provider = $entityManager->getRepository(Provider::class)->find($id);

        if (!$provider) {
            return new JsonResponse(['error' => 'Provider non trovato']);
        }

        $filter = clone $container->get($provider->getFilter());
        if (!$filter instanceof AbstractFilter) {
            return new JsonResponse(['error' => 'Filtro provider non valido']);
        }
        $filter->setParameters($provider->getFilterData());
        $filter->setIsManualImport(true);
        try {
            $filter->parseRemoteData();
        } catch (\ErrorException $e) {
            return new JsonResponse(['error' => "Errore nel parsing del provider {$provider->getName()}: ".$e->getMessage()]);
        }

        $data = [];
        $data['sectors'] = $this->compareProviderData($filter->getSectors(), $provider, 'Sector', 'name', $entityManager);

        $newGroups = $filter->getGroups();
        foreach ($newGroups as $newGroup) {
            if (null !== $newGroup->getSectorId() && isset($this->importedEntities['Sector'][$newGroup->getSectorId()])) {
                $newGroup->setSector($this->importedEntities['Sector'][$newGroup->getSectorId()]);
            }
        }

        $data['groups'] = $this->compareProviderData($newGroups, $provider, 'Group', 'name', $entityManager, $filter->getInvalidGroupsById());
        $data['subjects'] = $this->compareProviderData($filter->getSubjects(), $provider, 'Subject', 'name', $entityManager);
        $data['teachers'] = $this->compareProviderData($filter->getTeachers(), $provider, 'Teacher', 'fiscalCode', $entityManager, $filter->getInvalidTeachersById());

        $newStudents = $filter->getStudents();
        foreach ($newStudents as $k => $newStudent) {
            if (null !== $newStudent->getGroupId() && isset($this->importedEntities['Group'][$newStudent->getGroupId()])) {
                $newStudent->setGroup($this->importedEntities['Group'][$newStudent->getGroupId()]);
            }
        }
        $data['students'] = $this->compareProviderData($newStudents, $provider, 'Student', 'fiscalCode', $entityManager, $filter->getInvalidStudentsById());
		$data['invalidEntities'] = $filter->getInvalidEntities();

        return new JsonResponse(['data' => $data]);
    }

    /**
     * @Route("/{providerId}/import-data", name="config_provider_import_data", methods={"POST"})
     * @IsGranted("ROLE_ACCOUNTS_MANAGER")
     */
    public function importDataAction(Request $request, EntityManagerInterface $entityManager, $providerId)
    {
        $entityData = $request->get('entityData');
        $entityType = $request->get('entityType');
        $entityId = $request->get('entityId');

        if (null === $entityData || null === $entityId || null === $entityType) {
            return new JsonResponse(['success' => false, 'error' => 'Invalid data']);
        }

        $provider = $entityManager->getRepository(Provider::class)->find($providerId);

        if (!$provider) {
            return new JsonResponse(['success' => false, 'error' => 'Provider non trovato']);
        }

        $entityData['id'] = $entityId;
        $entityType = ucfirst(substr($entityType, 0, -1));
        $entityTypeFull = '\\App\\Entity\\'.$entityType;

        if (!class_exists($entityTypeFull)) {
            return new JsonResponse(['success' => false, 'error' => 'Invalid entity type']);
        }

        $repository = $entityManager->getRepository($this->getEntityClass($entityType));

        $importedEntity = $this->getImportedEntityFromData($entityData, $entityType, $entityManager);
        if (!$importedEntity instanceof AbstractEntity || !$importedEntity->isValid()) {
            return $importedEntity instanceof JsonResponse ? $importedEntity : new JsonResponse(['success' => false, 'error' => 'Invalid entity type or data']);
        }

        if ('Student' === $entityType || 'Teacher' === $entityType) {
            $entity = $repository->findOneBy(['fiscalCode' => $importedEntity->getFiscalCode()]);
            if ($entity instanceof $entityTypeFull) {
                if ($entity->getProvider()->getId() != $providerId) {
                    $entity->addAdditionalProvider($provider);
                    $entityManager->flush();

                    if ($entity instanceof Student && !$entity->haveSameGroup($importedEntity, $provider)) {
                        $repository->updateMembershipWithImportData($entity, $importedEntity, $provider);
                    }
                }

                return new JsonResponse(['success' => true, 'error' => null]);
            }
        }

        try {
            $entity = $repository->createWithImportData($importedEntity, $provider, $entityData['id']);
        } catch (\Exception $e) {
            return new JsonResponse(['success' => false, 'error' => 'Error during entity creation']);
        }

        if ('Student' === $entityType) {
            try {
                $this->studentRepositoryAction->executeAfterCreate($entity);
            } catch (\Exception $e) {
                return new JsonResponse(['success' => false, 'error' => 'Error during post operation: '.$e->getMessage()]);
            }
        } elseif ('Teacher' === $entityType) {
            try {
                $this->teacherRepositoryAction->executeAfterCreate($entity);
            } catch (\Exception $e) {
                return new JsonResponse(['success' => false, 'error' => 'Error during post operation: '.$e->getMessage()]);
            }
        } elseif ('Group' === $entityType) {
            try {
                $this->groupRepositoryAction->executeAfterCreate($entity);
            } catch (\Exception $e) {
                return new JsonResponse(['success' => false, 'error' => 'Error during post operation: '.$e->getMessage()]);
            }
        }

        return new JsonResponse(['success' => true, 'error' => null, 'newEntityId' => $entity->getId()]);
    }

    /**
     * @Route("/{providerId}/update-data", name="config_provider_update_data", methods={"POST"})
     * @IsGranted("ROLE_ACCOUNTS_MANAGER")
     */
    public function updateDataAction(Request $request, EntityManagerInterface $entityManager, $providerId)
    {
        $entityData = $request->get('entityData');
        $entityId = $request->get('entityId');
        $entityType = $request->get('entityType');

        if (null === $entityData || null === $entityId || null === $entityType || !isset($entityData['oldId'])) {
            return new JsonResponse(['success' => false, 'error' => 'Invalid data']);
        }

        $provider = $entityManager->getRepository(Provider::class)->find($providerId);

        if (!$provider) {
            return new JsonResponse(['success' => false, 'error' => 'Provider non trovato']);
        }

        $entityData['id'] = $entityId;
        $entityType = ucfirst(substr($entityType, 0, -1));
        $entityTypeFull = '\\App\\Entity\\'.$entityType;

        if (!class_exists($entityTypeFull)) {
            return new JsonResponse(['success' => false, 'error' => 'Invalid entity type']);
        }

        $repository = $entityManager->getRepository($this->getEntityClass($entityType));

        $entity = $repository->find($entityData['oldId']);
        if (!$entity instanceof $entityTypeFull) {
            return new JsonResponse(['success' => false, 'error' => 'Invalid entity']);
        }

        $importedEntity = $this->getImportedEntityFromData($entityData, $entityType, $entityManager);
        if (!$importedEntity instanceof AbstractEntity || !$importedEntity->isValid()) {
            return $importedEntity instanceof JsonResponse ? $importedEntity : new JsonResponse(['success' => false, 'error' => 'Invalid entity type or data']);
        }

        if (('Student' === $entityType || 'Teacher' === $entityType) && $entity->getProvider()->getId() != $providerId) {
            $entity->addAdditionalProvider($provider);
            $entityManager->flush();

            if ($entity instanceof Student && !$entity->haveSameGroup($importedEntity, $provider)) {
                $repository->updateMembershipWithImportData($entity, $importedEntity, $provider);
            }

            return new JsonResponse(['success' => true, 'error' => null]);
        }

        $previousEntity = clone $entity;

        try {
            $repository->updateWithImportData($entity, $importedEntity, $provider);
        } catch (\Exception $e) {
            return new JsonResponse(['success' => false, 'error' => 'Error during entity update']);
        }

        if ('Student' === $entityType) {
            try {
                $this->studentRepositoryAction->executeAfterUpdate($entity, $previousEntity);
            } catch (\Exception $e) {
                return new JsonResponse(['success' => false, 'error' => 'Error during post operation: '.$e->getMessage()]);
            }
        } elseif ('Teacher' === $entityType) {
            try {
                $this->teacherRepositoryAction->executeAfterUpdate($entity, $previousEntity);
            } catch (\Exception $e) {
                return new JsonResponse(['success' => false, 'error' => 'Error during post operation: '.$e->getMessage()]);
            }
        } elseif ('Group' === $entityType) {
            try {
                $this->groupRepositoryAction->executeAfterUpdate($entity, $previousEntity->getName());
            } catch (\Exception $e) {
                return new JsonResponse(['success' => false, 'error' => 'Error during post operation: '.$e->getMessage()]);
            }
        }

        return new JsonResponse(['success' => true, 'error' => null]);
    }

    /**
     * @Route("/{providerId}/remove-data", name="config_provider_remove_data", methods={"POST"})
     * @IsGranted("ROLE_ADMIN")
     */
    public function removeDataAction(Request $request, $providerId, EntityManagerInterface $entityManager, PersonsAndGroups $personsAndGroups)
    {
        $entityData = $request->get('entityData');
        $entityId = $request->get('entityId');
        $entityType = $request->get('entityType');
        $options = $request->get('options');

        if (null === $entityData || null === $entityId || null === $entityType) {
            return new JsonResponse(['success' => false, 'error' => 'Invalid data']);
        }

        $entityType = ucfirst(substr($entityType, 0, -1));
        $entityTypeFull = '\\App\\Entity\\'.$entityType;

        if (!class_exists($entityTypeFull)) {
            return new JsonResponse(['success' => false, 'error' => 'Invalid entity type']);
        }

        $repository = $entityManager->getRepository($this->getEntityClass($entityType));
        $entity = $repository->find($entityId);
        if (!$entity instanceof $entityTypeFull) {
            return new JsonResponse(['success' => false, 'error' => 'Invalid entity']);
        }

        $currentProvider = $entityManager->getRepository(Provider::class)->find($providerId);
        if (!$currentProvider instanceof Provider) {
            return new JsonResponse(['success' => false, 'error' => 'Invalid current provider']);
        }

        $errors = [];
        if ($entity instanceof Student) {
            if ($entity->getProvider()->getId() != $providerId) {
                try {
                    $this->studentRepositoryAction->executeBeforeRemoveFromAdditionalProvider($entity, $currentProvider);
                    $entity->removeAdditionalProvider($currentProvider);
                    $entityManager->flush();
                } catch (\Exception $e) {
                    $errors[] = $e->getMessage();
                }

                return new JsonResponse(['success' => true, 'error' => implode("\n", $errors)]);
            }

            try {
                $this->studentRepositoryAction->executeBeforeRemove(
                    $entity,
                    isset($options['deleteOnLdap']) && ('true' == $options['deleteOnLdap']),
                    isset($options['deleteOnGApps']) ? (int) $options['deleteOnGApps'] : 0,
                    isset($options['gAppsOu']) ? $options['gAppsOu'] : ''
                );
            } catch (\Exception $e) {
                $errors[] = $e->getMessage();
            }
        } elseif ($entity instanceof Teacher) {
            if ($entity->getProvider()->getId() != $providerId) {
                try {
                    $this->teacherRepositoryAction->executeBeforeRemoveFromAdditionalProvider($entity, $currentProvider);
                    $entity->removeAdditionalProvider($currentProvider);
                    $entityManager->flush();
                } catch (\Exception $e) {
                    $errors[] = $e->getMessage();
                }

                return new JsonResponse(['success' => true, 'error' => implode("\n", $errors)]);
            }

            try {
                $this->teacherRepositoryAction->executeBeforeRemove(
                    $entity,
                    isset($options['deleteOnLdap']) && ('true' == $options['deleteOnLdap']),
                    isset($options['deleteOnGApps']) ? (int) $options['deleteOnGApps'] : 0,
                    isset($options['gAppsOu']) ? $options['gAppsOu'] : ''
                );
            } catch (\Exception $e) {
                $errors[] = $e->getMessage();
            }
        } elseif ($entity instanceof Group) {
            if (count($entity->getStudents()) > 0) {
                return new JsonResponse(['success' => false, 'error' => 'Impossibile eliminare un gruppo non vuoto']);
            }

            try {
                $this->groupRepositoryAction->executeBeforeRemove(
                    $entity,
                    isset($options['deleteOnLdap']) && ('true' == $options['deleteOnLdap']),
                    isset($options['deleteOnGApps']) && ('true' == $options['deleteOnGApps'])
                );
            } catch (\Exception $e) {
                $errors[] = $e->getMessage();
            }
        }

        try {
            $entityManager->remove($entity);
            $entityManager->flush();
        } catch (\Exception $e) {
            $errors[] = $personsAndGroups->prepareFormErrorMessage("Errore durante l'eliminazione dell'entità", $e);
        }

        return new JsonResponse(['success' => true, 'error' => implode("\n", $errors)]);
    }

    protected function getImportedEntityFromData($entityData, $entityType, EntityManagerInterface $entityManager)
    {
        $importedEntity = null;

        if ('Student' === $entityType) {
            $importedEntity = new \App\ImporterFilter\ImportedEntity\Student(
                $entityData['id'],
                $entityData['fiscalCode'],
                $entityData['firstName'],
                $entityData['lastName'],
                $entityData['groupId'] ?? null,
                $entityData['email'] ?? null,
                $entityData['username'] ?? null,
                $entityData['password'] ?? null
            );

            if (!isset($entityData['internalGroupId']) || '' == $entityData['internalGroupId']) {
                return new JsonResponse(['success' => false, 'error' => 'Invalid group id for student']);
            }

            $group = $entityManager->getRepository(Group::class)->find($entityData['internalGroupId']);
            if (!$group instanceof Group) {
                return new JsonResponse(['success' => false, 'error' => 'Invalid group for student']);
            }

            $importedEntity->setGroup($group);
        } elseif ('Group' === $entityType) {
            $importedEntity = new \App\ImporterFilter\ImportedEntity\Group($entityData['id'], $entityData['name'], $entityData['sectorId']);

            if (!isset($entityData['internalSectorId']) || '' == $entityData['internalSectorId']) {
                return new JsonResponse(['success' => false, 'error' => 'Invalid group id for student']);
            }

            $sector = $entityManager->getRepository(Sector::class)->find($entityData['internalSectorId']);
            if (!$sector instanceof Sector) {
                return new JsonResponse(['success' => false, 'error' => 'Invalid sector for group']);
            }

            $importedEntity->setSector($sector);
        } elseif ('Teacher' === $entityType) {
            $importedEntity = new \App\ImporterFilter\ImportedEntity\Teacher(
                $entityData['id'],
                $entityData['fiscalCode'],
                $entityData['firstName'],
                $entityData['lastName'],
                $entityData['email'] ?? null,
                $entityData['password'] ?? null
            );
        } elseif ('Subject' === $entityType) {
            $importedEntity = new \App\ImporterFilter\ImportedEntity\Subject($entityData['id'], $entityData['name']);
        } elseif ('Sector' === $entityType) {
            $importedEntity = new \App\ImporterFilter\ImportedEntity\Sector($entityData['id'], $entityData['name']);
        }

        return $importedEntity;
    }

    /**
     * @param AbstractEntity[] $newEntities
     * @param $entityType
     * @param string $keyToCheck
     *
     * @return array
     */
    protected function compareProviderData($newEntities, Provider $provider, $entityType, string $keyToCheck, EntityManagerInterface $entityManager, $invalidEntitiesById = [])
    {
        $toImport = $toUpdate = $residualCurrentEntities = $checkKey = [];
        $this->importedEntities[$entityType] = [];

        $keyToCheckGet = 'get'.str_replace('_', '', ucwords($keyToCheck, '_'));
	    $entityTypeClass = $this->getEntityClass($entityType);
        $currentEntities = $entityManager->getRepository($entityTypeClass)->findBy(['provider' => $provider->getId()]);
        foreach ($currentEntities as $currentEntity) {
            // aggiungo a entità residue, se non ha un idOnProvider o se non esiste l'entità con quell'id fra le nuove
            if ('' == $currentEntity->getIdOnProvider() || !isset($newEntities[$currentEntity->getIdOnProvider()])) {
                $residualCurrentEntities[(string)$currentEntity->getId()] = $currentEntity;
                if ('' != $keyToCheck) {
                    $checkKey[strtolower($currentEntity->$keyToCheckGet())] = (string)$currentEntity->getId();
                }

                continue;
            }

            $newEntity = $newEntities[$currentEntity->getIdOnProvider()];
            if (!$newEntity->isValid()) {
//                $this->logger->error("* Dati dell'entità dal provider {$currentEntity->getIdOnProvider()} da aggiornare su id locale {$currentEntity->getId()}, non validi");
                continue;
            }
            unset($newEntities[$currentEntity->getIdOnProvider()]);
            $this->importedEntities[$entityType][$currentEntity->getIdOnProvider()] = $currentEntity;

			$isTrashed = ('Student' == $entityType || 'Teacher' == $entityType) && $currentEntity->isTrashed();
	        if (!$currentEntity->isSame($newEntity) || $isTrashed) { //se è cestinato, aggiornando viene ripristinato
                $toUpdate[$currentEntity->getIdOnProvider()] = $newEntity->toArray();
                $toUpdate[$currentEntity->getIdOnProvider()]['oldId'] = (string)$currentEntity->getId();
                if ('Sector' == $entityType || 'Subject' == $entityType || 'Group' == $entityType) {
                    $toUpdate[$currentEntity->getIdOnProvider()]['_name'] = $currentEntity->getName();
                } elseif ('Student' == $entityType || 'Teacher' == $entityType) {
                    $toUpdate[$currentEntity->getIdOnProvider()]['_name'] = $currentEntity->getLastname().' '.$currentEntity->getFirstname().' ('.$currentEntity->getFiscalCode().($isTrashed?', cestinato, verrà ripristinato':'').')';
                }
            }
        }

        if ('Student' == $entityType || 'Teacher' == $entityType) {
            $additionalPersons = 'Teacher' == $entityType ? $provider->getAdditionalTeachers() : $provider->getAdditionalStudents();
            $residualCurrentAdditionalEntities = [];
            if ('' != $keyToCheck) {
                foreach ($additionalPersons as $k => $additionalPerson) {
                    $residualCurrentAdditionalEntities[strtolower($additionalPerson->$keyToCheckGet())] = $additionalPerson;
                }
            }
        }

        foreach ($newEntities as $idOnProvider => $newEntity) {
            // cerco se esiste un entità con la chiave da cercare (nome o id) combaciante, e nel caso la aggiorno, altrimenti inserico
            if ('' != $keyToCheck && isset($checkKey[strtolower($newEntity->$keyToCheckGet())]) &&
                isset($residualCurrentEntities[$checkKey[strtolower($newEntity->$keyToCheckGet())]])
            ) {
                $currentEntity = $residualCurrentEntities[$checkKey[strtolower($newEntity->$keyToCheckGet())]];
                unset($residualCurrentEntities[$checkKey[strtolower($newEntity->$keyToCheckGet())]]);

                if (!$currentEntity instanceof $entityTypeClass) {
                    continue;
                }

                if (!$newEntity->isValid()) {
//                    $this->logger->error("* Dati dell'entità dal provider {$keyToCheck} = {$newEntity->$keyToCheckGet()} da aggiornare su id locale {$entity->getId()}, non validi");
                    continue;
                }

                $currentEntity->setIdOnProvider($idOnProvider);
                $this->importedEntities[$entityType][$idOnProvider] = $currentEntity;

	            $isTrashed = ('Student' == $entityType || 'Teacher' == $entityType) && $currentEntity->isTrashed();
	            if (!$currentEntity->isSame($newEntity) || $isTrashed) { //se è cestinato, aggiornando viene ripristinato
                    $toUpdate[$currentEntity->getIdOnProvider()] = $newEntity->toArray();
                    $toUpdate[$currentEntity->getIdOnProvider()]['oldId'] = (string)$currentEntity->getId();
                    if ('Sector' == $entityType || 'Subject' == $entityType || 'Group' == $entityType) {
                        $toUpdate[$currentEntity->getIdOnProvider()]['_name'] = $currentEntity->getName();
                    } elseif ('Student' == $entityType || 'Teacher' == $entityType) {
                        $toUpdate[$currentEntity->getIdOnProvider()]['_name'] = $currentEntity->getLastname().' '.$currentEntity->getFirstname().' ('.$currentEntity->getFiscalCode().($isTrashed?', cestinato, verrà ripristinato':'').')';
                    }
                }
            } elseif (('Student' == $entityType || 'Teacher' == $entityType) && '' != $keyToCheck && isset($residualCurrentAdditionalEntities[strtolower($newEntity->$keyToCheckGet())])) { // se è una persona, cerco se esiste fra le persone non del provider, ma associate
                $entity = $residualCurrentAdditionalEntities[strtolower($newEntity->$keyToCheckGet())];
                unset($residualCurrentAdditionalEntities[strtolower($newEntity->$keyToCheckGet())]);

                if (!$entity instanceof $entityTypeClass) {
                    continue;
                }

                if (!$newEntity->isValid()) {
                    continue;
                }

                if ('Student' == $entityType && !$entity->haveSameGroup($newEntity, $provider)) {
                    $toUpdate[$currentEntity->getIdOnProvider()] = $newEntity->toArray();
                    $toUpdate[$currentEntity->getIdOnProvider()]['oldId'] = (string)$currentEntity->getId();
                    $toUpdate[$currentEntity->getIdOnProvider()]['_name'] = $currentEntity->getLastname().' '.$currentEntity->getFirstname().' ('.$currentEntity->getFiscalCode().')';
                }
            } elseif ('' != $keyToCheck && ('Student' == $entityType || 'Teacher' == $entityType) && '' != $newEntity->$keyToCheckGet() &&
                ($currentEntity = $entityManager->getRepository($this->getEntityClass($entityType))->findOneBy([$keyToCheck => strtolower($newEntity->$keyToCheckGet())])) instanceof $entityTypeClass
            ) { // se è una persona, cerco con la chiave di controllo se non esiste già su un'altro provider per associarla a questo provider e se studente aggiornare i suoi gruppi
	            $toImport[$idOnProvider] = $newEntity->toArray();
	            $toImport[$idOnProvider]['_name'] = $newEntity->getLastName().' '.$newEntity->getFirstName().' ('.$newEntity->getFiscalCode().')';
            } else {
                if (!$newEntity->isValid()) {
//                    $this->logger->error("* Dati dell'entità dal provider {$newEntity->$keyToCheckGet()} da inserire, non validi");
                    continue;
                }

                if ($entityManager->getRepository($this->getEntityClass($entityType))->findOneBy([$keyToCheck => strtolower($newEntity->$keyToCheckGet())]) instanceof $entityTypeClass) {
                    continue;
                }

                $toImport[$idOnProvider] = $newEntity->toArray();
                if ('Sector' == $entityType || 'Subject' == $entityType || 'Group' == $entityType) {
                    $toImport[$idOnProvider]['_name'] = $newEntity->getName();
                } elseif ('Student' == $entityType || 'Teacher' == $entityType) {
                    $toImport[$idOnProvider]['_name'] = $newEntity->getLastName().' '.$newEntity->getFirstName().' ('.$newEntity->getFiscalCode().')';
                }
            }

            unset($newEntities[$idOnProvider]);
        }

        $toDelete = [];
        foreach ($residualCurrentEntities as $id => $residualCurrentEntity) {
			$reason = isset($invalidEntitiesById[$residualCurrentEntity->getIdOnProvider()]) ? $invalidEntitiesById[$residualCurrentEntity->getIdOnProvider()]['error'] : "Non presente sul provider o non valido.";
            if ('Sector' == $entityType || 'Subject' == $entityType || 'Group' == $entityType) {
                $toDelete[$id] = ['_name' => $residualCurrentEntity->getName(), 'reason' => $reason];
            } elseif ('Student' == $entityType || 'Teacher' == $entityType) {
	            if(!$residualCurrentEntity->isTrashed()) {
					$toDelete[$id] = ['_name' => $residualCurrentEntity->getLastname().' '.$residualCurrentEntity->getFirstname().' ('.$residualCurrentEntity->getFiscalCode().')', 'reason' => $reason];
				}
            }
        }

        return ['toImport' => $toImport, 'toUpdate' => $toUpdate, 'toDelete' => $toDelete];
    }

	protected function getEntityClass($entityType) {
		switch ($entityType) {
			case "Sector":
				return Sector::class;
			case "Group":
				return Group::class;
			case "Student":
				return Student::class;
			case "Teacher":
				return Teacher::class;
			case "Subject":
				return Subject::class;
			default:
				throw new \InvalidArgumentException( "Invalid entity type" );
		}
	}
}
