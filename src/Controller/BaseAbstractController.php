<?php

namespace App\Controller;

use Psr\Log\LoggerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;

abstract class BaseAbstractController extends AbstractController
{
    protected $logger;
    protected $actionLogger;

    public function __construct(LoggerInterface $logger, LoggerInterface $actionLogger)
    {
        $this->logger = $logger;
        $this->actionLogger = $actionLogger;
    }

	protected function logAction($action, $info, $extraData=[]) {
		if (!is_array($extraData)) {
			$extraData = [];
		}

		$extraData['action'] = $action;
		$extraData['username'] = $this->getUser()->getUserIdentifier();
		$extraData['ip'] = $_SERVER['REMOTE_ADDR'];
		$this->actionLogger->info("[{$action}] ".$this->getUser()->getUserIdentifier()."({$extraData['ip']}) {$info}", $extraData);
	}
}
