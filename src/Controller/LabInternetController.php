<?php

namespace App\Controller;

use App\Message\MikrotikMessage;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Messenger\Envelope;
use Symfony\Component\Messenger\MessageBusInterface;
use Symfony\Component\Routing\Annotation\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use App\Entity\InternetOpen;
use App\Entity\MikrotikList;
use Knp\Component\Pager\PaginatorInterface;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\IsGranted;

/**
 * LabInternet controller.
 *
 * @Route(path="/lab/internet")
 */
class LabInternetController extends BaseAbstractController
{
    /**
     * Lists all MikrotikList entities marked as laboratory.
     *
     * @Route(path="/", name="lab_internet_list", methods={"GET"})
     * @IsGranted("ROLE_LAB_TEACHER")
     * @Template("lab_internet/index.html.twig")
     */
    public function indexAction(PaginatorInterface $paginator, EntityManagerInterface $em, Request $request)
    {
	    $query = $em->createQuery('SELECT l FROM App:MikrotikList l WHERE l.showAsLaboratory = true ORDER BY l.nome');

        $currentUser = $this->getUser();
        $currentUserName = "";
        $currentUserIsAdmin = $this->isGranted('ROLE_ADMIN');
        if($currentUser instanceof \IMAG\LdapBundle\User\LdapUser){ //@todo
            $currentUserName = $currentUser->getUsername();
        }

	    $activatingList = false;
        $mikrotikLists = $query->getResult();
        foreach ($mikrotikLists as $mikrotikList) {
            $mikrotikList->state                = "not-active";
            $mikrotikList->endingTime           = "";
            $mikrotikList->activedByOtherUser   = false;
            $mikrotikList->activedBy            = "";

            $internetOpen = $em->getRepository(InternetOpen::class)->findOneBy(['account' => $mikrotikList->getId(), 'type' => 'ipList']);
            if($internetOpen instanceof InternetOpen) {
                if(!$internetOpen->getActivationCompleated()) {
                    $mikrotikList->state = "activating";
	                $activatingList      = true;
                } else {
                    $mikrotikList->state      = "active";
                    $mikrotikList->endingTime = $internetOpen->getCloseAt()->format("H:i");

                    $mikrotikList->activedBy = $currentUserIsAdmin && $internetOpen->getActivedBy() != "" ? $internetOpen->getActivedBy() : "";
                    if($currentUserName != "" && $internetOpen->getActivedBy() != $currentUserName) {
                        $mikrotikList->activedByOtherUser = true;
                    }
                }
            }
        }

        $pagination = $paginator->paginate(
            $mikrotikLists,
            $request->query->get('page', 1),
            25
        );

	    $activatedList = "";
	    if ($activatingList && $request->get('activatedList', "") != "") {
		    $activatedList = $request->get('activatedList', "");
	    }

        return array(
            'pagination' => $pagination,
            'timeToSet' => new \DateTime("+1 hour"),
	        'activatedList' => $activatedList
        );
    }

	/**
	 * Enable internet for specified mikrotik ip list
	 *
	 * @todo refactor: spostare codice in comune con InternetController in un Manager?
	 *
	 * @Route(path="/{mikrotikList}/on", name="lab_internet_on", methods={"GET"})
	 * @IsGranted("ROLE_LAB_TEACHER")
	 */
	public function internetOnIpList(EntityManagerInterface $em, Request $request, MikrotikList $mikrotikList, MessageBusInterface $messageBus)
	{
		if(!$mikrotikList instanceof MikrotikList) {
			throw $this->createNotFoundException('Non è stato possibile trovare la lista indicata.');
		}

		if(!$mikrotikList->isShowAsLaboratory()) {
			throw $this->createAccessDeniedException("Impossibile attivare una lista che non è segnata come laboratorio.");
		}

		$timeToSet = $request->get('timeToSet', false);
		$closeAt = new \DateTime($timeToSet);
		$currentUserName = $this->getUser()->getUserIdentifier();

		$currentEntity = $em->getRepository(InternetOpen::class)->findOneBy(['account' => $mikrotikList->getId(), 'type' => 'ipList']);
		if($currentEntity instanceof InternetOpen) {
			$currentEntity->setCloseAt($closeAt);
			$currentEntity->setActivedBy($currentUserName);
			$em->flush();

			$this->logAction("internetOn-ipList","ha esteso {$mikrotikList->getNome()}", ['id' => $mikrotikList->getId()]);
		} else {
			$internetOpen = new InternetOpen();
			$internetOpen->setType('ipList');
			$internetOpen->setAccount($mikrotikList->getId());
			$internetOpen->setCloseAt($closeAt);
			$internetOpen->setActivedBy($currentUserName);
			$internetOpen->setActivationCompleated(FALSE);
			$em->persist($internetOpen);
			$em->flush();

			$this->logAction("internetOn-ipList","ha abilitato {$mikrotikList->getNome()}", ['id' => $mikrotikList->getId()]);
		}

		$msg = new MikrotikMessage();
		$msg->setCommand('addIpToBypassedList');
		$messageBus->dispatch(new Envelope($msg));

		return $this->redirect($this->generateUrl('lab_internet_list', ['activatedList' => $mikrotikList->getId()]));
	}

	/**
	 * Disable internet for specified mikrotik ip list
	 *
	 * @Route(path="/{mikrotikListId}/off", name="lab_internet_off", methods={"GET"})
	 * @IsGranted("ROLE_LAB_TEACHER")
	 */
	public function internetOffIpList(EntityManagerInterface $em, MessageBusInterface $messageBus, $mikrotikListId)
	{
		$mikrotikList = $em->getRepository(MikrotikList::class)->find($mikrotikListId);
		if(!$mikrotikList instanceof MikrotikList) {
			throw $this->createNotFoundException('Non è stato possibile trovare la lista indicata.');
		}

		if(!$mikrotikList->isShowAsLaboratory()) {
			throw $this->createAccessDeniedException("Impossibile attivare una lista che non è segnata come laboratorio.");
		}

		$internetOpen = $em->getRepository(InternetOpen::class)->findOneBy(['account' => $mikrotikListId, 'type' => "ipList"]);
		if(!$internetOpen instanceof InternetOpen) {
			return $this->redirect($this->generateUrl('mikrotik_list', []));
		}

		$em->remove($internetOpen);
		$em->flush();

		$this->logAction("internetOff-ipList","ha disabilitato {$mikrotikList->getNome()}", ['id' => $mikrotikList->getId()]);

		$msg = new MikrotikMessage();
		$msg->setCommand('addIpToBypassedList');
		$messageBus->dispatch(new Envelope($msg));

		return $this->redirect($this->generateUrl('lab_internet_list', []));
	}

	/**
	 * @Route(path="/{mikrotikListId}/isOn", name="lab_internet_is_on", methods={"GET"})
	 */
	public function internetIsOnForIpListAction(EntityManagerInterface $em, $mikrotikListId)
	{
		$response = new Response();
		$response->headers->set('Content-Type', 'application/json');

		$mikrotikList = $em->getRepository(MikrotikList::class)->find($mikrotikListId);
		if(!$mikrotikList instanceof MikrotikList) {
			$response->setContent(json_encode(["on" => null, 'activationCompleted' => null, 'error' => 'Non è stato possibile trovare la lista indicata.']));
			return $response;
		}

		$internetOpen = $em->getRepository(InternetOpen::class)->findOneBy(['account' => $mikrotikListId, 'type' => "ipList"]);
		if(!$internetOpen instanceof InternetOpen) {
			$response->setContent(json_encode(["on" => false, 'activationCompleted' => null]));
		} else {
			$response->setContent(json_encode(["on" => true, 'activationCompleted' => $internetOpen->getActivationCompleated()]));
		}

		return $response;
	}

}
