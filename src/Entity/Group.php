<?php

namespace App\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use JMS\Serializer\Annotation as Serializer;
use Symfony\Component\Uid\Uuid;

/**
 * @ORM\Table(name="`Group`")
 * @ORM\Entity(repositoryClass="App\Repository\GroupRepository")
 */
class Group
{
    /**
     * @var string
     *
     * @ORM\Column(type="uuid", unique=true)
     * @ORM\Id
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(type="string")
     */
    private $name;

    /**
     * @var Provider
     *
     * @ORM\ManyToOne(targetEntity="Provider", inversedBy="groups")
     * Serializer\Groups({"group_provider"})
     * Serializer\MaxDepth(2)
     **/
    private $provider;

    /**
     * @var Student[]
     *
     * @ORM\ManyToMany(targetEntity="Student", mappedBy="memberOf")
     * @ORM\OrderBy({"lastname" = "ASC", "firstname" = "ASC"})
     * Serializer\Groups({"group_students"})
     * Serializer\MaxDepth(2)
     **/
    private $students;

    /**
     * @var bool
     * @ORM\Column(type="boolean", nullable=true)
     *
     **/
    private $manageManually = false;

    /**
     * @var int
     * @ORM\Column(type="integer", nullable=true)
     *
     **/
    private $radiusVlan = null;

    /**
     * @var Sector
     *
     * @ORM\ManyToOne(targetEntity="Sector", inversedBy="groups")
     * @ORM\JoinColumn(name="sector_id", referencedColumnName="id")
     * Serializer\Groups({"group_sector"})
     * Serializer\MaxDepth(2)
     **/
    private $sector;

    /**
     * @var ArrayCollection|TeacherSubjectGroup[]
     *
     * @ORM\OneToMany(targetEntity="TeacherSubjectGroup", mappedBy="group")
     * Serializer\Groups({"group_teacher_subject_group"})
     * Serializer\MaxDepth(2)
     **/
    private $teacherSubjectGroups;

    // @todo relazione a teacher passando da teachersubjectgroup

    /**
     * @var string
     * @ORM\Column(name="id_on_provider", type="string", nullable=true)
     */
    private $idOnProvider;

    /**
     * Constructor.
     */
    public function __construct()
    {
        $this->students = new \Doctrine\Common\Collections\ArrayCollection();
        $this->teacherSubjectGroups = new \Doctrine\Common\Collections\ArrayCollection();
        $this->id = Uuid::v4();
    }

    public function __toString()
    {
        return $this->name;
    }

    /**
     * @return string
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set name.
     *
     * @param string $name
     *
     * @return Group
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get name.
     *
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Set provider.
     *
     * @param Provider $provider
     *
     * @return Group
     */
    public function setProvider(Provider $provider = null)
    {
        $this->provider = $provider;

        return $this;
    }

    /**
     * Get provider.
     *
     * @return Provider
     */
    public function getProvider()
    {
        return $this->provider;
    }

    /**
     * Add member.
     *
     * @return Group
     */
    public function addStudent(Student $member)
    {
        $this->students->add($member);
        $member->addMemberOf($this);

        return $this;
    }

    /**
     * Remove member.
     */
    public function removeStudent(Student $member)
    {
        $this->students->removeElement($member);
        $member->removeMemberOf($this);
    }

    /**
     * Get members.
     *
     * @return Student[]|\Doctrine\Common\Collections\Collection
     */
    public function getStudents()
    {
        return $this->students;
    }

    /**
     * @return bool
     */
    public function getManageManually()
    {
        return $this->manageManually;
    }

    /**
     * @param bool $manageManually
     */
    public function setManageManually($manageManually)
    {
        $this->manageManually = (bool) $manageManually;
    }

    /**
     * @return mixed
     */
    public function getRadiusVlan()
    {
        return $this->radiusVlan;
    }

    /**
     * @param mixed $radiusVlan
     */
    public function setRadiusVlan($radiusVlan)
    {
        $this->radiusVlan = $radiusVlan;
    }

    /**
     * @return Sector
     */
    public function getSector()
    {
        return $this->sector;
    }

    /**
     * @param Sector|null $sector
     */
    public function setSector($sector)
    {
        $this->sector = $sector;
    }

    /**
     * @return ArrayCollection|TeacherSubjectGroup[]
     */
    public function getTeacherSubjectGroups()
    {
        return $this->teacherSubjectGroups;
    }

    public function setTeacherSubjectGroups(ArrayCollection $teacherSubjectGroups)
    {
        $this->teacherSubjectGroups = $teacherSubjectGroups;
    }

    /**
     * @return string
     */
    public function getIdOnProvider()
    {
        return $this->idOnProvider;
    }

    /**
     * @param string $idOnProvider
     */
    public function setIdOnProvider($idOnProvider)
    {
        $this->idOnProvider = $idOnProvider;
    }

    /**
     * @return ArrayCollection|Teacher[]
     */
    public function getTeachers()
    {
        $teacherSubjectGroups = $this->getTeacherSubjectGroups();
        $teachers = [];
        foreach ($teacherSubjectGroups as $teacherSubjectGroup) {
            $teacher = $teacherSubjectGroup->getTeacher();
            if (!$teacher instanceof Teacher || '' == $teacher->getId() || isset($teachers[(string)$teacher->getId()])) {
                continue;
            }

            $teachers[(string)$teacher->getId()] = $teacher;
        }

        return new \Doctrine\Common\Collections\ArrayCollection($teachers);
    }

    /**
     * @return ArrayCollection|Subject[]
     */
    public function getSubjects()
    {
        $teacherSubjectGroups = $this->getTeacherSubjectGroups();
        $subjects = [];
        foreach ($teacherSubjectGroups as $teacherSubjectGroup) {
            $subject = $teacherSubjectGroup->getSubject();
            if (!$subject instanceof Subject || '' == $subject->getId() || isset($subjects[(string)$subject->getId()])) {
                continue;
            }

            $subjects[(string)$subject->getId()] = $subject;
        }

        return new \Doctrine\Common\Collections\ArrayCollection($subjects);
    }

    public function isSame(\App\ImporterFilter\ImportedEntity\Group $importedGroup)
    {
        if ($importedGroup->getName() != $this->getName()) {
            return false;
        }

        if ($importedGroup->getSector() instanceof Sector &&
            (!$this->getSector() instanceof Sector || $importedGroup->getSector()->getId() != $this->getSector()->getId())) {
            return false;
        }

        return true;
    }

    public function addTeacherSubjectGroup(TeacherSubjectGroup $teacherSubjectGroup): self
    {
        if (!$this->teacherSubjectGroups->contains($teacherSubjectGroup)) {
            $this->teacherSubjectGroups[] = $teacherSubjectGroup;
            $teacherSubjectGroup->setGroup($this);
        }

        return $this;
    }

    public function removeTeacherSubjectGroup(TeacherSubjectGroup $teacherSubjectGroup): self
    {
        if ($this->teacherSubjectGroups->contains($teacherSubjectGroup)) {
            $this->teacherSubjectGroups->removeElement($teacherSubjectGroup);
            // set the owning side to null (unless already changed)
            if ($teacherSubjectGroup->getGroup() === $this) {
                $teacherSubjectGroup->setGroup(null);
            }
        }

        return $this;
    }

    public function isManageManually(): ?bool
    {
        return $this->manageManually;
    }
}
