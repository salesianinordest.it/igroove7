<?php


namespace App\Entity;

use Doctrine\DBAL\Types\Types;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;

use Symfony\Component\Uid\Uuid;

/**
 * @ORM\Entity(repositoryClass="App\Repository\MdmDeviceRepository")
 * @UniqueEntity("serial")
 *
 */
class MdmDevice
{

    /**
     * @ORM\Id
     * @ORM\Column(type="uuid", unique=true)
     */
    private $id;

    /**
     * @ORM\Column(type="string", nullable=false)
     */
    private $serial;

    /**
     * @ORM\Column(type="string", nullable=false)
     */
    private $mac;

    /**
     * @ORM\Column(type="string", nullable=true)
     */
    private $ownerUsername;

    /**
     * @ORM\Column(type="string", nullable=true)
     */
    private $name;

    /**
     * @ORM\Column(type="string", nullable=true)
     */
    private $model;

    /**
     * @ORM\Column(type="string", nullable=true)
     */
    private $osVersion;

    /**
     * @ORM\Column(type="boolean")
     */
    private $fromDep;

    /**
     * @ORM\Column(type="boolean")
     */
    private $supervised;

    /**
     * @ORM\Column(type="string", nullable=true)
     */
    private $activationBypassCode;

    /**
     * @ORM\Column(type="datetime", nullable=true)
     */
    private $lastInfoUpdate;

    /**
     * @ORM\Column(type="datetime", nullable=true)
     */
    private $lastUpdate;

    /**
     * @ORM\Column(type="boolean", nullable=false)
     */
    protected $removed = false;

    /**
     * @ORM\ManyToOne(targetEntity="MdmServer", inversedBy="devices")
     **/
    private $mdmServer;

    /**
     * @ORM\Column(type="string", nullable=false)
     */
    private $idOnMdmServer;

    /**
     * @ORM\ManyToOne(targetEntity="Student", inversedBy="mdmDevices")
     * @ORM\JoinColumn(nullable=true, onDelete="SET NULL")
     * @var Student|null
     */
    protected $studentOwner;

    /**
     * @ORM\ManyToOne(targetEntity="Teacher", inversedBy="mdmDevices")
     * @ORM\JoinColumn(nullable=true, onDelete="SET NULL")
     * @var Teacher|null
     */
    protected $teacherOwner;

    public function __construct()
    {
        $this->id = Uuid::v4();
    }

    /**
     * @param \App\MdmConnector\ImportedEntity\Device $data
     * @throws \Exception
     */
    public function importData(\App\MdmConnector\ImportedEntity\Device $data)
    {
        $dataToCopy = ['serial', 'mac', 'ownerUsername', 'name', 'model', 'osVersion', 'fromDep', 'supervised', 'activationBypassCode', 'lastInfoUpdate', 'lastUpdate', 'studentOwner', 'teacherOwner'];

        foreach ($dataToCopy as $k) {
            $getter = "get" . ucfirst($k);
            if (!method_exists($data, $getter) || !property_exists($this, $k)) {
                throw new \Exception("Invalid property to import in MdmBook");
            }

            $this->$k = $data->$getter();
        }

        $this->idOnMdmServer = $data->getId();
    }

    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @return string
     */
    public function getSerial()
    {
        return $this->serial;
    }

    /**
     * @param string $serial
     */
    public function setSerial($serial)
    {
        $this->serial = $serial;
    }

    /**
     * @return string
     */
    public function getMac()
    {
        return $this->mac;
    }

    /**
     * @param string $mac
     */
    public function setMac($mac)
    {
        $this->mac = $mac;
    }

    /**
     * @return string
     */
    public function getOwnerUsername()
    {
        return $this->ownerUsername;
    }

    /**
     * @param string $ownerUsername
     */
    public function setOwnerUsername($ownerUsername)
    {
        $this->ownerUsername = $ownerUsername;
    }

    /**
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * @param string $name
     */
    public function setName($name)
    {
        $this->name = $name;
    }

    /**
     * @return string
     */
    public function getModel()
    {
        return $this->model;
    }

    /**
     * @param string $model
     */
    public function setModel($model)
    {
        $this->model = $model;
    }

    /**
     * @return string
     */
    public function getOsVersion()
    {
        return $this->osVersion;
    }

    /**
     * @param string $osVersion
     */
    public function setOsVersion($osVersion)
    {
        $this->osVersion = $osVersion;
    }

    /**
     * @return boolean
     */
    public function getFromDep()
    {
        return $this->fromDep;
    }

    /**
     * @param boolean $fromDep
     */
    public function setFromDep($fromDep)
    {
        $this->fromDep = $fromDep;
    }

    /**
     * @return boolean
     */
    public function getSupervised()
    {
        return $this->supervised;
    }

    /**
     * @param boolean $supervised
     */
    public function setSupervised($supervised)
    {
        $this->supervised = $supervised;
    }

    /**
     * @return string
     */
    public function getActivationBypassCode()
    {
        return $this->activationBypassCode;
    }

    /**
     * @param string $activationBypassCode
     */
    public function setActivationBypassCode($activationBypassCode)
    {
        $this->activationBypassCode = $activationBypassCode;
    }

    /**
     * @return \DateTime
     */
    public function getLastInfoUpdate()
    {
        return $this->lastInfoUpdate;
    }

    /**
     * @param \DateTime $lastInfoUpdate
     */
    public function setLastInfoUpdate($lastInfoUpdate)
    {
        $this->lastInfoUpdate = $lastInfoUpdate;
    }

    /**
     * @return \DateTime
     */
    public function getLastUpdate()
    {
        return $this->lastUpdate;
    }

    /**
     * @param \DateTime $lastUpdate
     */
    public function setLastUpdate($lastUpdate)
    {
        $this->lastUpdate = $lastUpdate;
    }

    /**
     * @return bool
     */
    public function getRemoved()
    {
        return $this->removed;
    }

    /**
     * @param bool $removed
     */
    public function setRemoved($removed)
    {
        $this->removed = $removed;
    }

    /**
     * @return MdmServer
     */
    public function getMdmServer()
    {
        return $this->mdmServer;
    }

    /**
     * @param MdmServer $mdmServer
     */
    public function setMdmServer($mdmServer)
    {
        $this->mdmServer = $mdmServer;
    }

    /**
     * @return string
     */
    public function getIdOnMdmServer()
    {
        return $this->idOnMdmServer;
    }

    /**
     * @param string $idOnMdmServer
     */
    public function setIdOnMdmServer($idOnMdmServer)
    {
        $this->idOnMdmServer = $idOnMdmServer;
    }

    /**
     * @return PersonAbstract|null
     */
    public function getOwner()
    {
        return $this->studentOwner ?? $this->teacherOwner;
    }

    /**
     * @return Student|null
     */
    public function getStudentOwner(): ?Student
    {
        return $this->studentOwner;
    }

    /**
     * @return Teacher|null
     */
    public function getTeacherOwner(): ?Teacher
    {
        return $this->teacherOwner;
    }

    public function isFromDep(): ?bool
    {
        return $this->fromDep;
    }

    public function isSupervised(): ?bool
    {
        return $this->supervised;
    }

    public function isRemoved(): ?bool
    {
        return $this->removed;
    }

    public function setStudentOwner(?Student $studentOwner): static
    {
        $this->studentOwner = $studentOwner;

        return $this;
    }

    public function setTeacherOwner(?Teacher $teacherOwner): static
    {
        $this->teacherOwner = $teacherOwner;

        return $this;
    }


}