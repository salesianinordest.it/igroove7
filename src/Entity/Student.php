<?php

namespace App\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\Common\Collections\Criteria;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;

/**
 * @ORM\Entity
 * @ORM\Table(name="Student")
 * @ORM\Entity(repositoryClass="App\Repository\StudentRepository")
 * @UniqueEntity("fiscalCode")
 * @UniqueEntity("username")
 * @UniqueEntity("email")
 */
class Student extends PersonAbstract
{
    /**
     * @ORM\ManyToMany(targetEntity="Group", inversedBy="students")
     * @ORM\JoinTable(name="students_groups",
     *  joinColumns={@ORM\JoinColumn(name="student_id", referencedColumnName="id")},
     *  inverseJoinColumns={@ORM\JoinColumn(name="group_id", referencedColumnName="id")}
     *      )
     * Serializer\MaxDepth(3)
     * Serializer\Groups({"student_groups"})
     * Serializer\SerializedName("groups")
     **/
    protected $memberOf;

    /**
     * Student constructor.
     *
     * @param $id
     */
    public function __construct($fiscalCode)
    {
        $this->memberOf = new \Doctrine\Common\Collections\ArrayCollection();
        parent::__construct($fiscalCode);
    }

    public function __clone()
    {
        parent::__clone();
        $this->memberOf = clone $this->memberOf;
    }

    /**
     * Add memberOf.
     *
     * @param \App\Entity\Group $memberOf
     *
     * @return PersonAbstract
     */
    public function addMemberOf(Group $memberOf)
    {
        if (!$this->isMemberOf($memberOf)) {
            $this->memberOf->add($memberOf);
        }

        return $this;
    }

    /**
     * Remove memberOf.
     *
     * @param \App\Entity\Group $memberOf
     */
    public function removeMemberOf(Group $memberOf)
    {
        $this->memberOf->removeElement($memberOf);
    }

    public function removeAllMemberOf()
    {
        foreach ($this->memberOf as $group) {
            $group->removeStudent($this);
        }
        $this->memberOf = new \Doctrine\Common\Collections\ArrayCollection();
    }

    /**
     * Get memberOf.
     *
     * @return Group[]|\Doctrine\Common\Collections\Collection
     */
    public function getMemberOf()
    {
        return $this->memberOf;
    }

    /**
     * @return Group[]|\Doctrine\Common\Collections\Collection
     */
    public function getGroups()
    {
        return $this->memberOf;
    }

    /**
     * @return Group[]|\Doctrine\Common\Collections\Collection
     */
    public function getNotManuallyManagedGroups()
    {
        $criteria = Criteria::create();
        $criteria->where(Criteria::expr()->eq('manageManually', false));

        return $this->memberOf->matching($criteria);
    }

    /**
     * @return Group[]|\Doctrine\Common\Collections\Collection|null
     */
    public function getNotManuallyManagedGroupOfProvider(Provider $provider)
    {
        $criteria = Criteria::create();
        $criteria->where(Criteria::expr()->andX(
            // @todo: sf34 aggiungere criterio provider_id ---> perchè non funziona come prima?
            Criteria::expr()->eq('manageManually', false),
//            Criteria::expr()->eq('provider_id', $provider->getId())
        ));

        $groups = $this->memberOf->matching($criteria);
        foreach ($groups as $group) {
            if ($group->getProvider()->getId() == $provider->getId()) {
                return $group;
            }
        }

        return null;
//        return isset($groups[0]) ? $groups[0] : null;
    }

    /**
     * @return Group[]|\Doctrine\Common\Collections\Collection
     */
    public function getManuallyManagedGroups()
    {
        $criteria = Criteria::create();
        $criteria->where(Criteria::expr()->eq('manageManually', true));

        return $this->memberOf->matching($criteria);
    }

    /**
     * check membership.
     *
     * @return bool
     */
    public function isMemberOf($groupToTest)
    {
        foreach ($this->memberOf as $group) {
            if ($group == $groupToTest) {
                return true;
            }
        }

        return false;
    }

    /**
     * @return bool
     */
    public function isSame(\App\ImporterFilter\ImportedEntity\Student $importedStudent, Provider $provider = null)
    {
        if (strtolower($this->fiscalCode) != strtolower($importedStudent->getFiscalCode())) {
            return false;
        }

        if ($this->firstname != $importedStudent->getFirstName()) {
            return false;
        }

        if ($this->lastname != $importedStudent->getLastName()) {
            return false;
        }

        if (null !== $importedStudent->getEmail() && strtolower($this->email) != strtolower($importedStudent->getEmail())) {
            return false;
        }

        if (null !== $importedStudent->getPassword() && $this->starting_password != $importedStudent->getPassword()) {
            return false;
        }
        if (null !== $importedStudent->getUsername() && strtolower($this->username) != strtolower($importedStudent->getUsername())) {
            return false;
        }

        if (null === $provider) {
            $provider = $this->getProvider();
        }

        $group = $this->getNotManuallyManagedGroupOfProvider($provider);
        if (null !== $importedStudent->getGroup()) {
            if (null === $group || $group->getId() != $importedStudent->getGroup()->getId()) {
                return false;
            }
        } elseif (null !== $group) {
            return false;
        }

        return true;
    }

    /**
     * @return bool
     */
    public function haveSameGroup(\App\ImporterFilter\ImportedEntity\Student $importedStudent, Provider $provider = null)
    {
        $group = $this->getNotManuallyManagedGroupOfProvider($provider);
        if (null !== $importedStudent->getGroup()) {
            if (null === $group || $group->getId() != $importedStudent->getGroup()->getId()) {
                return false;
            }
        } elseif (null !== $group) {
            return false;
        }

        return true;
    }

    /**
     * @return array
     */
    public function getProviderSettings()
    {
        if (!$this->provider instanceof Provider) {
            return Provider::$settingsFieldNulled;
        }

        return $this->provider->getStudentSettings();
    }
}
