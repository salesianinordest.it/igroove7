<?php

namespace App\Entity;

use Doctrine\DBAL\Types\Types;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity
 */
class ConfigurationStorage
{
    /**
     * @ORM\Id
     * @ORM\Column(name="`key`",type="string")
     */
    private $key;

    /**
     * @ORM\Column (name="`value`",type="text", nullable=true)
     */
    private $value;

    /**
     * Set key.
     *
     * @param string $key
     *
     * @return ConfigurationStorage
     */
    public function setKey($key)
    {
        $this->key = $key;

        return $this;
    }

    /**
     * Get key.
     *
     * @return string
     */
    public function getKey()
    {
        return $this->key;
    }

    /**
     * Set value.
     *
     * @param string $value
     *
     * @return ConfigurationStorage
     */
    public function setValue($value)
    {
        $this->value = $value;

        return $this;
    }

    /**
     * Get value.
     *
     * @return string
     */
    public function getValue()
    {
        return $this->value;
    }
}
