<?php

namespace App\Entity;

use Doctrine\DBAL\Types\Types;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Uid\Uuid;

/**
 * @ORM\Entity
 * @ORM\Entity(repositoryClass="App\Repository\CronRepository")
 * @ORM\HasLifecycleCallbacks
 */
class Cron
{
    /**
     * @ORM\Column(type="uuid", unique=true)
     * @ORM\Id
     */
    private $id;

    /**
     * @ORM\Column(type="string", nullable=true)
     */
    private $script;

    /**
     * @ORM\Column(type="datetime", nullable=true)
     */
    private $latestRun;

    /**
     * @ORM\Column(type="datetime", nullable=true)
     */
    private $updated_at;

    /**
     * @ORM\Column(type="datetime", nullable=true)
     */
    private $created_at;

    public function __construct()
    {
        $this->id = Uuid::v4();
    }

    /**
     * @ORM\PrePersist
     */
    public function setCreatedAtValue()
    {
        $this->created_at = new \DateTime();
    }

    /**
     * @ORM\PreUpdate
     */
    public function setUpdatedAtValue()
    {
        $this->updated_at = new \DateTime();
    }

    public function getId()
    {
        return $this->id;
    }

    /**
     * Set script.
     *
     * @param string $script
     *
     * @return Cron
     */
    public function setScript($script)
    {
        $this->script = $script;

        return $this;
    }

    /**
     * Get script.
     *
     * @return string
     */
    public function getScript()
    {
        return $this->script;
    }

    /**
     * Set latestRun.
     *
     * @param \DateTime $latestRun
     *
     * @return Cron
     */
    public function setLatestRun($latestRun)
    {
        $this->latestRun = $latestRun;

        return $this;
    }

    /**
     * Get latestRun.
     *
     * @return \DateTime
     */
    public function getLatestRun()
    {
        return $this->latestRun;
    }

    /**
     * Set updated_at.
     *
     * @param \DateTime $updatedAt
     *
     * @return Cron
     */
    public function setUpdatedAt($updatedAt)
    {
        $this->updated_at = $updatedAt;

        return $this;
    }

    /**
     * Get updated_at.
     *
     * @return \DateTime
     */
    public function getUpdatedAt()
    {
        return $this->updated_at;
    }

    /**
     * Set created_at.
     *
     * @param \DateTime $createdAt
     *
     * @return Cron
     */
    public function setCreatedAt($createdAt)
    {
        $this->created_at = $createdAt;

        return $this;
    }

    /**
     * Get created_at.
     *
     * @return \DateTime
     */
    public function getCreatedAt()
    {
        return $this->created_at;
    }
}
