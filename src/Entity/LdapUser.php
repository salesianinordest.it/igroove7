<?php

namespace App\Entity;

use App\Manager\ConfigurationManager;
use Doctrine\DBAL\Types\Types;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity
 * @ORM\Entity(repositoryClass="App\Repository\LdapUserRepository")
 */
class LdapUser
{
    /**
     * @ORM\Id
     * @ORM\Column(type="string", nullable=true)
     */
    private $username;

    /**
     * @ORM\Column(type="string", nullable=true)
     */
    private $distinguishedId;

    /**
     * @ORM\Column(type="string", nullable=true)
     */
    private $operation;

    /**
     * @ORM\Column(type="string", nullable=true)
     */
    private $dn;

    /**
     * @ORM\Column(type="string", nullable=true)
     */
    private $lastname;

    /**
     * @ORM\Column(type="string", nullable=true)
     */
    private $firstname;

    /**
     * @ORM\Column(type="text", nullable=true)
     */
    private $attributes;

    /**
     * @ORM\Column(type="string", nullable=true)
     */
    private $googleId;

    /**
     * @ORM\Column(type="string", nullable=true)
     */
    private $starting_password;

    public function getMemberOf($em)
    {
        $membership = [];
        $groups = $em->getRepository('App\Entity\LdapGroup')->findAll();
        foreach ($groups as $group) {
            $g = $group->getMembersList();
            if (array_key_exists('user', $g) && is_array($g['user']) && in_array($this->getUsername(), $g['user'])) {
                $membership[] = $group->getName();
            }
        }
        $membership = array_unique($membership);

        return $membership;
    }

    /* public function setChanges(Student $student, ConfigurationManager $configurationManager)
     {
         if ($student->getEmail() != "") {
             $email = $student->getEmail();
         } else {
             $provider = $student->getProvider();
             if ((strlen($provider->getStudentGoogleAppClientId()) == 0) or !$provider->getStudentGoogleAppAutoEmail()) {
                 $email = 'nessuna@email';
             } else {
                 $email = $this->username . '@' . $provider->getStudentGoogleAppDomain();
             }
         }

         $currentAttributes = (array)json_decode($this->attributes);
         $attributes = [
             "description" => "Creato da igroove",
             "homedrive" => (strlen($configurationManager->getActiveDirectoryHomeDrive()) == 1) ? strtoupper(
                 $configurationManager->getActiveDirectoryHomeDrive() . ':'
             ) : null,
             "homedirectory" => (strlen(
                     $configurationManager->getActiveDirectoryHomeFolder()
                 ) > 2) ? $configurationManager->getActiveDirectoryHomeFolder() . '\\' . $this->getUsername() : null,
             'email' => $email,
             'radius-vlan' => ''
         ];

         $mainClassroom = $student->getNotManuallyManagedGroups()->first();
         if($mainClassroom instanceof Group && (int)$mainClassroom->getRadiusVlan() > 0) {
             $attributes['radius-vlan'] = (int)$mainClassroom->getRadiusVlan();
         }

         if (
             $this->firstname == $student->getFirstname() &&
             $this->lastname == $student->getLastname() &&
             $currentAttributes == $attributes
         ) {
             return false;
         } else {
             $this->firstname = $student->getFirstname();
             $this->lastname = $student->getLastname();
             $this->attributes = json_encode($attributes);
             if ($this->operation != 'CREATE') {
                 $this->operation = "MODIFY";
             }

             return true;
         }
     }*/

    public function getEmail()
    {
        $attributes = json_decode($this->attributes, true);

        return @$attributes['email'];
    }

    /**
     * Set username.
     *
     * @param string $username
     *
     * @return LdapUser
     */
    public function setUsername($username)
    {
        $this->username = $username;

        return $this;
    }

    /**
     * Get username.
     *
     * @return string
     */
    public function getUsername()
    {
        return $this->username;
    }

    /**
     * Set distinguishedId.
     *
     * @param string $distinguishedId
     *
     * @return LdapUser
     */
    public function setDistinguishedId($distinguishedId)
    {
        $this->distinguishedId = $distinguishedId;

        return $this;
    }

    /**
     * Get distinguishedId.
     *
     * @return string
     */
    public function getDistinguishedId()
    {
        return $this->distinguishedId;
    }

    /**
     * Set operation.
     *
     * @param string $operation
     *
     * @return LdapUser
     */
    public function setOperation($operation)
    {
        $this->operation = $operation;

        return $this;
    }

    /**
     * Get operation.
     *
     * @return string
     */
    public function getOperation()
    {
        return $this->operation;
    }

    /**
     * Set dn.
     *
     * @param string $dn
     *
     * @return LdapUser
     */
    public function setDn($dn)
    {
        $this->dn = $dn;

        return $this;
    }

    /**
     * Get dn.
     *
     * @return string
     */
    public function getDn()
    {
        return $this->dn;
    }

    /**
     * Set lastname.
     *
     * @param string $lastname
     *
     * @return LdapUser
     */
    public function setLastname($lastname)
    {
        $this->lastname = $lastname;

        return $this;
    }

    /**
     * Get lastname.
     *
     * @return string
     */
    public function getLastname()
    {
        return $this->lastname;
    }

    /**
     * Set firstname.
     *
     * @param string $firstname
     *
     * @return LdapUser
     */
    public function setFirstname($firstname)
    {
        $this->firstname = $firstname;

        return $this;
    }

    /**
     * Get firstname.
     *
     * @return string
     */
    public function getFirstname()
    {
        return $this->firstname;
    }

    /**
     * Set attributes.
     *
     * @param string $attributes
     *
     * @return LdapUser
     */
    public function setAttributes($attributes)
    {
        $this->attributes = $attributes;

        return $this;
    }

    /**
     * Set a single attribute for the user.
     *
     * @param $key
     * @param $value
     */
    public function setAttribute($key, $value)
    {
        $attributes = (array) json_decode($this->attributes);
        $attributes[$key] = $value;
        $this->attributes = json_encode($attributes);
    }

    /**
     * Get attributes.
     *
     * @return string
     */
    public function getAttributes()
    {
        return $this->attributes;
    }

	/**
	 * Get a single attribute value
	 *
	 * @param $key
	 * @return mixed|null
	 */
	public function getAttribute($key)
   	{
   		$attributes = json_decode($this->attributes, true);
   
   		return $attributes[$key] ?? null;
   	}

	/**
	 * Get a single attribute value
	 *
	 * @param $key
	 * @return bool
	 */
	public function hasAttribute($key)
   	{
   		$attributes = json_decode($this->attributes, true);
   
   		return isset($attributes[$key]);
   	}

    /**
     * Set startingPassword.
     *
     * @param string $startingPassword
     *
     * @return LdapUser
     */
    public function setStartingPassword($startingPassword)
    {
        $this->starting_password = $startingPassword;

        return $this;
    }

    /**
     * Get startingPassword.
     *
     * @return string
     */
    public function getStartingPassword()
    {
        return $this->starting_password;
    }

    public function getGoogleId(): ?string
    {
        return $this->googleId;
    }

    public function setGoogleId(?string $googleId): self
    {
        $this->googleId = $googleId;

        return $this;
    }

	/**
	 * @return bool
	 */
	public function isCreationFailed()
   	{
   		$objectGUID = $this->getAttribute('objectGUID');
   		return $this->operation == "ERROR" && ($objectGUID == null || $objectGUID == "");
   	}

//    public function checkPassword(Student $student, ConfigurationManager $configurationManager)
//    {
//
//        $provider = $student->getProvider();
//        if ($provider->getStudentForceImportedPassword()) {
//            $this->starting_password = $student->getStartingPassword();
//        }
//
//        elseif (strlen(trim($student->getStartingPassword())) == 0
//        ) {
//            $this->starting_password = $this->getRandomPassword($configurationManager);
//            $student->setStartingPassword($this->starting_password);
// //            $this->starting_password = $this->starting_password;
//        }
//        elseif ($this->operation == 'CREATE') {
//            $this->starting_password=$student->getStartingPassword();
//        } else {
//            return false;
//        }
//        if ($this->operation != 'CREATE') {
//            $this->operation = "MODIFY";
//        }
//
//        return true;
//    }
//
//
//    private function getRandomPassword($configurationManager)
//    {
//        $password = '';
//        if ($configurationManager->getActiveDirectoryPasswordComplexity() == true) {
//            $password = 'Az-';
//        }
//        $len = $configurationManager->getActiveDirectoryPasswordMinChar() - strlen($password);
//        $password .= rand(pow(10, $len - 1), pow(10, $len) - 1);
//
//        return $password;
//    }
}
