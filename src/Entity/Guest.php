<?php

namespace App\Entity;

use Doctrine\DBAL\Types\Types;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity
 * @ORM\Entity(repositoryClass="App\Repository\GuestRepository")
 */
class Guest
{
    /**
     * @ORM\Id
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @ORM\Column(type="string", nullable=true)
     */
    private $lastname;

    /**
     * @ORM\Column(type="string", nullable=true)
     */
    private $firstname;

    /**
     * @ORM\Column(type="string", nullable=true)
     */
    private $password;

    /**
     * @ORM\Column(type="string", nullable=true)
     */
    private $identified_by;

    /**
     * @ORM\Column(type="date", nullable=true)
     */
    private $starting_from;

    /**
     * @ORM\Column(type="date", nullable=true)
     */
    private $ending_to;

    /**
     * Set password.
     */
    public function setPassword()
    {
        $password = rand(1111, 9999);
        $this->password = $password;
    }

    /**
     * Get id.
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set lastname.
     *
     * @param string $lastname
     *
     * @return Guest
     */
    public function setLastname($lastname)
    {
        $this->lastname = $lastname;

        return $this;
    }

    /**
     * Get lastname.
     *
     * @return string
     */
    public function getLastname()
    {
        return $this->lastname;
    }

    /**
     * Set firstname.
     *
     * @param string $firstname
     *
     * @return Guest
     */
    public function setFirstname($firstname)
    {
        $this->firstname = $firstname;

        return $this;
    }

    /**
     * Get firstname.
     *
     * @return string
     */
    public function getFirstname()
    {
        return $this->firstname;
    }

    /**
     * Get password.
     *
     * @return string
     */
    public function getPassword()
    {
        return $this->password;
    }

    /**
     * Set identifiedBy.
     *
     * @param string $identifiedBy
     *
     * @return Guest
     */
    public function setIdentifiedBy($identifiedBy)
    {
        $this->identified_by = $identifiedBy;

        return $this;
    }

    /**
     * Get identifiedBy.
     *
     * @return string
     */
    public function getIdentifiedBy()
    {
        return $this->identified_by;
    }

    /**
     * Set startingFrom.
     *
     * @param \DateTime $startingFrom
     *
     * @return Guest
     */
    public function setStartingFrom($startingFrom)
    {
        $this->starting_from = $startingFrom;

        return $this;
    }

    /**
     * Get startingFrom.
     *
     * @return \DateTime
     */
    public function getStartingFrom()
    {
        return $this->starting_from;
    }

    /**
     * Set endingTo.
     *
     * @param \DateTime $endingTo
     *
     * @return Guest
     */
    public function setEndingTo($endingTo)
    {
        $this->ending_to = $endingTo;

        return $this;
    }

    /**
     * Get endingTo.
     *
     * @return \DateTime
     */
    public function getEndingTo()
    {
        return $this->ending_to;
    }
}
