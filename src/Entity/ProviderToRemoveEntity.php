<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;
use JMS\Serializer\Annotation as Serializer;
use Symfony\Component\Uid\Uuid;

/**
 * @ORM\Entity
 */
class ProviderToRemoveEntity
{
    /**
     * @var string
     *
     * @ORM\Column(type="uuid", unique=true)
     * @ORM\Id
     */
    private $id;

    /**
     * @var Provider
     *
     * @ORM\ManyToOne(targetEntity="Provider", inversedBy="groups")
     * Serializer\MaxDepth(2)
     **/
    private $provider;

    /**
     * @var string
     * @ORM\Column(name="entity_type", type="string", nullable=true)
     */
    private $entityType;

    /**
     * @var string
     * @ORM\Column(name="entity_id", type="string", nullable=true)
     */
    private $entityId;

    public function __construct()
    {
        $this->id = Uuid::v4();
    }

    public function getId(): string
    {
        return $this->id;
    }

    public function getProvider(): Provider
    {
        return $this->provider;
    }

    public function setProvider(Provider $provider): ProviderToRemoveEntity
    {
        $this->provider = $provider;

        return $this;
    }

    public function getEntityType(): string
    {
        return $this->entityType;
    }

    public function setEntityType(string $entityType): ProviderToRemoveEntity
    {
        $this->entityType = $entityType;

        return $this;
    }

    public function getEntityId(): string
    {
        return $this->entityId;
    }

    public function setEntityId(string $entityId): ProviderToRemoveEntity
    {
        $this->entityId = $entityId;

        return $this;
    }
}
