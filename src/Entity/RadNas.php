<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Radreply.
 *
 * @ORM\Table(name="nas", indexes={@ORM\Index(name="nas_nasname", columns={"nasname"})})
 * @ORM\Entity
 */
class RadNas
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="nasname", type="string", length=128, nullable=false)
     */
    private $nasname = '';

    /**
     * @var string
     *
     * @ORM\Column(name="shortname", type="string", length=32, nullable=true)
     */
    private $shortname = '';

    /**
     * @var string
     *
     * @ORM\Column(name="type", type="string", length=30, nullable=false)
     */
    private $type = 'other';

    /**
     * @var string
     *
     * @ORM\Column(name="ports", type="integer", length=5, nullable=true)
     */
    private $ports;

    /**
     * @var string
     *
     * @ORM\Column(name="secret", type="string", length=60, nullable=false)
     */
    private $secret = 'secret';

    /**
     * @var string
     *
     * @ORM\Column(name="server", type="string", length=60, nullable=true)
     */
    private $server;

    /**
     * @var string
     *
     * @ORM\Column(name="community", type="string", length=50, nullable=true)
     */
    private $community;

    /**
     * @var string
     *
     * @ORM\Column(name="description", type="string", length=200, nullable=true)
     */
    private $description = 'RADIUS Client';

    /**
     * Get id.
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    public function getNasname(): string
    {
        return $this->nasname;
    }

    public function setNasname(string $nasname): RadNas
    {
        $this->nasname = $nasname;

        return $this;
    }

    public function getShortname(): string
    {
        return $this->shortname;
    }

    public function setShortname(string $shortname): RadNas
    {
        $this->shortname = $shortname;

        return $this;
    }

    public function getType(): string
    {
        return $this->type;
    }

    public function setType(string $type): RadNas
    {
        $this->type = $type;

        return $this;
    }

    public function getPorts(): string
    {
        return $this->ports;
    }

    public function setPorts(string $ports): RadNas
    {
        $this->ports = $ports;

        return $this;
    }

    public function getSecret(): string
    {
        return $this->secret;
    }

    public function setSecret(string $secret): RadNas
    {
        $this->secret = $secret;

        return $this;
    }

    public function getServer(): string
    {
        return $this->server;
    }

    public function setServer(string $server): RadNas
    {
        $this->server = $server;

        return $this;
    }

    public function getCommunity(): string
    {
        return $this->community;
    }

    public function setCommunity(string $community): RadNas
    {
        $this->community = $community;

        return $this;
    }

    public function getDescription(): string
    {
        return $this->description;
    }

    public function setDescription(string $description): RadNas
    {
        $this->description = $description;

        return $this;
    }
}
