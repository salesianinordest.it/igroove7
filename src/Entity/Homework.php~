<?php

namespace Zen\IgrooveBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity
 * @ORM\Entity(repositoryClass="Zen\IgrooveBundle\Repository\HomeworkRepository")
 */
class Homework
{
    /**
     * @ORM\Column(type="guid")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="UUID")
     */
    private $id;

    /**
     * @ORM\Column(type="string")
     */
    private $teacherUsername;

    /**
     * @ORM\Column(type="string")
     */
    private $rootGroup;

    /**
     * @ORM\Column(type="string")
     */
    private $subGroup;

    /**
     * @ORM\Column(type="string")
     */
    private $title;

    /**
     * @ORM\Column(type="datetime")
     */
    private $startingDate;

    /**
     * @ORM\Column(type="datetime")
     */
    private $closingDate;



    /**
     * Get id
     *
     * @return guid
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set teacherUsername
     *
     * @param string $teacherUsername
     *
     * @return Homework
     */
    public function setTeacherUsername($teacherUsername)
    {
        $this->teacherUsername = $teacherUsername;

        return $this;
    }

    /**
     * Get teacherUsername
     *
     * @return string
     */
    public function getTeacherUsername()
    {
        return $this->teacherUsername;
    }

    /**
     * Set rootGroup
     *
     * @param string $rootGroup
     *
     * @return Homework
     */
    public function setRootGroup($rootGroup)
    {
        $this->rootGroup = $rootGroup;

        return $this;
    }

    /**
     * Get rootGroup
     *
     * @return string
     */
    public function getRootGroup()
    {
        return $this->rootGroup;
    }

    /**
     * Set subGroup
     *
     * @param string $subGroup
     *
     * @return Homework
     */
    public function setSubGroup($subGroup)
    {
        $this->subGroup = $subGroup;

        return $this;
    }

    /**
     * Get subGroup
     *
     * @return string
     */
    public function getSubGroup()
    {
        return $this->subGroup;
    }

    /**
     * Set title
     *
     * @param string $title
     *
     * @return Homework
     */
    public function setTitle($title)
    {
        $this->title = $title;

        return $this;
    }

    /**
     * Get title
     *
     * @return string
     */
    public function getTitle()
    {
        return $this->title;
    }

    /**
     * Set startingDate
     *
     * @param \DateTime $startingDate
     *
     * @return Homework
     */
    public function setStartingDate($startingDate)
    {
        $this->startingDate = $startingDate;

        return $this;
    }

    /**
     * Get startingDate
     *
     * @return \DateTime
     */
    public function getStartingDate()
    {
        return $this->startingDate;
    }

    /**
     * Set closingDate
     *
     * @param \DateTime $closingDate
     *
     * @return Homework
     */
    public function setClosingDate($closingDate)
    {
        $this->closingDate = $closingDate;

        return $this;
    }

    /**
     * Get closingDate
     *
     * @return \DateTime
     */
    public function getClosingDate()
    {
        return $this->closingDate;
    }
}
