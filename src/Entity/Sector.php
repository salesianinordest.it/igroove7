<?php

namespace App\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use JMS\Serializer\Annotation as Serializer;
use Symfony\Component\Uid\Uuid;

/**
 * Sector.
 *
 * @ORM\Table()
 * @ORM\Entity
 * @ORM\Entity(repositoryClass="App\Repository\SectorRepository")
 */
class Sector
{
    /**
     * @var string
     *
     * @ORM\Column(type="uuid", unique=true)
     * @ORM\Id
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="name", type="string", length=255)
     */
    private $name;

    /**
     * @var Group[]
     *
     * @ORM\OneToMany(targetEntity="Group", mappedBy="sector")
     * Serializer\Groups({"sector_groups"})
     * Serializer\MaxDepth(3)
     *
     **/
    private $groups;

    /**
     * @var Provider
     *
     * @ORM\ManyToOne(targetEntity="Provider", inversedBy="sectors")
     * Serializer\Groups({"sector_provider"})
     * Serializer\MaxDepth(2)
     **/
    private $provider;

    /**
     * @var string
     * @ORM\Column(name="id_on_provider", type="integer", nullable=true)
     */
    private $idOnProvider;

    public function __construct()
    {
        $this->groups = new ArrayCollection();
        $this->id = Uuid::v4();
    }

    /**
     * Get id.
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set name.
     *
     * @param string $name
     *
     * @return Sector
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get name.
     *
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * @return mixed
     */
    public function getGroups()
    {
        return $this->groups;
    }

    /**
     * @param mixed $groups
     */
    public function setGroups($groups)
    {
        $this->groups = $groups;
    }

    /**
     * @return Provider
     */
    public function getProvider()
    {
        return $this->provider;
    }

    /**
     * @param Provider $provider
     */
    public function setProvider($provider)
    {
        $this->provider = $provider;
    }

    /**
     * @return string
     */
    public function getIdOnProvider()
    {
        return $this->idOnProvider;
    }

    /**
     * @param string $idOnProvider
     */
    public function setIdOnProvider($idOnProvider)
    {
        $this->idOnProvider = $idOnProvider;
    }

    public function isSame(\App\ImporterFilter\ImportedEntity\Sector $importedSector)
    {
        if ($importedSector->getName() != $this->getName()) {
            return false;
        }

        return true;
    }

    public function __clone()
    {
        $this->groups = clone $this->groups;
    }

    public function addGroup(Group $group): self
    {
        if (!$this->groups->contains($group)) {
            $this->groups[] = $group;
            $group->setSector($this);
        }

        return $this;
    }

    public function removeGroup(Group $group): self
    {
        if ($this->groups->contains($group)) {
            $this->groups->removeElement($group);
            // set the owning side to null (unless already changed)
            if ($group->getSector() === $this) {
                $group->setSector(null);
            }
        }

        return $this;
    }
}
