<?php

namespace App\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;
use Symfony\Component\Uid\Uuid;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * @ORM\Entity
 * @ORM\Entity(repositoryClass="App\Repository\DeviceRepository")
 * @UniqueEntity("mac")
 */
class Device
{
    /**
     * @ORM\Column(type="uuid", unique=true)
     * @ORM\Id
     */
    private $id;

    /**
     * @ORM\Column(type="string", nullable=true)
     */
    private $device;

    /**
     * @ORM\Column(type="string", nullable=false)
     * @Assert\Regex("/^([a-fA-F0-9]{2}[:|\-]?){6}$/")
     */
    private $mac;

    /**
     * @ORM\ManyToOne(targetEntity="MikrotikList", inversedBy="macs")
     * @ORM\OrderBy({"nome" = "ASC"})
     **/
    private $mikrotikList;

    /**
     * @ORM\OneToMany(targetEntity="DeviceIp", mappedBy="mac", cascade={"persist"})
     * @Assert\Valid
     */
	protected Collection $ips;

    /**
     * @ORM\Column(type="boolean", nullable=true)
     */
    private $bypassHotspot;

    /**
     * @ORM\Column(type="boolean", nullable=true)
     */
    private $active;

	/**
	 * @var bool|null
	 * @ORM\Column(type="boolean", nullable=true)
	 */
	private $toApprove = false;

    /**
     * Constructor.
     */
    public function __construct()
    {
        $this->ips = new ArrayCollection();
        $this->id = Uuid::v4();
    }

    public function getId()
    {
        return $this->id;
    }

    /**
     * Set device.
     *
     * @param string $device
     *
     * @return Device
     */
    public function setDevice($device)
    {
        $this->device = $device;

        return $this;
    }

    /**
     * Get device.
     *
     * @return string
     */
    public function getDevice()
    {
        return $this->device;
    }

    /**
     * Set mac.
     *
     * @param string $mac
     *
     * @return Device
     */
    public function setMac($mac)
    {
        $this->mac = $mac;

        return $this;
    }

    /**
     * Get mac.
     *
     * @return string
     */
    public function getMac()
    {
        return $this->mac;
    }

    /**
     * Set ips.
     *
     * @param Collection $ips
     *
     * @return Device
     */
    public function setIps($ips)
    {
        $this->ips = $ips;

        return $this;
    }

    /**
     * Get ips.
     *
     * @return [DeviceIp]
     */
    public function getIps() : Collection
    {
        return $this->ips;
    }

    public function addIp(DeviceIp $deviceIp)
    {
        $deviceIp->setMac($this);
        $this->ips->add($deviceIp);
    }

    public function removeIp(DeviceIp $deviceIp)
    {
        $this->ips->removeElement($deviceIp);
    }

    /**
     * Set active.
     *
     * @param bool $active
     *
     * @return Device
     */
    public function setActive($active)
    {
        $this->active = $active;

        return $this;
    }

    /**
     * Get active.
     *
     * @return bool
     */
    public function getActive()
    {
        return $this->active;
    }

	/**
	 * @return bool|null
	 */
	public function isToApprove(): ?bool {
      		return $this->toApprove;
      	}

	/**
	 * @param bool $toApprove
	 * @return Device
	 */
	public function setToApprove(bool $toApprove): Device {
      		$this->toApprove = $toApprove;
      
      		return $this;
      	}

    /**
     * Set mikrotikList.
     *
     * @param \App\Entity\MikrotikList $mikrotikList
     *
     * @return Device
     */
    public function setMikrotikList(MikrotikList $mikrotikList = null)
    {
        $this->mikrotikList = $mikrotikList;

        return $this;
    }

    /**
     * Get mikrotikList.
     *
     * @return \App\Entity\MikrotikList
     */
    public function getMikrotikList()
    {
        return $this->mikrotikList;
    }

    /**
     * Set bypassHotspot.
     *
     * @param bool $bypassHotspot
     *
     * @return Device
     */
    public function setBypassHotspot($bypassHotspot)
    {
        $this->bypassHotspot = $bypassHotspot;

        return $this;
    }

    /**
     * Get bypassHotspot.
     *
     * @return bool
     */
    public function getBypassHotspot()
    {
        return $this->bypassHotspot;
    }

    public function isBypassHotspot(): ?bool
    {
        return $this->bypassHotspot;
    }

    public function isActive(): ?bool
    {
        return $this->active;
    }
}
