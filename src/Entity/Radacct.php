<?php

namespace App\Entity;

use Doctrine\DBAL\Types\Types;
use Doctrine\ORM\Mapping as ORM;

/**
 * Radacct.
 *
 * @ORM\Table(name="radacct", indexes={@ORM\Index(name="username", columns={"username"}), @ORM\Index(name="framedipaddress", columns={"framedipaddress"}), @ORM\Index(name="acctsessionid", columns={"acctsessionid"}), @ORM\Index(name="acctsessiontime", columns={"acctsessiontime"}), @ORM\Index(name="acctuniqueid", columns={"acctuniqueid"}), @ORM\Index(name="acctstarttime", columns={"acctstarttime"}), @ORM\Index(name="acctstoptime", columns={"acctstoptime"}), @ORM\Index(name="nasipaddress", columns={"nasipaddress"})})
 * @ORM\Entity
 */
class Radacct
{
    /**
     * @var int
     *
     * @ORM\Column(name="radacctid", type="bigint", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $radacctid;

    /**
     * @var string
     *
     * @ORM\Column(name="acctsessionid", type="string", length=64, nullable=false)
     */
    private $acctsessionid = '';

    /**
     * @var string
     *
     * @ORM\Column(name="acctuniqueid", type="string", length=32, nullable=false)
     */
    private $acctuniqueid = '';

    /**
     * @var string
     *
     * @ORM\Column(name="username", type="string", length=64, nullable=false)
     */
    private $username = '';

    /**
     * @var string
     *
     * @ORM\Column(name="groupname", type="string", length=64, nullable=false)
     */
    private $groupname = '';

    /**
     * @var string
     *
     * @ORM\Column(name="realm", type="string", length=64, nullable=true)
     */
    private $realm = '';

    /**
     * @var string
     *
     * @ORM\Column(name="nasipaddress", type="string", length=15, nullable=false)
     */
    private $nasipaddress = '';

    /**
     * @var string
     *
     * @ORM\Column(name="nasportid", type="string", length=15, nullable=true)
     */
    private $nasportid;

    /**
     * @var string
     *
     * @ORM\Column(name="nasporttype", type="string", length=32, nullable=true)
     */
    private $nasporttype;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="acctstarttime", type="datetime", nullable=true)
     */
    private $acctstarttime;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="acctupdatetime", type="datetime", nullable=true)
     */
    private $acctupdatetime;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="acctstoptime", type="datetime", nullable=true)
     */
    private $acctstoptime;

    /**
     * @var int
     *
     * @ORM\Column(name="acctinterval", type="integer", length=12, nullable=true)
     */
    private $acctinterval;

    /**
     * @var int
     *
     * @ORM\Column(name="acctsessiontime", type="integer", nullable=true)
     */
    private $acctsessiontime;

    /**
     * @var string
     *
     * @ORM\Column(name="acctauthentic", type="string", length=32, nullable=true)
     */
    private $acctauthentic;

    /**
     * @var string
     *
     * @ORM\Column(name="connectinfo_start", type="string", length=50, nullable=true)
     */
    private $connectinfoStart;

    /**
     * @var string
     *
     * @ORM\Column(name="connectinfo_stop", type="string", length=50, nullable=true)
     */
    private $connectinfoStop;

    /**
     * @var int
     *
     * @ORM\Column(name="acctinputoctets", type="bigint", nullable=true)
     */
    private $acctinputoctets;

    /**
     * @var int
     *
     * @ORM\Column(name="acctoutputoctets", type="bigint", nullable=true)
     */
    private $acctoutputoctets;

    /**
     * @var string
     *
     * @ORM\Column(name="calledstationid", type="string", length=50, nullable=false)
     */
    private $calledstationid = '';

    /**
     * @var string
     *
     * @ORM\Column(name="callingstationid", type="string", length=50, nullable=false)
     */
    private $callingstationid = '';

    /**
     * @var string
     *
     * @ORM\Column(name="acctterminatecause", type="string", length=32, nullable=false)
     */
    private $acctterminatecause = '';

    /**
     * @var string
     *
     * @ORM\Column(name="servicetype", type="string", length=32, nullable=true)
     */
    private $servicetype;

    /**
     * @var string
     *
     * @ORM\Column(name="framedprotocol", type="string", length=32, nullable=true)
     */
    private $framedprotocol;

    /**
     * @var string
     *
     * @ORM\Column(name="framedipaddress", type="string", length=15, nullable=false)
     */
    private $framedipaddress = '';

    /**
     * @var string
     *
     * @ORM\Column(name="framedipv6address", type="string", length=45, nullable=false)
     */
    private $framedipv6address = '';

    /**
     * @var string
     *
     * @ORM\Column(name="framedipv6prefix", type="string", length=45, nullable=false)
     */
    private $framedipv6prefix = '';

    /**
     * @var string
     *
     * @ORM\Column(name="framedinterfaceid", type="string", length=44, nullable=false)
     */
    private $framedinterfaceid = '';

    /**
     * @var string
     *
     * @ORM\Column(name="delegatedipv6prefix", type="string", length=45, nullable=false)
     */
    private $delegatedipv6prefix = '';

    /**
     * @var int
     *
     * @ORM\Column(name="acctstartdelay", type="integer", nullable=true)
     */
    private $acctstartdelay;

    /**
     * @var int
     *
     * @ORM\Column(name="acctstopdelay", type="integer", nullable=true)
     */
    private $acctstopdelay;

    /**
     * @var string
     *
     * @ORM\Column(name="xascendsessionsvrkey", type="string", length=10, nullable=true)
     */
    private $xascendsessionsvrkey;

    /**
     * Get radacctid.
     *
     * @return int
     */
    public function getRadacctid()
    {
        return $this->radacctid;
    }

    /**
     * Set acctsessionid.
     *
     * @param string $acctsessionid
     *
     * @return Radacct
     */
    public function setAcctsessionid($acctsessionid)
    {
        $this->acctsessionid = $acctsessionid;

        return $this;
    }

    /**
     * Get acctsessionid.
     *
     * @return string
     */
    public function getAcctsessionid()
    {
        return $this->acctsessionid;
    }

    /**
     * Set acctuniqueid.
     *
     * @param string $acctuniqueid
     *
     * @return Radacct
     */
    public function setAcctuniqueid($acctuniqueid)
    {
        $this->acctuniqueid = $acctuniqueid;

        return $this;
    }

    /**
     * Get acctuniqueid.
     *
     * @return string
     */
    public function getAcctuniqueid()
    {
        return $this->acctuniqueid;
    }

    /**
     * Set username.
     *
     * @param string $username
     *
     * @return Radacct
     */
    public function setUsername($username)
    {
        $this->username = $username;

        return $this;
    }

    /**
     * Get username.
     *
     * @return string
     */
    public function getUsername()
    {
        return $this->username;
    }

    /**
     * Set groupname.
     *
     * @param string $groupname
     *
     * @return Radacct
     */
    public function setGroupname($groupname)
    {
        $this->groupname = $groupname;

        return $this;
    }

    /**
     * Get groupname.
     *
     * @return string
     */
    public function getGroupname()
    {
        return $this->groupname;
    }

    /**
     * Set realm.
     *
     * @param string $realm
     *
     * @return Radacct
     */
    public function setRealm($realm)
    {
        $this->realm = $realm;

        return $this;
    }

    /**
     * Get realm.
     *
     * @return string
     */
    public function getRealm()
    {
        return $this->realm;
    }

    /**
     * Set nasipaddress.
     *
     * @param string $nasipaddress
     *
     * @return Radacct
     */
    public function setNasipaddress($nasipaddress)
    {
        $this->nasipaddress = $nasipaddress;

        return $this;
    }

    /**
     * Get nasipaddress.
     *
     * @return string
     */
    public function getNasipaddress()
    {
        return $this->nasipaddress;
    }

    /**
     * Set nasportid.
     *
     * @param string $nasportid
     *
     * @return Radacct
     */
    public function setNasportid($nasportid)
    {
        $this->nasportid = $nasportid;

        return $this;
    }

    /**
     * Get nasportid.
     *
     * @return string
     */
    public function getNasportid()
    {
        return $this->nasportid;
    }

    /**
     * Set nasporttype.
     *
     * @param string $nasporttype
     *
     * @return Radacct
     */
    public function setNasporttype($nasporttype)
    {
        $this->nasporttype = $nasporttype;

        return $this;
    }

    /**
     * Get nasporttype.
     *
     * @return string
     */
    public function getNasporttype()
    {
        return $this->nasporttype;
    }

    /**
     * Set acctstarttime.
     *
     * @param \DateTime $acctstarttime
     *
     * @return Radacct
     */
    public function setAcctstarttime($acctstarttime)
    {
        $this->acctstarttime = $acctstarttime;

        return $this;
    }

    /**
     * Get acctstarttime.
     *
     * @return \DateTime
     */
    public function getAcctstarttime()
    {
        return $this->acctstarttime;
    }

    /**
     * @return \DateTime
     */
    public function getAcctupdatetime()
    {
        return $this->acctupdatetime;
    }

    /**
     * @param \DateTime $acctupdatetime
     */
    public function setAcctupdatetime($acctupdatetime)
    {
        $this->acctupdatetime = $acctupdatetime;
    }

    /**
     * Set acctstoptime.
     *
     * @param \DateTime $acctstoptime
     *
     * @return Radacct
     */
    public function setAcctstoptime($acctstoptime)
    {
        $this->acctstoptime = $acctstoptime;

        return $this;
    }

    /**
     * Get acctstoptime.
     *
     * @return \DateTime
     */
    public function getAcctstoptime()
    {
        return $this->acctstoptime;
    }

    /**
     * @return int
     */
    public function getAcctinterval()
    {
        return $this->acctinterval;
    }

    /**
     * @param int $acctinterval
     */
    public function setAcctinterval($acctinterval)
    {
        $this->acctinterval = $acctinterval;
    }

    /**
     * Set acctsessiontime.
     *
     * @param int $acctsessiontime
     *
     * @return Radacct
     */
    public function setAcctsessiontime($acctsessiontime)
    {
        $this->acctsessiontime = $acctsessiontime;

        return $this;
    }

    /**
     * Get acctsessiontime.
     *
     * @return int
     */
    public function getAcctsessiontime()
    {
        return $this->acctsessiontime;
    }

    /**
     * Set acctauthentic.
     *
     * @param string $acctauthentic
     *
     * @return Radacct
     */
    public function setAcctauthentic($acctauthentic)
    {
        $this->acctauthentic = $acctauthentic;

        return $this;
    }

    /**
     * Get acctauthentic.
     *
     * @return string
     */
    public function getAcctauthentic()
    {
        return $this->acctauthentic;
    }

    /**
     * Set connectinfoStart.
     *
     * @param string $connectinfoStart
     *
     * @return Radacct
     */
    public function setConnectinfoStart($connectinfoStart)
    {
        $this->connectinfoStart = $connectinfoStart;

        return $this;
    }

    /**
     * Get connectinfoStart.
     *
     * @return string
     */
    public function getConnectinfoStart()
    {
        return $this->connectinfoStart;
    }

    /**
     * Set connectinfoStop.
     *
     * @param string $connectinfoStop
     *
     * @return Radacct
     */
    public function setConnectinfoStop($connectinfoStop)
    {
        $this->connectinfoStop = $connectinfoStop;

        return $this;
    }

    /**
     * Get connectinfoStop.
     *
     * @return string
     */
    public function getConnectinfoStop()
    {
        return $this->connectinfoStop;
    }

    /**
     * Set acctinputoctets.
     *
     * @param int $acctinputoctets
     *
     * @return Radacct
     */
    public function setAcctinputoctets($acctinputoctets)
    {
        $this->acctinputoctets = $acctinputoctets;

        return $this;
    }

    /**
     * Get acctinputoctets.
     *
     * @return int
     */
    public function getAcctinputoctets()
    {
        return $this->acctinputoctets;
    }

    /**
     * Set acctoutputoctets.
     *
     * @param int $acctoutputoctets
     *
     * @return Radacct
     */
    public function setAcctoutputoctets($acctoutputoctets)
    {
        $this->acctoutputoctets = $acctoutputoctets;

        return $this;
    }

    /**
     * Get acctoutputoctets.
     *
     * @return int
     */
    public function getAcctoutputoctets()
    {
        return $this->acctoutputoctets;
    }

    /**
     * Set calledstationid.
     *
     * @param string $calledstationid
     *
     * @return Radacct
     */
    public function setCalledstationid($calledstationid)
    {
        $this->calledstationid = $calledstationid;

        return $this;
    }

    /**
     * Get calledstationid.
     *
     * @return string
     */
    public function getCalledstationid()
    {
        return $this->calledstationid;
    }

    /**
     * Set callingstationid.
     *
     * @param string $callingstationid
     *
     * @return Radacct
     */
    public function setCallingstationid($callingstationid)
    {
        $this->callingstationid = $callingstationid;

        return $this;
    }

    /**
     * Get callingstationid.
     *
     * @return string
     */
    public function getCallingstationid()
    {
        return $this->callingstationid;
    }

    /**
     * Set acctterminatecause.
     *
     * @param string $acctterminatecause
     *
     * @return Radacct
     */
    public function setAcctterminatecause($acctterminatecause)
    {
        $this->acctterminatecause = $acctterminatecause;

        return $this;
    }

    /**
     * Get acctterminatecause.
     *
     * @return string
     */
    public function getAcctterminatecause()
    {
        return $this->acctterminatecause;
    }

    /**
     * Set servicetype.
     *
     * @param string $servicetype
     *
     * @return Radacct
     */
    public function setServicetype($servicetype)
    {
        $this->servicetype = $servicetype;

        return $this;
    }

    /**
     * Get servicetype.
     *
     * @return string
     */
    public function getServicetype()
    {
        return $this->servicetype;
    }

    /**
     * Set framedprotocol.
     *
     * @param string $framedprotocol
     *
     * @return Radacct
     */
    public function setFramedprotocol($framedprotocol)
    {
        $this->framedprotocol = $framedprotocol;

        return $this;
    }

    /**
     * Get framedprotocol.
     *
     * @return string
     */
    public function getFramedprotocol()
    {
        return $this->framedprotocol;
    }

    /**
     * Set framedipaddress.
     *
     * @param string $framedipaddress
     *
     * @return Radacct
     */
    public function setFramedipaddress($framedipaddress)
    {
        $this->framedipaddress = $framedipaddress;

        return $this;
    }

    /**
     * Get framedipaddress.
     *
     * @return string
     */
    public function getFramedipaddress()
    {
        return $this->framedipaddress;
    }

    /**
     * Set acctstartdelay.
     *
     * @param int $acctstartdelay
     *
     * @return Radacct
     */
    public function setAcctstartdelay($acctstartdelay)
    {
        $this->acctstartdelay = $acctstartdelay;

        return $this;
    }

    /**
     * Get acctstartdelay.
     *
     * @return int
     */
    public function getAcctstartdelay()
    {
        return $this->acctstartdelay;
    }

    /**
     * Set acctstopdelay.
     *
     * @param int $acctstopdelay
     *
     * @return Radacct
     */
    public function setAcctstopdelay($acctstopdelay)
    {
        $this->acctstopdelay = $acctstopdelay;

        return $this;
    }

    /**
     * Get acctstopdelay.
     *
     * @return int
     */
    public function getAcctstopdelay()
    {
        return $this->acctstopdelay;
    }

    /**
     * Set xascendsessionsvrkey.
     *
     * @param string $xascendsessionsvrkey
     *
     * @return Radacct
     */
    public function setXascendsessionsvrkey($xascendsessionsvrkey)
    {
        $this->xascendsessionsvrkey = $xascendsessionsvrkey;

        return $this;
    }

    /**
     * Get xascendsessionsvrkey.
     *
     * @return string
     */
    public function getXascendsessionsvrkey()
    {
        return $this->xascendsessionsvrkey;
    }

    public function getFramedipv6address(): ?string
    {
        return $this->framedipv6address;
    }

    public function setFramedipv6address(string $framedipv6address): static
    {
        $this->framedipv6address = $framedipv6address;

        return $this;
    }

    public function getFramedipv6prefix(): ?string
    {
        return $this->framedipv6prefix;
    }

    public function setFramedipv6prefix(string $framedipv6prefix): static
    {
        $this->framedipv6prefix = $framedipv6prefix;

        return $this;
    }

    public function getFramedinterfaceid(): ?string
    {
        return $this->framedinterfaceid;
    }

    public function setFramedinterfaceid(string $framedinterfaceid): static
    {
        $this->framedinterfaceid = $framedinterfaceid;

        return $this;
    }

    public function getDelegatedipv6prefix(): ?string
    {
        return $this->delegatedipv6prefix;
    }

    public function setDelegatedipv6prefix(string $delegatedipv6prefix): static
    {
        $this->delegatedipv6prefix = $delegatedipv6prefix;

        return $this;
    }
}
