<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Radreply.
 *
 * @ORM\Table(name="radusergroup", indexes={@ORM\Index(name="radusergroup_username", columns={"username"})})
 * @ORM\Entity
 */
class Radusergroup
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="username", type="string", length=64, nullable=false)
     */
    private $username = '';

    /**
     * @var string
     *
     * @ORM\Column(name="groupname", type="string", length=64, nullable=false)
     */
    private $groupname = '';

    /**
     * @var string
     *
     * @ORM\Column(name="priority", type="string", length=11, nullable=false)
     */
    private $priority = '1';

    /**
     * Get id.
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set username.
     *
     * @param string $username
     *
     * @return Radusergroup
     */
    public function setUsername($username)
    {
        $this->username = $username;

        return $this;
    }

    /**
     * Get username.
     *
     * @return string
     */
    public function getUsername()
    {
        return $this->username;
    }

    /**
     * Set attribute.
     *
     * @param string $attribute
     *
     * @return Radusergroup
     */
    public function setAttribute($attribute)
    {
        $this->attribute = $attribute;

        return $this;
    }

    /**
     * Get attribute.
     *
     * @return string
     */
    public function getAttribute()
    {
        return $this->attribute;
    }

    public function getGroupname(): string
    {
        return $this->groupname;
    }

    public function setGroupname(string $groupname): Radusergroup
    {
        $this->groupname = $groupname;

        return $this;
    }

    public function getPriority(): ?string
    {
        return $this->priority;
    }

    public function setPriority(string $priority): static
    {
        $this->priority = $priority;

        return $this;
    }
}
