<?php

namespace App\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\DBAL\Types\Types;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity
 * @ORM\HasLifecycleCallbacks
 */
class AppleSchool
{
    /**
     * @ORM\Id
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @ORM\Column(type="string", nullable=false)
     */
    protected $organizationName;

    /**
     * @ORM\Column(type="string", nullable=false)
     */
    protected $organizationId;

    /**
     * @ORM\Column(type="string", nullable=true)
     */
    protected $sftpUsername;

    /**
     * @ORM\Column(type="string", nullable=true)
     */
    protected $sftpPassword;

    /**
     * @ORM\Column(type="smallint")
     */
    protected $courseType = 0;

    /**
     * @ORM\Column(type="boolean", nullable=false)
     */
    protected $active;

    /**
     * @var ArrayCollection|AppleSchoolLocation[]
     * @ORM\OneToMany(targetEntity="AppleSchoolLocation", mappedBy="appleSchool", cascade={"persist"})
     **/
    private $locations;

    /**
     * AppleSchool constructor.
     */
    public function __construct()
    {
        $this->locations = new \Doctrine\Common\Collections\ArrayCollection();
    }

    public function __clone()
    {
        $this->locations = clone $this->locations;
    }

    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @return mixed
     */
    public function getOrganizationName()
    {
        return $this->organizationName;
    }

    /**
     * @param mixed $organizationName
     */
    public function setOrganizationName($organizationName)
    {
        $this->organizationName = $organizationName;
    }

    /**
     * @return mixed
     */
    public function getOrganizationId()
    {
        return $this->organizationId;
    }

    /**
     * @param mixed $organizationId
     */
    public function setOrganizationId($organizationId)
    {
        $this->organizationId = $organizationId;
    }

    /**
     * @return mixed
     */
    public function getSftpUsername()
    {
        return $this->sftpUsername;
    }

    /**
     * @param mixed $sftpUsername
     */
    public function setSftpUsername($sftpUsername)
    {
        $this->sftpUsername = $sftpUsername;
    }

    /**
     * @return mixed
     */
    public function getSftpPassword()
    {
        return $this->sftpPassword;
    }

    /**
     * @param mixed $sftpPassword
     */
    public function setSftpPassword($sftpPassword)
    {
        if (!is_null($sftpPassword)) {
            $this->sftpPassword = $sftpPassword;
        }
    }

    /**
     * @return int
     */
    public function getCourseType()
    {
        return (int) $this->courseType;
    }

    /**
     * @param mixed $courseType
     */
    public function setCourseType($courseType)
    {
        $this->courseType = $courseType;
    }

    /**
     * @return mixed
     */
    public function getActive()
    {
        return $this->active;
    }

    /**
     * @param mixed $active
     */
    public function setActive($active)
    {
        $this->active = $active;
    }

    /**
     * @return AppleSchoolLocation[]|ArrayCollection
     */
    public function getLocations()
    {
        return $this->locations;
    }

    /**
     * @param ArrayCollection|AppleSchoolLocation[] $locations
     */
    public function setLocations($locations)
    {
        $this->locations = $locations;
    }

    public function addLocation(AppleSchoolLocation $location)
    {
        $location->setAppleSchool($this);
        $this->locations->add($location);
    }

    public function removeLocation(AppleSchoolLocation $location)
    {
        $this->locations->removeElement($location);
    }

    /**
     * @return ArrayCollection|Provider[]
     */
    public function getProviders()
    {
        static $providers;

        if (!isset($providers)) {
            $providers = new \Doctrine\Common\Collections\ArrayCollection();
            foreach ($this->getLocations() as $location) {
                foreach ($location->getProviders() as $provider) {
                    $providers->add($provider);
                }
            }
        }

        return $providers;
    }

    public function isActive(): ?bool
    {
        return $this->active;
    }
}
