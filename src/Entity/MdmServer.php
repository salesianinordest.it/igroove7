<?php


namespace App\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\DBAL\Types\Types;
use Doctrine\ORM\Mapping as ORM;
use App\MdmConnector\AbstractMdmConnector;
use App\MdmConnector\ZuluConnector;
use Symfony\Component\Uid\Uuid;

/**
 * @ORM\Entity
 */
class MdmServer {
    /**
     * @ORM\Column(type="uuid", unique=true)
     * @ORM\Id
     */
    private $id;

    /**
     * @ORM\Column(type="string", nullable=false)
     */
    private $name;

    /**
     * @ORM\Column(type="string", nullable=false)
     */
    private $type;

    /**
     * @ORM\Column(type="json")
     */
    private $connectionData;

    /**
     * @ORM\Column(type="datetime", nullable=true)
     */
    private $lastUpdate;

    /**
     * @ORM\Column(type="string", nullable=true)
     */
    private $lastUpdateStatus;

    /**
     * @ORM\Column(type="boolean", nullable=true)
     */
    private $active;

    /**
     * @ORM\OneToMany(targetEntity="MdmDevice", mappedBy="mdmServer")
     */
    private $devices;

    /**
     * Constructor
     */
    public function __construct()
    {
        $this->devices = new ArrayCollection();
	    $this->id = Uuid::v4();
    }

    /**
     * @return mixed
     */
    public function getId() {
        return $this->id;
    }

    /**
     * @return string
     */
    public function getName() {
        return $this->name;
    }

    /**
     * @param string $name
     */
    public function setName($name) {
        $this->name = $name;
    }

    /**
     * @return string
     */
    public function getType() {
        return $this->type;
    }

    /**
     * @param string $type
     */
    public function setType($type) {
        $this->type = $type;
    }

    /**
     * @return mixed
     */
    public function getConnectionData() {
        return $this->connectionData;
    }

    /**
     * @param mixed $connectionData
     */
    public function setConnectionData($connectionData) {
        $this->connectionData = $connectionData;
    }

    /**
     * @return \DateTime
     */
    public function getLastUpdate() {
        return $this->lastUpdate;
    }

    /**
     * @param \DateTime $lastUpdate
     */
    public function setLastUpdate($lastUpdate) {
        $this->lastUpdate = $lastUpdate;
    }

    /**
     * @return string
     */
    public function getLastUpdateStatus() {
        return $this->lastUpdateStatus;
    }

    /**
     * @param string $lastUpdateStatus
     */
    public function setLastUpdateStatus($lastUpdateStatus) {
        $this->lastUpdateStatus = $lastUpdateStatus;
    }

    /**
     * @return bool
     */
    public function getActive() {
        return $this->active;
    }

    /**
     * @param bool $active
     */
    public function setActive($active) {
        $this->active = $active;
    }

    /**
     * @return MdmDevice[]|ArrayCollection
     */
    public function getDevices() {
        return $this->devices;
    }

    /**
     * @return AbstractMdmConnector
     * @throws \Exception
     */
    public function getConnectionManager() {
        if(!is_array($this->connectionData)) {
            throw new \Exception("Connection data to the server not set");
        }

        switch ($this->type) {
            case "zulu":
                return new ZuluConnector($this->connectionData);
        }

        throw new \Exception("Invalid server type");
    }

    public function isActive(): ?bool
    {
        return $this->active;
    }

    public function addDevice(MdmDevice $device): static
    {
        if (!$this->devices->contains($device)) {
            $this->devices->add($device);
            $device->setMdmServer($this);
        }

        return $this;
    }

    public function removeDevice(MdmDevice $device): static
    {
        if ($this->devices->removeElement($device)) {
            // set the owning side to null (unless already changed)
            if ($device->getMdmServer() === $this) {
                $device->setMdmServer(null);
            }
        }

        return $this;
    }

}