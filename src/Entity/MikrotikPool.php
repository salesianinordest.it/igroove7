<?php

namespace App\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Uid\Uuid;

/**
 * @ORM\Entity
 * @ORM\Entity(repositoryClass="App\Repository\MikrotikPoolRepository")
 */
class MikrotikPool
{
    /**
     * @ORM\Column(type="uuid", unique=true)
     * @ORM\Id
     */
    private $id;

    /**
     * @ORM\Column(type="string", nullable=true)
     */
    private $nome;

    /**
     * @ORM\Column(type="integer", nullable=false, options={"unsigned":true})
     */
    private $ipStart;
    /**
     * @ORM\Column(type="integer", nullable=false, options={"unsigned":true})
     */
    private $ipEnd;

    /**
     * @ORM\Column(type="boolean", nullable=true)
     */
    private $creareSuMikrotik;

    /**
     * @ORM\ManyToOne(targetEntity="Mikrotik", inversedBy="pools")
     **/
    private $mikrotik;

    /**
     * @ORM\Column(type="string", nullable=false)
     */
    private $dhcpServerName;

    /**
     * @var DeviceIp[]
     * @ORM\OneToMany(targetEntity="DeviceIp", mappedBy="mikrotikPool")
     * @ORM\OrderBy({"ip" = "ASC"})
     */
    private $deviceIps;

    public function __construct()
    {
        $this->deviceIps = new ArrayCollection();

        $this->id = Uuid::v4();
    }

    public function __toString()
    {
        return $this->nome;
    }

    /**
     * Get id.
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set nome.
     *
     * @param string $nome
     *
     * @return MikrotikPool
     */
    public function setNome($nome)
    {
        $this->nome = $nome;

        return $this;
    }

    /**
     * Get nome.
     *
     * @return string
     */
    public function getNome()
    {
        return $this->nome;
    }

    /**
     * Set ipStart.
     *
     * @param string $ipStart
     *
     * @return MikrotikPool
     */
    public function setIpStart($ipStart)
    {
        $this->ipStart = $ipStart;

        return $this;
    }

    /**
     * Get ipStart.
     *
     * @return string
     */
    public function getIpStart()
    {
        return $this->ipStart;
    }

    /**
     * Set ipStart from string with format xxx.xxx.xxx.xxx.
     *
     * @param int $ipStart
     *
     * @return DeviceIp
     */
    public function setDottedIpStart($ipStart)
    {
        $this->ipStart = ip2long($ipStart);
    }

    /**
     * Get ipStart as string with format xxx.xxx.xxx.xxx.
     *
     * @return string
     */
    public function getDottedIpStart()
    {
        return long2ip($this->ipStart);
    }

    /**
     * Set ipEnd.
     *
     * @param string $ipEnd
     *
     * @return MikrotikPool
     */
    public function setIpEnd($ipEnd)
    {
        $this->ipEnd = $ipEnd;

        return $this;
    }

    /**
     * Get ipEnd.
     *
     * @return string
     */
    public function getIpEnd()
    {
        return $this->ipEnd;
    }

    /**
     * Set ipEnd from string with format xxx.xxx.xxx.xxx.
     *
     * @param int $ipEnd
     *
     * @return DeviceIp
     */
    public function setDottedIpEnd($ipEnd)
    {
        $this->ipEnd = ip2long($ipEnd);
    }

    /**
     * Get ipEnd as string with format xxx.xxx.xxx.xxx.
     *
     * @return string
     */
    public function getDottedIpEnd()
    {
        return long2ip($this->ipEnd);
    }

    /**
     * Set creareSuMikrotik.
     *
     * @param bool $creareSuMikrotik
     *
     * @return MikrotikPool
     */
    public function setCreareSuMikrotik($creareSuMikrotik)
    {
        $this->creareSuMikrotik = $creareSuMikrotik;

        return $this;
    }

    /**
     * Get creareSuMikrotik.
     *
     * @return bool
     */
    public function getCreareSuMikrotik()
    {
        return $this->creareSuMikrotik;
    }

    /**
     * Set mikrotik.
     *
     * @param Mikrotik $mikrotik
     *
     * @return MikrotikPool
     */
    public function setMikrotik(Mikrotik $mikrotik = null)
    {
        $this->mikrotik = $mikrotik;

        return $this;
    }

    /**
     * Get mikrotik.
     *
     * @return Mikrotik
     */
    public function getMikrotik()
    {
        return $this->mikrotik;
    }

    /**
     * @return string
     */
    public function getDhcpServerName()
    {
        return $this->dhcpServerName;
    }

    /**
     * @param string $dhcpServerName
     */
    public function setDhcpServerName($dhcpServerName)
    {
        $this->dhcpServerName = $dhcpServerName;
    }

    /**
     * @return DeviceIp[]
     */
    public function getDeviceIps()
    {
        return $this->deviceIps;
    }

    /**
     * @param DeviceIp[] $deviceIps
     */
    public function setDeviceIps($deviceIps)
    {
        $this->deviceIps = $deviceIps;
    }

    public function addDeviceIp(DeviceIp $deviceIp): self
    {
        if (!$this->deviceIps->contains($deviceIp)) {
            $this->deviceIps[] = $deviceIp;
            $deviceIp->setMikrotikPool($this);
        }

        return $this;
    }

    public function removeDeviceIp(DeviceIp $deviceIp): self
    {
        if ($this->deviceIps->contains($deviceIp)) {
            $this->deviceIps->removeElement($deviceIp);
            // set the owning side to null (unless already changed)
            if ($deviceIp->getMikrotikPool() === $this) {
                $deviceIp->setMikrotikPool(null);
            }
        }

        return $this;
    }

    public function isCreareSuMikrotik(): ?bool
    {
        return $this->creareSuMikrotik;
    }
}
