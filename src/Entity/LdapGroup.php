<?php

namespace App\Entity;

use Doctrine\DBAL\Types\Types;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity
 * @ORM\Entity(repositoryClass="App\Repository\LdapGroupRepository")
 */
class LdapGroup
{
    /**
     * @ORM\Id
     * @ORM\Column(type="string", nullable=true)
     */
    private $name;

    /**
     * @ORM\Column(type="string", nullable=true)
     */
    private $operation;

    /**
     * @ORM\Column(type="text", nullable=true)
     **/
    private $members;

    public function __toString()
    {
        return $this->name;
    }

    public function getMembersList($type = null)
    {
        if ($this->members) {
            $members = json_decode($this->members, true);
            if (!is_array($members)) {
                return [];
            }

            if (null !== $type) {
                return isset($members[$type]) ? $members[$type] : [];
            }
        } else {
            return [];
        }

        return $members;
    }

    /**
     * Set name.
     *
     * @param string $type
     * @param string $id
     */
    public function addMember($type, $id)
    {
        $actual = $this->getMembersList();
        if (!array_key_exists($type, $actual)) {
            $actual[$type] = [];
        }

        if ($this->memberExists($type, $id)) {
            return;
        }

        $actual[$type][] = $id;
        $this->members = json_encode($actual);
        $this->operation = ('CREATE' != $this->operation) ? 'MEMBERS CHANGED' : $this->operation;
    }

    /**
     * Set name.
     *
     * @param string $type
     * @param string $id
     */
    public function removeMember($type, $id)
    {
        $actual = $this->getMembersList();
        if (!array_key_exists($type, $actual)) {
            return;
        }

        $id = strtolower($id);
        $newList = [];
        foreach ($actual[$type] as $v) {
            if (strtolower($v) != $id) {
                $newList[] = $v;
            } else {
                $this->operation = ('CREATE' != $this->operation) ? 'MEMBERS CHANGED' : $this->operation;
            }
        }
        $actual[$type] = $newList;
        $this->members = json_encode($actual);
    }

    public function memberExists($type, $id)
    {
        $members = $this->getMembersList($type);
        if (empty($members)) {
            return false;
        }

        return in_array(strtolower($id), array_map('strtolower', $members));
    }

    /**
     * Set name.
     *
     * @param string $name
     *
     * @return LdapGroup
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get name.
     *
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Set operation.
     *
     * @param string $operation
     *
     * @return LdapGroup
     */
    public function setOperation($operation)
    {
        $this->operation = $operation;

        return $this;
    }

    /**
     * Get operation.
     *
     * @return string
     */
    public function getOperation()
    {
        return $this->operation;
    }

    /**
     * Set members.
     *
     * @param string $members
     *
     * @return LdapGroup
     */
    public function setMembers($members)
    {
        $this->members = $members;

        return $this;
    }

    /**
     * Get members.
     *
     * @return string
     */
    public function getMembers()
    {
        return $this->members;
    }

    /**
     * function ldap_escape.
     *
     * @param string $subject The subject string
     * @param bool $dn Treat subject as a DN if TRUE
     * @param string|array $ignore Set of characters to leave untouched
     *
     * @return string The escaped string
     *
     * $user = 'Test , Name S.L';
     * if (!ldap_add($ds, "cn=".ldap_escape($user, TRUE).",".LDAP_DN_BASE, $info)) {
     *  include 'error_new_account.php';
     * }
     * @author Chris Wright
     *
     * @version 2.0
     *
     */
    public function ldapEscape($subject)
    {
        $subject = str_replace(
            [
                'à',
                'á',
                'â',
                'ã',
                'ä',
                'ç',
                'è',
                'é',
                'ê',
                'ë',
                'ì',
                'í',
                'î',
                'ï',
                'ñ',
                'ò',
                'ó',
                'ô',
                'õ',
                'ö',
                'ù',
                'ú',
                'û',
                'ü',
                'ý',
                'ÿ',
                'À',
                'Á',
                'Â',
                'Ã',
                'Ä',
                'Ç',
                'È',
                'É',
                'Ê',
                'Ë',
                'Ì',
                'Í',
                'Î',
                'Ï',
                'Ñ',
                'Ò',
                'Ó',
                'Ô',
                'Õ',
                'Ö',
                'Ù',
                'Ú',
                'Û',
                'Ü',
                'Ý',
            ],
            [
                'a',
                'a',
                'a',
                'a',
                'a',
                'c',
                'e',
                'e',
                'e',
                'e',
                'i',
                'i',
                'i',
                'i',
                'n',
                'o',
                'o',
                'o',
                'o',
                'o',
                'u',
                'u',
                'u',
                'u',
                'y',
                'y',
                'A',
                'A',
                'A',
                'A',
                'A',
                'C',
                'E',
                'E',
                'E',
                'E',
                'I',
                'I',
                'I',
                'I',
                'N',
                'O',
                'O',
                'O',
                'O',
                'O',
                'U',
                'U',
                'U',
                'U',
                'Y',
            ],
            $subject
        );
        $ignore = [
            ':',
            '/',
            '\\',
            ',',
            '=',
            '+',
            '<',
            '>',
            ';',
            '"',
            '#',
            "'",
            '(',
            ')',
            "'",
            "\x00",
            '?',
            '.',
            '-',
            '!',
        ];
        $subject = str_replace('  ', ' ', $subject);

        return trim(substr(str_replace($ignore, '', $subject), 0, 50));
    }
}
