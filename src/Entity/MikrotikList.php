<?php

namespace App\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Uid\Uuid;

/**
 * @ORM\Entity
 */
class MikrotikList
{
    /**
     * @ORM\Column(type="uuid", unique=true)
     * @ORM\Id
     */
    private $id;

    /**
     * @ORM\Column(type="string", nullable=true)
     */
    private $nome;

	/**
	 * @var int
	 * @ORM\Column(type="integer", nullable=true)
	 *
	 **/
	private $radiusVlan = null;

	/**
	 * @var bool
	 * @ORM\Column(type="boolean", nullable=true)
	 */
	private $showAsLaboratory = false;

    /**
     * @ORM\OneToMany(targetEntity="Device", mappedBy="mikrotikList")
     **/
    private $macs;

    /**
     * @ORM\ManyToOne(targetEntity="Mikrotik", inversedBy="ipLists")
     **/
    private $mikrotik;

    /**
     * Constructor.
     */
    public function __construct()
    {
        $this->macs = new \Doctrine\Common\Collections\ArrayCollection();
        $this->id = Uuid::v4();
    }

    public function __toString()
    {
        return $this->nome;
    }

    /**
     * Get id.
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set nome.
     *
     * @param string $nome
     *
     * @return MikrotikList
     */
    public function setNome($nome)
    {
        $this->nome = $nome;

        return $this;
    }

    /**
     * Get nome.
     *
     * @return string
     */
    public function getNome()
    {
        return $this->nome;
    }

	/**
	 * @return int
	 */
	public function getRadiusVlan() {
      		return $this->radiusVlan;
      	}

	/**
	 * @param int $radiusVlan
	 */
	public function setRadiusVlan($radiusVlan) {
      		$this->radiusVlan = $radiusVlan;
      	}

	/**
	 * @return bool
	 */
	public function isShowAsLaboratory(): bool {
      		return (bool)$this->showAsLaboratory;
      	}

	/**
	 * @param bool $showAsLaboratory
	 */
	public function setShowAsLaboratory(bool $showAsLaboratory): void {
      		$this->showAsLaboratory = $showAsLaboratory;
      	}

    /**
     * Add mac.
     *
     * @param \App\Entity\Device $mac
     *
     * @return MikrotikList
     */
    public function addMac(Device $mac)
    {
        $this->macs[] = $mac;

        return $this;
    }

    /**
     * Remove mac.
     *
     * @param \App\Entity\Device $mac
     */
    public function removeMac(Device $mac)
    {
        $this->macs->removeElement($mac);
    }

    /**
     * Get macs.
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getMacs()
    {
        return $this->macs;
    }

    /**
     * Set mikrotik.
     *
     * @param \App\Entity\Mikrotik $mikrotik
     *
     * @return MikrotikList
     */
    public function setMikrotik(Mikrotik $mikrotik = null)
    {
        $this->mikrotik = $mikrotik;

        return $this;
    }

    /**
     * Get mikrotik.
     *
     * @return \App\Entity\Mikrotik
     */
    public function getMikrotik()
    {
        return $this->mikrotik;
    }
}
