<?php

namespace App\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Uid\Uuid;

/**
 * @ORM\Entity
 */
class Mikrotik
{
    /**
     * @ORM\Column(type="uuid", unique=true)
     * @ORM\Id
     */
    private $id;

    /**
     * @ORM\Column(type="string", nullable=true)
     */
    private $ip;

    /**
     * @ORM\Column(type="string", nullable=true)
     */
    private $apiUsername;

    /**
     * @ORM\Column(type="string", nullable=true)
     */
    private $apiPassword;

    /**
     * @ORM\Column(type="boolean", nullable=true)
     */
    private $status;

    /**
     * @ORM\Column(type="string", nullable=true)
     */
    private $syncSuffix;

    /**
     * @ORM\Column(type="boolean", nullable=true)
     */
    private $useBypass;
    /**
     * @ORM\OneToMany(targetEntity="MikrotikList", mappedBy="mikrotik")
     **/
    private $ipLists;

    /**
     * @ORM\OneToMany(targetEntity="MikrotikPool", mappedBy="mikrotik")
     **/
    private $pools;

    public function __toString()
    {
        return $this->ip;
    }

    /**
     * Get id.
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set ip.
     *
     * @param string $ip
     *
     * @return Mikrotik
     */
    public function setIp($ip)
    {
        $this->ip = $ip;

        return $this;
    }

    /**
     * Get ip.
     *
     * @return string
     */
    public function getIp()
    {
        return $this->ip;
    }

    /**
     * Set apiUsername.
     *
     * @param string $apiUsername
     *
     * @return Mikrotik
     */
    public function setApiUsername($apiUsername)
    {
        $this->apiUsername = $apiUsername;

        return $this;
    }

    /**
     * Get apiUsername.
     *
     * @return string
     */
    public function getApiUsername()
    {
        return $this->apiUsername;
    }

    /**
     * Set apiPassword.
     *
     * @param string $apiPassword
     *
     * @return Mikrotik
     */
    public function setApiPassword($apiPassword)
    {
        if (null === $apiPassword) {
            return $this;
        }

        $this->apiPassword = $apiPassword;

        return $this;
    }

    /**
     * Get apiPassword.
     *
     * @return string
     */
    public function getApiPassword()
    {
        return $this->apiPassword;
    }

    /**
     * Set status.
     *
     * @param bool $status
     *
     * @return Mikrotik
     */
    public function setStatus($status)
    {
        $this->status = $status;

        return $this;
    }

    /**
     * Get status.
     *
     * @return bool
     */
    public function getStatus()
    {
        return $this->status;
    }

    /**
     * @return string
     */
    public function getSyncSuffix()
    {
        return $this->syncSuffix;
    }

    /**
     * @param string $syncSuffix
     */
    public function setSyncSuffix($syncSuffix)
    {
        $this->syncSuffix = $syncSuffix;
    }

    /**
     * Set status.
     *
     * @param bool $status
     *
     * @return Mikrotik
     */
    public function setUseGuest($status)
    {
        $this->useGuest = $status;

        return $this;
    }

    /**
     * Get status.
     *
     * @return bool
     */
    public function getUseGuest()
    {
        return $this->useGuest;
    }

    /**
     * Set status.
     *
     * @param bool $status
     *
     * @return Mikrotik
     */
    public function setUseBypass($status)
    {
        $this->useBypass = $status;

        return $this;
    }

    /**
     * Get status.
     *
     * @return bool
     */
    public function getUseBypass()
    {
        return $this->useBypass;
    }

    /**
     * Constructor.
     */
    public function __construct()
    {
        $this->ipLists = new \Doctrine\Common\Collections\ArrayCollection();
        $this->pools = new \Doctrine\Common\Collections\ArrayCollection();

        $this->id = Uuid::v4();
    }

    /**
     * Add ipList.
     *
     * @param \App\Entity\MikrotikList $ipList
     *
     * @return Mikrotik
     */
    public function addIpList(MikrotikList $ipList)
    {
        $this->ipLists[] = $ipList;

        return $this;
    }

    /**
     * Remove ipList.
     *
     * @param \App\Entity\MikrotikList $ipList
     */
    public function removeIpList(MikrotikList $ipList)
    {
        $this->ipLists->removeElement($ipList);
    }

    /**
     * Get ipLists.
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getIpLists()
    {
        return $this->ipLists;
    }

    /**
     * Add pool.
     *
     * @param \App\Entity\MikrotikPool $pool
     *
     * @return Mikrotik
     */
    public function addPool(MikrotikPool $pool)
    {
        $this->pools[] = $pool;

        return $this;
    }

    /**
     * Remove pool.
     *
     * @param \App\Entity\MikrotikPool $pool
     */
    public function removePool(MikrotikPool $pool)
    {
        $this->pools->removeElement($pool);
    }

    /**
     * Get pools.
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getPools()
    {
        return $this->pools;
    }

    public function isStatus(): ?bool
    {
        return $this->status;
    }

    public function isUseBypass(): ?bool
    {
        return $this->useBypass;
    }
}
