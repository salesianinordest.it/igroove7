<?php

namespace App\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;
use JMS\Serializer\Annotation as Serializer;
use Symfony\Component\Uid\Uuid;
use Symfony\Component\Validator\Constraints as Assert;

abstract class PersonAbstract
{
    /**
     * @ORM\Column(type="uuid", unique=true)
     * @ORM\Id
     */
    protected $id;

    /**
     * @ORM\Column(type="string", length=20, unique=true)
     */
    protected $fiscalCode;

    /**
     * @ORM\Column(type="string")
     */
    protected $lastname;

    /**
     * @ORM\Column(type="string")
     */
    protected $firstname;

    /**
     * @ORM\Column(type="string", nullable=true)
     */
    protected $email;

    /**
     * @ORM\Column(type="string", nullable=true)
     */
    protected $username;

    /**
     * @ORM\Column(type="string", nullable=true)
     */
    protected $starting_password;

    /**
     * @ORM\ManyToOne(targetEntity="Provider", inversedBy="students")
     * Serializer\MaxDepth(1)
     * Serializer\Groups({"person_provider"})
     **/
    protected $provider;

    /**
     * @var string
     * @ORM\Column(name="id_on_provider", type="string", nullable=true)
     */
    protected $idOnProvider;

    /**
     * @var ArrayCollection|Provider[]
     * @ORM\ManyToMany(targetEntity="Provider", inversedBy="additionalStudents")
     */
    protected $additionalProviders;

	/**
	 * @ORM\OneToMany(targetEntity="MdmDevice", mappedBy="studentOwner")
	 * @var MdmDevice[]|ArrayCollection
	 */
	protected $mdmDevices;

	/**
	 * @Assert\Type("\DateTimeInterface")
	 * @var \DateTime|null
	 * @ORM\Column(type="date", nullable=true)
	 *
	 **/
	protected ?\DateTime $trashedDate = NULL;

    /**
     * Constructor.
     */
    public function __construct($fiscalCode)
    {
//        if(trim($fiscalCode) == "") {
//            throw new \Exception('Empty fiscal code not allowed');
//        }

        $this->additionalProviders = new \Doctrine\Common\Collections\ArrayCollection();
        $this->fiscalCode = $fiscalCode;
        $this->id = Uuid::v4();
    }

    public function __toString()
    {
        return $this->getLastname().' '.$this->getFirstname();
    }

    public function __clone()
    {
        $this->additionalProviders = clone $this->additionalProviders;
    }

    /**
     * Get id.
     *
     * @return string
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @return string
     */
    public function getFiscalCode()
    {
        return $this->fiscalCode;
    }

    /**
     * @param string $fiscalCode
     *
     * @throws \Exception
     */
    public function setFiscalCode($fiscalCode)
    {
        if ('' == trim($fiscalCode)) {
            throw new \Exception('Empty fiscal code not allowed');
        }

        $this->fiscalCode = $fiscalCode;
    }

    /**
     * Set lastname.
     *
     * @param string $lastname
     *
     * @return PersonAbstract
     */
    public function setLastname($lastname)
    {
        $this->lastname = $lastname;

        return $this;
    }

    /**
     * Get lastname.
     *
     * @return string
     */
    public function getLastname()
    {
        return $this->lastname;
    }

    /**
     * Set firstname.
     *
     * @param string $firstname
     *
     * @return PersonAbstract
     */
    public function setFirstname($firstname)
    {
        $this->firstname = $firstname;

        return $this;
    }

    /**
     * Get firstname.
     *
     * @return string
     */
    public function getFirstname()
    {
        return $this->firstname;
    }

    /**
     * Set email.
     *
     * @param string $email
     *
     * @return PersonAbstract
     */
    public function setEmail($email)
    {
        $this->email = $email;

        return $this;
    }

    /**
     * Get email.
     *
     * @return string
     */
    public function getEmail()
    {
        return $this->email;
    }

    /**
     * Set username.
     *
     * @param string $username
     *
     * @return PersonAbstract
     */
    public function setUsername($username)
    {
        $this->username = $username;

        return $this;
    }

    /**
     * Get username.
     *
     * @return string
     */
    public function getUsername()
    {
        return $this->username;
    }

    /**
     * Set startingPassword.
     *
     * @param string $startingPassword
     *
     * @return PersonAbstract
     */
    public function setStartingPassword($startingPassword)
    {
        $this->starting_password = $startingPassword;

        return $this;
    }

    /**
     * Get startingPassword.
     *
     * @return string
     */
    public function getStartingPassword()
    {
        return $this->starting_password;
    }

    /**
     * Set provider.
     *
     * @param \App\Entity\Provider $provider
     *
     * @return PersonAbstract
     */
    public function setProvider(Provider $provider = null)
    {
        $this->provider = $provider;

        return $this;
    }

    /**
     * Get provider.
     *
     * @return \App\Entity\Provider
     */
    public function getProvider()
    {
        return $this->provider;
    }

    /**
     * @return string
     */
    public function getIdOnProvider()
    {
        return $this->idOnProvider;
    }

    /**
     * @param string $idOnProvider
     */
    public function setIdOnProvider($idOnProvider)
    {
        $this->idOnProvider = $idOnProvider;
    }

    /**
     * @return ArrayCollection|Provider[]
     */
    public function getAdditionalProviders()
    {
        return $this->additionalProviders;
    }

    /**
     * @return ArrayCollection|Provider[]
     */
    public function getProviders()
    {
        $providers = $this->additionalProviders;
        if ($this->provider instanceof Provider && !$this->additionalProviders->contains($this->provider)) {
            $providers->add($this->provider);
        }

        return $providers;
    }

    public function addAdditionalProvider(Provider $provider)
    {
        if (!$this->haveAdditionalProvider($provider)) {
            $this->additionalProviders->add($provider);
        }
    }

    public function removeAdditionalProvider(Provider $provider)
    {
        $this->additionalProviders->removeElement($provider);
    }

    /**
     * @return bool
     */
    public function haveAdditionalProvider(Provider $provider)
    {
        return $this->additionalProviders->contains($provider);
    }

    public function removeAllAdditionalProviders()
    {
        foreach ($this->additionalProviders as $additionalProvider) {
            if ($this instanceof Teacher) {
                $additionalProvider->removeAdditionalTeacher($this);
            } elseif ($this instanceof Student) {
                $additionalProvider->removeAdditionalStudent($this);
            }
        }
        $this->memberOf = new \Doctrine\Common\Collections\ArrayCollection();
    }

	/**
	 * @return ArrayCollection|MdmDevice[]
	 */
	public function getMdmDevices() {
		return $this->mdmDevices;
	}

	/**
	 * @return bool
	 */
	public function isTrashed(): bool {
		return $this->trashedDate !== NULL && $this->trashedDate !== "";
	}

	/**
	 * @return \DateTime|null
	 */
	public function getTrashedDate(): ?\DateTime {
		return $this->trashedDate;
	}

	/**
	 * @param \DateTime|null $trashedDate
	 */
	public function setTrashedDate( ?\DateTime $trashedDate ): void {
		$this->trashedDate = $trashedDate;
	}

    /**
     * @return array
     */
    abstract public function getProviderSettings();
}
