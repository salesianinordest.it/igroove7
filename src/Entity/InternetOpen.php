<?php

namespace App\Entity;

use Doctrine\DBAL\Types\Types;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Uid\Uuid;

/**
 * @ORM\Entity
 * @ORM\HasLifecycleCallbacks
 * @ORM\Entity(repositoryClass="App\Repository\InternetOpenRepository")
 */
class InternetOpen
{
    /**
     * @ORM\Column(type="uuid", unique=true)
     * @ORM\Id
     */
    private $id;

    /**
     * @ORM\Column(type="string")
     */
    private $type;

    /**
     * @ORM\Column(type="string")
     */
    private $account;

    /**
     * @ORM\Column(type="boolean")
     */
    private $permitPersonalDevices = false;

    /**
     * @ORM\Column(type="datetime")
     */
    private $close_at;

    /**
     * @ORM\Column(type="string")
     */
    private $activedBy;

    /**
     * @ORM\Column(type="boolean")
     */
    private $activationCompleated;

    /**
     * @ORM\Column(type="datetime", nullable=true)
     */
    private $updated_at;

    /**
     * @ORM\Column(type="datetime", nullable=true)
     */
    private $created_at;

    /**
     * Constructor.
     */
    public function __construct()
    {
        $this->id = Uuid::v4();
    }

    /**
     * @ORM\PrePersist
     */
    public function setCreatedAtValue()
    {
        $this->created_at = new \DateTime();
    }

    /**
     * @ORM\PreUpdate
     */
    public function setUpdatedAtValue()
    {
        $this->updated_at = new \DateTime();
    }

    /**
     * Get id.
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set type.
     *
     * @param string $type
     *
     * @return InternetOpen
     */
    public function setType($type)
    {
        $this->type = $type;

        return $this;
    }

    /**
     * Get type.
     *
     * @return string
     */
    public function getType()
    {
        return $this->type;
    }

    /**
     * Set account.
     *
     * @param string $account
     *
     * @return InternetOpen
     */
    public function setAccount($account)
    {
        $this->account = $account;

        return $this;
    }

    /**
     * Get account.
     *
     * @return string
     */
    public function getAccount()
    {
        return $this->account;
    }

    /**
     * @return bool
     */
    public function getPermitPersonalDevices()
    {
        return $this->permitPersonalDevices;
    }

    /**
     * @param bool $permitPersonalDevices
     */
    public function setPermitPersonalDevices($permitPersonalDevices)
    {
        $this->permitPersonalDevices = $permitPersonalDevices;
    }

    /**
     * Set close_at.
     *
     * @param \DateTime $closeAt
     *
     * @return InternetOpen
     */
    public function setCloseAt($closeAt)
    {
        $this->close_at = $closeAt;

        return $this;
    }

    /**
     * Get close_at.
     *
     * @return \DateTime
     */
    public function getCloseAt()
    {
        return $this->close_at;
    }

    /**
     * @return string
     */
    public function getActivedBy()
    {
        return $this->activedBy;
    }

    /**
     * @param string $activedBy
     */
    public function setActivedBy($activedBy)
    {
        $this->activedBy = $activedBy;
    }

    /**
     * @return bool
     */
    public function getActivationCompleated()
    {
        return $this->activationCompleated;
    }

    /**
     * @param bool $activationCompleated
     */
    public function setActivationCompleated($activationCompleated)
    {
        $this->activationCompleated = $activationCompleated;
    }

    /**
     * Set updated_at.
     *
     * @param \DateTime $updatedAt
     *
     * @return InternetOpen
     */
    public function setUpdatedAt($updatedAt)
    {
        $this->updated_at = $updatedAt;

        return $this;
    }

    /**
     * Get updated_at.
     *
     * @return \DateTime
     */
    public function getUpdatedAt()
    {
        return $this->updated_at;
    }

    /**
     * Set created_at.
     *
     * @param \DateTime $createdAt
     *
     * @return InternetOpen
     */
    public function setCreatedAt($createdAt)
    {
        $this->created_at = $createdAt;

        return $this;
    }

    /**
     * Get created_at.
     *
     * @return \DateTime
     */
    public function getCreatedAt()
    {
        return $this->created_at;
    }

    /**
     * function ldap_escape.
     *
     * @param string       $subject The subject string
     * @param bool         $dn      Treat subject as a DN if TRUE
     * @param string|array $ignore  Set of characters to leave untouched
     *
     * @return string The escaped string
     *
     * $user = 'Test , Name S.L';
     * if (!ldap_add($ds, "cn=".ldap_escape($user, TRUE).",".LDAP_DN_BASE, $info)) {
     *  include 'error_new_account.php';
     * }
     *
     * @author Chris Wright
     *
     * @version 2.0
     */
    public function ldapEscape($subject)
    {
        $subject = str_replace(
            [
                'à',
                'á',
                'â',
                'ã',
                'ä',
                'ç',
                'è',
                'é',
                'ê',
                'ë',
                'ì',
                'í',
                'î',
                'ï',
                'ñ',
                'ò',
                'ó',
                'ô',
                'õ',
                'ö',
                'ù',
                'ú',
                'û',
                'ü',
                'ý',
                'ÿ',
                'À',
                'Á',
                'Â',
                'Ã',
                'Ä',
                'Ç',
                'È',
                'É',
                'Ê',
                'Ë',
                'Ì',
                'Í',
                'Î',
                'Ï',
                'Ñ',
                'Ò',
                'Ó',
                'Ô',
                'Õ',
                'Ö',
                'Ù',
                'Ú',
                'Û',
                'Ü',
                'Ý',
            ],
            [
                'a',
                'a',
                'a',
                'a',
                'a',
                'c',
                'e',
                'e',
                'e',
                'e',
                'i',
                'i',
                'i',
                'i',
                'n',
                'o',
                'o',
                'o',
                'o',
                'o',
                'u',
                'u',
                'u',
                'u',
                'y',
                'y',
                'A',
                'A',
                'A',
                'A',
                'A',
                'C',
                'E',
                'E',
                'E',
                'E',
                'I',
                'I',
                'I',
                'I',
                'N',
                'O',
                'O',
                'O',
                'O',
                'O',
                'U',
                'U',
                'U',
                'U',
                'Y',
            ],
            $subject
        );
        $ignore = [
            ':',
            '/',
            '\\',
            ',',
            '=',
            '+',
            '<',
            '>',
            ';',
            '"',
            '#',
            "'",
            '(',
            ')',
            "'",
            "\x00",
            '?',
            '.',
            '-',
            '!',
        ];
        $subject = str_replace('  ', ' ', $subject);

        return trim(substr(str_replace($ignore, '', $subject), 0, 50));
    }

    public function isPermitPersonalDevices(): ?bool
    {
        return $this->permitPersonalDevices;
    }

    public function isActivationCompleated(): ?bool
    {
        return $this->activationCompleated;
    }
}
