<?php

namespace App\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Uid\Uuid;

/**
 * Subject.
 *
 * @ORM\Table()
 * @ORM\Entity
 * @ORM\Entity(repositoryClass="App\Repository\SubjectRepository")
 */
class Subject
{
    /**
     * @var string
     *
     * @ORM\Column(type="uuid", unique=true)
     * @ORM\Id
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="name", type="string", length=255)
     */
    private $name;

    /**
     * @var ArrayCollection|TeacherSubjectGroup[]
     *
     * @ORM\OneToMany(targetEntity="TeacherSubjectGroup", mappedBy="subject")
     **/
    private $teacherSubjectGroups;

    /**
     * @var Provider
     *
     * @ORM\ManyToOne(targetEntity="Provider", inversedBy="subjects")
     **/
    private $provider;

    /**
     * @var string
     * @ORM\Column(name="id_on_provider", type="string", nullable=true)
     */
    private $idOnProvider;

    public function __construct()
    {
        $this->teacherSubjectGroups = new ArrayCollection();

        $this->id = Uuid::v4();
    }

    /**
     * Get id.
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set name.
     *
     * @param string $name
     *
     * @return Subject
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get name.
     *
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * @return ArrayCollection|TeacherSubjectGroup[]
     */
    public function getTeacherSubjectGroups()
    {
        return $this->teacherSubjectGroups;
    }

    public function setTeacherSubjectGroups(ArrayCollection $teacherSubjectGroups)
    {
        $this->teacherSubjectGroups = $teacherSubjectGroups;
    }

    public function getTeachers()
    {
        $teacherSubjectGroups = $this->getTeacherSubjectGroups();
        $teachers = [];
        foreach ($teacherSubjectGroups as $teacherSubjectGroup) {
            $teacher = $teacherSubjectGroup->getTeacher();
            if (!$teacher instanceof Teacher || '' == $teacher->getId() || isset($teachers[(string)$teacher->getId()])) {
                continue;
            }

            $teachers[(string)$teacher->getId()] = $teacher;
        }

        return $teachers;
    }

    public function getGroups()
    {
        $teacherSubjectGroups = $this->getTeacherSubjectGroups();
        $groups = [];
        foreach ($teacherSubjectGroups as $teacherSubjectGroup) {
            $group = $teacherSubjectGroup->getGroup();
            if (!$group instanceof Group || '' == $group->getId() || isset($groups[(string)$group->getId()])) {
                continue;
            }

            $groups[(string)$group->getId()] = $group;
        }

        return $groups;
    }

    /**
     * @return Provider
     */
    public function getProvider()
    {
        return $this->provider;
    }

    /**
     * @param Provider $provider
     */
    public function setProvider($provider)
    {
        $this->provider = $provider;
    }

    /**
     * @return string
     */
    public function getIdOnProvider()
    {
        return $this->idOnProvider;
    }

    /**
     * @param string $idOnProvider
     */
    public function setIdOnProvider($idOnProvider)
    {
        $this->idOnProvider = $idOnProvider;
    }

    public function isSame(\App\ImporterFilter\ImportedEntity\Subject $importedSubject)
    {
        if ($importedSubject->getName() != $this->getName()) {
            return false;
        }

        return true;
    }

    public function addTeacherSubjectGroup(TeacherSubjectGroup $teacherSubjectGroup): self
    {
        if (!$this->teacherSubjectGroups->contains($teacherSubjectGroup)) {
            $this->teacherSubjectGroups[] = $teacherSubjectGroup;
            $teacherSubjectGroup->setSubject($this);
        }

        return $this;
    }

    public function removeTeacherSubjectGroup(TeacherSubjectGroup $teacherSubjectGroup): self
    {
        if ($this->teacherSubjectGroups->contains($teacherSubjectGroup)) {
            $this->teacherSubjectGroups->removeElement($teacherSubjectGroup);
            // set the owning side to null (unless already changed)
            if ($teacherSubjectGroup->getSubject() === $this) {
                $teacherSubjectGroup->setSubject(null);
            }
        }

        return $this;
    }
}
