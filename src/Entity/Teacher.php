<?php

namespace App\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;

/**
 * Teacher.
 *
 * @ORM\Entity
 * @ORM\Entity(repositoryClass="App\Repository\TeacherRepository")
 * @UniqueEntity("fiscalCode")
 * @UniqueEntity("username")
 * @UniqueEntity("email")
 */
class Teacher extends PersonAbstract
{
    /**
     * @var ArrayCollection|TeacherSubjectGroup[]
     *
     * @ORM\OneToMany(targetEntity="TeacherSubjectGroup", mappedBy="teacher")
     **/
    private $teacherSubjectGroups;

    /**
     * @var ArrayCollection|Provider[]
     * @ORM\ManyToMany(targetEntity="Provider", inversedBy="additionalTeachers")
     */
    protected $additionalProviders;

	/**
	 * @ORM\OneToMany(targetEntity="MdmDevice", mappedBy="teacherOwner")
	 * @var MdmDevice[]|ArrayCollection
	 */
	protected $mdmDevices;

    /**
     * Teacher constructor.
     *
     * @param $id
     */
    public function __construct($fiscalCode)
    {
        $this->teacherSubjectGroups = new \Doctrine\Common\Collections\ArrayCollection();
        parent::__construct($fiscalCode);
        $this->additionalProviders = new ArrayCollection();
        $this->mdmDevices = new ArrayCollection();
    }

    public function __clone()
    {
        parent::__clone();
        $this->teacherSubjectGroups = clone $this->teacherSubjectGroups;
    }

    /**
     * @return ArrayCollection|TeacherSubjectGroup[]
     */
    public function getTeacherSubjectGroups()
    {
        return $this->teacherSubjectGroups;
    }

    public function setTeacherSubjectGroups(ArrayCollection $teacherSubjectGroups)
    {
        $this->teacherSubjectGroups = $teacherSubjectGroups;
    }

    /**
     * @return ArrayCollection|Subject[]
     */
    public function getSubjects()
    {
        $teacherSubjectGroups = $this->getTeacherSubjectGroups();
        $subjects = [];
        foreach ($teacherSubjectGroups as $teacherSubjectGroup) {
            $subject = $teacherSubjectGroup->getSubject();
            if (!$subject instanceof Subject || '' == $subject->getId() || isset($subjects[(string)$subject->getId()])) {
                continue;
            }

            $subjects[(string)$subject->getId()] = $subject;
        }

        return new \Doctrine\Common\Collections\ArrayCollection($subjects);
    }

    /**
     * @return ArrayCollection|Group[]
     */
    public function getGroups()
    {
        $teacherSubjectGroups = $this->getTeacherSubjectGroups();
        $groups = [];
        foreach ($teacherSubjectGroups as $teacherSubjectGroup) {
            $group = $teacherSubjectGroup->getGroup();
            if (!$group instanceof Group || '' == $group->getId() || isset($groups[(string)$group->getId()])) {
                continue;
            }

            $groups[(string)$group->getId()] = $group;
        }

        return new \Doctrine\Common\Collections\ArrayCollection($groups);
    }

    /**
     * @return bool
     */
    public function isSame(\App\ImporterFilter\ImportedEntity\Teacher $importedTeacher)
    {
        if ($this->fiscalCode != $importedTeacher->getFiscalCode()) {
            return false;
        }

        if ($this->firstname != $importedTeacher->getFirstName()) {
            return false;
        }

        if ($this->lastname != $importedTeacher->getLastName()) {
            return false;
        }

        if (null !== $importedTeacher->getEmail() && $this->email != $importedTeacher->getEmail()) {
            return false;
        }
        if (null !== $importedTeacher->getUsername() && strtolower($this->username) != strtolower($importedTeacher->getUsername())) {
            return false;
        }
        if (null !== $importedTeacher->getPassword() && $this->starting_password != $importedTeacher->getPassword()) {
            return false;
        }

        return true;
    }

    /**
     * @return array
     */
    public function getProviderSettings()
    {
        if (!$this->provider instanceof Provider) {
            return Provider::$settingsFieldNulled;
        }

        return $this->provider->getTeacherSettings();
    }

    public function addTeacherSubjectGroup(TeacherSubjectGroup $teacherSubjectGroup): self
    {
        if (!$this->teacherSubjectGroups->contains($teacherSubjectGroup)) {
            $this->teacherSubjectGroups[] = $teacherSubjectGroup;
            $teacherSubjectGroup->setTeacher($this);
        }

        return $this;
    }

    public function removeTeacherSubjectGroup(TeacherSubjectGroup $teacherSubjectGroup): self
    {
        if ($this->teacherSubjectGroups->contains($teacherSubjectGroup)) {
            $this->teacherSubjectGroups->removeElement($teacherSubjectGroup);
            // set the owning side to null (unless already changed)
            if ($teacherSubjectGroup->getTeacher() === $this) {
                $teacherSubjectGroup->setTeacher(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection|Provider[]
     */
    public function getAdditionalProviders(): Collection
    {
        return $this->additionalProviders;
    }

    public function addAdditionalProvider(Provider $additionalProvider): self
    {
        if (!$this->additionalProviders->contains($additionalProvider)) {
            $this->additionalProviders[] = $additionalProvider;
        }

        return $this;
    }

    public function removeAdditionalProvider(Provider $additionalProvider): self
    {
        if ($this->additionalProviders->contains($additionalProvider)) {
            $this->additionalProviders->removeElement($additionalProvider);
        }

        return $this;
    }

    /**
     * @return Collection<int, MdmDevice>
     */
    public function getMdmDevices(): Collection
    {
        return $this->mdmDevices;
    }

    public function addMdmDevice(MdmDevice $mdmDevice): static
    {
        if (!$this->mdmDevices->contains($mdmDevice)) {
            $this->mdmDevices->add($mdmDevice);
            $mdmDevice->setTeacherOwner($this);
        }

        return $this;
    }

    public function removeMdmDevice(MdmDevice $mdmDevice): static
    {
        if ($this->mdmDevices->removeElement($mdmDevice)) {
            // set the owning side to null (unless already changed)
            if ($mdmDevice->getTeacherOwner() === $this) {
                $mdmDevice->setTeacherOwner(null);
            }
        }

        return $this;
    }
}
