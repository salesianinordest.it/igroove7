<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;
use Symfony\Component\Validator\Constraints as Assert;
use Symfony\Component\Validator\Context\ExecutionContextInterface;

/**
 * DeviceIp.
 *
 * @ORM\Table()
 * @ORM\Entity
 * @ORM\Entity(repositoryClass="App\Repository\DeviceIpRepository")
 * @UniqueEntity("ip", message="L'ip scelto è già in uso.")
 */
class DeviceIp
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var Device
     * @ORM\ManyToOne(targetEntity="Device", inversedBy="ips")
     */
    private $mac;

    /**
     * @var int
     *
     * @ORM\Column(name="ip", type="integer", unique=true, options={"unsigned":true})
     */
    private $ip;

    /**
     * @var MikrotikPool
     *
     * @ORM\ManyToOne(targetEntity="MikrotikPool", inversedBy="deviceIps")
     */
    private $mikrotikPool;

    /**
     * Get id.
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @return Device
     */
    public function getMac()
    {
        return $this->mac;
    }

    /**
     * @param Device $mac
     */
    public function setMac($mac)
    {
        $this->mac = $mac;
    }

    /**
     * Set ip.
     *
     * @param int $ip
     *
     * @return DeviceIp
     */
    public function setRawIp($ip)
    {
        $this->ip = $ip;

        return $this;
    }

    /**
     * Get ip.
     *
     * @return int
     */
    public function getRawIp()
    {
        return $this->ip;
    }

    /**
     * Set ip from string with format xxx.xxx.xxx.xxx.
     *
     * @param int $ip
     *
     * @return DeviceIp
     */
    public function setIp($ip)
    {
        $this->ip = ip2long($ip);

        return $this;
    }

    /**
     * Get ip as string with format xxx.xxx.xxx.xxx.
     *
     * @return string
     */
    public function getIp()
    {
        return long2ip($this->ip);
    }

    /**
     * @return MikrotikPool
     */
    public function getMikrotikPool()
    {
        return $this->mikrotikPool;
    }

    /**
     * @param MikrotikPool|null $mikrotikPool
     */
    public function setMikrotikPool($mikrotikPool)
    {
        $this->mikrotikPool = $mikrotikPool;
    }

    /**
     * @Assert\Callback()
     */
    public static function validate(DeviceIp $deviceIp, ExecutionContextInterface $context)
    {
        $mikrotikPool = $deviceIp->getMikrotikPool();
        if (!$mikrotikPool instanceof MikrotikPool) {
            $context->buildViolation('Pool mikrotik non valido')
                ->addViolation();

            return;
        }

        if ($deviceIp->getRawIp() < $mikrotikPool->getIpStart() || $deviceIp->getRawIp() > $mikrotikPool->getIpEnd()) {
            $context->buildViolation("L'ip non può essere fuori dal range del pool mikrotik")
//                ->atPath('ips')
                ->addViolation();
        }
    }

    public function __toString()
    {
        return $this->getIp();
    }
}
