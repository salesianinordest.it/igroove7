<?php namespace App\RepositoryAction;

use App\Entity\Provider;
use App\Entity\Teacher;
use App\Exception\LdapUsernameMissingAndNotAutoCreated;
use App\Manager\PersonsAndGroups;
use Doctrine\ORM\EntityManagerInterface;

class TeacherRepositoryAction {

    protected $personsAndGroups;
    protected $em;

    /**
     * StudentRepositoryAction constructor.
     */
    public function __construct(PersonsAndGroups $personsAndGroups, EntityManagerInterface $em) {
        $this->personsAndGroups = $personsAndGroups;
        $this->em = $em;
    }

    public function executeAfterCreate(Teacher $teacher) {
        $errors = [];

        try {
            $this->personsAndGroups->syncPersonWithLdapUser($teacher, true);
            $this->personsAndGroups->syncPersonMembershipsWithLdapGroups($teacher, true);
	        $this->personsAndGroups->syncProviderGroupLdapMembers($teacher->getProvider(), true);
	        $this->personsAndGroups->moveTeacherToLdapOU($teacher);
        } catch (\Exception $e) {
	        if(!$e instanceof LdapUsernameMissingAndNotAutoCreated) {
		        $previousException = $e;
		        $errors[] = $this->personsAndGroups->prepareFormErrorMessage( "Errore durante la creazione dell'utenza su AD", $e );
	        }
        }

        try {
            $this->personsAndGroups->syncPersonWithGoogleAppsUser($teacher, "", true);
            $this->personsAndGroups->moveTeacherToGoogleAppsOU($teacher, true);
            $this->personsAndGroups->syncProviderGroupWithGoogleGroup($teacher->getProvider(), true, true);

            foreach ($teacher->getGroups() as $group) {
                $this->personsAndGroups->syncGroupMembershipsWithGoogleGroupMemberships($group, true, true);
            }
        } catch (\Exception $e) {
	        $previousException = $e;
            $errors[] = $this->personsAndGroups->prepareFormErrorMessage("Errore durante la creazione dell'utenza su GApps", $e);
        }

        if(count($errors) > 0) {
            throw new \Exception(join(PHP_EOL, $errors), 0, $previousException);
        }
    }

    public function executeAfterUpdate(Teacher $teacher, Teacher $previousTeacherEntity) {
        $errors = [];
        $previousUsername = $previousTeacherEntity->getUsername();
        $previousEmail = $previousTeacherEntity->getEmail();
        $originalGroups = $previousTeacherEntity->getGroups();
        $toRemoveGroups = clone $originalGroups;

        $providersToUpdate = clone $teacher->getAdditionalProviders();
	    if($teacher->isTrashed() != $previousTeacherEntity->isTrashed() && (!$teacher->getProvider()->isTeacherTrashedLdapLeaveInProviderGroup() || !$teacher->getProvider()->isTeacherTrashedGoogleAppLeaveInProviderGroup())) {
		    if(!$providersToUpdate->contains($teacher->getProvider())) {
			    $providersToUpdate->add($teacher->getProvider());
		    }
	    } else {
		    foreach ( $previousTeacherEntity->getAdditionalProviders() as $previousProvider ) {
			    if ( $providersToUpdate->contains( $previousProvider ) ) {
				    $providersToUpdate->removeElement( $previousProvider );
			    } else {
				    $providersToUpdate->add( $previousProvider );
			    }
		    }
	    }

        if($teacher->getProvider()->getId() !== $previousTeacherEntity->getProvider()->getId()) {
            if(!$providersToUpdate->contains($teacher->getProvider())) {
                $providersToUpdate->add($teacher->getProvider());
            }

            if(!$providersToUpdate->contains($previousTeacherEntity->getProvider())) {
                $providersToUpdate->add($previousTeacherEntity->getProvider());
            }
        }

        try {
            $this->personsAndGroups->syncPersonWithLdapUser($teacher, TRUE);

	        if(!$teacher->isTrashed() || $teacher->getProvider()->getTeacherTrashedLdapBehaviour() != 2) { //do not sync groups and ou if the users has been deleted because of the trashed state
		        $this->personsAndGroups->moveTeacherToLdapOU( $teacher );

		        foreach ( $providersToUpdate as $provider ) {
			        $this->personsAndGroups->syncProviderGroupLdapMembers( $provider, TRUE );
		        }

		        foreach ( $teacher->getGroups() as $group ) {
			        if ( ! $originalGroups->contains( $group ) || $previousUsername != $teacher->getUsername() || ( $teacher->isTrashed() != $previousTeacherEntity->isTrashed() && ! $teacher->getProvider()->isTeacherTrashedLdapLeaveInClassesGroup() ) ) {
				        $this->personsAndGroups->syncGroupMembershipWithLdapGroupMembership( $group, TRUE );
			        }

			        $toRemoveGroups->removeElement( $group );
		        }

		        foreach ( $toRemoveGroups as $toRemoveGroup ) {
			        $this->personsAndGroups->syncGroupMembershipWithLdapGroupMembership( $toRemoveGroup, TRUE );
		        }

		        if ( $teacher->isTrashed() != $previousTeacherEntity->isTrashed() ) {
			        $this->personsAndGroups->syncLdapTrashedGroupForProvider( $teacher->getProvider(), TRUE, TRUE );
			        $this->personsAndGroups->syncTrashedLdapOUForProvider( $teacher->getProvider(), TRUE, TRUE );
		        }
	        }
        } catch (\Exception $e) {
			if(!$e instanceof LdapUsernameMissingAndNotAutoCreated) {
				$previousException = $e;
				$errors[] = $this->personsAndGroups->prepareFormErrorMessage("Errore durante l'aggiornamento dell'utenza su AD", $e);
			}
        }

        try {
            $this->personsAndGroups->syncPersonWithGoogleAppsUser($teacher, $previousEmail, true);

	        if(!$teacher->isTrashed() || $teacher->getProvider()->getTeacherTrashedGoogleAppBehaviour() != 2) { //do not sync groups and ou if the users has been deleted because of the trashed state
		        $this->personsAndGroups->moveTeacherToGoogleAppsOU( $teacher, TRUE );

		        foreach ( $providersToUpdate as $provider ) {
			        $this->personsAndGroups->syncProviderGroupWithGoogleGroup( $provider, TRUE, TRUE );
		        }

		        foreach ( $teacher->getGroups() as $group ) {
			        if ( ! $originalGroups->contains( $group ) || ( $teacher->isTrashed() != $previousTeacherEntity->isTrashed() && ! $teacher->getProvider()->isTeacherTrashedGoogleAppLeaveInClassesGroup() ) ) {
				        $this->personsAndGroups->syncGroupMembershipsWithGoogleGroupMemberships( $group, TRUE, TRUE );
			        }
		        }

		        foreach ( $toRemoveGroups as $toRemoveGroup ) {
			        $this->personsAndGroups->syncGroupMembershipsWithGoogleGroupMemberships( $toRemoveGroup, TRUE, TRUE );
		        }

		        if ( $teacher->isTrashed() != $previousTeacherEntity->isTrashed() ) {
			        $this->personsAndGroups->syncGoogleAppsTrashedGroupForProvider( $teacher->getProvider(), TRUE, TRUE );
			        $this->personsAndGroups->syncTrashedGoogleAppsOUForProvider( $teacher->getProvider(), TRUE, TRUE );
		        }
	        }
        } catch (\Exception $e) {
	        $previousException = $e;
            $errors[] = $this->personsAndGroups->prepareFormErrorMessage("Errore durante l'aggiornamento dell'utenza su GApps", $e);
        }

        if(count($errors) > 0) {
            throw new \Exception(join(PHP_EOL, $errors), 0, $previousException);
        }
    }

    public function executeBeforeRemove(Teacher $teacher, $deleteOnLdap=false, $deleteOnGApps=0, $gAppsOu="/") {
        $errors = [];
        $groups = $teacher->getGroups();
        foreach ($teacher->getTeacherSubjectGroups() as $teacherSubjectGroup) {
            $this->em->remove($teacherSubjectGroup);
        }

        $providers = $teacher->getProviders();
        $teacher->removeAllAdditionalProviders();

        try {
            $this->personsAndGroups->syncPersonMembershipsWithLdapGroups($teacher, TRUE);
            foreach ($providers as $provider) {
                $this->personsAndGroups->syncProviderGroupLdapMembers($provider, true);
            }

        } catch (\Exception $e) {
            $errors[] = $this->personsAndGroups->prepareFormErrorMessage("Errore durante l'eliminazione dell'utenza dai gruppi su AD", $e);
        }

        try {
            foreach ($groups as $group) {
                $this->personsAndGroups->syncGroupMembershipsWithGoogleGroupMemberships($group, true, TRUE);
                foreach ($providers as $provider) {
                    $this->personsAndGroups->syncProviderGroupWithGoogleGroup($provider, true, true);
                }
            }
        } catch (\Exception $e) {
            $errors[] = $this->personsAndGroups->prepareFormErrorMessage("Errore durante l'eliminazione dell'utenza dai gruppi su GApps", $e);
        }

        if($deleteOnLdap) {
            try {
                $this->personsAndGroups->removePersonLdapUser($teacher);
            } catch (\Exception $e) {
                $errors[] = $this->personsAndGroups->prepareFormErrorMessage("Errore durante l'eliminazione dell'utenza da AD", $e);
            }
        }

        if($deleteOnGApps == 2) {
            try {
                $this->personsAndGroups->moveTeacherToGoogleAppsOU($teacher, TRUE, $gAppsOu);
            } catch (\Exception $e) {
                $errors[] = $this->personsAndGroups->prepareFormErrorMessage("Errore durante lo spostamento dell'utenza da GApps", $e);
            }
        } elseif($deleteOnGApps == 1) {
            try {
                $this->personsAndGroups->removePersonGoogleAppsUser($teacher, TRUE);
            } catch (\Exception $e) {
                $errors[] = $this->personsAndGroups->prepareFormErrorMessage("Errore durante l'eliminazione dell'utenza da GApps", $e);
            }
        }

        if(count($errors) > 0) {
            throw new \Exception(join(PHP_EOL, $errors));
        }
    }



    public function executeBeforeRemoveFromAdditionalProvider(Teacher $teacher, Provider $provider) {
        foreach ($teacher->getTeacherSubjectGroups() as $teacherSubjectGroup) {
            if($teacherSubjectGroup->getProvider()->getId() === $provider->getId()) {
                $this->em->remove($teacherSubjectGroup);
            }
        }
    }
}