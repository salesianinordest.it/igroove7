<?php namespace App\RepositoryAction;

use App\Entity\Group;
use App\Entity\Provider;
use App\Entity\Student;
use App\Exception\LdapUsernameMissingAndNotAutoCreated;
use App\Manager\PersonsAndGroups;

class StudentRepositoryAction {

    protected $personsAndGroups;

    /**
     * StudentRepositoryAction constructor.
     */
    public function __construct(PersonsAndGroups $personsAndGroups) {
        $this->personsAndGroups = $personsAndGroups;
    }

    public function executeAfterCreate(Student $student) {
        foreach ($student->getNotManuallyManagedGroups() as $notManuallyManagedGroup) {
            if($notManuallyManagedGroup->getProvider()->getId() === $student->getProvider()->getId()) {
                $mainGroup = $notManuallyManagedGroup;
            }
        }

        $errors = [];

        try {
            $this->personsAndGroups->syncPersonWithLdapUser($student, true);
            $this->personsAndGroups->syncPersonMembershipsWithLdapGroups($student, true);
            $this->personsAndGroups->syncProviderGroupLdapMembers($student->getProvider(), false);

            if(isset($mainGroup) && $mainGroup instanceof Group) {
                $this->personsAndGroups->moveStudentToLdapOU($student, $mainGroup->getName());
            }
        } catch (\Exception $e) {
	        if(!$e instanceof LdapUsernameMissingAndNotAutoCreated) {
		        $previousException = $e;
		        $errors[] = $this->personsAndGroups->prepareFormErrorMessage( "Errore durante la creazione dell'utenza su AD", $e );
	        }
        }

        try {
            $this->personsAndGroups->syncPersonWithGoogleAppsUser($student, "", true);
            $this->personsAndGroups->syncProviderGroupWithGoogleGroup($student->getProvider(), false, true);
            foreach ($student->getGroups() as $group) {
                $this->personsAndGroups->syncGroupMembershipsWithGoogleGroupMemberships($group, false, true);
            }
            if(isset($mainGroup) && $mainGroup instanceof Group) {
                $this->personsAndGroups->moveStudentToGoogleAppsOU($student, $mainGroup->getName(), true);
            }
        } catch (\Exception $e) {
	        $previousException = $e;
            $errors[] = $this->personsAndGroups->prepareFormErrorMessage("Errore durante la creazione dell'utenza su GApps", $e);
        }

        if(count($errors) > 0) {
            throw new \Exception(join(PHP_EOL, $errors), 0, $previousException);
        }
    }

    public function executeAfterUpdate(Student $student, Student $previousStudentEntity) {
        $errors = [];
        $previousUsername = $previousStudentEntity->getUsername();
        $previousEmail = $previousStudentEntity->getEmail();
        $originalGroups = $previousStudentEntity->getMemberOf();
        $toRemoveGroups = clone $originalGroups;

        $providersToUpdate = clone $student->getAdditionalProviders();
		if($student->isTrashed() != $previousStudentEntity->isTrashed() && (!$student->getProvider()->isStudentTrashedLdapLeaveInProviderGroup() || !$student->getProvider()->isStudentTrashedGoogleAppLeaveInProviderGroup())) {
			if(!$providersToUpdate->contains($student->getProvider())) {
				$providersToUpdate->add($student->getProvider());
			}
		} else {
			foreach ($previousStudentEntity->getAdditionalProviders() as $previousProvider) {
				if($providersToUpdate->contains($previousProvider)) {
					$providersToUpdate->removeElement($previousProvider);
				} else {
					$providersToUpdate->add($previousProvider);
				}
			}
		}

	    if($student->getProvider()->getId() !== $previousStudentEntity->getProvider()->getId()) {
		    if(!$providersToUpdate->contains($student->getProvider())) {
			    $providersToUpdate->add($student->getProvider());
		    }

		    if(!$providersToUpdate->contains($previousStudentEntity->getProvider())) {
			    $providersToUpdate->add($previousStudentEntity->getProvider());
		    }
	    }

	    $previousException = null;
	    try {
            $this->personsAndGroups->syncPersonWithLdapUser($student, TRUE);

			if(!$student->isTrashed() || $student->getProvider()->getStudentTrashedLdapBehaviour() != 2) { //do not sync groups or ou if the users has been deleted because of the trashed state
				foreach ($providersToUpdate as $provider) {
					$this->personsAndGroups->syncProviderGroupLdapMembers($provider, false);
				}

				foreach ($student->getMemberOf() as $group) {
					if(!$originalGroups->contains($group) || $previousUsername != $student->getUsername() || ($student->isTrashed() != $previousStudentEntity->isTrashed() && !$student->getProvider()->isStudentTrashedLdapLeaveInClassesGroup())) {
						$this->personsAndGroups->syncGroupMembershipWithLdapGroupMembership($group, false);
					}

					if(!$group->getManageManually() && $student->getProvider()->getId() === $group->getProvider()->getId()) {
						$this->personsAndGroups->moveStudentToLdapOU($student, $group->getName());
					}

					$toRemoveGroups->removeElement($group);
				}

				foreach ($toRemoveGroups as $toRemoveGroup) {
					$this->personsAndGroups->syncGroupMembershipWithLdapGroupMembership($toRemoveGroup, false);
				}

				if($student->isTrashed() != $previousStudentEntity->isTrashed()) {
					$this->personsAndGroups->syncLdapTrashedGroupForProvider($student->getProvider(), false, true);
					$this->personsAndGroups->syncTrashedLdapOUForProvider($student->getProvider(), false, true);
				}
			}

        } catch (\Exception $e) {
		    if(!$e instanceof LdapUsernameMissingAndNotAutoCreated) {
			    $previousException = $e;
			    $errors[] = $this->personsAndGroups->prepareFormErrorMessage( "Errore durante l'aggiornamento dell'utenza su AD", $e );
		    }
        }


        try {
            $this->personsAndGroups->syncPersonWithGoogleAppsUser($student, $previousEmail, true);

	        if(!$student->isTrashed() || $student->getProvider()->getStudentTrashedGoogleAppBehaviour() != 2) { //do not sync groups or ou if the users has been deleted because of the trashed state
		        foreach ( $providersToUpdate as $provider ) {
			        $this->personsAndGroups->syncProviderGroupWithGoogleGroup( $provider, FALSE, TRUE );
		        }

		        foreach ( $student->getMemberOf() as $group ) {
			        if ( ! $originalGroups->contains( $group ) || ( $student->isTrashed() != $previousStudentEntity->isTrashed() && ! $student->getProvider()->isStudentTrashedGoogleAppLeaveInClassesGroup() ) ) {
				        $this->personsAndGroups->syncGroupMembershipsWithGoogleGroupMemberships( $group, FALSE, TRUE );

				        if ( ! $group->getManageManually() && $student->getProvider()->getId() === $group->getProvider()->getId() ) {
					        $this->personsAndGroups->moveStudentToGoogleAppsOU( $student, $group->getName(), TRUE );
				        }
			        }
		        }

		        foreach ( $toRemoveGroups as $toRemoveGroup ) {
			        $this->personsAndGroups->syncGroupMembershipsWithGoogleGroupMemberships( $toRemoveGroup, FALSE, TRUE );
		        }

		        if ( $student->isTrashed() != $previousStudentEntity->isTrashed() ) {
			        $this->personsAndGroups->syncGoogleAppsTrashedGroupForProvider( $student->getProvider(), FALSE, TRUE );
			        $this->personsAndGroups->syncTrashedGoogleAppsOUForProvider( $student->getProvider(), FALSE, TRUE );
		        }
	        }
        } catch (\Exception $e) {
	        $previousException = $e;
            $errors[] = $this->personsAndGroups->prepareFormErrorMessage("Errore durante l'aggiornamento dell'utenza su GApps", $e);
        }

        if(count($errors) > 0) {
            throw new \Exception(join(PHP_EOL, $errors), 0, $previousException);
        }
    }

    public function executeBeforeRemove(Student $student, $deleteOnLdap=false, $deleteOnGApps=0, $gAppsOu="/") {
        $errors = [];
        $groups = $student->getMemberOf();
        $student->removeAllMemberOf();
        $providers = $student->getProviders();
        $student->removeAllAdditionalProviders();

        try {
            $this->personsAndGroups->syncPersonMembershipsWithLdapGroups($student, TRUE);
            foreach ($providers as $provider) {
                $this->personsAndGroups->syncProviderGroupLdapMembers($provider, false);
            }
        } catch (\Exception $e) {
            $errors[] = $this->personsAndGroups->prepareFormErrorMessage("Errore durante l'eliminazione dell'utenza dai gruppi su AD", $e);
        }

        try {
            foreach ($groups as $group) {
                $this->personsAndGroups->syncGroupMembershipsWithGoogleGroupMemberships($group, FALSE, TRUE);
            }
            foreach ($providers as $provider) {
                $this->personsAndGroups->syncProviderGroupWithGoogleGroup($provider, false, true);
            }
        } catch (\Exception $e) {
            $errors[] = $this->personsAndGroups->prepareFormErrorMessage("Errore durante l'eliminazione dell'utenza dai gruppi su GApps", $e);
        }

        if($deleteOnLdap) {
            try {
                $this->personsAndGroups->removePersonLdapUser($student);
            } catch (\Exception $e) {
                $errors[] = $this->personsAndGroups->prepareFormErrorMessage("Errore durante l'eliminazione dell'utenza da AD", $e);
            }
        }

        if($deleteOnGApps == 2) {
            try {
                $this->personsAndGroups->moveStudentToGoogleAppsOU($student, $gAppsOu, TRUE);
            } catch (\Exception $e) {
                $errors[] = $this->personsAndGroups->prepareFormErrorMessage("Errore durante lo spostamento dell'utenza da GApps", $e);
            }
        } elseif($deleteOnGApps == 1) {
            try {
                $this->personsAndGroups->removePersonGoogleAppsUser($student, TRUE);
            } catch (\Exception $e) {
                $errors[] = $this->personsAndGroups->prepareFormErrorMessage("Errore durante l'eliminazione dell'utenza da GApps", $e);
            }
        }

        if(count($errors) > 0) {
            throw new \Exception(join(PHP_EOL, $errors));
        }
    }

    public function executeBeforeRemoveFromAdditionalProvider(Student $student, Provider $provider) {
        $groups = $student->getGroups();
        foreach ($groups as $group) {
            if($group->getProvider()->getId() === $provider->getId()) {
                $student->removeMemberOf($group);
            }
        }
    }
}