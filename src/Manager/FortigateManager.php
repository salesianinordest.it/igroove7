<?php

namespace App\Manager;

use App\Entity\Fortigate;
use Doctrine\ORM\EntityManager;
use Doctrine\ORM\EntityManagerInterface;
use Psr\Log\LoggerInterface;

class FortigateManager
{
    protected $internetAccessUsers;

    /**
     * @var LoggerInterface
     */
    private $logger;

    /**
     * @var EntityManager
     */
    protected $em;

    public function __construct(ConfigurationManager $configurationManager, EntityManagerInterface $em, LoggerInterface $logger)
    {
        $this->em = $em;
        $this->configurationManager = $configurationManager;
        $this->logger = $logger;
    }

    /**
     * kick off the users not currently active or in active group.
     */
    public function KickOffUsers()
    {
        $this->checkFortigates();
        $this->internetAccessUsers = $groupMembersUsers = $this->em->getRepository('App\Entity\LdapGroup')->getAllChildrenRecursiveUsers($this->configurationManager->getActiveDirectoryGeneratedGroupPrefix() . 'InternetAccess');
        $userList = [];
        foreach ($groupMembersUsers as $user) {
            $userList[] = strtolower($user);
        }

        $igrooveUsers = $this->em->getRepository('App\Entity\Student')->getAllStudentsUsername();

        foreach ($this->fortigates as $fortigate) {
            if (false == $fortigate->getStatus()) {
                continue;
            }
            $usersIntoFortigate = $this->getUsersInHotspot($fortigate);
            $usernameToRemove = array_diff(
                array_unique(array_keys($usersIntoFortigate)),
                array_unique($userList)
            );

            foreach ($usernameToRemove as $user) {
                if (0 == strlen(trim($user))) {
                    continue;
                }
                if (!array_search($user, $igrooveUsers)) {
                    continue;
                }
                if (array_key_exists(strtolower($user), $usersIntoFortigate)) {
                    $this->deAuth($fortigate, $usersIntoFortigate[strtolower($user)]);
                    echo "\r\n --> Fortigate kickoff " . $user;
                    $this->logger->info("Fortigate kickoff $user");
                }
            }
        }
    }

    /**
     * Get Current active users in hotspot of fotigate with specified ip.
     *
     * @return array
     */
    private function getUsersInHotspot(Fortigate $fortigate)
    {
        $response = $this->callApi($fortigate, '/api/v2/monitor/user/firewall', 'include_fsso=false&include_wad=false&ipv4=true&ipv6=true&vdom=root&usergroup=' . $fortigate->getUsersGroup());

        $users = json_decode($response->getBody(), true)['results'];

        $usersIntoFortinet = [];

        foreach ($users as $element) {
            $username = explode('@', $element['username']);
            if (empty($username) || '' == $username[0]) {
                continue;
            }

            $usersIntoFortinet[strtolower($username[0])] = $element;
        }

        return $usersIntoFortinet;
    }

    protected function checkFortigates()
    {
        $this->fortigates = $this->em->getRepository('App\Entity\Fortigate')->findAll();
        $client = new \GuzzleHttp\Client();

        foreach ($this->fortigates as $fg) {
            $url = 'https://' . $fg->getHost() . ':' . $fg->getPort() . '/api/v2/cmdb/firewall/address/?access_token=' . $fg->getToken();
            $fg->setStatus(false);
            try {
                $response = $client->request('GET', $url, ['verify' => false]);
                if ('200' == $response->getStatusCode()) {
                    $fg->setStatus(true);
                }
            } catch (\Exception $e) {
                $this->logger->error("[fortigate] Error connecting to fortigate {$fg->getHost()}: " . $e->getMessage(), ['error' => $e]);
            }
            $this->em->flush();
        }
    }

    private function callApi(Fortigate $fortigate, string $verb, string $queryParams = '')
    {
        $client = new \GuzzleHttp\Client();

        $url = 'https://' . $fortigate->getHost() . ':' . $fortigate->getPort() . $verb . '?access_token=' . $fortigate->getToken();
        if (strlen($queryParams) > 0) {
            $url .= '&' . $queryParams;
        }
        $response = $client->request('GET', $url, ['verify' => false]);

        return $response;
    }


    private function deAuth(Fortigate $fortigate, array $user)
    {
        $client = new \GuzzleHttp\Client();
        $verb = '/api/v2/monitor/user/firewall/deauth';
        $url = 'https://' . $fortigate->getHost() . ':' . $fortigate->getPort() . $verb . '?access_token=' . $fortigate->getToken() . '&vdom=root';
        $data = '{"users":[{"user_type":"firewall","id":' . $user['id'] . ',"ip":"' . $user['ipaddr'] . '","ip_version":"ip4","method":"firewall"}]}';
        try {
            $response = $client->request('POST', $url, ['verify' => false, 'body' => $data]);
        } catch (\Exception $e) {
            $this->logger->error("[fortigate] Error kickoff to fortigate {$fortigate->getHost()}: " . $e->getMessage(), ['error' => $e]);
        }

    }

}
