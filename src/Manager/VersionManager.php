<?php

namespace App\Manager;

use GuzzleHttp\Client;
use Symfony\Component\Yaml\Yaml;

class VersionManager
{
    public function __construct($projectDir, $cacheDir)
    {
        $this->versionFile = $projectDir . '/config/packages/igroove_version.yaml';
        $this->cachedDownloadFile = $cacheDir . '/igroove_version.yaml';
    }

    public function getVersions($refresh = false)
    {
        try {
            if (file_exists($this->versionFile)) {
                $versionInstalled = Yaml::parse(file_get_contents($this->versionFile));
            } else {
                $versionInstalled = ['parameters' => ['igroove_version_installed' => 0]];
            }
        } catch (\Exception $e) {
        }
        try {
            if (file_exists($this->cachedDownloadFile) and !$refresh) {
                $versionAvailable = Yaml::parse(file_get_contents($this->cachedDownloadFile));
            } else {
                $this->checkNewVersion();
                $versionAvailable = Yaml::parse(file_get_contents($this->cachedDownloadFile));
            }
        } catch (\Exception $e) {
        }

        return [
            'versionInstalled' => $versionInstalled['parameters']['versionInstalled'],
            'versionDateInstalled' => $versionInstalled['parameters']['versionDate'],
            'versionAvailable' => $versionAvailable['parameters']['versionInstalled'],
            'versionDateAvailable' => $versionAvailable['parameters']['versionDate'],
        ];
    }

    public function checkNewVersion()
    {
        $guzzleClient = new Client();
        try {
            $response = $guzzleClient->request('get', 'https://gitlab.com/salesianinordest.it/igroove7/raw/master/config/packages/igroove_version.yaml');
            $versionAvailable = trim((string)$response->getBody());
            file_put_contents($this->cachedDownloadFile, $versionAvailable);
        } catch (BadResponseException $e) {
            echo "\r\nOps Error getting new version";
        }
    }
}
