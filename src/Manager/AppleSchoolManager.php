<?php

namespace App\Manager;

use App\Entity\AppleSchool;
use App\Entity\Group;
use App\Entity\LdapUser;
use App\Entity\Provider;
use Doctrine\ORM\EntityManagerInterface;
use Psr\Log\LoggerInterface;

class AppleSchoolManager
{
    protected $staffInserted = [];
    protected $subjectsPerClassroom = [];

	/**
	 * @var string
	 */
	protected $cacheDirectory = "";

	/**
	 * @var EntityManagerInterface
	 */
	protected $em;
	/**
	 * @var LoggerInterface
	 */
	protected $logger;

	/**
	 * @var bool
	 */
	protected $inDebug;

    public function __construct(EntityManagerInterface $em, string $cacheDirectory, LoggerInterface $logger, $inDebug = false)
    {
        $this->em = $em;
	    $this->cacheDirectory = $cacheDirectory;
        $this->logger = $logger;
        $this->inDebug = $inDebug;
    }

    public function generateAndUploadAllCsvFiles()
    {
        $this->logger->info('[appleSchool] Start generating & uploading all the csv');

        $appleSchools = $this->em->getRepository('App\Entity\AppleSchool')->findAll();
        foreach ($appleSchools as $appleSchool) {
            try {
                $this->generateAndUploadCsvFilesFor($appleSchool);
            } catch (\Exception $e) {
                $this->logger->error("[appleSchool] Error during the generation/upload of the csv file for the org {$appleSchool->getOrganizationName()}: ".$e->getMessage());
                if ($this->inDebug) {
                    throw $e;
                }
            }
        }
    }

    /**
     * @throws \Exception
     */
    public function generateAndUploadCsvFilesFor(AppleSchool $appleSchool)
    {
        if (!$appleSchool->getActive()) {
            return;
        }

        $this->logger->info("[appleSchool] Generating the csv for the org {$appleSchool->getOrganizationName()}");
	    $basePath = $this->cacheDirectory.$appleSchool->getOrganizationId()."/";
	    if((!is_dir($this->cacheDirectory) && !mkdir($this->cacheDirectory)) || (!is_dir($basePath) && !mkdir($basePath))) {
            throw new \Exception('Error creating the local cache directory');
        }

        //		$providers = $appleSchool->getProviders();
        $this->generateLocationsCsvFile($appleSchool, $basePath.'locations.csv');
        $this->generateStaffCsvFile($appleSchool, $basePath.'staff.csv');
        $this->generateStudentsCsvFile($appleSchool, $basePath.'students.csv');
        $this->generateCoursesAndClassesCsvFile($appleSchool, $basePath.'courses.csv', $basePath.'classes.csv');
        //		$this->generateCoursesCsvFile($appleSchool, $basePath."courses.csv");
        //		$this->generateClassesCsvFile($appleSchool, $basePath."classes.csv");
        $this->generateRostersCsvFile($appleSchool, $basePath.'rosters.csv');

	    $zipPath = $this->cacheDirectory.$appleSchool->getOrganizationId().".zip";
        if (file_exists($zipPath) && !unlink($zipPath)) {
            throw new \Exception('Error deleting the previous zip file');
        }

        $zip = new \ZipArchive();
        if (true !== $zip->open($zipPath, \ZipArchive::CREATE)) {
            throw new \Exception('Error creating the zip file');
        }

        foreach (['locations.csv', 'staff.csv', 'students.csv', 'courses.csv', 'classes.csv', 'rosters.csv'] as $filename) {
            $zip->addFile($basePath.$filename, $filename);
        }

        $zip->close();

        $connection = ssh2_connect('upload.appleschoolcontent.com', 22);
        if (false === $connection || !ssh2_auth_password($connection, $appleSchool->getSftpUsername(), $appleSchool->getSftpPassword())) {
            throw new \Exception('Error connecting to the sftp');
        }

        $this->logger->info("[appleSchool] Uploading the csv for the org {$appleSchool->getOrganizationName()}");
        $sftp = ssh2_sftp($connection);
        $stream = @fopen("ssh2.sftp://{$sftp}/dropbox/{$appleSchool->getOrganizationId()}.zip", 'w');
        if (!$stream) {
            throw new \Exception('Impossible to create the sftp stream');
        }

        if (false === @fwrite($stream, @file_get_contents($zipPath))) {
            throw new \Exception('Error writing to the sftp connection');
        }

        @fclose($stream);

        $this->logger->info("[appleSchool] Upload compleated for the org {$appleSchool->getOrganizationName()}");
    }

    public function generateLocationsCsvFile(AppleSchool $appleSchool, $filePath)
    {
        $csvFile = fopen($filePath, 'w');
        fputcsv($csvFile, ['location_id', 'location_name']);
        foreach ($appleSchool->getLocations() as $location) {
            fputcsv($csvFile, [(string)$location->getId(), $location->getName()]);
        }
        fclose($csvFile);
    }

    public function generateStaffCsvFile(AppleSchool $appleSchool, $filePath)
    {
        $csvFile = fopen($filePath, 'w');
        fputcsv($csvFile, ['person_id', 'person_number', 'first_name', 'middle_name', 'last_name', 'email_address', 'sis_username', 'password_policy', 'location_id']);
        foreach ($appleSchool->getLocations() as $location) {
            foreach ($location->getProviders() as $provider) {
                /** @var Provider $provider */
                $teachers = $provider->getTeachers();
                foreach ($teachers as $teacher) {
	                $ldapUser = $this->em->getRepository(LdapUser::class)->findOneBy(['username' =>$teacher->getUsername()]);
                    if (!$ldapUser instanceof LdapUser) {
                        continue;
                    }

                    $ldapAttributes = json_decode($ldapUser->getAttributes());
                    if (!isset($ldapAttributes->objectGUID)) {
                        continue;
                    }

					if($teacher->isTrashed() && $provider->getTeacherTrashedAsmBehaviour() == 1) {
						continue;
					}

                    $this->staffInserted[] = (string)$teacher->getId();

                    fputcsv($csvFile, [
	                    (string)$teacher->getId(),
                        $this->generateUniqueIdFromObjectGUID($ldapAttributes->objectGUID),
                        $teacher->getFirstname(),
                        '',
                        $teacher->getLastname(),
                        $teacher->getEmail(),
                        $teacher->getUsername(),
                        '',
	                    (string)$location->getId(),
                    ]);
                }
            }
        }
        fclose($csvFile);
    }

    public function generateStudentsCsvFile(AppleSchool $appleSchool, $filePath)
    {
        $csvFile = fopen($filePath, 'w');
        fputcsv($csvFile, ['person_id', 'person_number', 'first_name', 'middle_name', 'last_name', 'grade_level', 'email_address', 'sis_username', 'password_policy', 'location_id']);
        //		fputcsv($csvFile, ['fake', 'fake', 'fake', '', 'fake', '', '', '', '', '1']);
        foreach ($appleSchool->getLocations() as $location) {
            foreach ($location->getProviders() as $provider) {
                /** @var Provider $provider */
                $students = $provider->getStudents();
	            $this->logger->info("-- adding {$students->count()} students of provider {$provider->getName()}");
                foreach ($students as $student) {
	                $ldapUser = $this->em->getRepository(LdapUser::class)->findOneBy(['username' =>$student->getUsername()]);
                    if (!$ldapUser instanceof LdapUser) {
                        continue;
                    }

                    $ldapAttributes = json_decode($ldapUser->getAttributes());
                    if (!isset($ldapAttributes->objectGUID)) {
                        continue;
                    }

	                if($student->isTrashed() && $provider->getStudentTrashedAsmBehaviour() == 1) {
		                continue;
	                }

                    fputcsv($csvFile, [
	                    (string)$student->getId(),
                        $this->generateUniqueIdFromObjectGUID($ldapAttributes->objectGUID),
                        $student->getFirstname(),
                        '',
                        $student->getLastname(),
                        '',
                        $student->getEmail(),
                        $student->getUsername(),
                        '',
                        (string)$location->getId(),
                    ]);
                }
            }
        }

        fclose($csvFile);
    }

    public function generateCoursesAndClassesCsvFile(AppleSchool $appleSchool, $coursesFilePath, $classesFilePath)
    {
        $courseCsvFile = fopen($coursesFilePath, 'w');
        $classesCsvFile = fopen($classesFilePath, 'w');
        fputcsv($courseCsvFile, ['course_id', 'course_number', 'course_name', 'location_id']);
	    $classesCsvHeaders = ['class_id', 'class_display_name', 'class_number', 'course_id', 'instructor_id'];
	    for($i=2; $i < 16; $i++) {
		    $classesCsvHeaders[] = "instructor_id_{$i}";
	    }
	    $classesCsvHeaders[] = 'location_id';
	    fputcsv($classesCsvFile, $classesCsvHeaders);
        foreach ($appleSchool->getLocations() as $location) {
            foreach ($location->getProviders() as $provider) {
                /** @var Provider $provider */
	            $classrooms = $this->em->getRepository(Group::class)->findBy(['provider' => $provider->getId(), 'manageManually' => false]);
                foreach ($classrooms as $classroom) {
                    fputcsv($courseCsvFile, [(string)$classroom->getId(), '', $classroom->getName(), (string)$location->getId()]);

	                $classroomTeachers = [];
	                //create classroom-subject combinations courses
                    if (1 === $appleSchool->getCourseType() || 2 === $appleSchool->getCourseType()) {
                        $tsgPerSubjects = [];
                        $this->subjectsPerClassroom[(string)$classroom->getId()] = [];

                        foreach ($classroom->getTeacherSubjectGroups() as $tsg) {
							if($tsg->getTeacher()->getProvider()->getAppleSchoolLocation() === null) {
								continue;
							}

                            if ($tsg->getTeacher()->getProvider()->getAppleSchoolLocation()->getId() !== $provider->getAppleSchoolLocation()->getId()) {
                                continue;
                            }

							if($tsg->getTeacher()->isTrashed() && !$provider->isTeacherTrashedAsmLeaveInClassesGroup()) {
								continue;
							}

                            if (!isset($tsgPerSubjects[(string)$tsg->getSubject()->getId()])) {
                                $tsgPerSubjects[(string)$tsg->getSubject()->getId()] = ['subjectName' => $tsg->getSubject()->getName(), 'teachers' => []];
                            }

                            if (false !== array_search((string)$tsg->getTeacher()->getId(), $this->staffInserted)) {
                                $tsgPerSubjects[(string)$tsg->getSubject()->getId()]['teachers'][] = (string)$tsg->getTeacher()->getId();

	                            if(!in_array((string)$tsg->getTeacher()->getId(), $classroomTeachers)) {
		                            $classroomTeachers[] = (string)$tsg->getTeacher()->getId();
	                            }
                            }

	                        $this->subjectsPerClassroom[(string)$classroom->getId()][(string)$tsg->getSubject()->getId()] = (string)$tsg->getSubject()->getId();
                        }

	                    foreach ($tsgPerSubjects as $subjectId => $tsgPerSubject) {
		                    $classesCsvData = [$classroom->getId()."-".$subjectId, $classroom->getName()." ".$tsgPerSubject['subjectName'], $tsgPerSubject['subjectName'], (string)$classroom->getId()];
		                    for($i = 0; $i < 15; $i++) {
			                    $classesCsvData[] = $tsgPerSubject['teachers'][$i] ?? '';
		                    }
		                    $classesCsvData[] = (string)$location->getId();
		                    fputcsv($classesCsvFile, $classesCsvData);
	                    }
                    } else {
	                    foreach($classroom->getTeacherSubjectGroups() as $tsg) {
		                    if (!in_array((string)$tsg->getTeacher()->getId(), $classroomTeachers) && array_search((string)$tsg->getTeacher()->getId(), $this->staffInserted) !== False
		                            && (!$tsg->getTeacher()->isTrashed() || $provider->isTeacherTrashedAsmLeaveInClassesGroup())) {
			                    $classroomTeachers[] = (string)$tsg->getTeacher()->getId();
		                    }
	                    }
                    }

	                //create classrooms courses with all the teachers in the classroom
	                if($appleSchool->getCourseType() === 0 || $appleSchool->getCourseType() === 2) {
		                $classesCsvData = [(string)$classroom->getId(), (string)$classroom->getName(), '', (string)$classroom->getId()];
		                for($i = 0; $i < 15; $i++) {
			                $classesCsvData[] = $classroomTeachers[$i] ?? '';
		                }
		                $classesCsvData[] = (string)$location->getId();
		                fputcsv($classesCsvFile, $classesCsvData);
	                }
                }
            }
        }

        //		fputcsv($courseCsvFile, ['fake', 'fake', 'fake', '1']);
        fclose($courseCsvFile);
    }

	public function generateCoursesCsvFile(AppleSchool $appleSchool, $filePath)
    {
        $csvFile = fopen($filePath, 'w');
        fputcsv($csvFile, ['course_id', 'course_number', 'course_name', 'location_id']);
        fputcsv($csvFile, ['fake', 'fake', 'fake', '1']);
        fclose($csvFile);
    }

	public function generateClassesCsvFile(AppleSchool $appleSchool, $filePath)
    {
        $csvFile = fopen($filePath, 'w');
        fputcsv($csvFile, ['class_id', 'class_number', 'course_id', 'instructor_id', 'instructor_id_2', 'instructor_id_3', 'location_id']);
        fputcsv($csvFile, ['fake', 'fake', 'fake', '', '', '', '1']);
        fclose($csvFile);
    }

    public function generateRostersCsvFile(AppleSchool $appleSchool, $filePath)
    {
        $csvFile = fopen($filePath, 'w');
        fputcsv($csvFile, ['roster_id', 'class_id', 'student_id']);
        foreach ($appleSchool->getLocations() as $location) {
            foreach ($location->getProviders() as $provider) {
                /** @var Provider $provider */
                $students = $provider->getStudents();
                foreach ($students as $student) {
                    $groups = $student->getNotManuallyManagedGroups();
                    if ($groups->count() < 1) {
                        continue;
                    } elseif (1 === $groups->count()) {
                        $class = $groups->first();
                        if ($class->getProvider()->getId() !== $provider->getId()) {
                            continue;
                        }
                    } else {
                        $class = null;
                        foreach ($groups as $group) {
                            if ($group->getProvider()->getId() === $provider->getId()) {
                                $class = $group;
                                break;
                            }
                        }

                        if (null === $class) {
                            continue;
                        }
                    }

					if($student->isTrashed() && !$provider->isStudentTrashedAsmLeaveInClassesGroup()) {
						continue;
					}

                    if (0 === $appleSchool->getCourseType() || 2 === $appleSchool->getCourseType()) {
                        fputcsv($csvFile, [$class->getId().'-'.$student->getId(), (string)$class->getId(), (string)$student->getId()]);
                    }

                    if (1 === $appleSchool->getCourseType() || 2 === $appleSchool->getCourseType()) {
                        if (isset($this->subjectsPerClassroom[(string)$class->getId()])) {
                            foreach ($this->subjectsPerClassroom[(string)$class->getId()] as $subjectId) {
                                fputcsv($csvFile, [$class->getId().'-'.$student->getId().'-'.$subjectId, $class->getId().'-'.$subjectId, (string)$student->getId()]);
                            }
                        }
                    }
                }
            }
        }
        //		fputcsv($csvFile, ['fake', 'fake', 'fake']);
        fclose($csvFile);
    }

    protected function generateUniqueIdFromObjectGUID($objectGUID)
    {
        switch (substr($objectGUID, 0, 1)) {
            case 'A':
                $uniqueUidStr = 2;
                break;

            case 'B':
                $uniqueUidStr = 3;
                break;

            case 'C':
                $uniqueUidStr = 4;
                break;

            case 'D':
                $uniqueUidStr = 5;
                break;

            case 'E':
                $uniqueUidStr = 6;
                break;

            case 'F':
                $uniqueUidStr = 7;
                break;

            case '9':
                $uniqueUidStr = 1;
                break;

            case '8':
                $uniqueUidStr = 0;
                break;

            default:
                $uniqueUidStr = substr($objectGUID, 0, 1);
        }

        $uniqueUidStr .= substr($objectGUID, 1, 7);

        return hexdec($uniqueUidStr);
    }
}
