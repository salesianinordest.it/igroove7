<?php

namespace App\Manager;

use App\Entity\LdapUser;
use LdapRecord\Auth\BindException;
use LdapRecord\Connection;
use LdapRecord\Container;
use LdapRecord\Ldap;
use LdapRecord\Models\ActiveDirectory\Group;
use LdapRecord\Models\ActiveDirectory\OrganizationalUnit;
use LdapRecord\Models\ActiveDirectory\User;

class MicrosoftLdapService
{
    /**
     * Active Directory Interface.
     *
     * @var Connection
     */
    protected $connection;

    /**
     * Active Directory Connection Configurations.
     *
     * @var array
     */
    protected $configurationAD;

    protected $baseDn;

//    /**
//     * Prefix for the AD group generated from iGroove.
//     *
//     * @var string
//     */
//    protected $generated_group_prefix;

    /**
     * Setup the Active Directory Configurations.
     *
     * @param $configurationAD
     *
     * @throws \Exception
     */
    public function setParameters($configurationAD)
    {
        $this->configurationAD = $configurationAD;
        $this->baseDn = str_replace('CN=Users,', '', $configurationAD['base_dn']);

//        $this->generated_group_prefix = $configurationAD['generated_group_prefix'];
    }

    public function connect()
    {
        if ($this->connection) {
            return;
        }
        try {
            $this->connection = new Connection([
                'hosts' => $this->configurationAD['domain_controllers'],

                'base_dn' => $this->baseDn,
                'username' => $this->configurationAD['admin_username'],
                'password' => $this->configurationAD['admin_password'],

                // Optional Configuration Options
                'port' => $this->configurationAD['use_ssl'] ? 636 : 389,
                'use_ssl' => $this->configurationAD['use_ssl'],
                'use_tls' => false,
                'version' => 3,
                'timeout' => 5,
                'follow_referrals' => false,
            ]);
        } catch (BindException $e) {
            $error = $e->getDetailedError();

            echo $error->getErrorCode();
            echo $error->getErrorMessage();
            echo $error->getDiagnosticMessage();
        }
        Container::addConnection($this->connection);
    }

    /**
     * Return all igroove users in AD.
     *
     * @return array
     */
    public function getAllUsers()
    {
        $fields = ['samaccountname',
            'dn',
            'displayname',
            'description',
            'department',
            'givenname',
            'sn',
            'mail',
            'homedirectory',
            'homedrive',
            'msradius-framedinterfaceid',
            'objectguid',
            'userprincipalname',
			'useraccountcontrol',
        ];

        try {
            $ldapMembers = User::all($fields);
        } catch (\Exception $e) {
            return [];
        }

        $ldapLists = [];
        if ($ldapMembers) {
            foreach ($ldapMembers as $user) {
                $v = $user->getOriginal();
                if ($user->getFirstAttribute('description') !== null && strpos($user->getFirstAttribute('description'), 'igroove' . $this->configurationAD['sync_suffix'])) {
                    $ldapLists[] = [
                        'username' => $v['samaccountname'][0],
                        'dn' => $user->getDn(),
                        'firstname' => $v['givenname'][0],
                        'lastname' => $v['sn'][0],
                        'distinguishedId' => $v['department'][0],
                        'parameters' => [
                            'email' => (isset($v['mail'][0]) ? $v['mail'][0] : ''),
                            'homedirectory' => (isset($v['homedirectory'][0]) ? $v['homedirectory'][0] : ''),
                            'homedrive' => (isset($v['homedrive'][0]) ? $v['homedrive'][0] : ''),
                            'description' => $user->getFirstAttribute('description'),
                            'radius-vlan' => (isset($v['msradius-framedinterfaceid'][0]) and (int)$v['msradius-framedinterfaceid'][0] > 0) ? (int)$v['msradius-framedinterfaceid'][0] : null,
                            'objectGUID' => $user->getConvertedGuid(),
                            'extendedUsername' => $user->getFirstAttribute('userprincipalname'),
                            'disabled' => $user->getFirstAttribute('useraccountcontrol') == 514 || $user->getFirstAttribute('useraccountcontrol') == 66050
                        ],
                    ];
                }
            }
        }

        return $ldapLists;
    }

    /**
     * Read the data of an user in AD.
     *
     * @param $username
     *
     * @return array
     *
     * @throws \Exception
     */
    public function getUser($username)
    {
	    $fields = ['samaccountname',
	               'dn',
	               'displayname',
	               'description',
	               'department',
	               'givenname',
	               'sn',
	               'mail',
	               'homedirectory',
	               'homedrive',
	               'msradius-framedinterfaceid',
	               'objectguid',
	               'userprincipalname',
	               'useraccountcontrol',
	    ];

        $user = User::select($fields)->where('samaccountname', '=', $username)->first();
	    if(!$user) {
		    throw new \Exception("Cannot find user with username {$username}");
	    }

        $v = $user->getOriginal();

        return [
	        'username' => $v['samaccountname'][0],
	        'dn' => $user->getDn(),
	        'firstname' => $v['givenname'][0],
	        'lastname' => $v['sn'][0],
	        'distinguishedId' => $v['department'][0],
	        'parameters' => [
		        'email' => (isset($v['mail'][0]) ? $v['mail'][0] : ''),
		        'homedirectory' => (isset($v['homedirectory'][0]) ? $v['homedirectory'][0] : ''),
		        'homedrive' => (isset($v['homedrive'][0]) ? $v['homedrive'][0] : ''),
		        'description' => $user->getFirstAttribute('description'),
		        'radius-vlan' => (isset($v['msradius-framedinterfaceid'][0]) and (int)$v['msradius-framedinterfaceid'][0] > 0) ? (int)$v['msradius-framedinterfaceid'][0] : null,
		        'objectGUID' => $user->getConvertedGuid(),
		        'extendedUsername' => $user->getFirstAttribute('userprincipalname'),
		        'disabled' => $user->getFirstAttribute('useraccountcontrol') == 514 || $user->getFirstAttribute('useraccountcontrol') == 66050
	        ],
        ];
    }

    /**
     * Get the ObjectGUID of an AD user from his username.
     *
     * @param $username
     *
     * @return string
     */
    public function getObjectGUIDFromUsername($username)
    {
        $user = User::where('samaccountname', '=', $username)->first();

        return $user->getConvertedGuid();
    }

    /**
     * Return the groups of an AD user.
     *
     * @param string $username
     *
     * @return array
     */
    public function getUserMembership($username)
    {
        $this->connect();
        $user = User::where('samaccountname', '=', $username)->first();
		if(!$user){
			return [];
		}
        $groups = $user->groups()->get();
        $members = [];
        foreach ($groups as $group) {
            $members[] = $group->cn[0];
        }

        return $members;
    }

    /**
     * Create a user in AD.
     *
     * @throws \Exception
     */
    public function createUser(LdapUser $ldapUser)
    {
        $this->connect();
        $ldapUserAttributes = json_decode($ldapUser->getAttributes(), true);

        $user = (new User())->inside($this->configurationAD['base_dn']);

        $user->cn = $this->generateUserDisplayName($ldapUser, $ldapUserAttributes);
	    $this->assignLdapUserDataToADUser($ldapUser, $user, $ldapUserAttributes);

        $user->save();
        $user->refresh();
        $user->unicodePwd = $ldapUser->getStartingPassword();
        $user->save();
	    $user->userAccountControl = isset($ldapUserAttributes['disabled']) && $ldapUserAttributes['disabled'] ? 514 : 512; //enable/disable status
        $user->save();
        $ldapUser->setStartingPassword('');
    }

    /**
     * Modify the data of an user in AD.
     *
     * @throws \Exception
     */
    public function modifyUser(LdapUser $ldapUser)
    {
        //    $this->connect();
        $ldapUserAttributes = json_decode($ldapUser->getAttributes(), true);

        if (isset($ldapUserAttributes['objectGUID']) && '' != $ldapUserAttributes['objectGUID']) {
            $user = User::findByGuid($ldapUserAttributes['objectGUID']);
            if (!$user) {
                $user = User::where('samaccountname', '=', $ldapUser->getUsername())->first();
            }
        } else {
            $user = User::where('samaccountname', '=', $ldapUser->getUsername())->first();
        }

	    if(!$user) {
		    throw new \Exception("Cannot find user with username {$ldapUser->getUsername()} to modify");
	    }

        $this->assignLdapUserDataToADUser($ldapUser, $user, $ldapUserAttributes);

        $user->save();
        $user->refresh();

        if ('' != $ldapUser->getStartingPassword()) {
            $user->unicodePwd = $ldapUser->getStartingPassword();
            $user->save();
            $ldapUser->setStartingPassword('');
        }

        $user->rename($this->generateUserDisplayName($ldapUser, $ldapUserAttributes));
        $user->save();

        $user->userAccountControl = isset($ldapUserAttributes['disabled']) && $ldapUserAttributes['disabled'] ? 514 : 512; //enable/disable status
        $user->save();
    }

	protected function assignLdapUserDataToADUser(LdapUser $ldapUser, User $user, ?array $ldapUserAttributes = null)
	{
		if(is_null($ldapUserAttributes)) {
			$ldapUserAttributes = json_decode($ldapUser->getAttributes(), true);
		}

		$user->sn = $ldapUser->getLastName();
		$user->givenname = $ldapUser->getFirstname();
		$user->samaccountname = $ldapUser->getUsername();
		$user->userPrincipalName = $ldapUserAttributes['extendedUsername'] ?? $ldapUser->getUsername().$this->configurationAD['account_suffix'];
		$user->department = $ldapUser->getDistinguishedId();
		$user->description = $ldapUserAttributes['description'];
		$user->mail = $ldapUserAttributes['email'];

		$user->setAttribute('msRADIUS-FramedInterfaceId', ((isset($ldapUserAttributes['radius-vlan'])) && (int)$ldapUserAttributes['radius-vlan'] > 0) ? $ldapUserAttributes['radius-vlan'] : null);

		if (($ldapUserAttributes['homedirectory']) && (strlen($ldapUserAttributes['homedirectory']) > 3)) {
			$user->homeDrive = $ldapUserAttributes['homedrive'];
			$user->homeDirectory = $ldapUserAttributes['homedirectory'];
		} else {
			$user->homeDrive = $user->homeDirectory = null;
		}
	}

    /**
     * Delete an user from AD.
     *
     * @param string $username
     *
     * @throws \Exception
     */
    public function removeUser($username)
    {
        $this->connect();
        $user = User::where('samaccountname', '=', $username)->first();
	    if(!$user) {
		    throw new \Exception("Cannot find user with username {$username} to delete");
	    }
        $user->delete();
    }

    /**
     * Change the password of a user in AD.
     *
     * @param $username
     * @param $password
     *
     * @throws \Exception
     */
    public function changeUserPassword($username, $password)
    {
        $user = User::where('samaccountname', '=', $username)->first();
	    if(!$user) {
		    throw new \Exception("Cannot find user with username {$username} to change the password");
	    }
        $user->unicodePwd = $password;
        $user->save();
    }

    /**
     * Check the validity of a user password.
     *
     * @param $username
     * @param $password
     *
     * @return bool
     */
    public function checkUserPassword($username, $password, $ignoreChangePasswordBindError = false)
    {
        $this->connect();

        return $this->connection->auth()->attempt($username.$this->configurationAD['account_suffix'], $password);
    }

    /**
     * List all groups in AD.
     *
     * @return array
     */
    public function getAllGroups()
    {
        $this->connect();
        $ldapGroups = Group::all();

        $groupNames = [];
        foreach ($ldapGroups as $group) {
            if ($group->getFirstAttribute('description') !== null && false !== strpos($group->getFirstAttribute('description'), 'igroove' . $this->configurationAD['sync_suffix'])) {
                $groupNames[] = $group->getFirstAttribute('name');
            }
        }

        return $groupNames;
    }

    /**
     * List all OU in AD.
     *
     * @return array
     */
    public function getAllOUs()
    {
        $ldapOUs = OrganizationalUnit::all();

        $ouNames = [];
        foreach ($ldapOUs as $ou) {
            $ouNames[] = $ou->getFirstAttribute('name');
        }

        return $ouNames;
    }

    /**
     * Return the groups of an AD group.
     *
     * @param string $groupName
     *
     * @return array
     */
    public function getGroupMembership($groupName)
    {
        $this->connect();
        $group = Group::where('cn', '=', $groupName)->first();
		if(!$group) {
			return [];
		}
        $groupMembersObj = $group->members()->get();

        $array = [];
        foreach ($groupMembersObj as $item) {
            if ('LdapRecord\Models\ActiveDirectory\Group' != get_class($item)) {
                continue;
            }
            $dn = $item->getFirstAttribute('distinguishedname');

            if ($dn === null || !strpos($dn, ',')) {
                continue;
            }

            $array[] = substr($dn, 3, strpos($dn, ',') - 3);
        }

        return $array;
    }

    /**
     * Create a group in AD.
     *
     * @param string $groupName
     *
     * @throws \Exception
     */
    public function createGroup($groupName)
    {
        $this->connect();
        $group = (new Group())->inside($this->configurationAD['base_dn']);
        $group->cn = $groupName;
        $group->samaccountname = $groupName;
        $group->description = 'Creato da igroove' . $this->configurationAD['sync_suffix'];
        $group->save();
    }

    /**
     * Rename a group in AD.
     *
     * @param string $groupName
     * @param string $newGroupName
     *
     * @throws \Exception
     */
    public function renameGroup($groupName, $newGroupName)
    {
        $group = Group::where('cn', '=', $groupName)->first();
		if(!$group) {
			throw new \Exception("Cannot find group with name {$groupName} in ldap to rename}");
		}
        $group->rename($newGroupName);
        $group->save();
    }

    /**
     * Delete a group from AD.
     *
     * @param string $groupName
     *
     * @throws \Exception
     */
    public function removeGroup($groupName)
    {
        $group = Group::where('cn', '=', $groupName)->first();
	    if(!$group) {
		    throw new \Exception("Cannot find group with name {$groupName} in ldap to delete}");
	    }
        $group->delete();
    }

    /**
     * Update the users in an AD group.
     *
     * @param string $groupName
     * @param array $users
     *
     * @throws \Exception
     */
    public function updateUsersIntoGroup($groupName, $users)
    {
        $group = Group::where('cn', '=', $groupName)->first();
	    if(!$group) {
		    throw new \Exception("Cannot find group with name {$groupName} in ldap to update the user members}");
	    }
        $groupMembersObj = $group->members()->get();
        $groupMembers = [];
        foreach ($groupMembersObj as $item) {
            if ('LdapRecord\Models\ActiveDirectory\User' != get_class($item)) {
                continue;
            }
            $groupMembers[] = $item->getFirstAttribute('samaccountname');
        }

        $toRemove = array_diff($groupMembers, $users);
        $toAdd = array_diff($users, array_intersect($groupMembers, $users));

        foreach ($toRemove as $username) {
            $user = User::where('samaccountname', '=', $username)->first();
            if ($user) {
                $group->members()->detach($user);
            }
        }
        foreach ($toAdd as $username) {
            $user = User::where('samaccountname', '=', $username)->first();
            if ($user) {
                $group->members()->attach($user);
            }
        }
    }

    /**
     * Update the AD groups into a group.
     *
     * @param string $groupName
     * @param array $groups
     */
    public function updateGroupsIntoGroup($groupName, $groups)
    {
        $mainGroup = Group::where('cn', '=', $groupName)->first();
	    if(!$mainGroup) {
		    throw new \Exception("Cannot find group with name {$groupName} in ldap to update the group members}");
	    }
        $groupMembersObj = $mainGroup->members()->get();
        $groupMembers = [];
        foreach ($groupMembersObj as $item) {
            if ('LdapRecord\Models\ActiveDirectory\Group' != get_class($item)) {
                continue;
            }

            $groupMembers[] = $item->getFirstAttribute('cn');
        }

        $toRemove = array_diff($groupMembers, $groups);
        $toAdd = array_diff($groups, array_intersect($groupMembers, $groups));
        $errors = [];

        foreach ($toRemove as $groupName) {
            $group = Group::where('cn', '=', $groupName)->first();
            if ($group) {
                $mainGroup->members()->detach($group);
            }
        }

        foreach ($toAdd as $groupName) {
            $group = Group::where('cn', '=', $groupName)->first();
            if ($group) {
                $mainGroup->members()->attach($group);
            }
        }
    }

    /**
     * Check if the ou specified exists in Ldap/Ad.
     *
     * @param string $ouName
     *
     * @return bool
     */
    public function ouExists($ouName, $path = '')
    {
	    $parent = $this->prepareOUPath($path, $this->configurationAD['base_dn']);
        $cn = 'OU=' . $ouName . ',' . $parent;
        $ou = $this->connection->query()->find($cn);

        return null != $ou;
    }

	/**
	 * Recurvively create the specified ou in Ldap/Ad
	 *
	 * @param string $ouName
	 * @param string $path
	 *
	 * @throws \Exception
	 */
	public function createOu($ouName, $path = '')
	{
		$path = $this->cleanOUPath($path);
		if(trim($path) != "") {
			$explodedPath = explode("/", $path);
			$pathAccumulator = "";
			foreach ($explodedPath as $currentPath) {
				if(!$this->ouExists($currentPath, $pathAccumulator)) {
					$this->doCreateOu($currentPath, $pathAccumulator);
				}
				$pathAccumulator .= ($pathAccumulator!=""?"/":"").$currentPath;
			}
		}

		$this->doCreateOu($ouName, $path);
	}

	/**
	 * Create the specified ou in Ldap/Ad.
	 *
	 * @param string $ouName
	 * @param string $path
	 *
	 * @throws \Exception
	 */
	protected function doCreateOu($ouName, $path = '')
	{
		$parent = $this->prepareOUPath($path, $this->configurationAD['base_dn']);
		$ou = (new OrganizationalUnit())->inside($parent);
		$ou->ou = $ouName;
		try {
			$ou->save();
		} catch (\Exception $exception) {
			throw new \Exception("Error creating ldap OU {$ouName} inside {$parent}: ".$exception->getMessage());
		}
	}

    /**
     * Delete a group from AD.
     *
     * @param string $groupName
     *
     * @throws \Exception
     */
    public function removeOu($ouName)
    {
        $ou = OrganizationalUnit::where('ou', '=', $ouName)->first();
	    if(!$ou) {
		    throw new \Exception("Cannot find ou with name {$ouName} in ldap to delete}");
	    }
        $ou->delete();
    }

    /**
     * Move the specified username into the specified ou.
     *
     * @param string $ouName
     * @param string $username
     * @param string $path
     *
     * @throws \Exception
     */
    public function moveUserIntoOu($ouName, $username, $path = '')
    {
	    $parent = $this->prepareOUPath($path, $this->configurationAD['base_dn']);
        $dn = 'OU=' . $ouName . ',' . $parent;
        $ou = OrganizationalUnit::find($dn);
        $user = User::where('samaccountname', '=', $username)->first();
		if(!$user) {
			throw new \Exception("Cannot find user with username {$username} in ldap to move in ou {$ouName}");
		}
        $user->move($ou);
    }

    /**
     * Move the specified users username into the specified ou.
     *
     * @param string $ouName
     * @param array $users
     * @param string $path
     */
    public function updateUsersIntoOu($ouName, $users, $path = '')
    {
        foreach ($users as $username) {
            $this->moveUserIntoOu($ouName, $username, $path);
        }
    }

	/**
	 * Move the specified group to the specified ou.
	 *
	 * @param $ouName
	 * @param $groupName
	 * @param string $path
	 *
	 * @throws \Exception
	 */
    public function moveGroupIntoOu($ouName, $groupName, $path = '')
    {
	    $parent = $this->prepareOUPath($path, $this->configurationAD['base_dn']);
        $dn = 'OU=' . $ouName . ',' . $parent;
        $ou = OrganizationalUnit::find($dn);
        $group = Group::where('cn', '=', $groupName)->first();
	    if(!$group) {
		    throw new \Exception("Cannot find group with name {$groupName} in ldap to move in ou {$ouName}");
	    }
        $group->move($ou);
    }

    /**
     * Move the specified groups into the specified ou.
     *
     * @param string $ouName
     * @param array $groups
     * @param string $path
     */
    public function updateGroupsIntoOu($ouName, $groups, $path = '')
    {
        foreach ($groups as $group) {
            $this->moveGroupIntoOu($ouName, $group, $path);
        }
    }

    /**
     * Extract the OUs path from a dn AD path.
     *
     * @param $dnPath
     *
     * @return array
     */
    public function dnPathToOUList($dnPath)
    {
        $path = str_replace($this->configurationAD['base_dn'], '', $dnPath);
        $explodedPath = explode(',', $path);
        $ous = [];
        foreach ($explodedPath as $p) {
            if ('OU=' == substr($p, 0, 3)) {
                $ous[] = substr($p, 3);
            }
        }

        return $ous;
    }

	public function prepareOUPath($ouPath, $basePath)
	{
		$basePath = str_replace('CN=Users,', '', $basePath);
		$ouPath = $this->cleanOUPath($ouPath);
		if($ouPath == "") {
			return $basePath;
		}

		$explodedPath = explode("/", $ouPath);
		return 'OU=' . implode(",OU=", array_reverse($explodedPath)) . ',' . $basePath;
	}

	protected function cleanOUPath($ouPath)
	{
		if($ouPath == "") {
			return $ouPath;
		}

		if(str_starts_with($ouPath, "/")) {
			$ouPath = substr($ouPath, 1);
		}

		if(str_ends_with($ouPath, "/")) {
			$ouPath = substr($ouPath, 0, -1);
		}

		return $ouPath;
	}

	public function generateUserDisplayName(LdapUser $ldapUser, ?array $ldapAttributes = null) {
		if(is_null($ldapAttributes)) {
			$ldapUserAttributes = json_decode($ldapUser->getAttributes(), true);
		}

		$ldapUserAttributes['display_name'] = $ldapUserAttributes['display_name'] ?? $ldapUser->getLastName() . ' ' . $ldapUser->getFirstname();

		if (preg_match_all('/\d+/', $ldapUser->getUsername(), $matchedNumbers) > 0 && isset($matchedNumbers[0])) {
			$ldapUserAttributes['display_name'] .= " ". implode(' ', $matchedNumbers[0]);
		}

		return trim($ldapUserAttributes['display_name']);
	}

    /**
     * Refresh connection to AD.
     */
    public function refreshConnection()
    {
        // $this->connection->close();
        // $this->connect();
    }
}
