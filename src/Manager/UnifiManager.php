<?php

namespace App\Manager;

use Doctrine\ORM\EntityManager;
use Monolog\Logger;

class UnifiManager
{
    protected $controllerClient;

    public function __construct(ConfigurationManager $configurationManager, EntityManager $em, Logger $logger)
    {
        $this->em = $em;
        $this->configurationManager = $configurationManager;
        $this->logger = $logger;
    }

    public function getActiveClientsList()
    {
        $controllerClient = $this->getControllerClient();
        $clients = $controllerClient->list_clients();

        return is_array($clients) ? $clients : [];
    }

    public function getActiveDot1xUsers()
    {
        $controllerClient = $this->getControllerClient();

        $clients = $controllerClient->list_clients();
        if (!$clients) {
            return [];
        }

        $clientsByUsername = [];
        foreach ($clients as $client) {
            if (!isset($client->{'1x_identity'}) || '' == $client->{'1x_identity'}) {
                continue;
            }

            $username = strtolower($client->{'1x_identity'});
            if (!isset($clientsByUsername[$username])) {
                $clientsByUsername[$username] = [];
            }

            $clientsByUsername[$username][$client->mac] = $client;
        }

        return $clientsByUsername;
    }

    public function kickOffClientExpiredClients()
    {
        if (!$this->configurationManager->getWirelessUnifiKickExpired()) {
            return;
        }

        $enabledNetwork = null;
        if ('' != $this->configurationManager->getWirelessUnifiKickExpiredNetworks()) {
            $enabledNetwork = explode(',', $this->configurationManager->getWirelessUnifiKickExpiredNetworks());
        }

        $controllerClient = $this->getControllerClient();
        $internetAccessUsers = $this->em->getRepository('App\Entity\LdapGroup')->getAllChildrenRecursiveUsers($this->configurationManager->getActiveDirectoryGeneratedGroupPrefix().'InternetAccess');
        $activatedUsers = [];
        foreach ($internetAccessUsers as $user) {
            $activatedUsers[] = strtolower($user);
        }

        $igrooveStudents = $this->em->getRepository('App\Entity\Student')->getAllStudentsUsername();
        $bypassedMacAddress = $this->em->getRepository('App\Entity\Device')->getAllBypassedMac();
        foreach ($this->getActiveDot1xUsers() as $username => $devices) {
            if (in_array($username, $activatedUsers) || !in_array($username, $igrooveStudents)) {
                continue;
            }

            foreach ($devices as $mac => $device) {
                if (in_array($mac, $bypassedMacAddress) || (is_array($enabledNetwork) && !empty($enabledNetwork) && !in_array($device->essid, $enabledNetwork))) {
                    continue;
                }

                $this->logger->info("reconnect wifi user {$username} device {$mac}");
                $controllerClient->reconnect_sta($mac);
            }
        }
    }

    public function restartAllAps()
    {
        $controllerClient = $this->getControllerClient();
        foreach ($controllerClient->list_aps() as $ap) {
            $controllerClient->restart_ap($ap['mac']);
        }
    }

    /**
     * @return \UniFi_API\Client
     *
     * @throws \Exception
     */
    protected function getControllerClient()
    {
        if (!isset($this->controllerClient) || !$this->controllerClient instanceof \UniFi_API\Client) {
            if ('' == $this->configurationManager->getWirelessUnifiController() ||
                '' == $this->configurationManager->getWirelessUnifiUsername() ||
                '' == $this->configurationManager->getWirelessUnifiPassword()) {
                throw new \Exception('Invalid connection data for the unifi controller');
            }

            $controllerUrl = $this->configurationManager->getWirelessUnifiController();
            if ('https://' != substr($controllerUrl, 0, 8)) {
                $controllerUrl = 'https://'.$controllerUrl.':8443';
            }

            $this->controllerClient = new \UniFi_API\Client(
                $this->configurationManager->getWirelessUnifiUsername(),
                $this->configurationManager->getWirelessUnifiPassword(),
                $controllerUrl
            );

            $this->controllerClient->login();
        }

        return $this->controllerClient;
    }
}
