<?php

namespace App\Manager;

use Symfony\Component\Filesystem\Exception\IOException;
use Symfony\Component\Filesystem\Filesystem;
use Symfony\Component\Process\Process;

class ServiceRestarter
{
    protected $configurationManager;
    protected $mikrotikManager;

    public function __construct(ConfigurationManager $configurationManager, MikrotikManager $mikrotikManager)
    {
        $this->configurationManager = $configurationManager;
        $this->mikrotikManager = $mikrotikManager;
    }

    public function restart($service)
    {
        $messages[0] = 'Non esiste questo servizio';
        switch ($service) {
            case 'unifi':
                $messages = $this->restartUnifi();
                break;
            case 'mikrotik':
                $messages = $this->mikrotikManager->rebootMikrotiks();
                break;
            case 'radius':
                $messages = $this->restartRadius();
                break;
        }

        return $messages;
    }

    private function restartUnifi()
    {
        $script = '/usr/local/bin/unifi-reboot-ap';
        $username = $this->configurationManager->getWirelessUnifiUsername();
        $password = $this->configurationManager->getWirelessUnifiPassword();
        $ip = $this->configurationManager->getWirelessUnifiController();
        $toRun = "$script -c $ip -u $username -p '$password' -v v3 -s default";
        $process = new Process($toRun);
        $process->run();
        if (!$process->isSuccessful()) {
            $messages = explode("\n", 'ERRORE: '.$process->getErrorOutput());
        } else {
            $messages = explode("\n", $process->getOutput());
        }

        return $messages;
    }

    private function restartRadius()
    {
        $fs = new Filesystem();

        try {
            $fs->touch('/tmp/reboot.this');
            $messages = ['Rebooting (tra circa un minuto)'];
        } catch (IOException $e) {
            $messages = ['ERRORE in /tmp/reboot.this'];
        }

        return $messages;
    }
}
