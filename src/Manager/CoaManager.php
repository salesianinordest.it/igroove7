<?php

namespace App\Manager;

use GuzzleHttp\Client;
use Psr\Log\LoggerInterface;

class CoaManager
{
    /**
     * @var LoggerInterface
     */
    private $logger;

    public function __construct(ConfigurationManager $configurationManager, LoggerInterface $logger)
    {
        $this->configurationManager = $configurationManager;
        $this->logger = $logger;
    }

    /**
     * kick off the users not currently active or in active group.
     */
    public function KickOffUser(string $username)
    {
        if (strlen($this->configurationManager->getCoaEndpoint()) > 0) {
            $client = new Client();
            $this->logger->info('CoA request: kickoff of '.$username);
            $client->post(
                $this->configurationManager->getCoaEndpoint(),
                [
                    'query' => [
                        'pattern' => $this->configurationManager->getCoaPattern(),
                        'password' => $this->configurationManager->getCoaPassword(),
                        'username' => $username,
                    ],
                ]
            );
        }

	    if (strlen($this->configurationManager->getCoaDeActivationEndpoint()) > 0) {
		    $client = new Client();
		    $this->logger->info('CoA request: kickoff of ' . $username);
		    $client->post(
			    $this->configurationManager->getCoaDeActivationEndpoint(),
			    [
				    'json' => [
					    'key' => $this->configurationManager->getCoaPassword(),
					    'username' => $username
				    ]
			    ]
		    );

	    }
    }

    public function KickOffUsers(array $usernames)
    {
        foreach ($usernames as $username) {
            $this->KickOffUser($username);
        }
    }

	/**
	 * notify the activation of a user
	 */
	public function notifyUserActivation(string $username, bool $personalDevicesPermitted = false) {
		if (strlen($this->configurationManager->getCoaActivationEndpoint()) > 0) {
			$client = new Client();
			$this->logger->info('CoA request: notify activation of ' . $username);
			$client->post(
				$this->configurationManager->getCoaActivationEndpoint(),
				array(
					'json' => [
						'key' => $this->configurationManager->getCoaPassword(),
						'username' => $username,
						'personalDevicesEnabled' => $personalDevicesPermitted
					]
				)
			);

		}
	}

	/**
	 * @param string[]|array $usernames
	 * @return void
	 */
	public function notifyUsersActivation(array $usernames, bool $personalDevicesPermitted = false) {
		foreach ($usernames as $username) {
			$this->notifyUserActivation($username, $personalDevicesPermitted);
		}
	}
}
