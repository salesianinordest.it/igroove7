<?php

namespace App\Manager;

use App\Entity\Student;
use App\Entity\Teacher;
use Doctrine\ORM\EntityManagerInterface;
use Psr\Log\LoggerInterface;
use App\Entity\MdmDevice;
use App\Entity\MdmServer;

class MdmServerManager {

    protected $em;
    protected $logger;
    protected $importedEntities = [];

    public function __construct(EntityManagerInterface $em, LoggerInterface $logger) {
        $this->em = $em;
        $this->logger = $logger;
    }

    /**
     * @param MdmServer $mdmServer
     * @param bool $fullSync
     * @throws \Exception
     */
    public function importFromServer(MdmServer $mdmServer, $fullSync = false) {
        if(!$mdmServer->getActive()) {
            throw new \Exception("Inactive mdm server {$mdmServer->getName()}");
        }

        $this->logger->info("Start ".($fullSync?"full ":"")."sync of data from mdm server {$mdmServer->getName()}");
        $this->importedEntities = [];

        try {
            $connector = $mdmServer->getConnectionManager();

            if($fullSync) {
                $connector->importRemoteData();
            } else {
                $connector->importRemoteData($mdmServer->getLastUpdate());
            }

            $devices = $connector->getDevices();
            foreach ($devices as $id => $device) {
	            if($device->getOwnerUsername() != "") {
					$student = $this->em->getRepository(Student::class)->findOneBy(['username' => $device->getOwnerUsername()]);
					if($student instanceof Student) {
						$device->setStudentOwner($student);
					} else {
						$teacher = $this->em->getRepository(Teacher::class)->findOneBy(['username' => $device->getOwnerUsername()]);
						if($teacher instanceof Teacher) {
							$device->setTeacherOwner($teacher);
						}
					}
	            }
            }

            $this->importEntity($mdmServer, $fullSync, "MdmDevice", $devices);
        } catch (\Exception $e) {
            $mdmServer->setLastUpdate(new \DateTime("now"));
            $mdmServer->setLastUpdateStatus("ERROR: ".$e->getMessage());
            $this->em->flush();
            throw $e;
        }

        $mdmServer->setLastUpdate(new \DateTime("now"));
        $mdmServer->setLastUpdateStatus("OK");
        $this->em->flush();
    }

    protected function importEntity(MdmServer $mdmServer, $fullSync, $entityType, $serverEntities) {
        $this->logger->info("Start import of entity type {$entityType}");

        $this->importedEntities[$entityType] = [];
        $entityTypeFull = '\\App\\Entity\\'.$entityType;
        $repository = $this->em->getRepository($entityTypeFull);
        $currentEntities = $repository->findBy(['mdmServer' => $mdmServer->getId()]);
        $entitiesToDelete = [];

        foreach ($currentEntities as $currentEntity) {
            if(!isset($serverEntities[$currentEntity->getIdOnMdmServer()])) {
                if($fullSync) {
                    $entitiesToDelete[] = $currentEntity;
                } else {
	                $this->importedEntities[$entityType][$currentEntity->getIdOnMdmServer()] = $currentEntity;
                }

                continue;
            }

            try {
                $currentEntity->importData($serverEntities[$currentEntity->getIdOnMdmServer()]);
            } catch (\Exception $e) {
                $this->logger->error("* Error during update of entity {$entityType} with local id of {$currentEntity->getId()} and server id {$currentEntity->getIdOnMdmServer()}");
                continue;
            }

            $this->importedEntities[$entityType][$currentEntity->getIdOnMdmServer()] = $currentEntity;
            unset($serverEntities[$currentEntity->getIdOnMdmServer()]);
            $this->logger->info("* Updated entity of type {$entityType} with local id  {$currentEntity->getId()} and id on server {$currentEntity->getIdOnMdmServer()}");
        }

        foreach ($serverEntities as $idOnMdmServer => $serverEntity) {
            $newEntity = new $entityTypeFull();

            try {
                $newEntity->importData($serverEntity);
                $newEntity->setMdmServer($mdmServer);
                $this->em->persist($newEntity);
            } catch (\Exception $e) {
                $this->logger->error("* Error during insert of entity {$entityType} with id on server {$idOnMdmServer}");
                continue;
            }

            $this->importedEntities[$entityType][$idOnMdmServer] = $newEntity;
            $this->logger->info("* Inserted entity {$entityType} with id on server {$idOnMdmServer}");
        }

        if($fullSync && !empty($entitiesToDelete)) {
            foreach ($entitiesToDelete as $entityToDelete) {
                try {
                    $this->em->remove($entityToDelete);
                } catch (\Exception $e) {
                    $this->logger->error("* Error during the removal of entity {$entityType} with id {$entityToDelete->getId()}");
                }

                $this->logger->info("* Removed entity {$entityType} with id {$entityToDelete->getId()}");
            }
        }
    }
}
