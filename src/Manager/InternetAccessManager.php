<?php

namespace App\Manager;

use App\Entity\LdapGroup;
use App\Manager\WatchguardManager;
use Doctrine\ORM\EntityManager;

class InternetAccessManager
{
    protected $mikrotikPool;
    protected $groupNameInInternet;
    protected $userNameInInternet;
    protected $em;
    protected $configurationAD;
    protected $adldapManager;
    protected $coaManager;
    protected $mikrotikManager;
    protected $fortigateManager;
    protected  $watchguardManager;


    public function __construct(ConfigurationManager $configurationManager, $adldapManager, MikrotikManager $mikrotikManager, FortigateManager $fortigateManager, EntityManager $em, CoaManager $coaManager, WatchguardManager $watchguardManager)
    {
        $this->configurationAD = $configurationManager->getActiveDirectoryConfiguration();
        $this->adldapManager = $adldapManager;

        $this->mikrotikManager = $mikrotikManager;
        $this->fortigateManager = $fortigateManager;
        $this->watchguardManager = $watchguardManager;
        $this->coaManager = $coaManager;
        $this->getUsers();
        $this->em = $em;
    }

    private function getUsers()
    {
        $adldap = $this->adldapManager->getAdLdap();
        $groupsInInternet = $adldap->group()->inGroup('_InternetAccess');
        $groupNameInInternet = [];

        if (!is_array($groupsInInternet)) {
            sleep(10);
            $groupsInInternet = $adldap->group()->inGroup('_InternetAccess');
        }

        if (is_array($groupsInInternet)) {
            foreach ($groupsInInternet as $key => $groupAD) {
                $groupNameInInternet[] = trim(substr($groupAD, 3, strpos($groupAD, ',', 3) - 3));
            }
        }

        $this->userNameInInternet = $adldap->group()->members('_InternetAccess', false);

        $this->groupNameInInternet = $groupNameInInternet;
    }

	/**
	 * @todo unused, remove?
	 */
	public function addGroup($groupName, $personalDevicesPermitted = false)
	{
		$adldap = $this->adldapManager->getAdLdap();

		if (!in_array($groupName, $this->groupNameInInternet)) {
			$result = $adldap->group()->addGroup('_InternetAccess', $groupName);
			if ($result) {
				$this->getUsers();
				$this->adldapManager->explodeGroupInternetAccess();
				$groupUsers = $this->em->getRepository(LdapGroup::class)->getAllChildrenRecursiveUsers($groupName);
				$this->coaManager->notifyUsersActivation($groupUsers, $personalDevicesPermitted);
			}
		}
	}

    public function removeGroup($groupName)
    {
        $adldap = $this->adldapManager->getAdLdap();

        if (in_array($groupName, $this->groupNameInInternet)) {
            $result = $adldap->group()->removeGroup('_InternetAccess', $groupName);
            if ($result) {
                $entities = $this->em->getRepository('App\Entity\InternetOpen')->findBy(
                    ['type' => 'group', 'account' => strtolower($groupName)]
                );

                foreach ($entities as $entity) {
                    $this->em->remove($entity);
                    $this->em->flush();
                    $this->removeCoaFromGroup($entity->getAccount());
                }

                $this->getUsers();

                $this->adldapManager->explodeGroupInternetAccess();
	            $this->mikrotikManager->KickOffUsers();
                $this->fortigateManager->KickOffUsers();
                $this->watchguardManager->KickOffUsers();
            }
        }
    }

	/**
	 * @todo unused, remove?
	 */
	public function addUser($user, $personalDevicesPermitted = false)
	{
		$adldap = $this->adldapManager->getAdLdap();
		if (!in_array($user, $this->userNameInInternet)) {
			$result = $adldap->group()->addUser('_InternetAccess', $user);
			if ($result) {
				$this->getUsers();
				$this->adldapManager->explodeGroupInternetAccess();
				$this->coaManager->notifyUsersActivation($user, $personalDevicesPermitted);
			}
		}
	}

    public function removeUser($user)
    {
        $adldap = $this->adldapManager->getAdLdap();
        if (in_array($user, $this->userNameInInternet)) {
            $result = $adldap->group()->removeUser('_InternetAccess', $user);
            if ($result) {
                $entities = $this->em->getRepository('App\Entity\InternetOpen')->findBy(
                    ['type' => 'user', 'account' => strtolower($user)]
                );
                foreach ($entities as $entity) {
                    $this->em->remove($entity);
                    $this->em->flush();
                    $this->coaManager->KickOffUser($entity->getAccount());
                }

                $this->getUsers();

                $this->adldapManager->explodeGroupInternetAccess();
                $this->mikrotikManager->KickOffUsers($this->adldapManager);
                $this->fortigateManager->KickOffUsers($this->adldapManager);
            }
        }
    }

    private function removeCoaFromGroup($groupName)
    {
        $usernames = $this->em->getRepository('App\Entity\LdapGroup')
            ->getAllChildrenRecursiveUsers($groupName);
        foreach ($usernames as $username) {
            $this->coaManager->KickOffUser($username);
        }
    }
}
