<?php

namespace App\Manager;

use App\Entity\InternetOpen;
use Doctrine\Common\Collections\Criteria;
use Doctrine\ORM\EntityManager;
use Doctrine\ORM\EntityManagerInterface;
use Doctrine\ORM\Query\Expr\Join;
use Psr\Log\LoggerInterface;
use RouterOS\Client;
use RouterOS\Query;

class MikrotikManager
{
    protected $mikrotiks;
    /**
     * @var Client[]
     */
    protected $mikrotikClient = [];
    protected $userIntoMikrotik;
    protected $internetAccessUsers;

    /**
     * @var LoggerInterface
     */
    private $logger;

    private $connected = false;
    /**
     * @var EntityManager
     */
    protected $em;

    public static $bypassedIpListName = 'Bypassed Ip';

    public function __construct(ConfigurationManager $configurationManager, EntityManagerInterface $em, LoggerInterface $logger)
    {
        $this->em = $em;
        $this->configurationManager = $configurationManager;
        $this->logger = $logger;
    }

    private function connect()
    {
        if (!$this->connected) {
            $this->loadClientsHandler();
            $this->connected = true;
        }
    }

    /**
     * kick off the users not currently active or in active group.
     */
    public function KickOffUsers()
    {
        $this->connect();
        $this->internetAccessUsers = $groupMembersUsers = $this->em->getRepository('App\Entity\LdapGroup')->getAllChildrenRecursiveUsers($this->configurationManager->getActiveDirectoryGeneratedGroupPrefix().'InternetAccess');
        $userList = [];
        foreach ($groupMembersUsers as $user) {
            $userList[] = strtolower($user);
        }

        $igrooveUsers = $this->em->getRepository('App\Entity\Student')->getAllStudentsUsername();

        foreach ($this->mikrotikClient as $mikrotikIp => $mikrotikClient) {
            $userIntoMikrotik = $this->getUsersInHotspot($mikrotikIp);
            $usernameToRemove = array_diff(
                array_unique(array_keys($userIntoMikrotik)),
                array_unique($userList)
            );

            foreach ($usernameToRemove as $user) {
                if (0 == strlen(trim($user))) {
                    continue;
                }
                if (!array_search($user, $igrooveUsers)) {
                    continue;
                }
                if (array_key_exists(strtolower($user), $userIntoMikrotik)) {
                    echo "\r\n --> Mikrotik kickoff ".$user;
                    $this->logger->info("Mikrotik kickoff $user");
                    $delRequest = new Query('/ip/hotspot/active/remove', ['=numbers='.$userIntoMikrotik[strtolower($user)]]);
                    $mtResult = $mikrotikClient->query($delRequest)->read();
                    if (isset($mtResult['after']['message'])) {
                        $this->logger->error('Error kickoff '.$user.': '.$mtResult['after']['message']);
                    }
                }
            }
        }
    }

    /**
     * add or remove the mac address bypassed in the hotspot of all the mikrotiks.
     *
     * @return array action executed
     */
    public function manageMacToHotspot()
    {
        $this->connect();
        $criteria = ['active' => true, 'bypassHotspot' => true];
        $entities = $this->em->getRepository('App\Entity\Device')->findBy($criteria);
        $macInIgroove = [];
        foreach ($entities as $entity) {
            $macInIgroove[strtoupper($entity->getMac())] = 'igroove - '.$entity->getDevice();
        }
        $result = [];
        foreach ($this->mikrotikClient as $mikrotikIp => $mikrotikClient) {
            if (false == array_key_exists($mikrotikIp, $this->mikrotiks)) {
                continue;
            }
            if (false == $this->mikrotiks[$mikrotikIp]->getUseBypass()) {
                $result[$mikrotikIp]['ROUTER'] = 'skip this router bypass';
                continue;
            }
            $macIntoMikrotik = $this->getMacsIntoHotspot($mikrotikIp);
            $macToAdd = array_diff_key($macInIgroove, $macIntoMikrotik);
            $macToRemove = array_diff_key($macIntoMikrotik, $macInIgroove);

            foreach ($macToAdd as $mac => $comment) {
                $result[$mikrotikIp][$mac] = 'Add '.$comment;
                $this->logger->info('Add Mikrotik bypass '.$comment);
                $addRequest = new Query(
                    '/ip/hotspot/ip-binding/add',
                    [
                        '=mac-address='.$mac,
                        '=type=bypassed',
                        '=comment='.$comment,
                    ]
                );
	            $mtResult = $mikrotikClient->query($addRequest)->read();
	            if (isset($mtResult['after']['message'])) {
		            $this->logger->error('Error adding mac-address to hotspot ' . $comment . ': ' . $mtResult['after']['message']);
		            $result[$mikrotikIp][$mac] = 'Error adding mac-address to hotspot ' . $comment. ': ' . $mtResult['after']['message'];
	            } else {
		            $result[$mikrotikIp][$mac] = 'Added mac-address to hotspot ' . $comment;
	            }
            }
	        foreach ($macToRemove as $mac => $element) {
		        $delRequest = new Query('/ip/hotspot/ip-binding/remove', ['=numbers=' . $element['id']]);
		        $this->logger->info('Remove Mikrotik bypass ' . $element['comment']);
		        $mtResult = $mikrotikClient->query($delRequest)->read();
		        if (isset($mtResult['after']['message'])) {
			        $this->logger->error('Error removing mac-address from hotspot ' . $element['comment'] . ': ' . $mtResult['after']['message']);
			        $result[$mikrotikIp][$mac] = 'Error removing mac-address from hotspot ' . $element['comment']. ': ' . $mtResult['after']['message'];
		        } else {
			        $result[$mikrotikIp][$mac] = 'Removed mac-address from hotspot ' . $element['comment'];
		        }

	        }
        }

        return $result;
    }

    /**
     * add or remove the ip addresses in the firewall addresslist.
     *
     * @return array action executed
     */
    public function addIpToIpList()
    {
        $this->connect();
        $ipsInIgroove = $this->getIgrooveIpInAddressList();

        $result = [];
        foreach ($this->mikrotikClient as $mikrotikIp => $mikrotikClient) {
            if (false == array_key_exists($mikrotikIp, $ipsInIgroove)) {
                continue;
            }

            $ipsIntoMikrotik = $this->getMikrotikIpAddresslists($mikrotikIp);
            $entriesToAdd = array_diff_key($ipsInIgroove[$mikrotikIp], $ipsIntoMikrotik);
            $entriesToRemove = array_diff_key($ipsIntoMikrotik, $ipsInIgroove[$mikrotikIp]);

	        foreach ($entriesToAdd as $k => $entry) {
		        $comment = 'igroove - ' . $entry['device'];
		        $addRequest = new Query('/ip/firewall/address-list/add', [
			        '=address=' . $entry['ip'],
			        '=list=' . $entry['list'],
			        '=comment=' . $comment
		        ]);
		        $returnValue = $mikrotikClient->query($addRequest)->read();

		        if(!empty($returnValue) && isset($returnValue['after']) && isset($returnValue['after']['message'])) {
			        $this->logger->error('Error adding ' . $entry['device'] . ' (' . $entry['ip'] . ') to list ' . $entry['list'].": ".$returnValue['after']['message']);
			        $result[$mikrotikIp][$k] = 'Error adding ' . $entry['device'] . ' (' . $entry['ip'] . ') to list ' . $entry['list'].": ".$returnValue['after']['message'];
		        } else {
			        $result[$mikrotikIp][$k] = 'Added ' . $entry['device'] . ' (' . $entry['ip'] . ') to list ' . $entry['list'];
		        }
	        }

	        foreach ($entriesToRemove as $k => $element) {
		        $comment = $element['comment'];

		        $delRequest = new Query('/ip/firewall/address-list/remove', [
			        '=numbers=' . $element['id']
		        ]);
		        $returnValue = $mikrotikClient->query($delRequest)->read();

		        if(!empty($returnValue) && isset($returnValue['after']) && isset($returnValue['after']['message'])) {
			        $this->logger->error('Error removing ip from list ' . $comment.": ".$returnValue['after']['message']);
			        $result[$mikrotikIp][$k] = 'Error removing ip from list ' . $comment.": ".$returnValue['after']['message'];
		        } else {
			        $result[$mikrotikIp][$k] = 'Removed ip from list ' . $comment;
		        }
	        }
        }

        return $result;
    }

    public function addIpToBypassedList()
    {
        $this->connect();
        $ipsInIgroove = $this->getBypassedIgrooveIp();

        $result = [];
        foreach ($this->mikrotikClient as $mikrotikIp => $mikrotikClient) {
            if (!isset($ipsInIgroove[$mikrotikIp])) {
                continue;
            }

            $ipInMikrotik = $this->getMikrotikIpInAddressList($mikrotikIp, self::$bypassedIpListName);
            $entriesToAdd = array_diff_key($ipsInIgroove[$mikrotikIp], $ipInMikrotik);
            $entriesToRemove = array_diff_key($ipInMikrotik, $ipsInIgroove[$mikrotikIp]);

	        foreach ($entriesToAdd as $k => $entry) {
		        $comment = 'igroove - ' . $entry['device'];
		        $addRequest = new Query('/ip/firewall/address-list/add', [
			        '=address=' . $entry['ip'],
			        '=list=' . self::$bypassedIpListName,
			        '=comment=' . $comment
		        ]);
		        $returnValue = $mikrotikClient->query($addRequest)->read();

		        if(!empty($returnValue) && isset($returnValue['after']) && isset($returnValue['after']['message'])) {
			        $this->logger->error('Error adding bypassed ip ' . $entry['device'] . ' (' . $entry['ip'] . ') to list ' . self::$bypassedIpListName.": ".$returnValue['after']['message']);
			        $result[$mikrotikIp][$k] = 'Error adding bypassed ip ' . $entry['device'] . ' (' . $entry['ip'] . ') to list ' . self::$bypassedIpListName.": ".$returnValue['after']['message'];
		        } else {
			        $this->logger->info('Added bypassed ip ' . $entry['device'] . ' (' . $entry['ip'] . ') to list ' . self::$bypassedIpListName);
			        $result[$mikrotikIp][$k] = 'Added bypassed ip ' . $entry['device'] . ' (' . $entry['ip'] . ') to list ' . self::$bypassedIpListName;
		        }
	        }

	        foreach ($entriesToRemove as $k => $element) {
		        $comment = $element['comment'];

		        $delRequest = new Query('/ip/firewall/address-list/remove', [
			        '=numbers=' . $element['id']
		        ]);
		        $returnValue = $mikrotikClient->query($delRequest)->read();

		        if(!empty($returnValue) && isset($returnValue['after']) && isset($returnValue['after']['message'])) {
			        $this->logger->error('Error removing bypassed ip ' . $comment.": ".$returnValue['after']['message']);
			        $result[$mikrotikIp][$k] = 'Error removing bypassed ip ' . $comment.": ".$returnValue['after']['message'];
		        } else {
			        $this->logger->info('Removed bypassed ip ' . $comment);
			        $result[$mikrotikIp][$k] = 'Removed bypassed ip ' . $comment;
		        }
	        }
        }

        $internetOpens = $this->em->getRepository('App\Entity\InternetOpen')->findBy(['type' => 'ipList', 'activationCompleated' => false]);
        foreach ($internetOpens as $internetOpen) {
            $internetOpen->setActivationCompleated(true);
        }
        $this->em->flush();

        return $result;
    }

    /**
     * add or remove pools with creareSuMikrotik option enabled on the mikrotiks.
     *
     * @return array
     */
    public function addPools()
    {
        $this->connect();
        $responses = [];
        foreach ($this->mikrotiks as $mikrotik) {
            try {
                $mikrotikClient = $this->getMikrotikClient($mikrotik->getIp());
            } catch (\ErrorException $e) {
                continue;
            }

            $mikrotikPools = $this->getMikrotikPools($mikrotik->getIp());
            $criteria = ['mikrotik' => $mikrotik, 'creareSuMikrotik' => true];
            $poolsInIgroove = $this->em->getRepository('App\Entity\MikrotikPool')->findBy($criteria);

	        foreach ($poolsInIgroove as $poolInIgroove) {
		        $rangesInIgroove = $poolInIgroove->getDottedIpStart() . '-' . $poolInIgroove->getDottedIpEnd();

		        if (array_key_exists($poolInIgroove->getNome(), $mikrotikPools)) {
			        $rangesInMikrotik = $mikrotikPools[$poolInIgroove->getNome()]['ranges'];
			        if ($rangesInIgroove == $rangesInMikrotik) {
				        continue;
			        }
			        $delRequest = new Query('/ip/pool/remove', [
				        '=numbers=' . $mikrotikPools[$poolInIgroove->getNome()]['id']
			        ]);
			        $returnValue = $mikrotikClient->query($delRequest)->read();

			        if(!empty($returnValue) && isset($returnValue['after']) && isset($returnValue['after']['message'])) {
				        $this->logger->error('Error removing pool ' . $poolInIgroove->getNome() . ': ' . $rangesInMikrotik.$returnValue['after']['message']);
				        $responses[$mikrotik->getIp()][] = 'Error removing pool ' . $poolInIgroove->getNome() . ': ' . $rangesInMikrotik.$returnValue['after']['message'];
			        } else {
				        $responses[$mikrotik->getIp()][] = 'Removed pool ' . $poolInIgroove->getNome() . ': ' . $rangesInMikrotik;
			        }
		        }

		        $addRequest = new Query('/ip/pool/add', [
			        '=name=' . $poolInIgroove->getNome(),
			        '=ranges=' . $rangesInIgroove
		        ]);
		        $returnValue = $mikrotikClient->query($addRequest)->read();

		        if(!empty($returnValue) && isset($returnValue['after']) && isset($returnValue['after']['message'])) {
			        $this->logger->error('Error adding pool ' . $poolInIgroove->getNome() . ': ' . $rangesInIgroove.": ".$returnValue['after']['message']);
			        $responses[$mikrotik->getIp()][] = 'Error adding pool ' . $poolInIgroove->getNome() . ': ' . $rangesInIgroove.": ".$returnValue['after']['message'];
		        } else {
			        $responses[$mikrotik->getIp()][] = 'Added pool ' . $poolInIgroove->getNome() . ': ' . $rangesInIgroove;
		        }
	        }
        }

        return $responses;
    }

    /**
     * Add ip reservation in dhcp server lease.
     */
    public function addIpReservation()
    {
        $this->connect();
        $ipsInIgroove = $this->getIgrooveDeviceIps();

        $result = [];
	    foreach ($this->mikrotikClient as $mikrotikIp => $mikrotikClient) {
		    $ipsInMikrotik = $ipsToRemoveFromMikrotik = $this->getMikrotikIpLeasesInDhcpServer($mikrotikIp);
		    $ipsToAddToMikrotik = [];

		    foreach ($ipsInIgroove[$mikrotikIp] as $deviceIpInIgroove => $ipInIgroove) {
			    if (array_key_exists($deviceIpInIgroove, $ipsInMikrotik) == false) {
				    if (strlen($ipInIgroove['ip']) > 0) {
					    $ipsToAddToMikrotik[] = $ipInIgroove;
				    }
				    continue;
			    }

			    if ($ipsInMikrotik[$deviceIpInIgroove]['check_string'] != $ipInIgroove['check_string']) {
				    $updRequest = new Query('/ip/dhcp-server/lease/set', [
					    '=numbers=' . $ipsInMikrotik[$deviceIpInIgroove]['id'],
					    '=comment=' . 'igroove - ' . $ipInIgroove['name'],
					    '=mac-address=' . $ipInIgroove['mac'],
					    '=address=' . $ipInIgroove['ip'],
					    '=server=' . $ipInIgroove['server']
				    ]);
				    $returnValue = $mikrotikClient->query($updRequest)->read();

				    if(!empty($returnValue) && isset($returnValue['after']) && isset($returnValue['after']['message'])) {
					    $this->logger->error('Error updating ip reservation  ' . $ipInIgroove['name'] . ' (' . $ipInIgroove['ip'] . ') in dhcp server ' . $ipInIgroove['server'].": ".$returnValue['after']['message']);
					    $result[$mikrotikIp][$deviceIpInIgroove] = 'Error updating ip reservation  ' . $ipInIgroove['name'] . ' (' . $ipInIgroove['ip'] . ') in dhcp server ' . $ipInIgroove['server'].": ".$returnValue['after']['message'];
				    } else {
					    $result[$mikrotikIp][$deviceIpInIgroove] = 'Updated ip reservation ' . $ipInIgroove['name'] . ' (' . $ipInIgroove['ip'] . ') in dhcp server ' . $ipInIgroove['server'] . " | {$ipsInMikrotik[$deviceIpInIgroove]['check_string']} {$ipInIgroove['check_string']}";
				    }
			    }

			    unset($ipsToRemoveFromMikrotik[$deviceIpInIgroove]);
		    }

		    foreach ($ipsToRemoveFromMikrotik as $ipToRemove => $ipToRemoveFromMikrotik) {
			    $delRequest = new Query('/ip/dhcp-server/lease/remove', [
				    '=numbers=' . $ipToRemoveFromMikrotik['id']
			    ]);
			    $returnValue = $mikrotikClient->query($delRequest)->read();
			    if(!empty($returnValue) && isset($returnValue['after']) && isset($returnValue['after']['message'])) {
				    $this->logger->error('Error Removing ip reservation ' . $ipToRemoveFromMikrotik['name'] . ' (' . $ipToRemoveFromMikrotik['ip'] . ' - ' . $ipToRemoveFromMikrotik['mac'] . ') to dhcp server ' . $ipToRemoveFromMikrotik['server'].": ".$returnValue['after']['message']);
				    $result[$mikrotikIp][$ipToRemove] = 'Error Removing ip reservation ' . $ipToRemoveFromMikrotik['name'] . ' (' . $ipToRemoveFromMikrotik['ip'] . ' - ' . $ipToRemoveFromMikrotik['mac'] . ') to dhcp server ' . $ipToRemoveFromMikrotik['server'].": ".$returnValue['after']['message'];
			    } else {
				    $result[$mikrotikIp][$ipToRemove] = 'Removed ip reservation ' . $ipToRemoveFromMikrotik['name'] . ' (' . $ipToRemoveFromMikrotik['ip'] . ' - ' . $ipToRemoveFromMikrotik['mac'] . ') to dhcp server ' . $ipToRemoveFromMikrotik['server'];
			    }
		    }

		    foreach ($ipsToAddToMikrotik as $ipToAddToMikrotik) {
			    $addRequest = new Query('/ip/dhcp-server/lease/add', [
				    '=comment=' . 'igroove - ' . $ipToAddToMikrotik['name'],
				    '=mac-address=' . $ipToAddToMikrotik['mac'],
				    '=address=' . $ipToAddToMikrotik['ip'],
				    '=server=' . $ipToAddToMikrotik['server']
			    ]);
			    $returnValue = $mikrotikClient->query($addRequest)->read();

			    if(!empty($returnValue) && isset($returnValue['after']) && isset($returnValue['after']['message'])) {
				    $this->logger->error("Error Adding ip reservation {$ipToAddToMikrotik['name']} ({$ipToAddToMikrotik['ip']} - {$ipToAddToMikrotik['mac']}) to dhcp server {$ipToAddToMikrotik['server']}: {$returnValue['after']['message']}");
				    $result[$mikrotikIp][$ipToAddToMikrotik['ip']] = "Error Adding ip reservation {$ipToAddToMikrotik['name']} ({$ipToAddToMikrotik['ip']} - {$ipToAddToMikrotik['mac']}) to dhcp server {$ipToAddToMikrotik['server']}: {$returnValue['after']['message']}";
			    } else {
				    $result[$mikrotikIp][$ipToAddToMikrotik['ip']] = 'Added ip reservation ' . $ipToAddToMikrotik['name'] . ' (' . $ipToAddToMikrotik['ip'] . ') to dhcp server ' . $ipToAddToMikrotik['server'];
			    }
		    }
	    }

        return $result;
    }

    /**
     * Get Current active users in hotspot of mikrotik with specified ip.
     *
     * @param $mikrotikIp
     *
     * @return array
     */
    private function getUsersInHotspot($mikrotikIp)
    {
        $userIntoMikrotik = [];

        try {
            $mikrotikClient = $this->getMikrotikClient($mikrotikIp);
        } catch (\ErrorException $e) {
            return $userIntoMikrotik;
        }

        $responses = $mikrotikClient->query(new Query('/ip/hotspot/active/print'))->read();
        $userIntoMikrotik = [];

        foreach ($responses as $element) {
            $username = explode('@', $element['user']);
            if (empty($username) || '' == $username[0]) {
                continue;
            }

            /*
            if (isset($username[1]) && strtolower($this->configurationManager->getFreeradiusRealm()) != strtolower($username[1])) {
                continue;
            } elseif (!isset($username[1]) && $this->configurationManager->getFreeradiusRealm() != '') {
                continue;
            }
*/
            $userIntoMikrotik[strtolower($username[0])] = $element['.id'];
        }

        return $userIntoMikrotik;
    }

    /**
     * Get Current active users in all the mikrotiks hotspot.
     *
     * @return array
     */
    public function getAllUsersInHotspots()
    {
        $this->connect();
        $userIntoMikrotik = [];
        foreach ($this->mikrotikClient as $mikrotikClient) {
            $responses = $mikrotikClient->query(
                '/ip/hotspot/active/print'
            )->read();

            foreach ($responses as $element) {
                $userIntoMikrotik[strtolower($element['user'])] = strtolower($element['server']);
            }
        }

        return $userIntoMikrotik;
    }

    /**
     * @param $mikrotikIp
     *
     * @return array
     */
    public function getDhcpServersName($mikrotikIp)
    {
        $this->connect();
        try {
            $mikrotikClient = $this->getMikrotikClient($mikrotikIp);
        } catch (\ErrorException $e) {
            return [];
        }

        $responses = $mikrotikClient->query('/ip/dhcp-server/print')->read();
        $dhcpServersName = [];
        foreach ($responses as $element) {
            if ('' == $element['name']) {
                continue;
            }

            $dhcpServersName[] = $element['name'];
        }

        return $dhcpServersName;
    }

    /**
     * get the mac addresses of the devices bypassed in the hotspot of the mikrotik with ip specified.
     *
     * @param $mikrotikIp
     *
     * @return array
     */
    private function getMacsIntoHotspot($mikrotikIp)
    {
        try {
            $mikrotikClient = $this->getMikrotikClient($mikrotikIp);
        } catch (\ErrorException $e) {
            return [];
        }

        $responses = $mikrotikClient->query('/ip/hotspot/ip-binding/print')->read();
        $macIntoMikrotik = [];
        foreach ($responses as $element) {
            if (isset($element['comment']) && 'igroove' == substr($element['comment'], 0, 7)) {
                $macIntoMikrotik[strtoupper($element['mac-address'])] = [
                    'id' => $element['.id'],
                    'comment' => $element['comment'],
                ];
            }
        }

        return $macIntoMikrotik;
    }

    /**
     * get the list of ips in all the addresslist of the mikrotik with specified ip.
     *
     * @param $mikrotikIp
     *
     * @return array
     */
    private function getMikrotikIpAddresslists($mikrotikIp)
    {
        try {
            $mikrotikClient = $this->getMikrotikClient($mikrotikIp);
        } catch (\ErrorException $e) {
            return [];
        }

        $responses = $mikrotikClient->query('/ip/firewall/address-list/print')->read();
        $ipAddresslistIntoMikrotik = [];
        foreach ($responses as $element) {
            if (isset($element['comment']) && 'igroove' == substr($element['comment'], 0, 7) && $element['list'] != self::$bypassedIpListName) {
                $ipAddresslistIntoMikrotik[md5(strtolower($element['address'].$element['list']))] = [
                    'id' => $element['.id'],
                    'address' => $element['address'],
                    'list' => $element['list'],
                    'comment' => $element['comment'],
                ];
            }
        }

        return $ipAddresslistIntoMikrotik;
    }

    private function getMikrotikIpInAddressList($mikrotikIp, $listName)
    {
        try {
            $mikrotikClient = $this->getMikrotikClient($mikrotikIp);
        } catch (\ErrorException $e) {
            return [];
        }

	    $request = new Query('/ip/firewall/address-list/print');
	    $request->where("list", $listName);
	    $responses = $mikrotikClient->query($request)->read();
        $ipAddresslistIntoMikrotik = [];
        foreach ($responses as $element) {
            if (isset($element['comment']) && 'igroove' == substr($element['comment'], 0, 7)) {
                $ipAddresslistIntoMikrotik[md5($element['address'])] = [
                    'id' => $element['.id'],
                    'address' => $element['address'],
                    'comment' => $element['comment'],
                ];
            }
        }

        return $ipAddresslistIntoMikrotik;
    }

    /**
     * Get all ips assigned to an address list in igroove.
     *
     * @return array
     */
    private function getIgrooveIpInAddressList()
    {
        $query = $this->em->getRepository('App\Entity\DeviceIp')
            ->createQueryBuilder('i')
            ->select('i.ip,d.device,l.nome,mk.ip as mip')
            ->join('i.mac', 'd')
            ->join('d.mikrotikList', 'l')
            ->join('App\Entity\Mikrotik', 'mk', Join::WITH, 'mk.id = l.mikrotik')
            ->where('d.active = :true')
            ->setParameter('true', true)
            ->getQuery();
        $results = $query->getResult();

        $ipInAddresslist = [];
        $mts = $this->em->getRepository('App\Entity\Mikrotik')->findAll();
        foreach ($mts as $m) {
            $ipInAddresslist[$m->getIp()] = [];
        }
        foreach ($results as $result) {
            if ($result['ip'] > 0) {
                $deviceIp = long2ip($result['ip']);
                $ipInAddresslist[$result['mip']][md5($deviceIp.strtolower($result['nome']))] = [
                    'ip' => $deviceIp,
                    'list' => $result['nome'],
                    'device' => $result['device'],
                ];
            }
        }

        return $ipInAddresslist;
    }

    private function getBypassedIgrooveIp()
    {
		$activeIpListIds = $this->em->getRepository(InternetOpen::class)->getActiveIpListIdsAsUuid(); //workaround to uuid-string incompatible type in join

		$query = $this->em->getRepository('App\Entity\DeviceIp')
            ->createQueryBuilder('i')
            ->select('i.ip,d.device,mk.ip as mip')
            ->join('i.mac', 'd')
            ->join('d.mikrotikList', 'l')
            ->join('App\Entity\Mikrotik', 'mk', Join::WITH, 'mk.id = l.mikrotik')
            ->where("d.active = :true AND l.id IN (:ipListIds)")
            ->setParameter('true', true)
            ->setParameter('ipListIds', $activeIpListIds)
            ->getQuery();
        $results = $query->getResult();

        $ipInAddresslist = [];
        $mts = $this->em->getRepository('App\Entity\Mikrotik')->findAll();
        foreach ($mts as $m) {
            $ipInAddresslist[$m->getIp()] = [];
        }
        foreach ($results as $result) {
            if ($result['ip'] > 0) {
                $deviceIp = long2ip($result['ip']);
                $ipInAddresslist[$result['mip']][md5($deviceIp)] = [
                    'ip' => $deviceIp,
                    'device' => $result['device'],
                ];
            }
        }

        return $ipInAddresslist;
    }

    /**
     * Get all ip currently set from igroove in dhcp server as lease.
     *
     * @param $mikrotikIp
     *
     * @return array
     */
    private function getMikrotikIpLeasesInDhcpServer($mikrotikIp)
    {
        try {
            $mikrotikClient = $this->getMikrotikClient($mikrotikIp);
        } catch (\ErrorException $e) {
            return [];
        }

        $responses = $mikrotikClient->query('/ip/dhcp-server/lease/print')->read();
        $ipIntoMikrotik = [];
        foreach ($responses as $element) {
            if (isset($element['comment']) && 'igroove' == substr($element['comment'], 0, 7)) {
                $name = substr($element['comment'], 10);
                $mac = strtoupper($element['mac-address']);
                $ipIntoMikrotik[$element['address']] = [
                    'id' => $element['.id'],
                    'ip' => $element['address'],
                    'name' => $name,
                    'mac' => $mac,
                    'server' => $element['server'],
                    'check_string' => md5(strtolower($element['address'].$name.$element['mac-address'].$element['server'])),
                ];
            }
        }

        return $ipIntoMikrotik;
    }

    /**
     * get the list of device ips currently in igroove.
     *
     * @return array
     */
    private function getIgrooveDeviceIps()
    {
        $query = $this->em->getRepository('App\Entity\DeviceIp')
            ->createQueryBuilder('i')
            ->select('i.ip,d.device,d.mac,p.dhcpServerName,mk.ip as mip')
            ->join('i.mac', 'd')
            ->join('i.mikrotikPool', 'p')
            ->join('App\Entity\Mikrotik', 'mk', Join::WITH, 'mk.id = p.mikrotik')
            ->where('d.active = :true')
            ->setParameter('true', true)
            ->getQuery();
        $results = $query->getResult();
        $mts = $this->em->getRepository('App\Entity\Mikrotik')->findAll();
        $ipInIgroove = [];
        foreach ($mts as $m) {
            $ipInIgroove[$m->getIp()] = [];
        }
        foreach ($results as $result) {
            if ($result['ip'] > 0) {
                $deviceIp = long2ip($result['ip']);
                $mac = strtoupper($result['mac']);
                $ipInIgroove[$result['mip']][$deviceIp] = [
                    'ip' => $deviceIp,
                    'name' => $result['device'],
                    'mac' => $mac,
                    'server' => $result['dhcpServerName'],
                    'check_string' => md5(strtolower($deviceIp.$result['device'].$result['mac'].$result['dhcpServerName'])),
                ];
            }
        }

        return $ipInIgroove;
    }

    /**
     * get the dhcp pools in the mikrotik with specified ip.
     *
     * @param $mikrotikIp
     *
     * @return array
     */
    private function getMikrotikPools($mikrotikIp)
    {
        $poolsIntoMikrotik = [];

        try {
            $mikrotikClient = $this->getMikrotikClient($mikrotikIp);
        } catch (\ErrorException $e) {
            return $poolsIntoMikrotik;
        }

        $responses = $mikrotikClient->query('/ip/pool/print')->read();
        foreach ($responses as $element) {
            if ($element['name']) {
                $poolsIntoMikrotik[$element['name']] = [
                    'id' => $element['.id'],
                    'name' => $element['name'],
                    'ranges' => $element['ranges'],
                    'comment' => $element['comment'] ?? '',
                ];
            }
        }

        return $poolsIntoMikrotik;
    }

    /**
     * reboot all the mikrotiks.
     *
     * @return array
     */
    public function rebootMikrotiks()
    {
        $this->connect();
        $messages = [];
        foreach ($this->mikrotikClient as $ip => $mikrotikClient) {
            $rebootRequest = new Query('/system/reboot');
	        $rebootRequest->tag('rebootFromIgroove');
	        $mikrotikClient->query($rebootRequest);
            $messages[] = 'Reboot '.$ip;
        }

        return $messages;
    }

    /**
     * load the mikrotiks client.
     */
    protected function loadClientsHandler()
    {
        $mts = $this->em->getRepository('App\Entity\Mikrotik')->findAll();
        $this->mikrotiks = [];
        foreach ($mts as $m) {
            $this->mikrotiks[$m->getIp()] = $m;
        }
        $this->mikrotikClient = [];
        if (count($this->mikrotiks) > 0) {
            foreach ($this->mikrotiks as $mikrotik) {
                try {
                    $this->mikrotikClient[$mikrotik->getIp()] = new Client(
                        [
                            'host' => $mikrotik->getIp(),
                            'user' => $mikrotik->getApiUsername(),
                            'pass' => $mikrotik->getApiPassword(),
                        ]
                    );
                    $mikrotik->setStatus(true);
                } catch (\Exception $e) {
	                $this->logger->error("[mikrotik] Error connecting to mikrotik {$mikrotik->getIp()}: ".$e->getMessage(), ['error' => $e]);
                    unset($this->mikrotikClient[$mikrotik->getIp()]);
                    $mikrotik->setStatus(false);
                    continue;
                }
            }
            $this->em->flush();
        }
    }

    /**
     * @param $mikrotikIp
     *
     * @return Client
     *
     * @throws \ErrorException
     */
    protected function getMikrotikClient($mikrotikIp)
    {
        if ((false == array_key_exists($mikrotikIp, $this->mikrotikClient)) || !$this->mikrotikClient[$mikrotikIp] instanceof Client) {
            throw new \ErrorException("Mikrotik with ip {$mikrotikIp} not accessible");
        }

        return $this->mikrotikClient[$mikrotikIp];
    }

    /**
     * @return Client[]
     */
    protected function getMikrotikClients()
    {
        return $this->mikrotikClient;
    }

    protected function checkSyncKey($stringToCheck)
    {
    }
}
