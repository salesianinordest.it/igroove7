<?php

namespace App\Manager;

use App\Entity\Group;
use App\Entity\LdapGroup;
use App\Entity\LdapUser;
use App\Entity\PersonAbstract;
use App\Entity\Provider;
use App\Entity\ProviderToRemoveEntity;
use App\Entity\Student;
use App\Entity\Subject;
use App\Entity\Teacher;
use App\Entity\TeacherSubjectGroup;
use App\Exception\LdapUsernameMissingAndNotAutoCreated;
use App\Exception\LoggedException;
use App\ImporterFilter\AbstractFilter;
use App\ImporterFilter\ImportedEntity\AbstractEntity;
use App\Message\GoogleMessage;
use App\Message\LdapMessage;
use App\Repository\GroupRepository;
use App\Repository\LdapGroupRepository;
use App\Repository\LdapUserRepository;
use App\Repository\StudentRepository;
use App\Repository\TeacherRepository;
use Doctrine\ORM\EntityManager;
use Doctrine\ORM\EntityManagerInterface;
use Psr\Log\LoggerInterface;
use Symfony\Component\Form\Form;
use Symfony\Component\Form\FormError;
use Symfony\Component\Messenger\Envelope;
use Symfony\Component\Messenger\MessageBusInterface;
use Symfony\Component\Security\Core\Authorization\AuthorizationChecker;
use Symfony\Component\Security\Core\Authorization\AuthorizationCheckerInterface;

class PersonsAndGroups
{
    /**
     * @var ConfigurationManager
     */
    protected $configurationManager;
    /**
     * @var \Doctrine\ORM\EntityManager
     */
    protected $em;

    /**
     * @var GroupRepository
     */
    protected $groupRepository;

    /**
     * @var StudentRepository
     */
    protected $studentRepository;

    /**
     * @var TeacherRepository
     */
    protected $teacherRepository;

    /**
     * @var EntityManager
     */
    protected $providerRepository;

    /**
     * @var LdapGroupRepository
     */
    protected $ldapGroupRepository;

    /**
     * @var LdapUserRepository
     */
    protected $ldapUserRepository;

    /**
     * @var LdapProxy
     */
    protected $ldapProxy;

    /**
     * @var string
     */
    protected $projectDir;

    /**
     * @var AuthorizationChecker
     */
    protected $authorizationChecker;

    /**
     * @var string
     */
    protected $inDebug;

    /**
     * @var string
     */
    protected $active_directory_generated_group_prefix;

    /**
     * @var string
     */
    protected $active_directory_generated_teacher_group_prefix;

    /**
     * @var LoggerInterface
     */
    protected $logger;

    /**
     * @var array
     */
    protected $importedEntities;

    /**
     * @var GoogleApps[]
     */
    protected $googleAppManagers = [];

    /**
     * @var array|null
     */
    protected $ldapAccountsUsername = null;

    /**
     * @var array
     */
    protected $gappsAccountsUsername = [];

    /**
     * @var MessageBusInterface
     */
    protected $messageBus;

    /**
     * @var PasswordGenerator
     */
    protected $passwordGenerator;

    /**
     * @param MessageBusInterface  $messageBus           ,
     * @param string               $projectDir
     * @param AuthorizationCheckerInterface $authorizationChecker
     * @param string               $inDebug
     */
    public function __construct(
        EntityManagerInterface $em,
        ConfigurationManager $configurationManager,
        MessageBusInterface $messageBus,
        LoggerInterface $logger,
        LdapProxy $ldapProxy,
        PasswordGenerator $passwordGenerator,
	    AuthorizationCheckerInterface $authorizationChecker,
	    $projectDir,
        $inDebug = false
    ) {
        $this->configurationManager = $configurationManager;
        $this->em = $em;
        $this->messageBus = $messageBus;
        $this->logger = $logger;

        $this->groupRepository = $this->em->getRepository('App\Entity\Group');
        $this->studentRepository = $this->em->getRepository('App\Entity\Student');
        $this->teacherRepository = $this->em->getRepository('App\Entity\Teacher');
        $this->ldapGroupRepository = $this->em->getRepository('App\Entity\LdapGroup');
        $this->ldapUserRepository = $this->em->getRepository('App\Entity\LdapUser');
        $this->providerRepository = $this->em->getRepository('App\Entity\Provider');
        $this->active_directory_generated_group_prefix = $configurationManager->getActiveDirectoryGeneratedGroupPrefix();
        $this->active_directory_generated_teacher_group_prefix = $configurationManager->getActiveDirectoryGeneratedTeacherGroupPrefix();
        $this->ldapProxy = $ldapProxy;
        $this->projectDir = $projectDir;
        $this->authorizationChecker = $authorizationChecker;
        $this->inDebug = $inDebug;
        $this->passwordGenerator = $passwordGenerator;
    }

    private function connect()
    {
        if (!$this->ldapProxy->isConnected()) {
            try {
                $this->ldapProxy->connect();
            } catch (\Exception) {
            }
        }
    }

    /**
     * Import People, Groups, Subject and Association from the filter loaded with the provider data.
     *
     * @throws \Exception
     */
    public function importElements(AbstractFilter $filter, Provider $provider)
    {
        $this->connect();
        $this->logger->info("Inizio importazione dal provider {$provider->getName()}");

        $this->importEntity($filter->getSectors(), $provider, 'Sector', 'name');

        $newGroups = $filter->getGroups();

        foreach ($newGroups as $newGroup) {
            if (null !== $newGroup->getSectorId() && isset($this->importedEntities['Sector'][$newGroup->getSectorId()])) {
                $newGroup->setSector($this->importedEntities['Sector'][$newGroup->getSectorId()]);
            }
        }

        $this->importEntity($newGroups, $provider, 'Group', 'name');

        $this->importEntity($filter->getTeachers(), $provider, 'Teacher', 'fiscalCode');

        $this->importEntity($filter->getSubjects(), $provider, 'Subject', 'name');

        $newStudents = $filter->getStudents();

        foreach ($newStudents as $newStudent) {
            if (null !== $newStudent->getGroupId() && isset($this->importedEntities['Group'][$newStudent->getGroupId()])) {
                $newStudent->setGroup($this->importedEntities['Group'][$newStudent->getGroupId()]);
            }
        }

        $this->importEntity($newStudents, $provider, 'Student', 'fiscalCode');
        $this->logger->info('Cancellazione associazioni teacher-subject-group del provider');

        $tsgsToDelete = $this->em->getRepository('App\Entity\TeacherSubjectGroup')->getQueryFilteredByProviderId($provider->getId())->execute();
        foreach ($tsgsToDelete as $tsgToDelete) {
            $this->em->remove($tsgToDelete);
        }
        $this->em->flush();

        $this->logger->info('Importazione associazioni teacher-subject-group del provider');
		$importedTSGIds = [];
        foreach ($filter->getTeacherSubjectGroupRelation() as $teacherSubjectGroupRelation) {
            if (!isset($this->importedEntities['Group'][$teacherSubjectGroupRelation['group_id']]) ||
                !isset($this->importedEntities['Subject'][$teacherSubjectGroupRelation['subject_id']])
            ) {
                continue;
            }

            if (!$this->importedEntities['Group'][$teacherSubjectGroupRelation['group_id']] instanceof Group ||
                !$this->importedEntities['Subject'][$teacherSubjectGroupRelation['subject_id']] instanceof Subject
            ) {
                continue;
            }

            if (isset($this->importedEntities['Teacher'][$teacherSubjectGroupRelation['teacher_id']]) &&
                $this->importedEntities['Teacher'][$teacherSubjectGroupRelation['teacher_id']] instanceof Teacher) {
                $teacher = $this->importedEntities['Teacher'][$teacherSubjectGroupRelation['teacher_id']];
            } else {
                continue;
            }

			$uniqueKey = $teacher->getId()."-".$this->importedEntities['Subject'][$teacherSubjectGroupRelation['subject_id']]->getId()."-".$this->importedEntities['Group'][$teacherSubjectGroupRelation['group_id']]->getId();
			if(isset($importedTSGIds[$uniqueKey])) {
				continue;
			}

            $teacherSubjectGroup = new TeacherSubjectGroup();
            $teacherSubjectGroup->setProvider($provider);
            $teacherSubjectGroup->setTeacher($teacher);
            $teacherSubjectGroup->setGroup($this->importedEntities['Group'][$teacherSubjectGroupRelation['group_id']]);
            $teacherSubjectGroup->setSubject($this->importedEntities['Subject'][$teacherSubjectGroupRelation['subject_id']]);
            $this->em->persist($teacherSubjectGroup);
	        $this->logger->info("* Inserita associazione con id {$teacherSubjectGroup->getId()}");

	        $importedTSGIds[$uniqueKey] = $uniqueKey;
        }
        $this->em->flush();
        $this->logger->info("Terminata importazione provider {$provider->getName()}\n");
    }

    /**
     * Import a specific entity type into the database.
     *
     * @param AbstractEntity[] $newEntities
     * @param $entityType
     * @param string $keyToCheck
     *
     * @throws \Exception
     */
    protected function importEntity($newEntities, Provider $provider, $entityType, $keyToCheck = 'id')
    {
        $this->connect();
        $this->logger->info("Importazione entità {$entityType}. Oggetti ricevuti ".count($newEntities));

        $keyToCheckGet = 'get'.str_replace('_', '', ucwords($keyToCheck, '_'));
        $entityTypeFull = '\\App\\Entity\\'.$entityType;
        $repository = $this->em->getRepository('App\Entity\\'.$entityType);

        $this->importedEntities[$entityType] = [];
        $currentEntities = $repository->findBy(['provider' => $provider->getId()]);
        $residualCurrentEntities = [];
        $checkKey = [];

        // controllo tra le entità correnti se trovo già un entità, usando l'id provider, altrimenti aggiungo alle entità locali residue
        foreach ($currentEntities as $currentEntity) {
            // aggiungo a entità residue, se non ha un idOnProvider o se non esiste l'entità con quell'id fra le nuove

            if ('' == $currentEntity->getIdOnProvider() || !isset($newEntities[(string) $currentEntity->getIdOnProvider()])) {
                $residualCurrentEntities[(string) $currentEntity->getId()] = $currentEntity;
                if ('' != $keyToCheck) {
                    $checkKey[strtolower($currentEntity->$keyToCheckGet())] = (string)($currentEntity->getId());
                }

                continue;
            }

            $newEntity = $newEntities[(string) $currentEntity->getIdOnProvider()];
            if (!$newEntity->isValid()) {
                $this->logger->error("* Dati dell'entità dal provider {$currentEntity->getIdOnProvider()} da aggiornare su id locale {$currentEntity->getId()}, non validi");
                continue;
            }
            unset($newEntities[$currentEntity->getIdOnProvider()]);
            $this->importedEntities[$entityType][(string) $currentEntity->getIdOnProvider()] = $currentEntity;
			$isTrashed = ('Student' == $entityType || 'Teacher' == $entityType) && $currentEntity->isTrashed();
	        $restoreTrashed = ('Student' == $entityType && !$provider->getStudentTrashedDisableRestoreIfPresentOnProvider()) || ('Teacher' == $entityType && !$provider->getTeacherTrashedDisableRestoreIfPresentOnProvider());
            if (!$currentEntity->isSame($newEntity)) {
                $repository->updateWithImportData($currentEntity, $newEntity, $provider);
                $this->logger->info("* Aggiornata".($isTrashed?" e ripristinata dal cestino":"")." entità con id locale {$currentEntity->getId()} e id provider {$currentEntity->getIdOnProvider()}");
            } elseif ($isTrashed && $restoreTrashed) {
				$currentEntity->setTrashedDate(null);
	            $this->logger->info("* Ripristinata dal cestino entità con id locale {$currentEntity->getId()} e id provider {$currentEntity->getIdOnProvider()}");
	            $this->em->flush();
            }
        }

        $residualCurrentAdditionalEntities = [];
        // scarico l'elenco delle persone già esistenti che hanno questo provider come addizionale
        if ('Student' == $entityType || 'Teacher' == $entityType) {
            $additionalPersons = 'Teacher' == $entityType ? $provider->getAdditionalTeachers() : $provider->getAdditionalStudents();
            if ('' != $keyToCheck) {
                foreach ($additionalPersons as $additionalPerson) {
                    $residualCurrentAdditionalEntities[strtolower($additionalPerson->$keyToCheckGet())] = $additionalPerson;
                }
            }
        }

        foreach ($newEntities as $idOnProvider => $newEntity) {
            // cerco se esiste un entità con la chiave da cercare (nome o id) combaciante, e nel caso la aggiorno, altrimenti inserisco
            if ('' != $keyToCheck && isset($checkKey[strtolower($newEntity->$keyToCheckGet())]) &&
                isset($residualCurrentEntities[$checkKey[strtolower($newEntity->$keyToCheckGet())]])
            ) {
                $entity = $residualCurrentEntities[$checkKey[strtolower($newEntity->$keyToCheckGet())]];
                unset($residualCurrentEntities[$checkKey[strtolower($newEntity->$keyToCheckGet())]]);

                if (!$entity instanceof $entityTypeFull) {
                    continue;
                }

                if (!$newEntity->isValid()) {
                    $this->logger->error("* Dati dell'entità dal provider {$keyToCheck} = {$newEntity->$keyToCheckGet()} da aggiornare su id locale {$entity->getId()}, non validi");
                    continue;
                }

                $this->logger->info("* Aggiorno entità con id locale {$entity->getId()} in base alla chiave {$keyToCheck} di valore {$newEntity->$keyToCheckGet()}");
                $entity->setIdOnProvider($idOnProvider);

	            $isTrashed = ('Student' == $entityType || 'Teacher' == $entityType) && $entity->isTrashed();
				$restoreTrashed = ('Student' == $entityType && !$provider->getStudentTrashedDisableRestoreIfPresentOnProvider()) || ('Teacher' == $entityType && !$provider->getTeacherTrashedDisableRestoreIfPresentOnProvider());
                if (!$entity->isSame($newEntity)) {
	                $repository->updateWithImportData( $entity, $newEntity, $provider );
	                $this->logger->info( '** Dati variati' . ( $isTrashed ? " e ripristinato dal cestino" : "" ) );
                } elseif($isTrashed && $restoreTrashed) {
	                $entity->setTrashedDate(null);
	                $this->logger->info("** Ripristinato dal cestino");
	                $this->em->flush();
                } else {
                    $this->em->flush();
                }
            } elseif (('Student' == $entityType || 'Teacher' == $entityType) && '' != $keyToCheck && isset($residualCurrentAdditionalEntities[strtolower($newEntity->$keyToCheckGet())])) { // se è una persona, cerco se esiste fra le persone che hanno questo provider come addizionale e nel caso sia studente gli aggiorno i gruppi
                $entity = $residualCurrentAdditionalEntities[strtolower($newEntity->$keyToCheckGet())];
                unset($residualCurrentAdditionalEntities[strtolower($newEntity->$keyToCheckGet())]);

                if (!$entity instanceof $entityTypeFull) {
                    continue;
                }

                if (!$newEntity->isValid()) {
                    $this->logger->error("* Dati della persona dal provider {$keyToCheck} = {$newEntity->$keyToCheckGet()} da aggiornare su id locale {$entity->getId()}, non validi");
                    continue;
                }

                if ('Student' == $entityType && !$entity->haveSameGroup($newEntity, $provider)) {
                    $repository->updateMembershipWithImportData($entity, $newEntity, $provider);
                    $this->logger->info("* Aggiornate le membership per lo studente con id locale {$entity->getId()}");
                }
            } elseif ('' != $keyToCheck && ('Student' == $entityType || 'Teacher' == $entityType) && '' != $newEntity->$keyToCheckGet() &&
                ($entity = $repository->findOneBy([$keyToCheck => strtolower($newEntity->$keyToCheckGet())])) instanceof $entityTypeFull
            ) { // se è una persona, cerco con la chiave di controllo se non esiste già su un'altro provider, e nel caso la associo a questo provider e se studente aggiorno i suoi gruppi
                $entity->addAdditionalProvider($provider);
                $this->em->flush();
                $this->logger->info("* Associata la persona con id locale {$entity->getId()} al provider {$provider->getId()}");

                if ('Student' == $entityType) {
                    $repository->updateMembershipWithImportData($entity, $newEntity, $provider);
                    $this->logger->info("* Aggiornate le membership per la persona con id locale {$entity->getId()}");
                }
            } else { // non esiste già, lo creo
                if (!$newEntity->isValid()) {
                    $this->logger->error("* Dati dell'entità dal provider {$newEntity->$keyToCheckGet()} da inserire, non validi");
                    continue;
                }

                try {
                    $entity = $repository->createWithImportData($newEntity, $provider, $idOnProvider);
                } catch (\Throwable $e) {
                    $this->logger->error("* Errore durante l'importazione di {$newEntity->$keyToCheckGet()}");
                    continue;
                }

                if (null === $entity) {
                    $this->logger->error("* Entità dal provider {$newEntity->$keyToCheckGet()} da inserire, non valida");
                    continue;
                }

                $this->logger->info("* Inserita nuova entità con nuovo id {$entity->getId()}");
            }

            $this->importedEntities[$entityType][$idOnProvider] = $entity;
            unset($newEntities[$idOnProvider]);
        }

        if ('databaseinterno' !== $provider->getFilter()) {
            if ('Student' === $entityType) {
                foreach ($residualCurrentEntities as $k => $residualCurrentEntity) {
                    if (!$residualCurrentEntity instanceof Student) {
                        continue;
                    }

                    if (1 === $residualCurrentEntity->getAdditionalProviders()->count()) {
                        $residualCurrentEntity->setProvider($residualCurrentEntity->getAdditionalProviders()->first());
                        $residualCurrentEntity->setIdOnProvider('');
                        $residualCurrentEntity->removeAllAdditionalProviders();
                        $this->em->flush();
                        $this->logger->info("* Spostato lo studente con id locale {$residualCurrentEntity->getId()} al provider {$residualCurrentEntity->getProvider()->getId()}");
                        unset($residualCurrentEntities[$k]);
                    }
                }
            }

            $toRemoveEntitiesByKey = [];
            $toRemoveEntities = $this->em->getRepository('App\Entity\ProviderToRemoveEntity')->findBy(['entityType' => $entityType, 'provider' => $provider]);
            foreach ($toRemoveEntities as $toRemoveEntity) {
                $toRemoveEntitiesByKey[$toRemoveEntity->getEntityId()] = $toRemoveEntity;
            }
            unset($toRemoveEntities, $toRemoveEntity);

            foreach ($residualCurrentEntities as $residualCurrentEntity) {
                if (isset($toRemoveEntitiesByKey[(string) $residualCurrentEntity->getId()])) {
                    unset($toRemoveEntitiesByKey[(string) $residualCurrentEntity->getId()]);
                } else {
                    $toRemoveEntity = new ProviderToRemoveEntity();
                    $toRemoveEntity->setProvider($provider);
                    $toRemoveEntity->setEntityType($entityType);
                    $toRemoveEntity->setEntityId((string) $residualCurrentEntity->getId());
                    $this->em->persist($toRemoveEntity);
                }
            }

            foreach ($toRemoveEntitiesByKey as $toRemoveEntityByKey) {
                $this->em->remove($toRemoveEntityByKey);
            }
            $this->em->flush();
        }

        $this->logger->info("Terminata importazione entità {$entityType}.");
        $this->logger->info('Id entità rimanenti ('.count($residualCurrentEntities).'): '.implode(', ', array_map(function ($el) {
            return $el->getId();
        }, $residualCurrentEntities)));
        $this->logger->info('Id entità non importate ('.count($newEntities).'): '.implode(', ', array_map(function ($el) use ($keyToCheckGet) {
            return $el->$keyToCheckGet();
        }, $newEntities)));
        $this->logger->info('Id associazioni addizionali entità rimanenti ('.count($residualCurrentAdditionalEntities).'): '.implode(', ', array_map(function ($el) {
            return $el->getId();
        }, $residualCurrentAdditionalEntities)));
    }

    /**
     * Check that all the Groups in database are present in Ldap/AD.
     */
    public function checkLdapGroups()
    {
        $this->logger->info('check groups not present in ldap');

        $groups = $this->groupRepository->findAll();
        foreach ($groups as $group) {
            try {
                $this->syncGroupWithLdapGroup($group);
            } catch (\Exception $e) {
                if ("App\Exception\LoggedException" != get_class($e) && "App\Exception\ConnectionException" != get_class($e)) {
                    $this->logger->error('PERS&GRP-checkLdapGroup-student: '.$e->getMessage());
                }
            }

            try {
                $this->syncGroupWithLdapGroup($group, null, true);
            } catch (\Exception $e) {
                if ("App\Exception\LoggedException" != get_class($e) && "App\Exception\ConnectionException" != get_class($e)) {
                    $this->logger->error('PERS&GRP-checkLdapGroup-teacher: '.$e->getMessage());
                }
            }
        }

        $this->checkInternetAccessLdapGroups();
        $this->checkPersonalDeviceAccessLdapGroups();

        $this->em->flush();
    }

    /**
     * Synchronize or create one Group in Ldap/AD based on a Group Datas.
     *
     * @param bool $teacher
     * @param bool $immediate
     *
     * @throws \Exception
     */
    public function syncGroupWithLdapGroup(Group $group, $currentName = null, bool $teacher = false, bool $immediate = false)
    {
        $this->connect();
        if ($teacher && '' == $this->active_directory_generated_teacher_group_prefix) {
            return;
        }

        $groupPrefix = $teacher ? $this->active_directory_generated_teacher_group_prefix : $this->active_directory_generated_group_prefix;
        $ldapName = $groupPrefix.$this->ldapEscape($group->getName());

        if (null !== $currentName) {
            $currentLdapName = $groupPrefix.$this->ldapEscape($currentName);
            if ($currentLdapName == $ldapName || $ldapName == $groupPrefix || $currentName == $groupPrefix) {
                return;
            }

            $ldapGroup = $this->ldapGroupRepository->findOneByName($currentLdapName);
            if (!$ldapGroup instanceof LdapGroup) {
                $this->logger->error('PERS&GRP: Impossible to find the Ldap Group to rename');
                throw new LoggedException('Impossible to find the Ldap Group to rename');
            }

            $ldapGroup->setOperation('RENAME:'.$ldapName);
        } else {
            $ldapGroup = $this->ldapGroupRepository->findOneByName($ldapName);

            if (!$ldapGroup) {
                $ldapGroup = new LdapGroup();
                $ldapGroup->setName($ldapName);
                $ldapGroup->setOperation('CREATE');
                $this->em->persist($ldapGroup);
                $this->logger->info('Group '.$group->getName().($teacher ? ' teacher' : '').' to be created in ldap');
            }
        }

//        $providerLdapGroup = $this->checkProviderLdapGroup($group->getProvider(), $teacher);
//        if ($providerLdapGroup instanceof LdapGroup) {
//            $providerLdapGroup->addMember('group', $ldapGroup->getName());
//        }

        if ($immediate) {
            $this->em->flush();
            $this->ldapProxy->syncLdapGroupWithDBGroup($ldapGroup);
//            if ($providerLdapGroup instanceof LdapGroup) {
//                $this->ldapProxy->syncLdapGroupWithDBGroup($providerLdapGroup);
//            }
            $this->em->flush();
        }
    }

	public function syncLdapTrashedGroupForProvider(Provider $provider, bool $teacher = false, bool $immediate = false)
	{
		$trashedGroup = $this->checkTrashedLdapGroup($provider, $teacher);
		if(!$trashedGroup instanceof LdapGroup) {
			return;
		}

		$ldapUsers = [];
		foreach ($trashedGroup->getMembersList('user') as $username) {
			if (is_string($username) && '' != $username) {
				$ldapUsers[] = strtolower($username);
			}
		}

		$persons = $teacher ? $this->teacherRepository->getTrashedQuery($provider->getId())->execute() : $this->studentRepository->getTrashedQuery($provider->getId())->execute();
		$personsInGroup = [];
		foreach ($persons as $person) {
			if($person->getUsername() != "") {
				$personsInGroup[] = $person->getUsername();
			}
		}

		foreach (array_diff($ldapUsers, $personsInGroup) as $username) {
			$this->logger->info('user '.$username.' removed from trashed group '.$trashedGroup->getName());
			$trashedGroup->removeMember('user', $username);
		}

		foreach (array_diff($personsInGroup, $ldapUsers) as $username) {
			$this->logger->info('user '.$username.' added to trashed group '.$trashedGroup->getName());
			$trashedGroup->addMember('user', $username);
		}

		if($immediate) {
			$this->em->flush();
			$this->ldapProxy->syncLdapGroupMembershipWithDB($trashedGroup);
			$this->em->flush();
		}
	}

    /**
     * Check if exist or create the Ldap/AD group for the provider.
     *
     * @param bool $teacher
     *
     * @return LdapGroup
     */
    protected function checkProviderLdapGroup(Provider $provider, bool $teacher = false)
    {
        $this->connect();
        static $ldapGroups = [];
        $cachedGroupId = (string) $provider->getId().($teacher ? 't' : '');

        if (!isset($ldapGroups[$cachedGroupId])) {
            $this->logger->info('Check if group '.$provider->getName().($teacher ? ' teacher' : '').' exists in ldap');

            $name = $this->ldapEscape($provider->getName());
            $groupPrefix = $teacher ? $this->active_directory_generated_teacher_group_prefix : $this->active_directory_generated_group_prefix;
            $ldapGroups[$cachedGroupId] = $this->ldapGroupRepository->findOneByName(
                $groupPrefix.$name
            );

            if (!$ldapGroups[$cachedGroupId]) {
                $ldapGroups[$cachedGroupId] = new LdapGroup();
                $ldapGroups[$cachedGroupId]->setName($groupPrefix.$name);
                $ldapGroups[$cachedGroupId]->setOperation('CREATE');
                $this->em->persist($ldapGroups[$cachedGroupId]);
                $this->em->flush();
                $this->logger->info('group '.$name.($teacher ? ' teacher' : '').' to be created in ldap');
            }
        }

        return $ldapGroups[$cachedGroupId];
    }

	protected function checkTrashedLdapGroup(Provider $provider, bool $teacher = false) : ?LdapGroup
	{
		static $ldapGroups = [];
		$cachedGroupId = (string) $provider->getId().($teacher ? 't' : '');

		if (!isset($ldapGroups[$cachedGroupId])) {
			$fullGroupName = $this->getTrashedLdapGroupNameForProvider($provider, $teacher);
			if($fullGroupName === null) {
				$ldapGroups[$cachedGroupId] = null;
				return null;
			}

			$this->logger->info('Check if trashed group '.$provider->getName().($teacher ? ' teacher' : '').' exists in ldap');

			$ldapGroups[$cachedGroupId] = $this->ldapGroupRepository->findOneByName($fullGroupName);

			if (!$ldapGroups[$cachedGroupId]) {
				$ldapGroups[$cachedGroupId] = new LdapGroup();
				$ldapGroups[$cachedGroupId]->setName($fullGroupName);
				$ldapGroups[$cachedGroupId]->setOperation('CREATE');
				$this->em->persist($ldapGroups[$cachedGroupId]);
				$this->em->flush();
				$this->logger->info('group '.$fullGroupName.($teacher ? ' teacher' : '').' to be created in ldap');
			}
		}

		return $ldapGroups[$cachedGroupId];
	}

	protected function getTrashedLdapGroupNameForProvider(Provider $provider, bool $teacher = false) : ?string
	{
		$name = $this->ldapEscape($teacher ? $provider->getTeacherTrashedLdapGroupName() : $provider->getStudentTrashedLdapGroupName());
		if($name == "") {
			return null;
		}

		$groupPrefix = $teacher ? $this->active_directory_generated_teacher_group_prefix : $this->active_directory_generated_group_prefix;
		return $groupPrefix.$name;
	}

    /**
     * Check if exist or create the default internet access Ldap/Ad group for activated users/groups.
     */
    public function checkInternetAccessLdapGroups()
    {
        $this->connect();
        $this->logger->info('checking default internetaccess group in ldap');
        $ldapGroup = $this->ldapGroupRepository->find(
            $this->active_directory_generated_group_prefix.'InternetAccess'
        );
        if (!$ldapGroup) {
            $ldapGroup = new LdapGroup();
            $ldapGroup->setName($this->active_directory_generated_group_prefix.'InternetAccess');
            $ldapGroup->setOperation('CREATE');
            $this->em->persist($ldapGroup);
            $this->em->flush();
            $this->logger->info('default internetaccess group to be created in ldap');
        }

        return $ldapGroup;
    }

    /**
     * Check if exist or create the default internet access Ldap/Ad group for activated users/groups.
     */
    public function checkPersonalDeviceAccessLdapGroups()
    {
        $this->connect();
        $this->logger->info('checking default personal device access group in ldap');
        $ldapGroup = $this->ldapGroupRepository->find(
            $this->active_directory_generated_group_prefix.'PersonalDeviceAccess'
        );
        if (!$ldapGroup) {
            $ldapGroup = new LdapGroup();
            $ldapGroup->setName($this->active_directory_generated_group_prefix.'PersonalDeviceAccess');
            $ldapGroup->setOperation('CREATE');
            $this->em->persist($ldapGroup);
            $this->em->flush();
            $this->logger->info('default personal device access group to be created in ldap');
        }

        return $ldapGroup;
    }

    /**
     * Remove the group from Ldap/Ad.
     *
     * @param bool $teacher
     *
     * @throws \Exception
     */
    public function removeGroupLdapGroup(Group $group, bool $teacher = false)
    {
        $this->connect();
        $groupPrefix = $teacher ? $this->active_directory_generated_teacher_group_prefix : $this->active_directory_generated_group_prefix;
        $ldapName = $groupPrefix.$this->ldapEscape($group->getName());

        $ldapGroup = $this->ldapGroupRepository->findOneByName($ldapName);
        if (!$ldapGroup instanceof LdapGroup) {
            $this->logger->error('PERS&GRP: Invalid Ldap Group to delete for the specified Group name '.$ldapName);
            throw new LoggedException('Invalid Ldap Group to delete for the specified Group name '.$ldapName);
        }

        $ldapGroup->setOperation('REMOVE');
        $this->em->persist($ldapGroup);
        $this->em->flush();
        $this->ldapProxy->syncLdapGroupWithDBGroup($ldapGroup);
    }

    /**
     * Create all the OU from the groups.
     */
    public function checkLdapOUs()
    {
        $this->connect();
        $this->logger->info('check groups not present in ldap');
        $groups = $this->groupRepository->findBy(['manageManually' => false]);

        foreach ($groups as $group) {
            try {
                $this->syncGroupWithLdapOU($group);
            } catch (\Exception $e) {
                if ("App\Exception\LoggedException" != get_class($e) && "App\Exception\ConnectionException" != get_class($e)) {
                    $this->logger->error('PERS&GRP-checkLdapOUs: '.$e->getMessage());
                }
            }
        }

        $this->em->flush();
    }

    /**
     * Create the OU of a group, if it not exists.
     */
    public function syncGroupWithLdapOU(Group $group)
    {
        $this->connect();
        $provider = $group->getProvider();
        $providerSettings = $provider->getStudentSettings();
        if (!$providerSettings['ldapCreateOU']) {
            return;
        }

        $prefix = $providerSettings['ldapOUPrefix'] ?: '';

        $this->ldapProxy->createOuIfNotExists($prefix.$group->getName(), $providerSettings['ldapOUPath'] ?: '');
    }

    /**
     * Synchronize all the Student and Teacher with the Ldap users.
     */
    public function checkLdapUsers()
    {
        $this->connect();
        $this->logger->info('check users not present in ldap');
        $students = $this->studentRepository->findAll();
        foreach ($students as $student) {
            try {
                $this->syncPersonWithLdapUser($student);
            } catch (\Exception $e) {
                if ("App\Exception\LoggedException" != get_class($e) && "App\Exception\ConnectionException" != get_class($e)) {
                    $this->logger->error('PERS&GRP-checkLdapUsers: '.$e->getMessage());
                }
            }
        }

        $teachers = $this->teacherRepository->findAll();
        foreach ($teachers as $teacher) {
            try {
                $this->syncPersonWithLdapUser($teacher);
            } catch (\Exception $e) {
                if ("App\Exception\LoggedException" != get_class($e) && "App\Exception\ConnectionException" != get_class($e)) {
                    $this->logger->error('PERS&GRP-checkLdapUsers-teacher: '.$e->getMessage());
                }
            }
        }
        $this->em->flush();
    }

    /**
     * Create or Synchronize an Ldap/AD account with the Person data.
     *
     * @param bool $immediate
     *
     * @throws \Exception
     */
    public function syncPersonWithLdapUser(PersonAbstract $person, bool $immediate = false)
    {
        $this->connect();
        $personId = (string) $person->getId();

        $providerSettings = $person->getProviderSettings();
        $modified = false;
        $ldapUser = $this->ldapUserRepository->findOneByDistinguishedId($personId);

        // create ldap user if not exist
        if (!$ldapUser instanceof LdapUser) {
            if ('' != $person->getUsername() && $this->ldapUserRepository->usernameAlreadyExists($person->getUsername())) {
                $this->logger->error("User with id = {$personId} NOT found in LdapUsers, but the username {$person->getUsername()} is already used in Ldap");

                return;
            }

			// person is trashed and should not be created
			if($person->isTrashed() && $providerSettings['trashedLdapBehaviour'] == 2) {
				return;
			}

            $ldapUser = new LdapUser();
            $ldapUser->setOperation('CREATE');
            $ldapUser->setDistinguishedId($personId);
            $ldapUser->setStartingPassword($person->getStartingPassword());

            $this->logger->info("User with id = {$personId} NOT found in LdapUsers set creation into LDAP");
        } else if($person->isTrashed() && $providerSettings['trashedLdapBehaviour'] == 2) {
	        $ldapUser->setOperation('REMOVE');
	        $this->em->persist($ldapUser);
			if($immediate) {
				$this->em->flush();
				$this->ldapProxy->syncLdapUserWithDBUser($ldapUser);
				$this->em->flush();
			}

	        return;
        }

	    // set the person username if missing and one already exists in ldap
	    if($ldapUser->getUsername() != "" && $person->getUsername() == "") {
		    $person->setUsername($ldapUser->getUsername());
		    $this->logger->info("Set username on person {$ldapUser->getUsername()} from ldap username for user {$personId}");
	    }
        // set username of ldap user if is empty or different from the person one
        else if ('' == $ldapUser->getUsername() || $person->getUsername() != $ldapUser->getUsername()) {
            // generate person username if is empty and the autocreate is active
            $username = ('' == $person->getUsername() && $providerSettings['autoCreateUsername']) ? $this->createUsername($person, $providerSettings['ldapAutoUsernameStyle'] ?: $this->configurationManager->getActiveDirectoryUsernameStyle()) : strtolower((string)$person->getUsername());

            if ('' != $username) {
                $username = substr($username, 0, 20);
                $ldapUser->setUsername($username);
                $person->setUsername($username);
                $this->ldapAccountsUsername[] = $username;
                $modified = true;
                $this->logger->info("Set username {$ldapUser->getUsername()} for user {$personId}");
            } else if($providerSettings['autoCreateUsername']) {
				$this->logger->error('PERS&GRP: Username empty for person '.$person->getId());
				throw new LoggedException('Username empty for person '.$person->getId());
			} else {
				$this->logger->info('PERS&GRP: Username empty for person on a provider without auto create '.$person->getId());
				throw new LdapUsernameMissingAndNotAutoCreated('PERS&GRP: Username empty for person '.$person->getId());
            }
        }
        // modify the ldapuser firstname or lastname if is changed in the person instance
        if ($ldapUser->getFirstname() != $person->getFirstname() || $ldapUser->getLastname() != $person->getLastname()) {
            $ldapUser->setFirstname($person->getFirstname());
            $ldapUser->setLastname($person->getLastname());
            $this->logger->info("Set name {$ldapUser->getLastname()} {$ldapUser->getFirstname()} for user {$ldapUser->getUsername()}");
            $modified = true;
        }

        // prepare the ldapuser attributes
        $currentAttributesJson = $ldapUser->getAttributes();
        $currentAttributes = (array) json_decode($currentAttributesJson);
        $attributes = [
            'description' => 'Creato da igroove'.$this->configurationManager->getActiveDirectorySyncSuffix(),
            'homedrive' => ($this->configurationManager->getActiveDirectoryHomeDrive() !== null &&
                            1 == strlen($this->configurationManager->getActiveDirectoryHomeDrive())) ?
	            strtoupper($this->configurationManager->getActiveDirectoryHomeDrive().':' ) : null,
            'homedirectory' => ($this->configurationManager->getActiveDirectoryHomeFolder() !== null &&
                                strlen($this->configurationManager->getActiveDirectoryHomeFolder() ) > 2) ?
	            $this->configurationManager->getActiveDirectoryHomeFolder().'\\'.$ldapUser->getUsername() : null,
            'email' => 'no@email',
            'radius-vlan' => null,
            'objectGUID' => $currentAttributes['objectGUID'] ?? '',
            'extendedUsername' => $person->getUsername(),
            'disabled' => $person->isTrashed() && $providerSettings['trashedLdapBehaviour'] == 1
        ];

        if ('' != $person->getEmail()) {
            $attributes['email'] = $person->getEmail();
            if ($providerSettings['ldapUseEmailForExtendedUsername']) {
                $attributes['extendedUsername'] = strtolower(substr($person->getEmail(), 0, strpos($person->getEmail(), '@')));
            }
        }

        if ('' != $providerSettings['ldapExtendedUsernameSuffix']) {
            $attributes['extendedUsername'] .= ('@' != substr($providerSettings['ldapExtendedUsernameSuffix'], 0, 1) ? '@' : '').$providerSettings['ldapExtendedUsernameSuffix'];
        } else {
            $attributes['extendedUsername'] .= ('@' != substr($this->configurationManager->getActiveDirectoryAccountSuffix(), 0, 1) ? '@' : '').$this->configurationManager->getActiveDirectoryAccountSuffix();
        }

        // set the vlan id from the group, if the person instance is an user
        if ($person instanceof Student) {
	        $mainClassroom = $person->getNotManuallyManagedGroupOfProvider($person->getProvider());
            if ($mainClassroom instanceof Group && (int) $mainClassroom->getRadiusVlan() > 0) {
                $attributes['radius-vlan'] = (int) $mainClassroom->getRadiusVlan();
            }
        }

        // update the attributes if is changed
        if ($currentAttributes != $attributes) {
            $modified = true;
            $ldapUser->setAttributes(json_encode($attributes));
            $this->logger->info(
                'user '.$ldapUser->getUsername().' attributes has changed ('.$currentAttributesJson.') -> ('.$ldapUser->getAttributes().')'
            );
        }

	    //the user was probably not created in ldap and we should retry creating it
	    if($ldapUser->isCreationFailed()) {
		    $modified = false;
		    $ldapUser->setOperation('CREATE');
		    $this->logger->info("User {$ldapUser->getUsername()} for user {$personId} was probably not created. Set it back to be created.");
	    }

	    if(($ldapUser->getOperation() === 'CREATE') && $providerSettings['forceImportedPasswordInitial']) {
		    if($person->getStartingPassword() == "") {
			    $ldapUser->setOperation("ERROR");
			    $this->logger->error('PERS&GRP: Cannot create the user for person ' . $person->getId().". The password must be passed by the provider and is empty.");
			    throw new LoggedException('PERS&GRP: Cannot create the user for person ' . $person->getId().". The password must be passed by the provider and is empty.");
		    }
		    $ldapUser->setStartingPassword($person->getStartingPassword());
            $this->logger->info('user '.$ldapUser->getUsername().' password has been initially forced');
        } elseif ($providerSettings['forceImportedPassword']) {
		    if($person->getStartingPassword() == "") {
			    $this->logger->error('PERS&GRP: Cannot force the password for the user ' . $person->getUsername().". The password must be passed by the provider and is empty.");
		    } else {
			    $ldapUser->setStartingPassword( $person->getStartingPassword() );
			    $this->logger->info( 'user ' . $ldapUser->getUsername() . ' password forced' );
			    $modified = true;
		    }
        } elseif (empty(trim($person->getStartingPassword()))) {
            $ldapUser->setStartingPassword(
	            $person instanceof Student ? $this->generateStudentPasswordForProvider($person->getProvider()) : $this->generateTeacherPasswordForProvider($person->getProvider())
            );
            $person->setStartingPassword($ldapUser->getStartingPassword());
            $this->logger->info('user '.$ldapUser->getUsername().' password has been created');
	        $modified = true;
        } elseif ($ldapUser->getOperation() === 'CREATE') {
		    $ldapUser->setStartingPassword($person->getStartingPassword());
		    $this->logger->info('user ' . $ldapUser->getUsername() . ' password has been set for user creation');
	    }

        if ('CREATE' == $ldapUser->getOperation()) {
            $this->em->persist($ldapUser);
        } elseif ($modified) {
            $ldapUser->setOperation('MODIFY');
        }
        if ($immediate) {
            $this->em->flush();
            $this->ldapProxy->syncLdapUserWithDBUser($ldapUser);
            $this->em->flush();
        }
    }

    /**
     * Reset the Ldap/AD password to the one set in the Person data.
     *
     * @param bool $immediate
     *
     * @throws \Exception
     */
    public function resetPersonLdapUserPassword(PersonAbstract $person, $immediate = true)
    {
        $this->connect();
        $ldapUser = $this->ldapUserRepository->findOneByUsername($person->getUsername());
        if (!$ldapUser instanceof LdapUser) {
            $this->logger->error('PERS&GRP: Invalid Ldap User for the specified Person '.$person->getUsername());
            throw new LoggedException('Invalid Ldap User for the specified Person '.$person->getUsername());
        }

        $ldapUser->setStartingPassword($person->getStartingPassword());
        $ldapUser->setOperation('MODIFY');

        if ($immediate) {
            $this->em->flush();
            $this->ldapProxy->syncLdapUserWithDBUser($ldapUser);
            $this->em->flush();
        }
    }

    /**
     * Remove the Ldap/AD account for a specific Person.
     *
     * @throws \Exception
     */
    public function removePersonLdapUser(PersonAbstract $person)
    {
        $this->connect();
        $ldapUser = $this->ldapUserRepository->findOneByDistinguishedId($person->getId());
        if (!$ldapUser instanceof LdapUser) {
            $this->logger->error('PERS&GRP: Invalid Ldap User to delete for the specified Person '.$person->getFiscalCode());
            throw new LoggedException('Invalid Ldap User to delete for the specified Person '.$person->getFiscalCode());
        }

        $ldapUser->setOperation('REMOVE');
        $this->em->persist($ldapUser);
        $this->em->flush();
        $this->ldapProxy->syncLdapUserWithDBUser($ldapUser);
    }

    /**
     * Synchronize the Memberships of all the Person Ldap/AD Account with the Ldap/AD Group.
     */
    public function checkLdapUsersMembership()
    {
        $this->connect();
        $students = $this->studentRepository->findAll();
        foreach ($students as $student) {
            try {
                $this->syncPersonMembershipsWithLdapGroups($student);
            } catch (\Exception $e) {
                if ("App\Exception\LoggedException" != get_class($e) && "App\Exception\ConnectionException" != get_class($e)) {
                    $this->logger->error('PERS&GRP-checkLdapUsersMembership('.$student->getId().'): '.$e->getMessage());
                }
            }
        }

        $teachers = $this->teacherRepository->findAll();
        foreach ($teachers as $teacher) {
            try {
                $this->syncPersonMembershipsWithLdapGroups($teacher);
            } catch (\Exception $e) {
                if ("App\Exception\LoggedException" != get_class($e) && "App\Exception\ConnectionException" != get_class($e)) {
                    $this->logger->error('PERS&GRP-checkLdapUsersMembership-teacher('.$teacher->getId().'): '.$e->getMessage());
                }
            }
        }

        $providers = $this->providerRepository->findBy(['active' => true]);
        foreach ($providers as $provider) {
            try {
                $this->syncProviderGroupLdapMembers($provider);
            } catch (\Exception $e) {
                if ("App\Exception\LoggedException" != get_class($e) && "App\Exception\ConnectionException" != get_class($e)) {
                    $this->logger->error('PERS&GRP-checkLdapUsersMembership-provider-student('.$provider->getName().'): '.$e->getMessage());
                }
            }

            try {
                $this->syncProviderGroupLdapMembers($provider, true);
            } catch (\Exception $e) {
                if ("App\Exception\LoggedException" != get_class($e) && "App\Exception\ConnectionException" != get_class($e)) {
                    $this->logger->error('PERS&GRP-checkLdapUsersMembership-provider-teacher('.$provider->getName().'): '.$e->getMessage());
                }
            }

	        try {
		        $this->syncLdapTrashedGroupForProvider($provider);
	        } catch (\Exception $e) {
		        if ("App\Exception\LoggedException" != get_class($e) && "App\Exception\ConnectionException" != get_class($e)) {
			        $this->logger->error('PERS&GRP-checkLdapUsersMembership-student-trashed('.$provider->getName().'): '.$e->getMessage());
		        }
	        }

	        try {
		        $this->syncLdapTrashedGroupForProvider($provider, true);
	        } catch (\Exception $e) {
		        if ("App\Exception\LoggedException" != get_class($e) && "App\Exception\ConnectionException" != get_class($e)) {
			        $this->logger->error('PERS&GRP-checkLdapUsersMembership-teacher-trashed('.$provider->getName().'): '.$e->getMessage());
		        }
	        }
        }

        try {
            $this->em->flush();
        } catch (\Exception $e) {
            $this->logger->error('PERS&GRP-checkLdapUsersMembership-flush: '.$e->getMessage());
        }
    }

    /**
     * Synchronize the membership of a Person Ldap/AD account with the Ldap Group.
     *
     * @param bool $immediate
     *
     * @throws \Exception
     */
    public function syncPersonMembershipsWithLdapGroups(PersonAbstract $person, bool $immediate = false)
    {
        $this->connect();
        $ldapUser = $this->ldapUserRepository->findOneByDistinguishedId($person->getId());
        if (!$ldapUser instanceof LdapUser) {
            return;
        }

        $ldapGroups = [];
        $ldapGroupNamesToSkip = $this->getProvidersLdapGroupName();
		$trashedLdapGroupName = $this->getTrashedLdapGroupNameForProvider($person->getProvider(), $person instanceof Teacher);
		if($trashedLdapGroupName) {
			$ldapGroupNamesToSkip[] = $trashedLdapGroupName;
		}

        foreach ($ldapUser->getMemberOf($this->em) as $ldapGroupName) {
			$ldapGroupName = strtolower($ldapGroupName);
            if (in_array($ldapGroupName, $ldapGroupNamesToSkip)) {
                continue;
            }
            $ldapGroups[] = $ldapGroupName;
        }

	    $personGroups = $personGroupsName = $toUpdateGroups = [];

        // retrive groups from student or teacher
        if ($person instanceof Student) {
	        if(!$person->isTrashed() || $person->getProvider()->isStudentTrashedLdapLeaveInClassesGroup()) {
		        $personGroups = $person->getMemberOf();
	        }

            $groupPrefix = $this->active_directory_generated_group_prefix;
        } elseif ($person instanceof Teacher) {
            if ('' == $this->active_directory_generated_teacher_group_prefix) {
                return;
            }

	        if(!$person->isTrashed() || $person->getProvider()->isTeacherTrashedLdapLeaveInClassesGroup()) {
		        $personGroups = $person->getGroups();
	        }
            $groupPrefix = $this->active_directory_generated_teacher_group_prefix;
        } else {
            $this->logger->error('PERS&GRP: Person type undefined for membership sync');
            throw new LoggedException('Person type undefined for membership sync');
        }

        foreach ($personGroups as $personGroup) {
            $personGroupsName[] = strtolower($groupPrefix.$this->ldapEscape($personGroup->getName()));
        }

        // remove the person instance to the ldap group
        foreach (array_diff($ldapGroups, $personGroupsName) as $ldapGroupName) {
            $ldapGroup = $this->ldapGroupRepository->findOneByName($ldapGroupName);
            if ($ldapGroup) {
                $this->logger->info('user '.$ldapUser->getUsername().' removed from ldap group '.$ldapGroupName);
                $ldapGroup->removeMember('user', $ldapUser->getUsername());
                if (!isset($toUpdateGroups[$ldapGroup->getName()])) {
                    $toUpdateGroups[$ldapGroup->getName()] = $ldapGroup;
                }
            }
        }

        // add the person instance to the ldap group
        foreach (array_diff($personGroupsName, $ldapGroups) as $ldapGroupName) {
            $ldapGroup = $this->ldapGroupRepository->findOneByName($ldapGroupName);
            if ($ldapGroup) {
                $this->logger->info('user '.$ldapUser->getUsername().' added to ldap group '.$ldapGroupName);
                $ldapGroup->addMember('user', $ldapUser->getUsername());
                if (!isset($toUpdateGroups[$ldapGroup->getName()])) {
                    $toUpdateGroups[$ldapGroup->getName()] = $ldapGroup;
                }
            }
        }

        if ($immediate) {
            $this->em->flush();
            foreach ($toUpdateGroups as $toUpdateGroup) {
                $this->ldapProxy->syncLdapGroupMembershipWithDB($toUpdateGroup);
            }
            $this->em->flush();
        }
    }

    /**
     * Synchronize all the Ldap/AD User Memberships in a Group.
     *
     * @param bool $teacher
     *
     * @throws \Exception
     */
    public function syncGroupMembershipWithLdapGroupMembership(Group $group, bool $teacher = false)
    {
        $this->connect();
        if ($teacher && '' == $this->active_directory_generated_teacher_group_prefix) {
            return;
        }

        $groupPrefix = $teacher ? $this->active_directory_generated_teacher_group_prefix : $this->active_directory_generated_group_prefix;
        $ldapName = strtolower($groupPrefix.$this->ldapEscape($group->getName()));
        $ldapGroup = $this->ldapGroupRepository->findOneByName($ldapName);

        if (!$ldapGroup instanceof LdapGroup) {
            return;
        }

        $this->logger->info("checking {$group->getName()} ldap group members");

        $ldapUsers = $personNames = [];
        foreach ($ldapGroup->getMembersList('user') as $username) {
            if (is_string($username) && '' !== $username) {
                $ldapUsers[] = strtolower($username);
            }
        }

        if ($teacher) {
            foreach ($group->getTeachers() as $teacher) {
                if ('' != $teacher->getUsername() && (!$teacher->isTrashed() || $teacher->getProvider()->isTeacherTrashedLdapLeaveInClassesGroup())) {
                    $personNames[] = strtolower($teacher->getUsername());
                }
            }
        } else {
            foreach ($group->getStudents() as $student) {
                if ('' != $student->getUsername() && (!$student->isTrashed() || $student->getProvider()->isStudentTrashedLdapLeaveInClassesGroup())) {
                    $personNames[] = strtolower($student->getUsername());
                }
            }
        }

        foreach (array_diff($ldapUsers, $personNames) as $username) {
            $this->logger->info('user '.$username.' removed from ldap group '.$ldapGroup->getName());
            $ldapGroup->removeMember('user', $username);
        }

        foreach (array_diff($personNames, $ldapUsers) as $username) {
            $this->logger->info('user '.$username.' added to ldap group '.$ldapGroup->getName());
            $ldapGroup->addMember('user', $username);
        }

        $this->em->flush();
        $this->ldapProxy->syncLdapGroupMembershipWithDB($ldapGroup);
        $this->em->flush();
    }

    public function syncProviderGroupLdapMembers(Provider $provider, bool $teacher = false)
    {
        $providerLdapGroup = $this->checkProviderLdapGroup($provider, $teacher);

        if (!$providerLdapGroup instanceof LdapGroup || ($teacher && '' == $this->active_directory_generated_teacher_group_prefix)) {
            return;
        }

        $this->logger->info("checking provider {$provider->getName()} ldap group members");

        $populateWithUsers = $teacher ? $this->configurationManager->getActiveDirectoryGeneratedTeacherProviderGroupWithUsers() : $this->configurationManager->getActiveDirectoryGeneratedProviderGroupWithUsers();
        if ($populateWithUsers) {
            $ldapUsers = $personNames = [];
            foreach ($providerLdapGroup->getMembersList('user') as $username) {
                if (is_string($username) && '' != $username) {
                    $ldapUsers[] = strtolower($username);
                }
            }

            if ($teacher) {
                foreach ($provider->getTeachers() as $teacher) {
                    if ('' != $teacher->getUsername() && (!$teacher->isTrashed() || $provider->isTeacherTrashedLdapLeaveInProviderGroup())) {
                        $personNames[] = strtolower($teacher->getUsername());
                    }
                }

                foreach ($provider->getAdditionalTeachers() as $teacher) {
                    if ('' != $teacher->getUsername() && (!$teacher->isTrashed() || $provider->isTeacherTrashedLdapLeaveInProviderGroup())) {
                        $personNames[] = strtolower($teacher->getUsername());
                    }
                }
            } else {
                foreach ($provider->getStudents() as $student) {
                    if ('' != $student->getUsername() && (!$student->isTrashed() || $provider->isStudentTrashedLdapLeaveInProviderGroup())) {
                        $personNames[] = strtolower($student->getUsername());
                    }
                }

                foreach ($provider->getAdditionalStudents() as $student) {
                    if ('' != $student->getUsername() && (!$student->isTrashed() || $provider->isStudentTrashedLdapLeaveInProviderGroup())) {
                        $personNames[] = strtolower($student->getUsername());
                    }
                }
            }

            foreach (array_diff($ldapUsers, $personNames) as $username) {
                $this->logger->info('user '.$username.' removed from provider group '.$providerLdapGroup->getName());
                $providerLdapGroup->removeMember('user', $username);
            }

            foreach (array_diff($personNames, $ldapUsers) as $username) {
                $this->logger->info('user '.$username.' added to provider group '.$providerLdapGroup->getName());
                $providerLdapGroup->addMember('user', $username);
            }
        } else {
            $ldapGroups = $groupsNames = [];
            foreach ($providerLdapGroup->getMembersList('group') as $groupName) {
                $ldapGroups[] = strtolower($groupName);
            }

            $groupPrefix = $teacher ? $this->active_directory_generated_teacher_group_prefix : $this->active_directory_generated_group_prefix;
            foreach ($provider->getGroups() as $group) {
                $groupsNames[] = strtolower($groupPrefix.$this->ldapEscape($group->getName()));
            }

            foreach (array_diff($ldapGroups, $groupsNames) as $groupName) {
                $this->logger->info('group '.$groupName.' removed from provider group '.$providerLdapGroup->getName());
                $providerLdapGroup->removeMember('group', $groupName);
            }

            foreach (array_diff($groupsNames, $ldapGroups) as $groupName) {
                $this->logger->info('group '.$groupName.' added to provider group '.$providerLdapGroup->getName());
                $providerLdapGroup->addMember('group', $groupName);
            }
        }
    }

    /**
     * Return the list of all the providers ldap groups name (for teacher and student).
     *
     * @return array
     */
    protected function getProvidersLdapGroupName()
    {
        static $groupNames;
        $this->connect();
        if (!isset($groupNames)) {
            $groupNames = [];
            $providers = $this->providerRepository->findAll();
            foreach ($providers as $provider) {
                $groupNames[] = strtolower($this->active_directory_generated_group_prefix.$this->ldapEscape($provider->getName()));
                if ('' != $this->active_directory_generated_teacher_group_prefix) {
                    $groupNames[] = strtolower($this->active_directory_generated_teacher_group_prefix.$this->ldapEscape($provider->getName()));
                }
            }
        }

        return $groupNames;
    }

    /**
     * Synchronize all the vlan id of the Ldap/AD Account in a Group.
     *
     * @throws \Exception
     */
    public function syncGroupMembersVlan(Group $group)
    {
        $this->connect();
        if ($group->getManageManually()) {
            return;
        }

        $students = $group->getStudents();
        foreach ($students as $student) {
            if ($student->getProvider()->getId() !== $group->getProvider()->getId()) {
                continue;
            }

            $ldapUser = $this->ldapUserRepository->findOneByDistinguishedId($student->getId());
            if (!$ldapUser instanceof LdapUser) {
                continue;
            }
            $attributes = (array) json_decode($ldapUser->getAttributes());
            $attributes['radius-vlan'] = (int) $group->getRadiusVlan() > 0 ? (int) $group->getRadiusVlan() : '';
            $ldapUser->setAttributes(json_encode($attributes));
            $ldapUser->setOperation('MODIFY');
            $this->ldapProxy->syncLdapUserWithDBUser($ldapUser);
        }
    }

    /**
     * Synchronize the Memberships of all the Person Account with the Ldap OU.
     */
    public function checkLdapUsersOUMembership()
    {
        $this->connect();
        $groups = $this->groupRepository->findBy(['manageManually' => false]);
        foreach ($groups as $group) {
            try {
                $this->syncGroupMembershipsWithLdapOU($group, false);
            } catch (\Exception $e) {
                if ("App\Exception\LoggedException" != get_class($e) && "App\Exception\ConnectionException" != get_class($e)) {
                    $this->logger->error('PERS&GRP-checkLdapUsersOUMembership('.$group->getName().'): '.$e->getMessage());
                }
            }
        }

        $teachers = $this->teacherRepository->findAll();
        foreach ($teachers as $teacher) {
            try {
                $this->moveTeacherToLdapOU($teacher, false);
            } catch (\Exception $e) {
                if ("App\Exception\LoggedException" != get_class($e) && "App\Exception\ConnectionException" != get_class($e)) {
                    $this->logger->error('PERS&GRP-checkLdapUsersOUMembership-teacher('.$teacher->getId().'): '.$e->getMessage());
                }
            }
        }


	    $providers = $this->providerRepository->findBy(['active' => true]);
	    foreach ($providers as $provider) {
		    try {
			    $this->syncTrashedLdapOUForProvider($provider);
		    } catch ( \Exception $e ) {
			    if ( "App\Exception\LoggedException" != get_class( $e ) && "App\Exception\ConnectionException" != get_class( $e ) ) {
				    $this->logger->error( 'PERS&GRP-checkLdapUsersOUMembership-provider-student('.$provider->getName().'): ' . $e->getMessage() );
			    }
		    }

		    try {
			    $this->syncTrashedLdapOUForProvider($provider, true);
		    } catch ( \Exception $e ) {
			    if ( "App\Exception\LoggedException" != get_class( $e ) && "App\Exception\ConnectionException" != get_class( $e ) ) {
				    $this->logger->error( 'PERS&GRP-checkLdapUsersOUMembership-provider-teacher('.$provider->getName().'): ' . $e->getMessage() );
			    }
		    }
	    }

        try {
            $this->em->flush();
        } catch (\Exception $e) {
            $this->logger->error('PERS&GRP-checkLdapUsersOUMembership-flush: '.$e->getMessage());
        }
    }

    /**
     * Synchronize all the LdapUser OU in a Group.
     *
     * @throws \Exception
     */
    public function syncGroupMembershipsWithLdapOU(Group $group, $immediate = true)
    {
        $this->connect();
        $provider = $group->getProvider();
        $providerSettings = $provider->getStudentSettings();
        if (!$providerSettings['ldapCreateOU']) {
            return;
        }

        $ouName = $providerSettings['ldapOUPrefix'] ? $providerSettings['ldapOUPrefix'].$this->ldapEscape($group->getName()) : $this->ldapEscape($group->getName());
        $path = $providerSettings['ldapOUPath'] ?: '';
        $members = $group->getStudents();
        $studentsInGroup = [];
        foreach ($members as $member) {
            if ('' != $member->getUsername() && $member->getProvider()->getId() === $provider->getId() && (!$member->isTrashed() || $provider->getStudentTrashedLdapOUPath() == "")) {
                $studentsInGroup[] = $member->getUsername();
            }
        }

        $this->ldapProxy->createOuIfNotExists($ouName, $path);

		if($immediate) {
			$this->ldapProxy->updateUsersIntoOu($ouName, $studentsInGroup, $path);
		} else {
			$message = new LdapMessage();
			$message->setAction('updateUsersIntoOu');
			$message->setParameters([
				'ouName' => $ouName,
				'users' => $studentsInGroup,
				'path' => $path
			]);
			$this->messageBus->dispatch(new Envelope($message));
		}
    }

    /**
     * Move a LdapUser into the teacher OU.
     *
     * @throws \Exception
     */
    public function moveTeacherToLdapOU(Teacher $teacher, $immediate = true)
    {
        $this->connect();
        $provider = $teacher->getProvider();
        $providerSettings = $provider->getTeacherSettings();

        if ('' == $teacher->getUsername() || $teacher->isTrashed() || !$providerSettings['ldapCreateOU']) {
            return;
        }

		$ouName = $providerSettings['ldapOUPrefix'] ?: 'Teachers';
		$path = $providerSettings['ldapOUPath'] ?: '';

        $this->ldapProxy->createOuIfNotExists($ouName, $path);
        if($immediate) {
	        $this->ldapProxy->moveUserIntoOu($ouName, $teacher->getUsername(), $path);
        } else {
	        $message = new LdapMessage();
	        $message->setAction('moveUserIntoOu');
	        $message->setParameters([
		        'ouName' => $ouName,
		        'username' => $teacher->getUsername(),
		        'path' => $path
	        ]);
	        $this->messageBus->dispatch(new Envelope($message));
        }
    }

    /**
     * Move a LdapUser into the OU specified.
     *
     * @param $ouName
     *
     * @throws \Exception
     */
    public function moveStudentToLdapOU(Student $student, $ouName)
    {
        $this->connect();
        $provider = $student->getProvider();
        $providerSettings = $provider->getStudentSettings();
        if ('' == $student->getUsername() || $student->isTrashed() || !$providerSettings['ldapCreateOU'] || $ouName == "") {
            return;
        }

	    $prefix = $providerSettings['ldapOUPrefix'] ?: '';
	    $path = $providerSettings['ldapOUPath'] ?: '';
	    $ouName = $prefix.$this->ldapEscape($ouName);

        $this->ldapProxy->createOuIfNotExists($ouName, $path);
        $this->ldapProxy->moveUserIntoOu($ouName, $student->getUsername(), $path);
    }

	public function syncTrashedLdapOUForProvider(Provider $provider, bool $teacher = false, bool $immediate = false) : void
	{
		$this->connect();
		$providerSettings = $teacher ? $provider->getTeacherSettings() : $provider->getStudentSettings();
		$trashedLdapOUPath = $providerSettings['trashedLdapOUPath'];
		if(str_ends_with($trashedLdapOUPath, "/")) {
			$trashedLdapOUPath = substr($trashedLdapOUPath, 0, -1);
		}

		if($trashedLdapOUPath == "") {
			return;
		}

		$explodedPath = explode("/", $trashedLdapOUPath);
		$ouName = $this->ldapEscape(array_pop($explodedPath));
		$path = implode("/", $explodedPath);

		$persons = $teacher ? $this->teacherRepository->getTrashedQuery($provider->getId())->execute() : $this->studentRepository->getTrashedQuery($provider->getId())->execute();
		$personsInGroup = [];
		foreach ($persons as $person) {
			if($person->getUsername() != "") {
				$personsInGroup[] = $person->getUsername();
			}
		}

		$this->ldapProxy->createOuIfNotExists($ouName, $path);
		if($immediate) {
			$this->ldapProxy->updateUsersIntoOu($ouName, $personsInGroup, $path);
		} else {
			$message = new LdapMessage();
			$message->setAction('updateUsersIntoOu');
			$message->setParameters([
				'ouName' => $ouName,
				'users' => $personsInGroup,
				'path' => $path
			]);
			$this->messageBus->dispatch(new Envelope($message));
		}
	}

    /**
     * Check that all the Groups in database are present in GApps.
     */
    public function checkGoogleAppsGroups()
    {
        $this->connect();
        $groups = $this->groupRepository->findAll();

        foreach ($groups as $group) {
            try {
                $this->syncGroupWithGoogleGroup($group);
            } catch (\Exception $e) {
                if ("App\Exception\LoggedException" != get_class($e) && "App\Exception\ConnectionException" != get_class($e)) {
                    $this->logger->error('PERS&GRP-checkGoogleAppsGroups: '.$e->getMessage());
                }
            }

            try {
                $this->syncGroupWithGoogleGroup($group, null, true);
            } catch (\Exception $e) {
                if ("App\Exception\LoggedException" != get_class($e) && "App\Exception\ConnectionException" != get_class($e)) {
                    $this->logger->error('PERS&GRP-checkGoogleAppsGroups-teacher: '.$e->getMessage());
                }
            }
        }

        $providers = $this->providerRepository->findBy(['active' => true]);
        foreach ($providers as $provider) {
            try {
                $this->syncProviderGroupWithGoogleGroup($provider);
            } catch (\Exception $e) {
                if ("App\Exception\LoggedException" != get_class($e) && "App\Exception\ConnectionException" != get_class($e)) {
                    $this->logger->error('PERS&GRP-checkGoogleAppsGroups-provider-student: '.$e->getMessage());
                }
            }

            try {
                $this->syncProviderGroupWithGoogleGroup($provider, true);
            } catch (\Exception $e) {
                if ("App\Exception\LoggedException" != get_class($e) && "App\Exception\ConnectionException" != get_class($e)) {
                    $this->logger->error('PERS&GRP-checkGoogleAppsGroups-provider-teacher: '.$e->getMessage());
                }
            }

	        try {
		        $this->syncGoogleAppsTrashedGroupForProvider($provider);
	        } catch (\Exception $e) {
		        if ("App\Exception\LoggedException" != get_class($e) && "App\Exception\ConnectionException" != get_class($e)) {
			        $this->logger->error('PERS&GRP-checkGoogleAppsGroups-provider-student-trashed: '.$e->getMessage());
		        }
	        }

	        try {
		        $this->syncGoogleAppsTrashedGroupForProvider($provider, true);
	        } catch (\Exception $e) {
		        if ("App\Exception\LoggedException" != get_class($e) && "App\Exception\ConnectionException" != get_class($e)) {
			        $this->logger->error('PERS&GRP-checkGoogleAppsGroups-provider-teacher-trashed: '.$e->getMessage());
		        }
	        }

        }
    }

    /**
     * Synchronize or create one Group in GApps based on a Group Datas.
     *
     * @param string $previousGroupName
     * @param bool   $teacher
     * @param bool   $immediate
     *
     * @throws \Exception
     */
    public function syncGroupWithGoogleGroup(Group $group, $previousGroupName = '', bool $teacher = false, bool $immediate = false)
    {
        $this->connect();
        $provider = $group->getProvider();
        $providerSettings = $teacher ? $provider->getTeacherSettings() : $provider->getStudentSettings();
        if (empty($providerSettings['googleAppClientId']) || !$providerSettings['googleAppCreateGroup']) {
            return;
        }
        $groupName = ($providerSettings['googleAppGroupPrefix'] ?: '').$this->googleAppsEscape($group->getName());
        $fullPreviousGroupName = ($providerSettings['googleAppGroupPrefix'] ?: '').$previousGroupName;

        $gam = $this->getGoogleAppsManagerForProvider($provider, $teacher);
        if ($gam->groupExists($groupName, !$immediate)) {
            return;
        }

	    $this->logger->info("Google Group {$groupName} need to be created");

        if ('' != $previousGroupName && $fullPreviousGroupName != $groupName) {
            if ($immediate) {
                $gam->renameGroup($groupName, $fullPreviousGroupName);
            } else {
                $this->sendRabbitMQMessage(
                    'renameGroup',
                    $provider,
                    $teacher,
                    ['groupName' => $groupName, 'previousGroupName' => $fullPreviousGroupName]
                );
            }
        } else {
            if ($immediate) {
                $gam->createGroup($groupName);
            } else {
                $this->sendRabbitMQMessage('createGroup', $provider, $teacher, ['groupName' => $groupName]);
            }
        }

        if ($providerSettings['googleAppCreateProviderGroup'] && $immediate) {
            $gam->addUserToGroup($groupName, ($providerSettings['googleAppGroupPrefix'] ?: '').$provider->getName());
        }
    }

    /**
     * Syncronize all the members of a provider in the relative GApps group.
     *
     * @param bool $teacher
     * @param bool $immediate
     *
     * @throws \Exception
     */
    public function syncProviderGroupWithGoogleGroup(Provider $provider, bool $teacher = false, bool $immediate = false)
    {
        $this->connect();
        $providerSettings = $teacher ? $provider->getTeacherSettings() : $provider->getStudentSettings();
        if (empty($providerSettings['googleAppClientId']) || !$providerSettings['googleAppCreateProviderGroup']) {
            return;
        }

        $groupPrefix = ($providerSettings['googleAppGroupPrefix'] ?: '');
        $groupName = $groupPrefix.$this->googleAppsEscape($provider->getName());
        $gam = $this->getGoogleAppsManagerForProvider($provider, $teacher);
        if (!$gam->groupExists($groupName, !$immediate)) {
            $gam->createGroup($groupName);
        }

        $membersInGroup = [];

        if ($providerSettings['googleAppUseUserInProviderGroup']) {
            if ($teacher) {
                foreach ($provider->getTeachers() as $teacherEntity) {
                    if ('' != $teacherEntity->getEmail() && filter_var($teacherEntity->getEmail(), FILTER_VALIDATE_EMAIL)
                            && (!$teacherEntity->isTrashed() || $providerSettings['trashedGoogleAppLeaveInProviderGroup'])) {
                        $membersInGroup[] = $teacherEntity->getEmail();
                    }
                }

                foreach ($provider->getAdditionalTeachers() as $teacherEntity) {
                    if ('' != $teacherEntity->getEmail() && filter_var($teacherEntity->getEmail(), FILTER_VALIDATE_EMAIL)
                            && (!$teacherEntity->isTrashed() || $providerSettings['trashedGoogleAppLeaveInProviderGroup'])) {
                        $membersInGroup[] = $teacherEntity->getEmail();
                    }
                }
            } else {
                foreach ($provider->getStudents() as $student) {
                    if ('' != $student->getEmail() && filter_var($student->getEmail(), FILTER_VALIDATE_EMAIL)
                            && (!$student->isTrashed() || $providerSettings['trashedGoogleAppLeaveInProviderGroup'])) {
                        $membersInGroup[] = $student->getEmail();
                    }
                }

                foreach ($provider->getAdditionalStudents() as $student) {
                    if ('' != $student->getEmail() && filter_var($student->getEmail(), FILTER_VALIDATE_EMAIL)
                            && (!$student->isTrashed() || $providerSettings['trashedGoogleAppLeaveInProviderGroup'])) {
                        $membersInGroup[] = $student->getEmail();
                    }
                }
            }
        } else {
            foreach ($provider->getGroups() as $group) {
                $membersInGroup[] = $gam->getCleanedGroupName($groupPrefix.$group, true);
            }
        }

        if ('' != $providerSettings['googleAppProviderGroupExtraEmail']) {
            foreach (explode(',', $providerSettings['googleAppProviderGroupExtraEmail']) as $email) {
                if (filter_var($email, FILTER_VALIDATE_EMAIL)) {
                    $membersInGroup[] = trim($email);
                }
            }
        }

        if ($immediate) {
            $gam->updateGroupMembers($membersInGroup, $groupName);
        } else {
            $this->sendRabbitMQMessage(
                'updateGroupMembers',
                $provider,
                $teacher,
                [
                    'studentsInGroup' => $membersInGroup,
                    'groupName' => $groupName,
                ]
            );
        }
    }

    /**
     * Remove the Group from GApps.
     *
     * @param bool $teacher
     * @param bool $immediate
     */
    public function removeGroupGoogleAppsGroup(Group $group, bool $teacher = false, bool $immediate = false)
    {
        $this->connect();
        $provider = $group->getProvider();
        $providerSettings = $teacher ? $provider->getTeacherSettings() : $provider->getStudentSettings();
        if (empty($providerSettings['googleAppClientId']) || !$providerSettings['googleAppCreateGroup']) {
            return;
        }
        $groupName = ($providerSettings['googleAppGroupPrefix'] ?: '').$this->googleAppsEscape($group->getName());

        if ($immediate) {
            $this->getGoogleAppsManagerForProvider($provider, $teacher)->removeGroup($groupName);
        } else {
            $this->sendRabbitMQMessage('removeGroup', $provider, $teacher, ['groupName' => $groupName]);
        }
    }

    /**
     * Check that all the Groups in database are present as GApps OU.
     */
    public function checkGoogleAppsOUs()
    {
        $this->connect();
        $groups = $this->groupRepository->findBy(['manageManually' => false]);

        foreach ($groups as $group) {
            try {
                $this->syncGroupWithGoogleOU($group);
            } catch (\Exception $e) {
                if ("App\Exception\LoggedException" != get_class($e) && "App\Exception\ConnectionException" != get_class($e)) {
                    $this->logger->error('PERS&GRP-checkGoogleAppsOUs: '.$e->getMessage());
                }
            }
        }
    }

    /**
     * Synchronize or create one Group as GApps OU.
     *
     * @param bool $immediate
     *
     * @throws \Exception
     */
    public function syncGroupWithGoogleOU(Group $group, bool $immediate = false)
    {
        $provider = $group->getProvider();
        $providerSettings = $provider->getStudentSettings();
        if (empty($providerSettings['googleAppClientId']) || !$providerSettings['googleAppCreateOU']) {
            return;
        }
        $prefix = $providerSettings['googleAppOUPrefix'] ?: '';
        $gam = $this->getGoogleAppsManagerForProvider($provider, false);
        $groupName = $prefix.$this->googleAppsEscape($group->getName());

        if ($gam->ouExists($groupName, !$immediate)) {
            return;
        }

		$this->logger->info("Google OU for group {$groupName} need to be created");

        if ($immediate) {
            $gam->createOU($groupName);
        } else {
            $this->sendRabbitMQMessage('createOU', $provider, false, ['ouName' => $groupName]);
        }
    }

    /**
     * Synchronize all the Student and Teacher with the Gapps account.
     */
    public function checkGoogleAppsUsers()
    {
        $this->connect();
        $students = $this->studentRepository->findAll();
        foreach ($students as $student) {
            try {
                $this->syncPersonWithGoogleAppsUser($student);
            } catch (\Exception $e) {
                if ("App\Exception\LoggedException" != get_class($e) && "App\Exception\ConnectionException" != get_class($e)) {
                    $this->logger->error('PERS&GRP-checkGoogleAppsUsers: '.$e->getMessage());
                }
            }
        }

        $teachers = $this->teacherRepository->findAll();
        foreach ($teachers as $teacher) {
            try {
                $this->syncPersonWithGoogleAppsUser($teacher);
            } catch (\Exception $e) {
                if ("App\Exception\LoggedException" != get_class($e) && "App\Exception\ConnectionException" != get_class($e)) {
                    $this->logger->error('PERS&GRP-checkGoogleAppsUsers-teacher: '.$e->getMessage());
                }
            }
        }

        try {
            $this->em->flush();
        } catch (\Exception $e) {
            $this->logger->error('PERS&GRP-checkGoogleAppsUsers-flush: '.$e->getMessage());
        }
    }

    /**
     * Create or Synchronize a GApps account with the Person data.
     *
     * @param string $previousEmail
     * @param bool   $immediate
     *
     * @throws \Exception
     */
    public function syncPersonWithGoogleAppsUser(PersonAbstract $person, $previousEmail = '', bool $immediate = false)
    {
        $this->connect();
        $provider = $person->getProvider();
        $teacher = $person instanceof Teacher;
        $providerSettings = $teacher ? $provider->getTeacherSettings() : $provider->getStudentSettings();
        if (empty($providerSettings['googleAppClientId'])) {
            return;
        }

        if (3 == $providerSettings['googleAppAutoEmail']) {
            return;
        }

        $ldapUser = $this->ldapUserRepository->findOneByDistinguishedId($person->getId());
        $gam = $this->getGoogleAppsManagerForProvider($provider, $teacher);
        $email = $this->getEmailForPerson($person);

        if ('' == $email) {
            return;
        }

        if (!$gam->emailInDomain($email)) {
            $this->logger->error('PERS&GRP-syncPersWithGApU: Invalid email for user with id '.$person->getId());
            throw new LoggedException('Invalid email for user with id '.$person->getId());
        }

		$user = $gam->getUser($email, !$immediate);
        if ($user !== null) {
            if ($person->getEmail() != $email) {
                $person->setEmail($email);
	            if ($ldapUser instanceof LdapUser) {
		            $this->updateLdapUserEmail($email, $ldapUser, $person, $immediate);
	            }
	            $this->em->flush();
            }

			if($user->getName()->getGivenName() != $person->getFirstname() || $user->getName()->getFamilyName() != $person->getLastname()) {
				$gam->updateUser($email, $person->getFirstname(), $person->getLastname());
			}

			if($person->isTrashed()) {
				if($providerSettings['trashedGoogleBehaviour'] == 2) {
					if ($immediate) {
						$gam->removeUser($email);
					} else {
						$this->sendRabbitMQMessage('removeUser', $provider, $person instanceof Teacher, ['email' => $email]);
					}
				} else if($providerSettings['trashedGoogleBehaviour'] == 1) {
					$gam->setSuspendedUser($email);
				}
			} else if($user->getSuspended()) {
				$gam->setUnsuspendedUser($email);
			}

            return;
        }

        if ('' == $person->getStartingPassword()) {
            $this->logger->error("Empty password for user {$person->getUsername()}. Cannot create email {$email}");

            return;
        }

		if($person->isTrashed()) {
			return;
		}

        $this->gappsAccountsUsername[$providerSettings['googleAppDomain']][] = $email;
        if ('' != $previousEmail && $previousEmail != $email) {
            if ($immediate) {
                $gam->renameUser($email, $previousEmail, $person->getFirstname(), $person->getLastname());
                if ($person->getEmail() != $email) {
                    $person->setEmail($email);
	                $this->em->flush();
	                if ($ldapUser instanceof LdapUser) {
	                    $this->updateLdapUserEmail($email, $ldapUser, $person);
                    }
                }
            } else {
                $this->sendRabbitMQMessage(
                    'renameUser',
                    $provider,
                    $teacher,
                    [
                        'email' => $email,
                        'firstname' => $person->getFirstname(),
                        'lastname' => $person->getLastname(),
                        'previousEmail' => $previousEmail,
                        'userId' => $person->getId(),
                        'teacher' => $teacher,
                    ]
                );
            }
        } else {
            if ($immediate) {
                $gam->createUser($email, $person->getFirstname(), $person->getLastname(), $person->getStartingPassword());
                if ($person->getEmail() != $email) {
                    $person->setEmail($email);
	                $this->em->flush();
	                if ($ldapUser instanceof LdapUser) {
	                    $this->updateLdapUserEmail($email, $ldapUser, $person);
                    }
                }
            } else {
                $this->sendRabbitMQMessage(
                    'createUser',
                    $provider,
                    $teacher,
                    [
                        'email' => $email,
                        'firstname' => $person->getFirstname(),
                        'lastname' => $person->getLastname(),
                        'password' => $person->getStartingPassword(),
                        'userId' => $person->getId(),
                        'teacher' => $teacher,
                    ]
                );
            }
        }
    }

    protected function updateLdapUserEmail($email, LdapUser $ldapUser, PersonAbstract $person, $immediate = true)
    {
        $providerSettings = $person->getProviderSettings();
        $ldapUser->setAttribute('email', $email);
        if ($providerSettings['ldapUseEmailForExtendedUsername']) {
            $extendedUsername = strtolower(substr($person->getEmail(), 0, strpos($person->getEmail(), '@')));
            if ('' != $providerSettings['ldapExtendedUsernameSuffix']) {
                $extendedUsername .= ('@' != substr($providerSettings['ldapExtendedUsernameSuffix'], 0, 1) ? '@' : '').$providerSettings['ldapExtendedUsernameSuffix'];
            } else {
                $extendedUsername .= ('@' != substr($this->configurationManager->getActiveDirectoryAccountSuffix(), 0, 1) ? '@' : '').$this->configurationManager->getActiveDirectoryAccountSuffix();
            }
            $ldapUser->setAttribute('extendedUsername', $extendedUsername);
        }

        if ('CREATE' != $ldapUser->getOperation()) {
	        $ldapUser->setOperation($ldapUser->isCreationFailed() ? 'CREATE' : 'MODIFY');
        }

        if ($immediate) {
            $this->em->flush();
            $this->ldapProxy->syncLdapUserWithDBUser($ldapUser);
        }
    }

    /**
     * Reset the Google Apps account password to the one set in the Person data.
     *
     * @param bool $immediate
     *
     * @throws \Exception
     */
    public function resetPersonGoogleAppsUserPassword(PersonAbstract $person, $immediate = true)
    {
        $this->connect();
        $provider = $person->getProvider();
        $teacher = $person instanceof Teacher;
        $email = $person->getEmail();
        $providerSettings = $teacher ? $provider->getTeacherSettings() : $provider->getStudentSettings();
        if (empty($providerSettings['googleAppClientId'])) {
            return;
        }

        if ('' == $email) {
            $this->logger->error('PERS&GRP-resetGAppPwd: Invalid email');
            throw new LoggedException('Invalid email');
        }

        $gam = $this->getGoogleAppsManagerForProvider($provider, $teacher);

        if ($immediate) {
            $gam->resetUserPassword($email, $person->getStartingPassword());
        } else {
            $this->sendRabbitMQMessage(
                'resetUserPassword',
                $provider,
                $teacher,
                [
                    'email' => $email,
                    'password' => $person->getStartingPassword(),
                ]
            );
        }
    }

    /**
     * Remove the GApps account for a Person.
     *
     * @param bool $immediate
     *
     * @throws \Exception
     */
    public function removePersonGoogleAppsUser(PersonAbstract $person, bool $immediate = false)
    {
        $this->connect();
        $email = $person->getEmail();
        $provider = $person->getProvider();
        $providerSettings = $person->getProviderSettings();
        if (empty($providerSettings['googleAppClientId']) || '' == $email) {
            return;
        }

        $gam = $this->getGoogleAppsManagerForProvider($provider, $person instanceof Teacher);
        if (!$gam->emailInDomain($email)) {
            return;
        }

        if ($immediate) {
            $gam->removeUser($email);
        } else {
            $this->sendRabbitMQMessage('removeUser', $provider, $person instanceof Teacher, ['email' => $email]);
        }
    }

    /**
     * Synchronize the Memberships of all the Person GApps Account with the GApps Group.
     */
    public function checkGoogleAppsUsersMembershipToGroup()
    {
        $this->connect();
        $groups = $this->groupRepository->findAll();
        foreach ($groups as $group) {
            try {
                $this->syncGroupMembershipsWithGoogleGroupMemberships($group);
            } catch (\Exception $e) {
                if ("App\Exception\LoggedException" != get_class($e) && "App\Exception\ConnectionException" != get_class($e)) {
                    $this->logger->error('PERS&GRP-checkGoogleAppsUsersMembershipToGroup: '.$e->getMessage());
                }
            }

            try {
                $this->syncGroupMembershipsWithGoogleGroupMemberships($group, true);
            } catch (\Exception $e) {
                if ("App\Exception\LoggedException" != get_class($e) && "App\Exception\ConnectionException" != get_class($e)) {
                    $this->logger->error('PERS&GRP-checkGoogleAppsUsersMembershipToGroup-teacher: '.$e->getMessage());
                }
            }
        }
    }

    /**
     * Synchronize all the GApps account Memberships in a Group.
     *
     * @param bool $teacher
     * @param bool $immediate
     *
     * @throws \Exception
     */
    public function syncGroupMembershipsWithGoogleGroupMemberships(Group $group, bool $teacher = false, bool $immediate = false)
    {
        $this->connect();
        $provider = $group->getProvider();
        $providerSettings = $teacher ? $provider->getTeacherSettings() : $provider->getStudentSettings();
        if (empty($providerSettings['googleAppClientId']) || !$providerSettings['googleAppCreateGroup']) {
            return;
        }

        $groupName = $providerSettings['googleAppGroupPrefix'] ? $providerSettings['googleAppGroupPrefix'].$this->googleAppsEscape($group->getName()) : $this->googleAppsEscape($group->getName());
        $members = $teacher ? $group->getTeachers() : $group->getStudents();
        $personsInGroup = [];
        foreach ($members as $member) {
			if($member->isTrashed() && !$providerSettings['trashedGoogleAppLeaveInClassesGroup']) {
				continue;
			}

            $ldapUser = $this->ldapUserRepository->findOneByUsername($member->getUsername());
            if (!is_null($ldapUser) && '' != $ldapUser->getEmail()) {
                $personsInGroup[] = $ldapUser->getEmail();
            }
        }

        if ('' != $providerSettings['googleAppGroupExtraEmail']) {
            foreach (explode(',', $providerSettings['googleAppGroupExtraEmail']) as $email) {
                if (filter_var($email, FILTER_VALIDATE_EMAIL)) {
                    $personsInGroup[] = trim($email);
                }
            }
        }

        if ($immediate) {
            $this->getGoogleAppsManagerForProvider($provider, $teacher)->updateGroupMembers($personsInGroup, $groupName);
        } else {
            $this->sendRabbitMQMessage(
                'updateGroupMembers',
                $provider,
                $teacher,
                [
                    'studentsInGroup' => $personsInGroup,
                    'groupName' => $groupName,
                ]
            );
        }
    }

	public function syncGoogleAppsTrashedGroupForProvider(Provider $provider, bool $teacher = false, bool $immediate = false)
	{
		$providerSettings = $teacher ? $provider->getTeacherSettings() : $provider->getStudentSettings();
		if (empty($providerSettings['googleAppClientId']) || !$providerSettings['googleAppCreateGroup'] || $providerSettings['trashedGoogleAppGroupName'] == "") {
			return;
		}

		$groupName = $this->googleAppsEscape($providerSettings['trashedGoogleAppGroupName']);
		$persons = $teacher ? $this->teacherRepository->getTrashedQuery($provider->getId())->execute() : $this->studentRepository->getTrashedQuery($provider->getId())->execute();
		$personsInGroup = [];
		foreach ($persons as $person) {
			if($person->getEmail() != "") {
				$personsInGroup[] = $person->getEmail();
			}
		}

		if ($immediate) {
			$this->getGoogleAppsManagerForProvider($provider, $teacher)->updateGroupMembers($personsInGroup, $groupName);
		} else {
			$this->sendRabbitMQMessage(
				'updateGroupMembers',
				$provider,
				$teacher,
				[
					'studentsInGroup' => $personsInGroup,
					'groupName' => $groupName,
				]
			);
		}
	}

    /**
     * Synchronize the Memberships of all the Person GApps Account with the GApps OU.
     */
    public function checkGoogleAppsUsersMembershipToOU()
    {
        $this->connect();
        $groups = $this->groupRepository->findBy(['manageManually' => false]);
        foreach ($groups as $group) {
            try {
                $this->syncGroupMembershipsWithGoogleOU($group);
            } catch (\Exception $e) {
                if ("App\Exception\LoggedException" != get_class($e) && "App\Exception\ConnectionException" != get_class($e)) {
                    $this->logger->error('PERS&GRP-checkGoogleAppsUsersMembershipToOU: '.$e->getMessage());
                }
            }
        }

        $providers = $this->providerRepository->findBy(['active' => true]);
		/** @var Provider $provider */
	    foreach ($providers as $provider) {
		    $ouToSync = [];
            try {
	            $this->syncTrashedGoogleAppsOUForProvider($provider);

                $providerSettings = $provider->getTeacherSettings();
                if (empty($providerSettings['googleAppClientId'])) {
                    continue;
                }

				$this->syncTrashedGoogleAppsOUForProvider($provider, true);

	            if(!$providerSettings['googleAppCreateOU']) {
					continue;
	            }

	            $teacherOuName = $this->googleAppsEscape($providerSettings['googleAppOUPrefix']);
	            if (!isset($ouToSync[$teacherOuName])) {
		            $ouToSync[$teacherOuName] = [];
	            }

	            $teachers = $this->teacherRepository->findBy(['provider' => $provider->getId()]);
	            foreach ($teachers as $teacher) {
		            if ($teacher->getEmail()) {
						if (!$teacher->isTrashed()) {
							$ouToSync[$teacherOuName][] = $teacher->getEmail();
						}
		            }
	            }
            } catch (\Exception $e) {
                if ("App\Exception\LoggedException" != get_class($e) && "App\Exception\ConnectionException" != get_class($e)) {
                    $this->logger->error('PERS&GRP-checkGoogleAppsUsersMembershipToOU-prov: '.$e->getMessage());
                }
            }

		    foreach ($ouToSync as $ouName => $members) {
			    try {
				    $this->sendRabbitMQMessage(
					    'updateOUMembers',
					    $provider,
					    true,
					    ['studentsInOU' => $members, 'ouName' => $ouName]
				    );
			    } catch (\Exception $e) {
				    if ("App\Exception\LoggedException" != get_class($e) && "App\Exception\ConnectionException" != get_class($e)) {
					    $this->logger->error('PERS&GRP-checkGoogleAppsUsersMembershipToOU-send: '.$e->getMessage());
				    }
			    }
		    }
        }

    }

    /**
     * Synchronize all the GApps account OU in a Group.
     *
     * @param bool $immediate
     *
     * @throws \Exception
     */
    public function syncGroupMembershipsWithGoogleOU(Group $group, bool $immediate = false)
    {
        $this->connect();
        $provider = $group->getProvider();
        $providerSettings = $provider->getStudentSettings();
        if (empty($providerSettings['googleAppClientId']) || !$providerSettings['googleAppCreateOU']) {
            return;
        }

        $ouName = $providerSettings['googleAppOUPrefix'] ? $providerSettings['googleAppOUPrefix'].$this->googleAppsEscape($group->getName()) : $this->googleAppsEscape($group->getName());
        $members = $group->getStudents();
        $studentsInGroup = [];
        foreach ($members as $member) {
            if ('' != $member->getEmail() && $member->getProvider()->getId() === $provider->getId() && (!$member->isTrashed() || $provider->getStudentTrashedGoogleAppOUPath() == "")) {
                $studentsInGroup[] = $member->getEmail();
            }
        }

        if ($immediate) {
            $this->getGoogleAppsManagerForProvider($provider, false)->updateOUMembers($studentsInGroup, $ouName);
        } else {
            $this->sendRabbitMQMessage(
                'updateOUMembers',
                $provider,
                false,
                [
                    'studentsInOU' => $studentsInGroup,
                    'ouName' => $ouName,
                ]
            );
        }
    }

	/**
	 * Move a GApps account into the teacher OU.
	 *
	 * @param Teacher $teacher
	 * @param bool $immediate
	 * @param string|null $gAppsOu
	 *
	 * @throws \Exception
	 */
    public function moveTeacherToGoogleAppsOU(Teacher $teacher, bool $immediate = false, ?string $gAppsOu = null) : void
    {
        $this->connect();
        $provider = $teacher->getProvider();
        $providerSettings = $provider->getTeacherSettings();
        if (empty($providerSettings['googleAppClientId']) || '' == $teacher->getEmail() || $teacher->isTrashed() || !$providerSettings['googleAppCreateOU']) {
            return;
        }

	    $ouName = null !== $gAppsOu ? $this->googleAppsEscape($gAppsOu) : ($providerSettings['googleAppOUPrefix'] ?: ''); //@todo prevent googleAppsEscape from escaping the "/"?

        $gam = $this->getGoogleAppsManagerForProvider($provider, true);
        if (!$gam->emailInDomain($teacher->getEmail()) || !$gam->userExists($teacher->getEmail(), !$immediate)) {
            return;
        }

        // @todo controllare se il docente non è già nell'ou, e nel caso non eseguire il comando

        if (!$gam->ouExists($ouName, !$immediate)) {
            if ($immediate) {
                $this->getGoogleAppsManagerForProvider($provider, true)->createOU($ouName);
            } else {
                $this->sendRabbitMQMessage('createOU', $provider, true, ['ouName' => $ouName]);
            }
        }

        if ($immediate) {
            $this->getGoogleAppsManagerForProvider($provider, true)->addUserToOU($teacher->getEmail(), $ouName);
        } else {
            $this->sendRabbitMQMessage('addUserToOU', $provider, true, ['email' => $teacher->getEmail(), 'ouName' => $ouName]);
        }
    }

	/**
	 * Move a GApps account into the OU specified.
	 *
	 * @param Student $student
	 * @param string $ouName
	 * @param bool $immediate
	 *
	 * @throws \Exception
	 */
    public function moveStudentToGoogleAppsOU(Student $student, string $ouName, bool $immediate = false) : void
    {
        $this->connect();
        $email = $student->getEmail();
        $provider = $student->getProvider();
        $providerSettings = $provider->getStudentSettings();
        if (empty($providerSettings['googleAppClientId']) || '' == $email || '' == $ouName || $student->isTrashed() || !$providerSettings['googleAppCreateOU']) {
            return;
        }

	    $ouName = ($providerSettings['googleAppOUPrefix'] ?: '') . $this->googleAppsEscape($ouName);

        $gam = $this->getGoogleAppsManagerForProvider($provider, false);
        if (!$gam->emailInDomain($email)) { // || !$gam->userExists($email)
            return;
        }

        if (!$gam->ouExists($ouName, !$immediate)) {
            if ($immediate) {
                $this->getGoogleAppsManagerForProvider($provider, false)->createOU($ouName);
            } else {
                $this->sendRabbitMQMessage('createOU', $provider, false, ['ouName' => $ouName]);
            }
        }

        if ($immediate) {
            $this->getGoogleAppsManagerForProvider($provider, false)->addUserToOU($email, $ouName);
        } else {
            $this->sendRabbitMQMessage('addUserToOU', $provider, false, ['email' => $email, 'ouName' => $ouName]);
        }
    }

	public function syncTrashedGoogleAppsOUForProvider(Provider $provider, bool $teacher = false, bool $immediate = false) : void
	{
		$providerSettings = $teacher ? $provider->getTeacherSettings() : $provider->getStudentSettings();
		if(empty($providerSettings['googleAppClientId']) || $providerSettings['trashedGoogleOUPath'] == "") {
			return;
		}

		$ouName = $this->googleAppsEscape($providerSettings['trashedGoogleOUPath']);
		$persons = $teacher ? $this->teacherRepository->getTrashedQuery($provider->getId())->execute() : $this->studentRepository->getTrashedQuery($provider->getId())->execute();
		$personsInGroup = [];
		foreach ($persons as $person) {
			if($person->getEmail() != "") {
				$personsInGroup[] = $person->getEmail();
			}
		}

		if ($immediate) {
			$this->getGoogleAppsManagerForProvider($provider, $teacher)->updateOUMembers($personsInGroup, $ouName);
		} else {
			$this->sendRabbitMQMessage(
				'updateOUMembers',
				$provider,
				$teacher,
				[
					'studentsInOU' => $personsInGroup,
					'ouName' => $ouName,
				]
			);
		}
	}

    public function prepareFormErrorMessage($message, \Exception $e)
    {
        if ($this->inDebug) {
            throw $e;
        }

        if ($this->authorizationChecker->isGranted('ROLE_ADMIN')) {
            $message .= ":  {$e->getMessage()}"; // ({$e->getFile()}:{$e->getLine()})
        }

        return $message;
    }

    public function addErrorsToForm(\Exception $e, Form $form)
    {
        if ($this->inDebug) {
            throw $e;
        }

        foreach (explode(PHP_EOL, $e->getMessage()) as $message) {
            $form->addError(new FormError($message));
        }
    }

    /**
     * Get the GoogleApps Manager configured with the data of the provider.
     *
     * @param bool $teacher
     *
     * @return GoogleApps
     *
     * @throws \Exception
     */
    protected function getGoogleAppsManagerForProvider(Provider $provider, $teacher)
    {
        $this->connect();
        $providerSettings = $teacher ? $provider->getTeacherSettings() : $provider->getStudentSettings();
        $key = ($teacher ? 't_' : '').$providerSettings['googleAppDomain']."_".$providerSettings['googleAppOUPath'];

        if (!isset($this->googleAppManagers[$key])) {
            $this->googleAppManagers[$key] = new GoogleApps(
                $providerSettings['googleAppDomain'],
                $providerSettings['googleAppClientId'],
                $providerSettings['googleAppClientSecret'],
                $providerSettings['googleAppOUPath'],
                $this->projectDir,
                $this->logger
            );
        }

        if (!$this->googleAppManagers[$key] instanceof GoogleApps) {
            $this->logger->error('PERS&GRP-getGappMang: Impossible to connect to the Gapps Service');
            throw new LoggedException('Impossible to connect to the Gapps Service');
        }

        return $this->googleAppManagers[$key];
    }

    /**
     * Prepare and send a RabbitMQ Command.
     *
     * @param $command
     * @param $provider
     * @param $teacher
     * @param $parameters
     */
    protected function sendRabbitMQMessage($command, $provider, $teacher, $parameters)
    {
        $this->connect();
        if ('' == $command || !is_array($parameters) || !$provider instanceof Provider) {
            return;
        }

        $providerSettings = $teacher ? $provider->getTeacherSettings() : $provider->getStudentSettings();

        $parameters = $parameters + [
                'domain' => $providerSettings['googleAppDomain'],
                'clientId' => $providerSettings['googleAppClientId'],
                'clientSecret' => $providerSettings['googleAppClientSecret'],
                'ouPath' => $providerSettings['googleAppOUPath'],
            ];

        $message = new GoogleMessage();
        $message->setCommand($command);
        $message->setParameters($parameters);
        $this->messageBus->dispatch(new Envelope($message));
    }

    /**
     * Create a username for an Ldap User, that is not currently used.
     *
     * @param string          $usernameStyle
     * @param GoogleApps|null $gam
     *
     * @return string
     *
     * @throws \Exception
     */
    public function createUsername(PersonAbstract $person, $usernameStyle, $gam = null)
    {
        $this->connect();
        $limit = 20;
        if ($gam instanceof GoogleApps) {
            $emailDomain = $gam->getDomain();
            if (!isset($this->gappsAccountsUsername[$emailDomain])) {
                $this->gappsAccountsUsername[$emailDomain] = [];
                foreach ($gam->getUsersList() as $user) {
                    $this->gappsAccountsUsername[$emailDomain][] = str_replace('@'.$emailDomain, '', strtolower($user->getPrimaryEmail()));
                }
            }

            $existentUsernames = $this->gappsAccountsUsername[$emailDomain];
            if ($this->configurationManager->getEmailLengthLimit() > 0 || 0 === $this->configurationManager->getEmailLengthLimit() || '0' === $this->configurationManager->getEmailLengthLimit()) {
                $limit = (int) $this->configurationManager->getEmailLengthLimit();
            }
        } else {
            if (null === $this->ldapAccountsUsername) {
                $this->ldapAccountsUsername = [];
                $ldapAllUsers = $this->ldapUserRepository->findAll();
                foreach ($ldapAllUsers as $ldapUser) {
		            if($ldapUser->getOperation() == 'ERROR') {
			            $ldapUserAttributes = (array)json_decode($ldapUser->getAttributes());
			            if(!isset($ldapUserAttributes['objectGUID']) || $ldapUserAttributes['objectGUID'] == "") {
				            continue;
			            }
		            }

		            $this->ldapAccountsUsername[] = strtolower($ldapUser->getUsername());
	            }

                $studentsUsername = $this->studentRepository->getUsernamesNotInLdapUser();
                $teachersUsername = $this->teacherRepository->getUsernamesNotInLdapUser();

                $this->ldapAccountsUsername = array_merge($this->ldapAccountsUsername, $studentsUsername, $teachersUsername);
            }

            $existentUsernames = $this->ldapAccountsUsername;
        }
        $usernameStyle = strtoupper($usernameStyle);

        $this->logger->info("PERS&GRP: Try to generate username with style $usernameStyle for user with {$person->getId()}: ".$person->getFirstname().' '.$person->getLastname());
        if ('INIZIALENOME.COGNOME' == $usernameStyle) {
            $lastname = $this->ldapEscape(str_replace([' ', '\''], '', $person->getLastname()));
            $firstname = $this->ldapEscape(str_replace([' ', '\''], '', $person->getFirstname()));
            $tryUsername = false;
            for ($i = 1; $i <= strlen($firstname); ++$i) {
                $tryUsername = strtolower(substr($firstname, 0, $i).'.'.$lastname);
                if ($limit > 0) {
                    $tryUsername = substr($tryUsername, 0, $limit);
                }
                if (!in_array($tryUsername, $existentUsernames)) {
                    $username = $tryUsername;
                    break;
                }
            }

            if (!isset($username)) {
                for ($i = 2; $i <= 9; ++$i) {
                    $tryUsername = strtolower("$firstname$i.$lastname");
                    if ($limit > 0) {
                        $tryUsername = substr($tryUsername, 0, $limit);
                    }
                    if (!in_array($tryUsername, $existentUsernames)) {
                        $username = $tryUsername;
                        break;
                    }
                }
            }

            if (!$tryUsername) {
                $this->logger->error('PERS&GRP: Error generating username with style INIZIALENOME.COGNOME for user '.$person->getFirstname().' '.$person->getLastname());
                throw new LoggedException("Impossible to create an username for the user {$person->getId()} with style INIZIALENOME.COGNOME");
            }
        } elseif ('INIZIALENOMECOGNOME' == $usernameStyle) {
            $lastname = $this->ldapEscape(str_replace([' ', '\''], '', $person->getLastname()));
            $firstname = $this->ldapEscape(str_replace([' ', '\''], '', $person->getFirstname()));
            $tryUsername = false;
            for ($i = 1; $i <= strlen($firstname); ++$i) {
                $tryUsername = strtolower(substr($firstname, 0, $i).$lastname);
                if ($limit > 0) {
                    $tryUsername = substr($tryUsername, 0, $limit);
                }
                if (!in_array($tryUsername, $existentUsernames)) {
                    $username = $tryUsername;
                    break;
                }
            }

            if (!isset($username)) {
                for ($i = 2; $i <= 9; ++$i) {
                    $tryUsername = strtolower($firstname.$i.$lastname);
                    if ($limit > 0) {
                        $tryUsername = substr($tryUsername, 0, $limit);
                    }
                    if (!in_array($tryUsername, $existentUsernames)) {
                        $username = $tryUsername;
                        break;
                    }
                }
            }

            if (!$tryUsername) {
                $this->logger->error('PERS&GRP: Error generating username with style INIZIALENOMECOGNOME for user '.$person->getFirstname().' '.$person->getLastname());
                throw new LoggedException("Impossible to create an username for the user {$person->getId()} with style INIZIALENOMECOGNOME");
            }
        } elseif ('ANNOINIZIALENOMECOGNOME' == $usernameStyle) {
            $lastname = strtolower(substr($this->ldapEscape(str_replace([' ', '\''], '', $person->getLastname())), 0, 20));
            $firstname = strtolower(substr($this->ldapEscape(str_replace([' ', '\''], '', $person->getFirstname())), 0, 20));

            $currentYear = (int) date('m') > 5 ? (int) date('Y') : (int) date('Y') - 1;
            $tryUsername = false;
            for ($i = 1; $i <= strlen($firstname); ++$i) {
                $tryUsername = strtolower($currentYear.substr($firstname, 0, $i).$lastname);
                if ($limit > 0) {
                    $tryUsername = substr($tryUsername, 0, $limit);
                }
                if (!in_array($tryUsername, $existentUsernames)) {
                    $username = $tryUsername;
                    break;
                }
            }

            if (!isset($username)) {
                for ($i = 2; $i <= 9; ++$i) {
                    $tryUsername = strtolower($currentYear.$firstname.$i.$lastname);
                    if ($limit > 0) {
                        $tryUsername = substr($tryUsername, 0, $limit);
                    }
                    if (!in_array($tryUsername, $existentUsernames)) {
                        $username = $tryUsername;
                        break;
                    }
                }
            }

            if (!$tryUsername) {
                $this->logger->error('PERS&GRP: Error generating username with style ANNOINIZIALENOMECOGNOME for user '.$person->getFirstname().' '.$person->getLastname());
                throw new LoggedException("Impossible to create an username for the user {$person->getId()} with style ANNOINIZIALENOMECOGNOME");
            }
        } elseif ('NOME.COGNOME' == $usernameStyle) {
            $lastname = substr($this->ldapEscape(str_replace([' ', '\''], '', $person->getLastname())), 0, 20);
            $firstname = substr($this->ldapEscape(str_replace([' ', '\''], '', $person->getFirstname())), 0, 20);

            $tryUsername = strtolower("$firstname.$lastname");
            if ($limit > 0) {
                $tryUsername = substr($tryUsername, 0, $limit);
            }
            if (!in_array($tryUsername, $existentUsernames)) {
                $username = $tryUsername;
            }

            if (!isset($username)) {
                for ($i = 2; $i <= 9; ++$i) {
                    $tryUsername = strtolower("$firstname$i.$lastname");
                    if ($limit > 0) {
                        $tryUsername = substr($tryUsername, 0, $limit);
                    }
                    if (!in_array($tryUsername, $existentUsernames)) {
                        $username = $tryUsername;
                        break;
                    }
                }
            }

            if (!$tryUsername) {
                $this->logger->error('PERS&GRP: Error generating username with style NOME.COGNOME for user'.$person->getFirstname().' '.$person->getLastname());
                throw new LoggedException("Impossible to create an username for the user {$person->getId()} with style NOME.COGNOME");
            }
        } elseif ('COGNOME.NOME' == $usernameStyle) {
            $lastname = substr($this->ldapEscape(str_replace([' ', '\''], '', $person->getLastname())), 0, 20);
            $firstname = substr($this->ldapEscape(str_replace([' ', '\''], '', $person->getFirstname())), 0, 20);

            $tryUsername = strtolower("$lastname.$firstname");
            if ($limit > 0) {
                $tryUsername = substr($tryUsername, 0, $limit);
            }
            if (!in_array($tryUsername, $existentUsernames)) {
                $username = $tryUsername;
            }

            if (!isset($username)) {
                $tryUsername = false;
                for ($i = 2; $i <= 9; ++$i) {
                    $tryUsername = strtolower("$lastname$i.$firstname");
                    if ($limit > 0) {
                        $tryUsername = substr($tryUsername, 0, $limit);
                    }
                    if (!in_array($tryUsername, $existentUsernames)) {
                        $username = $tryUsername;
                        break;
                    }
                }
            }

            if (!$tryUsername) {
                $this->logger->error('PERS&GRP: Error generating username with style COGNOME.NOME for user'.$person->getFirstname().' '.$person->getLastname());
                throw new LoggedException("Impossible to create an username for the user {$person->getId()} with style COGNOME.NOME");
            }
        } elseif ('COGNOME.PRIMONOME' == $usernameStyle) {
            $explodedFirstname = explode(' ', $person->getFirstname());
            $firstFirstname = count($explodedFirstname) > 1 ? $explodedFirstname[0] : $person->getFirstname();
            $firstname = $this->ldapEscape(str_replace([' ', '\''], '', $firstFirstname));
            $fullFirstname = $this->ldapEscape(str_replace([' ', '\''], '', $person->getFirstname()));
            $lastname = $this->ldapEscape(str_replace([' ', '\''], '', $person->getLastname()));

            $tryUsername = strtolower("$lastname.$firstname");
            if ($limit > 0) {
                $tryUsername = substr($tryUsername, 0, $limit);
            }
            if (!in_array($tryUsername, $existentUsernames)) {
                $username = $tryUsername;
            }

            if (!isset($username)) {
                $tryUsername = strtolower("$lastname.$fullFirstname");
                if ($limit > 0) {
                    $tryUsername = substr($tryUsername, 0, $limit);
                }
                if (!in_array($tryUsername, $existentUsernames)) {
                    $username = $tryUsername;
                }
            }

            if (!isset($username)) {
                $tryUsername = false;
                for ($i = 2; $i <= 9; ++$i) {
                    $tryUsername = strtolower("$lastname$i.$firstname");
                    if ($limit > 0) {
                        $tryUsername = substr($tryUsername, 0, $limit);
                    }
                    if (!in_array($tryUsername, $existentUsernames)) {
                        $username = $tryUsername;
                        break;
                    }

                    $tryUsername = strtolower("$lastname$i.$fullFirstname");
                    if ($limit > 0) {
                        $tryUsername = substr($tryUsername, 0, $limit);
                    }
                    if (!in_array($tryUsername, $existentUsernames)) {
                        $username = $tryUsername;
                        break;
                    }
                }
            }

            if (!$tryUsername) {
                $this->logger->error('PERS&GRP: Error generating username with style COGNOME.PRIMONOME for user'.$person->getFirstname().' '.$person->getLastname());
                throw new LoggedException("Impossible to create an username for the user {$person->getId()} with style COGNOME.PRIMONOME");
            }
        } elseif ('PRIMONOMECOGNOME' == $usernameStyle) {
            $explodedFirstname = explode(' ', $person->getFirstname());
            $firstFirstname = count($explodedFirstname) > 1 ? $explodedFirstname[0] : $person->getFirstname();
            $firstname = $this->ldapEscape(str_replace([' ', '\''], '', $firstFirstname));
            $lastname = $this->ldapEscape(str_replace([' ', '\''], '', $person->getLastname()));

            $tryUsername = strtolower("$firstname$lastname");
            if ($limit > 0) {
                $tryUsername = substr($tryUsername, 0, $limit);
            }
            if (!in_array($tryUsername, $existentUsernames)) {
                $username = $tryUsername;
            }

            if (!isset($username)) {
                $tryUsername = false;
                for ($i = 2; $i <= 9; ++$i) {
                    $tryUsername = strtolower("$firstname$lastname");
                    if ($limit > 0) {
                        $tryUsername = substr($tryUsername, 0, $limit - 1);
                    }
                    $tryUsername .= $i;

                    if (!in_array($tryUsername, $existentUsernames)) {
                        $username = $tryUsername;
                        break;
                    }
                }
            }

            if (!$tryUsername) {
                $this->logger->error('PERS&GRP: Error generating username with style PRIMONOMECOGNOME for user'.$person->getFirstname().' '.$person->getLastname());
                throw new LoggedException("Impossible to create an username for the user {$person->getId()} with style PRIMONOMECOGNOME");
            }
        } elseif ('PRIMONOME.COGNOME' === $usernameStyle) {
            $explodedFirstname = explode(' ', $person->getFirstname());
            $firstFirstname = count($explodedFirstname) > 1 ? $explodedFirstname[0] : $person->getFirstname();
            $firstname = $this->ldapEscape(str_replace([' ', '\''], '', $firstFirstname));
            $lastname = $this->ldapEscape(str_replace([' ', '\''], '', $person->getLastname()));

            $tryUsername = strtolower("$firstname.$lastname");
            if ($limit > 0) {
                $tryUsername = substr($tryUsername, 0, $limit);
            }
            if (!in_array($tryUsername, $existentUsernames)) {
                $username = $tryUsername;
            }

            if (!isset($username)) {
                $tryUsername = false;
                for ($i = 2; $i <= 9; ++$i) {
                    $tryUsername = strtolower("$firstname.$lastname");
                    if ($limit > 0) {
                        $tryUsername = substr($tryUsername, 0, $limit - 1);
                    }
                    $tryUsername .= $i;

                    if (!in_array($tryUsername, $existentUsernames)) {
                        $username = $tryUsername;
                        break;
                    }
                }
            }

            if (!$tryUsername) {
                $this->logger->error('PERS&GRP: Error generating username with style PRIMONOME.COGNOME for user'.$person->getFirstname().' '.$person->getLastname());
                throw new LoggedException("Impossible to create an username for the user {$person->getId()} with style PRIMONOME.COGNOME");
            }
        } elseif ('COGNOME.NOME.ANNONASCITA' == $usernameStyle) {
            $lastname = substr($this->ldapEscape(str_replace([' ', '\''], '', $person->getLastname())), 0, 20);
            $firstname = substr($this->ldapEscape(str_replace([' ', '\''], '', $person->getFirstname())), 0, 20);
            $birthYear = $this->ldapEscape(substr($person->getFiscalCode(), 6, 2));

            $tryUsername = strtolower("$lastname.$firstname");
            if ($limit > 0) {
                $tryUsername = substr($tryUsername, 0, $limit - 3);
            }
            $tryUsername .= ".$birthYear";

            if (!in_array($tryUsername, $existentUsernames)) {
                $username = $tryUsername;
            }

            if (!isset($username)) {
                $tryUsername = false;
                for ($i = 2; $i <= 9; ++$i) {
                    $tryUsername = strtolower("$lastname.$firstname");
                    if ($limit > 0) {
                        $tryUsername = substr($tryUsername, 0, $limit - 5);
                    }
                    $tryUsername .= ".$birthYear.$i";

                    if (!in_array($tryUsername, $existentUsernames)) {
                        $username = $tryUsername;
                        break;
                    }
                }
            }

            if (!$tryUsername) {
                $this->logger->error('PERS&GRP: Error generating username with style COGNOME.NOME.ANNONASCITA for user'.$person->getFirstname().' '.$person->getLastname());
                throw new LoggedException("Impossible to create an username for the user {$person->getId()} with style COGNOME.NOME.ANNONASCITA");
            }
        } elseif ('NOME.COGNOME.ANNONASCITA' == $usernameStyle) {
            $lastname = substr($this->ldapEscape(str_replace([' ', '\''], '', $person->getLastname())), 0, 20);
            $firstname = substr($this->ldapEscape(str_replace([' ', '\''], '', $person->getFirstname())), 0, 20);
            $birthYear = $this->ldapEscape(substr($person->getFiscalCode(), 6, 2));

            $tryUsername = strtolower("$firstname.$lastname");
            if ($limit > 0) {
                $tryUsername = substr($tryUsername, 0, $limit - 3);
            }
            $tryUsername .= ".$birthYear";
            if (!in_array($tryUsername, $existentUsernames)) {
                $username = $tryUsername;
            }

            if (!isset($username)) {
                $tryUsername = false;
                for ($i = 2; $i <= 9; ++$i) {
                    $tryUsername = strtolower("$firstname.$lastname");
                    if ($limit > 0) {
                        $tryUsername = substr($tryUsername, 0, $limit - 5);
                    }
                    $tryUsername .= ".$birthYear.$i";
                    if (!in_array($tryUsername, $existentUsernames)) {
                        $username = $tryUsername;
                        break;
                    }
                }
            }

            if (!$tryUsername) {
                $this->logger->error('PERS&GRP: Error generating username with style NOME.COGNOME.ANNONASCITA for user'.$person->getFirstname().' '.$person->getLastname());
                throw new LoggedException("Impossible to create an username for the user {$person->getId()} with style NOME.COGNOME.ANNONASCITA");
            }
        } elseif ('COGNOME.PRIMONOME.ANNONASCITA' == $usernameStyle) {
            $explodedFirstname = explode(' ', $person->getFirstname());
            $firstFirstname = count($explodedFirstname) > 1 ? $explodedFirstname[0] : $person->getFirstname();
            $firstname = $this->ldapEscape(str_replace([' ', '\''], '', $firstFirstname));
            $fullFirstname = $this->ldapEscape(str_replace([' ', '\''], '', $person->getFirstname()));
            $lastname = $this->ldapEscape(str_replace([' ', '\''], '', $person->getLastname()));
            $birthYear = $this->ldapEscape(substr($person->getFiscalCode(), 6, 2));

            $tryUsername = strtolower("$lastname.$firstname");
            if ($limit > 0) {
                $tryUsername = substr($tryUsername, 0, $limit - 3);
            }
            $tryUsername .= ".$birthYear";
            if (!in_array($tryUsername, $existentUsernames)) {
                $username = $tryUsername;
            }

            if (!isset($username)) {
                $tryUsername = strtolower("$lastname.$fullFirstname");
                if ($limit > 0) {
                    $tryUsername = substr($tryUsername, 0, $limit - 3);
                }
                $tryUsername .= ".$birthYear";
                if (!in_array($tryUsername, $existentUsernames)) {
                    $username = $tryUsername;
                }
            }

            if (!isset($username)) {
                for ($i = 2; $i <= 9; ++$i) {
                    $tryUsername = strtolower("$lastname.$firstname");
                    if ($limit > 0) {
                        $tryUsername = substr($tryUsername, 0, $limit - 5);
                    }
                    $tryUsername .= ".$birthYear.$i";
                    if (!in_array($tryUsername, $existentUsernames)) {
                        $username = $tryUsername;
                        break;
                    }

                    $tryUsername = strtolower("$lastname.$fullFirstname");
                    if ($limit > 0) {
                        $tryUsername = substr($tryUsername, 0, $limit - 5);
                    }
                    $tryUsername .= ".$birthYear.$i";
                    if (!in_array($tryUsername, $existentUsernames)) {
                        $username = $tryUsername;
                        break;
                    }
                }
            }

            if (!$tryUsername) {
                $this->logger->error('PERS&GRP: Error generating username with style COGNOME.NOME.ANNONASCITA for user'.$person->getFirstname().' '.$person->getLastname());
                throw new LoggedException("Impossible to create an username for the user {$person->getId()} with style COGNOME.NOME.ANNONASCITA");
            }
        } elseif ('PRIMONOME.COGNOME.ANNONASCITA' == $usernameStyle) {
            $explodedFirstname = explode(' ', $person->getFirstname());
            $firstFirstname = count($explodedFirstname) > 1 ? $explodedFirstname[0] : $person->getFirstname();
            $firstname = $this->ldapEscape(str_replace([' ', '\''], '', $firstFirstname));
            $fullFirstname = $this->ldapEscape(str_replace([' ', '\''], '', $person->getFirstname()));
            $lastname = $this->ldapEscape(str_replace([' ', '\''], '', $person->getLastname()));
            $birthYear = $this->ldapEscape(substr($person->getFiscalCode(), 6, 2));

            $tryUsername = strtolower("$firstname.$lastname");
            if ($limit > 0) {
                $tryUsername = substr($tryUsername, 0, $limit - 3);
            }
            $tryUsername .= ".$birthYear";
            if (!in_array($tryUsername, $existentUsernames)) {
                $username = $tryUsername;
            }

            if (!isset($username)) {
                $tryUsername = strtolower("$fullFirstname.$lastname");
                if ($limit > 0) {
                    $tryUsername = substr($tryUsername, 0, $limit - 3);
                }
                $tryUsername .= ".$birthYear";
                if (!in_array($tryUsername, $existentUsernames)) {
                    $username = $tryUsername;
                }
            }

            if (!isset($username)) {
                for ($i = 2; $i <= 9; ++$i) {
                    $tryUsername = strtolower("$firstname.$lastname");
                    if ($limit > 0) {
                        $tryUsername = substr($tryUsername, 0, $limit - 5);
                    }
                    $tryUsername .= ".$birthYear.$i";
                    if (!in_array($tryUsername, $existentUsernames)) {
                        $username = $tryUsername;
                        break;
                    }

                    $tryUsername = strtolower("$fullFirstname.$lastname");
                    if ($limit > 0) {
                        $tryUsername = substr($tryUsername, 0, $limit - 5);
                    }
                    $tryUsername .= ".$birthYear.$i";
                    if (!in_array($tryUsername, $existentUsernames)) {
                        $username = $tryUsername;
                        break;
                    }
                }
            }

            if (!$tryUsername) {
                $this->logger->error('PERS&GRP: Error generating username with style NOME.COGNOME.ANNONASCITA for user'.$person->getFirstname().' '.$person->getLastname());
                throw new LoggedException("Impossible to create an username for the user {$person->getId()} with style NOME.COGNOME.ANNONASCITA");
            }
        } elseif ('NOMEUTENTE-DA-EMAIL' == $usernameStyle) {
            if ($limit > 0) {
                $tryUsername = strtolower(substr($person->getEmail(), 0, min($limit, strpos($person->getEmail(), '@'))));
            } else {
                $tryUsername = strtolower(substr($person->getEmail(), 0, strpos($person->getEmail(), '@')));
            }
            if (!in_array($tryUsername, $existentUsernames)) {
                $username = $tryUsername;
            }

            if (!$tryUsername) {
                $this->logger->error('PERS&GRP: Error generating username with style NOMEUTENTE-DA-EMAIL for user'.$person->getFirstname().' '.$person->getLastname());
                throw new LoggedException("Impossible to create an username for the user {$person->getId()} with style COGNOME.PRIMONOME");
            }
        }

        if (!isset($username) || '' == $username || '.' == $username) {
            $this->logger->error("PERS&GRP: Impossible to create an username for the user {$person->getId()}");
            throw new LoggedException("Impossible to create an username for the user {$person->getId()}");
        }

        if ( str_ends_with( $username, '.' ) ) {
            $username = substr($username, 0, -1);
        }

        return $username;
    }

    public function getEmailForPerson(PersonAbstract $person)
    {
        /*
         *  'Crea solo gli indirizzi email presenti nel provider'=>0,
         *  'Crea gli indirizzi presenti nel provider e usa lo username se mancano'=>1,
         *  'Crea gli indirizzi email usando lo username e non considerando quanto presente nel provider'=>2,
         * 	'Crea gli indirizzi usando una sintassi specificata' => 4,
         *	'Crea gli indirizzi presenti nel provider oppure crea gli indirizzi usando una sintassi specificata' => 5,
         *  'Non creare nulla, sospendi la creazione degli indirizzi email'=>3,
         */
        $providerSettings = $person->getProviderSettings();
        $email = '';

//        echo "\r\n----------------------------------------------------------------";
//        echo "\r\nUsername:      ".  $person->getUsername();
//        echo "\r\nAttuale email: ". $person->getEmail();

        switch ($providerSettings['googleAppAutoEmail']) {
            case 0:
                $email = $person->getEmail();
                break;
            case 1:
                $email = $person->getEmail();
                if (empty($email) or false === strpos($email, '@'.$providerSettings['googleAppDomain'])) {
                    $email = $person->getUsername().'@'.$providerSettings['googleAppDomain'];
                }
                break;
            case 2:
                $email = $person->getUsername().'@'.$providerSettings['googleAppDomain'];
                break;
            case 3:
                $email = '';
                break;
            case 4:
                $email = $person->getEmail();
                if (empty($email)) {
                    $gam = $this->getGoogleAppsManagerForProvider($person->getProvider(), $person instanceof Teacher);
                    $email = $this->createUsername($person, $providerSettings['googleAppAutoEmailStyle'], $gam).'@'.$providerSettings['googleAppDomain'];
                }
                break;
            case 5:
                $email = $person->getEmail();
                if (empty($email) or false === strpos($email, '@'.$providerSettings['googleAppDomain'])) {
                    $gam = $this->getGoogleAppsManagerForProvider($person->getProvider(), $person instanceof Teacher);
                    $email = $this->createUsername($person, $providerSettings['googleAppAutoEmailStyle'], $gam).'@'.$providerSettings['googleAppDomain'];
                }
        }
//        echo "\r\nNuova email:   ". $email;

        return $email !== null ? trim(strtolower($email)) : '';
    }

	/**
	 * @param $subject
	 *
	 * @return string
	 */
    public function ldapEscape($subject): string
    {
        return LdapGroupRepository::ldapEscape($subject);
    }

	/**
	 * @param $subject
	 *
	 * @return string
	 */
    public function googleAppsEscape($subject) : string
    {
        return LdapGroupRepository::ldapEscape($subject);
    }

	public function generateStudentPasswordForProvider(Provider $provider) : string
	{
		if($provider->isStudentLdapPasswordOverrideOptions()) {
			return $this->passwordGenerator->getRandomPassword(
				$provider->isStudentLdapPasswordComplexity(),
				$provider->isStudentLdapPasswordUseDictionary(),
				$provider->getStudentLdapPasswordMinChar(),
				(string)$provider->getStudentLdapPasswordPrefix()
			);
		} else {
			return $this->passwordGenerator->getRandomPassword(
				$this->configurationManager->getActiveDirectoryPasswordComplexity(),
				$this->configurationManager->getActiveDirectoryPasswordUseDictionary(),
				$this->configurationManager->getActiveDirectoryPasswordMinChar(),
				(string)$this->configurationManager->getActiveDirectoryPasswordPrefix()
			);
		}

	}

	public function generateTeacherPasswordForProvider(Provider $provider) : string
	{
		if($provider->isTeacherLdapPasswordOverrideOptions()) {
			return $this->passwordGenerator->getRandomPassword(
				$provider->isTeacherLdapPasswordComplexity(),
				$provider->isTeacherLdapPasswordUseDictionary(),
				$provider->getTeacherLdapPasswordMinChar(),
				(string)$provider->getTeacherLdapPasswordPrefix()
			);
		} else {
			return $this->passwordGenerator->getRandomPassword(
				$this->configurationManager->getActiveDirectoryPasswordComplexity(),
				$this->configurationManager->getActiveDirectoryPasswordUseDictionary(),
				$this->configurationManager->getActiveDirectoryPasswordMinChar(),
				(string)$this->configurationManager->getActiveDirectoryPasswordPrefix()
			);
		}

	}
}
