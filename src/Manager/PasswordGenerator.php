<?php

namespace App\Manager;

class PasswordGenerator
{
    public function getRandomPassword(bool $complexity, bool $useDictonary, $minLenght, string $prefix = ""): string
    {
        $charsNumbers = '0123456789';
        $charsLettersLower = 'abcdefghijkmnpqrstuvwxyz';
        $charsLettersUpper = 'ABCDEFGHJKLMNPQRSTUVWXYZ';
        $charsSimbols = '+-=*!?';

		if(!$useDictonary && $minLenght < 1) {
			$minLenght = 8;
		}

        if (false == $complexity and true == $useDictonary) {
            $dizionario = $this->getDizionario();
            shuffle($dizionario);

            if (strlen($prefix.$dizionario[0]) > $minLenght) {
                return $prefix.strtolower($dizionario[0]);
            }

            return $prefix.strtolower($dizionario[0].$dizionario[1]);
        } else if (true == $complexity and false == $useDictonary) {
            $size = (int) (($minLenght - 1 - strlen($prefix)) / 3);
            $simbol = substr(str_shuffle($charsSimbols), 0, 1);
            $lower = substr(str_shuffle($charsLettersLower), 0, $size);
            $upper = substr(str_shuffle($charsLettersUpper), 0, $size);
            $number = substr(str_shuffle($charsNumbers), 0, $minLenght - $size - $size - 1 - strlen($prefix));

            return $prefix.str_shuffle($simbol.$lower.$upper.$number);
        } else if (true == $complexity and true == $useDictonary) {
            $dizionario = $this->getDizionario();
            shuffle($dizionario);

            if (strlen($prefix.$dizionario[0]) > $minLenght) {
                return $prefix.$dizionario[0].substr(str_shuffle($charsSimbols), 0, 1);
            }

            return $prefix.$dizionario[0].substr(str_shuffle($charsSimbols), 0, 1).$dizionario[1];
        } else {
	        return $prefix.substr(str_shuffle($charsNumbers), 0, $minLenght - strlen($prefix));
        }
    }

    private function getDizionario()
    {
        return array_unique(['Iglesias', 'Massa', 'Carrara', 'Olbia', 'Tempio', 'Verano', 'Cusio', 'Ossola', 'Barletta', 'Andria', 'Trani', 'Agrigento', 'Alessandria', 'Ancona', 'Aosta', 'Arezzo', 'Ascoli', 'Asti', 'Avellino', 'Bari', 'Barletta', 'Belluno', 'Benevento', 'Bergamo', 'Biella', 'Bologna', 'Bolzano', 'Brescia', 'Brindisi', 'Cagliari', 'Caltanissetta', 'Campobasso', 'Carbonia', 'Caserta', 'Catania', 'Catanzaro', 'Chieti', 'Como', 'Cosenza', 'Cremona', 'Crotone', 'Cuneo', 'Enna', 'Fermo', 'Ferrara', 'Firenze', 'Foggia', 'Cesena', 'Frosinone', 'Genova', 'Gorizia', 'Grosseto', 'Imperia', 'Isernia', 'Latina', 'Lecce', 'Lecco', 'Livorno', 'Lodi', 'Lucca', 'Macerata', 'Mantova', 'Matera', 'Messina', 'Milano', 'Modena', 'Monza', 'Napoli', 'Novara', 'Nuoro', 'Olbia', 'Oristano', 'Padova', 'Palermo', 'Parma', 'Pavia', 'Perugia', 'Pesaro', 'Urbino', 'Pescara', 'Piacenza', 'Pisa', 'Pistoia', 'Pordenone', 'Potenza', 'Prato', 'Ragusa', 'Ravenna', 'Reggio', 'Rieti', 'Rimini', 'Roma', 'Rovigo', 'Salerno', 'Sassari', 'Savona', 'Siena', 'Siracusa', 'Sondrio', 'Taranto', 'Teramo', 'Terni', 'Torino', 'Ogliastra', 'Trapani', 'Trento', 'Treviso', 'Trieste', 'Udine', 'Varese', 'Venezia', 'Verbano', 'Vercelli', 'Verona', 'Vicenza', 'Viterbo']);
    }
}
