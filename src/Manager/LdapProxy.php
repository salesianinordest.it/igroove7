<?php

namespace App\Manager;

use App\Entity\LdapGroup;
use App\Entity\LdapUser;
use App\Exception\LoggedException;
use Doctrine\ORM\EntityManager;
use Doctrine\ORM\EntityManagerInterface;
use Psr\Log\LoggerInterface;

class LdapProxy
{
    private $ldapProxy;
    private $logger;
    protected $em;
    protected $configurationManager;
    protected $groupRepository;
    protected $studentRepository;
    protected $ldapGroupRepository;
    protected $ldapUserRepository;
    protected $connected = false;

    /**
     * LdapProxy constructor.
     *
     * @param EntityManager $em
     */
    public function __construct(EntityManagerInterface $em, ConfigurationManager $configurationManager, MicrosoftLdapService $MicrosoftLdapService, LoggerInterface $logger)
    {
        $this->em = $em;
        $this->logger = $logger;
        $this->configurationManager = $configurationManager;
        $this->ldapProxy = $MicrosoftLdapService;
        $this->ldapProxy->setParameters($configurationManager->getActiveDirectoryConfiguration());
        $this->groupRepository = $this->em->getRepository('App\Entity\Group');
        $this->studentRepository = $this->em->getRepository('App\Entity\Student');
        $this->ldapGroupRepository = $this->em->getRepository('App\Entity\LdapGroup');
        $this->ldapUserRepository = $this->em->getRepository('App\Entity\LdapUser');
    }

    public function connect()
    {
        if (!$this->isConnected()) {
            $this->ldapProxy->connect();
            $this->connected = true;
            $this->logger->debug('LDAP: Connected  ');
        }
    }

    public function isConnected()
    {
        return $this->connected;
    }

    /**
     * Clear all the LdapUser and LdapGroup from database,
     * and regenerate it from AD.
     */
    public function purgeAndPopulateAll()
    {
        $this->connect();

        $this->logger->info('LDAPtoDB: started');
        $this->refreshConnection();
        $ldapGroups = $this->ldapGroupRepository->findAll();
        foreach ($ldapGroups as $ldapGroup) {
            $this->em->remove($ldapGroup);
        }
        $ldapUsers = $this->ldapUserRepository->findAll();
        foreach ($ldapUsers as $ldapUser) {
            $this->em->remove($ldapUser);
        }

        $this->logger->info('LDAPtoDB: database flushed');

        $this->em->flush();

        $this->populateGroups();
        $this->populateUsers();
        $this->populateMembership();
        $this->logger->info('LDAPtoDB: ended');
    }

    /**
     * Recreate all the LdapGroup reading the current groups in AD.
     */
    protected function populateGroups()
    {
        $this->connect();

        $groups = $this->ldapProxy->getAllGroups();
        if ($groups) {
            foreach ($groups as $group) {
                $ldapGroup = new LdapGroup();
                $ldapGroup->setName($group);
                $this->em->persist($ldapGroup);
            }

            try {
                $this->em->flush();
            } catch (\Throwable $e) {
                $this->logger->error('LDAPtoDB-populateGroups: Error during import of users: '.$e->getMessage());
            }
        }

        $this->logger->info('LDAPtoDB: group database populated with '.count($groups).' entry');
    }

    /**
     * Recreate all the LdapUser reading the current users created by iGroove in AD.
     */
    protected function populateUsers()
    {
        $this->connect();

        $users = $this->ldapProxy->getAllUsers();
        if ($users) {
            foreach ($users as $user) {
                $ldapUser = new LdapUser();
                $ldapUser->setDn($user['dn']);
                $ldapUser->setUsername($user['username']);
                $ldapUser->setFirstname($user['firstname']);
                $ldapUser->setLastname($user['lastname']);
                $ldapUser->setDistinguishedId($user['distinguishedId']);
                $ldapUser->setAttributes(json_encode($user['parameters']));
                $this->em->persist($ldapUser);
            }

            try {
                $this->em->flush();
            } catch (\Throwable $e) {
                $this->logger->error('LDAPtoDB-populateUsers: Error during import of users: '.$e->getMessage());
            }
        }

        $this->logger->info('LDAPtoDB: user database populated with '.count($users).' entry');
    }

    /**
     * Reinsert all the member/group of a LdapGroup present in AD.
     */
    protected function populateMembership()
    {
        $this->connect();

        $ldapUsers = $this->ldapUserRepository->findAll();

        foreach ($ldapUsers as $ldapUser) {
            $groups = $this->ldapProxy->getUserMembership($ldapUser->getUsername());

            if (is_array($groups)) {
                foreach ($groups as $group) {
                    $ldapGroup = $this->ldapGroupRepository->find($group);
                    if ($ldapGroup) {
                        $ldapGroup->addMember('user', $ldapUser->getUsername());
                        $ldapGroup->setOperation(null);
                    }
                }
            }
        }
        $this->logger->info('LDAPtoDB: user membership updated');

        $ldapGroups = $this->ldapGroupRepository->findAll();
        foreach ($ldapGroups as $ldapGroup) {
            $groups = $this->ldapProxy->getGroupMembership($ldapGroup->getName());
            foreach ($groups as $groupName) {
                $ldapGroup->addMember('group', $groupName);
                $ldapGroup->setOperation(null);
            }
        }

        try {
            $this->em->flush();
        } catch (\Throwable $e) {
            $this->logger->error('LDAPtoDB-populateMembership: Error during import of users: '.$e->getMessage());
        }

        $this->logger->info('LDAPtoDB: group membership updated');
    }

    /**
     * Execute all the operation of the LdapUsers an LdapGroups to AD.
     */
    public function syncLDAPfromDB()
    {
        $this->logger->info('DBtoLDAP: started');
        $this->em->clear();
        $this->syncLDAPfromDB_groups();
        $this->syncLDAPfromDB_users();
        $this->syncLDAPfromDB_groups_membership();
        $this->logger->info('DBtoLDAP: ended');
    }

    /**
     * Execute all the operation of the LdapGroups to AD.
     */
    protected function syncLDAPfromDB_groups()
    {
        $ldapGroups = $this->ldapGroupRepository->getAllWithOperation();
        foreach ($ldapGroups as $ldapGroup) {
            try {
                $this->syncLdapGroupWithDBGroup($ldapGroup);
            } catch (\Exception $e) {
            }
        }
        $this->em->flush();
    }

    /**
     * Execute the operation on an LdapGroup to AD.
     *
     * @throws \Exception
     */
    public function syncLdapGroupWithDBGroup(LdapGroup $ldapGroup)
    {
        $this->connect();

        if ('CREATE' == $ldapGroup->getOperation()) {
            $this->logger->info('DBtoLDAP: creating group '.$ldapGroup->getName());

            try {
                $this->ldapProxy->createGroup($ldapGroup->getName());
                $ldapGroup->setOperation('MEMBERS CHANGED');
            } catch (\Throwable $e) {
                $ldapGroup->setOperation('ERROR');
                $this->logger->error('[DBtoLDAP-group-create] '.$e->getMessage());
                throw new LoggedException($e->getMessage(), $e->getCode(), $e);
            } catch (\Exception $e) {
                $ldapGroup->setOperation('ERROR');
                $this->logger->error('[DBtoLDAP-group-create] '.$e->getMessage());
                throw $e;
            }
        } elseif ('RENAME:' == substr($ldapGroup->getOperation(), 0, 7) && strlen($ldapGroup->getOperation()) > 8) {
            $newName = substr($ldapGroup->getOperation(), 7);
            $this->logger->info('DBtoLDAP: renaming group '.$ldapGroup->getName()." to {$newName}");

            try {
                $this->ldapProxy->renameGroup($ldapGroup->getName(), $newName);
                $ldapGroup->setName($newName);
                $ldapGroup->setOperation('');
            } catch (\Throwable $e) {
                $ldapGroup->setOperation('ERROR');
                $this->logger->error('[DBtoLDAP-group-rename] '.$e->getMessage());
                throw new LoggedException($e->getMessage(), $e->getCode(), $e);
            } catch (\Exception $e) {
                $ldapGroup->setOperation('ERROR');
                $this->logger->error('[DBtoLDAP-group-rename] '.$e->getMessage());
                throw $e;
            }
        } elseif ('REMOVE' == $ldapGroup->getOperation()) {
            $this->logger->info('DBtoLDAP: removing group '.$ldapGroup->getName());

            try {
                $this->ldapProxy->removeGroup($ldapGroup->getName());
                $this->em->remove($ldapGroup);
            } catch (\Throwable $e) {
                $ldapGroup->setOperation('ERROR');
                $this->logger->error('[DBtoLDAP-group-remove] '.$e->getMessage());
                throw new LoggedException($e->getMessage(), $e->getCode(), $e);
            } catch (\Exception $e) {
                $ldapGroup->setOperation('ERROR');
                $this->logger->error('[DBtoLDAP-group-remove] '.$e->getMessage());
                throw $e;
            }
        }
    }

    /**
     * Execute all the operation of the LdapUsers to AD.
     */
    protected function syncLDAPfromDB_users()
    {
        $ldapUsers = $this->ldapUserRepository->getAllWithOperation();
        foreach ($ldapUsers as $ldapUser) {
            try {
                $this->syncLdapUserWithDBUser($ldapUser);
                $this->em->flush();
            } catch (\Exception $e) {
            }
        }
        $this->em->flush();
    }

    /**
     * Execute the operation on an LdapUser to AD.
     *
     * @throws \Exception
     */
    public function syncLdapUserWithDBUser(LdapUser $ldapUser)
    {
        if (('' == $ldapUser->getOperation()) or ('ERROR' == $ldapUser->getOperation())) {
            return;
        }

        $this->connect();

        if ('CREATE' == $ldapUser->getOperation()) {
            $this->logger->info('DBtoLDAP: creating user '.$ldapUser->getUsername());

            try {
                $this->ldapProxy->createUser($ldapUser);
                $ldapUser->setOperation('');
                $objectGUID = $this->ldapProxy->getObjectGUIDFromUsername($ldapUser->getUsername());
                if ('' != $objectGUID) {
                    $ldapUser->setAttribute('objectGUID', $objectGUID);
                }
            } catch (\Exception $e) {
                $ldapUser->setOperation('ERROR');
	            $this->em->flush();
                $this->logger->error('[DBtoLDAP-user-create] '.$e->getMessage());
                throw $e;
            }
        } elseif ('MODIFY' == $ldapUser->getOperation()) {
            $this->logger->info('DBtoLDAP: modifying user '.$ldapUser->getUsername());

            try {
                $this->ldapProxy->modifyUser($ldapUser);
                $ldapUser->setOperation('');
            } catch (\Exception $e) {
                $ldapUser->setOperation('ERROR');
	            $this->em->flush();
                $this->logger->error('[DBtoLDAP-user-modify] '.$e->getMessage());
                throw $e;
            }
        } elseif ('REMOVE' == $ldapUser->getOperation()) {
            $this->logger->info('DBtoLDAP: removing user '.$ldapUser->getUsername());

            try {
                $this->ldapProxy->removeUser($ldapUser->getUsername());
                $this->em->remove($ldapUser);
            } catch (\Exception $e) {
                $ldapUser->setOperation('ERROR');
	            $this->em->flush();
                $this->logger->error('[DBtoLDAP-user-remove] '.$e->getMessage());
                throw $e;
            }
        }
    }

    /**
     * Syncronize all the membership of the LdapGroups to Ad.
     */
    protected function syncLDAPfromDB_groups_membership()
    {
        $ldapGroups = $this->ldapGroupRepository->getAllWithOperation();
        foreach ($ldapGroups as $ldapGroup) {
            try {
                $this->syncLdapGroupMembershipWithDB($ldapGroup);
            } catch (\Exception $e) {
            }
        }
        $this->em->flush();
    }

    /**
     * Syncronize the membership of an LdapGroup to AD.
     *
     * @throws \Exception
     */
    public function syncLdapGroupMembershipWithDB(LdapGroup $ldapGroup)
    {
        if ('MEMBERS CHANGED' != $ldapGroup->getOperation()) {
            return;
        }
        $this->connect();
        $name = $ldapGroup->getName();
        $memberList = $ldapGroup->getMembersList();
        $this->logger->info('DBtoLDAP: updating member of group '.$name);

        $errors = [];
        if (isset($memberList['user'])) {
            try {
                $this->ldapProxy->updateUsersIntoGroup($name, $memberList['user']);
            } catch (\Throwable $e) {
                $this->logger->error('[DBtoLDAP-group-users] '.$e->getMessage());
                $errors[] = $e->getMessage();
            } catch (\Exception $e) {
                $this->logger->error('[DBtoLDAP-group-users] '.$e->getMessage());
                $errors[] = $e->getMessage();
            }
        }

        if (isset($memberList['group'])) {
            try {
                $this->ldapProxy->updateGroupsIntoGroup($name, $memberList['group']);
            } catch (\Throwable $e) {
                $this->logger->error('[DBtoLDAP-group-groups] '.$e->getMessage());
                $errors[] = $e->getMessage();
            } catch (\Exception $e) {
                $this->logger->error('[DBtoLDAP-group-groups] '.$e->getMessage());
                $errors[] = $e->getMessage();
            }
        }

        if (!empty($errors)) {
            $ldapGroup->setOperation('ERROR');
            $this->logger->error('[DBtoLDAP-group-groups] '.implode(' - ', $errors));
            throw new LoggedException(implode(" \n", $errors));
        } else {
            $ldapGroup->setOperation('');
        }
    }

    /**
     * Synchronize the member of the InternetAccess group with the Ldap and set as activated the members.
     *
     * @throws \Exception
     */
    public function syncInternetAccessLdapGroup()
    {
        $internetAccessGroupName = $this->configurationManager->getActiveDirectoryGeneratedGroupPrefix().'InternetAccess';
        $internetLdapGroup = $this->em->getRepository('App\Entity\LdapGroup')->find($internetAccessGroupName);

        $personalDeviceAccessGroupName = $this->configurationManager->getActiveDirectoryGeneratedGroupPrefix().'PersonalDeviceAccess';
        $personalDeviceLdapGroup = $this->em->getRepository('App\Entity\LdapGroup')->find($personalDeviceAccessGroupName);

        if (!$internetLdapGroup instanceof LdapGroup) {
            $this->logger->error("[DBtoLDAP-syncInternetAccess] Invalid group {$internetAccessGroupName}");
            throw new LoggedException("Invalid group {$internetAccessGroupName}");
        }

        if (!$personalDeviceLdapGroup instanceof LdapGroup) {
            $this->logger->error("[DBtoLDAP-syncInternetAccess] Invalid group {$personalDeviceAccessGroupName}");
            throw new LoggedException("Invalid group {$personalDeviceAccessGroupName}");
        }

        $this->syncLdapGroupMembershipWithDB($internetLdapGroup);
        $this->syncLdapGroupMembershipWithDB($personalDeviceLdapGroup);

        $internetOpensToClose = $this->em->getRepository('App\Entity\InternetOpen')->findBy(['activationCompleated' => false, 'type' => ['user', 'group']]);
        foreach ($internetOpensToClose as $internetOpen) {
            $internetOpen->setActivationCompleated(true);
        }
        $this->em->flush();
    }

    /**
     * Create the specified OU, if not exists.
     *
     * @param string $ouName
     * @param string $path
     */
    public function createOuIfNotExists($ouName, $path = '')
    {
        if ($this->ldapProxy->ouExists($ouName, $path)) {
            return;
        }

        $this->ldapProxy->createOu($ouName, $path);
    }

	/**
	 * Move a username into an OU.
	 *
	 * @param string $ouName
	 * @param string $username
	 * @param string $path
	 *
	 * @throws LoggedException
	 */
    public function moveUserIntoOu($ouName, $username, $path = '')
    {
        try {
	        $this->ldapProxy->moveUserIntoOu($ouName, $username, $path);
			$this->logger->info("[ldap-moveUserIntoOu] moved user {$username} into ou {$path} {$ouName}");
        } catch (\Exception $e) {
	        $this->logger->error("[ldap-moveUserIntoOu] ".$e->getMessage());
	        throw new LoggedException($e->getMessage());
        }
    }

	/**
	 * Move a group into an OU.
	 *
	 * @param string $ouName
	 * @param string $groupName
	 * @param string $path
	 *
	 * @throws LoggedException
	 */
    public function moveGroupIntoOu($ouName, $groupName, $path = '')
    {
        try {
			$this->ldapProxy->moveGroupIntoOu($ouName, $groupName, $path);
	        $this->logger->info("[ldap-moveGroupIntoOu] moved group {$groupName} into ou {$path} {$ouName}");
        } catch (\Exception $e) {
	        $this->logger->error("[ldap-moveGroupIntoOu] ".$e->getMessage());
	        throw new LoggedException($e->getMessage());
        }
    }

    /**
     * Update the OU position of the specified users.
     *
     * @param string $ouName
     * @param array  $users
     * @param string $path
     */
    public function updateUsersIntoOu($ouName, $users, $path = '')
    {
        $this->ldapProxy->updateUsersIntoOu($ouName, $users, $path);
    }

    /**
     * Update the OU position of the specified groups.
     *
     * @param string $ouName
     * @param array  $groups
     * @param string $path
     */
    public function updateGroupsIntoOu($ouName, $groups, $path = '')
    {
        $this->ldapProxy->updateGroupsIntoOu($ouName, $groups, $path);
    }

    /**
     * Refresh an opened connection.
     */
    public function refreshConnection()
    {
        $this->ldapProxy->refreshConnection();
    }
}
