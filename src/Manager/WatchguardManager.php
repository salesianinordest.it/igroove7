<?php

namespace App\Manager;

use App\Entity\Fortigate;
use App\Entity\Watchguard;
use Doctrine\ORM\EntityManager;
use Doctrine\ORM\EntityManagerInterface;
use Psr\Log\LoggerInterface;

class WatchguardManager
{
    protected $internetAccessUsers;

    /**
     * @var LoggerInterface
     */
    private $logger;

    /**
     * @var EntityManager
     */
    protected $em;

    protected $watchguardes;

    protected string $cacheDir;

    protected $ch;
    protected string $cp_csrf_token;

    public function __construct(string $cacheDir, ConfigurationManager $configurationManager, EntityManagerInterface $em, LoggerInterface $logger)
    {
        $this->em = $em;
        $this->cacheDir = $cacheDir;
        $this->configurationManager = $configurationManager;
        $this->logger = $logger;
    }

    /**
     * kick off the users not currently active or in active group.
     */
    public function KickOffUsers()
    {
        $this->checkWatchguardes();
        $this->internetAccessUsers = $groupMembersUsers = $this->em->getRepository('App\Entity\LdapGroup')->getAllChildrenRecursiveUsers($this->configurationManager->getActiveDirectoryGeneratedGroupPrefix() . 'InternetAccess');
        $userList = [];
        foreach ($groupMembersUsers as $user) {
            $userList[] = strtolower($user);
        }

        $igrooveUsers = $this->em->getRepository('App\Entity\Student')->getAllStudentsUsername();

        foreach ($this->watchguardes as $watchguard) {
            if (false == $watchguard->getStatus()) {
                continue;
            }
            $usersIntoWatchguard = $this->getUsersInHotspot($watchguard);
            $usernameToRemove = array_diff(
                array_unique(array_keys($usersIntoWatchguard)),
                array_unique($userList)
            );




            foreach ($usernameToRemove as $user) {
                if (0 == strlen(trim($user))) {
                    continue;
                }
                if (!array_search($user, $igrooveUsers)) {
                    continue;
                }
                if (array_key_exists(strtolower($user), $usersIntoWatchguard)) {
                    $this->deAuth($watchguard, $usersIntoWatchguard[strtolower($user)]);
                    echo "\r\n --> Watchguard kickoff " . $user;
                    $this->logger->info("Watchguard kickoff $user");
                }
            }
        }
    }




    protected function checkWatchguardes()
    {
        $this->watchguardes = $this->em->getRepository('App\Entity\Watchguard')->findAll();
        $client = new \GuzzleHttp\Client();

        foreach ($this->watchguardes as $wg) {
            $url = 'https://' . $wg->getHost() . ':' . $wg->getPort() ;
            $wg->setStatus(false);
            try {
                $response = $client->request('GET', $url, ['verify' => false]);
                if ('200' == $response->getStatusCode()) {
                    $wg->setStatus(true);
                }
            } catch (\Exception $e) {
                $this->logger->error("[watchguard] Error connecting to fortigate {$wg->getHost()}: " . $e->getMessage(), ['error' => $e]);
            }
            $this->em->flush();
        }
    }







    private function login(Watchguard $watchguard)
    {

        $cookieFile =$this->cacheDir. "/".$watchguard->getHost().'_cookies.txt';
        if(!file_exists($cookieFile)) {
            $fh = fopen($cookieFile, "w");
            fwrite($fh, "");
            fclose($fh);
        }
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_COOKIEFILE, $cookieFile); // Cookie aware
        curl_setopt($ch, CURLOPT_COOKIEJAR, $cookieFile); // Cookie aware
        curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, false);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
        curl_setopt($ch, CURLOPT_HEADER, 1);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_URL, 'https://'.$watchguard->getHost().':'.$watchguard->getPort().'/auth/login?from_page=/');
        $response = curl_exec($ch);
        $raw=substr($response,strpos($response,'name="cp_csrf_token"'));
        $raw=substr($raw,strpos($raw,'value="')+7);
        $cp_csrf_token=substr($raw,0,strpos($raw,'"'));

        curl_setopt($ch, CURLOPT_URL, 'https://'.$watchguard->getHost().':'.$watchguard->getPort().'/agent/login');
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_CUSTOMREQUEST, 'POST');
        curl_setopt($ch, CURLOPT_HTTPHEADER, [
            'Accept: application/xml, text/xml, */*; q=0.01',
            'Accept-Language: it-IT,it;q=0.9,en-US;q=0.8,en;q=0.7',
            'Cache-Control: no-cache',
            'Connection: keep-alive',
            'Content-Type: text/xml',
            'DNT: 1',
            'Origin: https://'.$watchguard->getHost().':'.$watchguard->getPort(),
            'Pragma: no-cache',
            'Referer: https://'.$watchguard->getHost().':'.$watchguard->getPort().'/auth/login?from_page=/',
            'Sec-Fetch-Dest: empty',
            'Sec-Fetch-Mode: cors',
            'Sec-Fetch-Site: same-origin',
            'User-Agent: Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/128.0.0.0 Safari/537.36',
            'X-Requested-With: XMLHttpRequest',
            'sec-ch-ua: "Chromium";v="128", "Not;A=Brand";v="24", "Google Chrome";v="128"',
            'sec-ch-ua-mobile: ?0',
            'sec-ch-ua-platform: "macOS"',
        ]);
        curl_setopt($ch, CURLOPT_POSTFIELDS, '<methodCall><methodName>login</methodName><params><param><value><struct><member><name>password</name><value><string>1qaz-2wsx</string></value></member><member><name>user</name><value><string>admin</string></value></member><member><name>domain</name><value><string>Firebox-DB</string></value></member><member><name>uitype</name><value><string>2</string></value></member></struct></value></param></params></methodCall>');
        curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, false);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);

        $response = curl_exec($ch);

        $raw=substr($response,strpos($response,'<member><name>sid</name><value>')+31);
        $sid=substr($raw,0,strpos($raw,'</value>'));


        $raw=substr($response,strpos($response,'<member><name>csrf_token</name><value>')+38);
        $wga_csrf_token=substr($raw,0,strpos($raw,'</value>'));

        curl_setopt($ch, CURLOPT_URL, 'https://'.$watchguard->getHost().':'.$watchguard->getPort().'/auth/login');
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_CUSTOMREQUEST, 'POST');
        curl_setopt($ch, CURLOPT_HTTPHEADER, [
            'Accept: text/html,application/xhtml+xml,application/xml;q=0.9,image/avif,image/webp,image/apng,*/*;q=0.8,application/signed-exchange;v=b3;q=0.7',
            'Accept-Language: it-IT,it;q=0.9,en-US;q=0.8,en;q=0.7',
            'Cache-Control: no-cache',
            'Connection: keep-alive',
            'Content-Type: application/x-www-form-urlencoded',
            'DNT: 1',
            'Origin: https://'.$watchguard->getHost().':'.$watchguard->getPort(),
            'Pragma: no-cache',
            'Referer: https://'.$watchguard->getHost().':'.$watchguard->getPort().'/auth/login',
            'Sec-Fetch-Dest: document',
            'Sec-Fetch-Mode: navigate',
            'Sec-Fetch-Site: same-origin',
            'Sec-Fetch-User: ?1',
            'Upgrade-Insecure-Requests: 1',
            'User-Agent: Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/128.0.0.0 Safari/537.36',
            'sec-ch-ua: "Chromium";v="128", "Not;A=Brand";v="24", "Google Chrome";v="128"',
            'sec-ch-ua-mobile: ?0',
            'sec-ch-ua-platform: "macOS"',
        ]);
        curl_setopt($ch, CURLOPT_POSTFIELDS, "username=".$watchguard->getUsername()."&password=".$watchguard->getPassword()."&domain=Firebox-DB&sid=$sid&wga_csrf_token=$wga_csrf_token&cp_csrf_token=$cp_csrf_token&privilege=2&from_page=%2F");
        curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, false);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);

        $response = curl_exec($ch);



    }

    /**
     * Get Current active users in hotspot of watchguard with specified ip.
     *
     * @return array
     */
    private function getUsersInHotspot(Watchguard $watchguard){
        $this->login($watchguard);
        $cookieFile =$this->cacheDir. "/".$watchguard->getHost().'_cookies.txt';
        if(!file_exists($cookieFile)) {
            $fh = fopen($cookieFile, "w");
            fwrite($fh, "");
            fclose($fh);
        }
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_COOKIEFILE, $cookieFile); // Cookie aware
        curl_setopt($ch, CURLOPT_COOKIEJAR, $cookieFile); // Cookie aware
        curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, false);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
        curl_setopt($ch, CURLOPT_HEADER, 1);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_URL, 'https://'.$watchguard->getHost().':'.$watchguard->getPort().'/dashboard/dboard_get_system?report=auth');
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_CUSTOMREQUEST, 'GET');
        curl_setopt($ch, CURLOPT_HTTPHEADER, [
            'Accept: application/json, text/javascript, */*; q=0.01',
            'Accept-Language: it-IT,it;q=0.9,en-US;q=0.8,en;q=0.7',
            'Cache-Control: no-cache',
            'Connection: keep-alive',
            'DNT: 1',
            'Pragma: no-cache',
            'Referer: https://'.$watchguard->getHost().':'.$watchguard->getPort().'/dashboard/system?report=auth',
            'Sec-Fetch-Dest: empty',
            'Sec-Fetch-Mode: cors',
            'Sec-Fetch-Site: same-origin',
            'User-Agent: Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/128.0.0.0 Safari/537.36',
            'X-Requested-With: XMLHttpRequest',
            'sec-ch-ua: "Chromium";v="128", "Not;A=Brand";v="24", "Google Chrome";v="128"',
            'sec-ch-ua-mobile: ?0',
            'sec-ch-ua-platform: "macOS"',
        ]);
        curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, false);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);



        $response = curl_exec($ch);

        curl_close($ch);
        $raw=substr($response,strpos($response,'"list":')+7);
        $raw=trim(substr($raw,0,strpos($raw,', "__class__": "PageStatusSystemObj')));
        $users=json_decode($raw,true);
        $list=[];
        foreach($users as $user){
            $list[strtolower($user['user_id'])]=$user;
        }
        return $list;
    }



    private function deAuth(Watchguard $watchguard, array $user)
    {
        $this->login($watchguard);
        $cookieFile =$this->cacheDir. "/".$watchguard->getHost().'_cookies.txt';
        if(!file_exists($cookieFile)) {
            $fh = fopen($cookieFile, "w");
            fwrite($fh, "");
            fclose($fh);
        }
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_COOKIEFILE, $cookieFile); // Cookie aware
        curl_setopt($ch, CURLOPT_COOKIEJAR, $cookieFile); // Cookie aware
        curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, false);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
        curl_setopt($ch, CURLOPT_HEADER, 1);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_URL, 'https://'.$watchguard->getHost().':'.$watchguard->getPort().'/dashboard/dboard_get_system?report=auth&param1='.$user['session_id']);
        curl_setopt($ch, CURLOPT_CUSTOMREQUEST, 'GET');
        curl_setopt($ch, CURLOPT_HTTPHEADER, [
            'Accept: application/json, text/javascript, */*; q=0.01',
            'Accept-Language: it-IT,it;q=0.9,en-US;q=0.8,en;q=0.7',
            'Cache-Control: no-cache',
            'Connection: keep-alive',
            'DNT: 1',
            'Pragma: no-cache',
            'Referer: hhttps://'.$watchguard->getHost().':'.$watchguard->getPort().'/dashboard/system?report=auth',
            'Sec-Fetch-Dest: empty',
            'Sec-Fetch-Mode: cors',
            'Sec-Fetch-Site: same-origin',
            'User-Agent: Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/128.0.0.0 Safari/537.36',
            'X-Requested-With: XMLHttpRequest',
            'sec-ch-ua: "Chromium";v="128", "Not;A=Brand";v="24", "Google Chrome";v="128"',
            'sec-ch-ua-mobile: ?0',
            'sec-ch-ua-platform: "macOS"',
        ]);
        curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, false);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
        $response = curl_exec($ch);

        curl_close($ch);

    }

}
