<?php

namespace App\ImporterFilter;

use App\ImporterFilter\ImportedEntity\Group;
use App\ImporterFilter\ImportedEntity\Student;
use App\ImporterFilter\ImportedEntity\Teacher;
use GuzzleHttp\Client;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\Extension\Core\Type\TextType;

class Gsd extends AbstractFilter
{
    public static $name = 'SEED GSD';
    public static $internalName = 'gsd';
    public static $parametersUi = ['uri' => ['title' => 'URI della fonte dati', 'type' => TextType::class], 'secretKey' => ['title' => 'Chiave Segreta', 'type' => TextType::class], 'considerThese' => ['title' => 'ID Discite degli studenti da considerare', 'type' => TextareaType::class]];
    protected $dataUri;
    protected $secretKeyWithSalt;

    protected $guzzle;
    protected $considerThese;

    public function __construct()
    {
        $this->guzzle = new Client();
    }

    public function setParameters($parameters)
    {
        parent::setParameters($parameters);
        $this->updateDataUri();
        $datas = explode(',', $this->parameters['considerThese']);
        $list = [];
        foreach ($datas as $id) {
            $list[] = trim($id);
        }
        $this->considerThese = $list;
    }

    /*
     * Come concordato, per ottenere i dati occorre passare il parametro
     * k = md5((KEY + SYSDATE))
     * che funge da chiave di autenticazione.
     * Con
     * KEY     = chiave
     * SYSDATE = data attuale nel formato YYYYmmdd     (esempio per oggi 20141127)
     */

    private function updateSalt()
    {
        $this->secretKeyWithSalt = md5($this->parameters['secretKey'].date('Ymd'));
    }

    private function updateDataUri()
    {
        $this->updateSalt();
        $this->dataUri = $this->parameters['uri'].'?k='.$this->secretKeyWithSalt;
    }

    public function parseRemoteData()
    {
        $request = $this->guzzle->get($this->dataUri);
        $response = $request->getBody()->getContents();
        $resultArray = json_decode($response, true);
        if ('success' != $resultArray['status']) {
            return;
        }

        echo "\r\n Importer obsoleto. Mi fermo";
        echo "\r\n Verificare se il codice fiscale viene usato dentro l'importer di gsd";
        echo "\r\n Sembra di no, viene usato solo lo username. In questo caso sarebbe ottimo e posso lasciare attivo il provider gsd (attivo e togliere die dal codice) tanto non influisce.";
        exit;
        $listDn = [];

        foreach ($resultArray['payload'] as $row) {
            $prefisso = '';
            if ('docente' == $row['tipo']) {
                $prefisso = 'DOC';
            }
            if ('studente' == $row['tipo']) {
                $prefisso = 'STU';
            }
            if (0 == strlen($row['username'])) {
                continue;
            }
            if ('studente' == $row['tipo'] && !in_array($row['id'], $this->considerThese)) {
//                echo "\r\nSkip studente con ID discite ".$row['id'];
                continue;
            }

            if (('STU' == strtoupper(substr($row['username'], 0, 3))) or ('DOC' == strtoupper(
                substr($row['username'], 0, 3)
            ))
            ) {
                $username = $row['username'];
            } else {
                $username = $prefisso.$row['username'];
            }

            $i = 3;
            while ('0' == substr($username, $i, 1)) {
                $username = substr($username, 0, 3).substr($username, $i + 1);
            }
            $str = base64_decode($row['password']);

            $key = substr(sha1($this->parameters['secretKey'], true), 0, 16);

            $password = trim(substr($this->getDecrypt($str, $key), 0, strlen(base64_decode($row['password']))));

            $password = preg_replace('/[\x00-\x1F\x80-\xFF]/', '', $password);

            if (0 == strlen(trim($password))) {
                continue;
            }

            $dn = trim(ucwords(strtolower($row['nome']))).' '.trim(ucwords(strtolower($row['cognome'])));

            if (in_array($dn, $listDn)) {
                $row['nome'] = trim(ucwords(strtolower($row['nome']))).' ('.strtoupper($username).')';
                $dn = trim(ucwords(strtolower($row['nome']))).' '.trim(ucwords(strtolower($row['cognome'])));
            }
            $listDn[] = $dn;
            $group = trim(ucwords(strtolower($row['tipo'])));
            $groupId = md5(strtolower($group));
            $this->groups[$groupId] = new Group($groupId, $group, 0);

            $id = strtolower($row['tipo'].'-'.$row['id']);

            if ('docente' == $row['tipo']) {
                $this->teachers[$id] = new Teacher(
                    strtolower($username),
                    strtolower($username),
                    trim(ucwords(strtolower($row['nome']))),
                    trim(ucwords(strtolower($row['cognome']))),
                    trim(strtolower($row['email'])),
                    strtoupper(str_replace('&', '', $username)),
                    $password
                );
            }

            if ('studente' == $row['tipo']) {
                $this->students[$id] = new Student(
                    strtolower($username),
                    strtolower($username),
                    trim(ucwords(strtolower($row['nome']))),
                    trim(ucwords(strtolower($row['cognome']))),
                    $groupId,
                    trim(strtolower($row['email'])),
                    strtoupper(str_replace('&', '', $username)),
                    $password
                );
            }
        }
    }

    private function pkcs5_pad($text, $blocksize)
    {
        $pad = $blocksize - (strlen($text) % $blocksize);

        return $text.str_repeat(chr($pad), $pad);
    }

    private function getDecrypt_old($sStr, $sKey)
    {
        return mcrypt_decrypt(
            MCRYPT_RIJNDAEL_128,
            $sKey,
            $sStr,
            MCRYPT_MODE_ECB
        );
    }

    public function getDecrypt($sStr, $sKey)
    {
        // $text = openssl_decrypt(base64_decode($sStr), 'bf-cbc', $sKey, OPENSSL_RAW_DATA);
        // $encrypted = base64_encode(mcrypt_encrypt(MCRYPT_RIJNDAEL_256, $key, $rs->fields[5], MCRYPT_MODE_CBC, md5(md5($key))));
        // $text = openssl_decrypt(($sStr), 'AES-128-CBC', $sKey, OPENSSL_RAW_DATA);
        // return $text;

        return openssl_decrypt($sStr, 'aes-128-ecb', $sKey, OPENSSL_RAW_DATA);
    }

    /* PEr installare mcrypt

pecl version
  apt-get -y install libmcrypt-dev
  pecl install mcrypt-1.0.1
  sudo bash -c "echo extension=/usr/lib/php/20170718/mcrypt.so > /etc/php/7.2/cli/conf.d/mcrypt.ini"
  sudo bash -c "echo extension=/usr/lib/php/20170718/mcrypt.so > /etc/php/7.2/apache2/conf.d/mcrypt.ini"
  php -i | grep "mcrypt"
    */
}
