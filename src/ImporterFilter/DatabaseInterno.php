<?php

namespace App\ImporterFilter;

class DatabaseInterno extends AbstractFilter
{
    public static $name = 'Database Interno';
    public static $internalName = 'databaseinterno';

    public function setUri($uri)
    {
    }

    public function setSecretKey($key)
    {
    }

    public function parseRemoteData()
    {
    }
}
