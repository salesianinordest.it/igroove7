<?php

namespace App\ImporterFilter;

use App\ImporterFilter\ImportedEntity\Group;
use App\ImporterFilter\ImportedEntity\Sector;
use App\ImporterFilter\ImportedEntity\Student;
use App\ImporterFilter\ImportedEntity\Subject;
use App\ImporterFilter\ImportedEntity\Teacher;

abstract class AbstractFilter
{
    protected $uri;
    public static $name;
    public static $internalName;
    protected $parameters = [];
    public static $parametersUi = [];

    protected $students = [];
    protected $groups = [];
    protected $teachers = [];
    protected $subjects = [];
    protected $sectors = [];
    protected $teacherSubjectGroupRelation = [];

    protected $isManualImport = false;

	protected $invalidStudents = [];
	protected $invalidTeachers = [];
	protected $invalidGroups = [];

    abstract public function parseRemoteData();

    public function setUri($uri)
    {
        $this->uri = $uri;
    }

    public function setParameters($parameters)
    {
        $this->parameters = $parameters;
    }

    /**
     * @return Student[]
     */
    public function getStudents()
    {
        return $this->students;
    }

    /**
     * @return Group[]
     */
    public function getGroups()
    {
        return $this->groups;
    }

    /**
     * @return Teacher[]
     */
    public function getTeachers()
    {
        return $this->teachers;
    }

    /**
     * @return Subject[]
     */
    public function getSubjects()
    {
        return $this->subjects;
    }

    /**
     * @return Sector[]
     */
    public function getSectors()
    {
        return $this->sectors;
    }

    /**
     * @return array
     */
    public function getTeacherSubjectGroupRelation()
    {
        return $this->teacherSubjectGroupRelation;
    }

    /**
     * @return bool
     */
    public function isManualImport()
    {
        return $this->isManualImport;
    }

    /**
     * @param bool $isManualImport
     */
    public function setIsManualImport($isManualImport)
    {
        $this->isManualImport = $isManualImport;
    }

	public function getInvalidEntities() : array
	{
		return [
			'groups' => $this->invalidGroups,
			'students' => $this->invalidStudents,
			'teachers' => $this->invalidTeachers,
		];
	}

	public function getInvalidGroupsById() : array
	{
		$invalidGroups = [];
		foreach ($this->invalidGroups as $invalidGroup) {
			if($invalidGroup['id']) {
				$invalidGroups[$invalidGroup['id']] = $invalidGroup;
			}
		}

		return $invalidGroups;
	}

	public function getInvalidStudentsById() : array
	{
		$invalidStudents = [];
		foreach ($this->invalidStudents as $invalidStudent) {
			if($invalidStudent['id']) {
				$invalidStudents[$invalidStudent['id']] = $invalidStudent;
			}
		}

		return $invalidStudents;
	}

	public function getInvalidTeachersById() : array
	{
		$invalidTeachers = [];
		foreach ($this->invalidTeachers as $invalidTeacher) {
			if($invalidTeacher['id']) {
				$invalidTeachers[$invalidTeacher['id']] = $invalidTeacher;
			}
		}

		return $invalidTeachers;
	}
}
