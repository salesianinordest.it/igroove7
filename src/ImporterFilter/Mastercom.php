<?php

namespace App\ImporterFilter;

use App\ImporterFilter\ImportedEntity\Group;
use App\ImporterFilter\ImportedEntity\Sector;
use App\ImporterFilter\ImportedEntity\Student;
use App\ImporterFilter\ImportedEntity\Subject;
use App\ImporterFilter\ImportedEntity\Teacher;
use Doctrine\Common\Collections\ArrayCollection;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\PasswordType;
use Symfony\Component\Form\Extension\Core\Type\TextType;

class Mastercom extends AbstractFilter
{
    public static $name = 'Mastercom';
    public static $internalName = 'mastercom';
    public static $parametersUi = [
                            'as' => ['title' => 'Inizio Anno Scolastico (20xx)', 'type' => TextType::class],
                            'server_uri' => ['title' => 'Dominio/Ip server (es scuola.registroelettronico.com)', 'type' => TextType::class],
                            'username' => ['title' => 'Username', 'type' => TextType::class],
                            'password' => ['title' => 'Password', 'type' => PasswordType::class],
                            'classroomFormat' => ['title' => 'Formato Nome Classe', 'type' => ChoiceType::class, 'choices' => [
                                'Annualità + Spazio + Sezione (es. 1 A)' => 'annualita sezione',
                                'Annualità + Sezione (es. 1A)' => 'annualitaSezione',
                                'Annualità + Sezione + Settore (es. 1ALIC)' => 'annualitaSezioneSettore',
                                'Annualità + Spzio + Sezione + Settore (es. 1 ALIC)' => 'annualita sezioneSettore',
                                'Annualità + Spzio + Sezione + Spazio + Settore (es. 1 A LIC)' => 'annualita sezione settore',
                                'Annualità + Sezione + Punto + Settore (es. 1A.LIC)' => 'annualitaSezione.settore',
                                'Settore + Annualità + Sezione (es. LIC1A)' => 'settoreAnnualitaSezione',
                                'Settore + Spazio + Annualità + Sezione (es. LIC 1A)' => 'settore annualitaSezione',
                                'Settore + Spazio + Annualità + Spazio + Sezione (es. LIC 1 A)' => 'settore annualita sezione',
                                'Settore + Punto + Settore + Annualità + Sezione (es. LIC.1A)' => 'settore.annualitaSezione',
                            ]],
                            'sectorIds' => ['title' => 'ID dei settori da importare (separati da virgola). Lasciare vuoto per importarli tutti', 'type' => TextType::class],
                            ];

    protected $guzzle = null;

    /**
     * @throws \GuzzleHttp\Exception\GuzzleException
     * @throws \Exception
     */
    public function parseRemoteData()
    {
        $sectorIds = explode(',', $this->parameters['sectorIds']);
        $classroomFormat = $this->parameters['classroomFormat'] ?: 'annualitaSezione';
        $sectorShortNames = [];

        $sectors = $this->callListingApi('school/structure/courses');
        foreach ($sectors as $sector) {
            if (!isset($sector['descrizione']) || !isset($sector['id_indirizzo']) || '' == $sector['descrizione']
                || ('' != $this->parameters['sectorIds'] && !in_array((int) $sector['id_indirizzo'], $sectorIds))) {
                continue;
            }

            if ('annualitaSezione' !== $classroomFormat && 'annualita sezione' !== $classroomFormat && isset($sector['codice'])) {
                $sectorShortNames[(int) $sector['id_indirizzo']] = $sector['codice'];
            }

            $this->sectors[(int) $sector['id_indirizzo']] = new Sector((int) $sector['id_indirizzo'], $sector['descrizione']);
        }
        unset($sectors, $sector);

        $classrooms = $this->callListingApi('school/structure/classes');
        foreach ($classrooms as $classroom) {
			foreach(['id_indirizzo', 'id_classe', 'classe', 'sezione', 'ordinamento'] as $key) {
				if(!isset($classroom[$key]) || '' == trim($classroom[$key])) {
					$this->invalidGroups[] = ['error' => "Campo '{$key}' mancante o non compilato sulla riga: ".json_encode($classroom), 'entityName' => "", 'severity' => 'warning', 'id' => $classroom['id_classe'] ?? null];
					continue 2;
				}
			}

            if ('' != $this->parameters['sectorIds'] && !in_array((int) $classroom['id_indirizzo'], $sectorIds)) {
	            $this->invalidGroups[] = ['error' => "Saltato perché non presente nell'elenco provider id impostato", 'entityName' => "Class '{$classroom['classe']} {$classroom['sezione']}'", 'severity' => 'info', 'id' => $classroom['id_classe']];
                continue;
            }

            // @todo enum con dentro classe ordinaria, classe temporanea o corso
            if ('0' != $classroom['ordinamento'] && '2' != $classroom['ordinamento']) {
	            $this->invalidGroups[] = ['error' => "Saltato per un ordinamento diverso da 0 o 2", 'entityName' => "Class '{$classroom['classe']} {$classroom['sezione']}'", 'severity' => 'info', 'id' => $classroom['id_classe']];
                continue;
            }

            $shortName = $this->getFormattedGroupName($classroom, $classroomFormat, $sectorShortNames);
            $this->groups[(int) $classroom['id_classe']] = new Group((int) $classroom['id_classe'], $shortName, (int) $classroom['id_indirizzo']);
        }
        unset($classrooms, $classroom, $shortName);

        $students = $this->callListingApi('school/structure/students');
        $tempStudents = [];
        foreach ($students as $student) {
	        foreach(['codice_fiscale', 'nome', 'cognome', 'id_studente'] as $key) {
		        if(!isset($student[$key]) || '' == trim($student[$key])) {
			        $this->invalidStudents[] = ['error' => "Campo '{$key}' mancante o non compilato sulla riga: ".json_encode($student), 'entityName' => "", 'severity' => 'warning', 'id' => $student['id_studente'] ?? null];
			        continue 2;
		        }
	        }

	        if(!in_array($student['stato'],[
		        "Qualificato","Qualificato - Giudizio sospeso","Qualificato - Ammesso alla classe successiva","In corso","Giudizio sospeso","Frequenza anno estero",
		        "Ammesso esame di stato","Ammesso esame di qualifica - Giudizio sospeso","Ammesso esame di qualifica - Ammesso alla classe successiva","Ammesso alla classe successiva",
		        "Ammesso al successivo grado dell'istruzione obbligatoria","Esame integrativo","Ammesso alla classe successiva con debito formativo","Ammesso alla classe successiva con carenze"
	        ])) {
		        $this->invalidStudents[] = ['error' => "Saltato per uno 'stato' non valido: '{$student['stato']}'", 'entityName' => "Student {$student['cognome']} {$student['nome']} ({$student['codice_fiscale']})", 'severity' => 'info', 'id' => $student['id_studente']];
		        continue;
	        }

            $tempStudents[(int) $student['id_studente']] = new Student(
                (int) $student['id_studente'],
                trim(strtolower($student['codice_fiscale'])),
                trim(mb_convert_case($student['nome'], MB_CASE_TITLE, 'UTF-8')),
                trim(mb_convert_case($student['cognome'], MB_CASE_TITLE, 'UTF-8')),
                0
            );
        }
        unset($student, $students);

        $studentClassrooms = $this->callListingApi('school/structure/students-pairings');
        if ($studentClassrooms->count() < 1) {
            throw new \Exception('Associazioni studente-classe non restituite dalle API di Mastercom');
        }
        foreach ($studentClassrooms as $studentClassroom) {
            if (!isset($studentClassroom['id_studente']) || !isset($studentClassroom['id_classe']) ||
                !isset($tempStudents[$studentClassroom['id_studente']]) || !$tempStudents[$studentClassroom['id_studente']] instanceof Student) {
                continue;
            }

            if (!isset($this->groups[(int) $studentClassroom['id_classe']]) || !$this->groups[(int) $studentClassroom['id_classe']] instanceof Group) {
                continue;
            }

            if (!isset($this->students[$studentClassroom['id_studente']])) {
                $this->students[$studentClassroom['id_studente']] = $tempStudents[$studentClassroom['id_studente']];
            }

            $this->students[$studentClassroom['id_studente']]->setGroupId((int) $studentClassroom['id_classe']);
        }
        unset($studentClassroom, $studentClassrooms);

		foreach ($tempStudents as $tempStudent) {
			$this->invalidStudents[] = ['error' => "Saltato per classe non valida o nel settore sbagliato", 'entityName' => "Student {$tempStudent->getLastName()} {$tempStudent->getFirstName()} ({$tempStudent->getFiscalCode()})", 'severity' => 'info', 'id' => $tempStudent->getId()];
		}
		unset($tempStudents);

        $teachers = $this->callListingApi('school/structure/teachers');
        foreach ($teachers as $teacher) {
            foreach(['codice_fiscale', 'nome', 'cognome', 'id_professore'] as $key) {
		        if(!isset($teacher[$key]) || '' == trim($teacher[$key])) {
			        $this->invalidTeachers[] = ['error' => "Campo '{$key}' mancante o non compilato sulla riga: ".json_encode($teacher), 'entityName' => "", 'severity' => 'warning', 'id' => $teacher['id_professore'] ?? null];
			        continue 2;
		        }
	        }

            $this->teachers[(int) $teacher['id_professore']] = new Teacher(
                (int) $teacher['id_professore'],
                trim(strtolower($teacher['codice_fiscale'])),
                trim(mb_convert_case($teacher['nome'], MB_CASE_TITLE, 'UTF-8')),
                trim(mb_convert_case($teacher['cognome'], MB_CASE_TITLE, 'UTF-8'))
            );
        }
        unset($teachers, $teacher);

        $usedSubjectsId = [];
        $teacherClassroomsCount = [];
        $teacherClassroomSubjects = $this->callListingApi('school/structure/teachers-pairings');
        foreach ($teacherClassroomSubjects as $teacherClassroomSubject) {
            if (!isset($teacherClassroomSubject['id_professore']) || !isset($teacherClassroomSubject['id_classe']) || !isset($teacherClassroomSubject['id_materia'])) {
                continue;
            }

            if (!isset($this->teachers[(int) $teacherClassroomSubject['id_professore']]) || !isset($this->groups[(int) $teacherClassroomSubject['id_classe']])) {
                continue;
            }

			$k = (int)$teacherClassroomSubject['id_professore']."-".(int)$teacherClassroomSubject['id_classe']."-".(int)$teacherClassroomSubject['id_materia'];
			if(isset($this->teacherSubjectGroupRelation[$k])) {
				continue;
			}

            $this->teacherSubjectGroupRelation[$k] = [
                'teacher_id' => (int) $teacherClassroomSubject['id_professore'],
                'group_id' => (int) $teacherClassroomSubject['id_classe'],
                'subject_id' => (int) $teacherClassroomSubject['id_materia'],
            ];

            if (!isset($teacherClassroomsCount[(int) $teacherClassroomSubject['id_professore']])) {
                $teacherClassroomsCount[(int) $teacherClassroomSubject['id_professore']] = 1;
            } else {
                ++$teacherClassroomsCount[(int) $teacherClassroomSubject['id_professore']];
            }

            if (!in_array($teacherClassroomSubject['id_materia'], $usedSubjectsId)) {
                $usedSubjectsId[] = (int) $teacherClassroomSubject['id_materia'];
            }
        }
        unset($teacherClassroomSubjects, $teacherClassroomSubject);

        $subjects = $this->callListingApi('school/structure/subjects');
        foreach ($subjects as $subject) {
            if (!isset($subject['descrizione']) || !isset($subject['id_materia']) || '' == trim($subject['descrizione']) || !in_array((int) $subject['id_materia'], $usedSubjectsId)) {
                continue;
            }

            if (!isset($subject['codice']) || '' == $subject['codice']) {
                continue;
            }

            $this->subjects[(int) $subject['id_materia']] = new Subject((int) $subject['id_materia'], $subject['descrizione']);
        }

        if (!$this->isManualImport()) {
            foreach ($this->teachers as $id => $teacher) {
                if (!isset($teacherClassroomsCount[$id]) || $teacherClassroomsCount[$id] < 1) {
                    unset($this->teachers[$id]);
                }
            }
        }
    }

    /**
     * Require the authentication token using the username & password credentials, and set the token in the guzzle header.
     *
     * @throws \Exception
     * @throws \GuzzleHttp\Exception\GuzzleException
     */
    protected function getAuthenticatedGuzzle()
    {
        $guzzle = new \GuzzleHttp\Client([
            'base_uri' => "https://{$this->parameters['server_uri']}/next-api/v1/",
//			'exceptions' => false,
            'timeout' => 10,
        ]);
        $response = $guzzle->get('login', [
            'query' => ['username' => $this->parameters['username'] ?? '', 'password' => $this->parameters['password'] ?? ''],
        ]);

        $token = (string) $response->getBody();
        if ('' === $token) {
            throw new \Exception('Invalid auth token from mastercom api: /login');
        }

        $this->authToken = $token;
        $currentConfig = $guzzle->getConfig();
        $currentConfig['headers']['Authorization'] = $token;

        return new \GuzzleHttp\Client($currentConfig);
    }

    /**
     * @return \GuzzleHttp\Client
     *
     * @throws \GuzzleHttp\Exception\GuzzleException
     */
    protected function getGuzzle()
    {
        if (null === $this->guzzle) {
            $this->guzzle = $this->getAuthenticatedGuzzle();
        }

        return $this->guzzle;
    }

    /**
     * @param $uri
     * @param array  $extraParams
     * @param null   $year
     * @param string $tipoEstrazione
     *
     * @return ArrayCollection
     *
     * @throws \GuzzleHttp\Exception\GuzzleException
     */
    protected function callListingApi($uri, $extraParams = [], $year = null, $tipoEstrazione = 'export_base')
    {
        $guzzle = $this->getGuzzle();
        $defaultParams = ['tipo_estrazione' => $tipoEstrazione, 'anno_scolastico' => $this->getFormattedSchoolYear($year)];
        $params = array_merge($defaultParams, $extraParams);
        $response = $guzzle->get($uri, ['query' => $params]);
        $body = (string) $response->getBody();
        $jsonBody = json_decode($body, true);
        if (!is_array($jsonBody)) {
            throw new \Exception('Invalid response from mastercom api: '.$uri);
        }

        return new ArrayCollection($jsonBody);
    }

    /**
     * Format the school year for the api.
     *
     * @param int|null $year
     *
     * @return string
     *
     * @throws \Exception
     */
    protected function getFormattedSchoolYear($year = null)
    {
        if (null === $year) {
            $year = $this->parameters['as'];
        }

        if (null === $year || (int) $year != $year || 0 == $year) {
            throw new \Exception('Invalid year');
        }

        $year = (int) $year;

        return $year.'_'.($year + 1);
    }

    protected function getFormattedGroupName($classroom, $classroomFormat, $sectorShortNames = [])
    {
        $cleanSection = trim(ucwords(strtolower($classroom['sezione'])));
        $sectorName = $sectorShortNames[(int) $classroom['id_indirizzo']] ?? '';
        switch ($classroomFormat) {
            default:
            case 'annualita sezione':
                return $classroom['classe'].' '.$cleanSection;

            case 'annualitaSezione':
                return $classroom['classe'].$cleanSection;

            case 'annualitaSezioneSettore':
                return $classroom['classe'].$cleanSection.$sectorName;

            case 'annualita sezioneSettore':
                return $classroom['classe'].' '.$cleanSection.$sectorName;

            case 'annualita sezione settore':
                return $classroom['classe'].' '.$cleanSection.' '.$sectorName;

            case 'annualitaSezione.settore':
                return $classroom['classe'].$cleanSection.'.'.$sectorName;

            case 'settoreAnnualitaSezione':
                return $sectorName.$classroom['classe'].$cleanSection;

            case 'settore annualitaSezione':
                return $sectorName.' '.$classroom['classe'].$cleanSection;

            case 'settore annualita sezione':
                return $sectorName.' '.$classroom['classe'].' '.$cleanSection;

            case 'settore.annualitaSezione':
                return $sectorName.'.'.$classroom['classe'].$cleanSection;
        }
    }
}
