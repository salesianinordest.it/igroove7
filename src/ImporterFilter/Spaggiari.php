<?php

namespace App\ImporterFilter;

use App\ImporterFilter\ImportedEntity\Group;
use App\ImporterFilter\ImportedEntity\Sector;
use App\ImporterFilter\ImportedEntity\Student;
use App\ImporterFilter\ImportedEntity\Subject;
use App\ImporterFilter\ImportedEntity\Teacher;
use Symfony\Component\Form\Extension\Core\Type\PasswordType;
use Symfony\Component\Form\Extension\Core\Type\TextType;

class Spaggiari extends AbstractFilter
{
    public static $name = 'Spaggiari';
    public static $internalName = 'spaggiari';
    public static $parametersUi = ['guid' => ['title' => 'GUID', 'type' => TextType::class],
                            'as' => ['title' => 'Inizio Anno Scolastico (20xx)', 'type' => TextType::class],
                            'cid' => ['title' => 'Codice Istituto (MISPxxxx)', 'type' => TextType::class],
                            'login' => ['title' => 'Login', 'type' => TextType::class],
                            'password' => ['title' => 'Password', 'type' => PasswordType::class],
                            'excludedMec' => ['title' => 'Codici Meccanografici esclusi (separati da virgola)', 'type' => TextType::class],
                            ];

    public function parseRemoteData()
    {
        $excludedMecs = explode(',', $this->parameters['excludedMec']);
        $datiAlunni = $this->callSpaggiari('wsExtData..downloadDatiAluWeb');

        $usedGroupsId = $usedSectorsId = [];

        foreach ($datiAlunni->studenti as $studente) {
            if ('' == $studente->cod_ministeriale_scuola_curriculum || in_array($studente->cod_ministeriale_scuola_curriculum, $excludedMecs)) {
                continue;
            }

            $this->students[(int) $studente->id_studente] = new Student(
                (int) $studente->id_studente,
                trim(strtolower($studente->codice_fiscale)),
                trim(ucwords(strtolower($studente->nome))),
                trim(ucwords(strtolower($studente->cognome))),
                (int) $studente->id_classe_curriculum ?: 0
            );

            if ((int) $studente->id_classe_curriculum > 0) {
                $usedGroupsId[] = (int) $studente->id_classe_curriculum;
            }
        }

        foreach ($datiAlunni->classi as $classe) {
            if (!in_array((int) $classe->id_classe, $usedGroupsId)) {
                continue;
            }

            if (($pos = strpos($classe->classe_descr, ' ')) !== false) {
                $shortName = substr($classe->classe_descr, 0, $pos);
            } else {
                $shortName = $classe->classe_descr;
            }

            // @todo tenere classi con spazio?
            $shortName = substr($shortName, 0, 1).' '.substr($shortName, 1);
            $shortName = trim(ucwords(strtolower($shortName)));

            $this->groups[(int) $classe->id_classe] = new Group((int) $classe->id_classe, $shortName, (int) $classe->id_settore);

            if ((int) $classe->id_settore > 0) {
                $usedSectorsId[] = (int) $classe->id_settore;
            }
        }

        foreach ($datiAlunni->settori as $settore) {
            if ('' == $settore->settore_descr || !in_array((int) $settore->id_settore, $usedSectorsId)) {
                continue;
            }

            $this->sectors[(int) $settore->id_settore] = new Sector((int) $settore->id_settore, $settore->settore_descr);
        }

        $datiAnagrafe = $this->callSpaggiari('wsExtData..downloadAnagrafeJson', ['special' => 'ACCOUNT-ABBINAMENTI']);

        foreach ($datiAnagrafe->account as $i => $account) {
            if ('' == $account->codice_fisc || ('Docente' != $account->ruolo_codice && 'Dirigente' != $account->ruolo_codice)) { // || in_array($account->sede_mecc, $excludedMecs) || $account->sede_mecc == ""
                continue;
            }

            $this->teachers[(int) $account->id] = new Teacher(
                (int) $account->id,
                trim(strtolower($account->codice_fisc)),
                trim(ucwords(strtolower($account->nome))),
                trim(ucwords(strtolower($account->cognome)))
            );
        }

        $teacherClassroomsCount = [];
//        $teacherAllClassroomsCount = [];
        foreach ($datiAnagrafe->abbinamenti as $abbinamento) {
            if (!isset($this->subjects[(int) $abbinamento->materia_id])) {
                $this->subjects[(int) $abbinamento->materia_id] = new Subject((int) $abbinamento->materia_id, $abbinamento->materia_desc);
            }

//            if(!isset($teacherAllClassroomsCount[(int)$abbinamento->docente_id])) {
//                $teacherAllClassroomsCount[(int)$abbinamento->docente_id] = 1;
//            } else {
//                $teacherAllClassroomsCount[(int)$abbinamento->docente_id]++;
//            }

            if (!isset($this->teachers[(int) $abbinamento->docente_id]) || !isset($this->groups[(int) $abbinamento->classe_id_alw])) {
                continue;
            }

            $this->teacherSubjectGroupRelation[] = [
                'teacher_id' => (int) $abbinamento->docente_id,
                'group_id' => (int) $abbinamento->classe_id_alw,
                'subject_id' => (int) $abbinamento->materia_id,
            ];

            if (!isset($teacherClassroomsCount[(int) $abbinamento->docente_id])) {
                $teacherClassroomsCount[(int) $abbinamento->docente_id] = 1;
            } else {
                ++$teacherClassroomsCount[(int) $abbinamento->docente_id];
            }
        }

        if (!$this->isManualImport()) {
            foreach ($this->teachers as $id => $teacher) {
                if (!isset($teacherClassroomsCount[$id]) || $teacherClassroomsCount[$id] < 1) {
                    unset($this->teachers[$id]);
                }
            }
        }

//        if($this->isManualImport()) {
//            foreach ($this->teachers as $id => $teacher) {
//                if(isset($teacherClassroomsCount[$id]) && isset($teacherAllClassroomsCount[$id]) && $teacherClassroomsCount[$id] < floor($teacherAllClassroomsCount[$id]/2)) {
//                    unset($this->teachers[$id]);
//                }
//            }
//        } else {
//            foreach ($this->teachers as $id => $teacher) {
//                if(!isset($teacherClassroomsCount[$id]) || !isset($teacherAllClassroomsCount[$id]) || $teacherClassroomsCount[$id] <= floor($teacherAllClassroomsCount[$id]/2)) {
//                    unset($this->teachers[$id]);
//                }
//            }
//        }
    }

    protected function callSpaggiari($call, $otherOptions = [])
    {
        if (!isset($this->parameters['guid']) ||
            !isset($this->parameters['as']) ||
            !isset($this->parameters['cid']) ||
            !isset($this->parameters['login']) ||
            !isset($this->parameters['password'])) {
            throw new \ErrorException('Empty connections parameter');
        }

        $soapOptions = [
            'exceptions' => true,
            'trace' => 1,
            'cache_wsdl' => WSDL_CACHE_NONE,
        ];

        $options = [
                'guid' => $this->parameters['guid'],
                'as' => $this->parameters['as'],
                'cid' => $this->parameters['cid'],
                'login' => $this->parameters['login'],
                'password' => $this->parameters['password'],
        ] + $otherOptions;

        try {
            $client = new \SoapClient('https://web.spaggiari.eu/services/ws/wsExtData.php?wsdl', $soapOptions);
            $result = $client->__soapCall($call, $options);
        } catch (\Exception $e) {
            throw new \ErrorException('Errore scaricamento dati per il sync ('.$call.'): '.$e->getMessage());
        }

        if ('' == $result) {
            throw new \ErrorException('Dati per il sync ('.$call.') non presenti');
        }

        try {
            $dati = json_decode($result);
        } catch (\Exception $e) {
        }

        if (!isset($dati) || !is_object($dati) || !isset($dati->errcode)) {
            throw new \ErrorException('Dati per il sync ('.$call.') non validi');
        }

        if ('' != $dati->errcode || '' != $dati->errmsg) {
            throw new \ErrorException('Errore nella chiamata '.$call.': '.$dati->errcode.' - '.$dati->errmsg);
        }

        return $dati;
    }

    public function setSecretKey($key)
    {
        list($schoolYear, $guid, $cid, $login, $password) = explode('==', $key);
        $this->parameters['guid'] = $guid;
        $this->parameters['as'] = $schoolYear;
        $this->parameters['cid'] = $cid;
        $this->parameters['login'] = $login;
        $this->parameters['password'] = $password;
    }
}
