<?php

namespace App\ImporterFilter\ImportedEntity;

class Teacher extends AbstractEntity
{
    protected $fieldToCheck = ['fiscalCode', 'firstName', 'lastName'];

    /**
     * @var string
     */
    protected $fiscalCode = null;

    /**
     * @var string
     */
    protected $firstName = null;

    /**
     * @var string
     */
    protected $lastName = null;

    /**
     * @var string
     */
    protected $email = null;

    /**
     * @var string
     */
    protected $password = null;

    /**
     * @var string
     */
    protected $username = null;

    /**
     * Teacher constructor.
     *
     * @param string $id
     * @param string $fiscalCode
     * @param string $firstName
     * @param string $lastName
     * @param string $groupId
     * @param string $email
     * @param string $password
     */
    public function __construct($id, $fiscalCode, $firstName, $lastName, $email = null, $username = null, $password = null)
    {
        $this->fiscalCode = $fiscalCode;
        $this->firstName = $firstName;
        $this->lastName = $lastName;
        $this->email = $email;
        $this->username = $username;
        $this->password = $password;
        parent::__construct($id);
    }

    /**
     * @return string
     */
    public function getFiscalCode()
    {
        return $this->fiscalCode;
    }

    /**
     * @param string $fiscalCode
     */
    public function setFiscalCode($fiscalCode)
    {
        $this->fiscalCode = $fiscalCode;
    }

    /**
     * @return string
     */
    public function getFirstName()
    {
        return $this->firstName;
    }

    /**
     * @param string $firstName
     */
    public function setFirstName($firstName)
    {
        $this->firstName = $firstName;
    }

    /**
     * @return string
     */
    public function getLastName()
    {
        return $this->lastName;
    }

    /**
     * @param string $lastName
     */
    public function setLastName($lastName)
    {
        $this->lastName = $lastName;
    }

    /**
     * @return string
     */
    public function getEmail()
    {
        return $this->email;
    }

    /**
     * @param string $email
     */
    public function setEmail($email)
    {
        $this->email = $email;
    }

    /**
     * @return string
     */
    public function getPassword()
    {
        return $this->password;
    }

    /**
     * @param string $password
     */
    public function setPassword($password)
    {
        $this->password = $password;
    }

    /**
     * @return string
     */
    public function getUsername()
    {
        return $this->username;
    }

    /**
     * @param string $username
     */
    public function setUsername($username)
    {
        $this->username = $username;
    }

    public function toArray()
    {
        return [
            'fiscalCode' => $this->fiscalCode,
            'firstName' => $this->firstName,
            'lastName' => $this->lastName,
            'email' => $this->email,
            'username' => $this->username,
            'password' => $this->password,
        ];
    }
}
