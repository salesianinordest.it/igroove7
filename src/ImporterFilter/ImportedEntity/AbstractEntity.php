<?php

namespace App\ImporterFilter\ImportedEntity;

abstract class AbstractEntity
{
    /**
     * @var string
     */
    protected $id;

    protected $fieldToCheck = [];

    /**
     * AbstractEntity constructor.
     *
     * @param string $id
     */
    public function __construct($id)
    {
        $this->id = $id;
    }

    public function isValid()
    {
        foreach ($this->fieldToCheck as $fieldName) {
            if (!isset($this->$fieldName) || $this->$fieldName === null || '' == trim($this->$fieldName)) {
                return false;
            }
        }

        return true;
    }

    /**
     * @return string
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param string $id
     */
    public function setId($id)
    {
        $this->id = $id;
    }

    abstract public function toArray();
}
