<?php

namespace App\ImporterFilter\ImportedEntity;

class Subject extends AbstractEntity
{
    protected $fieldToCheck = ['name'];

    /**
     * @var string
     */
    protected $name;

    /**
     * Subject constructor.
     *
     * @param string $id
     * @param string $name
     */
    public function __construct($id, $name)
    {
        $this->name = $name;
        parent::__construct($id);
    }

    /**
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * @param string $name
     */
    public function setName($name)
    {
        $this->name = $name;
    }

    /**
     * @return array
     */
    public function toArray()
    {
        return ['name' => $this->name];
    }
}
