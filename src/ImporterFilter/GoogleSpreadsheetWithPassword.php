<?php

namespace App\ImporterFilter;

use App\ImporterFilter\ImportedEntity\Group;
use App\ImporterFilter\ImportedEntity\Student;
use App\ImporterFilter\ImportedEntity\Teacher;
use Symfony\Component\Form\Extension\Core\Type\TextType;

class GoogleSpreadsheetWithPassword extends AbstractFilter
{
    public static $name = 'Google Spreadsheet con password';
    public static $internalName = 'GoogleSpreadsheetWithPassword';
    public static $parametersUi = [
        'spreadsheetId' => ['title' => 'spreadsheetId', 'type' => TextType::class],
        'clientId' => ['title' => 'Client ID', 'type' => TextType::class],
        'clientSecret' => ['title' => 'Client Secret', 'type' => TextType::class],
        'refreshToken' => ['title' => 'Refresh token', 'type' => TextType::class],
    ];

    /**
     * @var \Google_Client
     */
    private $client;
    private $kernelRootDir;

    public function __construct(string $projectDir)
    {
        $this->kernelRootDir = $projectDir;
    }

    public function setParameters($parameters)
    {
        parent::setParameters($parameters);
        $this->client = $this->authorize();
    }

    private function authorize()
    {
        // ##> google/apiclient ###
        // ##
        // Indicazioni su come creare client id, secret e refresh token:
        // https://github.com/ivanvermeyen/laravel-google-drive-demo/blob/master/README.md#create-your-google-drive-api-keys
        //
        // Account per:
        //   - leggere il foglio google
        // Deve avere lo scope:
        //   https://www.googleapis.com/auth/spreadsheets.readonly
        // Gli scopes si trovano nella pagina https://developers.google.com/identity/protocols/oauth2/scopes
        // Usare la pagina https://developers.google.com/oauthplayground/ per creare il refresh token
        //

        $client = new \Google_Client();
        $client->setClientId($this->parameters['clientId']);
        $client->setClientSecret($this->parameters['clientSecret']);
        $client->refreshToken($this->parameters['refreshToken']);
        $client->authorize();

        return $client;
    }

    public function parseRemoteData()
    {
        $remove = [
            '^',
            ',',
            '.',
            ':',
            '/',
            '\\',
            ',',
            '=',
            '+',
            '<',
            '>',
            ';',
            '"',
            '#',
            "'",
            '(',
            ')',
            "'",
            "\x00",
            '?',
            '.',
            '-',
            '!',
            '°',
            '*',
        ];

        $service = new \Google_Service_Sheets($this->client);

        $range = 'Studenti!A1:H1';
        $response = $service->spreadsheets_values->get($this->parameters['spreadsheetId'], $range);
        $values = $response->getValues();

        if (empty($values)) {
            throw new \ErrorException("Dati $range non presenti");
        }

        $headers = array_flip($values[0]);
        $range = 'Studenti!A2:H';
        $response = $service->spreadsheets_values->get($this->parameters['spreadsheetId'], $range);
        $values = $response->getValues();

        if (!empty($values)) {
            foreach ($values as $row) {
                if (!isset($row[6])) {
                    continue;
                }
                $classe = trim($row[$headers['Classe']]);
                $classe = str_replace($remove, '', $classe);

                $idClasse = md5(strtolower($classe));

                $id = trim(strtolower($row[$headers['Identificativo']]));
                if ('' == $id) {
                    continue;
                }
                if (0 == strlen(trim($classe))) {
                    continue;
                }

                $this->groups[$idClasse] = new Group($idClasse, $classe, 0);

                $this->students[(int) $id] = new Student(
                    (int) $id,
                    trim(strtolower($row[$headers['Codice fiscale']])),
                    trim(ucwords(strtolower($row[$headers['Nome']]))),
                    trim(ucwords(strtolower($row[$headers['Cognome']]))),
                    $idClasse,
                    trim(strtolower($row[$headers['Email']])),
                    trim(strtolower($row[$headers['Username']])),
                    trim($row[$headers['Password iniziale']])
                );
            }
        }

        $range = 'Docenti!A1:G1';
        $response = $service->spreadsheets_values->get($this->parameters['spreadsheetId'], $range);
        $values = $response->getValues();

        $headers = array_flip($values[0]);
        $range = 'Docenti!A2:G';
        $response = $service->spreadsheets_values->get($this->parameters['spreadsheetId'], $range);
        $values = $response->getValues();

        if (!empty($values)) {
            foreach ($values as $row) {
                $id = trim(strtolower($row[$headers['Identificativo']]));
                if ('' == $id) {
                    continue;
                }

                $this->teachers[(int) $id] = new Teacher(
                    (int) $id,
                    trim(strtolower($row[$headers['Codice fiscale']])),
                    trim(ucwords(strtolower($row[$headers['Nome']]))),
                    trim(ucwords(strtolower($row[$headers['Cognome']]))),
                    trim(strtolower($row[$headers['Email']])),
                    trim(strtolower($row[$headers['Username']])),
                    trim($row[$headers['Password iniziale']])
                );
            }
        }
    }
}
