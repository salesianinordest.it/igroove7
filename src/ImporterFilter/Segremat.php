<?php

namespace App\ImporterFilter;

use App\ImporterFilter\ImportedEntity\Group;
use App\ImporterFilter\ImportedEntity\Student;
use GuzzleHttp\Client;
use Symfony\Component\Form\Extension\Core\Type\TextType;

class Segremat extends AbstractFilter
{
    public static $name = 'Soluzione Srl - Segremat';
    public static $internalName = 'segremat';
    public static $parametersUi = ['uri' => ['title' => 'URI della fonte dati', 'type' => TextType::class], 'secretKey' => ['title' => 'Chiave Segreta', 'type' => TextType::class]];
    protected $dataUri;
    protected $guzzle;

    public function __construct()
    {
        $this->guzzle = new Client();
    }

    public function setParameters($parameters)
    {
        parent::setParameters($parameters);

        /*
         * Come concordato, per ottenere i dati occorre passare il parametro
         * k = md5((KEY + SYSDATE))
         * che funge da chiave di autenticazione.
         * Con
         * KEY     = chiave
         * SYSDATE = data attuale nel formato YYYYmmdd     (esempio per oggi 20141127)
         */

        $secretKey = md5($this->parameters['secretKey'].date('Ymd'));
        $this->dataUri = $this->parameters['uri'].'?key='.$secretKey;
    }

    public function parseRemoteData()
    {
        try {
            $request = $this->guzzle->get($this->dataUri);
            $response = $request->getBody()->getContents();
            $list = json_decode($response, true);
        } catch (\Guzzle\Common\Exception\RuntimeException $e) {
            $list = [];
        }

        $genitori = [];

        foreach ($list as $k => $v) {
            $classe = trim($v['group']);
            $remove = [
                '^',
                ',',
                '.',
                ':',
                '/',
                '\\',
                ',',
                '=',
                '+',
                '<',
                '>',
                ';',
                '"',
                '#',
                "'",
                '(',
                ')',
                "'",
                "\x00",
                '?',
                '.',
                '-',
                '!',
            ];
            $classe = str_replace($remove, '', $classe);

            if ('' == trim(strtolower($v['id']))) {
                continue;
            }
            if (0 == strlen(trim($classe))) {
                continue;
            }
            $id = $v['id'];
            $idClasse = md5(strtolower($classe));

            $this->students[$id] = new Student((int) $id, trim(strtolower($v['cf'])), trim(ucwords(strtolower($v['firstname']))), trim(ucwords(strtolower($v['lastname']))), $idClasse, trim(strtolower($v['email'])));
            $this->groups[$idClasse] = new Group($idClasse, $classe, 0);

            if (array_key_exists('genitori', $v)) {
                $gs = $v['genitori'];
                foreach ($gs as $g) {
                    $e = $g['email'];
                    if (strlen($e) > 3) {
                        $genitori[$v['group']][] = $e;
                    }
                }
            }
        }

        foreach ($genitori as $classe => $elenco) {
            @mkdir('/var/www/igroove/app/elenco_genitori');
            file_put_contents('/var/www/igroove/app/elenco_genitori/'.$classe.'.txt', implode("\r\n", $elenco));
        }
    }
}
