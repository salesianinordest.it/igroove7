<?php

namespace App\ImporterFilter;

use App\ImporterFilter\ImportedEntity\Group;
use App\ImporterFilter\ImportedEntity\Student;
use App\ImporterFilter\ImportedEntity\Teacher;
use Symfony\Component\Form\Extension\Core\Type\TextType;

class GoogleSpreadsheet extends AbstractFilter
{
    public static $name = 'Google Spreadsheet';
    public static $internalName = 'GoogleSpreadsheet';
    public static $parametersUi = [
        'spreadsheetId' => ['title' => 'spreadsheetId', 'type' => TextType::class],
        'clientId' => ['title' => 'Client ID', 'type' => TextType::class],
        'clientSecret' => ['title' => 'Client Secret', 'type' => TextType::class],
        'excludeGroups' => ['title' => "Classi da escludere nell'importazione (separati da virgola)", 'type' => TextType::class],
    ];
    private $client;
    private $kernelRootDir;

    public function __construct(string $projectDir)
    {
        $this->kernelRootDir = $projectDir;
    }

    public function setParameters($parameters)
    {
        parent::setParameters($parameters);
        $this->authorize();
    }

    private function authorize()
    {
        if ('' == $this->parameters['clientId'] || '' == $this->parameters['clientSecret']) {
            return;
        }
        $client = new \Google_Client();
        $client->setApplicationName('igroove');
        $client->setClientId($this->parameters['clientId']);
        $client->setClientSecret($this->parameters['clientSecret']);
        $client->setRedirectUri('urn:ietf:wg:oauth:2.0:oob');
        $client->setScopes(\Google_Service_Sheets::SPREADSHEETS_READONLY);

        $client->setAccessType('offline');

        $tokenFilename = $this->kernelRootDir.'/../googleAppsToken_'.$this->parameters['spreadsheetId'].'.json';
        if (!file_exists($tokenFilename)) {
            if ('cli' != php_sapi_name()) {
                $this->logger->error('GOOGLEAPPS: Autenticazione API GOOGLE non effettuata. Chiamare il cron da terminale');
                throw new LoggedException('Autenticazione API GOOGLE non effettuata. Chiamare il cron da terminale');
            }

            $authUrl = $client->createAuthUrl();
            // Request authorization
            echo "\n* * * *  Prima autenticazione";
            echo "Inserisci nel browser:\n$authUrl\n\n";
            echo "Inserisci il codice di autenticazione:\n";
            $authCode = trim(fgets(STDIN));
            // Exchange authorization code for access token
            $accessToken = $client->authenticate($authCode);

            $client->setAccessToken($accessToken);

            file_put_contents($tokenFilename, json_encode($accessToken));
        }

        $client->setAccessToken(file_get_contents($tokenFilename));
        if ($client->isAccessTokenExpired()) {
            if ('cli' == php_sapi_name()) {
                echo "\n* * * *  Riautenticazione API GOOGLE in corso...\n";
            }
            $accessToken = $client->getAccessToken();
            $client->refreshToken($accessToken['refresh_token']);
            file_put_contents($tokenFilename, json_encode($client->getAccessToken()));
        }
        $this->client = $client;
    }

    public function parseRemoteData()
    {
        $remove = [
            '^',
            ',',
            '.',
            ':',
            '/',
            '\\',
            ',',
            '=',
            '+',
            '<',
            '>',
            ';',
            '"',
            '#',
            "'",
            '(',
            ')',
            "'",
            "\x00",
            '?',
            '.',
            '-',
            '!',
            '°',
            '*',
        ];

        $service = new \Google_Service_Sheets($this->client);

        $range = 'Studenti!A1:F1';
        $response = $service->spreadsheets_values->get($this->parameters['spreadsheetId'], $range);
        $values = $response->getValues();

        if (empty($values)) {
            throw new \ErrorException("Dati $range non presenti");
        }

        $headers = array_flip($values[0]);
        $range = 'Studenti!A2:F';
        $response = $service->spreadsheets_values->get($this->parameters['spreadsheetId'], $range);
        $values = $response->getValues();

        if (empty($values)) {
            throw new \ErrorException("Dati $range non presenti");
        }
        foreach ($values as $row) {
            $classe = trim($row[$headers['Classe']]);
            $classe = str_replace($remove, '', $classe);

            if ($this->skipThisClasse($classe)) {
                continue;
            }

            $idClasse = md5(strtolower($classe));

            $id = trim(strtolower($row[$headers['Identificativo']]));
            if ('' == $id) {
                continue;
            }
            if (0 == strlen(trim($classe))) {
                continue;
            }

            $this->groups[$idClasse] = new Group($idClasse, $classe, 0);

            $this->students[(int) $id] = new Student(
                (int) $id,
                trim(strtolower($row[$headers['CodiceFiscale']])),
                trim(ucwords(strtolower($row[$headers['Nome']]))),
                trim(ucwords(strtolower($row[$headers['Cognome']]))),
                $idClasse,
                trim(strtolower($row[$headers['Email']]))
            );
        }

        $range = 'Docenti!A1:F1';
        $response = $service->spreadsheets_values->get($this->parameters['spreadsheetId'], $range);
        $values = $response->getValues();

        if (empty($values)) {
            throw new \ErrorException("Dati $range non presenti");
        }

        $headers = array_flip($values[0]);
        $range = 'Docenti!A2:F';
        $response = $service->spreadsheets_values->get($this->parameters['spreadsheetId'], $range);
        $values = $response->getValues();

        if (empty($values)) {
            throw new \ErrorException("Dati $range non presenti");
        }
        foreach ($values as $row) {
            $id = trim(strtolower($row[$headers['Identificativo']]));
            if ('' == $id) {
                continue;
            }

            $password = (isset($headers['Password iniziale']) && isset($row[$headers['Password iniziale']])) ? $row[$headers['Password iniziale']] : null;

            $this->teachers[(int) $id] = new Teacher(
                (int) $id,
                trim(strtolower($row[$headers['CodiceFiscale']])),
                trim(ucwords(strtolower($row[$headers['Nome']]))),
                trim(ucwords(strtolower($row[$headers['Cognome']]))),
                trim(strtolower($row[$headers['Email']])),
                null,
                $password
            );
        }
    }

    private function skipThisClasse($classe)
    {
        if (!isset($this->parameters['excludeGroups'])) {
            return false;
        }
        if (0 == strlen($this->parameters['excludeGroups'])) {
            return false;
        }
        $excludeGroups = explode(',', $this->parameters['excludeGroups']);
        foreach ($excludeGroups as $group) {
            $group = trim(strtolower($group));
            $classe = trim(strtolower($classe));
            if (strlen($group) > strlen($classe)) {
                return false;
            }
            if (substr($classe, 0, strlen($group)) == $group) {
                return true;
            }
        }

        return false;
    }
}
