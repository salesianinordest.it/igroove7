<?php

namespace App\Message;

class MikrotikMessage
{
    /**
     * @var string
     */
    private $command;

    /**
     * @var array
     */
    private $parameters;

    public function getCommand(): string
    {
        return $this->command;
    }

    public function setCommand(string $command): void
    {
        $this->command = $command;
    }

    public function getParameters(): array
    {
        return $this->parameters;
    }

    public function setParameters(array $parameters): void
    {
        $this->parameters = $parameters;
    }
}
