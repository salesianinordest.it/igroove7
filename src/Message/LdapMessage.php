<?php

namespace App\Message;

class LdapMessage
{
    /**
     * @var string
     */
    private $action;

	/**
	 * @var array
	 */
	private $parameters;

    public function getAction(): string
    {
        return $this->action;
    }

    public function setAction(string $action): void
    {
        $this->action = $action;
    }

	public function getParameters(): array
	{
		return $this->parameters;
	}

	public function setParameters(array $parameters): void
	{
		$this->parameters = $parameters;
	}
}
