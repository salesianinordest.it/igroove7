<?php

namespace App\MdmConnector;

use App\MdmConnector\ImportedEntity\Device;

abstract class AbstractMdmConnector {

    /**
     * @var array
     */
    protected $connectionParameters;

    /**
     * @var Device[]
     */
    protected $devices;

    /**
     * AbstractMdmConnector constructor.
     *
     * @param array $connectionParameters
     */
    public function __construct(array $connectionParameters) {
        $this->connectionParameters = $connectionParameters;
        $this->devices = [];
    }

    abstract public function importRemoteData($onlyAfterDate = null);

    /**
     * @return Device[]
     */
    public function getDevices(): array {
        return $this->devices;
    }

}