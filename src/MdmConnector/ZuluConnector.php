<?php

namespace App\MdmConnector;

use App\MdmConnector\ImportedEntity\Device;
use Symfony\Contracts\HttpClient\HttpClientInterface;

class ZuluConnector extends AbstractMdmConnector {

	public static $name = 'Zulu - Jamf School';
	public static $internalName = 'zulu';
	public static $parametersUi = [
		'networkId' => ['title' => 'Network ID', 'type' => 'text'],
		'apiKey' => ['title' => 'Api Key', 'type' => 'text'],
		'apiBaseUrl' => ['title' => 'URI personalizzato della fonte dati Api', 'type' => 'text'],
	];

	protected \GuzzleHttp\Client $apiConnection;

	public function __construct(array $connectionParameters) {
		parent::__construct($connectionParameters);

//		if(!isset($this->connectionParameters['apiBaseUrl']) || $this->connectionParameters['apiBaseUrl'] == "") {
//			throw new \Exception("Invalid base url for the profile manager api");
//		}

		if($this->connectionParameters['apiBaseUrl'] == null || strlen($this->connectionParameters['apiBaseUrl']) < 1) {
			$this->connectionParameters['apiBaseUrl'] = "https://api.zuludesk.com/";
		} else if(substr($this->connectionParameters['apiBaseUrl'],-1) !== "/") {
			$this->connectionParameters['apiBaseUrl'] .= "/";
		}

		if(!isset($this->connectionParameters['networkId']) || $this->connectionParameters['networkId'] == "") {
			throw new \Exception("Invalid username for the profile manager api");
		}

		if(!isset($this->connectionParameters['apiKey']) || $this->connectionParameters['apiKey'] == "") {
			throw new \Exception("Invalid password for the profile manager api");
		}


		$this->apiConnection = new \GuzzleHttp\Client();
	}

	public function importRemoteData($onlyAfterDate = NULL) {
		$devices = $this->callGetApi("devices");
		if(!is_array($devices) || !isset($devices['devices'])) {
			return;
		}

		foreach ($devices['devices'] as $device) {
			$this->devices[$device['UDID']] = new Device($device, [
				['from' => "UDID", 'to' => "id", 'type' => "string"],
				['from' => "serialNumber", 'to' => "serial", 'type' => "string"],
				['from' => "name", 'to' => "name", 'type' => "string"],
				['from' => "isSupervised", 'to' => "supervised", 'type' => "bool"],
				['from' => "lastCheckin", 'to' => "lastInfoUpdate", 'type' => "datetime"],
				['from' => "modified", 'to' => "lastUpdate", 'type' => "datetime"]
			]);

			$this->devices[$device['UDID']]->setFromDep(isset($device['enrollType']) && $device['enrollType']);
			$this->devices[$device['UDID']]->setModel($device['model']['identifier']??'');
			$this->devices[$device['UDID']]->setOsVersion($device['os']['version']??'');
			$this->devices[$device['UDID']]->setMac($device['networkInformation']['WiFiMAC']??'');
			$this->devices[$device['UDID']]->setBluethootMac($device['networkInformation']['BluetoothMAC']??'');
			$this->devices[$device['UDID']]->setActivationBypassCode('');

			if(isset($device['owner']) && is_array($device['owner'])) {
				if(isset($device['owner']['id']) && $device['owner']['id'] != "") {
					$this->devices[$device['UDID']]->setUserId($device['owner']['id']);
				}

				if(isset($device['owner']['username']) && $device['owner']['username'] != "") {
					$this->devices[$device['UDID']]->setOwnerUsername($device['owner']['username']);
				}
			}
		}
	}

	protected function callGetApi($apiPath,$filters=[]) {
		$options = ['auth' => [$this->connectionParameters['networkId'], $this->connectionParameters['apiKey']]];
		if(!empty($filters)) {
			$options['filter'] = json_encode($filters);
		}

		return json_decode($this->apiConnection->get($this->connectionParameters['apiBaseUrl'].$apiPath, $options)->getBody(), true);
	}

	protected function getAuthorizationKey() {
		static $authKey;
		if(!isset($authKey)) {
			$authKey = "Basic ".base64_encode($this->connectionParameters['networkId'].":".$this->connectionParameters['apiKey']);
		}
		return $authKey;
	}
}