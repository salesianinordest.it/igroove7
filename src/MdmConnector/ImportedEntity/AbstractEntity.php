<?php

namespace App\MdmConnector\ImportedEntity;

abstract class AbstractEntity {

    public function __construct($dataFromApi = [], $importInstructions = []) {
        $this->setFromApi($dataFromApi, $importInstructions);
    }

    public function setFromApi($data, $importInstructions) {
        if(!is_array($data) || empty($data) || empty($importInstructions)) {
            return;
        }

        foreach ($importInstructions as $importInstruction) {
            if(!is_array($importInstruction) || !isset($importInstruction['from']) || !isset($importInstruction['to']) ||
                !isset($importInstruction['type']) || !property_exists($this, $importInstruction['to'])|| !array_key_exists($importInstruction['from'],$data)
            ) {
                continue;
            }

            if(isset($importInstruction['nullable']) && $importInstruction['nullable'] && $data[$importInstruction['from']] == null) {
                $this->{$importInstruction['to']} = null;
            }

            switch ($importInstruction['type']) {
                case "int":
                    $this->{$importInstruction['to']} = (int)$data[$importInstruction['from']];
                    break;

                case "bool":
                    $this->{$importInstruction['to']} = (bool)$data[$importInstruction['from']];
                    break;

                case "string":
                    $this->{$importInstruction['to']} = (string)$data[$importInstruction['from']];
                    break;

                case "datetime":
                    $this->{$importInstruction['to']} = new \DateTime($data[$importInstruction['from']]);
                    break;
            }
        }
    }
}