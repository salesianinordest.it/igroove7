<?php

namespace App\MdmConnector\ImportedEntity;


use App\Entity\Student;
use App\Entity\Teacher;

class Device extends AbstractEntity {

    /**
     * @var int
     */
    protected $id;

    /**
     * @var string
     */
    protected $serial;

    /**
     * @var string
     */
    protected $mac;

    /**
     * @var string
     */
    protected $bluethootMac;

    /**
     * @var string|null
     */
    protected $ownerUsername;

    /**
     * @var int|null
     */
    protected $userId;

    /**
     * @var string
     */
    protected $name;

    /**
     * @var string
     */
    protected $model;

    /**
     * @var string
     */
    protected $osVersion;

    /**
     * @var bool
     */
    protected $fromDep;

    /**
     * @var bool
     */
    protected $supervised;

    /**
     * @var string
     */
    protected $activationBypassCode;

    /**
     * @var \DateTime
     */
    protected $lastInfoUpdate;

    /**
     * @var \DateTime
     */
    protected $lastUpdate;

	/**
	 * @var null|Student
	 */
	protected $studentOwner = null;

	/**
	 * @var null|Teacher
	 */
	protected $teacherOwner = null;


    /**
     * @return string
     */
    public function getId() {
        return $this->id;
    }

    /**
     * @param string $id
     */
    public function setId($id) {
        $this->id = $id;
    }


    /**
     * @return string
     */
    public function getSerial(): string {
        return $this->serial;
    }

    /**
     * @param string $serial
     */
    public function setSerial(string $serial) {
        $this->serial = $serial;
    }

    /**
     * @return string
     */
    public function getMac(): string {
        return $this->mac;
    }

    /**
     * @param string $mac
     */
    public function setMac(string $mac) {
        $this->mac = $mac;
    }

    /**
     * @return string
     */
    public function getBluethootMac(): string {
        return $this->bluethootMac;
    }

    /**
     * @param string $bluethootMac
     */
    public function setBluethootMac(string $bluethootMac) {
        $this->bluethootMac = $bluethootMac;
    }

    /**
     * @return string|null
     */
    public function getOwnerUsername() {
        return $this->ownerUsername;
    }

    /**
     * @param string|null $ownerUsername
     */
    public function setOwnerUsername($ownerUsername) {
        $this->ownerUsername = $ownerUsername;
    }

    /**
     * @return int|null
     */
    public function getUserId() {
        return $this->userId;
    }

    /**
     * @param int|null $userId
     */
    public function setUserId($userId) {
        $this->userId = $userId;
    }

    /**
     * @return string
     */
    public function getName(): string {
        return $this->name;
    }

    /**
     * @param string $name
     */
    public function setName(string $name) {
        $this->name = $name;
    }

    /**
     * @return string
     */
    public function getModel(): string {
        return $this->model;
    }

    /**
     * @param string $model
     */
    public function setModel(string $model) {
        $this->model = $model;
    }

    /**
     * @return string
     */
    public function getOsVersion(): string {
        return $this->osVersion;
    }

    /**
     * @param string $osVersion
     */
    public function setOsVersion(string $osVersion) {
        $this->osVersion = $osVersion;
    }

    /**
     * @return bool
     */
    public function getFromDep(): bool {
        return $this->fromDep;
    }

    /**
     * @param bool $fromDep
     */
    public function setFromDep(bool $fromDep) {
        $this->fromDep = $fromDep;
    }

    /**
     * @return bool
     */
    public function getSupervised(): bool {
        return $this->supervised;
    }

    /**
     * @param bool $supervised
     */
    public function setSupervised(bool $supervised) {
        $this->supervised = $supervised;
    }

    /**
     * @return string
     */
    public function getActivationBypassCode(): string {
        return $this->activationBypassCode;
    }

    /**
     * @param string $activationBypassCode
     */
    public function setActivationBypassCode(string $activationBypassCode) {
        $this->activationBypassCode = $activationBypassCode;
    }

    /**
     * @return \DateTime
     */
    public function getLastInfoUpdate(): \DateTime {
        return $this->lastInfoUpdate;
    }

    /**
     * @param \DateTime $lastInfoUpdate
     */
    public function setLastInfoUpdate(\DateTime $lastInfoUpdate) {
        $this->lastInfoUpdate = $lastInfoUpdate;
    }

    /**
     * @return \DateTime
     */
    public function getLastUpdate(): \DateTime {
        return $this->lastUpdate;
    }

    /**
     * @param \DateTime $lastUpdate
     */
    public function setLastUpdate(\DateTime $lastUpdate) {
        $this->lastUpdate = $lastUpdate;
    }

	/**
	 * @return Student|null
	 */
	public function getStudentOwner(): ?Student {
		return $this->studentOwner;
	}

	/**
	 * @param Student|null $studentOwner
	 * @return Device
	 */
	public function setStudentOwner(?Student $studentOwner): Device {
		$this->studentOwner = $studentOwner;

		return $this;
	}

	/**
	 * @return Teacher|null
	 */
	public function getTeacherOwner(): ?Teacher {
		return $this->teacherOwner;
	}

	/**
	 * @param Teacher|null $teacherOwner
	 * @return Device
	 */
	public function setTeacherOwner(?Teacher $teacherOwner): Device {
		$this->teacherOwner = $teacherOwner;

		return $this;
	}
}