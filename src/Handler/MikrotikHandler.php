<?php

namespace App\Handler;

use App\Manager\MikrotikManager;
use App\Message\MikrotikMessage;
use Psr\Log\LoggerInterface;
use Symfony\Component\Messenger\Handler\MessageHandlerInterface;

class MikrotikHandler implements MessageHandlerInterface
{
    /**
     * @var LoggerInterface
     */
    private $_logger;

    /**
     * @var MikrotikMessage
     */
    private $mikrotikMessage;

    /**
     * @var MikrotikManager
     */
    private $mikrotikManager;

    public function __construct(MikrotikManager $mikrotikManager, LoggerInterface $logger, MikrotikMessage $mikrotikMessage)
    {
        $this->mikrotikManager = $mikrotikManager;
        $this->_logger = $logger;
        $this->mikrotikMessage = $mikrotikMessage;
    }

    public function __invoke(MikrotikMessage $message)
    {
	    $this->_logger->info("[mikrotik consumer] Received message command ".$message->getCommand());

		try {
			switch ($message->getCommand()) {
	            case 'addMacToHospot':
	                $results = $this->mikrotikManager->manageMacToHotspot();
	                break;

	            case 'addIpReservation':
	                $results = $this->mikrotikManager->addIpReservation();
	                break;

	            case 'addIpList':
	                $results = $this->mikrotikManager->addIpToIpList();
	                break;

	            case 'addIpToBypassedList':
	                $results = $this->mikrotikManager->addIpToBypassedList();
	                break;

	            case 'addPools':
	                $results = $this->mikrotikManager->addPools();
	                break;
	        }

		} catch(\Throwable $e) {
			$this->_logger->error("- Error elaborating the command: ".$e->getMessage(), ['error' => $e]);
			//@todo retry?
		}

	    if (isset($results) && !empty($results)) {
		    $this->_logger->info("- Result of the elaboration: ".json_encode($results));
	    }
    }
}
