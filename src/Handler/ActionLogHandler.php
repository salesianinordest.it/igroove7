<?php

namespace App\Handler;

use Doctrine\ORM\EntityManagerInterface;
use Monolog\Handler\AbstractProcessingHandler;
use App\Entity\ActionLog;

class ActionLogHandler extends AbstractProcessingHandler
{
    /**
     * @var EntityManagerInterface
     */
    protected $em;

    /**
     * MonologDBHandler constructor.
     * @param EntityManagerInterface $em
     */
    public function __construct(EntityManagerInterface $em)
    {
        parent::__construct();
        $this->em = $em;
    }

    /**
     * Called when writing to our database
     * @param array $record
     */
	protected function write(array $record): void
    {
        $logEntry = new ActionLog();
        $logEntry->setMessage($record['message']);
        $logEntry->setLevel($record['level']);
        $logEntry->setExtra($record['extra']);

        if(is_array($record['context'])) {
            if(isset($record['context']['action'])) {
                $logEntry->setAction($record['context']['action']);
                unset($record['context']['action']);
            }

            if(isset($record['context']['ip'])) {
                $logEntry->setIp($record['context']['ip']);
                unset($record['context']['ip']);
            } else {
                $logEntry->setIp($_SERVER['REMOTE_ADDR']);
            }

            if(isset($record['context']['username'])) {
                $logEntry->setUsername($record['context']['username']);
                unset($record['context']['username']);
            }
        }

        $logEntry->setContext($record['context']);

        $this->em->persist($logEntry);
        $this->em->flush();
    }
}