<?php

namespace App\Handler;

use App\Entity\LdapUser;
use App\Entity\PersonAbstract;
use App\Manager\ConfigurationManager;
use App\Manager\GoogleApps;
use App\Manager\LdapProxy;
use App\Message\GoogleMessage;
use Doctrine\ORM\EntityManagerInterface;
use Psr\Log\LoggerInterface;
use Symfony\Component\Messenger\Handler\MessageHandlerInterface;

class GoogleHandler implements MessageHandlerInterface
{
    /**
     * @param $projectDir
     */
    public function __construct($projectDir, EntityManagerInterface $em, LoggerInterface $logger, ConfigurationManager $configurationManager, LdapProxy $ldapProxy, GoogleMessage $googleMessage)
    {
        $this->projectDir = $projectDir;
        $this->em = $em;
        $this->startTime = time();
        $this->logger = $logger;
        $this->configurationManager = $configurationManager;
        $this->ldapProxy = $ldapProxy;
        $this->googleMessage = $googleMessage;
    }

    public function __invoke(GoogleMessage $message)
    {
        $keys = array_keys($message->getParameters());
        $allKeysPresent = (4 == sizeof(array_intersect($keys, ['domain', 'clientId', 'clientSecret', 'ouPath'])));
        if (!$allKeysPresent) {
            $this->logger->error('GOOGLEAPPSCONS: Invalid configuration data');

            return true;
        }

        $this->em->clear();
        if (time() - $this->startTime > 600) {
            $this->em->getConnection()->close();
            $this->em->getConnection()->connect();
        }

        $this->googleApps = new GoogleApps(
            $message->getParameters()['domain'],
            $message->getParameters()['clientId'],
            $message->getParameters()['clientSecret'],
            $message->getParameters()['ouPath'],
            $this->projectDir,
            $this->logger
        );

	    $this->logger->info("[google consumer] Received message command ".$message->getCommand());

        switch ($message->getCommand()) {
            case 'createUser':
                $this->createUser($message->getParameters()['email'], isset($message->getParameters()['firstname']) ? $message->getParameters()['firstname'] : null, isset($message->getParameters()['lastname']) ? $message->getParameters()['lastname'] : null, isset($message->getParameters()['password']) ? $message->getParameters()['password'] : null, isset($message->getParameters()['userId']) ? $message->getParameters()['userId'] : null, isset($message->getParameters()['teacher']) ? (bool) $message->getParameters()['teacher'] : false);
                break;

            case 'renameUser':
                $this->renameUser($message->getParameters()['email'], $message->getParameters()['previousEmail'], isset($message->getParameters()['firstname']) ? $message->getParameters()['firstname'] : null, isset($message->getParameters()['lastname']) ? $message->getParameters()['lastname'] : null, isset($message->getParameters()['userId']) ? $message->getParameters()['userId'] : null, isset($message->getParameters()['teacher']) ? (bool) $message->getParameters()['teacher'] : false);
                break;

            case 'createGroup':
                $this->googleApps->createGroup($message->getParameters()['groupName']);
                break;

            case 'renameGroup':
                $this->googleApps->renameGroup($message->getParameters()['groupName'], $message->getParameters()['previousGroupName']);
                break;

            case 'createOU':
                $this->googleApps->createOU($message->getParameters()['ouName']);
                break;

            case 'addUserToGroup':
                $this->googleApps->addUserToGroup($message->getParameters()['email'], $message->getParameters()['groupName']);
                break;

            case 'addUserToOU':
                $this->googleApps->addUserToOU($message->getParameters()['email'], $message->getParameters()['ouName']);
                break;

            case 'updateGroupMembers':
                $this->googleApps->updateGroupMembers($message->getParameters()['studentsInGroup'], $message->getParameters()['groupName']);
                break;

            case 'updateOUMembers':
                $this->googleApps->updateOUMembers($message->getParameters()['studentsInOU'], $message->getParameters()['ouName']);
                break;

            case 'resetUserPassword':
                $this->googleApps->resetUserPassword($message->getParameters()['email'], $message->getParameters()['password']);
                break;

            case 'removeUser':
                $this->googleApps->removeUser($message->getParameters()['email']);
                break;

            case 'removeGroup':
                $this->googleApps->removeGroup($message->getParameters()['groupName']);
                break;
        }
    }

    private function createUser($email, $firstname, $lastname, $password, $userId, $teacher)
    {
        $this->googleApps->createUser($email, $firstname, $lastname, $password);

        if ('' != $userId) {
            $this->updateLdapUser($email, $userId, $teacher);
        }
    }

    private function renameUser($email, $previousEmail, $firstname, $lastname, $userId, $teacher)
    {
        $this->googleApps->renameUser($email, $previousEmail, $firstname, $lastname);

        if ('' != $userId) {
            $this->updateLdapUser($email, $userId, $teacher);
        }
    }

    protected function updateLdapUser($email, $userId, $teacher)
    {
        $person = $this->em->getRepository($teacher ? 'App\Entity\Teacher' : 'App\Entity\Student')->find($userId);
        if (!$person instanceof PersonAbstract) {
            return;
        }

        $person->setEmail((false === strpos($email, '@')) ? $email.'@'.$this->googleApps->getDomain() : $email);
	    $this->em->flush();

        $ldapUser = $this->em->getRepository('App\Entity\LdapUser')->findOneByDistinguishedId($userId);
        if ($ldapUser instanceof LdapUser) {
            $ldapUser->setAttribute('email', $email);
            $providerSettings = $person->getProviderSettings();
            if ($providerSettings['ldapUseEmailForExtendedUsername']) {
                $extendedUsername = strtolower(substr($person->getEmail(), 0, strpos($person->getEmail(), '@')));
                if ('' != $providerSettings['ldapExtendedUsernameSuffix']) {
                    $extendedUsername .= ('@' != substr($providerSettings['ldapExtendedUsernameSuffix'], 0, 1) ? '@' : '').$providerSettings['ldapExtendedUsernameSuffix'];
                } else {
                    $extendedUsername .= ('@' != substr($this->configurationManager->getActiveDirectoryAccountSuffix(), 0, 1) ? '@' : '').$this->configurationManager->getActiveDirectoryAccountSuffix();
                }
                $ldapUser->setAttribute('extendedUsername', $extendedUsername);
            }
            if ('CREATE' != $ldapUser->getOperation()) {
                $ldapUser->setOperation('MODIFY');
            }
            $this->em->flush();
            $this->ldapProxy->syncLdapUserWithDBUser($ldapUser);
	        $this->em->flush();
        }
    }
}
