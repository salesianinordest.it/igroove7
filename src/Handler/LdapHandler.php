<?php

namespace App\Handler;

use App\Manager\LdapProxy;
use App\Message\LdapMessage;
use Psr\Log\LoggerInterface;
use Symfony\Component\Messenger\Handler\MessageHandlerInterface;

class LdapHandler implements MessageHandlerInterface
{
    /**
     * @var LoggerInterface
     */
    private $_logger;

    /**
     * @var LdapMessage
     */
    private $ldapMessage;

    /**
     * @var
     */
    private $entityManager;

    public function __construct(LdapProxy $ldapProxy, LoggerInterface $logger, LdapMessage $ldapMessage)
    {
        $this->ldapProxy = $ldapProxy;
        $this->_logger = $logger;
        $this->ldapMessage = $ldapMessage;
    }

    public function __invoke(LdapMessage $message)
    {
        $this->ldapProxy->connect();

	    $this->_logger->info("[ldap consumer] Received message command ".$message->getAction());
		switch ($message->getAction()) {
            case 'syncInternetAccessLdapGroup':
                $this->ldapProxy->syncInternetAccessLdapGroup();
                break;

            case 'purgeAndPopulate':
                $this->ldapProxy->purgeAndPopulateAll();
                break;

            case 'syncLDAPfromDB':
                $this->ldapProxy->syncLDAPfromDB();
                break;

            case 'updateUsersIntoOu':
                $this->ldapProxy->updateUsersIntoOu($message->getParameters()['ouName'], $message->getParameters()['users'], $message->getParameters()['path']);
                break;

            case 'moveUserIntoOu':
                $this->ldapProxy->moveUserIntoOu($message->getParameters()['ouName'], $message->getParameters()['username'], $message->getParameters()['path']);
                break;
        }
    }
}
