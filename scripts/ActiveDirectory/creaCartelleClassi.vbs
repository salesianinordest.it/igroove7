' on error resume next 
Set objRootDSE = GetObject("LDAP://RootDSE")
Set objFSO = CreateObject("Scripting.FileSystemObject")
Set objShell = CreateObject("Wscript.Shell") 
folderPath="\\srvd.issm.network\didatticaStudenti\"




gDN = SearchGroup("_Prima formazione")
GetMembers(gDN)
gDN = SearchGroup("_Formazione continua")
GetMembers(gDN)

Function GetMembers(gDN)
    Set objGroup = GetObject("LDAP://" & gDN)
    objGroup.GetInfo
    arrMemberOf = objGroup.GetEx("member")
    
    For Each strMember in arrMemberOf
        Set objMember = GetObject("LDAP://" & strMember)
        ObjDisp = objMember.Name
        oDL = Len(ObjDisp) - 3
        ObjDisp = Right(ObjDisp,oDL)
        ObjCatArray = Split(objMember.objectCategory,",")
        oType = ObjCatArray(0)
        oTL = Len(oType) - 3
        oType = Right(oType,oTL)
        
If oType = "Group" Then
folder=folderPath & objMember.sAMAccountName
If not objFSO.FolderExists(folder) Then
' WScript.Echo "Member:" & ObjDisp & Space(20-Len(ObjDIsp)) &" Folder:" & folder
    Set objFolder = objFSO.CreateFolder(folder)
end if
    cmd = folder & " /G " & objMember.sAMAccountName & ":R ""Domain Admins"":F ""docenti"":F /Y /T"
    objShell.Run ("c:\igroove\xcacls.exe " & cmd )
end if
        If oType = "Group" Then
            GetMembers(strMember)
        End If
        Set objMember = Nothing
    Next
End Function

Public Function SearchGroup(ByVal vSAN)
    Dim oRootDSE, oConnection, oCommand, oRecordSet
    Set oRootDSE = GetObject("LDAP://rootDSE")
    Set oConnection = CreateObject("ADODB.Connection")
    oConnection.Open "Provider=ADsDSOObject;"
    Set oCommand = CreateObject("ADODB.Command")
    oCommand.ActiveConnection = oConnection
    oCommand.CommandText = "<LDAP://" & oRootDSE.get("defaultNamingContext") & _
        ">;(&(objectCategory=Group)(samAccountName=" & vSAN & "));distinguishedName;subtree"
    Set oRecordSet = oCommand.Execute
    On Error Resume Next
    SearchGroup = oRecordSet.Fields("distinguishedName")
    On Error GoTo 0
    oConnection.Close
    Set oRecordSet = Nothing
    Set oCommand = Nothing
    Set oConnection = Nothing
    Set oRootDSE = Nothing
End Function

