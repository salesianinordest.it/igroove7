#!/usr/bin/env bash
DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )"
FILE=$DIR/../config/packages/igroove_version.yaml

version=`git describe --abbrev=0 --tags`
minorFuture=`git rev-list --count --branches ^refs/tags/$version`
minor=`expr $minorFuture - 1`
versionDate=`date +"%Y-%m-%dT%H:%M:%S%z"`


echo "# This file is generated using script/makeVersionFile.sh" > $FILE
echo "parameters:" >> $FILE
echo "    versionInstalled: $version.$minor" >> $FILE
echo "    versionDate: $versionDate" >> $FILE

git commit $FILE -m "Update minor version to $version.$minor"

echo "Updated minor version to $version.$minor"
