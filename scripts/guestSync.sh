#!/usr/bin/env bash

DBUSER=postgres
TODAY=`date +%Y-%m-%d`
LIST=`sudo -u $DBUSER psql -d igroove --no-align --quiet --tuples-only  --field-separator=$'\t' -c "select concat('guest',id),password from Guest where starting_from<='$TODAY' and ending_to>='$TODAY' ORDER BY id DESC;"`
ADLIST=`samba-tool user list`

# Prima passata: creo gli utenti che mancano

IFS=$'\n'
for row in $LIST
do
IFS=$'\t'
  USERNAME=`echo $row | awk '{print $1}'`
  PASSWORD=`echo $row | awk '{print $2}'`
  EXISTS=`echo $ADLIST | grep $USERNAME -c`
  if [ "$EXISTS" == "0" ]; then
     echo samba-tool user create $USERNAME $PASSWORD
     samba-tool user create $USERNAME $PASSWORD
  fi
  IFS=$'\n'
done

# Seconda passata: elimino gli utenti superflui

 IFS=$'\n'
for USERNAME in $ADLIST
do
if [ "$USERNAME" != "Administrator" ];then
  if [ "$USERNAME" != "krbtgt" ]; then
    if [ "$USERNAME" != "Guest" ]; then
      EXISTS=`echo $LIST | grep $USERNAME -c`
      if [ "$EXISTS" != "1" ]; then
        echo samba-tool user delete $USERNAME
        samba-tool user delete $USERNAME
      fi
    fi
  fi
fi
done
