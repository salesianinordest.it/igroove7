#!/usr/bin/env bash

remote=10.10.241.4
docker compose down
docker compose up --pull always -d
ssh root@$remote 'cd /tmp;sudo -u postgres pg_dump  dbname=igroove > /tmp/tmp.sql'
scp root@$remote:/tmp/tmp.sql tmp.sql
docker compose exec  php bin/console doctrine:database:drop --force
docker compose exec  php bin/console doctrine:database:create
cat tmp.sql| docker  exec -i  igroove-database-1 psql app app
rm tmp.sql
