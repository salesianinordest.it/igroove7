# Installazione

Installare i pacchetti:

```
apt install samba smbldap-tools winbind ntpdate
```

Creare un nuovo dominio con:

```

rm /etc/samba/smb.conf
samba-tool domain provision --use-rfc2307 --interactive



Realm [GUEST.NETWORK]:
 Domain [GUEST]:
 Server Role (dc, member, standalone) [dc]:
 DNS backend (SAMBA_INTERNAL, BIND9_FLATFILE, BIND9_DLZ, NONE) [SAMBA_INTERNAL]:
 DNS forwarder IP address (write 'none' to disable forwarding) [10.10.248.1]: none
Administrator password:
Retype password:
```

Impostare i criteri delle password che usseranno gli ospiti:

```
samba-tool domain passwordsettings set --complexity=off
samba-tool domain passwordsettings set --min-pwd-length=4
samba-tool domain passwordsettings set --min-pwd-age=0
samba-tool domain passwordsettings set --max-pwd-age=0
```

Attivare il dc

```
systemctl unmask samba-ad-dc
systemctl enable samba-ad-dc
systemctl start samba-ad-dc
```

Provare a creare un utente:

```
samba-tool user create guest0 1234
```

Verificare la corretta creazione:

```
samba-tool user list
```


Copiare lo script di sincronizzazione:

```
cp /var/www/igroove/scripts/guestSync.sh /root
```

Modificare i parametri (username e password) e poi inserirlo nel crontab ogni minuto.

```
# m h  dom mon dow   command
*   *  *   *   *     /root/guestSync.sh
```




# 802.1x



Aggiungere le righe mancanti al file /etc/samba/smb.conf:

```
# Global parameters
[global]
  dns proxy = no
	netbios name = ENTERPRISE
	realm = GUEST.NETWORK
	workgroup = GUEST
	server role = active directory domain controller
	idmap_ldb:use rfc2307 = yes
	winbind refresh tickets = yes
	winbind offline logon = yes
	winbind enum users = yes
	winbind enum groups = yes
	winbind nested groups = yes
	winbind expand groups = 2
	winbind use default domain = yes
	server role check:inhibit=yes
  ntlm auth = yes
  password server = 127.0.0.1
```

Modificare il  /etc/freeradius/3.0/mods-enabled/mschap

```
require_encryption = yes
require_strong = yes
ntlm_auth = "/usr/bin/ntlm_auth --request-nt-key --allow-mschapv2 --username=%{%{Stripped-User-Name}:-%{%{User-Name}:-None}} --challenge=%{%{mschap:Challenge}:-00} --nt-response=%{%{mschap:NT-Response}:-00}"
```

Eventuale workaround per il problema di accesso alla pipe winbindd_privileged:

```
usermod -a -G root freerad
```
