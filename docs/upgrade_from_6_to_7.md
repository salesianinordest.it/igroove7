# 1. Backup
```
cd /var/www
mv igroove igroove6
service supervisor stop
rsync -av /var/www/igroove* /tmp --exclude=*.log
cp -r /etc/freera* /root
mysqldump -u root -p igroove --complete-insert  --no-create-db --no-create-info --no-set-names --no-tablespaces --compact > /root/dump`date --iso-8601`.sql

```

Disabilitare i task `cron` ed `internet` dal 
crontab


# 2. Verificare la versione ed aggiornare a Debian 11

Verificare la versione con `lsb_release -a` e se <11 procedere con l'aggiornamento.

Ad esempio per passare da *stretch* a *bullseye* usare la seguente strategia:

```
apt update
apt purge php*
apt autoremove -y
apt upgrade -y

sed -i 's/stretch/buster/g' /etc/apt/sources.list
sed -i 's/stretch/buster/g' /etc/apt/sources.list.d/*.list
sed -i 's/oldstable/buster/g' /etc/apt/sources.list
sed -i 's/oldstable/buster/g' /etc/apt/sources.list.d/*.list
sed -i 's#/debian-security stretch/updates# stretch-security#g' /etc/apt/sources.list

apt update
apt upgrade
apt dist-upgrade


sed -i 's/buster/bullseye/g' /etc/apt/sources.list
sed -i 's/buster/bullseye/g' /etc/apt/sources.list.d/*.list
sed -i 's#/debian-security bullseye/updates# bullseye-security#g' /etc/apt/sources.list


apt update
apt upgrade -y
apt full-upgrade -y
```

Installare i pacchetti necessari:
```
apt-get install lsb-release ca-certificates sudo wget curl git-core build-essential  \
        vim elinks zip unzip software-properties-common apache2 php8.1-pgsql composer  \
        libapache2-mod-php8.1 php8.1-cli php8.1-curl php8.1-gd php8.1-mbstring   php-amqp \
        php8.1-imagick   php8.1-dev php8.1-xml php8.1-intl php8.1-zip php8.1-bcmath \
        php8.1-ldap php-dev   php8.1-soap  postgresql autoconf automake build-essential pgloader wkhtmltopdf -y

rabbitVersion=`rabbitmqctl status | grep version | awk '{print $3}'`
link=`curl https://github.com/rabbitmq/rabbitmq-delayed-message-exchange/releases/tag/$rabbitVersion | grep .ez | grep href | grep $rabbitVersion | sed 's/.*download//g' | sed 's/\.ez.*/\.ez/g'`
wget https://github.com/rabbitmq/rabbitmq-delayed-message-exchange/releases/download/$link -O /usr/lib/rabbitmq/lib/rabbitmq_server-$rabbitVersion/plugins/rabbitmq_delayed_message_exchange-$rabbitVersion.ez
sudo rabbitmq-plugins enable rabbitmq_management rabbitmq_tracing rabbitmq_federation rabbitmq_delayed_message_exchange


service rabbitmq-server restart
```

# 3. Installazione di igroove7

Eseguire lo script che copia i file e cambiare i permessi:

Dal proprio pc:
```
./script/fastdeploy.sh 192.168.1.1
```

Dal server:
```
cd /var/www
chmod -R 777 igroove
chown -R www-data:www-data igroove
cd /var/www/igroove
```


Copiare il security.yaml corrispondente. Ad esempio:
```
cd /var/www/igroove
cp config/packages/security.yaml_singleDomain config/packages/security.yaml
```

Copiare i file della configurazione e i token google.
```
cp ../igroove6/googleAppsToken_* .
cp ../igroove6/.env.local .     
```

Modificare il file `.env.local`:
- rimuovere riga databaseurl da .env.local
- controllare che non ci siano spazi nei nomi dei gruppi AD ed eventualmente racchiudere tra ''

Occhio agli spazi nel nome del gruppo in .env.local

Esempio sbagliato:
```
ROLE_TEACHER=ROLE_DOCENTI
ROLE_ADMIN=ROLE_DOMAIN_ADMINS
ROLE_RECEPTION=ROLE_PORTINERIA
```
Esempio corretto:
```
ROLE_TEACHER=ROLE_DOCENTI
ROLE_ADMIN='ROLE_DOMAIN ADMINS'
ROLE_RECEPTION=ROLE_PORTINERIA
```



# 4. Creazione database postgreSQL e schema

```
sudo -u postgres psql -U postgres -d postgres -c "alter user postgres with password 'postgres';"
php bin/console doctrine:database:create
php bin/console doctrine:schema:update --force

chmod -R 777 var
chown -R www-data:www-data var
```

# 5. Verifica corretta installazione

Aprire il browser `http://igroove....` e verificare il login.



# 6. Importazioni dei dati.

Eseguire i comandi all'interno della cartella  `/var/www/igroove/upgrade/db_from_6_to_7/tables/`.
Attenzione che i comandi vengano eseguiti correttamente.

Impostare le sequenze per le tabelle con indici numerici:
```
sudo -u postgres psql -d igroove -c "select setval('"public"."guest_id_seq"'::regclass, (select MAX("id") FROM "public"."guest"));"
```


# 7. Prima verifica dei componenti


```
php bin/console test:ad
php bin/console test:mikrotik
php bin/console test:queues
```

# 8. Installare i nuovi consumer

Rimuovere eventuali consumer presenti in `/etc/supervisor/conf.d` ed installare i nuovi:

```
cp /var/www/igroove/scripts/supervisor/* /etc/supervisor/conf.d
```

# 9. Seconda verifica dei componenti
Verificare `cron` e `internet`. Se tutto ok riattivare crontab e supervisor.

# 10. Completamento
- Caricare `docs\demo-badge-ospiti.html`:
  - `cat docs/demo-badge-ospiti.html | pbcopy`
  - Da web, incollare il testo in "HTML per la stampa del badge degli ospiti:" nel tab Wireless della configurazione di igroove
- Rimuovere vecchio script di sincronizzazione degli ospiti ed installare `script/guestSync.sh`
- Se si usa il provider `SegrematWeb` ricordarsi di aggiornare le regexpr di inclusione ed esclusione:
  - Esclusioni nella configurazione del provider: `((X|x).*)|(Y.*)`
  - Inclusioni nella configurazione del provider: `[A-Z]\d[A-Z]`
- Controllare il corretto funzionamento di freeradius.