#!/bin/bash
workingDir="$(dirname -- "$0")/../../"

if ! [ -x "$(command -v docker-compose)" ];
then
  echo "docker-compose missing."
  exit
fi

cd -P -- $workingDir
echo "Moving to working directory $(pwd)"

if [ ! -f "docker-compose.override.yml" ]; then
  read -p "The docker-compose.override.yml file is missing. Are you sure you want to continue? [N/y]" -n 1 -r
  if [[ ! $REPLY =~ ^[YySs]$ ]]
  then
      [[ "$0" = "$BASH_SOURCE" ]] && exit 1 || return 1 # handle exits from shell or function but don't exit interactive shell
  fi
fi


if [ ! -f "config/packages/security.yaml" ]; then
  read -p "The config/packages/security.yaml file is missing (can be copied from the examples in the same folder). Are you sure you want to continue? [N/y]" -n 1 -r
  if [[ ! $REPLY =~ ^[YySs]$ ]]
  then
      [[ "$0" = "$BASH_SOURCE" ]] && exit 1 || return 1
  fi
fi


command docker-compose up -d --build

echo $'\nExecuting composer install'
command docker exec igroove_php composer install --no-interaction --ignore-platform-req=composer-plugin-api

echo $'\nUpdating database schema'
command docker exec igroove_php php /var/www/igroove/bin/console doctrine:schema:update --force

echo $'\Cleaning caches'
command docker exec igroove_php php /var/www/igroove/bin/console cache:clear --env=prod
command docker exec igroove_php php /var/www/igroove/bin/console cache:clear --env=dev