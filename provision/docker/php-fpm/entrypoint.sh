#!/bin/sh

#printf "\nInstall hirak/prestissimo to speedup composer download"
#composer global --ignore-platform-req=composer-plugin-api require hirak/prestissimo

#echo $'\nExecuting composer install'
#composer install --no-interaction --ignore-platform-req=composer-plugin-api

#echo $'\nUpdating database schema'
#php /var/www/igroove/bin/console doctrine:schema:update --force

echo $'\nCopy crontab'
cp /etc/crontab_orig /etc/crontabs/root
chown root:root /etc/crontabs/root

echo $'\nStart supervisor'
exec /usr/bin/supervisord -n