#!/usr/bin/env bash

export DEBIAN_FRONTEND=noninteractive
sed -i "s/bullseye main/bullseye main contrib/"  /etc/apt/sources.list
apt-get update

echo "Update packages"
apt-get install wget gnupg2 -y
apt-get install ca-certificates apt-transport-https wget -y
apt-get update
apt-get upgrade -y


echo "localhost" > /etc/hostname
hostname --file /etc/hostname


echo "Installing APT packages"
apt-get install lsb-release ca-certificates build-essential pkg-config sudo wget curl git-core build-essential \
  vim elinks zip unzip software-properties-common  graphviz supervisor postgresql \
  autoconf automake build-essential rabbitmq-server  wkhtmltopdf -y

echo "Add rabbitMQ plugins"
rabbitVersion=`rabbitmqctl status | grep version | awk '{print $3}'`
link=`curl https://github.com/rabbitmq/rabbitmq-delayed-message-exchange/releases/tag/$rabbitVersion | grep .ez | grep href | grep $rabbitVersion | sed 's/.*download//g' | sed 's/\.ez.*/\.ez/g'`
wget https://github.com/rabbitmq/rabbitmq-delayed-message-exchange/releases/download/$link -O /usr/lib/rabbitmq/lib/rabbitmq_server-$rabbitVersion/plugins/rabbitmq_delayed_message_exchange-$rabbitVersion.ez
sudo rabbitmq-plugins enable rabbitmq_management rabbitmq_tracing rabbitmq_federation rabbitmq_delayed_message_exchange

service rabbitmq-server restart
rabbitmqctl add_user admin AsecretPASS
rabbitmqctl set_user_tags admin administrator
rabbitmqctl set_permissions -p / admin ".*" ".*" ".*"



echo "Installing postfix"
debconf-set-selections <<<"postfix postfix/mailname string dev.Gdpr.local"
debconf-set-selections <<<"postfix postfix/main_mailer_type string 'Internet Site'"
apt-get install -y postfix

if [ ! -f  /etc/ldap/certs/ad.igroovedev.network.cer ]; then
    mkdir -p /etc/ldap/certs/
    cp /vagrant/provision/ldap.conf /etc/ldap/ldap.conf
    cp /vagrant/provision/ad.igroovedev.network.cer /etc/ldap/certs/ad.igroovedev.network.cer
fi



echo "Configuring postgresQL"
sudo -u postgres psql -U postgres -d postgres -c "alter user postgres with password 'postgres';"
PG_PATH=$(pg_config | grep SHAREDIR | awk -e '{ print $3 }' | sed 's/usr\/share/etc/g')
PG_CONFIG=$PG_PATH/main/postgresql.conf
COUNT=$(grep -c "listen_addresses = '\*'" $PG_CONFIG)
if [ $COUNT -eq 0 ]; then
  echo "listen_addresses = '*'" >>$PG_CONFIG
fi
PG_HBA=$PG_PATH/main/pg_hba.conf
COUNT=$(grep -c "host    all       all   0.0.0.0/0     md5" $PG_HBA)
if [ $COUNT -eq 0 ]; then
  echo "host    all       all   0.0.0.0/0     md5" >>$PG_HBA
fi
service postgresql restart


echo "Configuring openLiteSpeed"
if [ ! -f /usr/local/lsws/admin/fcgi-bin/admin_php ]; then
    wget -O - http://rpms.litespeedtech.com/debian/enable_lst_debain_repo.sh | bash
    apt install openlitespeed -yq
    systemctl is-enabled lsws
    systemctl status lsws
    apt install lsphp81 lsphp81-common lsphp81-mysql  lsphp81-curl pkg-config \
          lsphp81-imagick lsphp81-intl  lsphp81-ldap lsphp81-pgsql -yq
    apt install lsphp81-dev libpcre3=2:8.39-13 -yq --allow-downgrades
    grep -r lsphp74/bin/lsphp /usr/local/lsws/conf -exec  sed -i 's\lsphp74\lsphp81\g' {} \;
    ENCRYPT_PASS=`/usr/local/lsws/admin/fcgi-bin/admin_php -q /usr/local/lsws/admin/misc/htpasswd.php password`
    echo "admin:$ENCRYPT_PASS" > /usr/local/lsws/admin/conf/htpasswd
    find /usr/local/lsws/conf -name 'httpd_config*.*' -exec  sed -i 's\lsphp74\lsphp81\g' {} \;
    find /usr/local/lsws/conf -name 'httpd_config*.*' -exec  sed -i 's\8088\80\g
' {} \;
    find /usr/local/lsws/conf -name 'httpd_config*.*' -exec  sed -i 's\vhRoot *Example\vhRoot /vagrant/public\g' {} \;
    find /usr/local/lsws/conf/vhosts -name 'vhconf*.*' -exec  sed -i 's\VH_ROOT/html/\VH_ROOT/\g' {} \;
    awk '!/rewrite {/' RS= ORS="\n\n" /usr/local/lsws/conf/vhosts/Example/vhconf.conf > /tmp/vhconf.conf
    cat >>  /tmp/vhconf.conf <<ADDTEXT
rewrite  {
  enable                  1
  autoLoadHtaccess        1
  logLevel                0
}
ADDTEXT
    cp /tmp/vhconf.conf /usr/local/lsws/conf/vhosts/Example/vhconf.conf
    systemctl restart lsws
    update-alternatives --install /usr/bin/php php /usr/local/lsws/lsphp81/bin/php8.1 10
    update-alternatives --set php /usr/local/lsws/lsphp81/bin/php8.1
fi




if [ ! -f /usr/local/lsws/lsphp81/etc/php/8.1/mods-available/40-amqp.ini ]; then
  apt install  librabbitmq-dev -yq
  cd /tmp
    wget https://pecl.php.net/get/amqp-1.11.0.tgz
      tar xvzf amqp-1.11.0.tgz
      cd amqp-1.11.0/
      /usr/local/lsws/lsphp81/bin/phpize
      ./configure --with-php-config=/usr/local/lsws/lsphp81/bin/php-config
  ./configure --with-php-config=/usr/local/lsws/lsphp81/bin/php-config --with-amqp=/usr/local/lsws/lsphp81
  make
  make install
  ls   /usr/local/lsws/lsphp81/lib/php/20210902/
  echo "extension=amqp.so" > /usr/local/lsws/lsphp81/etc/php/8.1/mods-available/40-amqp.ini
  killall lsphp;killall lsphp;killall lsphp;
  systemctl restart lsws
fi




if [ ! -f /usr/local/bin/composer ]; then
  echo "Get composer"
  curl -sS https://getcomposer.org/installer | php -- --install-dir=/usr/bin
  mv /usr/bin/composer.phar /usr/bin/composer
  chmod a+x /usr/bin/composer
fi

mkdir /dev/shm/sf
chmod -R 777 /dev/shm/sf

