# igroove v.7

## Cosa è igroove

E' un sistema web costruito utilizzando PHP, mysql, jquery, HTML5,... e tante altre tecnologie.

Si installa su una macchina Debian (meglio se 11) con PHP, PostgreSQL, rabbitMQ

# Cosa serve?

Serve per riportare l'equilibrio nella gestione della infrastruttura di rete di una scuola o di un centro di formazione professionale. Ridare ai docenti il controllo dell'accesso ad internet, della gestione delle password dimenticate dagli studenti, delle cartelle in cui consegnare gli elaborati,...



## Licenza

Il software è coperto da licenza MIT la quale consente il libero utilizzo del software, la modifica e la distribuzione con o senza modifiche, con qualsiasi licenza, a patto che si rispettino alcuni vincoli:

* una copia della licenza deve essere inclusa nel materiale fornito con il programma;
* in caso di distribuzione di opere derivate, deve essere evidenziato quali file sono stati modificati;
* deve essere mantenuto ogni riferimento a copyright, marchi, brevetti e altre attribuzioni indicate nel software originario;
* deve essere evidenziato nel materiale fornito con l'opera derivata il fatto di aver utilizzato il software coperto dalla licenza.



## Nota importante

Si ricorda che igroove è fornito as-it-it. Nessuna responsabilità è imputabile a chi sviluppa il codice.

Ma se hai bisogno di una funzionalità o se ritieni che la documentazione sia carente perchè non investire e contribuire allo sviluppo del progetto?


## Documentazione specifica della versione 7

La documentazione è nella cartella [docs](docs)

- [Script per una tipica installazione](provision/provision.sh)
- [Upgrade dalla versione 6](docs/upgrade_from_6_to_7.md)
- [Promemoria per la gestione degli ospiti con 802.1x](docs/guest.md)

## Documentazione precedente
- [Installazione](installazione)
- [Integrazione con segremat](segremat)
- [Integrazione con freeRADIUS](freeradius)
- [Integrazione con Google Apps](googleApps)
- [Risoluzione problemi comuni](risoluzioneProblemi)
- [Aggiornamento del sistema a versioni minori](aggiornamento)
- [Come permettere il riavvio: server Ubiquiti unifi](istruzioniUnifi)
- [Come permettere il riavvio: server Radius](istruzioniRadius)
- [Come aggiornare dalla v3 alla v4](upgrading)
- [Come aggiornare dalla v4 alla v5](aggiornamento)
- [Samba 802.1x per guest users](samba)
- [Check list di una tipica installazione](checklist)

---

## Modifiche rispetto alla versione 6
- Debian 11
- PHP 8.1 + symfony 5.4 LTS + LdapRecord
- PostgreSQL
- RabbitMQ con plugin per le delayed queues
- Pagina di informazioni sulla versione installata
- Comandi di test

## Comandi di test


`php bin/console [comando]`

- `test:version` Informazioni sulla versione installata e disponibile
- `test:ad` Test della corretta integrazione con Microsoft Active directory
- `test:password` Informazioni ed esempi di impostazione delle password
- `test:queue` Test della corretta integrazione con RabbitMQ server
- `test:google-apps` Test della corretta integrazione con Google workspace
