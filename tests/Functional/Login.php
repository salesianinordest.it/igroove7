<?php

namespace App\Tests\Functional;

use App\Entity\User;
use App\Tests\FunctionalTester;

final class Login
{
    public function redirectToLogin(FunctionalTester $I)
    {
        $I->amOnPage('/');
        $I->canSeeCurrentActionIs('login');
        $I->see('Accedi');
    }

    public function seeAuthentication(FunctionalTester $I)
    {
        $user = $I->grabEntityFromRepository(User::class, [
            'username' => 'raffaella',
        ]);
        $I->amLoggedInAs($user);
        $I->amOnPage('/');

        $I->seeAuthentication();
    }
}
